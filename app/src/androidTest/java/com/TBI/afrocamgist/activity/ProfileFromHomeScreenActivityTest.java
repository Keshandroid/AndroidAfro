package com.TBI.afrocamgist.activity;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.runner.AndroidJUnit4;

import com.TBI.afrocamgist.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ProfileFromHomeScreenActivityTest {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    @Rule
    public GrantPermissionRule mGrantPermissionRule =
            GrantPermissionRule.grant(
                    "android.permission.ACCESS_FINE_LOCATION",
                    "android.permission.ACCESS_COARSE_LOCATION",
                    "android.permission.CAMERA",
                    "android.permission.READ_EXTERNAL_STORAGE",
                    "android.permission.RECORD_AUDIO",
                    "android.permission.WRITE_EXTERNAL_STORAGE");

    @Test
    public void profileFromHomeScreenActivityTest() {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.loginHere), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottom),
                                        1),
                                2),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.username),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(longClick());

        ViewInteraction linearLayout = onView(
                allOf(withContentDescription("\u200E\u200F\u200E\u200E\u200E\u200E\u200E\u200F\u200E\u200F\u200F\u200F\u200E\u200E\u200E\u200E\u200E\u200F\u200E\u200E\u200F\u200E\u200E\u200E\u200E\u200F\u200F\u200F\u200F\u200F\u200F\u200F\u200F\u200E\u200E\u200F\u200F\u200F\u200E\u200E\u200E\u200F\u200E\u200E\u200E\u200E\u200F\u200E\u200F\u200E\u200F\u200F\u200E\u200F\u200E\u200F\u200F\u200E\u200E\u200F\u200E\u200F\u200E\u200F\u200F\u200F\u200E\u200F\u200F\u200E\u200F\u200F\u200E\u200E\u200E\u200F\u200E\u200F\u200F\u200E\u200E\u200E\u200E\u200F\u200E\u200E\u200F\u200F\u200E\u200E\u200F\u200E\u200E\u200E\u200F\u200EPaste\u200E\u200F\u200E\u200E\u200F\u200E"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        1),
                                0),
                        isDisplayed()));
        linearLayout.perform(click());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.username),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("akshaydobariya3437@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.password),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        1),
                                0),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("12345678"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        1),
                                3),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatTextView2 = onView(
                allOf(withId(R.id.ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                2),
                        isDisplayed()));
        appCompatTextView2.perform(click());

        ViewInteraction relativeLayout = onView(
                allOf(withId(R.id.relProfile),
                        childAtPosition(
                                allOf(withId(R.id.topProfileLayout),
                                        childAtPosition(
                                                withId(R.id.mainItemLayout),
                                                5)),
                                0),
                        isDisplayed()));
        relativeLayout.perform(click());

        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.follow1),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction appCompatImageView2 = onView(
                allOf(withId(R.id.follow1),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView2.perform(click());

        ViewInteraction appCompatImageView3 = onView(
                allOf(withId(R.id.more),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.header),
                                        0),
                                3),
                        isDisplayed()));
        appCompatImageView3.perform(click());

        ViewInteraction appCompatTextView3 = onView(
                allOf(withId(R.id.tvBlock), withText("Block"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.ll_more),
                                        0),
                                0),
                        isDisplayed()));
        appCompatTextView3.perform(click());

        ViewInteraction appCompatTextView4 = onView(
                allOf(withId(R.id.ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                4),
                        isDisplayed()));
        appCompatTextView4.perform(click());

        ViewInteraction appCompatImageView4 = onView(
                allOf(withId(R.id.more),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.header),
                                        0),
                                3),
                        isDisplayed()));
        appCompatImageView4.perform(click());

        ViewInteraction appCompatTextView5 = onView(
                allOf(withId(R.id.tvBlock), withText("Unblock"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.ll_more),
                                        0),
                                0),
                        isDisplayed()));
        appCompatTextView5.perform(click());

        ViewInteraction appCompatTextView6 = onView(
                allOf(withId(R.id.ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                4),
                        isDisplayed()));
        appCompatTextView6.perform(click());

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.horizontal_list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        pressBack();

        pressBack();

        ViewInteraction relativeLayout2 = onView(
                allOf(withId(R.id.relProfile),
                        childAtPosition(
                                allOf(withId(R.id.topProfileLayout),
                                        childAtPosition(
                                                withId(R.id.mainItemLayout),
                                                5)),
                                0),
                        isDisplayed()));
        relativeLayout2.perform(click());

        pressBack();

        ViewInteraction appCompatTextView7 = onView(
                allOf(withId(R.id.ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                3),
                        isDisplayed()));
        appCompatTextView7.perform(click());

        ViewInteraction appCompatTextView8 = onView(
                allOf(withId(R.id.follow), withText("Follow"),
                        childAtPosition(
                                allOf(withId(R.id.topProfileLayout),
                                        childAtPosition(
                                                withId(R.id.mainItemLayout),
                                                5)),
                                1),
                        isDisplayed()));
        appCompatTextView8.perform(click());

        ViewInteraction relativeLayout3 = onView(
                allOf(withId(R.id.relProfile),
                        childAtPosition(
                                allOf(withId(R.id.topProfileLayout),
                                        childAtPosition(
                                                withId(R.id.mainItemLayout),
                                                5)),
                                0),
                        isDisplayed()));
        relativeLayout3.perform(click());

        ViewInteraction appCompatImageView5 = onView(
                allOf(withId(R.id.follow1),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView5.perform(click());

        ViewInteraction appCompatImageView6 = onView(
                allOf(withId(R.id.back),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.header),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView6.perform(click());

        pressBack();

        ViewInteraction tabView = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.tabs),
                                0),
                        0),
                        isDisplayed()));
        tabView.perform(click());

        ViewInteraction relativeLayout4 = onView(
                allOf(withId(R.id.relProfile),
                        childAtPosition(
                                allOf(withId(R.id.topProfileLayout),
                                        childAtPosition(
                                                withId(R.id.mainItemLayout),
                                                5)),
                                0),
                        isDisplayed()));
        relativeLayout4.perform(click());

        ViewInteraction appCompatImageView7 = onView(
                allOf(withId(R.id.follow1),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView7.perform(click());

        ViewInteraction appCompatImageView8 = onView(
                allOf(withId(R.id.follow1),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView8.perform(click());

        pressBack();

        ViewInteraction relativeLayout5 = onView(
                allOf(withId(R.id.relProfile),
                        childAtPosition(
                                allOf(withId(R.id.topProfileLayout),
                                        childAtPosition(
                                                withId(R.id.mainItemLayout),
                                                5)),
                                0),
                        isDisplayed()));
        relativeLayout5.perform(click());

        ViewInteraction appCompatImageView9 = onView(
                allOf(withId(R.id.more),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.header),
                                        0),
                                3),
                        isDisplayed()));
        appCompatImageView9.perform(click());

        ViewInteraction linearLayout2 = onView(
                allOf(withId(R.id.ll_cancel),
                        childAtPosition(
                                allOf(withId(R.id.ll_more),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                3)),
                                1),
                        isDisplayed()));
        linearLayout2.perform(click());

        ViewInteraction appCompatImageView10 = onView(
                allOf(withId(R.id.back),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.header),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView10.perform(click());

        pressBack();
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
