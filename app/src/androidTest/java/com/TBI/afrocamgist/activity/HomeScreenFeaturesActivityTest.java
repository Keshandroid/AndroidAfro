package com.TBI.afrocamgist.activity;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.intent.Intents;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.runner.AndroidJUnit4;

import com.TBI.afrocamgist.R;

import junit.framework.TestCase;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class HomeScreenFeaturesActivityTest {

    @Rule
    public ActivityTestRule<UsernameActivity> mActivityTestRule = new ActivityTestRule<>(UsernameActivity.class);

   /* @Rule
    public GrantPermissionRule mGrantPermissionRule =
            GrantPermissionRule.grant(
                    "android.permission.ACCESS_FINE_LOCATION",
                    "android.permission.ACCESS_COARSE_LOCATION",
                    "android.permission.CAMERA",
                    "android.permission.READ_EXTERNAL_STORAGE",
                    "android.permission.RECORD_AUDIO",
                    "android.permission.WRITE_EXTERNAL_STORAGE");*/

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    public static ViewAction waitFor(long delay) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for " + delay + "milliseconds";
            }

            @Override
            public void perform(UiController uiController, View view) {
                uiController.loopMainThreadForAtLeast(delay);
            }
        };
    }

    @Test
    public void homeScreenFeaturesActivityTest() {
        waitFor(7000L);

//        onView(withText(endsWith("Login"))).check(matches(isDisplayed()));

        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.loginHere), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottom),
                                        1),
                                2),
                        isDisplayed()));
        appCompatTextView.perform(click());


        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.username),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(click());

        /*ViewInteraction linearLayout = onView(
                allOf(withContentDescription("\u200E\u200F\u200E\u200E\u200E\u200E\u200E\u200F\u200E\u200F\u200F\u200F\u200E\u200E\u200E\u200E\u200E\u200F\u200E\u200E\u200F\u200E\u200E\u200E\u200E\u200F\u200F\u200F\u200F\u200F\u200F\u200F\u200F\u200E\u200E\u200F\u200F\u200F\u200E\u200E\u200E\u200F\u200E\u200E\u200E\u200E\u200F\u200E\u200F\u200E\u200F\u200F\u200E\u200F\u200E\u200F\u200F\u200E\u200E\u200F\u200E\u200F\u200E\u200F\u200F\u200F\u200E\u200F\u200F\u200E\u200F\u200F\u200E\u200E\u200E\u200F\u200E\u200F\u200F\u200E\u200E\u200E\u200E\u200F\u200E\u200E\u200F\u200F\u200E\u200E\u200F\u200E\u200E\u200E\u200F\u200EPaste\u200E\u200F\u200E\u200E\u200F\u200E"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        1),
                                0),
                        isDisplayed()));
        linearLayout.perform(click());*/

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.username),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("akshaydobariya3437@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.password),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        1),
                                0),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("12345678"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        1),
                                3),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatTextView2 = onView(
                allOf(withId(R.id.update), withText("Update detail"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        1),
                                14)));
        appCompatTextView2.perform(scrollTo(), click());

        ViewInteraction appCompatTextView3 = onView(
                allOf(withId(R.id.ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                2),
                        isDisplayed()));
        appCompatTextView3.perform(click());

        ViewInteraction appCompatToggleButton = onView(
                allOf(withId(R.id.like),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.relControllers),
                                        0),
                                1),
                        isDisplayed()));
        appCompatToggleButton.perform(click());

        ViewInteraction appCompatToggleButton2 = onView(
                allOf(withId(R.id.like),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.relControllers),
                                        0),
                                1),
                        isDisplayed()));
        appCompatToggleButton2.perform(click());

        ViewInteraction appCompatTextView4 = onView(
                allOf(withId(R.id.comment), withText("1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.relControllers),
                                        0),
                                3),
                        isDisplayed()));
        appCompatTextView4.perform(click());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.user_comment),
                        childAtPosition(
                                allOf(withId(R.id.write_comment),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                4)),
                                0),
                        isDisplayed()));
        appCompatEditText4.perform(replaceText("tetest"), closeSoftKeyboard());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.comment), withText("Comment"),
                        childAtPosition(
                                allOf(withId(R.id.write_comment),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                4)),
                                1),
                        isDisplayed()));
        appCompatButton2.perform(click());

        ViewInteraction appCompatTextView5 = onView(
                allOf(withId(R.id.emoji2), withText("\uD83D\uDE4C"),
                        childAtPosition(
                                allOf(withId(R.id.llEmoji),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                2)),
                                1),
                        isDisplayed()));
        appCompatTextView5.perform(click());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.comment), withText("Comment"),
                        childAtPosition(
                                allOf(withId(R.id.write_comment),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                4)),
                                1),
                        isDisplayed()));
        appCompatButton3.perform(click());

        ViewInteraction switchCompat = onView(
                allOf(withId(R.id.switchAutoPlayVideoDialog), withText("Auto Play Videos"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        switchCompat.perform(click());

        ViewInteraction appCompatTextView6 = onView(
                allOf(withId(R.id.ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                3),
                        isDisplayed()));
        appCompatTextView6.perform(click());

        ViewInteraction view = onView(
                allOf(withId(R.id.touch_outside),
                        childAtPosition(
                                allOf(withId(R.id.coordinator),
                                        childAtPosition(
                                                withId(R.id.container),
                                                0)),
                                0),
                        isDisplayed()));
        view.perform(click());

        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.share),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.relControllers),
                                        0),
                                4),
                        isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.share_post_text),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText5.perform(replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatButton4 = onView(
                allOf(withId(R.id.share), withText("Share"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                2)));
        appCompatButton4.perform(scrollTo(), click());

        ViewInteraction appCompatImageView2 = onView(
                allOf(withId(R.id.report),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.relControllers),
                                        0),
                                6),
                        isDisplayed()));
        appCompatImageView2.perform(click());

        ViewInteraction appCompatTextView7 = onView(
                allOf(withId(R.id.cancel), withText("Cancel"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        2),
                                2),
                        isDisplayed()));
        appCompatTextView7.perform(click());

        ViewInteraction appCompatImageView3 = onView(
                allOf(withId(R.id.imgEdit),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.relControllers),
                                        0),
                                7),
                        isDisplayed()));
        appCompatImageView3.perform(click());

        ViewInteraction appCompatTextView8 = onView(
                allOf(withId(R.id.label), withText("Delete"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.menuList),
                                        1),
                                0),
                        isDisplayed()));
        appCompatTextView8.perform(click());

        ViewInteraction appCompatTextView9 = onView(
                allOf(withId(R.id.delete), withText("Delete"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        2),
                                0),
                        isDisplayed()));
        appCompatTextView9.perform(click());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
