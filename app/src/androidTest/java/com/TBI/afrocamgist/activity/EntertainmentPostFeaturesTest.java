package com.TBI.afrocamgist.activity;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.runner.AndroidJUnit4;

import com.TBI.afrocamgist.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EntertainmentPostFeaturesTest {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    @Rule
    public GrantPermissionRule mGrantPermissionRule =
            GrantPermissionRule.grant(
                    "android.permission.ACCESS_FINE_LOCATION",
                    "android.permission.ACCESS_COARSE_LOCATION",
                    "android.permission.CAMERA",
                    "android.permission.READ_EXTERNAL_STORAGE",
                    "android.permission.RECORD_AUDIO",
                    "android.permission.WRITE_EXTERNAL_STORAGE");

    @Test
    public void entertainmentPostFeaturesTest() {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.loginHere), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottom),
                                        1),
                                2),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.username),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(longClick());

        ViewInteraction linearLayout = onView(
                allOf(withContentDescription("\u200E\u200F\u200E\u200E\u200E\u200E\u200E\u200F\u200E\u200F\u200F\u200F\u200E\u200E\u200E\u200E\u200E\u200F\u200E\u200E\u200F\u200E\u200E\u200E\u200E\u200F\u200F\u200F\u200F\u200F\u200F\u200F\u200F\u200E\u200E\u200F\u200F\u200F\u200E\u200E\u200E\u200F\u200E\u200E\u200E\u200E\u200F\u200E\u200F\u200E\u200F\u200F\u200E\u200F\u200E\u200F\u200F\u200E\u200E\u200F\u200E\u200F\u200E\u200F\u200F\u200F\u200E\u200F\u200F\u200E\u200F\u200F\u200E\u200E\u200E\u200F\u200E\u200F\u200F\u200E\u200E\u200E\u200E\u200F\u200E\u200E\u200F\u200F\u200E\u200E\u200F\u200E\u200E\u200E\u200F\u200EPaste\u200E\u200F\u200E\u200E\u200F\u200E"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        1),
                                0),
                        isDisplayed()));
        linearLayout.perform(click());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.username),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("akshaydobariya3437@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.password),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        1),
                                0),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("12345678"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        1),
                                3),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatTextView2 = onView(
                allOf(withId(R.id.ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                2),
                        isDisplayed()));
        appCompatTextView2.perform(click());

        ViewInteraction tabView = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.tabs),
                                0),
                        2),
                        isDisplayed()));
        tabView.perform(click());

        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.post_image),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.entertainment),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction appCompatToggleButton = onView(
                allOf(withId(R.id.volume),
                        childAtPosition(
                                allOf(withId(R.id.video_layout),
                                        childAtPosition(
                                                withClassName(is("android.widget.FrameLayout")),
                                                1)),
                                1),
                        isDisplayed()));
        appCompatToggleButton.perform(click());

        ViewInteraction appCompatToggleButton2 = onView(
                allOf(withId(R.id.volume),
                        childAtPosition(
                                allOf(withId(R.id.video_layout),
                                        childAtPosition(
                                                withClassName(is("android.widget.FrameLayout")),
                                                1)),
                                1),
                        isDisplayed()));
        appCompatToggleButton2.perform(click());

        ViewInteraction appCompatToggleButton3 = onView(
                allOf(withId(R.id.likes),
                        childAtPosition(
                                allOf(withId(R.id.llBottom),
                                        childAtPosition(
                                                withId(R.id.gallery_layout),
                                                4)),
                                0),
                        isDisplayed()));
        appCompatToggleButton3.perform(click());

        ViewInteraction appCompatToggleButton4 = onView(
                allOf(withId(R.id.likes),
                        childAtPosition(
                                allOf(withId(R.id.llBottom),
                                        childAtPosition(
                                                withId(R.id.gallery_layout),
                                                4)),
                                0),
                        isDisplayed()));
        appCompatToggleButton4.perform(click());

        ViewInteraction appCompatTextView3 = onView(
                allOf(withId(R.id.comments), withText("0"),
                        childAtPosition(
                                allOf(withId(R.id.llBottom),
                                        childAtPosition(
                                                withId(R.id.gallery_layout),
                                                4)),
                                2),
                        isDisplayed()));
        appCompatTextView3.perform(click());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.user_comment),
                        childAtPosition(
                                allOf(withId(R.id.write_comment),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                4)),
                                0),
                        isDisplayed()));
        appCompatEditText4.perform(replaceText("tr"), closeSoftKeyboard());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.comment), withText("Comment"),
                        childAtPosition(
                                allOf(withId(R.id.write_comment),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                4)),
                                1),
                        isDisplayed()));
        appCompatButton2.perform(click());

        ViewInteraction appCompatTextView4 = onView(
                allOf(withId(R.id.comments), withText("1"),
                        childAtPosition(
                                allOf(withId(R.id.llBottom),
                                        childAtPosition(
                                                withId(R.id.gallery_layout),
                                                4)),
                                2),
                        isDisplayed()));
        appCompatTextView4.perform(click());

        ViewInteraction appCompatImageView2 = onView(
                allOf(withId(R.id.menu),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatImageView2.perform(click());

        ViewInteraction appCompatTextView5 = onView(
                allOf(withId(R.id.label), withText("Delete"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.menuList),
                                        1),
                                0),
                        isDisplayed()));
        appCompatTextView5.perform(click());

        ViewInteraction view = onView(
                allOf(withId(R.id.touch_outside),
                        childAtPosition(
                                allOf(withId(R.id.coordinator),
                                        childAtPosition(
                                                withId(R.id.container),
                                                0)),
                                0),
                        isDisplayed()));
        view.perform(click());

        ViewInteraction appCompatImageView3 = onView(
                allOf(withId(R.id.share),
                        childAtPosition(
                                allOf(withId(R.id.llBottom),
                                        childAtPosition(
                                                withId(R.id.gallery_layout),
                                                4)),
                                3),
                        isDisplayed()));
        appCompatImageView3.perform(click());

        ViewInteraction appCompatToggleButton5 = onView(
                allOf(withId(R.id.follow), withText("Follow"),
                        childAtPosition(
                                allOf(withId(R.id.profile_layout),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatToggleButton5.perform(click());

        ViewInteraction appCompatImageView4 = onView(
                allOf(withId(R.id.back),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView4.perform(click());

        pressBack();

        ViewInteraction appCompatImageView5 = onView(
                allOf(withId(R.id.share),
                        childAtPosition(
                                allOf(withId(R.id.llBottom),
                                        childAtPosition(
                                                withId(R.id.gallery_layout),
                                                4)),
                                3),
                        isDisplayed()));
        appCompatImageView5.perform(click());

        pressBack();

        ViewInteraction relativeLayout = onView(
                allOf(withId(R.id.relProfile),
                        childAtPosition(
                                allOf(withId(R.id.topProfileLayout),
                                        childAtPosition(
                                                withId(R.id.gallery_layout),
                                                1)),
                                0),
                        isDisplayed()));
        relativeLayout.perform(click());

        ViewInteraction appCompatImageView6 = onView(
                allOf(withId(R.id.follow1),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatImageView6.perform(click());

        pressBack();

        pressBack();

        ViewInteraction appCompatTextView6 = onView(
                allOf(withId(R.id.ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                3),
                        isDisplayed()));
        appCompatTextView6.perform(click());

        ViewInteraction appCompatTextView7 = onView(
                allOf(withId(R.id.hashtags), withText("Hashtags"),
                        childAtPosition(
                                allOf(withId(R.id.explorer_tabs),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatTextView7.perform(click());

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.hashtag_list),
                        childAtPosition(
                                withId(R.id.swipeContainer),
                                0)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction appCompatToggleButton6 = onView(
                allOf(withId(R.id.follow_hashtag), withText("Follow"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatToggleButton6.perform(click());

        pressBack();

        ViewInteraction appCompatToggleButton7 = onView(
                allOf(withId(R.id.follow), withText("Unfollow"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.hashtags),
                                        1),
                                2),
                        isDisplayed()));
        appCompatToggleButton7.perform(click());

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.search),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layout_explorer),
                                        0),
                                1),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.search),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText5.perform(replaceText("yh"), closeSoftKeyboard());

        ViewInteraction appCompatToggleButton8 = onView(
                allOf(withId(R.id.follow), withText("Follow"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.hashtags),
                                        1),
                                2),
                        isDisplayed()));
        appCompatToggleButton8.perform(click());

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.hashtag_list),
                        childAtPosition(
                                withId(R.id.swipeContainer),
                                0)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

        pressBack();

        ViewInteraction appCompatToggleButton9 = onView(
                allOf(withId(R.id.follow), withText("Unfollow"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.hashtags),
                                        1),
                                2),
                        isDisplayed()));
        appCompatToggleButton9.perform(click());

        pressBack();
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
