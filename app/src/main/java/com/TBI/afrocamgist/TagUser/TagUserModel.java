package com.TBI.afrocamgist.TagUser;

import com.TBI.afrocamgist.model.chat.Conversation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TagUserModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<SomeOne> allUsersData = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<SomeOne> getAllUsersData() {
        return allUsersData;
    }

    public void setAllUsersData(ArrayList<SomeOne> allUsersData) {
        this.allUsersData = allUsersData;
    }
}
