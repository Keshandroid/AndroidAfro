package com.TBI.afrocamgist.socialview;

import com.TBI.afrocamgist.model.background.BackgroundImage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HashtagPojo {
    @SerializedName("count")
    @Expose
    private int count;

    @SerializedName("data")
    @Expose
    private ArrayList<HashtagAll> hashtagAlls = null;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<HashtagAll> getHashtagAlls() {
        return hashtagAlls;
    }

    public void setHashtagAlls(ArrayList<HashtagAll> hashtagAlls) {
        this.hashtagAlls = hashtagAlls;
    }

    public class HashtagAll{

        @SerializedName("_id")
        @Expose
        private String _id;
        @SerializedName("hashtag_slug")
        @Expose
        private String hashtag_slug;
        @SerializedName("hashtag_status")
        @Expose
        private String hashtag_status;
        @SerializedName("hashtag_id")
        @Expose
        private String hashtag_id;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getHashtag_slug() {
            return hashtag_slug;
        }

        public void setHashtag_slug(String hashtag_slug) {
            this.hashtag_slug = hashtag_slug;
        }

        public String getHashtag_status() {
            return hashtag_status;
        }

        public void setHashtag_status(String hashtag_status) {
            this.hashtag_status = hashtag_status;
        }

        public String getHashtag_id() {
            return hashtag_id;
        }

        public void setHashtag_id(String hashtag_id) {
            this.hashtag_id = hashtag_id;
        }
    }

}
