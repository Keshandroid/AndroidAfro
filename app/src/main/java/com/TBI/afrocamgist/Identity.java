package com.TBI.afrocamgist;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Identity {

    private static final String AUTO_PLAY_VIDEO = "AUTO_PLAY_VIDEO";
    private static final String AUTO_PLAY_VIDEO_PREFERENCE = "AUTO_PLAY_VIDEO_PREFERENCE";
    private static final String DIALOG_GUIDE_PREFERENCE = "DIALOG_GUIDE_PREFERENCE";
    private static final String ANNOUNCEMENT_POPUP = "ANNOUNCEMENT_POPUP";

    public static boolean getAutoPlayVideo(Context mContext) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return mPreferences.getBoolean(AUTO_PLAY_VIDEO, true);
    }

    public static void setAutoPlayVideo(Context mContext, boolean localeKey) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mPreferences.edit().putBoolean(AUTO_PLAY_VIDEO, localeKey).apply();
    }

    public static boolean getAutoPlayVideoPreference(Context mContext) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return mPreferences.getBoolean(AUTO_PLAY_VIDEO_PREFERENCE, false);
    }

    public static void setAutoPlayVideoPreference(Context mContext, boolean localeKey) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mPreferences.edit().putBoolean(AUTO_PLAY_VIDEO_PREFERENCE, localeKey).apply();
    }

    public static boolean getDialogGuidePreference(Context mContext) {
        if(mContext!=null){
            SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
            return mPreferences.getBoolean(DIALOG_GUIDE_PREFERENCE, false);
        }
        return false;
    }

    public static void setDialogGuidePreference(Context mContext, boolean localeKey) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mPreferences.edit().putBoolean(DIALOG_GUIDE_PREFERENCE, localeKey).apply();
    }

    public static boolean getAnnouncementPopup(Context mContext) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return mPreferences.getBoolean(ANNOUNCEMENT_POPUP, false);
    }

    public static void setAnnouncementPopup(Context mContext, boolean isAnnouncement) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mPreferences.edit().putBoolean(ANNOUNCEMENT_POPUP, isAnnouncement).apply();
    }

}
