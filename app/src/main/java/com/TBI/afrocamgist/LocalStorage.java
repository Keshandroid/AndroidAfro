package com.TBI.afrocamgist;

import android.content.Context;
import android.content.SharedPreferences;

import com.TBI.afrocamgist.app.AfrocamgistApplication;
import com.TBI.afrocamgist.constants.PrefKeys;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.user.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class LocalStorage {

    public static void setToken(String token) {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PrefKeys.KEY_TOKEN, token);
        editor.apply();
    }

    public static String getToken() {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(PrefKeys.KEY_TOKEN, "");
    }

    public static void setIsUserLoggedIn(boolean isLoggedIn) {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PrefKeys.KEY_USER_LOGIN, isLoggedIn);
        editor.apply();
    }

    public static boolean getIsUserLoggedIn() {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(PrefKeys.KEY_USER_LOGIN, false);
    }

    public static void setIsUserNotRegisterd(boolean isLoggedIn) {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PrefKeys.KEY_USER_NOT_REGISTERED, isLoggedIn);
        editor.apply();
    }

    public static boolean getIsUserNotRegisterd() {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(PrefKeys.KEY_USER_NOT_REGISTERED, false);
    }

    public static void setUserPass(String pass) {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PrefKeys.KEY_USER_PASS, pass);
        editor.apply();
    }

    public static String getUserPass() {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(PrefKeys.KEY_USER_PASS, "");
    }

    public static void setProfileImage(boolean isLoggedIn) {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PrefKeys.KEY_USER_PROFILE, isLoggedIn);
        editor.apply();
    }

    public static boolean getProfileImage() {
        SharedPreferences sharedPreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_LOGIN_PREF,
                Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(PrefKeys.KEY_USER_PROFILE, false);
    }

    public static void saveLoginToken(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_LOGIN_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_LOGIN, value);
        editor.apply();
    }

    public static String getLoginToken() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_LOGIN_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_LOGIN, "");
    }

    public static void saveSecretKey(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_LOGIN_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_SECRET, value);
        editor.apply();
    }

    public static String getSecretKey() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_LOGIN_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_SECRET, "");
    }

    public static void setIsVideoScreenOpen(boolean value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_VIDEO_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(PrefKeys.IS_VIDEO_SCREEN_OPEN, value);
        editor.apply();
    }

    public static boolean getIsVideoScreenOpen() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_VIDEO_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getBoolean(PrefKeys.IS_VIDEO_SCREEN_OPEN, false);
    }

    public static void saveIntroduce(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_LOGIN_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_INTRODUCE, value);
        editor.apply();
    }

    public static String getIntroduce() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_LOGIN_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_INTRODUCE, "");
    }

    public static void saveUserDetails(User user) {
        SharedPreferences prefs = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(PrefKeys.KEY_USER, json);
        editor.apply();
    }

    public static User getUserDetails() {
        SharedPreferences prefs = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_USER_PREF,Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(PrefKeys.KEY_USER, null);
        Type type = new TypeToken<User>() {}.getType();
        return gson.fromJson(json, type);
    }

    public static void savePopularPostsImage(ArrayList<String> images) {

        SharedPreferences prefs = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_POPULAR_POST_IMAGES_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        Gson gson = new Gson();
        String json = gson.toJson(images);
        editor.putString(PrefKeys.KEY_POPULAR_POST_IMAGES, json);
        editor.apply();
    }

    public static ArrayList<String> getPopularPostsImage() {
        SharedPreferences prefs = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_POPULAR_POST_IMAGES_PREF,Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(PrefKeys.KEY_POPULAR_POST_IMAGES, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }

    public static void savePopularPosts(ArrayList<Post> images) {

        SharedPreferences prefs = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_POPULAR_POST_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        Gson gson = new Gson();
        String json = gson.toJson(images);
        editor.putString(PrefKeys.KEY_POPULAR_POST, json);
        editor.apply();
    }

    public static ArrayList<Post> getPopularPosts() {
        SharedPreferences prefs = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_POPULAR_POST_PREF,Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(PrefKeys.KEY_POPULAR_POST, null);
        Type type = new TypeToken<ArrayList<Post>>() {}.getType();
        return gson.fromJson(json, type);
    }








    public static void setAdvertTransactionId(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_TRANSACTION_ID, value);
        editor.apply();
    }

    public static String getAdvertTransactionId() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_TRANSACTION_ID, "");
    }

    public static void setAdvertPostId(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_POST_ID, value);
        editor.apply();
    }

    public static String getAdvertPostId() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_POST_ID, "");
    }


    public static void setAdvertDuration(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_DURATION, value);
        editor.apply();
    }

    public static String getAdvertDuration() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_DURATION, "");
    }


    public static void setAdvertAmount(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_AMOUNT, value);
        editor.apply();
    }

    public static String getAdvertAmount() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_AMOUNT, "");
    }


    public static void setAdvertAudienceID(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_AUDIENCE_ID, value);
        editor.apply();
    }

    public static String getAdvertAudienceID() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_AUDIENCE_ID, "");
    }


    public static void setAdvertURL(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_URL, value);
        editor.apply();
    }

    public static String getAdvertURL() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_URL, "");
    }


    public static void setAdvertGoalType(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_GOAL_TYPE, value);
        editor.apply();
    }

    public static String getAdvertGoalType() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_GOAL_TYPE, "");
    }


    public static void setAdvertAudienceName(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_AUDIENCE_NAME, value);
        editor.apply();
    }

    public static String getAdvertAudienceName() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_AUDIENCE_NAME, "");
    }


    public static void setAdvertAudienceCountries(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_COUNTRIES, value);
        editor.apply();
    }

    public static String getAdvertAudienceCountries() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_COUNTRIES, "");
    }


    public static void setAdvertAudienceAgeRange(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_AGE_RANGE, value);
        editor.apply();
    }

    public static String getAdvertAudienceAgeRange() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_AGE_RANGE, "");
    }


    public static void setBudgetDuration(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_BUDGET_DURATION, value);
        editor.apply();
    }

    public static String getBudgetDuration() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_BUDGET_DURATION, "");
    }

    public static void setBudgetAmount(String value) {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefKeys.KEY_ADVERT_BUDGET_AMOUNT, value);
        editor.apply();
    }

    public static String getBudgetAmount() {
        SharedPreferences sharedpreferences = AfrocamgistApplication.getInstance().getSharedPreferences(PrefKeys.KEY_ADVERT_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PrefKeys.KEY_ADVERT_BUDGET_AMOUNT, "");
    }



}
