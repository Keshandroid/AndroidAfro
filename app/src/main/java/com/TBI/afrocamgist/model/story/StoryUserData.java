package com.TBI.afrocamgist.model.story;

import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.otheruser.RequestButton;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class StoryUserData implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("story_text")
    @Expose
    private String storyText;
    @SerializedName("post_lat_long")
    @Expose
    private String postLatLong;
    @SerializedName("posted_for")
    @Expose
    private String postedFor;
    @SerializedName("story_type")
    @Expose
    private String storyType;
    @SerializedName("story_image")
    @Expose
    private ArrayList<String> storyImage = new ArrayList<>();
    @SerializedName("story_video")
    @Expose
    private String storyVideo;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("posted_by")
    @Expose
    private Integer postedBy;
    @SerializedName("post_location")
    @Expose
    private String postLocation;
    @SerializedName("like_count")
    @Expose
    private Integer likeCount;
    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("post_status")
    @Expose
    private String postStatus;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("post_view_count")
    @Expose
    private String postViewCount;
    @SerializedName("story_id")
    @Expose
    private Integer storyId;
    @SerializedName("following")
    @Expose
    private Boolean following;
    @SerializedName("enable_follow")
    @Expose
    private Boolean enable_follow;
    @SerializedName("requestedUser")
    @Expose
    private Boolean requestedUser;
    @SerializedName("user_name")
    @Expose
    private String user_name;
    @SerializedName("profile_image_url")
    @Expose
    private String profileImageUrl;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("bg_map_post")
    @Expose
    private Boolean isMapPost;
    @SerializedName("bg_image_post")
    @Expose
    private Boolean isBackgroundImagePost;
    @SerializedName("bg_image")
    @Expose
    private String backgroundImage;

    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("comments")
    @Expose
    private ArrayList<Comment> comments = null;

    @SerializedName("private")
    @Expose
    private Boolean privateAccount;
    @SerializedName("request_buttons")
    @Expose
    private ArrayList<RequestButton> requestButtons = null;
    @SerializedName("liked")
    @Expose
    private Boolean liked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoryText() {
        return storyText;
    }

    public void setStoryText(String storyText) {
        this.storyText = storyText;
    }

    public String getPostLatLong() {
        return postLatLong;
    }

    public void setPostLatLong(String postLatLong) {
        this.postLatLong = postLatLong;
    }

    public String getPostedFor() {
        return postedFor;
    }

    public void setPostedFor(String postedFor) {
        this.postedFor = postedFor;
    }

    public String getStoryType() {
        return storyType;
    }

    public void setStoryType(String storyType) {
        this.storyType = storyType;
    }

    public ArrayList<String> getStoryImage() {
        return storyImage;
    }

    public void setStoryImage(ArrayList<String> storyImage) {
        this.storyImage = storyImage;
    }

    public String getStoryVideo() {
        return storyVideo;
    }

    public void setStoryVideo(String storyVideo) {
        this.storyVideo = storyVideo;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(Integer postedBy) {
        this.postedBy = postedBy;
    }

    public String getPostLocation() {
        return postLocation;
    }

    public void setPostLocation(String postLocation) {
        this.postLocation = postLocation;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPostViewCount() {
        return postViewCount;
    }

    public void setPostViewCount(String postViewCount) {
        this.postViewCount = postViewCount;
    }

    public Integer getStoryId() {
        return storyId;
    }

    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }

    public Boolean getFollowing() {
        return following;
    }

    public void setFollowing(Boolean following) {
        this.following = following;
    }

    public Boolean getEnable_follow() {
        return enable_follow;
    }

    public void setEnable_follow(Boolean enable_follow) {
        this.enable_follow = enable_follow;
    }

    public Boolean getRequestedUser() {
        return requestedUser;
    }

    public void setRequestedUser(Boolean requestedUser) {
        this.requestedUser = requestedUser;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getMapPost() {
        return isMapPost;
    }

    public void setMapPost(Boolean mapPost) {
        isMapPost = mapPost;
    }

    public Boolean getBackgroundImagePost() {
        return isBackgroundImagePost;
    }

    public void setBackgroundImagePost(Boolean backgroundImagePost) {
        isBackgroundImagePost = backgroundImagePost;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public Boolean getPrivateAccount() {
        return privateAccount;
    }

    public void setPrivateAccount(Boolean privateAccount) {
        this.privateAccount = privateAccount;
    }

    public ArrayList<RequestButton> getRequestButtons() {
        return requestButtons;
    }

    public void setRequestButtons(ArrayList<RequestButton> requestButtons) {
        this.requestButtons = requestButtons;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }
}
