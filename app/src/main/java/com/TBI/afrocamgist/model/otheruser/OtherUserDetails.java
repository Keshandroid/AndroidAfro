
package com.TBI.afrocamgist.model.otheruser;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtherUserDetails implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private OtherUser otherUser;
    private final static long serialVersionUID = 6849061996931226512L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public OtherUser getOtherUser() {
        return otherUser;
    }

    public void setOtherUser(OtherUser otherUser) {
        this.otherUser = otherUser;
    }

}
