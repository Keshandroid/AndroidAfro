package com.TBI.afrocamgist.model.post;

import com.TBI.afrocamgist.model.chat.Conversation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PostBaseResponse implements Serializable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private Post postData = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Post getPostData() {
        return postData;
    }

    public void setPostData(Post postData) {
        this.postData = postData;
    }
}
