package com.TBI.afrocamgist.model.ehealth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class FlutterwaveCreateResponse implements Serializable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private FlutterwaveResponse flutterwaveResponse;

    private final static long serialVersionUID = 2840523502568794762L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public FlutterwaveResponse getFlutterwaveResponse() {
        return flutterwaveResponse;
    }

    public void setFlutterwaveResponse(FlutterwaveResponse flutterwaveResponse) {
        this.flutterwaveResponse = flutterwaveResponse;
    }
}
