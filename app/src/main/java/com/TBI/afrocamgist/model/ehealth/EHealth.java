package com.TBI.afrocamgist.model.ehealth;

import com.TBI.afrocamgist.model.advert.Budget;
import com.TBI.afrocamgist.model.advert.Goal;
import com.TBI.afrocamgist.model.post.Post;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class EHealth implements Serializable{
    //create audience list
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("plan_id")
    @Expose
    private String plan_id;
    @SerializedName("slug")
    @Expose
    private String slug;

    @SerializedName("subPlans")
    @Expose
    private ArrayList<EHealthSubPlans> subPlans = null;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public ArrayList<EHealthSubPlans> getSubPlans() {
        return subPlans;
    }

    public void setSubPlans(ArrayList<EHealthSubPlans> subPlans) {
        this.subPlans = subPlans;
    }
}
