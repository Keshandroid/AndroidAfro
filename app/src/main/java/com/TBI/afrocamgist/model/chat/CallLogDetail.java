package com.TBI.afrocamgist.model.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class CallLogDetail implements Serializable {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<CallLog> callLogDetail = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<CallLog> getCallLogDetail() {
        return callLogDetail;
    }

    public void setCallLogDetail(ArrayList<CallLog> callLogDetail) {
        this.callLogDetail = callLogDetail;
    }
}
