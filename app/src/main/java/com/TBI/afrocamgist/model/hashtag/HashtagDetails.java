
package com.TBI.afrocamgist.model.hashtag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HashtagDetails implements Serializable
{

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("currentPage")
    @Expose
    private Integer currentPage;
    @SerializedName("data")
    @Expose
    private ArrayList<Hashtag> hashtagList = new ArrayList<>();
    @SerializedName("nextPage")
    @Expose
    private Integer nextPage;
    @SerializedName("status")
    @Expose
    private Boolean status;
    private final static long serialVersionUID = -229648804927750709L;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public ArrayList<Hashtag> getHashtagList() {
        return hashtagList;
    }

    public void setHashtagList(ArrayList<Hashtag> hashtagList) {
        this.hashtagList = hashtagList;
    }

    public Integer getNextPage() {
        return nextPage;
    }

    public void setNextPage(Integer nextPage) {
        this.nextPage = nextPage;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
