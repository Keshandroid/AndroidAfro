
package com.TBI.afrocamgist.model.rolemodel;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoleModelDetails implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<RoleModel> roleModels = null;
    private final static long serialVersionUID = -8637595166632704671L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<RoleModel> getRoleModels() {
        return roleModels;
    }

    public void setRoleModels(ArrayList<RoleModel> roleModels) {
        this.roleModels = roleModels;
    }

}
