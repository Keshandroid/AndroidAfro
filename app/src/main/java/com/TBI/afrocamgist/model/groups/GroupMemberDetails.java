
package com.TBI.afrocamgist.model.groups;

import com.TBI.afrocamgist.model.follow.Follow;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class GroupMemberDetails implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<GroupMemberData> groupMemberList = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<GroupMemberData> getGroupMemberList() {
        return groupMemberList;
    }

    public void setGroupMemberList(ArrayList<GroupMemberData> groupMemberList) {
        this.groupMemberList = groupMemberList;
    }
}
