package com.TBI.afrocamgist.model.ehealth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FlutterwaveResponse implements Serializable {

    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("tx_ref")
    @Expose
    private String tx_ref;

    @SerializedName("customer")
    @Expose
    private FlutterwaveCustomer customer;

    @SerializedName("meta")
    @Expose
    private FlutterwaveMeta meta;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTx_ref() {
        return tx_ref;
    }

    public void setTx_ref(String tx_ref) {
        this.tx_ref = tx_ref;
    }

    public FlutterwaveCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(FlutterwaveCustomer customer) {
        this.customer = customer;
    }

    public FlutterwaveMeta getMeta() {
        return meta;
    }

    public void setMeta(FlutterwaveMeta meta) {
        this.meta = meta;
    }
}
