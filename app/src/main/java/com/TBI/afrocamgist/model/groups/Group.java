
package com.TBI.afrocamgist.model.groups;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Group implements Serializable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("group_title")
    @Expose
    private String groupTitle;
    @SerializedName("group_category")
    @Expose
    private String groupCategory;
    @SerializedName("private")
    @Expose
    private Boolean _private;
    @SerializedName("isJoined")
    @Expose
    private Boolean isJoined;
    @SerializedName("group_description")
    @Expose
    private String groupDescription;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("group_admins")
    @Expose
    private ArrayList<Integer> groupAdmins = null;
    @SerializedName("group_members")
    @Expose
    private ArrayList<Integer> groupMembers = null;
    @SerializedName("group_image")
    @Expose
    private String groupImage;
    @SerializedName("group_cover_image")
    @Expose
    private String groupCoverImage;
    @SerializedName("group_status")
    @Expose
    private String groupStatus;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("group_id")
    @Expose
    private Integer groupId;
    @SerializedName("admin")
    @Expose
    private Boolean admin;
    @SerializedName("post_count")
    @Expose
    private Integer postCount;
    @SerializedName("buttons")
    @Expose
    private ArrayList<Button> buttons = null;
    @SerializedName("request_buttons")
    @Expose
    private ArrayList<Button> requestButtons = null;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("group_invited_members")
    @Expose
    private ArrayList<Object> groupInvitedMembers = null;
    @SerializedName("group_members_details")
    @Expose
    private ArrayList<GroupMember> groupMembersDetails = null;
    private final static long serialVersionUID = -6326530482607988159L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public String getGroupCategory() {
        return groupCategory;
    }

    public void setGroupCategory(String groupCategory) {
        this.groupCategory = groupCategory;
    }

    public Boolean getPrivate() {
        return _private;
    }

    public void setPrivate(Boolean _private) {
        this._private = _private;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public ArrayList<Integer> getGroupAdmins() {
        return groupAdmins;
    }

    public void setGroupAdmins(ArrayList<Integer> groupAdmins) {
        this.groupAdmins = groupAdmins;
    }

    public ArrayList<Integer> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(ArrayList<Integer> groupMembers) {
        this.groupMembers = groupMembers;
    }

    public String getGroupImage() {
        return groupImage;
    }

    public void setGroupImage(String groupImage) {
        this.groupImage = groupImage;
    }

    public String getGroupCoverImage() {
        return groupCoverImage;
    }

    public void setGroupCoverImage(String groupCoverImage) {
        this.groupCoverImage = groupCoverImage;
    }

    public String getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(String groupStatus) {
        this.groupStatus = groupStatus;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Integer getPostCount() {
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }

    public ArrayList<Button> getButtons() {
        return buttons;
    }

    public void setButtons(ArrayList<Button> buttons) {
        this.buttons = buttons;
    }

    public ArrayList<Button> getRequestButtons() {
        return requestButtons;
    }

    public void setRequestButtons(ArrayList<Button> requestButtons) {
        this.requestButtons = requestButtons;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public ArrayList<Object> getGroupInvitedMembers() {
        return groupInvitedMembers;
    }

    public void setGroupInvitedMembers(ArrayList<Object> groupInvitedMembers) {
        this.groupInvitedMembers = groupInvitedMembers;
    }

    public ArrayList<GroupMember> getGroupMembersDetails() {
        return groupMembersDetails;
    }

    public void setGroupMembersDetails(ArrayList<GroupMember> groupMembersDetails) {
        this.groupMembersDetails = groupMembersDetails;
    }

    public Boolean getJoined() {
        return isJoined;
    }

    public void setJoined(Boolean joined) {
        isJoined = joined;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("groupTitle", groupTitle).append("groupCategory", groupCategory).append("_private", _private).append("groupDescription", groupDescription).append("createdBy", createdBy).append("groupAdmins", groupAdmins).append("groupMembers", groupMembers).append("groupImage", groupImage).append("groupCoverImage", groupCoverImage).append("groupStatus", groupStatus).append("createdDate", createdDate).append("groupId", groupId).append("buttons", buttons).append("updatedDate", updatedDate).append("groupInvitedMembers", groupInvitedMembers).toString();
    }

}
