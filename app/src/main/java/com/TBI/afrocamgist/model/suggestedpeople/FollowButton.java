
package com.TBI.afrocamgist.model.suggestedpeople;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowButton implements Serializable
{

    @SerializedName("button_link")
    @Expose
    private String buttonLink;
    @SerializedName("button_text")
    @Expose
    private String buttonText;
    @SerializedName("button_type")
    @Expose
    private String buttonType;
    private final static long serialVersionUID = -2667509163961756395L;

    public String getButtonLink() {
        return buttonLink;
    }

    public void setButtonLink(String buttonLink) {
        this.buttonLink = buttonLink;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getButtonType() {
        return buttonType;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }

}
