package com.TBI.afrocamgist.model.hashtag;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class TrendingHashtagDetails implements Serializable {

    @SerializedName("data")
    @Expose
    private ArrayList<Hashtag> trendingHashtags = null;

    public ArrayList<Hashtag> getTrendingHashtags() {
        return trendingHashtags;
    }

    public void setTrendingHashtags(ArrayList<Hashtag> trendingHashtags) {
        this.trendingHashtags = trendingHashtags;
    }
}
