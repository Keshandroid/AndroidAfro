
package com.TBI.afrocamgist.model.advert;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Budget implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("budget")
    @Expose
    private String budget;

    @SerializedName("budget_id")
    @Expose
    private String budget_id;

    @SerializedName("duration")
    @Expose
    private String duration;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getBudget_id() {
        return budget_id;
    }

    public void setBudget_id(String budget_id) {
        this.budget_id = budget_id;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
