
package com.TBI.afrocamgist.model.notification;

import java.io.Serializable;

import com.TBI.afrocamgist.model.user.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification implements Serializable
{

    @SerializedName("user_details")
    @Expose
    private User userDetails;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("client_router_link")
    @Expose
    private String clientRouterLink;
    @SerializedName("raw_details")
    @Expose
    private NotificationRawDetails notificationRawDetails;
    private final static long serialVersionUID = -8993877882555696461L;

    public User getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(User userDetails) {
        this.userDetails = userDetails;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getClientRouterLink() {
        return clientRouterLink;
    }

    public void setClientRouterLink(String clientRouterLink) {
        this.clientRouterLink = clientRouterLink;
    }

    public NotificationRawDetails getNotificationRawDetails() {
        return notificationRawDetails;
    }

    public void setNotificationRawDetails(NotificationRawDetails notificationRawDetails) {
        this.notificationRawDetails = notificationRawDetails;
    }
}
