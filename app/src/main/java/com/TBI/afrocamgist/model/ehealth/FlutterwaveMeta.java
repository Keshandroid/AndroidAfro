package com.TBI.afrocamgist.model.ehealth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FlutterwaveMeta implements Serializable {

    @SerializedName("plan_id")
    @Expose
    private String plan_id;
    @SerializedName("isNewCustomer")
    @Expose
    private Boolean isNewCustomer;

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public Boolean getNewCustomer() {
        return isNewCustomer;
    }

    public void setNewCustomer(Boolean newCustomer) {
        isNewCustomer = newCustomer;
    }
}
