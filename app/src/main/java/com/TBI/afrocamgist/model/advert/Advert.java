package com.TBI.afrocamgist.model.advert;

import com.TBI.afrocamgist.model.post.Post;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Advert implements Serializable{
    //create audience list
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("country")
    @Expose
    private ArrayList<String> country = new ArrayList<>();
    @SerializedName("interests")
    @Expose
    private ArrayList<String> interests = new ArrayList<>();
    @SerializedName("age_range")
    @Expose
    private String age_range;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("minAge")
    @Expose
    private String minAge;
    @SerializedName("maxAge")
    @Expose
    private String maxAge;
    @SerializedName("audience_id")
    @Expose
    private String audience_id;
    @SerializedName("user_id")
    @Expose
    private String user_id;

    //Created Advert List
    @SerializedName("advert_id")
    @Expose
    private String advert_id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("transaction")
    @Expose
    private String transaction;
    @SerializedName("end_timestamp")
    @Expose
    private String end_timestamp;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("post")
    @Expose
    private ArrayList<Post> posts = null;
    @SerializedName("budget")
    @Expose
    private ArrayList<Budget> budget = null;
    @SerializedName("goal")
    @Expose
    private ArrayList<Goal> goal = null;

    @SerializedName("audience")
    @Expose
    private ArrayList<Audience> audience = null;


    public ArrayList<Budget> getBudget() {
        return budget;
    }

    public void setBudget(ArrayList<Budget> budget) {
        this.budget = budget;
    }

    public ArrayList<Goal> getGoal() {
        return goal;
    }

    public void setGoal(ArrayList<Goal> goal) {
        this.goal = goal;
    }

    public ArrayList<Audience> getAudience() {
        return audience;
    }

    public void setAudience(ArrayList<Audience> audience) {
        this.audience = audience;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge_range() {
        return age_range;
    }

    public void setAge_range(String age_range) {
        this.age_range = age_range;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMinAge() {
        return minAge;
    }

    public void setMinAge(String minAge) {
        this.minAge = minAge;
    }

    public String getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(String maxAge) {
        this.maxAge = maxAge;
    }

    public String getAudience_id() {
        return audience_id;
    }

    public void setAudience_id(String audience_id) {
        this.audience_id = audience_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public ArrayList<String> getCountry() {
        return country;
    }

    public void setCountry(ArrayList<String> country) {
        this.country = country;
    }

    public ArrayList<String> getInterests() {
        return interests;
    }

    public void setInterests(ArrayList<String> interests) {
        this.interests = interests;
    }


    public String getAdvert_id() {
        return advert_id;
    }

    public void setAdvert_id(String advert_id) {
        this.advert_id = advert_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getEnd_timestamp() {
        return end_timestamp;
    }

    public void setEnd_timestamp(String end_timestamp) {
        this.end_timestamp = end_timestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }
}
