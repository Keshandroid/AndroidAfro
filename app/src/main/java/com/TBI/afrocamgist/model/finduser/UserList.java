
package com.TBI.afrocamgist.model.finduser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserList implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<UserDetails> users = null;
    private final static long serialVersionUID = 8555230180896759128L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<UserDetails> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserDetails> users) {
        this.users = users;
    }
}
