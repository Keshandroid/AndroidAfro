
package com.TBI.afrocamgist.model.suggestedpeople;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuggestedPeopleDetails implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<SuggestedPeople> suggestedPeopleList = null;
    private final static long serialVersionUID = 7620532500956967209L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<SuggestedPeople> getSuggestedPeopleList() {
        return suggestedPeopleList;
    }

    public void setSuggestedPeopleList(ArrayList<SuggestedPeople> suggestedPeopleList) {
        this.suggestedPeopleList = suggestedPeopleList;
    }

}
