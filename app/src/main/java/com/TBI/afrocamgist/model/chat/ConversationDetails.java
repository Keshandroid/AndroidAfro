
package com.TBI.afrocamgist.model.chat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConversationDetails implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("pages")
    @Expose
    private Integer pages;

    @SerializedName("data")
    @Expose
    private ArrayList<Conversation> conversationList = null;
    private final static long serialVersionUID = 1751519301770627554L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public ArrayList<Conversation> getConversationList() {
        return conversationList;
    }

    public void setConversationList(ArrayList<Conversation> conversationList) {
        this.conversationList = conversationList;
    }
}
