package com.TBI.afrocamgist.model.advertisement;

import com.TBI.afrocamgist.model.story.StoryUserData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Announcement {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("data")
    @Expose
    private ArrayList<AnnouncementData> announcementData = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<AnnouncementData> getAnnouncementData() {
        return announcementData;
    }

    public void setAnnouncementData(ArrayList<AnnouncementData> announcementData) {
        this.announcementData = announcementData;
    }
}
