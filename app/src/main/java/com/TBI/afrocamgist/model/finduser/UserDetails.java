
package com.TBI.afrocamgist.model.finduser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails implements Serializable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("follower_ids")
    @Expose
    private ArrayList<Integer> followerIds = null;
    @SerializedName("blocked_by_me")
    @Expose
    private Boolean blockedByMe;
    @SerializedName("profile_image_url")
    @Expose
    private String profileImageUrl;
    @SerializedName("private")
    @Expose
    private Boolean _private;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("request_buttons")
    @Expose
    private ArrayList<RequestButton> requestButtons = null;
    private final static long serialVersionUID = -7396157166213494847L;

    public Boolean getBlockedByMe() {
        return blockedByMe;
    }

    public void setBlockedByMe(Boolean blockedByMe) {
        this.blockedByMe = blockedByMe;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Integer> getFollowerIds() {
        return followerIds;
    }

    public void setFollowerIds(ArrayList<Integer> followerIds) {
        this.followerIds = followerIds;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public Boolean getPrivate() {
        return _private;
    }

    public void setPrivate(Boolean _private) {
        this._private = _private;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public ArrayList<RequestButton> getRequestButtons() {
        return requestButtons;
    }

    public void setRequestButtons(ArrayList<RequestButton> requestButtons) {
        this.requestButtons = requestButtons;
    }

}
