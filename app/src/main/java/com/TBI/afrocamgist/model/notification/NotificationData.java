
package com.TBI.afrocamgist.model.notification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationData implements Serializable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("notification_details")
    @Expose
    private Notification notificationDetails;
    @SerializedName("notification_type")
    @Expose
    private String notificationType;
    @SerializedName("notify_users")
    @Expose
    private ArrayList<Integer> notifyUsers = null;
    @SerializedName("notification_status")
    @Expose
    private String notificationStatus;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("notification_id")
    @Expose
    private Integer notificationId;
    private final static long serialVersionUID = 1585193297261674076L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Notification getNotificationDetails() {
        return notificationDetails;
    }

    public void setNotificationDetails(Notification notificationDetails) {
        this.notificationDetails = notificationDetails;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public ArrayList<Integer> getNotifyUsers() {
        return notifyUsers;
    }

    public void setNotifyUsers(ArrayList<Integer> notifyUsers) {
        this.notifyUsers = notifyUsers;
    }

    public String getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(String notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

}
