
package com.TBI.afrocamgist.model.follow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Follow implements Serializable
{

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("religion")
    @Expose
    private String religion;
    @SerializedName("follower_ids")
    @Expose
    private ArrayList<Integer> followerIds = null;
    @SerializedName("following_ids")
    @Expose
    private ArrayList<Integer> followingIds = null;
    @SerializedName("profile_image_url")
    @Expose
    private String profileImageUrl;
    @SerializedName("cover_image_url")
    @Expose
    private String coverImageUrl;
    @SerializedName("last_active")
    @Expose
    private String lastActive;
    @SerializedName("career_interest")
    @Expose
    private String careerInterest;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("politics_interest")
    @Expose
    private String politicsInterest;
    @SerializedName("sports_interests")
    @Expose
    private ArrayList<String> sportsInterests = null;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("registered_with")
    @Expose
    private String registeredWith;
    @SerializedName("private")
    @Expose
    private Boolean _private;
    @SerializedName("profile_cover_image")
    @Expose
    private String profileCoverImage;
    @SerializedName("request_buttons")
    @Expose
    private ArrayList<RequestButton> requestButtons = null;
    @SerializedName("password_reset_expiry")
    @Expose
    private String passwordResetExpiry;
    @SerializedName("password_reset_otp")
    @Expose
    private String passwordResetOtp;
    @SerializedName("hidden_posts")
    @Expose
    private ArrayList<Integer> hiddenPosts = null;
    @SerializedName("email_verified")
    @Expose
    private Boolean emailVerified;
    @SerializedName("verification_token")
    @Expose
    private Object verificationToken;
    private final static long serialVersionUID = -8178277314037653959L;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public ArrayList<Integer> getFollowerIds() {
        return followerIds;
    }

    public void setFollowerIds(ArrayList<Integer> followerIds) {
        this.followerIds = followerIds;
    }

    public ArrayList<Integer> getFollowingIds() {
        return followingIds;
    }

    public void setFollowingIds(ArrayList<Integer> followingIds) {
        this.followingIds = followingIds;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getLastActive() {
        return lastActive;
    }

    public void setLastActive(String lastActive) {
        this.lastActive = lastActive;
    }

    public String getCareerInterest() {
        return careerInterest;
    }

    public void setCareerInterest(String careerInterest) {
        this.careerInterest = careerInterest;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPoliticsInterest() {
        return politicsInterest;
    }

    public void setPoliticsInterest(String politicsInterest) {
        this.politicsInterest = politicsInterest;
    }

    public ArrayList<String> getSportsInterests() {
        return sportsInterests;
    }

    public void setSportsInterests(ArrayList<String> sportsInterests) {
        this.sportsInterests = sportsInterests;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getRegisteredWith() {
        return registeredWith;
    }

    public void setRegisteredWith(String registeredWith) {
        this.registeredWith = registeredWith;
    }

    public Boolean getPrivate() {
        return _private;
    }

    public void setPrivate(Boolean _private) {
        this._private = _private;
    }

    public String getProfileCoverImage() {
        return profileCoverImage;
    }

    public void setProfileCoverImage(String profileCoverImage) {
        this.profileCoverImage = profileCoverImage;
    }

    public ArrayList<RequestButton> getRequestButtons() {
        return requestButtons;
    }

    public void setRequestButtons(ArrayList<RequestButton> requestButtons) {
        this.requestButtons = requestButtons;
    }

    public String getPasswordResetExpiry() {
        return passwordResetExpiry;
    }

    public void setPasswordResetExpiry(String passwordResetExpiry) {
        this.passwordResetExpiry = passwordResetExpiry;
    }

    public String getPasswordResetOtp() {
        return passwordResetOtp;
    }

    public void setPasswordResetOtp(String passwordResetOtp) {
        this.passwordResetOtp = passwordResetOtp;
    }

    public ArrayList<Integer> getHiddenPosts() {
        return hiddenPosts;
    }

    public void setHiddenPosts(ArrayList<Integer> hiddenPosts) {
        this.hiddenPosts = hiddenPosts;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public Object getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(Object verificationToken) {
        this.verificationToken = verificationToken;
    }

}
