package com.TBI.afrocamgist.model.story;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StoryDetail {

    @SerializedName("status")
    @Expose
    private Boolean status;


    @SerializedName("data")
    @Expose
    private ArrayList<ArrayList<StoryUserData>> storyUserData = null;
    //private ArrayList<StoryUserData> storyUserData = null;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<ArrayList<StoryUserData>> getStoryUserData() {
        return storyUserData;
    }

    public void setStoryUserData(ArrayList<ArrayList<StoryUserData>> storyUserData) {
        this.storyUserData = storyUserData;
    }
}
