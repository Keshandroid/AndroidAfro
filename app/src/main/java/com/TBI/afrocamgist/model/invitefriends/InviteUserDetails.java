
package com.TBI.afrocamgist.model.invitefriends;

import com.TBI.afrocamgist.model.otheruser.OtherUser;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class InviteUserDetails implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<OtherUser> userList = null;
    private final static long serialVersionUID = -2852885076296363137L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<OtherUser> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<OtherUser> userList) {
        this.userList = userList;
    }
}
