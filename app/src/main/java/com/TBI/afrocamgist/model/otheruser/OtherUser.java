
package com.TBI.afrocamgist.model.otheruser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.rolemodel.RoleModel;
import com.TBI.afrocamgist.model.user.FriendsList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtherUser implements Serializable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_name")
    @Expose
    private String user_name;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("profile_image_url")
    @Expose
    private String profileImageUrl;
    @SerializedName("profile_cover_image")
    @Expose
    private String profileCoverUrl;
    @SerializedName("cover_image_url")
    @Expose
    private String coverImageUrl;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("last_active")
    @Expose
    private String lastActive;
    @SerializedName("last_login_ip")
    @Expose
    private String lastLoginIp;
    @SerializedName("follower_ids")
    @Expose
    private ArrayList<Integer> followerIds = null;
    @SerializedName("following_ids")
    @Expose
    private ArrayList<Integer> followingIds = null;
    @SerializedName("friend_ids")
    @Expose
    private ArrayList<Integer> friendIds = null;
    @SerializedName("career_interest")
    @Expose
    private String careerInterest;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("politics_interest")
    @Expose
    private String politicsInterest;
    @SerializedName("religion")
    @Expose
    private String religion;

    @SerializedName("totalPostLikes")
    @Expose
    private Integer totalPostLikes;
    @SerializedName("profile_title")
    @Expose
    private String profile_title;

    @SerializedName("sports_interests")
    @Expose
    private ArrayList<String> sportsInterests = null;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("password_reset_expiry")
    @Expose
    private String passwordResetExpiry;
    @SerializedName("password_reset_otp")
    @Expose
    private String passwordResetOtp;
    @SerializedName("registered_with")
    @Expose
    private String registeredWith;
    @SerializedName("private")
    @Expose
    private Boolean _private;
    @SerializedName("hidden_posts")
    @Expose
    private ArrayList<Integer> hiddenPosts = null;
    @SerializedName("following_hashtags")
    @Expose
    private ArrayList<String> followingHashtags = null;
    @SerializedName("mutual_friends")
    @Expose
    private ArrayList<Object> mutualFriends = null;
    @SerializedName("is_my_friend")
    @Expose
    private Boolean isMyFriend;
    @SerializedName("blocked_by_me")
    @Expose
    private Boolean blockedByMe;
    @SerializedName("request_buttons")
    @Expose
    private ArrayList<RequestButton> requestButtons = null;
    @SerializedName("friends_list")
    @Expose
    private ArrayList<RoleModel> friendsList = null;
    @SerializedName("followings_list")
    @Expose
    private ArrayList<Follow> followingsList = null;
    @SerializedName("followers_list")
    @Expose
    private ArrayList<Follow> followersList = null;
    @SerializedName("button")
    @Expose
    private RequestButton button = null;
    private final static long serialVersionUID = -7235833313189657655L;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Integer getTotalPostLikes() {
        return totalPostLikes;
    }

    public void setTotalPostLikes(Integer totalPostLikes) {
        this.totalPostLikes = totalPostLikes;
    }

    public String getProfile_title() {
        return profile_title;
    }

    public void setProfile_title(String profile_title) {
        this.profile_title = profile_title;
    }

    public Boolean getBlockedByMe() {
        return blockedByMe;
    }

    public void setBlockedByMe(Boolean blockedByMe) {
        this.blockedByMe = blockedByMe;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getProfileCoverUrl() {
        return profileCoverUrl;
    }

    public void setProfileCoverUrl(String profileCoverUrl) {
        this.profileCoverUrl = profileCoverUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastActive() {
        return lastActive;
    }

    public void setLastActive(String lastActive) {
        this.lastActive = lastActive;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public ArrayList<Integer> getFollowerIds() {
        return followerIds;
    }

    public void setFollowerIds(ArrayList<Integer> followerIds) {
        this.followerIds = followerIds;
    }

    public ArrayList<Integer> getFollowingIds() {
        return followingIds;
    }

    public void setFollowingIds(ArrayList<Integer> followingIds) {
        this.followingIds = followingIds;
    }

    public ArrayList<Integer> getFriendIds() {
        return friendIds;
    }

    public void setFriendIds(ArrayList<Integer> friendIds) {
        this.friendIds = friendIds;
    }

    public String getCareerInterest() {
        return careerInterest;
    }

    public void setCareerInterest(String careerInterest) {
        this.careerInterest = careerInterest;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPoliticsInterest() {
        return politicsInterest;
    }

    public void setPoliticsInterest(String politicsInterest) {
        this.politicsInterest = politicsInterest;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public ArrayList<String> getSportsInterests() {
        return sportsInterests;
    }

    public void setSportsInterests(ArrayList<String> sportsInterests) {
        this.sportsInterests = sportsInterests;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getPasswordResetExpiry() {
        return passwordResetExpiry;
    }

    public void setPasswordResetExpiry(String passwordResetExpiry) {
        this.passwordResetExpiry = passwordResetExpiry;
    }

    public String getPasswordResetOtp() {
        return passwordResetOtp;
    }

    public void setPasswordResetOtp(String passwordResetOtp) {
        this.passwordResetOtp = passwordResetOtp;
    }

    public String getRegisteredWith() {
        return registeredWith;
    }

    public void setRegisteredWith(String registeredWith) {
        this.registeredWith = registeredWith;
    }

    public Boolean getPrivate() {
        return _private;
    }

    public void setPrivate(Boolean _private) {
        this._private = _private;
    }

    public ArrayList<Integer> getHiddenPosts() {
        return hiddenPosts;
    }

    public void setHiddenPosts(ArrayList<Integer> hiddenPosts) {
        this.hiddenPosts = hiddenPosts;
    }

    public ArrayList<String> getFollowingHashtags() {
        return followingHashtags;
    }

    public void setFollowingHashtags(ArrayList<String> followingHashtags) {
        this.followingHashtags = followingHashtags;
    }

    public ArrayList<Object> getMutualFriends() {
        return mutualFriends;
    }

    public void setMutualFriends(ArrayList<Object> mutualFriends) {
        this.mutualFriends = mutualFriends;
    }

    public Boolean getIsMyFriend() {
        return isMyFriend;
    }

    public void setIsMyFriend(Boolean isMyFriend) {
        this.isMyFriend = isMyFriend;
    }

    public ArrayList<RequestButton> getRequestButtons() {
        return requestButtons;
    }

    public void setRequestButtons(ArrayList<RequestButton> requestButtons) {
        this.requestButtons = requestButtons;
    }

    public ArrayList<RoleModel> getFriendsList() {
        return friendsList;
    }

    public void setFriendsList(ArrayList<RoleModel> friendsList) {
        this.friendsList = friendsList;
    }

    public ArrayList<Follow> getFollowingsList() {
        return followingsList;
    }

    public void setFollowingsList(ArrayList<Follow> followingsList) {
        this.followingsList = followingsList;
    }

    public ArrayList<Follow> getFollowersList() {
        return followersList;
    }

    public void setFollowersList(ArrayList<Follow> followersList) {
        this.followersList = followersList;
    }

    public RequestButton getButton() {
        return button;
    }

    public void setButton(RequestButton button) {
        this.button = button;
    }
}
