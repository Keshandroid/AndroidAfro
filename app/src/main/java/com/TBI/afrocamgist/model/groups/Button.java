
package com.TBI.afrocamgist.model.groups;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Button implements Serializable
{

    @SerializedName("button_type")
    @Expose
    private String buttonType;
    @SerializedName("button_link")
    @Expose
    private String buttonLink;
    @SerializedName("button_request_type")
    @Expose
    private String buttonRequestType;
    @SerializedName("button_text")
    @Expose
    private String buttonText;
    private final static long serialVersionUID = 5353095056625816082L;

    public String getButtonType() {
        return buttonType;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }

    public String getButtonLink() {
        return buttonLink;
    }

    public void setButtonLink(String buttonLink) {
        this.buttonLink = buttonLink;
    }

    public String getButtonRequestType() {
        return buttonRequestType;
    }

    public void setButtonRequestType(String buttonRequestType) {
        this.buttonRequestType = buttonRequestType;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("buttonType", buttonType).append("buttonLink", buttonLink).append("buttonRequestType", buttonRequestType).toString();
    }

}
