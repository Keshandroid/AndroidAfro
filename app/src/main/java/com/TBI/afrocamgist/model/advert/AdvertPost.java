
package com.TBI.afrocamgist.model.advert;

import com.TBI.afrocamgist.model.post.Post;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AdvertPost implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<Advert> posts = null;


    private final static long serialVersionUID = 2840523502568794762L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<Advert> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Advert> posts) {
        this.posts = posts;
    }
}
