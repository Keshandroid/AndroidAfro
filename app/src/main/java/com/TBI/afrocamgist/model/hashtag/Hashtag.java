
package com.TBI.afrocamgist.model.hashtag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hashtag implements Serializable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("hashtag_slug")
    @Expose
    private String hashtagSlug;
    @SerializedName("post_count")
    @Expose
    private Integer postCount;
    @SerializedName("followers")
    @Expose
    private ArrayList<Integer> followers = null;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("hashtag_status")
    @Expose
    private String hashtagStatus;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;
    @SerializedName("hashtag_id")
    @Expose
    private Integer hashtagId;
    @SerializedName("followed_by_me")
    @Expose
    private Boolean followedByMe;
    private final static long serialVersionUID = -6102159605737689516L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHashtagSlug() {
        return hashtagSlug;
    }

    public void setHashtagSlug(String hashtagSlug) {
        this.hashtagSlug = hashtagSlug;
    }

    public Integer getPostCount() {
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }

    public ArrayList<Integer> getFollowers() {
        return followers;
    }

    public void setFollowers(ArrayList<Integer> followers) {
        this.followers = followers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHashtagStatus() {
        return hashtagStatus;
    }

    public void setHashtagStatus(String hashtagStatus) {
        this.hashtagStatus = hashtagStatus;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public Integer getHashtagId() {
        return hashtagId;
    }

    public void setHashtagId(Integer hashtagId) {
        this.hashtagId = hashtagId;
    }

    public Boolean getFollowedByMe() {
        return followedByMe;
    }

    public void setFollowedByMe(Boolean followedByMe) {
        this.followedByMe = followedByMe;
    }

}
