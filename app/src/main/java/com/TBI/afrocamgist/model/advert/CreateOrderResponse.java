package com.TBI.afrocamgist.model.advert;

import com.TBI.afrocamgist.model.notification.NotificationData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class CreateOrderResponse implements Serializable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private PayPalOrder payPalOrder;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public PayPalOrder getPayPalOrder() {
        return payPalOrder;
    }

    public void setPayPalOrder(PayPalOrder payPalOrder) {
        this.payPalOrder = payPalOrder;
    }

    public class PayPalOrder{

        @SerializedName("order")
        @Expose
        private OrderDetail orderDetail;

        public OrderDetail getOrderDetail() {
            return orderDetail;
        }

        public void setOrderDetail(OrderDetail orderDetail) {
            this.orderDetail = orderDetail;
        }

        public class OrderDetail{

            @SerializedName("id")
            @Expose
            private int id;
            @SerializedName("status")
            @Expose
            private String status;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

    }

}
