
package com.TBI.afrocamgist.model.photos;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Photo implements Serializable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("upload_date")
    @Expose
    private String uploadDate;
    @SerializedName("photo_status")
    @Expose
    private String photoStatus;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("user_photo_id")
    @Expose
    private Integer userPhotoId;
    private final static long serialVersionUID = 1666520589068785594L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getPhotoStatus() {
        return photoStatus;
    }

    public void setPhotoStatus(String photoStatus) {
        this.photoStatus = photoStatus;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getUserPhotoId() {
        return userPhotoId;
    }

    public void setUserPhotoId(Integer userPhotoId) {
        this.userPhotoId = userPhotoId;
    }

}
