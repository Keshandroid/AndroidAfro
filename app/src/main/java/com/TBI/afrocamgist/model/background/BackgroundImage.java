
package com.TBI.afrocamgist.model.background;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BackgroundImage implements Serializable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("bg_id")
    @Expose
    private Integer bgId;
    @SerializedName("path")
    @Expose
    private String path;
    private final static long serialVersionUID = 9106924710567186968L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getBgId() {
        return bgId;
    }

    public void setBgId(Integer bgId) {
        this.bgId = bgId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
