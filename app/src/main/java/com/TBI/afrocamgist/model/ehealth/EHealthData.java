
package com.TBI.afrocamgist.model.ehealth;

import com.TBI.afrocamgist.model.advert.Advert;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class EHealthData implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<EHealth> eHealth = null;


    private final static long serialVersionUID = 2840523502568794762L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<EHealth> geteHealth() {
        return eHealth;
    }

    public void seteHealth(ArrayList<EHealth> eHealth) {
        this.eHealth = eHealth;
    }
}
