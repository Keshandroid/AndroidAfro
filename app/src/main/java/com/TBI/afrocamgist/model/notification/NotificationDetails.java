
package com.TBI.afrocamgist.model.notification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationDetails implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("nextPage")
    @Expose
    private Integer nextPage;
    @SerializedName("data")
    @Expose
    private ArrayList<NotificationData> notificationList = null;
    private final static long serialVersionUID = -197895843718227344L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getNextPage() {
        return nextPage;
    }

    public void setNextPage(Integer nextPage) {
        this.nextPage = nextPage;
    }

    public ArrayList<NotificationData> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(ArrayList<NotificationData> notificationList) {
        this.notificationList = notificationList;
    }
}
