
package com.TBI.afrocamgist.model.rolemodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.TBI.afrocamgist.model.finduser.RequestButton;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoleModel implements Serializable
{

    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("registered_with")
    @Expose
    private String registeredWith;
    @SerializedName("profile_image_url")
    @Expose
    private String profileImageUrl;
    @SerializedName("cover_image_url")
    @Expose
    private String coverImageUrl;
    @SerializedName("private")
    @Expose
    private String _private;
    @SerializedName("last_active")
    @Expose
    private String lastActive;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("email_verified")
    @Expose
    private Boolean emailVerified;
    @SerializedName("verification_token")
    @Expose
    private Object verificationToken;
    @SerializedName("introduced")
    @Expose
    private Boolean introduced;
    @SerializedName("last_login_ip")
    @Expose
    private String lastLoginIp;
    @SerializedName("career_interest")
    @Expose
    private String careerInterest;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("religion")
    @Expose
    private String religion;
    @SerializedName("sports_interests")
    @Expose
    private List<String> sportsInterests = null;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("follower_ids")
    @Expose
    private List<Integer> followerIds = null;
    @SerializedName("following_ids")
    @Expose
    private List<Integer> followingIds = null;
    @SerializedName("following_hashtags")
    @Expose
    private List<String> followingHashtags = null;
    @SerializedName("hidden_posts")
    @Expose
    private List<Integer> hiddenPosts = null;
    @SerializedName("profile_cover_image")
    @Expose
    private String profileCoverImage;
    @SerializedName("request_buttons")
    @Expose
    private ArrayList<RequestButton> requestButtons = null;
    private final static long serialVersionUID = 4844087612196328204L;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getRegisteredWith() {
        return registeredWith;
    }

    public void setRegisteredWith(String registeredWith) {
        this.registeredWith = registeredWith;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getPrivate() {
        return _private;
    }

    public void setPrivate(String _private) {
        this._private = _private;
    }

    public String getLastActive() {
        return lastActive;
    }

    public void setLastActive(String lastActive) {
        this.lastActive = lastActive;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public Object getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(Object verificationToken) {
        this.verificationToken = verificationToken;
    }

    public Boolean getIntroduced() {
        return introduced;
    }

    public void setIntroduced(Boolean introduced) {
        this.introduced = introduced;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public String getCareerInterest() {
        return careerInterest;
    }

    public void setCareerInterest(String careerInterest) {
        this.careerInterest = careerInterest;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public List<String> getSportsInterests() {
        return sportsInterests;
    }

    public void setSportsInterests(List<String> sportsInterests) {
        this.sportsInterests = sportsInterests;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public List<Integer> getFollowerIds() {
        return followerIds;
    }

    public void setFollowerIds(List<Integer> followerIds) {
        this.followerIds = followerIds;
    }

    public List<Integer> getFollowingIds() {
        return followingIds;
    }

    public void setFollowingIds(List<Integer> followingIds) {
        this.followingIds = followingIds;
    }

    public List<String> getFollowingHashtags() {
        return followingHashtags;
    }

    public void setFollowingHashtags(List<String> followingHashtags) {
        this.followingHashtags = followingHashtags;
    }

    public List<Integer> getHiddenPosts() {
        return hiddenPosts;
    }

    public void setHiddenPosts(List<Integer> hiddenPosts) {
        this.hiddenPosts = hiddenPosts;
    }

    public String getProfileCoverImage() {
        return profileCoverImage;
    }

    public void setProfileCoverImage(String profileCoverImage) {
        this.profileCoverImage = profileCoverImage;
    }

    public ArrayList<RequestButton> getRequestButtons() {
        return requestButtons;
    }

    public void setRequestButtons(ArrayList<RequestButton> requestButtons) {
        this.requestButtons = requestButtons;
    }

}
