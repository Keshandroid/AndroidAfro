package com.TBI.afrocamgist.model.ehealth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Testimonials implements Serializable{
    //create audience list
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("testimonial_id")
    @Expose
    private String testimonial_id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("testimonial_video")
    @Expose
    private String testimonial_video;
    @SerializedName("thumbnails")
    @Expose
    private ArrayList<String> thumbnails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTestimonial_id() {
        return testimonial_id;
    }

    public void setTestimonial_id(String testimonial_id) {
        this.testimonial_id = testimonial_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTestimonial_video() {
        return testimonial_video;
    }

    public void setTestimonial_video(String testimonial_video) {
        this.testimonial_video = testimonial_video;
    }

    public ArrayList<String> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(ArrayList<String> thumbnails) {
        this.thumbnails = thumbnails;
    }
}
