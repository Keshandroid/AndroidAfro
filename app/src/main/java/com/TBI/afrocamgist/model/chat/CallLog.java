package com.TBI.afrocamgist.model.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CallLog implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("caller")
    @Expose
    private Integer callerID;

    @SerializedName("receiver")
    @Expose
    private Integer receiver;

    @SerializedName("call_status")
    @Expose
    private String call_status;

    @SerializedName("call_duration")
    @Expose
    private String call_duration;

    @SerializedName("call_time")
    @Expose
    private String call_time;

    @SerializedName("call_id")
    @Expose
    private Integer call_id;

    @SerializedName("call_type")
    @Expose
    private String call_type;

    @SerializedName("call")
    @Expose
    private String mCall;

    @SerializedName("callerUser")
    @Expose
    private CallUser callUser = null;

    @SerializedName("receiverUser")
    @Expose
    private ReceiverUser receiverUser = null;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Integer getCallerID() {
        return callerID;
    }

    public void setCallerID(Integer callerID) {
        this.callerID = callerID;
    }

    public Integer getReceiver() {
        return receiver;
    }

    public void setReceiver(Integer receiver) {
        this.receiver = receiver;
    }

    public String getCall_status() {
        return call_status;
    }

    public void setCall_status(String call_status) {
        this.call_status = call_status;
    }

    public String getCall_duration() {
        return call_duration;
    }

    public void setCall_duration(String call_duration) {
        this.call_duration = call_duration;
    }

    public String getCall_time() {
        return call_time;
    }

    public void setCall_time(String call_time) {
        this.call_time = call_time;
    }

    public Integer getCall_id() {
        return call_id;
    }

    public void setCall_id(Integer call_id) {
        this.call_id = call_id;
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public String getmCall() {
        return mCall;
    }

    public void setmCall(String mCall) {
        this.mCall = mCall;
    }

    public CallUser getCallUser() {
        return callUser;
    }

    public void setCallUser(CallUser callUser) {
        this.callUser = callUser;
    }

    public ReceiverUser getReceiverUser() {
        return receiverUser;
    }

    public void setReceiverUser(ReceiverUser receiverUser) {
        this.receiverUser = receiverUser;
    }
}
