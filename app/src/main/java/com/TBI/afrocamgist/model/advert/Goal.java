
package com.TBI.afrocamgist.model.advert;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Goal implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("goal_id")
    @Expose
    private String goal_id;

    @SerializedName("goal_type")
    @Expose
    private String goal_type;

    @SerializedName("post_id")
    @Expose
    private String post_id;

    @SerializedName("url")
    @Expose
    private String url;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoal_id() {
        return goal_id;
    }

    public void setGoal_id(String goal_id) {
        this.goal_id = goal_id;
    }

    public String getGoal_type() {
        return goal_type;
    }

    public void setGoal_type(String goal_type) {
        this.goal_type = goal_type;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
