
package com.TBI.afrocamgist.model.suggestedpeople;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuggestedPeople implements Serializable
{

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("profile_image_url")
    @Expose
    private String profileImageUrl;
    @SerializedName("follow_button")
    @Expose
    private FollowButton followButton;
    private final static long serialVersionUID = -1816387328575402830L;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public FollowButton getFollowButton() {
        return followButton;
    }

    public void setFollowButton(FollowButton followButton) {
        this.followButton = followButton;
    }

}
