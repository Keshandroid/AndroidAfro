
package com.TBI.afrocamgist.model.background;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BackgroundImageDetails implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<BackgroundImage> backgroundImages = null;
    private final static long serialVersionUID = 6464416012679007383L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<BackgroundImage> getBackgroundImages() {
        return backgroundImages;
    }

    public void setBackgroundImages(ArrayList<BackgroundImage> backgroundImages) {
        this.backgroundImages = backgroundImages;
    }
}
