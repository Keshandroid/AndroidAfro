
package com.TBI.afrocamgist.model.follow;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowDetails implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ArrayList<Follow> followList = null;
    private final static long serialVersionUID = -2852885076296363137L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<Follow> getFollowList() {
        return followList;
    }

    public void setFollowList(ArrayList<Follow> followList) {
        this.followList = followList;
    }

}
