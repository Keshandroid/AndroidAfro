
package com.TBI.afrocamgist.model.ehealth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class TestimonialsData implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("count")
    @Expose
    private String count;
    @SerializedName("data")
    @Expose
    private ArrayList<Testimonials> testimonials = null;


    private final static long serialVersionUID = 2840523502568794762L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public ArrayList<Testimonials> getTestimonials() {
        return testimonials;
    }

    public void setTestimonials(ArrayList<Testimonials> testimonials) {
        this.testimonials = testimonials;
    }
}
