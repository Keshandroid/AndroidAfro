package com.TBI.afrocamgist.model.notification;

import com.TBI.afrocamgist.model.chat.Message;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class NotificationCounter implements Serializable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private NotificationData notificationData;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public NotificationData getNotificationData() {
        return notificationData;
    }

    public void setNotificationData(NotificationData notificationData) {
        this.notificationData = notificationData;
    }

    public class NotificationData{
        @SerializedName("notification_count")
        @Expose
        private int notification_count;
        @SerializedName("navCounters")
        @Expose
        private NavCounters navCounters;

        public int getNotification_count() {
            return notification_count;
        }

        public void setNotification_count(int notification_count) {
            this.notification_count = notification_count;
        }

        public NavCounters getNavCounters() {
            return navCounters;
        }

        public void setNavCounters(NavCounters navCounters) {
            this.navCounters = navCounters;
        }

        public class NavCounters{
            @SerializedName("messages")
            @Expose
            private int messages;

            public int getMessages() {
                return messages;
            }

            public void setMessages(int messages) {
                this.messages = messages;
            }
        }

    }

}
