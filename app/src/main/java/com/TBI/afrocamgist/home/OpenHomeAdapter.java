package com.TBI.afrocamgist.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.HashtagPostsActivity;
import com.TBI.afrocamgist.activity.PostLikedByActivity;
import com.TBI.afrocamgist.adapters.MenuAdapter;
import com.TBI.afrocamgist.app.AfrocamgistApplication;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.advertisement.Advertisement;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.post.Post;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;

import static com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter.isMute;

public class OpenHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements MenuAdapter.OnMenuItemClickListener {

    private ArrayList<Post> swaggerPosts;
    private ArrayList<Advertisement> advertisements;
    private Activity context;
    public OnSwaggerPostClickListener listener;
    private static final int POST = 0, ADVERTISEMENT = 1;
    public static int LAST_ADVERTISEMENT_POSITION = 0;
    public static int CURRENT_ADVERTISEMENT = 0;

    public SimpleExoPlayer helper;
    private PopupWindow popup;

    private HashMap<Integer,VideoViewHolder> viewHolderList = new HashMap<>();

    public OpenHomeAdapter(Activity context, ArrayList<Post> swaggerPosts, OnSwaggerPostClickListener listener) {
        this.context = context;
        this.swaggerPosts = swaggerPosts;
        this.listener = listener;
        viewHolderList.clear();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        /*return new VideoViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_container_swagger,
                        parent,
                        false
                )
        );*/

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;

        switch (viewType) {

            case ADVERTISEMENT:
                view = inflater.inflate(R.layout.item_advertisement_new, parent, false);

                AdvertisementHolder advertisementHolder = new AdvertisementHolder(view);
                advertisementHolder.itemView.setTag("advertisement");

                return advertisementHolder;
            default:
                view = inflater.inflate(R.layout.item_container_new_popular_post, parent, false);
                VideoViewHolder videoViewHolder = new VideoViewHolder(view);
                videoViewHolder.itemView.setTag("normal");
                return videoViewHolder;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case ADVERTISEMENT:
                showAdvertisement((OpenHomeAdapter.AdvertisementHolder) holder);
                break;
            default:
                showPosts((OpenHomeAdapter.VideoViewHolder) holder);
                break;
        }

    }

    public void showPosts(VideoViewHolder holder){
        //Log.e(VideosAdapter.class.getSimpleName(),"onBindViewHodler Invoked");

        //viewHolderList.put(holder.getAdapterPosition(),holder);

        /*Post post = swaggerPosts.get(position);

         if(post!=null){
            if(listener!=null){
                listener.onPauseVideo(post,holder.simpleExoPlayer);
            }
            holder.showPostData(post,context);
            holder.setClickListener(post,context);

            if(post.getPostType().equalsIgnoreCase("shared")){
                if(post.getSharedPost()!=null){
                    Log.d("isSharedPost","yes...");
                    holder.setPostData(post.getSharedPost(),context);
                }
            }else {
                Log.d("isSharedPost","No...");
                holder.setPostData(post,context);
            }
        }*/



        Post post = swaggerPosts.get(holder.getAdapterPosition());

        if(post != null){
            /*if(listener!=null){
                if(post.getPostType().equalsIgnoreCase("shared")){
                    listener.onPauseVideo(post.getSharedPost(),holder.helper);
                }else {
                    listener.onPauseVideo(post,holder.helper);
                }
            }*/

            holder.pause();

            if(post.getPostType().equalsIgnoreCase("shared")){
                if(post.getSharedPost()!=null){
                    Log.d("isSharedPost","yes...");
                    holder.share.setVisibility(View.GONE);
                    listener.onPostView(post.getPostId()); //call post view api
                    listener.onPostViewCount(post.getPostId()); // call post view count api
                    holder.setPostData(post.getSharedPost(),context,listener);
                }
            }else {
                Log.d("isSharedPost","No...");
                //holder.share.setVisibility(View.VISIBLE); // not accessible for non-registerd user
                listener.onPostView(post.getPostId()); //call post view api
                listener.onPostViewCount(post.getPostId()); // call post view count api
                holder.setPostData(post,context,listener);
            }

            holder.showPostData(post,context);
            holder.setClickListener(post,context,listener,holder.getAdapterPosition(),holder);
        }

    }

    /*public void loadItemData(int position) {
        if(position-1 >=0)
        {
            if(viewHolderList.containsKey(position-1))
            {
                VideoViewHolder holder = viewHolderList.get(position-1);
                holder.storyImage.setImageResource(0);
                holder.text_with_image_background.setVisibility(View.GONE);


                if(holder.playerView.getPlayer()!=null){
                    Log.d("scroll99","upscroll....");
                    holder.simpleExoPlayer.setPlayWhenReady(false);
                    holder.simpleExoPlayer.seekTo(0);
                    holder.simpleExoPlayer.stop();
                }

                holder.playerView.setPlayer(null);

            }
        }
        if(position+1 < swaggerPosts.size())
        {
            if(viewHolderList.containsKey(position+1))
            {
                VideoViewHolder holder = viewHolderList.get(position+1);
                holder.storyImage.setImageResource(0);
                holder.text_with_image_background.setVisibility(View.GONE);

                if(holder.playerView.getPlayer()!=null){
                    Log.d("scroll99","bottomscroll....");
                    holder.simpleExoPlayer.setPlayWhenReady(false);
                    holder.simpleExoPlayer.seekTo(0);
                    holder.simpleExoPlayer.stop();
                }

                holder.playerView.setPlayer(null);
            }
        }


        if(!viewHolderList.containsKey(position)) {
            Log.e(VideosAdapter.class.getSimpleName(),"Position Not Found : "+position);
            return;
        }
        VideoViewHolder holder = viewHolderList.get(position);

        Post post = swaggerPosts.get(position);

        if(holder != null && post!=null){
            if(listener!=null){
                if(post.getPostType().equalsIgnoreCase("shared")){
                    listener.onPauseVideo(post.getSharedPost(),holder.simpleExoPlayer);
                }else {
                    listener.onPauseVideo(post,holder.simpleExoPlayer);
                }
            }


            if(post.getPostType().equalsIgnoreCase("shared")){
                if(post.getSharedPost()!=null){
                    Log.d("isSharedPost","yes...");
                    holder.share.setVisibility(View.GONE);
                    holder.setPostData(post.getSharedPost(),context,listener);
                }
            }else {
                Log.d("isSharedPost","No...");
                holder.share.setVisibility(View.VISIBLE);
                holder.setPostData(post,context,listener);
            }

            holder.showPostData(post,context);
            holder.setClickListener(post,context,listener,position);
        }
    }*/

    public void openDetailActivity(RecyclerView.ViewHolder viewHolder)
    {
        if(listener!=null){
            listener.openDetailScreen(viewHolder);
        }
    }

    @Override
    public int getItemCount() {
        return swaggerPosts.size();
    }

    @Override
    public int getItemViewType(int position) {
        /*if ((position - 1) > 0 && ((position - 1) % 7 == 0)) {
            if (swaggerPosts.get(position).getFirstName() != null)
                swaggerPosts.add(position, new Post());
            return ADVERTISEMENT;
        }else {
            return POST;
        }*/
        return POST;
    }

    public void setAdvertisementList(ArrayList<Advertisement> advertisements) {

        if (advertisements.size() != 0) {
            this.advertisements = new ArrayList<>();
            this.advertisements.addAll(advertisements);
            notifyDataSetChanged();
        }
    }

    private void showAdvertisement(OpenHomeAdapter.AdvertisementHolder holder) {

        if (advertisements.size() > 0) {
            if (LAST_ADVERTISEMENT_POSITION != holder.getAdapterPosition()) {

                if (advertisements != null && advertisements.size() > 0) {

                    Advertisement advertisement = advertisements.get(CURRENT_ADVERTISEMENT);

                    holder.companyName.setText(advertisement.getCompanyName());

                    Glide.with(holder.adImage)
                            .load(UrlEndpoints.MEDIA_BASE_URL + advertisement.getAdImage())
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                            .into(holder.adImage);

                    holder.content.setText(advertisement.getContent());

                    CURRENT_ADVERTISEMENT++;
                    LAST_ADVERTISEMENT_POSITION = holder.getAdapterPosition();

                    if (advertisements != null && CURRENT_ADVERTISEMENT == (advertisements.size()))
                        CURRENT_ADVERTISEMENT = 0;
                }
            }
        }
    }

    private void showMenu(View menu, int position) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int popupWidth = 450;
        int popupHeight = LinearLayout.LayoutParams.WRAP_CONTENT;

        LinearLayout viewGroup = context.findViewById(R.id.menu_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.drop_down_menu, viewGroup);
        popupMenu(layout, position);

        popup = new PopupWindow(layout, popupWidth, popupHeight);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        if (position == (swaggerPosts.size() - 1))
            popup.showAsDropDown(menu, 0, -300, Gravity.TOP);
        else
            popup.showAsDropDown(menu);
    }

    private void popupMenu(View layout, int position) {

        RecyclerView menu = layout.findViewById(R.id.menuList);
        menu.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        menu.setLayoutManager(mLayoutManager);
        MenuAdapter adapter;
        if (!LocalStorage.getUserDetails().getUserId().equals(swaggerPosts.get(position).getUserId())) {
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_other_menu))), position, this);
        } else {
            if (swaggerPosts.get(position).getMapPost())
                adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_map_menu))), position, this);
            else
                adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_menu))), position, this);
        }
        menu.setAdapter(adapter);
    }

    @Override
    public void onMenuItemClicked(String item, int position) {
        if (popup != null && popup.isShowing())
            popup.dismiss();

        switch (item.toLowerCase()) {

            case "edit":
                listener.onEditPost(swaggerPosts.get(position), position);
                break;
            case "delete":
                listener.onDeletePost(swaggerPosts.get(position));
                break;
            case "hide":
                listener.onHidePost(swaggerPosts.get(position));
                break;
            default:
                break;
        }
    }

    static class AdvertisementHolder extends RecyclerView.ViewHolder {
        TextView companyName, content;
        ImageView adImage;

        AdvertisementHolder(View itemView) {
            super(itemView);

            companyName = itemView.findViewById(R.id.companyName);
            content = itemView.findViewById(R.id.content);
            adImage = itemView.findViewById(R.id.ad_image);

        }

    }

    public class VideoViewHolder extends RecyclerView.ViewHolder implements ToroPlayer {

        //Precache videos
        CacheDataSourceFactory cacheDataSourceFactory = null;
        SimpleCache simpleCache = null;

        private RelativeLayout videoLayout;
        private FrameLayout thumbnail_layout;
        private ImageView storyImage, share,socialShare,report,background_image,imgAddPost, imgEdit,backgroundThumbnail;
        private PlayerView playerView;
        //ExoPlayerViewHelper helper;

        Uri mediaUri;
        ToggleButton volume;
        private AutoLinkTextView postText, txtStory;
        private TextView txtUserName,likesCount,comment,follow,txtShared,more,less;
        private CircleImageView profile_picture;
        private ToggleButton like;
        private RelativeLayout relSimpleText,relProfile,text_with_image_background;
        private TextView post_text_on_image,videoCount;
        private String postType;
        private RelativeLayout mainItemLayout;

        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);

            storyImage = itemView.findViewById(R.id.storyImage);
            playerView = itemView.findViewById(R.id.player);
            txtUserName = itemView.findViewById(R.id.txtUserName);
            profile_picture = itemView.findViewById(R.id.profile_picture);
            like = itemView.findViewById(R.id.like);
            likesCount = itemView.findViewById(R.id.likes_count);
            comment = itemView.findViewById(R.id.comment);
            share = itemView.findViewById(R.id.share);
            socialShare = itemView.findViewById(R.id.social_share);
            report = itemView.findViewById(R.id.report);
            follow = itemView.findViewById(R.id.follow);
            postText = itemView.findViewById(R.id.post_text);
            more = itemView.findViewById(R.id.more);
            less = itemView.findViewById(R.id.less);
            relProfile = itemView.findViewById(R.id.relProfile);
            relSimpleText = itemView.findViewById(R.id.relSimpleText);
            txtStory = itemView.findViewById(R.id.txtStory);
            text_with_image_background = itemView.findViewById(R.id.text_with_image_background);
            background_image = itemView.findViewById(R.id.background_image);
            post_text_on_image = itemView.findViewById(R.id.post_text_on_image);
            txtShared = itemView.findViewById(R.id.txtShared);
            videoCount = itemView.findViewById(R.id.videoCount);
            imgAddPost = itemView.findViewById(R.id.imgAddPost);
            imgEdit = itemView.findViewById(R.id.imgEdit);
            videoLayout = itemView.findViewById(R.id.video_layout);
            backgroundThumbnail = itemView.findViewById(R.id.backgroundThumbnail);
            mainItemLayout = itemView.findViewById(R.id.mainItemLayout);
            thumbnail_layout = itemView.findViewById(R.id.thumbnail_layout);
            volume = itemView.findViewById(R.id.volume);
            volume.setOnCheckedChangeListener((buttonView, isChecked) -> {
                // For Mute and Unmute
                /*if (isChecked)
                    unMute();
                else
                    mute();*/
            });

            /*simpleExoPlayer = newSimpleExoPlayer(itemView.getContext());
            simpleCache = AfrocamgistApplication.simpleCache;
            String userAgent = Util.getUserAgent(itemView.getContext(), "Afrocamgist");
            cacheDataSourceFactory =
                    new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(userAgent),CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);*/


            simpleCache = AfrocamgistApplication.simpleCache;
            String userAgent = Util.getUserAgent(itemView.getContext(), "Afrocamgist");
            cacheDataSourceFactory =
                    new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(userAgent),CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

        }

        @NonNull
        @Override
        public View getPlayerView() {
            return playerView;
        }

        @NonNull
        @Override
        public PlaybackInfo getCurrentPlaybackInfo() {
            return new PlaybackInfo();
        }

        @Override
        public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {
            thumbnail_layout.setVisibility(View.VISIBLE);
            String type="";
            if((videoLayout.getTag()!=null)){
                type=(String)videoLayout.getTag();
            }
            if(type.equals("video")|| type.equals("")) {
                if (helper == null) {
                    Log.d("initialize9","intialize...");
                    helper = newSimpleExoPlayer(context);
                    //helper = new ExoPlayerViewHelper(this, mediaUri);
                }
                //helper.initialize(container, playbackInfo);
            }
        }

        @Override
        public void play() {
            if (helper != null) {
                Log.e("LLLLLL_Play: ", String.valueOf(true));



                MediaSource mediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaUri);
                playerView.setPlayer(helper);
                //Turn On/Off auto play videos
                if (Identity.getAutoPlayVideo(context)){
                    helper.setPlayWhenReady(true);
                }else {
                    helper.setPlayWhenReady(false);
                }
                helper.seekTo(0);
                helper.prepare(mediaSource,true,false);

                listener.onPlayItem(getAdapterPosition());

                if(playerView.getPlayer()!=null)
                    playerView.getPlayer().setRepeatMode(Player.REPEAT_MODE_ONE);


                playerView.getPlayer().addListener(new Player.DefaultEventListener() {
                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                        super.onPlayerStateChanged(playWhenReady, playbackState);
                        if (playWhenReady && playbackState == Player.STATE_READY) {
                            // media actually playing
                            thumbnail_layout.setVisibility(View.GONE);
                        } else if (playWhenReady) {
                            // might be idle (plays after prepare()),
                            // buffering (plays when data available)
                            // or ended (plays when seek away from end)
                        } else {
                            // simpleExoPlayer paused in any state
                            thumbnail_layout.setVisibility(View.VISIBLE);
                        }

                        if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED || !playWhenReady) {
                            playerView.setKeepScreenOn(false);
                        } else { // STATE_READY, STATE_BUFFERING
                            // This prevents the screen from getting dim/lock
                            playerView.setKeepScreenOn(true);
                        }

                    }
                });



                // For Mute and Unmute
                /*if (isMute) {
                    mute();
                    volume.setChecked(false);
                } else {
                    unMute();
                    volume.setChecked(true);
                }*/

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if(Identity.getAutoPlayVideo(context)){
                            thumbnail_layout.setVisibility(View.GONE);
                        }else {
                            thumbnail_layout.setVisibility(View.VISIBLE);
                        }


                    }
                }, 500);
            } else {
                Log.e("LLLLLL_Play: ", String.valueOf(false));
            }
        }

        @Override
        public void pause() {
            //if (helper != null) helper.pause();
            if(helper!=null){
                helper.setPlayWhenReady(false);
            }
        }

        @Override
        public boolean isPlaying() {
            return helper != null && helper.getPlayWhenReady();
        }

        @Override
        public void release() {
            Log.d("RELEASE9","printed...");
            if (helper != null) {
                Log.d("RELEASE9","called...");
                thumbnail_layout.setVisibility(View.VISIBLE);
                helper.release();
                helper = null;
            }
        }

        @Override
        public boolean wantsToPlay() {
            return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85;
        }

        @Override
        public int getPlayerOrder() {
            return getAdapterPosition();
        }

        @SuppressLint("ClickableViewAccessibility")
        void bind(Uri media) {
            this.mediaUri = media;

            // For Mute and Unmute
           /* this.volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        unMute();
                    else
                        mute();
                }
            });*/
        }

        private void mute() {
            isMute = true;
            this.setVolume(0);
        }

        private void unMute() {
            isMute = false;
            this.setVolume(100);
        }

        private void setVolume(int amount) {
            final int max = 100;
            final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
            final float volume = (float) (1 - (numerator / Math.log(max)));
            if (helper != null) {
                helper.setVolume(volume);
            }
        }

        /*void setVideoData(Post videoItem){
            videoView.setVideoPath(UrlEndpoints.MEDIA_BASE_URL + videoItem.getPostVideo());
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    //videoProgressBar.setVisibility(View.GONE);
                    mp.start();

                    float videoRatio = mp.getVideoWidth() / (float) mp.getVideoHeight();
                    float screenRation = videoView.getWidth() / (float) videoView.getHeight();

                    float scale = videoRatio / screenRation;
                    if(scale >= 1f){
                        videoView.setScaleX(scale);
                    }else {
                        videoView.setScaleY(1f/scale);
                    }
                }
            });

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.start();
                }
            });
        }*/

        void setPostData(Post post,Activity context, OnSwaggerPostClickListener listener){

            if(post!=null){


                if(post.getPostType().equalsIgnoreCase("image")){
                    videoLayout.setTag(post.getPostType());
                    if(post.getMapPost()){
                        postType = "MAP";
                        storyImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    }else {
                        postType = "IMAGE";
                        storyImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }

                    viewVisibility("image");
                    if(post.getPostText()!=null && !post.getPostText().equalsIgnoreCase("")){
                        postText.setVisibility(View.VISIBLE);
                    }else {
                        postText.setVisibility(View.GONE);
                    }

                    /*Picasso.get()
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .into(storyImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                    //important
                                *//*try {
                                    momentz.resume();
                                }catch (IndexOutOfBoundsException e){
                                    pausePlayer();
                                    finish();
                                }*//*

                                }

                                @Override
                                public void onError(Exception ex) {
                                    //do smth when there is picture loading error
                                    Toast.makeText(context,ex.getLocalizedMessage()+"",Toast.LENGTH_LONG).show();
                                }
                            });*/



                    //Glide
                    /*Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .into(storyImage);*/


                    storyImage.setImageDrawable(null);

                    Glide.with(storyImage)
                            .asBitmap()
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0) + "?width=50&height=50")
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    storyImage.setImageBitmap(resource);

                                    Glide.with(storyImage)
                                            .asBitmap()
                                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                            .into(new SimpleTarget<Bitmap>() {
                                                @Override
                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                    storyImage.setImageBitmap(resource);
                                                }
                                            });
                                }
                            });


                }else if(post.getPostType().equalsIgnoreCase("video")){
                    videoLayout.setTag(post.getPostType());
                    postType = "VIDEO";
                    viewVisibility("video");

                    if(post.getPostText()!=null && !post.getPostText().equalsIgnoreCase("")){
                        postText.setVisibility(View.VISIBLE);
                    }else {
                        postText.setVisibility(View.GONE);
                    }

                    //set background thumbnail
                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail())
                            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                            .into(backgroundThumbnail);


                    bind(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo()) );
                    //initPlayer(post,context); //Play video from catch memory
                    listener.onVideoCount(post.getPostId());
                }else if(post.getPostType().equalsIgnoreCase("text") && !post.getBackgroundImagePost()){
                    videoLayout.setTag(post.getPostType());
                    postType = "TEXT";
                    viewVisibility("text");
                    //txtStory.setText(post.getPostText());

                    txtStory.addAutoLinkMode(AutoLinkMode.MODE_HASHTAG,AutoLinkMode.MODE_URL);
                    txtStory.setHashtagModeColor(ContextCompat.getColor(context, R.color.hashtag));
                    txtStory.setUrlModeColor(ContextCompat.getColor(context, R.color.colorAccent));
                    txtStory.setAutoLinkText(post.getPostText());

                    txtStory.setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
                        @Override
                        public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {
                            if(autoLinkMode == AutoLinkMode.MODE_HASHTAG){
                                Hashtag hashtag = new Hashtag();
                                if(matchedText.startsWith("#")){
                                    matchedText =matchedText.substring(1);
                                }
                                hashtag.setHashtagSlug(matchedText);
                                Log.d("hashtag1",""+hashtag.getHashtagSlug());
                                context.startActivity(new Intent(context, HashtagPostsActivity.class).putExtra("hashtag",hashtag));
                            }else if(autoLinkMode == AutoLinkMode.MODE_URL){
                                try{
                                    Log.d("linkUrl",""+matchedText);

                                    Uri uri = Uri.parse(matchedText);
                                    Intent httpIntent = new Intent(Intent.ACTION_VIEW,uri);
                                    httpIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getBaseContext().startActivity(httpIntent);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                            }
                        }
                    });

                }else if(post.getPostType().equalsIgnoreCase("text") && post.getBackgroundImagePost()){
                    videoLayout.setTag(post.getPostType());
                    postType = "BACKGROUND_IMG";
                    viewVisibility("background_post");
                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getBackgroundImage())
                            .into(background_image);
                    post_text_on_image.setText(post.getPostText());
                }
            }
        }

        void showPostData(Post post, Activity context){

            if(post.getPostType().equalsIgnoreCase("shared")){
                txtShared.setVisibility(View.VISIBLE);
            }else {
                txtShared.setVisibility(View.GONE);
            }

            if(post.getUser_name()!=null && !post.getUser_name().isEmpty()){
                txtUserName.setText(post.getUser_name());
            }else {
                String fullName = post.getFirstName() + " " + post.getLastName();
                txtUserName.setText(fullName);
            }
            Glide.with(context)
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                    .into(profile_picture);

            if(post.getPostType().equalsIgnoreCase("shared")){
                if(post.getSharedPost()!=null){
                    if(post.getSharedPost().getVideo_play_count()!=null){
                        Log.d("name_count",post.getFirstName()+"==="+post.getVideo_play_count());
                        videoCount.setVisibility(View.VISIBLE);
                        if(post.getSharedPost().getVideo_play_count()==1){
                            videoCount.setText(post.getSharedPost().getVideo_play_count()+" "+"view");
                        }else {
                            videoCount.setText(post.getSharedPost().getVideo_play_count()+" "+"views");
                        }
                    }else {
                        videoCount.setVisibility(View.GONE);
                    }
                }
            }else {
                if(post.getVideo_play_count()!=null){
                    videoCount.setVisibility(View.VISIBLE);
                    Log.d("name_count",post.getFirstName()+"==="+post.getVideo_play_count());
                    if(post.getVideo_play_count()==1){
                        videoCount.setText(post.getVideo_play_count()+" "+"view");
                    }else {
                        videoCount.setText(post.getVideo_play_count()+" "+"views");
                    }
                }else {
                    videoCount.setVisibility(View.GONE);
                }
            }

            if (post.getLiked()) {
                like.setChecked(true);
                likesCount.setTextColor(Color.parseColor("#FF7400"));
            } else {
                like.setChecked(false);
                likesCount.setTextColor(Color.parseColor("#ffffff"));
            }

            String likeCount = post.getLikeCount() + "";
            likesCount.setText(likeCount);
            String commentCount = post.getCommentCount() + "";
            comment.setText(commentCount);

            if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
                follow.setVisibility(View.GONE);
            } else {
                //follow.setVisibility(View.VISIBLE); // not accessible for non-registered users
                if (post.getFollowing() != null) {
                    if (post.getFollowing()) {
                        follow.setTextColor(context.getResources().getColor(R.color.orange));
                        follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        follow.setText(context.getResources().getString(R.string.following));
                        follow.setEnabled(false);
                    } else {
                        follow.setTextColor(context.getResources().getColor(R.color.blue));
                        follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                        follow.setText(context.getResources().getString(R.string.follow));
                        follow.setEnabled(true);

                        //(Important)
                        if(checkIsRequested(post)){
                            follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                            follow.setTextColor(context.getResources().getColor(R.color.colorAccent));
                            follow.setText(R.string.requested);
                            follow.setEnabled(false);
                        }

                    }
                } else {
                    follow.setTextColor(context.getResources().getColor(R.color.blue));
                    follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                    follow.setText(context.getResources().getString(R.string.follow));
                    follow.setEnabled(true);
                }
            }

            //postText.setText(post.getPostText());




            postText.addAutoLinkMode(AutoLinkMode.MODE_HASHTAG,AutoLinkMode.MODE_URL);
            postText.setHashtagModeColor(ContextCompat.getColor(context, R.color.hashtag));
            postText.setUrlModeColor(ContextCompat.getColor(context, R.color.colorAccent));
            postText.setAutoLinkText(post.getPostText());
            more.setVisibility(View.GONE);
            less.setVisibility(View.GONE);

            postText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    postText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int linecount = postText.getLineCount();

                    if(linecount > 4 && post.getPostType().equalsIgnoreCase("shared")){
                        if(post.getSharedPost().getPostType().equalsIgnoreCase("image") ||
                        post.getSharedPost().getPostType().equalsIgnoreCase("video")){
                            more.setVisibility(View.VISIBLE);
                            postText.setMaxLines(4);
                            postText.setEllipsize(TextUtils.TruncateAt.END);
                            more.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    postText.setMaxLines(Integer.MAX_VALUE);
                                    more.setVisibility(View.GONE);
                                    less.setVisibility(View.VISIBLE);
                                }
                            });
                            less.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    postText.setMaxLines(4);
                                    more.setVisibility(View.VISIBLE);
                                    less.setVisibility(View.GONE);
                                }
                            });
                        }else {
                            more.setVisibility(View.GONE);
                            less.setVisibility(View.GONE);
                        }
                    }

                    if(linecount > 4 && (post.getPostType().equalsIgnoreCase("video") ||
                            post.getPostType().equalsIgnoreCase("image"))){
                        more.setVisibility(View.VISIBLE);
                        postText.setMaxLines(4);
                        postText.setEllipsize(TextUtils.TruncateAt.END);
                        more.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                postText.setMaxLines(Integer.MAX_VALUE);
                                more.setVisibility(View.GONE);
                                less.setVisibility(View.VISIBLE);
                            }
                        });
                        less.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                postText.setMaxLines(4);
                                more.setVisibility(View.VISIBLE);
                                less.setVisibility(View.GONE);
                            }
                        });
                    }else {
                        more.setVisibility(View.GONE);
                        less.setVisibility(View.GONE);
                    }


                }
            });

            postText.setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
                @Override
                public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {
                    if(autoLinkMode == AutoLinkMode.MODE_HASHTAG){
                        Hashtag hashtag = new Hashtag();
                        if(matchedText.startsWith("#")){
                            matchedText =matchedText.substring(1);
                        }
                        hashtag.setHashtagSlug(matchedText);
                        Log.d("hashtag1",""+hashtag.getHashtagSlug());
                        context.startActivity(new Intent(context, HashtagPostsActivity.class).putExtra("hashtag",hashtag));
                    }else if(autoLinkMode == AutoLinkMode.MODE_URL){
                        try{
                            Log.d("postTextUrl",""+matchedText);
                            Uri uri = Uri.parse(matchedText);
                            Intent httpIntent = new Intent(Intent.ACTION_VIEW,uri);
                            httpIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.getBaseContext().startActivity(httpIntent);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
            });



        }

        void setClickListener(Post post, Activity context, OnSwaggerPostClickListener listener, int position, VideoViewHolder holder){

            final GestureDetector gd = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDown(MotionEvent e) {
                    return true;
                }

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    if(helper!=null){
                        if(helper.getPlayWhenReady()){
                            thumbnail_layout.setVisibility(View.VISIBLE);
                            pausePlayerToPlayAgain();
                        }else {
                            thumbnail_layout.setVisibility(View.GONE);
                            startPlayer();
                        }
                    }
                    return true;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    if(like.isChecked()){
                        like.setChecked(false);
                    }else {
                        like.setChecked(true);
                    }
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    return true;
                }
            });

            storyImage.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gd.onTouchEvent(event);
                }
            });

            /*txtStory.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gd.onTouchEvent(event);
                }
            });*/

            text_with_image_background.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gd.onTouchEvent(event);
                }
            });

            playerView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gd.onTouchEvent(event);
                }
            });

            imgAddPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCreatePostClicked();
                }
            });

            like.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.isPressed()) {
                        if (isChecked) {
                            likesCount.setTextColor(Color.parseColor("#FF7400"));

                            int counter = post.getLikeCount();
                            post.setLikeCount(counter + 1);

                            String likeCount = (Integer.valueOf(likesCount.getText().toString()) + 1) + "";
                            likesCount.setText(likeCount);
                        } else {
                            if (Integer.valueOf(likesCount.getText().toString()) > 0) {
                                likesCount.setTextColor(Color.parseColor("#ffffff"));

                                int counter = post.getLikeCount();
                                post.setLikeCount(counter - 1);

                                String likeCount = (Integer.parseInt(likesCount.getText().toString().trim()) - 1) + "";
                                likesCount.setText(likeCount);
                            }
                        }
                    }else {
                        if(isChecked){

                            Log.d("isChecked","called...");

                            likesCount.setTextColor(Color.parseColor("#FF7400"));

                            int counter = post.getLikeCount();
                            post.setLikeCount(counter + 1);

                            String likeCount = (Integer.valueOf(likesCount.getText().toString()) + 1) + "";
                            likesCount.setText(likeCount);
                        }else {

                            Log.d("isChecked","false...");

                            if (Integer.valueOf(likesCount.getText().toString()) > 0) {
                                likesCount.setTextColor(Color.parseColor("#ffffff"));

                                int counter = post.getLikeCount();
                                post.setLikeCount(counter - 1);

                                String likeCount = (Integer.parseInt(likesCount.getText().toString().trim()) - 1) + "";
                                likesCount.setText(likeCount);
                            }
                        }
                    }


                    if (buttonView.isPressed()) {
                        post.setLiked(isChecked);
                        listener.onPostChecked(post,position,false);
                    }else {
                        post.setLiked(isChecked);
                        listener.onPostChecked(post,position,false);
                    }
                }
            });

            likesCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!"0".equals(likesCount.getText().toString())) {
                        context.startActivity(new Intent(context, PostLikedByActivity.class)
                                .putExtra("postId", post.getPostId()));
                    }
                }
            });



            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onFollowClicked(post);

                    // Original follow click code

                    /*boolean isFollow = false;
                    if(post != null && post.getFollowing() != null){
                        if(post.getFollowing()) {
                            isFollow = false;
                        }else {
                            isFollow = true;
                            if(checkIsRequested(post)){
                                isFollow = false;
                            }
                        }
                    }else {
                        isFollow = true;
                    }

                    if (isFollow) {
                        if(post.getPrivateAccount()) {
                            follow.setTextColor(context.getResources().getColor(R.color.orange));
                            follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                            follow.setText(context.getResources().getString(R.string.requested));
                            follow.setEnabled(false);
                            listener.onFollowClicked(post);
                        }else {
                            follow.setTextColor(context.getResources().getColor(R.color.orange));
                            follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                            follow.setText(context.getResources().getString(R.string.following));
                            follow.setEnabled(false);
                            listener.onFollowClicked(post);
                        }
                    }*/

                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(post.getPostType().equalsIgnoreCase("shared")){

                    }else {
                        listener.onShareClicked(post,context);
                    }

                }
            });

            socialShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = UrlEndpoints.BASE_SHARE_POST_URL + post.getPostId();

                    if(listener!=null){
                        if ("shared".equals(post.getPostType())) {
                            listener.onDownloadClick(post.getSharedPost().getPostVideo(),url);
                        }else {
                            listener.onDownloadClick(post.getPostVideo(),url);
                        }
                    }
                }
            });

            comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCommentClicked(post,position,holder);
                }
            });

        /*share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainStoryPostActivity.this, SharePostActivity.class).putExtra("post",post),100);
            }
        });

        socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = UrlEndpoints.BASE_SHARE_POST_URL + post.getStoryId();
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TITLE,"Afrocamgist");
                i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                i.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(i, "Share URL"));
            }
        });*/

            report.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onReportClicked(post.getUserId());
                }
            });


            relProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onProfileClicked(post.getUserId());
                }
            });

            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenu(imgEdit, holder.getAdapterPosition());
                }
            });

        }




        void viewVisibility(String postType){
            if(postType.equalsIgnoreCase("image")){
                storyImage.setVisibility(View.VISIBLE);
                videoLayout.setVisibility(View.GONE);
                txtStory.setVisibility(View.GONE);
                text_with_image_background.setVisibility(View.GONE);
                videoCount.setVisibility(View.GONE);
                socialShare.setVisibility(View.GONE);
                //postText.setVisibility(View.VISIBLE);
            }else if (postType.equalsIgnoreCase("video")){
                videoLayout.setVisibility(View.VISIBLE);
                storyImage.setVisibility(View.GONE);
                txtStory.setVisibility(View.GONE);
                videoCount.setVisibility(View.VISIBLE);
                text_with_image_background.setVisibility(View.GONE);
                //socialShare.setVisibility(View.VISIBLE); // not accessible for non-registered user
                //postText.setVisibility(View.VISIBLE);
            }else if (postType.equalsIgnoreCase("text")){
                txtStory.setVisibility(View.VISIBLE);
                storyImage.setVisibility(View.GONE);
                videoLayout.setVisibility(View.GONE);
                text_with_image_background.setVisibility(View.GONE);
                videoCount.setVisibility(View.GONE);
                socialShare.setVisibility(View.GONE);
                postText.setVisibility(View.GONE);
            }else if (postType.equalsIgnoreCase("background_post")){
                text_with_image_background.setVisibility(View.VISIBLE);
                txtStory.setVisibility(View.GONE);
                storyImage.setVisibility(View.GONE);
                videoLayout.setVisibility(View.GONE);
                videoCount.setVisibility(View.GONE);
                socialShare.setVisibility(View.GONE);
                postText.setVisibility(View.GONE);
            }
        }
/*

        void initPlayer(Post post, Activity context){

            String videoUrl = UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo();
            MediaSource mediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(videoUrl));

            if(simpleExoPlayer!=null){
                playerView.setPlayer(simpleExoPlayer);
                simpleExoPlayer.setPlayWhenReady(true);
                simpleExoPlayer.seekTo(0);
                simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
                simpleExoPlayer.prepare(mediaSource,true,false);

                simpleExoPlayer.addListener(new Player.DefaultEventListener() {
                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                        super.onPlayerStateChanged(playWhenReady, playbackState);

                    */
/*if(simpleExoPlayer.getPlayWhenReady()){
                        if(isNewVideo && !isPlayerPaused){
                            momentz.editDurationAndResume(index, (int) ((simpleExoPlayer.getDuration()) / 1000));
                        }else if(!isNewVideo && isPlayerPaused){
                            isNewVideo = false;
                            isPlayerPaused = true;
                        }
                    }*//*

                    }
                });
            }
        }
*/

        private void pausePlayer(){
            if (helper != null) {
                helper.setPlayWhenReady(false);
                helper.seekTo(0);
                helper.stop();
                helper.getPlaybackState();
            }
        }

        private void pausePlayerToPlayAgain(){
            if (helper != null) {
                helper.setPlayWhenReady(false);
                helper.getPlaybackState();
            }
        }

        public void startPlayer() {
            if (helper != null) {
                helper.setPlayWhenReady(true);
                helper.getPlaybackState();
            }
        }

        public void createAndStartPlayer(VideoViewHolder holder) {
            if (helper != null) {
                helper.setPlayWhenReady(true);
                helper.getPlaybackState();
            }else {
                helper = newSimpleExoPlayer(context);
                holder.play();
            }
        }

        public void releasePlayer() {
            if (helper != null) {
                helper.setPlayWhenReady(false);
                helper.release();
                helper.stop(true);
            }
        }

        private SimpleExoPlayer newSimpleExoPlayer(Context context) {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            LoadControl loadControl = new DefaultLoadControl();
            return ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl);
        }

        private boolean checkIsRequested(Post profileUser) {
            if(profileUser!=null)
            {
                boolean isRequestSend = false;
                if(profileUser.getRequestButtons().get(0).getButtonText().equalsIgnoreCase("Requested")){
                    isRequestSend = true;
                }
                return isRequestSend;
            }
            return false;
        }

    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (getItemViewType(holder.getAdapterPosition()) == POST) {
            startHolderVideo((OpenHomeAdapter.VideoViewHolder) holder);
        }
    }

    private void startHolderVideo(VideoViewHolder holder){
        if(holder.postType!=null){
            if(holder.postType.equalsIgnoreCase("video")){
                if(holder.getAdapterPosition() != 0){
                    Log.d("playerStateFrom99","attached......");
                    holder.createAndStartPlayer(holder);
                }
            }
        }
    }

    public void createPlayer(RecyclerView.ViewHolder holder){
        if(holder instanceof VideoViewHolder){
            helper = newSimpleExoPlayer(context);
            ((OpenHomeAdapter.VideoViewHolder) holder).play();
        }
    }

    private SimpleExoPlayer newSimpleExoPlayer(Context context) {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
        return ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (getItemViewType(holder.getAdapterPosition()) == POST) {
            Log.d("playerStateFrom99","detached...");
            pauseHolderVideo((OpenHomeAdapter.VideoViewHolder) holder);
        }
    }

    public void onCommentDialogDismiss(int scrollPosition, int commentCount, VideoViewHolder holder){
        if(holder!=null){
            holder.comment.setText("" + commentCount);
        }
    }

    private void pauseHolderVideo(VideoViewHolder holder){
        holder.pausePlayer();
    }

    public interface OnSwaggerPostClickListener{
        void onPauseVideo(Post post,SimpleExoPlayer simpleExoPlayer);
        void onPostChecked(Post post,int position, boolean isDoubleClicked);
        void onFollowClicked(Post post);
        void onCommentClicked(Post post,int position, VideoViewHolder holder);
        void onReportClicked(Integer userId);
        void onProfileClicked(Integer userId);
        void onPostView(Integer postId);
        void onPlayItem(int adapterPosition);
        void onVideoCount(Integer postId);
        void onPostViewCount(Integer postId);
        void onCreatePostClicked();
        void onShareClicked(Post post,Activity context);
        void onDownloadClick(String videoUrl, String postLink);
        void openDetailScreen(RecyclerView.ViewHolder viewHolder);

        void onEditPost(Post post, int position);
        void onDeletePost(Post post);
        void onHidePost(Post post);
    }
}
