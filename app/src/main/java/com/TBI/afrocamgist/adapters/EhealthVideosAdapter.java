package com.TBI.afrocamgist.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.AdvertSelectGoalActivity;
import com.TBI.afrocamgist.activity.HashtagPostsActivity;
import com.TBI.afrocamgist.activity.PostLikedByActivity;
import com.TBI.afrocamgist.app.AfrocamgistApplication;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.advertisement.Advertisement;
import com.TBI.afrocamgist.model.ehealth.Testimonials;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.post.Post;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.jsibbold.zoomage.ZoomageView;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;

import static com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter.isMute;

public class EhealthVideosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Testimonials> testimonialsArrayList;
    private Activity context;
    public OnTestimonialsClickListener listener;
    private static final int POST = 0;


    public SimpleExoPlayer helper;
    private PopupWindow popup;


    public EhealthVideosAdapter(Activity context, ArrayList<Testimonials> testimonialsArrayList, OnTestimonialsClickListener listener) {
        this.context = context;
        this.testimonialsArrayList = testimonialsArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        /*LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;

        view = inflater.inflate(R.layout.testimonials_item, parent, false);
        VideoViewHolder videoViewHolder = new VideoViewHolder(view);
        return videoViewHolder;*/


        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.testimonials_item, parent, false);
        int width = parent.getWidth();
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = (int)(width * 0.8);
        view.setLayoutParams(params);
        return new VideoViewHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        showPosts((VideoViewHolder) holder);

    }

    public void showPosts(VideoViewHolder holder) {

        Testimonials post = testimonialsArrayList.get(holder.getAdapterPosition());

        Log.d("Testimonials",""+post.getThumbnails().get(9));

        if (post != null) {

            Glide.with(context)
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnails().get(9))
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(holder.backgroundThumbnail);


            holder.backgroundThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null){
                        listener.onClickTestimonials(post);
                    }
                }
            });


        }

    }

    @Override
    public int getItemCount() {
        return testimonialsArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return POST;
    }


    public class VideoViewHolder extends RecyclerView.ViewHolder {

        private ImageView backgroundThumbnail;

        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);

            backgroundThumbnail = itemView.findViewById(R.id.backgroundThumbnail);


        }

    }


    public interface OnTestimonialsClickListener {
        void onClickTestimonials(Testimonials post);
    }
}
