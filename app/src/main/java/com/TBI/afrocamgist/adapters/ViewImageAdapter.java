package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.TBI.afrocamgist.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ViewImageAdapter extends RecyclerView.Adapter<ViewImageAdapter.DataObjectHolder> {

    private ArrayList<String> images;
    private OnImageClickListener listener;

    public ViewImageAdapter(ArrayList<String> images, OnImageClickListener listener) {
        this.images = images;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_post_image, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        Glide.with(holder.image)
                .load(images.get(position))
                .into(holder.image);

        holder.editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onEditImageClick(position);
            }
        });

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRemoveImageClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (images ==null)
            return 0;
        else
            return images.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        ImageView image;
        ImageView editImage, remove;

        DataObjectHolder(View itemView) {
            super(itemView);
            editImage = itemView.findViewById(R.id.edit_image);
            image = itemView.findViewById(R.id.image);
            remove = itemView.findViewById(R.id.remove);
        }
    }

    public interface OnImageClickListener {
        void onRemoveImageClick(int position);
        void onEditImageClick(int position);
    }
}
