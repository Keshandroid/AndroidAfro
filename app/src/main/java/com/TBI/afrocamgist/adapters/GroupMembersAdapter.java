package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.ChatActivity;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.groups.GroupMemberData;
import com.TBI.afrocamgist.model.user.User;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupMembersAdapter extends RecyclerView.Adapter<GroupMembersAdapter.DataObjectHolder> {

    private Activity context;
    private ArrayList<GroupMemberData> groupMemberData;
    User user;

    public GroupMembersAdapter(Activity context, ArrayList<GroupMemberData> groupMemberList) {
        this.context = context;
        this.groupMemberData = groupMemberList;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_group_member, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {
        setGroupMemberList(holder);
    }

    private void setGroupMemberList(DataObjectHolder holder) {

        GroupMemberData groupMember = groupMemberData.get(holder.getAdapterPosition());

        String fullName = groupMember.getFirstName() + " " + groupMember.getLastName();
        holder.name.setText(fullName);

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + groupMember.getProfileImageUrl())
                .into(holder.profileImage);

        holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LocalStorage.getUserDetails().getUserId().equals(groupMember.getUserId()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class).putExtra("userId", groupMember.getUserId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (groupMemberData == null)
            return 0;
        else
            return groupMemberData.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name;
        CircleImageView profileImage;

        CardView profile;

        DataObjectHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            profileImage = itemView.findViewById(R.id.profile_image);
            profile = itemView.findViewById(R.id.profile);
        }
    }

}
