package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.groups.Group;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AfroGroupInvitationAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<Group> groupInvitations;
    private OnGroupClickListener listener;

    public AfroGroupInvitationAdapter(Activity context, ArrayList<Group> groupInvitations, OnGroupClickListener listener) {
        this.context = context;
        this.groupInvitations = groupInvitations;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return groupInvitations.size();
    }

    @Override
    public Object getItem(int position) {
        return groupInvitations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.item_group_invitation_grid, parent, false);
            convertView.setTag(holder);
            holder.groupIcon = convertView.findViewById(R.id.group_icon);
            holder.groupTitle = convertView.findViewById(R.id.group_title);
            holder.join = convertView.findViewById(R.id.join);
            holder.other = convertView.findViewById(R.id.other);
        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + groupInvitations.get(position).getGroupImage())
                .into(holder.groupIcon);

        holder.groupTitle.setText(groupInvitations.get(position).getGroupTitle());
        holder.other.setText(R.string.reject);

        holder.join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onGroupInvitationAcceptClick(groupInvitations.get(position));
            }
        });

        return convertView;
    }

    class ViewHolder {
        ImageView groupIcon;
        TextView groupTitle;
        Button join,other;
    }

    public interface OnGroupClickListener {
        void onGroupInvitationAcceptClick(Group group);
    }
}
