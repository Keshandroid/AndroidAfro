package com.TBI.afrocamgist.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.BitmapUtils;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.View.DataObjectHolder;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.PostLikedByActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.ReportPostActivity;
import com.TBI.afrocamgist.activity.SharePostActivity;
import com.TBI.afrocamgist.activity.ViewPostImageActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.Post;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerView;

import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;

import static com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter.isMute;

public class PopularPostAdapter extends RecyclerView.Adapter<PopularPostAdapter.RecyclerObjectHolder> implements MenuAdapter.OnMenuItemClickListener{

    private Activity context;
    private ArrayList<Post> posts;
    private OnPostClickListener listener;
    private PopupWindow popup;
    //private boolean isFollow = false;

    public PopularPostAdapter(Activity context, ArrayList<Post> posts, OnPostClickListener listener) {
        this.context = context;
        this.posts = posts;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_group_post,viewGroup,false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerObjectHolder holder, int position) {

        if ("shared".equals(posts.get(position).getPostType())) {
            holder.sharedLayout.setVisibility(View.VISIBLE);
            holder.share.setVisibility(View.GONE); //Cannot share a shared post (same in website)
            holder.follow.setVisibility(View.GONE);

            if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                holder.sharedFollow.setVisibility(View.GONE);
            } else {
                holder.sharedFollow.setVisibility(View.VISIBLE);
                if (posts.get(position)!=null && posts.get(position).getFollowing()!=null) {
                    holder.sharedFollow.setChecked(posts.get(position).getFollowing());
                    holder.sharedFollow.setEnabled(!posts.get(position).getFollowing());
                } else {
                    holder.sharedFollow.setChecked(true);
                    holder.sharedFollow.setEnabled(false);
                }
            }

            if ("video".equals(posts.get(position).getSharedPost().getPostType())) {
                Glide.with(context)
                        .load(UrlEndpoints.MEDIA_BASE_URL + posts.get(position).getSharedPost().getThumbnail())
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .into(holder.thumbnail);
            }
        } else {
            holder.sharedLayout.setVisibility(View.GONE);
            holder.share.setVisibility(View.VISIBLE);
            holder.sharedFollow.setVisibility(View.GONE);

            if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                holder.follow.setVisibility(View.GONE);
            } else {
                holder.follow.setVisibility(View.VISIBLE);
               /* if (posts.get(position)!=null && posts.get(position).getFollowing()!=null) {
                    holder.follow.setText(context.getResources().getString(R.string.follow));
                    holder.follow.setEnabled(!posts.get(position).getFollowing());
                } else {
                    holder.follow.setText(context.getResources().getString(R.string.following));
                    holder.follow.setEnabled(false);
                }*/

                if (posts.get(position) != null && posts.get(position).getFollowing() != null) {
                    if (posts.get(position).getFollowing()) {
                         holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                         holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                         holder.follow.setText(context.getResources().getString(R.string.following));
                        holder.follow.setEnabled(false);
                        //isFollow = false;
                    } else {
                        holder.follow.setTextColor(context.getResources().getColor(R.color.blue));
                        holder.follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                        holder.follow.setText(context.getResources().getString(R.string.follow));
//                                Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                        holder.follow.setEnabled(true);
                        //isFollow = true;

                        if(checkIsRequested(posts.get(position))){
                            holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                            holder.follow.setTextColor(context.getResources().getColor(R.color.colorAccent));
                            holder.follow.setText(R.string.requested);
                            holder.follow.setEnabled(false);
                            //isFollow = false;
                        }

                    }
                } else {
                    holder.follow.setTextColor(context.getResources().getColor(R.color.blue));
                    holder.follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                    holder.follow.setText(context.getResources().getString(R.string.follow));
//                            Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                    holder.follow.setEnabled(true);
                    //isFollow = true;
                }
            }

            if ("video".equals(posts.get(position).getPostType())) {
                Glide.with(context)
                        .load(UrlEndpoints.MEDIA_BASE_URL + posts.get(position).getThumbnail())
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .into(holder.thumbnail);
            }
        }

        setPostData(posts.get(position),holder, false);
        setClickListener(holder);
    }

    private void setPostData(Post post, RecyclerObjectHolder holder, Boolean isSharedPost) {

        if ("shared".equals(post.getPostType())) {

            setSharedPostData(post, holder);
            setPostData(post.getSharedPost(), holder, true);

        } else {

            if(post.getUser_name()!=null && !post.getUser_name().isEmpty()){
                holder.name.setText(post.getUser_name());
            }else {
                String fullName = post.getFirstName() + " " + post.getLastName();
                holder.name.setText(fullName);
            }

            if (post.getPostDate()==null)
                holder.time.setText("");
            else
                holder.time.setText(Utils.getSocialStyleTime(post.getPostDate()));

            if (post.getPostLatLng()!=null && !"".equals(post.getPostLatLng())) {
                holder.location.setVisibility(View.VISIBLE);
                holder.location.setText(post.getPostLocation());
            } else {
                holder.location.setVisibility(View.GONE);
            }

            if (!isSharedPost) {
                if (post.getLiked()) {
                    holder.like.setChecked(true);
                    holder.likesCount.setTextColor(Color.parseColor("#FF7400"));
                } else {
                    holder.like.setChecked(false);
                    holder.likesCount.setTextColor(Color.parseColor("#004F95"));
                }

                if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
                    holder.menu.setVisibility(View.VISIBLE);
                } else {
                    holder.menu.setVisibility(View.GONE);
                }
                String likeCount = post.getLikeCount() + "";
                holder.likesCount.setText(likeCount);
                String commentCount = post.getCommentCount() + "";
                holder.comment.setText(commentCount);
            }

            holder.postText.setText(post.getPostText());
            Glide.with(context)
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                    .into(holder.profilePicture);
            setPostDetails(post, holder);
        }
    }

    private boolean checkIsRequested(Post profileUser) {
        if(profileUser!=null)
        {
            boolean isRequestSend = false;
            if(profileUser.getRequestButtons().get(0).getButtonText().equalsIgnoreCase("Requested")){
                isRequestSend = true;
            }
            return isRequestSend;
        }
        return false;
    }

    private void setSharedPostData(Post post, RecyclerObjectHolder holder) {

        String fullName = post.getFirstName() + " " + post.getLastName();
        holder.sharedName.setText(fullName);
        holder.sharedTime.setText(Utils.getSocialStyleTime(post.getPostDate()));
        holder.sharedPostText.setText(post.getPostText());

        if (post.getPostLatLng()!=null && !"".equals(post.getPostLatLng())) {
            holder.sharedLocation.setVisibility(View.VISIBLE);
            holder.sharedLocation.setText(post.getPostLocation());
        } else {
            holder.sharedLocation.setVisibility(View.GONE);
        }

        if (post.getLiked()) {
            holder.like.setChecked(true);
            holder.likesCount.setTextColor(Color.parseColor("#FF7400"));
        } else {
            holder.like.setChecked(false);
            holder.likesCount.setTextColor(Color.parseColor("#004F95"));
        }

        if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
            holder.menu.setVisibility(View.VISIBLE);
        } else {
            holder.menu.setVisibility(View.GONE);
        }
        String likeCount = post.getLikeCount() + "";
        holder.likesCount.setText(likeCount);
        String commentCount = post.getCommentCount() + "";
        holder.comment.setText(commentCount);

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(holder.sharedProfileImage);
    }

    private void setPostDetails(Post post, RecyclerObjectHolder holder) {

        switch (post.getPostType()) {

            case "text":
                holder.layoutOnlyImages.setVisibility(View.GONE);
                holder.videoLayout.setVisibility(View.GONE);

                if (post.getBackgroundImagePost()){

                    holder.layoutPostWithBackgroundImage.setVisibility(View.VISIBLE);

                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getBackgroundImage())
                            .into(holder.backgroundImage);

                    holder.postTextOnImage.setText(post.getPostText());
                    holder.postText.setText("");
                } else {
                    holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);
                }

                break;
            case "image":
                holder.layoutOnlyImages.setVisibility(View.VISIBLE);
                holder.videoLayout.setVisibility(View.GONE);
                holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.placeholder);

                if (post.getPostImage().size() > 1) {
                    String count = (post.getPostImage().size() - 1) + "";

                    Glide.with(holder.postImage)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .placeholder(R.drawable.placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(holder.postImage);

                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(1))
                            .into(holder.moreImages);

                    holder.mapPostImage.setVisibility(View.GONE);
                    holder.postImage.setVisibility(View.VISIBLE);
                    holder.moreImagesLayout.setVisibility(View.VISIBLE);

                    holder.imageCount.setText(count);
                } else {

                    if (post.getMapPost()) {

                        holder.mapPostImage.setVisibility(View.VISIBLE);
                        holder.postImage.setVisibility(View.GONE);

                        Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                .apply(requestOptions)
                                .into(holder.mapPostImage);
                    } else {

                        holder.mapPostImage.setVisibility(View.GONE);
                        holder.postImage.setVisibility(View.VISIBLE);

                        Glide.with(holder.postImage)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                .placeholder(R.drawable.placeholder)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(holder.postImage);
                    }

                    holder.moreImagesLayout.setVisibility(View.GONE);
                }

                new Zoomy.Builder(context)
                        .target(holder.postImage)
                        .enableImmersiveMode(false)
                        .tapListener(v -> context.startActivity(new Intent(context, ViewPostImageActivity.class)
                                .putExtra("position",0)
                                .putExtra("images",post.getPostImage())))
                        .register();

                break;
            case "video":
                holder.layoutOnlyImages.setVisibility(View.GONE);
                holder.videoLayout.setVisibility(View.VISIBLE);
                holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);
                holder.bind(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo()));
                break;
            default:
                break;
        }
    }

    private void setClickListener(RecyclerObjectHolder holder) {

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()) && posts.get(holder.getAdapterPosition()).getSharedPost()!=null){
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getSharedPost().getUserId()))
                        context.startActivity(new Intent(context, MyProfileActivity.class));
                    else
                        context.startActivity(new Intent(context, ProfileActivity.class)
                                .putExtra("userId",posts.get(holder.getAdapterPosition()).getSharedPost().getUserId()));
                } else {
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getUserId()))
                        context.startActivity(new Intent(context, MyProfileActivity.class));
                    else
                        context.startActivity(new Intent(context, ProfileActivity.class)
                                .putExtra("userId", posts.get(holder.getAdapterPosition()).getUserId()));
                }
            }
        });

        holder.sharedName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getUserId()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class)
                            .putExtra("userId", posts.get(holder.getAdapterPosition()).getUserId()));
            }
        });

        holder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onCommentClicked(posts.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivityForResult(new Intent(context, SharePostActivity.class).putExtra("post",posts.get(holder.getAdapterPosition())),100);
            }
        });

        holder.socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = UrlEndpoints.BASE_SHARE_POST_URL + posts.get(holder.getAdapterPosition()).getPostId();

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TITLE,"Afrocamgist");
                i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                i.putExtra(Intent.EXTRA_TEXT, url);
                context.startActivity(Intent.createChooser(i, "Share URL"));
            }
        });

        holder.report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ReportPostActivity.class).putExtra("postId",posts.get(holder.getAdapterPosition()).getPostId()));
            }
        });

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(holder.menu,holder.getAdapterPosition());
            }
        });

        holder.like.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    if (isChecked) {
                        holder.likesCount.setTextColor(Color.parseColor("#FF7400"));

                        int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                        posts.get(holder.getAdapterPosition()).setLikeCount(counter + 1);

                        String likesCount = (Integer.valueOf(holder.likesCount.getText().toString()) + 1) + "";
                        holder.likesCount.setText(likesCount);
                    } else {
                        if (Integer.valueOf(holder.likesCount.getText().toString()) > 0) {
                            holder.likesCount.setTextColor(Color.parseColor("#004F95"));

                            int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                            posts.get(holder.getAdapterPosition()).setLikeCount(counter - 1);

                            String likesCount = (Integer.parseInt(holder.likesCount.getText().toString().trim()) - 1) + "";
                            holder.likesCount.setText(likesCount);
                        }
                    }
                }

                if (buttonView.isPressed()) {
                    posts.get(holder.getAdapterPosition()).setLiked(isChecked);
                    listener.onPostChecked(posts.get(holder.getAdapterPosition()));
                }
            }
        });

        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isFollow = false;
                if(posts.get(holder.getAdapterPosition()) != null && posts.get(holder.getAdapterPosition()).getFollowing() != null){
                    if(posts.get(holder.getAdapterPosition()).getFollowing()) {
                        isFollow = false;
                    }else {
                        isFollow = true;
                        if(checkIsRequested(posts.get(holder.getAdapterPosition()))){
                            isFollow = false;
                        }
                    }
                }else {
                    isFollow = true;
                }

                if (isFollow) {
                    if(posts.get(holder.getAdapterPosition()).getPrivateAccount()) {
                        holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.follow.setText(context.getResources().getString(R.string.requested));
                        holder.follow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }else {
                        holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.follow.setText(context.getResources().getString(R.string.following));
                        holder.follow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }


                }
            }
        });

//        holder.follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked && buttonView.isPressed()) {
//                    holder.follow.setEnabled(false);
//                    listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
//                }
//            }
//        });

        holder.sharedFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && buttonView.isPressed()) {
                    holder.sharedFollow.setEnabled(false);
                    listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                }
            }
        });

        holder.likesCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!"0".equals(holder.likesCount.getText().toString())) {
                    context.startActivity(new Intent(context, PostLikedByActivity.class)
                            .putExtra("postId", posts.get(holder.getAdapterPosition()).getPostId()));
                }
            }
        });

        holder.moreImagesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> images;

                if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()))
                    images = posts.get(holder.getAdapterPosition()).getSharedPost().getPostImage();
                else
                    images = posts.get(holder.getAdapterPosition()).getPostImage();

                context.startActivity(new Intent(context, ViewPostImageActivity.class)
                        .putExtra("position",1)
                        .putExtra("images",images));
            }
        });
    }

    private void showMenu(View menu, int position) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int popupWidth = 450;
        int popupHeight = LinearLayout.LayoutParams.WRAP_CONTENT;

        LinearLayout viewGroup = context.findViewById(R.id.menu_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.drop_down_menu, viewGroup);
        popupMenu(layout, position);

        popup = new PopupWindow(layout, popupWidth, popupHeight);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        if (position == (posts.size()-1))
            popup.showAsDropDown(menu,0,-300, Gravity.TOP);
        else
            popup.showAsDropDown(menu);
    }

    private void popupMenu(View layout, int position) {

        RecyclerView menu = layout.findViewById(R.id.menuList);
        menu.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        menu.setLayoutManager(mLayoutManager);
        MenuAdapter adapter;
        if (posts.get(position).getMapPost())
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_map_menu))), position,this);
        else
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_menu))), position,this);
        menu.setAdapter(adapter);
    }

    @Override
    public void onMenuItemClicked(String item, int postPosition) {

        if (popup!=null && popup.isShowing())
            popup.dismiss();

        switch (item.toLowerCase()) {

            case "edit":
                listener.onEditPost(posts.get(postPosition), postPosition);
                break;
            case "delete":
                listener.onDeletePost(posts.get(postPosition));
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (posts==null)
            return 0;
        else
            return posts.size();
    }

    static class RecyclerObjectHolder extends RecyclerView.ViewHolder {

        ImageView postImage, moreImages, share, menu,thumbnail, backgroundImage, socialShare, report, mapPostImage;
        /*PhotoView postImage;*/
        CircleImageView profilePicture, sharedProfileImage, myProfilePicture;
        RelativeLayout /*layoutOnlyImages,*/ videoLayout, moreImagesLayout;
        FrameLayout layoutOnlyImages,thumbnailLayout;
        LinearLayout sharedLayout;
        RelativeLayout layoutPostWithBackgroundImage;
        TextView follow;
        ToggleButton like, sharedFollow;
        TextView name,time, postText, imageCount, likesCount, comment, location, sharedName, sharedLocation,
                sharedTime, sharedPostText, listTitle, postTextOnImage;
        Uri mediaUri;

        RecyclerObjectHolder(View itemView){
            super(itemView);
            moreImages = itemView.findViewById(R.id.more_images);
            profilePicture = itemView.findViewById(R.id.profile_picture);
            myProfilePicture = itemView.findViewById(R.id.profile_image);
            sharedProfileImage = itemView.findViewById(R.id.shared_profile_image);
            share = itemView.findViewById(R.id.share);
            menu = itemView.findViewById(R.id.menu);
            layoutOnlyImages = itemView.findViewById(R.id.layout_only_images);
            moreImagesLayout = itemView.findViewById(R.id.more_images_layout);
            sharedLayout = itemView.findViewById(R.id.shared_layout);
            videoLayout = itemView.findViewById(R.id.video_layout);
            name = itemView.findViewById(R.id.name);
            time = itemView.findViewById(R.id.time);
            postText = itemView.findViewById(R.id.post_text);
            sharedName = itemView.findViewById(R.id.shared_name);
            sharedLocation = itemView.findViewById(R.id.shared_location);
            sharedTime = itemView.findViewById(R.id.shared_time);
            sharedPostText = itemView.findViewById(R.id.shared_post_text);
            postImage = itemView.findViewById(R.id.post_image);
            imageCount = itemView.findViewById(R.id.image_count);
            likesCount = itemView.findViewById(R.id.likes_count);
            comment = itemView.findViewById(R.id.comment);
            location = itemView.findViewById(R.id.location);
            like = itemView.findViewById(R.id.like);
            follow = itemView.findViewById(R.id.follow);
            sharedFollow = itemView.findViewById(R.id.follow_shared);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            listTitle = itemView.findViewById(R.id.list_title);
            backgroundImage = itemView.findViewById(R.id.background_image);
            postTextOnImage = itemView.findViewById(R.id.post_text_on_image);
            layoutPostWithBackgroundImage = itemView.findViewById(R.id.text_with_image_background);
            socialShare = itemView.findViewById(R.id.social_share);
            report = itemView.findViewById(R.id.report);
            thumbnailLayout = itemView.findViewById(R.id.thumbnail_layout);
            mapPostImage = itemView.findViewById(R.id.post_map_image);
        }

        void bind(Uri media) {
            this.mediaUri = media;
        }
    }

    static class DataObjectHolder extends RecyclerObjectHolder implements ToroPlayer {

        PlayerView playerView;
        ExoPlayerViewHelper helper;
        Uri mediaUri;
        ToggleButton volume;

        DataObjectHolder(View itemView) {
            super(itemView);
            playerView = itemView.findViewById(R.id.player);
            volume = itemView.findViewById(R.id.volume);
        }

        @NonNull
        @Override
        public View getPlayerView() {
            return playerView;
        }

        @NonNull
        @Override
        public PlaybackInfo getCurrentPlaybackInfo() {
            return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
        }

        @Override
        public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {
            thumbnailLayout.setVisibility(View.VISIBLE);
            if (helper == null) {
                helper = new ExoPlayerViewHelper(this, mediaUri);
            }
            helper.initialize(container,playbackInfo);
        }

        @Override
        public void play() {
            if (helper != null) {
                helper.play();
                playerView.getPlayer().setRepeatMode(Player.REPEAT_MODE_ALL);
                if (isMute) {
                    mute();
                    volume.setChecked(false);
                } else {
                    unMute();
                    volume.setChecked(true);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        thumbnailLayout.setVisibility(View.GONE);
                    }
                },1000);
            }
        }

        @Override
        public void pause() {
            if (helper != null) helper.pause();
        }

        @Override
        public boolean isPlaying() {
            return helper != null && helper.isPlaying();
        }

        @Override
        public void release() {
            if (helper != null) {
                thumbnailLayout.setVisibility(View.VISIBLE);
                helper.release();
                helper = null;
            }
        }

        @Override
        public boolean wantsToPlay() {
            return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85;
        }

        @Override
        public int getPlayerOrder() {
            return getAdapterPosition();
        }

        @SuppressLint("ClickableViewAccessibility")
        void bind(Uri media) {
            this.mediaUri = media;
            this.volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        unMute();
                    else
                        mute();
                }
            });
        }


        private void mute() {
            isMute = true;
            this.setVolume(0);
        }

        private void unMute() {
            isMute = false;
            this.setVolume(100);
        }

        private void setVolume(int amount) {
            final int max = 100;
            final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
            final float volume = (float) (1 - (numerator / Math.log(max)));

            if(this.helper !=null){
                this.helper.setVolume(volume);
            }
        }
    }


    public interface OnPostClickListener {
        void onPostChecked(Post post);
        void onEditPost(Post post, int position);
        void onDeletePost(Post post);
        void onCommentClicked(Post post, int position);
        void onFollowClicked(Post post);
    }
}
