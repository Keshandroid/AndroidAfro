package com.TBI.afrocamgist.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.BitmapUtils;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.View.DataObjectHolder;
import com.TBI.afrocamgist.activity.HashtagPostsActivity;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.PostLikedByActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.ReportPostActivity;
import com.TBI.afrocamgist.activity.SharePostActivity;
import com.TBI.afrocamgist.activity.ViewPostImageActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnPostClickListener;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.post.Post;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerView;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter.isMute;
import static com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter.isSwaggerPlay;
import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class AfroTalentPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements MenuAdapter.OnMenuItemClickListener {

    private Activity context;
    private ArrayList<Post> posts;
    private PopupWindow popup;
    private static final int UPLOAD_POST = 0, POST = 1, POPULAR_POSTS = 2, POSTING = 3;
    private int progress;
    private AfroPopularPostAdapter adapter;
    private ArrayList<Post> popularPostList;
    private OnPostClickListener listener;
    public static boolean isTalentPlay = false;
    //private boolean isFollow = false;
    public Handler handler;

    public AfroTalentPostAdapter(Activity context, ArrayList<Post> posts, OnPostClickListener listener) {
        this.context = context;
        this.posts = posts;
        this.progress = 0;
        this.popularPostList = new ArrayList<>();
        this.listener = listener;
        adapter = new AfroPopularPostAdapter(context, popularPostList);
        handler = new Handler();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;

        switch (viewType) {

            case UPLOAD_POST:
                view = inflater.inflate(R.layout.item_upload_post,parent,false);
                return new RecyclerObjectHolder(view);
            case POPULAR_POSTS:
                view = inflater.inflate(R.layout.item_horizontal_list,parent,false);
                return new RecyclerObjectHolder(view);
            case POSTING:
                view = inflater.inflate(R.layout.item_uploading_post,parent, false);
                return new RecyclerObjectHolder(view);
            default:
                view = inflater.inflate(R.layout.item_group_post,parent,false);
                return new DataObjectHolder(view,listener,context);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)){
            case UPLOAD_POST:
                ((RecyclerObjectHolder)holder).write.setText(R.string.postHereText);
                setUploadPostClickListener(((RecyclerObjectHolder)holder));
                break;
            case POPULAR_POSTS:
                setPopularPostsRecyclerView(((RecyclerObjectHolder)holder));
                break;
            case POSTING:
                showPostingView(((RecyclerObjectHolder)holder));
                break;
            default:

                if ("shared".equals(posts.get(position).getPostType()) && posts.get(position).getSharedPost()!=null) {

                    listener.onPostViewCount(posts.get(position).getPostId());

                    //((DataObjectHolder)holder).sharedTime.setVisibility(View.VISIBLE);
                    //((DataObjectHolder)holder).time.setVisibility(View.GONE);

                    ((DataObjectHolder)holder).playerView.setTag(posts.get(position).getSharedPost().getPostType());
                    ((DataObjectHolder)holder).sharedLayout.setVisibility(View.VISIBLE);
                    ((DataObjectHolder)holder).share.setVisibility(View.GONE); //Cannot share a shared post (same in website)
                    ((DataObjectHolder)holder).follow.setVisibility(View.GONE);


                    //Original code for shared post follow button
                    /*if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                        ((DataObjectHolder)holder).sharedFollow.setVisibility(View.GONE);
                    } else {
                        ((DataObjectHolder)holder).sharedFollow.setVisibility(View.VISIBLE);
                        if (posts.get(position)!=null && posts.get(position).getFollowing()!=null) {
                            ((DataObjectHolder)holder).sharedFollow.setChecked(posts.get(position).getFollowing());
                            ((DataObjectHolder)holder).sharedFollow.setEnabled(!posts.get(position).getFollowing());
                        } else {
                            ((DataObjectHolder)holder).sharedFollow.setChecked(true);
                            ((DataObjectHolder)holder).sharedFollow.setEnabled(false);
                        }
                    }*/


                    //New Code for shared post follow button
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                        ((DataObjectHolder) holder).sharedFollow.setVisibility(View.GONE);
                    } else {
                        ((DataObjectHolder) holder).sharedFollow.setVisibility(View.VISIBLE);

                        if (posts.get(position) != null && posts.get(position).getFollowing() != null) {
                            if (posts.get(position).getFollowing()) {
                                ((DataObjectHolder) holder).sharedFollow.setTextColor(context.getResources().getColor(R.color.orange));
                                ((DataObjectHolder) holder).sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                ((DataObjectHolder) holder).sharedFollow.setText(context.getResources().getString(R.string.following));
                                ((DataObjectHolder) holder).sharedFollow.setEnabled(false);
                            } else {
                                ((DataObjectHolder) holder).sharedFollow.setTextColor(context.getResources().getColor(R.color.blue));
                                ((DataObjectHolder) holder).sharedFollow.setBackgroundResource(R.drawable.round_corner_blue_border);
                                ((DataObjectHolder) holder).sharedFollow.setText(context.getResources().getString(R.string.follow));
//                                Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                                ((DataObjectHolder) holder).sharedFollow.setEnabled(true);

                                if(checkIsRequested(posts.get(position))){
                                    ((DataObjectHolder) holder).sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                    ((DataObjectHolder) holder).sharedFollow.setTextColor(context.getResources().getColor(R.color.colorAccent));
                                    ((DataObjectHolder) holder).sharedFollow.setText(R.string.requested);
                                    ((DataObjectHolder) holder).sharedFollow.setEnabled(false);
                                }
                            }
                        } else {
                            ((DataObjectHolder) holder).sharedFollow.setTextColor(context.getResources().getColor(R.color.blue));
                            ((DataObjectHolder) holder).sharedFollow.setBackgroundResource(R.drawable.round_corner_blue_border);
                            ((DataObjectHolder) holder).sharedFollow.setText(context.getResources().getString(R.string.follow));
//                            Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                            ((DataObjectHolder) holder).sharedFollow.setEnabled(true);
                        }
                    }

                    if ("video".equals(posts.get(position).getSharedPost().getPostType())) {
                        Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + posts.get(position).getSharedPost().getThumbnail())
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                .into(((DataObjectHolder)holder).thumbnail);
                    }
                } else {

                    listener.onPostViewCount(posts.get(position).getPostId());

                    //((DataObjectHolder)holder).sharedTime.setVisibility(View.GONE);
                    //((DataObjectHolder)holder).time.setVisibility(View.VISIBLE);

                    ((DataObjectHolder)holder).playerView.setTag(posts.get(position).getPostType());
                    ((DataObjectHolder)holder).sharedLayout.setVisibility(View.GONE);
                    ((DataObjectHolder)holder).share.setVisibility(View.VISIBLE);
                    ((DataObjectHolder)holder).sharedFollow.setVisibility(View.GONE);

                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                        ((DataObjectHolder)holder).follow.setVisibility(View.GONE);
                    } else {
                        ((DataObjectHolder)holder).follow.setVisibility(View.VISIBLE);
                        if (posts.get(position) != null && posts.get(position).getFollowing() != null) {
                            if (posts.get(position).getFollowing()) {
                                ((DataObjectHolder) holder).follow.setTextColor(context.getResources().getColor(R.color.orange));
                                ((DataObjectHolder) holder).follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                ((DataObjectHolder) holder).follow.setText(context.getResources().getString(R.string.following));
                                ((DataObjectHolder) holder).follow.setEnabled(false);
                                //isFollow = false;
                            } else {
                                ((DataObjectHolder) holder).follow.setTextColor(context.getResources().getColor(R.color.blue));
                                ((DataObjectHolder) holder).follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                                ((DataObjectHolder) holder).follow.setText(context.getResources().getString(R.string.follow));
//                                Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                                ((DataObjectHolder) holder).follow.setEnabled(true);
                                //isFollow = true;

                                if(checkIsRequested(posts.get(position))){
                                    ((DataObjectHolder) holder).follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                    ((DataObjectHolder) holder).follow.setTextColor(context.getResources().getColor(R.color.colorAccent));
                                    ((DataObjectHolder) holder).follow.setText(R.string.requested);
                                    ((DataObjectHolder) holder).follow.setEnabled(false);
                                    //isFollow = false;
                                }

                            }
                        } else {
                            ((DataObjectHolder) holder).follow.setTextColor(context.getResources().getColor(R.color.blue));
                            ((DataObjectHolder) holder).follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                            ((DataObjectHolder) holder).follow.setText(context.getResources().getString(R.string.follow));
//                            Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                            ((DataObjectHolder) holder).follow.setEnabled(true);
                            //isFollow = true;
                        }
                    }

                    if ("video".equals(posts.get(position).getPostType())) {
                        Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + posts.get(position).getThumbnail())
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                .into(((DataObjectHolder)holder).thumbnail);
                    }
                }
                setPostData(posts.get(position),((DataObjectHolder)holder), false);
                setClickListener(((DataObjectHolder)holder));
                break;

        }
    }

    private void setUploadPostClickListener(RecyclerObjectHolder holder) {

        Glide.with(holder.myProfilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                .into(holder.myProfilePicture);

        holder.write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCreatePostClicked();
            }
        });
    }

    private boolean checkIsRequested(Post profileUser) {
        if(profileUser!=null)
        {
            boolean isRequestSend = false;
            if(profileUser.getRequestButtons().get(0).getButtonText().equalsIgnoreCase("Requested")){
                isRequestSend = true;
            }
            return isRequestSend;
        }
        return false;
    }

    private void setPopularPostsRecyclerView(RecyclerObjectHolder holder) {

        holder.listTitle.setText(R.string.popularPosts);
        holder.popularPosts.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        holder.popularPosts.setAdapter(adapter);
    }

    private void showPostingView(RecyclerObjectHolder holder) {

        holder.uploadProgress.setProgress(getUploadProgress());
    }

    public void setUploadProgress(Integer progress) {
        this.progress = progress;
    }

    private Integer getUploadProgress() {
        return this.progress;
    }

    public void setPopularPost(ArrayList<Post> popularPosts) {
        if (adapter!=null) {
            popularPostList.addAll(popularPosts);
            adapter.notifyDataSetChanged();
        }
    }

    private void setPostData(Post post, DataObjectHolder holder, Boolean isSharedPost) {

        if ("shared".equals(post.getPostType()) && post.getSharedPost()!=null) {

            setSharedPostData(post, holder);
            setPostData(post.getSharedPost(), holder, true);

        } else {

            if(post.getUser_name()!=null && !post.getUser_name().isEmpty()){
                holder.name.setText(post.getUser_name());
            }else {
                String fullName = post.getFirstName() + " " + post.getLastName();
                holder.name.setText(fullName);
            }

            if(post.getTagged_id()!=null && !post.getTagged_id().isEmpty()){
                holder.icTag.setVisibility(View.VISIBLE);
            }else {
                holder.icTag.setVisibility(View.GONE);
            }

            holder.time.setText(Utils.getSocialStyleTime(post.getPostDate()));

            if (post.getPostLatLng()!=null && !"".equals(post.getPostLatLng())) {
                holder.location.setVisibility(View.VISIBLE);
                holder.location.setText(post.getPostLocation());
            } else {
                holder.location.setVisibility(View.GONE);
            }

            if (!isSharedPost) {
                if (post.getLiked()) {
                    holder.like.setChecked(true);
                    holder.likesCount.setTextColor(Color.parseColor("#FF7400"));
                } else {
                    holder.like.setChecked(false);
                    holder.likesCount.setTextColor(Color.parseColor("#004F95"));
                }


                String likeCount = post.getLikeCount() + "";
                holder.likesCount.setText(likeCount);
                String commentCount = post.getCommentCount() + "";
                holder.comment.setText(commentCount);
            }


            if(post.getVideo_play_count()!=null){

                Logger.d("name_count",post.getFirstName()+"==="+post.getVideo_play_count());

                if(post.getVideo_play_count()==1){
                    holder.videoCount.setText(post.getVideo_play_count()+" "+"view");
                }else {
                    holder.videoCount.setText(post.getVideo_play_count()+" "+"views");
                }
            }


            holder.postText.addAutoLinkMode(AutoLinkMode.MODE_HASHTAG,AutoLinkMode.MODE_URL);
            holder.postText.setHashtagModeColor(ContextCompat.getColor(context, R.color.hashtag));
            holder.postText.setUrlModeColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.postText.setAutoLinkText(post.getPostText());

            holder.postText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    holder.postText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int linecount = holder.postText.getLineCount();

                    if(linecount > 4){
                        holder.more.setVisibility(View.VISIBLE);
                        holder.postText.setMaxLines(4);
                        holder.postText.setEllipsize(TextUtils.TruncateAt.END);
                        holder.more.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                holder.postText.setMaxLines(Integer.MAX_VALUE);
                                holder.more.setVisibility(View.GONE);
                                holder.less.setVisibility(View.VISIBLE);
                            }
                        });
                        holder.less.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.postText.setMaxLines(4);
                                holder.more.setVisibility(View.VISIBLE);
                                holder.less.setVisibility(View.GONE);
                            }
                        });
                    }else {
                        holder.more.setVisibility(View.GONE);
                        holder.less.setVisibility(View.GONE);
                    }

                }
            });


            //makeTextViewResizable(holder.postText,4,".. See More",true);
            holder.postText.setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
                @Override
                public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {
                    if(autoLinkMode == AutoLinkMode.MODE_HASHTAG){
                        Hashtag hashtag =new Hashtag();
                        if(matchedText.startsWith("#")){
                            matchedText =matchedText.substring(1);
                        }
                        hashtag.setHashtagSlug(matchedText);
                        context.startActivity(new Intent(context, HashtagPostsActivity.class).putExtra("hashtag",hashtag));
                    }else if(autoLinkMode == AutoLinkMode.MODE_URL){
                        try{
                            Uri uri = Uri.parse(matchedText);
                            Intent httpIntent = new Intent(Intent.ACTION_VIEW,uri);
                            context.getParent().startActivity(httpIntent);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
            });

            Glide.with(context)
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                    .into(holder.profilePicture);
            setPostDetails(post, holder);
        }
    }

    public void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {
        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    // tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                            viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if(maxLine == -1){
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });
    }

    private SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                     final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {

            ssb.setSpan(new MySpannable(false){
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "See Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 4, ".. See More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;
    }

    public class MySpannable extends ClickableSpan {

        private boolean isUnderline = true;

        /**
         * Constructor
         */
        public MySpannable(boolean isUnderline) {
            this.isUnderline = isUnderline;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setUnderlineText(isUnderline);
            ds.setColor(Color.parseColor("#1b76d3"));
        }

        @Override
        public void onClick(View widget) {


        }
    }

    private void setSharedPostData(Post post, DataObjectHolder holder) {

        String fullName = post.getFirstName() + " " + post.getLastName();
        holder.sharedName.setText(fullName);
        holder.sharedTime.setText(Utils.getSocialStyleTime(post.getPostDate()));
        holder.sharedPostText.setText(post.getPostText());

        if (post.getPostLatLng()!=null && !"".equals(post.getPostLatLng())) {
            holder.sharedLocation.setVisibility(View.VISIBLE);
            holder.sharedLocation.setText(post.getPostLocation());
        } else {
            holder.sharedLocation.setVisibility(View.GONE);
        }

        if (post.getLiked()) {
            holder.like.setChecked(true);
            holder.likesCount.setTextColor(Color.parseColor("#FF7400"));
        } else {
            holder.like.setChecked(false);
            holder.likesCount.setTextColor(Color.parseColor("#004F95"));
        }


        String likeCount = post.getLikeCount() + "";
        holder.likesCount.setText(likeCount);
        String commentCount = post.getCommentCount() + "";
        holder.comment.setText(commentCount);

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(holder.sharedProfileImage);
    }

    private void setPostDetails(Post post, DataObjectHolder holder) {

        switch (post.getPostType()) {

            case "text":
                holder.layoutOnlyImages.setVisibility(View.GONE);
                holder.videoLayout.setVisibility(View.GONE);

                if (post.getBackgroundImagePost()){

                    holder.layoutPostWithBackgroundImage.setVisibility(View.VISIBLE);

                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getBackgroundImage())
                            .into(holder.backgroundImage);

                    holder.postTextOnImage.setText(post.getPostText());
                    holder.postText.setText("");
                } else {
                    holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);
                }

                break;
            case "image":
                holder.layoutOnlyImages.setVisibility(View.VISIBLE);
                holder.videoLayout.setVisibility(View.GONE);
                holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.placeholder);

                if (post.getPostImage().size() > 1) {
                    String count = (post.getPostImage().size() - 1) + "";
                   // holder.postImage.setImageDrawable(context.getDrawable(R.drawable.placeholder));
                    RequestOptions options = new RequestOptions()
                            //.skipMemoryCache(true)
                            .apply(bitmapTransform(new BlurTransformation(25, 3)))
                            .diskCacheStrategy(DiskCacheStrategy.ALL);
                    Glide.with(context)
                            .asBitmap()
                            .thumbnail(0.70f)
                            .apply(options)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                   // holder.postImage.setImageDrawable(null);
                                    holder.postImage.setImageBitmap(BitmapUtils.getResizedBitmap(resource, holder.postImage.getMaxHeight()));
                                    RequestOptions options = new RequestOptions()
                                            //.skipMemoryCache(true)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL);
                                    Glide.with(context)
                                            .asBitmap()
                                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                            .apply(options)
                                            .into(new SimpleTarget<Bitmap>() {

                                                @Override
                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                    holder.postImage.setImageBitmap(resource);
                                                    /*handler.postAtTime(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            holder.postImage.setImageBitmap(resource);
                                                        }
                                                    },300);*/
                                                }
                                            });
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {

                                }
                            });

                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(1))
                            .into(holder.moreImages);

                    holder.mapPostImage.setVisibility(View.GONE);
                    holder.postImage.setVisibility(View.VISIBLE);
                    holder.moreImagesLayout.setVisibility(View.VISIBLE);
                    holder.imageCount.setText(count);
                } else {
                    if (post.getMapPost()) {
                        //holder.mapPostImage.setImageDrawable(context.getDrawable(R.drawable.placeholder));
                        holder.mapPostImage.setVisibility(View.VISIBLE);
                        holder.postImage.setVisibility(View.GONE);
                        RequestOptions options = new RequestOptions()
                                //.skipMemoryCache(true)
                                .apply(bitmapTransform(new BlurTransformation(25, 3)))
                                .diskCacheStrategy(DiskCacheStrategy.ALL);
                        Glide.with(context)
                                .asBitmap()
                                .thumbnail(0.70f)
                                .apply(options)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                .into(new CustomTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                        //holder.mapPostImage.setImageDrawable(null);
                                        holder.mapPostImage.setImageBitmap(BitmapUtils.getResizedBitmap(resource, holder.postImage.getMaxHeight()));
                                        RequestOptions options = new RequestOptions()
                                                //.skipMemoryCache(true)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL);
                                        Glide.with(context)
                                                .asBitmap()
                                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                                .apply(options)
                                                .into(new SimpleTarget<Bitmap>() {

                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        holder.mapPostImage.setImageBitmap(resource);
                                                       /* handler.postAtTime(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                holder.mapPostImage.setImageBitmap(resource);
                                                            }
                                                        },300);*/

                                                    }
                                                });
                                    }

                                    @Override
                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                    }
                                });

                    } else {

                        holder.mapPostImage.setVisibility(View.GONE);
                        holder.postImage.setVisibility(View.VISIBLE);
                        //holder.postImage.setImageDrawable(context.getDrawable(R.drawable.placeholder));
                        RequestOptions options = new RequestOptions()
                                //.skipMemoryCache(true)
                                .apply(bitmapTransform(new BlurTransformation(25, 3)))
                                .diskCacheStrategy(DiskCacheStrategy.ALL);
                        Glide.with(context)
                                .asBitmap()
                                .thumbnail(0.70f)
                                .apply(options)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                .into(new CustomTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                                        holder.postImage.setImageDrawable(null);
                                        holder.postImage.setImageBitmap(BitmapUtils.getResizedBitmap(resource, holder.postImage.getMaxHeight()));
                                        RequestOptions options = new RequestOptions()
                                                //.skipMemoryCache(true)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL);
                                        Glide.with(context)
                                                .asBitmap()
                                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                                .apply(options)
                                                .into(new SimpleTarget<Bitmap>() {

                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        holder.postImage.setImageBitmap(resource);
                                                        /*handler.postAtTime(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                holder.postImage.setImageBitmap(resource);
                                                            }
                                                        },300);*/
                                                    }
                                                });
                                    }

                                    @Override
                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                    }
                                });

                    }

                    holder.moreImagesLayout.setVisibility(View.GONE);
                }

                new Zoomy.Builder(context)
                        .target(holder.postImage)
                        .enableImmersiveMode(false)
                        .tapListener(v -> context.startActivity(new Intent(context, ViewPostImageActivity.class)
                                .putExtra("position",0)
                                .putExtra("images",post.getPostImage())))
                        .register();

                break;
            case "video":
                holder.layoutOnlyImages.setVisibility(View.GONE);
                holder.videoLayout.setVisibility(View.VISIBLE);
                holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);
                holder.bind(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo()),holder.getAdapterPosition(),post.getPostId());
                break;
            default:
                break;
        }
    }

    private void setClickListener(DataObjectHolder holder) {

        holder.icTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()) && posts.get(holder.getAdapterPosition()).getSharedPost()!=null) {
                    if(posts.get(holder.getAdapterPosition()).getSharedPost().getTagged_id()!=null && !posts.get(holder.getAdapterPosition()).getSharedPost().getTagged_id().isEmpty()){
                        listener.onTagClicked(posts.get(holder.getAdapterPosition()).getSharedPost().getTagged_id());
                    }
                }else {
                    if(posts.get(holder.getAdapterPosition()).getTagged_id()!=null && !posts.get(holder.getAdapterPosition()).getTagged_id().isEmpty()){
                        listener.onTagClicked(posts.get(holder.getAdapterPosition()).getTagged_id());
                    }
                }*/
            }
        });

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()) && posts.get(holder.getAdapterPosition()).getSharedPost()!=null){
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getSharedPost().getUserId()))
                        context.startActivity(new Intent(context, MyProfileActivity.class));
                    else
                        context.startActivity(new Intent(context, ProfileActivity.class)
                                .putExtra("userId",posts.get(holder.getAdapterPosition()).getSharedPost().getUserId()));
                } else {
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getUserId()))
                        context.startActivity(new Intent(context, MyProfileActivity.class));
                    else
                        context.startActivity(new Intent(context, ProfileActivity.class)
                                .putExtra("userId", posts.get(holder.getAdapterPosition()).getUserId()));
                }
            }
        });

        holder.sharedName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getUserId()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class)
                            .putExtra("userId", posts.get(holder.getAdapterPosition()).getUserId()));
            }
        });

        holder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCommentClicked(posts.get(holder.getAdapterPosition()),holder.getAdapterPosition());
            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivityForResult(new Intent(context, SharePostActivity.class).putExtra("post",posts.get(holder.getAdapterPosition())),100);
            }
        });

        holder.socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = UrlEndpoints.BASE_SHARE_POST_URL + posts.get(holder.getAdapterPosition()).getPostId();

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TITLE,"Afrocamgist");
                i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                i.putExtra(Intent.EXTRA_TEXT, url);
                context.startActivity(Intent.createChooser(i, "Share URL"));
            }
        });

        holder.report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ReportPostActivity.class).putExtra("postId",posts.get(holder.getAdapterPosition()).getPostId()));
            }
        });

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(holder.menu,holder.getAdapterPosition());
            }
        });

        holder.like.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && buttonView.isPressed()) {
                    holder.likesCount.setTextColor(Color.parseColor("#FF7400"));

                    int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                    posts.get(holder.getAdapterPosition()).setLikeCount(counter+1);

                    String likesCount = (Integer.parseInt(holder.likesCount.getText().toString()) + 1) + "";
                    holder.likesCount.setText(likesCount);
                } else {
                    if (Integer.parseInt(holder.likesCount.getText().toString().trim()) > 0){
                        holder.likesCount.setTextColor(Color.parseColor("#004F95"));

                        int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                        posts.get(holder.getAdapterPosition()).setLikeCount(counter-1);

                        String likesCount = (Integer.parseInt(holder.likesCount.getText().toString()) - 1) + "";
                        holder.likesCount.setText(likesCount);
                    }

                }
                if (buttonView.isPressed()) {
                    posts.get(holder.getAdapterPosition()).setLiked(isChecked);
                    listener.onPostChecked(posts.get(holder.getAdapterPosition()));
                }
            }
        });

        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isFollow = false;
                if(posts.get(holder.getAdapterPosition()) != null && posts.get(holder.getAdapterPosition()).getFollowing() != null){
                    if(posts.get(holder.getAdapterPosition()).getFollowing()) {
                        isFollow = false;
                    }else {
                        isFollow = true;
                        if(checkIsRequested(posts.get(holder.getAdapterPosition()))){
                            isFollow = false;
                        }
                    }
                }else {
                    isFollow = true;
                }



                if (isFollow) {
                    if(posts.get(holder.getAdapterPosition()).getPrivateAccount()) {
                        holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.follow.setText(context.getResources().getString(R.string.requested));
                        holder.follow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }else {
                        holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.follow.setText(context.getResources().getString(R.string.following));
                        holder.follow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }
                }
            }
        });

//        holder.follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked && buttonView.isPressed()) {
//                    holder.follow.setEnabled(false);
//                    listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
//                }
//            }
//        });





        holder.sharedFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //New Code for shared follow click
                boolean isFollow = false;
                if(posts.get(holder.getAdapterPosition()) != null && posts.get(holder.getAdapterPosition()).getFollowing() != null){
                    if(posts.get(holder.getAdapterPosition()).getFollowing()) {
                        isFollow = false;
                    }else {
                        isFollow = true;
                        if(checkIsRequested(posts.get(holder.getAdapterPosition()))){
                            isFollow = false;
                        }
                    }
                }else {
                    isFollow = true;
                }

                if (isFollow) {
                    if(posts.get(holder.getAdapterPosition()).getPrivateAccount()){
                        holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.sharedFollow.setText(context.getResources().getString(R.string.requested));
                        holder.sharedFollow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }else {
                        holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.sharedFollow.setText(context.getResources().getString(R.string.following));
                        holder.sharedFollow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }
                }
            }
        });


        /*holder.sharedFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && buttonView.isPressed()) {
                    holder.sharedFollow.setEnabled(false);
                    listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                }
            }
        });*/

        holder.likesCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!"0".equals(holder.likesCount.getText().toString())) {
                    context.startActivity(new Intent(context, PostLikedByActivity.class)
                            .putExtra("postId", posts.get(holder.getAdapterPosition()).getPostId()));
                }
            }
        });

        holder.moreImagesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> images;

                if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()) && posts.get(holder.getAdapterPosition()).getSharedPost()!=null)
                    images = posts.get(holder.getAdapterPosition()).getSharedPost().getPostImage();
                else
                    images = posts.get(holder.getAdapterPosition()).getPostImage();

                context.startActivity(new Intent(context, ViewPostImageActivity.class)
                        .putExtra("position",1)
                        .putExtra("images",images));
            }
        });
    }

    private void showMenu(View menu, int position) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int popupWidth = 450;
        int popupHeight = LinearLayout.LayoutParams.WRAP_CONTENT;

        LinearLayout viewGroup = context.findViewById(R.id.menu_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.drop_down_menu, viewGroup);
        popupMenu(layout, position);

        popup = new PopupWindow(layout, popupWidth, popupHeight);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        if (position == (posts.size()-1))
            popup.showAsDropDown(menu,0,-300, Gravity.TOP);
        else
            popup.showAsDropDown(menu);
    }

    private void popupMenu(View layout, int position) {

        RecyclerView menu = layout.findViewById(R.id.menuList);
        menu.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        menu.setLayoutManager(mLayoutManager);
        MenuAdapter adapter;
        if (!LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_other_menu))), position, this);
        } else {
            if (posts.get(position).getMapPost())
                adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_map_menu))), position, this);
            else
                adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_menu))), position, this);
        }
        menu.setAdapter(adapter);
    }

    @Override
    public void onMenuItemClicked(String item, int postPosition) {

        if (popup!=null && popup.isShowing())
            popup.dismiss();

        switch (item.toLowerCase()) {

            case "edit":
                listener.onEditPost(posts.get(postPosition), postPosition);
                break;
            case "delete":
                listener.onDeletePost(posts.get(postPosition));
                break;
            case "hide":
                listener.onHidePost(posts.get(postPosition));
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (posts ==null)
            return 0;
        else
            return posts.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (posts.get(position)!=null) {
            if ((position-1) > 0 && ((position-1)%5 == 0)){
                if (posts.get(position).getFirstName()!=null)
                    posts.add(position,new Post());
                return POPULAR_POSTS;
            } else if (posts.get(1)==null) {
                if (position == 3) {
                    return POPULAR_POSTS;
                } else {
                    switch (position) {

                        case 0:
                            return UPLOAD_POST;
                        case 2:
                            return POST;
                        default:
                            return POST;
                    }
                }
            } else {
                switch (position) {

                    case 0:
                        return UPLOAD_POST;
                    case 2:
                        return POPULAR_POSTS;
                    default:
                        return POST;
                }
            }
        } else {
            return POSTING;
        }
    }

    static class RecyclerObjectHolder extends RecyclerView.ViewHolder {

        ImageView postImage, moreImages, share, menu,thumbnail, backgroundImage,socialShare,report, mapPostImage;
        /*PhotoView postImage;*/
        CircleImageView profilePicture, sharedProfileImage, myProfilePicture;
        RelativeLayout /*layoutOnlyImages,*/ videoLayout, moreImagesLayout;
        FrameLayout layoutOnlyImages, thumbnailLayout;
        LinearLayout sharedLayout;
        RelativeLayout layoutPostWithBackgroundImage;
        TextView follow;
        ToggleButton like, sharedFollow;
        TextView name,time, postText, imageCount, likesCount, comment, location,
                sharedName, sharedLocation, sharedTime, sharedPostText,write, listTitle, postTextOnImage;
        RecyclerView popularPosts;
        Uri mediaUri;
        RoundedHorizontalProgressBar uploadProgress;

        RecyclerObjectHolder(View itemView){
            super(itemView);
            moreImages = itemView.findViewById(R.id.more_images);
            profilePicture = itemView.findViewById(R.id.profile_picture);
            myProfilePicture = itemView.findViewById(R.id.profile_image);
            sharedProfileImage = itemView.findViewById(R.id.shared_profile_image);
            share = itemView.findViewById(R.id.share);
            menu = itemView.findViewById(R.id.menu);
            layoutOnlyImages = itemView.findViewById(R.id.layout_only_images);
            moreImagesLayout = itemView.findViewById(R.id.more_images_layout);
            sharedLayout = itemView.findViewById(R.id.shared_layout);
            videoLayout = itemView.findViewById(R.id.video_layout);
            name = itemView.findViewById(R.id.name);
            time = itemView.findViewById(R.id.time);
            postText = itemView.findViewById(R.id.post_text);
            sharedName = itemView.findViewById(R.id.shared_name);
            sharedLocation = itemView.findViewById(R.id.shared_location);
            sharedTime = itemView.findViewById(R.id.shared_time);
            sharedPostText = itemView.findViewById(R.id.shared_post_text);
            postImage = itemView.findViewById(R.id.post_image);
            imageCount = itemView.findViewById(R.id.image_count);
            likesCount = itemView.findViewById(R.id.likes_count);
            comment = itemView.findViewById(R.id.comment);
            location = itemView.findViewById(R.id.location);
            like = itemView.findViewById(R.id.like);
            follow = itemView.findViewById(R.id.follow);
            sharedFollow = itemView.findViewById(R.id.follow_shared);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            write = itemView.findViewById(R.id.write);
            listTitle = itemView.findViewById(R.id.list_title);
            popularPosts = itemView.findViewById(R.id.horizontal_list);
            backgroundImage = itemView.findViewById(R.id.background_image);
            postTextOnImage = itemView.findViewById(R.id.post_text_on_image);
            layoutPostWithBackgroundImage = itemView.findViewById(R.id.text_with_image_background);
            socialShare = itemView.findViewById(R.id.social_share);
            report = itemView.findViewById(R.id.report);
            thumbnailLayout = itemView.findViewById(R.id.thumbnail_layout);
            mapPostImage = itemView.findViewById(R.id.post_map_image);
            uploadProgress = itemView.findViewById(R.id.upload_progress);
        }

        void bind(Uri media) {
            this.mediaUri = media;
        }
    }

//    public static class DataObjectHolder extends RecyclerObjectHolder implements ToroPlayer {
//
//        PlayerView playerView;
//        ExoPlayerViewHelper helper;
//        Uri mediaUri;
//        ToggleButton volume;
//
//        DataObjectHolder(View itemView) {
//            super(itemView);
//            playerView = itemView.findViewById(R.id.player);
//            volume = itemView.findViewById(R.id.volume);
//        }
//
//        @NonNull
//        @Override
//        public View getPlayerView() {
//            return playerView;
//        }
//
//        @NonNull
//        @Override
//        public PlaybackInfo getCurrentPlaybackInfo() {
//            return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
//        }
//
//        @Override
//        public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {
//            thumbnailLayout.setVisibility(View.VISIBLE);
//            if (helper == null) {
//                helper = new ExoPlayerViewHelper(this, mediaUri);
//            }
//            helper.initialize(container,playbackInfo);
//        }
//
//        @Override
//        public void play() {
//            if (helper != null) {
//                helper.play();
//                isTalentPlay = true;
//                playerView.getPlayer().setRepeatMode(Player.REPEAT_MODE_ALL);
//                if (isMute) {
//                    mute();
//                    volume.setChecked(false);
//                } else {
//                    unMute();
//                    volume.setChecked(true);
//                }
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        thumbnailLayout.setVisibility(View.GONE);
//                    }
//                },1500);
//            }
//        }
//
//        @Override
//        public void pause() {
//            if (helper != null) helper.pause();
//        }
//
//        @Override
//        public boolean isPlaying() {
//            return helper != null && helper.isPlaying();
//        }
//
//        @Override
//        public void release() {
//            if (helper != null) {
//                thumbnailLayout.setVisibility(View.VISIBLE);
//                helper.release();
//                helper = null;
//            }
//        }
//
//        @Override
//        public boolean wantsToPlay() {
//            return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85;
//        }
//
//        @Override
//        public int getPlayerOrder() {
//            return getAdapterPosition();
//        }
//
//        @SuppressLint("ClickableViewAccessibility")
//        void bind(Uri media) {
//            this.mediaUri = media;
//            this.volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked)
//                        unMute();
//                    else
//                        mute();
//                }
//            });
//        }
//
//        private void mute() {
//            isMute = true;
//            this.setVolume(0);
//        }
//
//        private void unMute() {
//            isMute = false;
//            this.setVolume(100);
//        }
//
//        private void setVolume(int amount) {
//            final int max = 100;
//            final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
//            final float volume = (float) (1 - (numerator / Math.log(max)));
//
//            this.helper.setVolume(volume);
//        }
//    }

   /* public void playvideo(Container posts) {
        try{
            LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
            int lastposition = layoutManager.findLastVisibleItemPosition();
            RecyclerView.ViewHolder viewHolder = posts.findViewHolderForLayoutPosition(lastposition);
            if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
//            if (viewHolder.getItemViewType() != UPLOAD_POST &&
//                    viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
//                    viewHolder.getItemViewType() != ADVERTISEMENT &&
//                    viewHolder.getItemViewType() != POPULAR_POSTS &&
//                    viewHolder.getItemViewType() != POSTING) {
                if (((DataObjectHolder) viewHolder).helper != null) {
                    if (viewHolder.getItemViewType() != UPLOAD_POST &&
                            viewHolder.getItemViewType() != POPULAR_POSTS &&
                            viewHolder.getItemViewType() != POSTING) {
                        if (!((DataObjectHolder) viewHolder).helper.isPlaying()) {
                            ((DataObjectHolder) viewHolder).helper.play();
                        }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                    }
                }else {
                    viewHolder = posts.findViewHolderForLayoutPosition(lastposition-1);
                    if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                        Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
                        if (viewHolder.getItemViewType() != UPLOAD_POST &&
                                viewHolder.getItemViewType() != POPULAR_POSTS &&
                                viewHolder.getItemViewType() != POSTING) {
                            if (((DataObjectHolder) viewHolder).helper != null) {
                                if (!((DataObjectHolder) viewHolder).helper.isPlaying()) {
                                    ((DataObjectHolder) viewHolder).helper.play();
                                }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                            }
                        }
//            }
                    }
                }
//            }
            }else{
                viewHolder = posts.findViewHolderForLayoutPosition(lastposition-1);
                if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                    Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
                    if (viewHolder.getItemViewType() != UPLOAD_POST &&
                            viewHolder.getItemViewType() != POPULAR_POSTS &&
                            viewHolder.getItemViewType() != POSTING) {
                        if (((DataObjectHolder) viewHolder).helper != null) {
                            if (!((DataObjectHolder) viewHolder).helper.isPlaying()) {
                                ((DataObjectHolder) viewHolder).helper.play();
                            }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                        }
                    }
//            }
                }
            }
        }catch (Exception e){

        }

    }

    public void pausevideo(Container posts) {
        try {
            LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
            int lastposition = layoutManager.findLastVisibleItemPosition();
            RecyclerView.ViewHolder viewHolder = posts.findViewHolderForLayoutPosition(lastposition);

            if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));

                if (((DataObjectHolder) viewHolder).helper != null) {
                    if (viewHolder.getItemViewType() != UPLOAD_POST &&
                            viewHolder.getItemViewType() != POPULAR_POSTS &&
                            viewHolder.getItemViewType() != POSTING) {
                        if (((DataObjectHolder) viewHolder).helper.isPlaying()) {
                            ((DataObjectHolder) viewHolder).helper.pause();
                        }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                    }
                }else {
                    viewHolder = posts.findViewHolderForLayoutPosition(lastposition-1);
                    if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                        Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
                        if (viewHolder.getItemViewType() != UPLOAD_POST &&
                                viewHolder.getItemViewType() != POPULAR_POSTS &&
                                viewHolder.getItemViewType() != POSTING) {
                            if (((DataObjectHolder) viewHolder).helper != null) {
                                if (((DataObjectHolder) viewHolder).helper.isPlaying()) {
                                    ((DataObjectHolder) viewHolder).helper.pause();
                                }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                            }
                        }
//            }
                    }
                }
            }else {
                viewHolder = posts.findViewHolderForLayoutPosition(lastposition-1);
                if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                    Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
                    if (viewHolder.getItemViewType() != UPLOAD_POST &&
                            viewHolder.getItemViewType() != POPULAR_POSTS &&
                            viewHolder.getItemViewType() != POSTING) {
                        if (((DataObjectHolder) viewHolder).helper != null) {
                            if (((DataObjectHolder) viewHolder).helper.isPlaying()) {
                                ((DataObjectHolder) viewHolder).helper.pause();
                            }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                        }
                    }
//            }
                }
            }
        }catch (Exception e){

        }

    }*/

    public void pausevideo(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder != null && viewHolder instanceof DataObjectHolder) {
            Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
            if (viewHolder.getItemViewType() != UPLOAD_POST &&
                    viewHolder.getItemViewType() != POPULAR_POSTS &&
                    viewHolder.getItemViewType() != POSTING) {
                if (((DataObjectHolder) viewHolder).helper != null) {
                    if (((DataObjectHolder) viewHolder).helper.isPlaying()) {
                        ((DataObjectHolder) viewHolder).helper.pause();
                    } else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }
                }
//            }
            }
        }
    }

    public void playvideo(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder != null && viewHolder instanceof DataObjectHolder) {
            Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
            if (viewHolder.getItemViewType() != UPLOAD_POST &&
                    viewHolder.getItemViewType() != POPULAR_POSTS &&
                    viewHolder.getItemViewType() != POSTING) {
                if (((DataObjectHolder) viewHolder).helper != null) {
                    if (!((DataObjectHolder) viewHolder).helper.isPlaying()) {
                        ((DataObjectHolder) viewHolder).helper.play();
                    } /*else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*/
                }
//            }
            }
        }
    }
    /*public interface OnPostClickListener {
        void onPostChecked(Post post);
        void onEditPost(Post post, int position);
        void onDeletePost(Post post);
        void onHidePost(Post post);
        void onCommentClicked(Post post, int position);
        void onCreatePostClicked();
        void onFollowClicked(Post post);
    }*/

}
