package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.StoryPopularPostActivity;
import com.TBI.afrocamgist.activity.ViewPopularPostsActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.Post;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AfroPopularPostAdapter extends RecyclerView.Adapter<AfroPopularPostAdapter.DataObjectHolder> {

    private Activity context;
    private ArrayList<Post> popularPosts;

    public AfroPopularPostAdapter(Activity context, ArrayList<Post> popularPosts) {
        this.context = context;
        this.popularPosts = popularPosts;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_popular_post, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        Post post = popularPosts.get(position);

        Glide.with(holder.profilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(holder.profilePicture);

        String fullName = post.getFirstName() + " " + post.getLastName();
        holder.name.setText(fullName);

        holder.postText.setText(post.getPostText());

        String likesCount = post.getLikeCount() + "";
        holder.likes.setText(likesCount);

        String commentCount = post.getCommentCount() + "";
        holder.comment.setText(commentCount);

        if ("text".equals(post.getPostType())) {
            holder.image.setVisibility(View.GONE);
            holder.postImage.setVisibility(View.GONE);
            holder.play.setVisibility(View.GONE);
            holder.postText.setLines(5);
        } else if ("image".equals(post.getPostType())) {

            holder.image.setVisibility(View.VISIBLE);
            holder.postText.setLines(1);
            holder.play.setVisibility(View.GONE);

            Glide.with(holder.postImage)
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                    .into(holder.postImage);
        } else {

            holder.image.setVisibility(View.VISIBLE);
            holder.postText.setLines(1);
            holder.play.setVisibility(View.VISIBLE);
            Glide.with(holder.postImage)
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail())
                    .into(holder.postImage);
        }

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(post.getPostedBy()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class)
                            .putExtra("userId", post.getPostedBy()));
            }
        });

        holder.post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                context.startActivity(new Intent(context, ViewPostActivity.class).putExtra("postId",post.getPostId()));

                //Open all popular post activty
                /*context.startActivity(new Intent(context, ViewPopularPostsActivity.class)
                        .putExtra("posts",popularPosts)
                        .putExtra("position",holder.getAdapterPosition()));*/

                //Open single Story line post activty
                /*context.startActivity(new Intent(context, StorySinglePostActivity.class)
                        .putExtra("posts",popularPosts)
                        .putExtra("position",holder.getAdapterPosition()));*/

                //Open all story popular post activity
                context.startActivity(new Intent(context, StoryPopularPostActivity.class)
                        .putExtra("posts",popularPosts)
                        .putExtra("postPosition",holder.getAdapterPosition()));


            }
        });
    }

    @Override
    public int getItemCount() {
        if (popularPosts==null)
            return 0;
        else
            return popularPosts.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {

        CircleImageView profilePicture;
        ImageView postImage, play;
        TextView name, postText, likes, comment;
        ToggleButton like;
        LinearLayout post;
        RelativeLayout image;

        DataObjectHolder(View itemView) {
            super(itemView);
            profilePicture = itemView.findViewById(R.id.profile_picture);
            postImage = itemView.findViewById(R.id.post_image);
            play = itemView.findViewById(R.id.play);
            name = itemView.findViewById(R.id.name);
            postText = itemView.findViewById(R.id.post_text);
            likes = itemView.findViewById(R.id.likes_count);
            comment = itemView.findViewById(R.id.comment);
            like = itemView.findViewById(R.id.like);
            post = itemView.findViewById(R.id.post);
            image = itemView.findViewById(R.id.image);
        }
    }
}
