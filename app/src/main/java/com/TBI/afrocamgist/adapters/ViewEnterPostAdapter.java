package com.TBI.afrocamgist.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.app.AfrocamgistApplication;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.DoubleClickListener;
import com.TBI.afrocamgist.model.post.Post;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;

import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;

import static com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter.isMute;

public class ViewEnterPostAdapter extends RecyclerView.Adapter<ViewEnterPostAdapter.RecyclerObjectHolder>  {

    private LayoutInflater inflater;
    private ArrayList<Post> posts = new ArrayList<>();
    //ExoPlayerViewHelper helper;
    public SimpleExoPlayer helper;
    public OnEntertainmentPostClickListener listener;


    Activity context;
    int getAdapterPosition = 0;

    public ViewEnterPostAdapter(Activity context, ArrayList<Post> posts, OnEntertainmentPostClickListener listener) {
        this.context = context;
        this.posts = posts;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        view = inflater.inflate(R.layout.item_image, parent, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerObjectHolder holder, int position) {
        Post post = posts.get(position);

        switch (post.getPostType()) {

            case "image":

                if(post.getMapPost()){
                    holder.image.setScaleType(ImageView.ScaleType.FIT_CENTER);
                }else {
                    holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }

                holder.image.setVisibility(View.VISIBLE);
                holder.videoLayout.setVisibility(View.GONE);
                holder.videoLayout.setTag(post.getPostType());
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.placeholder);
                try {
                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .into(holder.image);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                holder.setClickListener(context,helper,listener);

                break;
            case "video":
                Logger.e("LLL_URL: ",UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo());
                holder.image.setVisibility(View.GONE);
                holder.videoLayout.setVisibility(View.VISIBLE);
                holder.videoLayout.setTag(post.getPostType());

                Glide.with(context)
                        .load(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail())
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .into(holder.backgroundThumbnail);

                holder.bind(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo()));

                holder.setClickListener(context,helper,listener);

                break;
            default:
                break;
        }

    }

    @Override
    public int getItemCount() {
        if (posts == null)
            return 0;
        else
            return posts.size();
    }

    public void setNotigy(int pos,Post post){
        posts.set(pos,post);
        notifyDataSetChanged();
    }

    static class RecyclerObjectHolder extends RecyclerView.ViewHolder  {
        ImageView image,backgroundThumbnail;
        RelativeLayout videoLayout,relImage;
        Uri mediaUri;
        FrameLayout thumbnailLayout;

        public RecyclerObjectHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            //relImage = itemView.findViewById(R.id.relImage);
            videoLayout = itemView.findViewById(R.id.video_layout);
            thumbnailLayout = itemView.findViewById(R.id.thumbnail_layout);
            backgroundThumbnail = itemView.findViewById(R.id.backgroundThumbnail);
        }

        @SuppressLint("ClickableViewAccessibility")
        void bind(Uri media) {
            this.mediaUri = media;
        }

        void setClickListener(Activity context, SimpleExoPlayer helper, OnEntertainmentPostClickListener listener){

            final GestureDetector gd = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDown(MotionEvent e) {
                    return true;
                }

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    if(helper!=null){
                        /*if(helper.isPlaying()){
                            helper.pause();
                        }else {
                            helper.play();
                        }*/
                        if (helper.getPlayWhenReady()) {
                            helper.setPlayWhenReady(false);
                        } else {
                            helper.setPlayWhenReady(true);
                        }

                    }
                    return true;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    listener.onPostChecked();
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    return true;
                }
            });

            videoLayout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gd.onTouchEvent(event);
                }
            });

            image.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gd.onTouchEvent(event);
                }
            });

        }

    }

    class DataObjectHolder extends RecyclerObjectHolder implements ToroPlayer {

        PlayerView playerView;
        Uri mediaUri;
        ToggleButton volume;
        //Precache videos
        CacheDataSourceFactory cacheDataSourceFactory = null;
        SimpleCache simpleCache = null;

        DataObjectHolder(View itemView) {
            super(itemView);
                playerView = itemView.findViewById(R.id.player);

            volume = itemView.findViewById(R.id.volume);
            volume.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked)
                    unMute();
                else
                    mute();
            });

            simpleCache = AfrocamgistApplication.simpleCache;
            String userAgent = Util.getUserAgent(itemView.getContext(), "Afrocamgist");
            cacheDataSourceFactory =
                    new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(userAgent), CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
        }

        @NonNull
        @Override
        public View getPlayerView() {
            return playerView;
        }

        @NonNull
        @Override
        public PlaybackInfo getCurrentPlaybackInfo() {
            //return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
            return new PlaybackInfo();
        }

        @Override
        public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {
            thumbnailLayout.setVisibility(View.VISIBLE);
            String type="";
            if((videoLayout.getTag()!=null)){
                type=(String)videoLayout.getTag();
            }
            if(type.equals("video")|| type.equals("")) {
                if (helper == null) {
                    //helper = new ExoPlayerViewHelper(this, mediaUri);
                    Log.d("initialize9", "intialize...");
                    helper = newSimpleExoPlayer(context);
                }
                //helper.initialize(container, playbackInfo);
            }
        }

        @Override
        public void play() {
            if (helper != null) {
                //Logger.e("LLLLLL_Play: ", String.valueOf(true));
                //helper.play();

                MediaSource mediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaUri);
                playerView.setPlayer(helper);
                //Turn On/Off auto play videos
                if (Identity.getAutoPlayVideo(context)) {
                    helper.setPlayWhenReady(true);
                } else {
                    helper.setPlayWhenReady(false);
                }
                helper.seekTo(0);
                helper.prepare(mediaSource, true, false);


                if(playerView.getPlayer()!=null)
                    playerView.getPlayer().setRepeatMode(Player.REPEAT_MODE_ALL);

                playerView.getPlayer().addListener(new Player.DefaultEventListener() {
                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                        super.onPlayerStateChanged(playWhenReady, playbackState);
                        if (playWhenReady && playbackState == Player.STATE_READY) {
                            // media actually playing
                            backgroundThumbnail.setVisibility(View.GONE);
                            thumbnailLayout.setVisibility(View.GONE);

                        } else if (playWhenReady) {
                            // might be idle (plays after prepare()),
                            // buffering (plays when data available)
                            // or ended (plays when seek away from end)
                        } else {
                            // simpleExoPlayer paused in any state
                            backgroundThumbnail.setVisibility(View.VISIBLE);
                            thumbnailLayout.setVisibility(View.VISIBLE);
                        }

                        if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED || !playWhenReady) {
                            playerView.setKeepScreenOn(false);
                        } else { // STATE_READY, STATE_BUFFERING
                            // This prevents the screen from getting dim/lock
                            playerView.setKeepScreenOn(true);
                        }

                    }
                });

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (Identity.getAutoPlayVideo(context)) {
                            backgroundThumbnail.setVisibility(View.GONE);
                            thumbnailLayout.setVisibility(View.GONE);
                        } else {
                            backgroundThumbnail.setVisibility(View.VISIBLE);
                            thumbnailLayout.setVisibility(View.VISIBLE);
                        }

                    }
                }, 500);

                //for mute and unmute
                if (isMute) {
                    mute();
                    volume.setChecked(false);
                } else {
                    unMute();
                    volume.setChecked(true);
                }

                //new Handler().postDelayed(() -> thumbnailLayout.setVisibility(View.GONE), 1000);
            } else {
                Logger.e("LLLLLL_Play: ", String.valueOf(false));
            }
        }

        @Override
        public void pause() {
            //if (helper != null) helper.pause();
            if (helper != null) {
                helper.setPlayWhenReady(false);
            }
        }

        @Override
        public boolean isPlaying() {
            //return helper != null && helper.isPlaying();
            return helper != null && helper.getPlayWhenReady();
        }

        @Override
        public void release() {
            if (helper != null) {
                thumbnailLayout.setVisibility(View.VISIBLE);
                helper.release();
                helper = null;
            }
        }

        @Override
        public boolean wantsToPlay() {
            return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85;
        }

        @Override
        public int getPlayerOrder() {
            return getAdapterPosition();
        }

        @SuppressLint("ClickableViewAccessibility")
        void bind(Uri media) {
            this.mediaUri = media;
            this.volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        unMute();
                    else
                        mute();
                }
            });
        }

        private void mute() {
            isMute = true;
            this.setVolume(0);
        }

        private void unMute() {
            isMute = false;
            this.setVolume(100);
        }

        private void setVolume(int amount) {
            final int max = 100;
            final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
            final float volume = (float) (1 - (numerator / Math.log(max)));
            if (helper != null) {
                helper.setVolume(volume);
            }
        }

        private SimpleExoPlayer newSimpleExoPlayer(Context context) {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            LoadControl loadControl = new DefaultLoadControl();
            return ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl);
        }

    }

    public void notifydata(ArrayList<Post> posts ){
        this.posts= posts;
        notifyDataSetChanged();
    }

    public interface OnEntertainmentPostClickListener{
        void onPostChecked();
    }

}