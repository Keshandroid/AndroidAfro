package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.ChatActivity;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.SocketChatActivity;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.user.User;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.DataObjectHolder> {

    private Activity context;
    private ArrayList<Follow> followings;
    private String from = "";
    User user;

    public FollowingAdapter(Activity context, ArrayList<Follow> followings,String from) {
        this.context = context;
        this.followings = followings;
        this.from = from;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_following, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        holder.follow.setVisibility(View.GONE);
        setFollower(holder);

    }

    private void setFollower(DataObjectHolder holder) {

        Follow follower = followings.get(holder.getAdapterPosition());

        User user = new User();


        String fullName = follower.getFirstName() + " " + follower.getLastName();
        holder.name.setText(fullName);

        String followers;
        if (follower.getFollowerIds()!=null)
            followers = follower.getFollowerIds().size() + " followers";
        else
            followers = "0 followers";

        holder.followers.setText(followers);

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + follower.getProfileImageUrl())
                .into(holder.profileImage);

        holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (from.equals("Profile")) {
                    if (LocalStorage.getUserDetails().getUserId().equals(follower.getUserId()))
                        context.startActivity(new Intent(context, MyProfileActivity.class));
                    else
                        context.startActivity(new Intent(context, ProfileActivity.class).putExtra("userId", follower.getUserId()));
                } else {
                    getUserProfile(follower.getUserId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (followings == null)
            return 0;
        else
            return followings.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name, followers;
        CircleImageView profileImage;
        ToggleButton follow;
        CardView profile;

        DataObjectHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            followers = itemView.findViewById(R.id.followers);
            profileImage = itemView.findViewById(R.id.profile_image);
            follow = itemView.findViewById(R.id.follow);
            profile = itemView.findViewById(R.id.profile);
        }
    }

    private void getUserProfile(int id) {
        user = new User();
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
        String url = UrlEndpoints.PROFILE + "/" + id;
        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    if (!object.has(Constants.MESSAGE)) {
                        user = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), User.class);
                        //KK
//                        context.startActivity(new Intent(context, ChatActivity.class).putExtra("user",user));
                        context.startActivity(new Intent(context, SocketChatActivity.class).putExtra("user",user));

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
