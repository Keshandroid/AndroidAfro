package com.TBI.afrocamgist.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.advert.Advert;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AdvertListAdapter extends RecyclerView.Adapter<AdvertListAdapter.DataObjectHolder> {

    private static final int ENTERTAINMENT = 0, LOADING = 1;
    private ArrayList<Advert> advertPosts;
    private OnAdvertPostClickListener listener;
    private Context context;


    public AdvertListAdapter(ArrayList<Advert> advertPosts, OnAdvertPostClickListener listener, Context context) {
        this.advertPosts = advertPosts;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view;

        switch (viewType) {

            case LOADING:
                view = inflater.inflate(R.layout.item_loading, viewGroup, false);
                return new DataObjectHolder(view);
            default:
                view = inflater.inflate(R.layout.item_advert_detail, viewGroup, false);
                return new DataObjectHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        switch (getItemViewType(position)) {

            case LOADING:
                showLoadingView(holder);
                break;
            default:
                setEntertainmentView(holder);
                break;
        }
    }

    private void showLoadingView(DataObjectHolder holder) {
    }

    private void setEntertainmentView(DataObjectHolder holder) {

        if (advertPosts.size() != 0) {

            Advert advertPost = advertPosts.get(holder.getAdapterPosition());

            if ("image".equals(advertPost.getPosts().get(0).getPostType())) {
                if (advertPost.getPosts().get(0).getPostImage().size() != 0) {
                    Glide.with(holder.postImage)
                            .load(UrlEndpoints.MEDIA_BASE_URL + advertPost.getPosts().get(0).getPostImage().get(0))
                            .into(holder.postImage);
                }
                holder.vid_image.setVisibility(View.GONE);
            } else if ("video".equals(advertPost.getPosts().get(0).getPostType())) {

                Glide.with(holder.postImage)
                        .load(UrlEndpoints.MEDIA_BASE_URL + advertPost.getPosts().get(0).getThumbnail())
                        .into(holder.postImage);
                holder.vid_image.setVisibility(View.VISIBLE);
            }


            holder.postImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener != null) {
                        listener.onPostImageClicked(advertPost);
                    }
                }
            });

            holder.txtStatus.setText(advertPost.getStatus());

            if(advertPost.getStatus().equalsIgnoreCase("active")){
                holder.statusImg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_active));
            }else if (advertPost.getStatus().equalsIgnoreCase("inactive")){
                holder.statusImg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_inactive));
            }else if(advertPost.getStatus().equalsIgnoreCase("review")){
                holder.statusImg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_in_review));
            }

        }
    }

    @Override
    public int getItemCount() {
        if (advertPosts == null)
            return 0;
        else
            return advertPosts.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (advertPosts.get(position) == null)
            return position == advertPosts.size() - 1 ? LOADING : ENTERTAINMENT;
        else
            return ENTERTAINMENT;
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        ImageView postImage, share, vid_image,statusImg;
        TextView likes, comments,txtStatus;

        DataObjectHolder(View itemView) {
            super(itemView);
            postImage = itemView.findViewById(R.id.post_image);
            share = itemView.findViewById(R.id.share);
            vid_image = itemView.findViewById(R.id.vid_image);
            likes = itemView.findViewById(R.id.likes);
            comments = itemView.findViewById(R.id.comments);
            statusImg = itemView.findViewById(R.id.statusImg);
            txtStatus = itemView.findViewById(R.id.txtStatus);

        }
    }

    public class ViewHolder extends BaseViewHolder {
        ImageView postImage, vid_image, statusImg;
        TextView txtStatus;

        ViewHolder(View itemView) {
            super(itemView);
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            postImage = itemView.findViewById(R.id.post_image);
            vid_image = itemView.findViewById(R.id.vid_image);
            statusImg = itemView.findViewById(R.id.statusImg);
            txtStatus = itemView.findViewById(R.id.txtStatus);

//            Logger.e("LLLL_EnterSie: ", String.valueOf(entertainmentPosts.size()));
            if (advertPosts.size() != 0) {

                Advert advertPost = advertPosts.get(getAdapterPosition());

                if ("image".equals(advertPost.getPosts().get(0).getPostType())) {
                    if (advertPost.getPosts().get(0).getPostImage().size() != 0) {
                        Glide.with(postImage)
                                .load(UrlEndpoints.MEDIA_BASE_URL + advertPost.getPosts().get(0).getPostImage().get(0))
                                .into(postImage);
                    }
                    vid_image.setVisibility(View.GONE);
                } else if ("video".equals(advertPost.getPosts().get(0).getPostType())) {
                    if (advertPost.getPosts().get(0).getPostImage().size() != 0) {
                        Glide.with(postImage)
//                            .load(UrlEndpoints.MEDIA_BASE_URL + advertPost.getThumbnail())
                                .load(UrlEndpoints.MEDIA_BASE_URL + advertPost.getPosts().get(0).getPostImage().get(0))
                                .into(postImage);
                        vid_image.setVisibility(View.VISIBLE);
                    }
                }


                postImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onPostImageClicked(advertPost);
                        }
                    }
                });

            }
        }
    }


    public Advert getItem(int position) {
        return advertPosts.get(position);
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public interface OnAdvertPostClickListener {
        void onViewPostClicked(Advert post);

        void onPostImageClicked(Advert advertPost);
    }
}

