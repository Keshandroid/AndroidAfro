package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ViewPostImageAdapter extends RecyclerView.Adapter<ViewPostImageAdapter.DataObjectHolder> {

    private ArrayList<String> images;

    public ViewPostImageAdapter(ArrayList<String> images) {
        this.images = images;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_post_image, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        Glide.with(holder.image)
                .load(UrlEndpoints.MEDIA_BASE_URL + images.get(position))
                .into(holder.image);

        holder.remove.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        if (images ==null)
            return 0;
        else
            return images.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        ImageView image;
        ImageView remove;

        DataObjectHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            remove = itemView.findViewById(R.id.remove);
        }
    }
}
