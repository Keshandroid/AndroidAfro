package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.SettingsActivity;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;

public class AfroInterestAdapter extends RecyclerView.Adapter<AfroInterestAdapter.DataObjectHolder>{

    private ArrayList<String> afroInterests;
    private ArrayList<String> selectedInterests;
    private ArrayList<String> interests = new ArrayList<>();

    public AfroInterestAdapter(ArrayList<String> afroInterests, ArrayList<String> selectedInterests) {
        this.afroInterests = afroInterests;
        this.selectedInterests = selectedInterests;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_afro_interest, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        holder.interest.setTextOn(afroInterests.get(position));
        holder.interest.setTextOff(afroInterests.get(position));

        if (selectedInterests!=null && selectedInterests.contains(afroInterests.get(position))) {
            interests.add(afroInterests.get(position));
            SettingsActivity.selectedInterests = interests;
            holder.interest.setChecked(true);
        } else
            holder.interest.setChecked(false);

        ViewGroup.LayoutParams layoutParams = holder.interest.getLayoutParams();
        if (layoutParams instanceof FlexboxLayoutManager.LayoutParams) {
            ((FlexboxLayoutManager.LayoutParams) layoutParams).setFlexGrow(1f);
            ((FlexboxLayoutManager.LayoutParams) layoutParams).setAlignSelf(AlignItems.BASELINE);
        }

        holder.interest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    interests.add(afroInterests.get(position));
                } else {
                    interests.remove(afroInterests.get(position));
                }

                SettingsActivity.selectedInterests = interests;
            }
        });
    }

    @Override
    public int getItemCount() {
        if (afroInterests==null)
            return 0;
        else
            return afroInterests.size();
    }


    static class DataObjectHolder extends RecyclerView.ViewHolder {
        ToggleButton interest;

        DataObjectHolder(View itemView) {
            super(itemView);
            interest = itemView.findViewById(R.id.interest);
        }
    }
}
