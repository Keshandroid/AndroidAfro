package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.suggestedpeople.SuggestedPeople;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class AfroSuggestedPeopleAdapter extends RecyclerView.Adapter<AfroSuggestedPeopleAdapter.DataObjectHolder> {

    private ArrayList<SuggestedPeople> suggestedPeopleList;
    private Activity context;

    AfroSuggestedPeopleAdapter(Activity context, ArrayList<SuggestedPeople> suggestedPeopleList) {
        this.context = context;
        this.suggestedPeopleList = suggestedPeopleList;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_suggested_people, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        SuggestedPeople people = suggestedPeopleList.get(position);

        String fullName = people.getFirstName() + " " + people.getLastName();
        holder.name.setText(fullName);

        Glide.with(holder.profilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + people.getProfileImageUrl())
                .into(holder.profilePicture);

        holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ProfileActivity.class).putExtra("userId", people.getUserId()));
            }
        });

        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followUser(people.getUserId());
                suggestedPeopleList.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(context, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    Utils.showAlert(context, "Oops something went wrong....");
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        if (suggestedPeopleList==null)
            return 0;
        else
            return suggestedPeopleList.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        CircleImageView profilePicture;
        TextView name;
        Button follow;
        CardView profile;

        DataObjectHolder(View itemView) {
            super(itemView);
            profilePicture = itemView.findViewById(R.id.profile_picture);
            name = itemView.findViewById(R.id.name);
            follow = itemView.findViewById(R.id.follow);
            profile = itemView.findViewById(R.id.profile);
        }
    }
}
