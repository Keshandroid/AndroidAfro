package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.rolemodel.RoleModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyRoleModelAdapter extends RecyclerView.Adapter<MyRoleModelAdapter.DataObjectHolder> {

    private ArrayList<RoleModel> roleModels;
    private OnRoleModelClickListener listener;

    public MyRoleModelAdapter(ArrayList<RoleModel> roleModels, OnRoleModelClickListener listener) {
        this.roleModels = roleModels;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_role_model, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        RoleModel roleModel = roleModels.get(position);

        String fullName = roleModel.getFirstName() + " " + roleModel.getLastName();
        holder.name.setText(fullName);

        String followers;
        if (roleModel.getFollowerIds()!=null)
            followers = roleModel.getFollowerIds().size() + " followers";
        else
            followers = "0 followers";

        holder.followers.setText(followers);

        Glide.with(holder.profilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + roleModel.getProfileImageUrl())
                .into(holder.profilePicture);

        holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRoleModelClick(roleModel);
            }
        });

        holder.undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRemoveRoleModelClick(roleModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (roleModels == null)
            return 0;
        else
            return roleModels.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name, followers, undo;
        CircleImageView profilePicture;
        CardView profile;

        DataObjectHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            followers = itemView.findViewById(R.id.followers);
            undo = itemView.findViewById(R.id.undo);
            profile = itemView.findViewById(R.id.profile);
            profilePicture = itemView.findViewById(R.id.profile_picture);
        }
    }

    public interface OnRoleModelClickListener {
        void onRemoveRoleModelClick(RoleModel roleModel);
        void onRoleModelClick(RoleModel roleModel);
    }
}
