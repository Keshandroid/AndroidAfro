package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.BitmapUtils;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.View.DataObjectHolder;
import com.TBI.afrocamgist.activity.FindUserActivity;
import com.TBI.afrocamgist.activity.HashtagPostsActivity;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.PostLikedByActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.ReportPostActivity;
import com.TBI.afrocamgist.activity.SharePostActivity;
import com.TBI.afrocamgist.activity.ViewPostImageActivity;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.TagBottomSheetFragment;
import com.TBI.afrocamgist.listener.OnPostClickListener;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.advertisement.Advertisement;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.story.StoryDetail;
import com.TBI.afrocamgist.model.story.StoryUserData;
import com.TBI.afrocamgist.model.suggestedpeople.SuggestedPeople;
import com.TBI.afrocamgist.model.user.User;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class AfroSwaggerPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements MenuAdapter.OnMenuItemClickListener {

    private Activity context;
    private ArrayList<Post> posts;
    private PopupWindow popup;
    private static final int STORY = 0, UPLOAD_POST = 1, POST = 2, SUGGESTED_PEOPLE = 3, ADVERTISEMENT = 4, POPULAR_POSTS = 5, POSTING = 6;
    public static int LAST_ADVERTISEMENT_POSITION = 0;
    public static int CURRENT_ADVERTISEMENT = 0;
    private int progress;
    private String progressTitle = "Posting...";

    private ArrayList<SuggestedPeople> suggestedPeopleList;
    private ArrayList<ArrayList<StoryUserData>> storyUserData;

    private AfroSuggestedPeopleAdapter adapter;
    private MainStoryAdapter mainStoryAdapter;
    private AfroPopularPostAdapter popularPostAdapter;
    private ArrayList<Advertisement> advertisements;
    private OnPostClickListener listener;
    private boolean folowReq = false;
    public static boolean isMute = true;
    public static boolean isSwaggerPlay = false;
    public Handler handler;

    private int count = 0;

    public AfroSwaggerPostAdapter(Activity context, ArrayList<Post> posts, OnPostClickListener listener) {
        this.context = context;
        this.posts = posts;
        this.progress = 0;
        this.suggestedPeopleList = new ArrayList<>();
        this.storyUserData = new ArrayList<>();
        adapter = new AfroSuggestedPeopleAdapter(context, suggestedPeopleList);
        mainStoryAdapter = new MainStoryAdapter(context, storyUserData);
        //mainStoryAdapter = new MainStoryAdapter(context, LocalStorage.getPopularPosts());
        popularPostAdapter = new AfroPopularPostAdapter(context, LocalStorage.getPopularPosts());
        this.advertisements = new ArrayList<>();
        this.listener = listener;
        handler = new Handler();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;

        switch (viewType) {

            case STORY:
                view = inflater.inflate(R.layout.item_story_horizontal_list, parent, false);
                return new RecyclerObjectHolder(view);
            case UPLOAD_POST:
                view = inflater.inflate(R.layout.item_upload_post, parent, false);
                return new RecyclerObjectHolder(view);
            case SUGGESTED_PEOPLE:
                view = inflater.inflate(R.layout.item_horizontal_list, parent, false);
                return new RecyclerObjectHolder(view);
            case ADVERTISEMENT:
                view = inflater.inflate(R.layout.item_advertisement, parent, false);
                return new RecyclerObjectHolder(view);
            case POPULAR_POSTS:
                view = inflater.inflate(R.layout.item_horizontal_list, parent, false);
                return new RecyclerObjectHolder(view);
            case POSTING:
                view = inflater.inflate(R.layout.item_uploading_post, parent, false);
                return new RecyclerObjectHolder(view);
            default:
                view = inflater.inflate(R.layout.item_group_post, parent, false);
                return new DataObjectHolder(view,listener,context);
        }

    }


    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {

            case STORY:
                setMainStoryRecyclerview(((RecyclerObjectHolder) holder));
                break;
            case UPLOAD_POST:
                ((RecyclerObjectHolder) holder).write.setText(R.string.postHereText);
                ((RecyclerObjectHolder) holder).search.setVisibility(View.VISIBLE);
                setUploadPostClickListener(((RecyclerObjectHolder) holder));
                break;
            case SUGGESTED_PEOPLE:
                setSuggestedPeopleRecyclerView(((RecyclerObjectHolder) holder));
                break;
            case POSTING:
                showPostingView(((RecyclerObjectHolder) holder));
                break;
            case ADVERTISEMENT:
                showAdvertisement((RecyclerObjectHolder) holder);
                break;
            case POPULAR_POSTS:
                setPopularPostsRecyclerView(((RecyclerObjectHolder) holder));
                break;
            default:

                if ("shared".equals(posts.get(position).getPostType())) {

                    if(posts.get(position)!=null && posts.get(position).getPostId()!=null) {
                        listener.onPostViewCount(posts.get(position).getPostId());
                        listener.onPostView(posts.get(position).getPostId());
                    }

                    //Hide Time and Shared time of the posts
                    //((DataObjectHolder)holder).sharedTime.setVisibility(View.VISIBLE);
                    //((DataObjectHolder)holder).time.setVisibility(View.GONE);

                    if(posts.get(position).getSharedPost()!=null){
                        ((DataObjectHolder)holder).playerView.setTag(posts.get(position).getSharedPost().getPostType());
                    }

                    ((DataObjectHolder) holder).sharedLayout.setVisibility(View.VISIBLE);
                    ((DataObjectHolder) holder).share.setVisibility(View.GONE); //Cannot share a shared post (same in website)
                    ((DataObjectHolder) holder).follow.setVisibility(View.GONE);

                    //Original code for shared post follow button
                    /*if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                        ((DataObjectHolder) holder).sharedFollow.setVisibility(View.GONE);
                    } else {
                        ((DataObjectHolder) holder).sharedFollow.setVisibility(View.VISIBLE);
                        if (posts.get(position) != null && posts.get(position).getFollowing() != null) {
                            ((DataObjectHolder) holder).sharedFollow.setChecked(posts.get(position).getFollowing());
                            ((DataObjectHolder) holder).sharedFollow.setEnabled(!posts.get(position).getFollowing());
                        } else {
                            ((DataObjectHolder) holder).sharedFollow.setChecked(true);
                            ((DataObjectHolder) holder).sharedFollow.setEnabled(false);
                        }
                    }*/



                    //New Code for shared post follow button
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                        ((DataObjectHolder) holder).sharedFollow.setVisibility(View.GONE);
                    } else {
                        ((DataObjectHolder) holder).sharedFollow.setVisibility(View.VISIBLE);

                        if (posts.get(position) != null && posts.get(position).getFollowing() != null) {
                            if (posts.get(position).getFollowing()) {
                                ((DataObjectHolder) holder).sharedFollow.setTextColor(context.getResources().getColor(R.color.orange));
                                ((DataObjectHolder) holder).sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                ((DataObjectHolder) holder).sharedFollow.setText(context.getResources().getString(R.string.following));
                                ((DataObjectHolder) holder).sharedFollow.setEnabled(false);
                            } else {
                                ((DataObjectHolder) holder).sharedFollow.setTextColor(context.getResources().getColor(R.color.blue));
                                ((DataObjectHolder) holder).sharedFollow.setBackgroundResource(R.drawable.round_corner_blue_border);
                                ((DataObjectHolder) holder).sharedFollow.setText(context.getResources().getString(R.string.follow));
//                                Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                                ((DataObjectHolder) holder).sharedFollow.setEnabled(true);

                                if(checkIsRequested(posts.get(position))){
                                    ((DataObjectHolder) holder).sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                    ((DataObjectHolder) holder).sharedFollow.setTextColor(context.getResources().getColor(R.color.colorAccent));
                                    ((DataObjectHolder) holder).sharedFollow.setText(R.string.requested);
                                    ((DataObjectHolder) holder).sharedFollow.setEnabled(false);
                                }
                            }
                        } else {
                            ((DataObjectHolder) holder).sharedFollow.setTextColor(context.getResources().getColor(R.color.blue));
                            ((DataObjectHolder) holder).sharedFollow.setBackgroundResource(R.drawable.round_corner_blue_border);
                            ((DataObjectHolder) holder).sharedFollow.setText(context.getResources().getString(R.string.follow));
//                            Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                            ((DataObjectHolder) holder).sharedFollow.setEnabled(true);
                        }
                    }


                    if(posts.get(position).getSharedPost()!=null){
                        if ("video".equals(posts.get(position).getSharedPost().getPostType())) {
                            ((DataObjectHolder) holder).socialShare.setVisibility(View.VISIBLE);
                            Glide.with(context)
                                    .load(UrlEndpoints.MEDIA_BASE_URL + posts.get(position).getSharedPost().getThumbnail())
                                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                    .into(((DataObjectHolder) holder).thumbnail);
                        }else {
                            ((DataObjectHolder) holder).socialShare.setVisibility(View.GONE);
                        }
                    }

                } else {

                    if(posts.get(position)!=null && posts.get(position).getPostId()!=null){
                        listener.onPostViewCount(posts.get(position).getPostId());
                        listener.onPostView(posts.get(position).getPostId());
                    }

                    //Hide Time and Shared time of the posts
                    //((DataObjectHolder)holder).sharedTime.setVisibility(View.GONE);
                    //((DataObjectHolder)holder).time.setVisibility(View.VISIBLE);


                    ((DataObjectHolder)holder).playerView.setTag(posts.get(position).getPostType());
                    ((DataObjectHolder) holder).sharedLayout.setVisibility(View.GONE);
                    ((DataObjectHolder) holder).share.setVisibility(View.VISIBLE);
                    ((DataObjectHolder) holder).sharedFollow.setVisibility(View.GONE);


                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                        ((DataObjectHolder) holder).follow.setVisibility(View.GONE);
                    } else {
                        ((DataObjectHolder) holder).follow.setVisibility(View.VISIBLE);

                        //----modified----
                        /*if (posts.get(position).getUserId() != null) {
                            getUserProfile(posts.get(position).getUserId());
                        }*/
                        //----Modified----

                        if (posts.get(position) != null && posts.get(position).getFollowing() != null) {
                            if (posts.get(position).getFollowing()) {
                                ((DataObjectHolder) holder).follow.setTextColor(context.getResources().getColor(R.color.orange));
                                ((DataObjectHolder) holder).follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                ((DataObjectHolder) holder).follow.setText(context.getResources().getString(R.string.following));
                                ((DataObjectHolder) holder).follow.setEnabled(false);
                            } else {
                                ((DataObjectHolder) holder).follow.setTextColor(context.getResources().getColor(R.color.blue));
                                ((DataObjectHolder) holder).follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                                ((DataObjectHolder) holder).follow.setText(context.getResources().getString(R.string.follow));
//                                Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                                ((DataObjectHolder) holder).follow.setEnabled(true);

                                if(checkIsRequested(posts.get(position))){
                                    ((DataObjectHolder) holder).follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                    ((DataObjectHolder) holder).follow.setTextColor(context.getResources().getColor(R.color.colorAccent));
                                    ((DataObjectHolder) holder).follow.setText(R.string.requested);
                                    ((DataObjectHolder) holder).follow.setEnabled(false);
                                }

                            }
                        } else {
                            ((DataObjectHolder) holder).follow.setTextColor(context.getResources().getColor(R.color.blue));
                            ((DataObjectHolder) holder).follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                            ((DataObjectHolder) holder).follow.setText(context.getResources().getString(R.string.follow));
//                            Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                            ((DataObjectHolder) holder).follow.setEnabled(true);
                        }
                    }

                    if ("video".equals(posts.get(position).getPostType())) {
                        ((DataObjectHolder) holder).socialShare.setVisibility(View.VISIBLE);
                        Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + posts.get(position).getThumbnail())
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                .into(((DataObjectHolder) holder).thumbnail);
                    }else {
                        ((DataObjectHolder) holder).socialShare.setVisibility(View.GONE);
                    }

                }

                if(posts.get(position)!=null){
                    setPostData(posts.get(position), ((DataObjectHolder) holder), false);
                    setClickListener(((DataObjectHolder) holder));
                }

                break;

        }
    }

    private boolean getUserProfile(int id) {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
        String url = UrlEndpoints.PROFILE + "/" + id;
        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (!object.has(Constants.MESSAGE)) {
                        User user = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), User.class);
                        folowReq = user.getPrivate();
//                        Logger.e("LLLLL_Private: ", user.getFirstName()+ folowReq);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return folowReq;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void setUploadPostClickListener(RecyclerObjectHolder holder) {

        Glide.with(holder.myProfilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                .into(holder.myProfilePicture);

        holder.write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCreatePostClicked();
            }
        });

        holder.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, FindUserActivity.class).putExtra("openFromScreen","search"));
            }
        });
    }

    private void setSuggestedPeopleRecyclerView(RecyclerObjectHolder holder) {

        holder.listTitle.setText(R.string.suggestedPeople);
        holder.horizontalList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.horizontalList.setAdapter(adapter);
    }

    private void setMainStoryRecyclerview(RecyclerObjectHolder holder) {
        holder.horizontal_story_list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.horizontal_story_list.setAdapter(mainStoryAdapter);
    }

    private void setPopularPostsRecyclerView(RecyclerObjectHolder holder) {

        holder.listTitle.setText(R.string.popularPosts);
        holder.horizontalList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.horizontalList.setAdapter(popularPostAdapter);
    }

    private void showPostingView(RecyclerObjectHolder holder) {
        if (getUploadProgress() != -1) {
            holder.uploadProgress.setProgress(getUploadProgress());
            holder.progressTitle.setText(getProgressTitle());
            if (holder.uploadProgress.getProgress() == 100) {
                holder.uploadProgress.setVisibility(View.GONE);
                holder.progressTitle.setText("Finishing...");
            }
        } else {
            holder.uploadProgress.setVisibility(View.GONE);
            holder.progressTitle.setText("Finishing...");
        }
    }

    public void setUploadProgress(Integer progress) {
        this.progress = progress;
    }

    public void setProgressTitle(String progressTitle) {
        this.progressTitle = progressTitle;
    }

    private String getProgressTitle() {
        return this.progressTitle;
    }

    private Integer getUploadProgress() {
        return this.progress;
    }

    public void setSuggestedPeople(ArrayList<SuggestedPeople> suggestedPeopleList) {
        if (adapter != null) {
            this.suggestedPeopleList.addAll(suggestedPeopleList);
            adapter.notifyDataSetChanged();
        }
    }

    public void setStoryList(ArrayList<ArrayList<StoryUserData>> storyUserData) {
        if (mainStoryAdapter != null) {
            this.storyUserData.clear();
            this.storyUserData.addAll(storyUserData);
            //mainStoryAdapter.notifyDataSetChanged();
        }
    }

    public void setAdvertisementList(ArrayList<Advertisement> advertisements) {

        if (adapter != null && advertisements.size() != 0) {
            this.advertisements = new ArrayList<>();
            this.advertisements.addAll(advertisements);
            adapter.notifyDataSetChanged();
        }
    }

    private void showAdvertisement(RecyclerObjectHolder holder) {

        if (advertisements.size() > 0) {
            if (LAST_ADVERTISEMENT_POSITION != holder.getAdapterPosition()) {

                if (advertisements != null && advertisements.size() > 0) {

                    Advertisement advertisement = advertisements.get(CURRENT_ADVERTISEMENT);

                    holder.companyName.setText(advertisement.getCompanyName());

                    Glide.with(holder.adImage)
                            .load(UrlEndpoints.MEDIA_BASE_URL + advertisement.getAdImage())
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                            .into(holder.adImage);

                    holder.content.setText(advertisement.getContent());

                    CURRENT_ADVERTISEMENT++;
                    LAST_ADVERTISEMENT_POSITION = holder.getAdapterPosition();

                    if (advertisements != null && CURRENT_ADVERTISEMENT == (advertisements.size()))
                        CURRENT_ADVERTISEMENT = 0;
                }
            }
        }
    }

    private void setPostData(Post post, DataObjectHolder holder, Boolean isSharedPost) {

        if(post!=null){
            if ("shared".equals(post.getPostType())) {
                setSharedPostData(post, holder);
                setPostData(post.getSharedPost(), holder, true);
            } else {
                if(post.getUser_name()!=null && !post.getUser_name().isEmpty()){
                    holder.name.setText(post.getUser_name());
                }else {
                    String fullName = post.getFirstName() + " " + post.getLastName();
                    holder.name.setText(fullName);
                }

                if(post.getTagged_id()!=null && !post.getTagged_id().isEmpty()){
                    holder.icTag.setVisibility(View.VISIBLE);
                }else {
                    holder.icTag.setVisibility(View.GONE);
                }

                holder.time.setText(Utils.getSocialStyleTime(post.getPostDate()));

                if (post.getPostLatLng() != null && !"".equals(post.getPostLatLng())) {
                    holder.location.setVisibility(View.VISIBLE);
                    holder.location.setText(post.getPostLocation());
                } else {
                    holder.location.setVisibility(View.GONE);
                }

                if (!isSharedPost) {
                    //Logger.e("LLLL_: ",post.getLikeCount().toString());

                    if(post.getLiked()!=null){
                        if (post.getLiked()) {
                            holder.like.setChecked(true);
                            holder.likesCount.setTextColor(Color.parseColor("#FF7400"));
                        } else {
                            holder.like.setChecked(false);
                            holder.likesCount.setTextColor(Color.parseColor("#004F95"));
                        }

                        String likeCount = post.getLikeCount() + "";
                        holder.likesCount.setText(likeCount);
                        String commentCount = post.getCommentCount() + "";
                        holder.comment.setText(commentCount);
                    }
                }

                if(post.getVideo_play_count()!=null){

                    Logger.d("name_count",post.getFirstName()+"==="+post.getVideo_play_count());

                    if(post.getVideo_play_count()==1){
                        holder.videoCount.setText(post.getVideo_play_count()+" "+"view");
                    }else {
                        holder.videoCount.setText(post.getVideo_play_count()+" "+"views");
                    }
                }


                if(post.getPostText()!=null){
                    holder.postText.addAutoLinkMode(AutoLinkMode.MODE_HASHTAG,AutoLinkMode.MODE_URL);
                    holder.postText.setHashtagModeColor(ContextCompat.getColor(context, R.color.hashtag));
                    holder.postText.setUrlModeColor(ContextCompat.getColor(context, R.color.colorAccent));
                    holder.postText.setAutoLinkText(post.getPostText());


                    holder.postText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            holder.postText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            int linecount = holder.postText.getLineCount();

                            if(linecount > 4){
                                holder.more.setVisibility(View.VISIBLE);
                                holder.postText.setMaxLines(4);
                                holder.postText.setEllipsize(TextUtils.TruncateAt.END);
                                holder.more.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        holder.postText.setMaxLines(Integer.MAX_VALUE);
                                        holder.more.setVisibility(View.GONE);
                                        holder.less.setVisibility(View.VISIBLE);
                                    }
                                });
                                holder.less.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        holder.postText.setMaxLines(4);
                                        holder.more.setVisibility(View.VISIBLE);
                                        holder.less.setVisibility(View.GONE);
                                    }
                                });
                            }else {
                                holder.more.setVisibility(View.GONE);
                                holder.less.setVisibility(View.GONE);
                            }


                        }
                    });


                    //makeTextViewResizable(holder.postText,4,".. See More",true);


                    holder.postText.setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
                        @Override
                        public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {
                            if(autoLinkMode == AutoLinkMode.MODE_HASHTAG){
                                Hashtag hashtag = new Hashtag();
                                if(matchedText.startsWith("#")){
                                    matchedText =matchedText.substring(1);
                                }
                                hashtag.setHashtagSlug(matchedText);
                                Logger.d("hashtag1",""+hashtag.getHashtagSlug());
                                context.startActivity(new Intent(context, HashtagPostsActivity.class).putExtra("hashtag",hashtag));
                            }else if(autoLinkMode == AutoLinkMode.MODE_URL){
                                try{
                                    Uri uri = Uri.parse(matchedText);
                                    Intent httpIntent = new Intent(Intent.ACTION_VIEW,uri);
                                    context.getParent().startActivity(httpIntent);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                            }
                        }
                    });
                }


                Glide.with(context)
                        .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                        .into(holder.profilePicture);
                setPostDetails(post, holder);
            }
        }
    }

    private void setSharedPostData(Post post, DataObjectHolder holder) {

        String fullName = post.getFirstName() + " " + post.getLastName();
        holder.sharedName.setText(fullName);
        holder.sharedTime.setText(Utils.getSocialStyleTime(post.getPostDate()));
        holder.sharedPostText.setText(post.getPostText());

        if (post.getPostLatLng() != null && !"".equals(post.getPostLatLng())) {
            holder.sharedLocation.setVisibility(View.VISIBLE);
            holder.sharedLocation.setText(post.getPostLocation());
        } else {
            holder.sharedLocation.setVisibility(View.GONE);
        }

        if (post.getLiked()) {
            holder.like.setChecked(true);
            holder.likesCount.setTextColor(Color.parseColor("#FF7400"));
        } else {
            holder.like.setChecked(false);
            holder.likesCount.setTextColor(Color.parseColor("#004F95"));
        }

//        if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
//            holder.menu.setVisibility(View.VISIBLE);
//        } else {
//            holder.menu.setVisibility(View.GONE);
//        }
        String likeCount = post.getLikeCount() + "";
        holder.likesCount.setText(likeCount);
        String commentCount = post.getCommentCount() + "";
        holder.comment.setText(commentCount);

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(holder.sharedProfileImage);
    }

    private void setPostDetails(Post post, DataObjectHolder holder) {

        if(post.getPostType()!=null){
            switch (post.getPostType()) {
                case "text":
                    holder.layoutOnlyImages.setVisibility(View.GONE);
                    holder.videoLayout.setVisibility(View.GONE);

                    if (post.getBackgroundImagePost()) {

                        holder.layoutPostWithBackgroundImage.setVisibility(View.VISIBLE);

                        Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getBackgroundImage())
                                .into(holder.backgroundImage);

                        holder.postTextOnImage.setText(post.getPostText());
                        holder.postText.setText("");
                    } else {
                        holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);
                    }

                    break;
                case "image":
                    holder.layoutOnlyImages.setVisibility(View.VISIBLE);
                    holder.videoLayout.setVisibility(View.GONE);
                    holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);

                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.placeholder);

                    holder.postImage.setImageBitmap(null);
//                holder.imageLoadingProgressBar.setVisibility(View.VISIBLE);

                    if (post.getPostImage().size() > 1) {
                        Logger.e("LLLLLL_size)_iameg: ", String.valueOf(post.getPostImage().size()));
                        String count = (post.getPostImage().size() - 1) + "";
                        //holder.postImage.setImageDrawable(context.getDrawable(R.drawable.placeholder));
                        try {
                            RequestOptions options = new RequestOptions()
                                    //.skipMemoryCache(true)
                                    .apply(bitmapTransform(new BlurTransformation(25, 3)))
                                    .diskCacheStrategy(DiskCacheStrategy.ALL);
                            Glide.with(context)
                                    .asBitmap()
                                    .thumbnail(0.7f)
                                    .apply(options)
                                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                    .into(new CustomTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                                        holder.imageLoadingProgressBar.setVisibility(View.GONE);
                                            // holder.postImage.setImageDrawable(null);
                                            holder.postImage.setImageBitmap(BitmapUtils.getResizedBitmap(resource, holder.postImage.getMaxHeight()));

                                            RequestOptions options = new RequestOptions()
                                                    //.skipMemoryCache(true)
                                                    .diskCacheStrategy(DiskCacheStrategy.ALL);

                                            Glide.with(context)
                                                    .asBitmap()
                                                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                                    .apply(options)
                                                    .into(new SimpleTarget<Bitmap>() {

                                                        @Override
                                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                            holder.postImage.setImageBitmap(resource);
                                                                /*handler.postAtTime(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        holder.postImage.setImageBitmap(resource);
                                                                    }
                                                                },200);*/
                                                        }
                                                    });
                                        }

                                        @Override
                                        public void onLoadCleared(@Nullable Drawable placeholder) {

                                        }
                                    });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(1))
                                .into(holder.moreImages);

                        holder.mapPostImage.setVisibility(View.GONE);
                        holder.postImage.setVisibility(View.VISIBLE);
                        holder.moreImagesLayout.setVisibility(View.VISIBLE);

                        holder.imageCount.setText(count);
                    } else {

                        if (post.getMapPost()) {

                            holder.mapPostImage.setVisibility(View.VISIBLE);
                            holder.postImage.setVisibility(View.GONE);
                            // holder.mapPostImage.setImageDrawable(context.getDrawable(R.drawable.placeholder));
                            RequestOptions options = new RequestOptions()
                                    //.skipMemoryCache(true)
                                    .apply(bitmapTransform(new BlurTransformation(25, 3)))
                                    .diskCacheStrategy(DiskCacheStrategy.ALL);
                            Glide.with(context)
                                    .asBitmap()
                                    .thumbnail(0.7f)
                                    .apply(options)
                                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                    .into(new CustomTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                                        holder.imageLoadingProgressBar.setVisibility(View.GONE);
                                            // holder.mapPostImage.setImageDrawable(null);
                                            holder.mapPostImage.setImageBitmap(BitmapUtils.getResizedBitmap(resource, 1024));

                                            RequestOptions options = new RequestOptions()
                                                    //.skipMemoryCache(true)
                                                    .diskCacheStrategy(DiskCacheStrategy.ALL);
                                            Glide.with(context)
                                                    .asBitmap()
                                                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                                    .apply(options)
                                                    .into(new SimpleTarget<Bitmap>() {

                                                        @Override
                                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                            holder.mapPostImage.setImageBitmap(resource);
                                                                /*handler.postAtTime(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        holder.mapPostImage.setImageBitmap(resource);
                                                                    }
                                                                },200);*/

                                                        }
                                                    });



                                        }

                                        @Override
                                        public void onLoadCleared(@Nullable Drawable placeholder) {

                                        }
                                    });
                        } else {
                            holder.mapPostImage.setVisibility(View.GONE);
                            holder.postImage.setVisibility(View.VISIBLE);
                            // holder.postImage.setImageDrawable(context.getDrawable(R.drawable.placeholder));
                            try {

                                Logger.d("post_image_url",""+UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0));

                                RequestOptions options = new RequestOptions()
                                        //.skipMemoryCache(true)
                                        .apply(bitmapTransform(new BlurTransformation(25, 3)))
                                        .diskCacheStrategy(DiskCacheStrategy.ALL);
                                Glide.with(context)
                                        .asBitmap()
                                        .thumbnail(0.7f)
                                        .apply(options)
                                        .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                        .into(new CustomTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                                                holder.imageLoadingProgressBar.setVisibility(View.GONE);
                                                //  holder.postImage.setImageDrawable(null);
                                                holder.postImage.setImageBitmap(BitmapUtils.getResizedBitmap(resource, holder.postImage.getMaxHeight()));
                                                RequestOptions options = new RequestOptions()
                                                        //.skipMemoryCache(true)
                                                        .diskCacheStrategy(DiskCacheStrategy.ALL);
                                                Glide.with(context)
                                                        .asBitmap()
                                                        .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                                        .apply(options)
                                                        .into(new SimpleTarget<Bitmap>() {
                                                            @Override
                                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                                holder.postImage.setImageBitmap(resource);
                                                                    /*handler.postAtTime(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            holder.postImage.setImageBitmap(resource);
                                                                        }
                                                                    },200);*/
                                                            }
                                                        });



                                            }

                                            @Override
                                            public void onLoadCleared(@Nullable Drawable placeholder) {

                                            }
                                        });
//                            }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        holder.moreImagesLayout.setVisibility(View.GONE);
                    }

                    new Zoomy.Builder(context)
                            .target(holder.postImage)
                            .enableImmersiveMode(false)
                            .tapListener(v -> context.startActivity(new Intent(context, ViewPostImageActivity.class)
                                    .putExtra("position", 0)
                                    .putExtra("images", post.getPostImage())))
                            .register();

                    break;
                case "video":
                    holder.layoutOnlyImages.setVisibility(View.GONE);
                    holder.videoLayout.setVisibility(View.VISIBLE);
                    holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);
                    holder.bind(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo()), holder.getAdapterPosition(),post.getPostId());
                    break;
                default:
                    break;
            }
        }
    }

    private void setClickListener(DataObjectHolder holder) {

        holder.icTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()) && posts.get(holder.getAdapterPosition()).getSharedPost()!=null) {
                    if(posts.get(holder.getAdapterPosition()).getSharedPost().getTagged_id()!=null && !posts.get(holder.getAdapterPosition()).getSharedPost().getTagged_id().isEmpty()){
                        listener.onTagClicked(posts.get(holder.getAdapterPosition()).getSharedPost().getTagged_id());
                    }
                }else {
                    if(posts.get(holder.getAdapterPosition()).getTagged_id()!=null && !posts.get(holder.getAdapterPosition()).getTagged_id().isEmpty()){
                        listener.onTagClicked(posts.get(holder.getAdapterPosition()).getTagged_id());
                    }
                }*/
            }
        });

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()) && posts.get(holder.getAdapterPosition()).getSharedPost() != null) {
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getSharedPost().getUserId()))
                        context.startActivity(new Intent(context, MyProfileActivity.class));
                    else
                        context.startActivity(new Intent(context, ProfileActivity.class)
                                .putExtra("userId", posts.get(holder.getAdapterPosition()).getSharedPost().getUserId()));
                } else {
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getUserId()))
                        context.startActivity(new Intent(context, MyProfileActivity.class));
                    else
                        context.startActivity(new Intent(context, ProfileActivity.class)
                                .putExtra("userId", posts.get(holder.getAdapterPosition()).getUserId()));
                }
            }
        });

        holder.sharedName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getUserId()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class)
                            .putExtra("userId", posts.get(holder.getAdapterPosition()).getUserId()));
            }
        });

        holder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onCommentClicked(posts.get(holder.getAdapterPosition()), holder.getAdapterPosition());
            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivityForResult(new Intent(context, SharePostActivity.class).putExtra("post", posts.get(holder.getAdapterPosition())), 100);
            }
        });

        holder.socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*String url = UrlEndpoints.BASE_SHARE_POST_URL + posts.get(holder.getAdapterPosition()).getPostId();

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
                i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                i.putExtra(Intent.EXTRA_TEXT, url);
                context.startActivity(Intent.createChooser(i, "Share URL"));*/

                String url = UrlEndpoints.BASE_SHARE_POST_URL + posts.get(holder.getAdapterPosition()).getPostId();

                if(listener!=null){
                    if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType())) {
                        listener.onDownloadClick(posts.get(holder.getAdapterPosition()).getSharedPost().getPostVideo(),url);
                    }else {
                        listener.onDownloadClick(posts.get(holder.getAdapterPosition()).getPostVideo(),url);
                    }
                }

            }
        });

        holder.report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ReportPostActivity.class).putExtra("postId", posts.get(holder.getAdapterPosition()).getPostId()));
            }
        });

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(holder.menu, holder.getAdapterPosition());
            }
        });

        holder.like.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (buttonView.isPressed()) {
                if (isChecked) {
                    holder.likesCount.setTextColor(Color.parseColor("#FF7400"));

                    int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                    posts.get(holder.getAdapterPosition()).setLikeCount(counter+1);

                    String likesCount = (Integer.valueOf(holder.likesCount.getText().toString()) + 1) + "";
                    holder.likesCount.setText(likesCount);
                } else {
                    if (Integer.parseInt(holder.likesCount.getText().toString().trim()) > 0) {
                        holder.likesCount.setTextColor(Color.parseColor("#004F95"));

                        int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                        posts.get(holder.getAdapterPosition()).setLikeCount(counter-1);

                        String likesCount = (Integer.parseInt(holder.likesCount.getText().toString().trim()) - 1) + "";
                        holder.likesCount.setText(likesCount);
                    }
                }
            }

            if (buttonView.isPressed()) {
                posts.get(holder.getAdapterPosition()).setLiked(isChecked);
                listener.onPostChecked(posts.get(holder.getAdapterPosition()));
            }
        });

        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isFollow = false;
                if(posts.get(holder.getAdapterPosition()) != null && posts.get(holder.getAdapterPosition()).getFollowing() != null){
                    if(posts.get(holder.getAdapterPosition()).getFollowing()) {
                        isFollow = false;
                    }else {
                        isFollow = true;
                        if(checkIsRequested(posts.get(holder.getAdapterPosition()))){
                            isFollow = false;
                        }
                    }
                }else {
                    isFollow = true;
                }

                if (isFollow) {
                    if(posts.get(holder.getAdapterPosition()).getPrivateAccount()){
                        holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.follow.setText(context.getResources().getString(R.string.requested));
                        holder.follow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }else {
                        holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.follow.setText(context.getResources().getString(R.string.following));
                        holder.follow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }
                }
            }
        });

        holder.sharedFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //New Code for shared follow click
                boolean isFollow = false;
                if(posts.get(holder.getAdapterPosition()) != null && posts.get(holder.getAdapterPosition()).getFollowing() != null){
                    if(posts.get(holder.getAdapterPosition()).getFollowing()) {
                        isFollow = false;
                    }else {
                        isFollow = true;
                        if(checkIsRequested(posts.get(holder.getAdapterPosition()))){
                            isFollow = false;
                        }
                    }
                }else {
                    isFollow = true;
                }

                if (isFollow) {
                    if(posts.get(holder.getAdapterPosition()).getPrivateAccount()){
                        holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.sharedFollow.setText(context.getResources().getString(R.string.requested));
                        holder.sharedFollow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }else {
                        holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.sharedFollow.setText(context.getResources().getString(R.string.following));
                        holder.sharedFollow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }
                }
            }
        });

        /*holder.sharedFollow.setOnCheckedChangeListener((buttonView, isChecked) -> {
            //Original code for shared follow click
            if (isChecked && buttonView.isPressed()) {
                holder.sharedFollow.setEnabled(false);
                listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
            }
        });*/

        holder.likesCount.setOnClickListener(v -> {

            if (!"0".equals(holder.likesCount.getText().toString())) {
                context.startActivity(new Intent(context, PostLikedByActivity.class)
                        .putExtra("postId", posts.get(holder.getAdapterPosition()).getPostId()));
            }
        });

        holder.moreImagesLayout.setOnClickListener(v -> {

            ArrayList<String> images;

            if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()))
                images = posts.get(holder.getAdapterPosition()).getSharedPost().getPostImage();
            else
                images = posts.get(holder.getAdapterPosition()).getPostImage();

            context.startActivity(new Intent(context, ViewPostImageActivity.class)
                    .putExtra("position", 1)
                    .putExtra("images", images));
        });
    }

    private void showMenu(View menu, int position) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int popupWidth = 450;
        int popupHeight = LinearLayout.LayoutParams.WRAP_CONTENT;

        LinearLayout viewGroup = context.findViewById(R.id.menu_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.drop_down_menu, viewGroup);
        popupMenu(layout, position);

        popup = new PopupWindow(layout, popupWidth, popupHeight);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        if (position == (posts.size() - 1))
            popup.showAsDropDown(menu, 0, -300, Gravity.TOP);
        else
            popup.showAsDropDown(menu);
    }

    private void popupMenu(View layout, int position) {

        RecyclerView menu = layout.findViewById(R.id.menuList);
        menu.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        menu.setLayoutManager(mLayoutManager);
        MenuAdapter adapter;
        if (!LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_other_menu))), position, this);
        } else {
            if (posts.get(position).getMapPost())
                adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_map_menu))), position, this);
            else
                adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_menu))), position, this);
        }
        menu.setAdapter(adapter);
    }

    private boolean checkIsRequested(Post profileUser) {
        if(profileUser!=null)
        {
            boolean isRequestSend = false;
            if(profileUser.getRequestButtons().get(0).getButtonText().equalsIgnoreCase("Requested")){
                isRequestSend = true;
            }
            return isRequestSend;
        }
        return false;
    }

    @Override
    public void onMenuItemClicked(String item, int postPosition) {

        if (popup != null && popup.isShowing())
            popup.dismiss();

        switch (item.toLowerCase()) {

            case "edit":
                listener.onEditPost(posts.get(postPosition), postPosition);
                break;
            case "delete":
                listener.onDeletePost(posts.get(postPosition));
                break;
            case "hide":
                listener.onHidePost(posts.get(postPosition));
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (posts == null)
            return 0;
        else
            return posts.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (posts.get(position) != null) {
            if ((position - 1) > 0 && ((position - 1) % 7 == 0)) { // Advertisement after every 7 posts
                if (posts.get(position).getFirstName() != null)
                    posts.add(position, new Post());
                return ADVERTISEMENT;
            } else if ((position - 1) > 0 && ((position - 1) % 10 == 0)) { // Popular posts list after every 10 posts
                if (posts.get(position).getFirstName() != null)
                    posts.add(position, new Post());
                return POPULAR_POSTS;
            } else if (posts.get(2) == null) {   //.get(1)
                if (position == 4) {    // position == 3
                    return SUGGESTED_PEOPLE;
                } else {
                    switch (position) {
                        case 0:
                            return STORY;     //case 0: UPLOAD_POST

                        case 1:                  //case 1: delete this
                            return UPLOAD_POST;

                        case 3:                  // case : 2 POST
                            return POST;
                        default:
                            return POST;
                    }
                }
            } else {

                switch (position) {
                    case 0:                       //case 0: UPLOAD_POST
                        return STORY;
                    case 1:
                        return UPLOAD_POST;         //case 1: delete this
                    case 3:                         // case 2: SUGGESTED_PEOPLE
                        return SUGGESTED_PEOPLE;
                    default:
                        return POST;
                }
            }
        } else {
            return POSTING;
        }
    }


    static class RecyclerObjectHolder extends RecyclerView.ViewHolder {

        Uri mediaUri;

        CircleImageView myProfilePicture;
        ImageView search, adImage;
        TextView write, listTitle, companyName, content, progressTitle;
        RecyclerView horizontalList,horizontal_story_list;
        RoundedHorizontalProgressBar uploadProgress;

        RecyclerObjectHolder(View itemView) {
            super(itemView);
            myProfilePicture = itemView.findViewById(R.id.profile_image);
            search = itemView.findViewById(R.id.search);

            write = itemView.findViewById(R.id.write);
            listTitle = itemView.findViewById(R.id.list_title);
            horizontalList = itemView.findViewById(R.id.horizontal_list);
            horizontal_story_list = itemView.findViewById(R.id.horizontal_story_list);
            companyName = itemView.findViewById(R.id.companyName);
            content = itemView.findViewById(R.id.content);
            uploadProgress = itemView.findViewById(R.id.upload_progress);
            progressTitle = itemView.findViewById(R.id.progress_title);
            adImage = itemView.findViewById(R.id.ad_image);
        }

        void bind(Uri media) {
            this.mediaUri = media;
        }
    }



    public ArrayList<Post> getPostsList(){
        return posts;
    }

    public void pausevideo(RecyclerView.ViewHolder viewHolder) {

        if (viewHolder != null && viewHolder instanceof DataObjectHolder) {
            Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
            if (viewHolder.getItemViewType() != UPLOAD_POST &&
                    viewHolder.getItemViewType() != STORY &&       // added for STORY
                    viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
                    viewHolder.getItemViewType() != ADVERTISEMENT &&
                    viewHolder.getItemViewType() != POPULAR_POSTS &&
                    viewHolder.getItemViewType() != POSTING) {
                if (((DataObjectHolder) viewHolder).helper != null) {
                    if (((DataObjectHolder) viewHolder).helper.isPlaying()) {
                        ((DataObjectHolder) viewHolder).helper.pause();
                    }/* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*/
                }
//            }
            }
        }
    }

     public void playvideo(RecyclerView.ViewHolder viewHolder) {
         if (viewHolder != null && viewHolder instanceof DataObjectHolder) {
             Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
             if (viewHolder.getItemViewType() != UPLOAD_POST &&
                     viewHolder.getItemViewType() != STORY &&       // added for STORY
                     viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
                     viewHolder.getItemViewType() != ADVERTISEMENT &&
                     viewHolder.getItemViewType() != POPULAR_POSTS &&
                     viewHolder.getItemViewType() != POSTING) {
                 if (((DataObjectHolder) viewHolder).helper != null) {
                     if (!((DataObjectHolder) viewHolder).helper.isPlaying()) {
                         ((DataObjectHolder) viewHolder).helper.play();
                     } /*else {
                    ((DataObjectHolder) viewHolder).helper.play();
                }*/
                 }
//            }
             }
         }
     }

    public void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {
        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                   // tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if(maxLine == -1){
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                     final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new MySpannable(false){
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "See Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 4, ".. See More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;
    }

    public class MySpannable extends ClickableSpan {

        private boolean isUnderline = true;

        /**
         * Constructor
         */
        public MySpannable(boolean isUnderline) {
            this.isUnderline = isUnderline;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setUnderlineText(isUnderline);
            ds.setColor(Color.parseColor("#1b76d3"));
        }

        @Override
        public void onClick(View widget) {


        }
    }
    /*public void pausevideo(Container posts) {
        try{
            LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
            int lastposition = layoutManager.findLastVisibleItemPosition();
            RecyclerView.ViewHolder viewHolder = posts.findViewHolderForLayoutPosition(lastposition);

            if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
//            if (viewHolder.getItemViewType() != UPLOAD_POST &&
//                    viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
//                    viewHolder.getItemViewType() != ADVERTISEMENT &&
//                    viewHolder.getItemViewType() != POPULAR_POSTS &&
//                    viewHolder.getItemViewType() != POSTING) {
                if (((DataObjectHolder) viewHolder).helper != null) {
                    if (viewHolder.getItemViewType() != UPLOAD_POST &&
                    viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
                    viewHolder.getItemViewType() != ADVERTISEMENT &&
                    viewHolder.getItemViewType() != POPULAR_POSTS &&
                    viewHolder.getItemViewType() != POSTING) {
                    if (((DataObjectHolder) viewHolder).helper.isPlaying()) {
                        ((DataObjectHolder) viewHolder).helper.pause();
                    }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                    }
                }else {
                    viewHolder = posts.findViewHolderForLayoutPosition(lastposition-1);
                    if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                        Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
                        if (viewHolder.getItemViewType() != UPLOAD_POST &&
                                viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
                                viewHolder.getItemViewType() != ADVERTISEMENT &&
                                viewHolder.getItemViewType() != POPULAR_POSTS &&
                                viewHolder.getItemViewType() != POSTING) {
                            if (((DataObjectHolder) viewHolder).helper != null) {
                                if (((DataObjectHolder) viewHolder).helper.isPlaying()) {
                                    ((DataObjectHolder) viewHolder).helper.pause();
                                }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                            }
                        }
//            }
                    }
                }
//            }
            }else {
                viewHolder = posts.findViewHolderForLayoutPosition(lastposition-1);
                if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                    Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
                    if (viewHolder.getItemViewType() != UPLOAD_POST &&
                            viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
                            viewHolder.getItemViewType() != ADVERTISEMENT &&
                            viewHolder.getItemViewType() != POPULAR_POSTS &&
                            viewHolder.getItemViewType() != POSTING) {
                        if (((DataObjectHolder) viewHolder).helper != null) {

                            if (((DataObjectHolder) viewHolder).helper.isPlaying()) {
                                ((DataObjectHolder) viewHolder).helper.pause();
                            }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                        }
                    }
//            }
                }
            }
        }catch (Exception e){

        }

    }


    public void playvideo(Container posts) {
        try {
            LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
            int lastposition = layoutManager.findLastVisibleItemPosition();
            RecyclerView.ViewHolder viewHolder = posts.findViewHolderForLayoutPosition(lastposition);
            if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
//            if (viewHolder.getItemViewType() != UPLOAD_POST &&
//                    viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
//                    viewHolder.getItemViewType() != ADVERTISEMENT &&
//                    viewHolder.getItemViewType() != POPULAR_POSTS &&
//                    viewHolder.getItemViewType() != POSTING) {
                if (((DataObjectHolder) viewHolder).helper != null) {
                    if (viewHolder.getItemViewType() != UPLOAD_POST &&
                            viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
                            viewHolder.getItemViewType() != ADVERTISEMENT &&
                            viewHolder.getItemViewType() != POPULAR_POSTS &&
                            viewHolder.getItemViewType() != POSTING) {
                        if (!((DataObjectHolder) viewHolder).helper.isPlaying()) {
                            ((DataObjectHolder) viewHolder).helper.play();
                        }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                    }
                }else {
                    viewHolder = posts.findViewHolderForLayoutPosition(lastposition-1);
                    if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                        Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
                        if (viewHolder.getItemViewType() != UPLOAD_POST &&
                                viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
                                viewHolder.getItemViewType() != ADVERTISEMENT &&
                                viewHolder.getItemViewType() != POPULAR_POSTS &&
                                viewHolder.getItemViewType() != POSTING) {
                            if (((DataObjectHolder) viewHolder).helper != null) {
                                if (!((DataObjectHolder) viewHolder).helper.isPlaying()) {
                                    ((DataObjectHolder) viewHolder).helper.play();
                                }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                            }
                        }
//            }
                    }
                }
//            }
            }else{
                viewHolder = posts.findViewHolderForLayoutPosition(lastposition-1);
                if (viewHolder!=null && viewHolder instanceof DataObjectHolder) {
                    Logger.e("LLLLL_Play: ", String.valueOf(viewHolder.getAdapterPosition()));
                    if (viewHolder.getItemViewType() != UPLOAD_POST &&
                            viewHolder.getItemViewType() != SUGGESTED_PEOPLE &&
                            viewHolder.getItemViewType() != ADVERTISEMENT &&
                            viewHolder.getItemViewType() != POPULAR_POSTS &&
                            viewHolder.getItemViewType() != POSTING) {
                        if (((DataObjectHolder) viewHolder).helper != null) {
                            if (!((DataObjectHolder) viewHolder).helper.isPlaying()) {
                                ((DataObjectHolder) viewHolder).helper.play();
                            }*//* else {
                        ((DataObjectHolder) viewHolder).helper.play();
                    }*//*
                        }
                    }
//            }
                }
            }
        }catch (Exception e){

        }

    }
*/
}
