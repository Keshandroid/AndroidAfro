package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.FollowingHashtagActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.follow.Follow;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyFollowingAdapter extends RecyclerView.Adapter<MyFollowingAdapter.DataObjectHolder> {

    private Activity context;
    private ArrayList<Follow> followings;
    private static final int HASHTAG = 0, FOLLOWING = 1;
    private OnFollowingClickListener listener;

    public MyFollowingAdapter(Activity context, ArrayList<Follow> followings, OnFollowingClickListener listener) {
        this.context = context;
        this.followings = followings;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view;

        switch (viewType) {

            case HASHTAG:
                view = inflater.inflate(R.layout.item_following_hashtag, viewGroup, false);
                break;
            case FOLLOWING:
                view = inflater.inflate(R.layout.item_following, viewGroup, false);
                break;
            default:
                view = inflater.inflate(R.layout.item_following, viewGroup, false);
                break;
        }

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        switch (getItemViewType(position)) {

            case HASHTAG:
                holder.follow.setVisibility(View.GONE);
                setHashtagItemClickListener(holder);
                break;
            case FOLLOWING:
                setFollower(holder);
                break;
        }

    }

    private void setHashtagItemClickListener(DataObjectHolder holder) {
        holder.hashtags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, FollowingHashtagActivity.class));
            }
        });
    }

    private void setFollower(DataObjectHolder holder) {

        Follow follower = followings.get(holder.getAdapterPosition());

        String fullName = follower.getFirstName() + " " + follower.getLastName();
        holder.name.setText(fullName);

        String followers;
        if (follower.getFollowerIds()!=null)
            followers = follower.getFollowerIds().size() + " followers";
        else
            followers = "0 followers";

        holder.followers.setText(followers);

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + follower.getProfileImageUrl())
                .into(holder.profileImage);

        holder.follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listener.onFollowAndUnFollowToggleClick(follower,isChecked);
            }
        });

        holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onProfileClick(follower);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HASHTAG;
        } else {
            return FOLLOWING;
        }
    }

    @Override
    public int getItemCount() {
        if (followings == null)
            return 0;
        else
            return followings.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name, followers;
        CircleImageView profileImage;
        ToggleButton follow;
        CardView hashtags, profile;

        DataObjectHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            followers = itemView.findViewById(R.id.followers);
            profileImage = itemView.findViewById(R.id.profile_image);
            follow = itemView.findViewById(R.id.follow);
            hashtags = itemView.findViewById(R.id.hashtags);
            profile = itemView.findViewById(R.id.profile);
        }
    }

    public interface OnFollowingClickListener {
        void onFollowAndUnFollowToggleClick(Follow follower, Boolean isFollowed);
        void onProfileClick(Follow follower);
    }
}
