package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.AfroViewGroupPostActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.groups.Group;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AfroJoinedGroupAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<Group> groupsList;
    private OnLeaveGroupClickListener listener;

    public AfroJoinedGroupAdapter(Activity context, ArrayList<Group> groupsList, OnLeaveGroupClickListener listener) {
        this.context = context;
        this.groupsList = groupsList;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return groupsList.size();
    }

    @Override
    public Object getItem(int position) {
        return groupsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.item_group_grid, parent, false);
            convertView.setTag(holder);
            holder.groupIcon = convertView.findViewById(R.id.group_icon);
            holder.groupTitle = convertView.findViewById(R.id.group_title);
            holder.other = convertView.findViewById(R.id.other);
            holder.group = convertView.findViewById(R.id.group);
            holder.join = convertView.findViewById(R.id.join);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + groupsList.get(position).getGroupImage())
                .into(holder.groupIcon);

        holder.groupTitle.setText(groupsList.get(position).getGroupTitle());


        if(groupsList.get(position).getJoined()!=null && groupsList.get(position).getJoined()){
            holder.other.setVisibility(View.VISIBLE);
            holder.join.setVisibility(View.GONE);
        }else {
            holder.other.setVisibility(View.GONE);
            holder.join.setVisibility(View.VISIBLE);
        }


        setClickListener(holder, position);

        return convertView;
    }

    private void setClickListener(ViewHolder holder, int position) {

        holder.group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, AfroViewGroupPostActivity.class).putExtra("group",groupsList.get(position)));
            }
        });

        holder.other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onLeaveGroup(groupsList.get(position));
            }
        });

        holder.join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onGroupInvitationAcceptClick(groupsList.get(position));
            }
        });

    }

    class ViewHolder {
        ImageView groupIcon;
        TextView groupTitle;
        Button other,join;
        RelativeLayout group;
    }

    public interface OnLeaveGroupClickListener {
        void onLeaveGroup(Group group);
        void onGroupInvitationAcceptClick(Group group);

    }
}
