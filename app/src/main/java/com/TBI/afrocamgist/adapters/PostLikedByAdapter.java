package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.finduser.UserDetails;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostLikedByAdapter extends RecyclerView.Adapter<PostLikedByAdapter.DataObjectHolder> {

    private Activity context;
    private ArrayList<UserDetails> users;

    public PostLikedByAdapter(Activity context, ArrayList<UserDetails> users) {
        this.context = context;
        this.users = users;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_invite_friends, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        UserDetails user = users.get(position);

        holder.invite.setVisibility(View.GONE);

        String fullName = user.getFirstName() + " " + user.getLastName();
        holder.name.setText(fullName);

        Glide.with(holder.profilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + user.getProfileImageUrl())
                .into(holder.profilePicture);

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(user.getUserId()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class)
                            .putExtra("userId", user.getUserId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (users==null)
            return 0;
        else
            return users.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        CircleImageView profilePicture;
        TextView name;
        ToggleButton invite;

        DataObjectHolder(View itemView) {
            super(itemView);
            profilePicture = itemView.findViewById(R.id.profile_picture);
            name = itemView.findViewById(R.id.name);
            invite = itemView.findViewById(R.id.invite);
        }
    }
}
