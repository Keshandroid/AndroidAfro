package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.otheruser.OtherUser;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class InviteFriendsAdapter extends RecyclerView.Adapter<InviteFriendsAdapter.DataObjectHolder> {

    private Activity context;
    private ArrayList<OtherUser> users;
    private OnInviteFriendClickListener listener;


    public InviteFriendsAdapter(Activity context, ArrayList<OtherUser> users, OnInviteFriendClickListener listener) {
        this.context = context;
        this.users = users;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_invite_friends, viewGroup, false);

        return new DataObjectHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        OtherUser user = users.get(position);

        String fullName = user.getFirstName() + " " + user.getLastName();
        holder.name.setText(fullName);

        Glide.with(holder.profilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + user.getProfileImageUrl())
                .into(holder.profilePicture);

        if ("sent".equals(user.getButton().getButtonText().toLowerCase())) {
            holder.invite.setChecked(true);
            holder.invite.setEnabled(false);
        } else {
            holder.invite.setChecked(false);
            holder.invite.setEnabled(true);
        }

        holder.invite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed() && isChecked) {
                    holder.invite.setEnabled(false);
                    listener.onInviteFriendClick(user);
                    users.get(holder.getAdapterPosition()).getButton().setButtonText("sent");
                }
            }
        });

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(user.getUserId()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class)
                            .putExtra("userId", user.getUserId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (users==null)
            return 0;
        else
            return users.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        CircleImageView profilePicture;
        TextView name;
        ToggleButton invite;

        DataObjectHolder(View itemView) {
            super(itemView);
            profilePicture = itemView.findViewById(R.id.profile_picture);
            name = itemView.findViewById(R.id.name);
            invite = itemView.findViewById(R.id.invite);
        }
    }

    public interface OnInviteFriendClickListener {
        void onInviteFriendClick(OtherUser user);
    }
}
