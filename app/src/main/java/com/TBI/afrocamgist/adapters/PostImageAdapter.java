package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.BitmapUtils;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.ViewPostImageActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.Post;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.exoplayer2.ui.PlayerView;

import java.util.ArrayList;

import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;

public class PostImageAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private ArrayList<String> images;
    private OnImageSwipeListener listener;

    PlayerView playerView;
    ExoPlayerViewHelper helper;
    Uri mediaUri;
    ToggleButton volume;

    FrameLayout thumbnailLayout;
    ImageView thumbnail;
    View view;
    Activity context;
    int getAdapterPosition = 0;

    public PostImageAdapter(Activity context, ArrayList<String> images) {
        this.images = images;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if (images == null)
            return 0;
        else
            return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        view = inflater.inflate(R.layout.item_image, container, false);

        //PhotoView image = view.findViewById(R.id.image);

        ImageView image = view.findViewById(R.id.image);

        RelativeLayout videoLayout = view.findViewById(R.id.video_layout);

        videoLayout.setVisibility(View.GONE);
        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + images.get(position))
                .into(image);

        getAdapterPosition = position;
        container.addView(view);

        return view;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((FrameLayout) object);
    }

    public interface OnImageSwipeListener {
        void onImageSwiped();
    }

}
