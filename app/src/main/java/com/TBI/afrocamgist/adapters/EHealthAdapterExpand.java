package com.TBI.afrocamgist.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.AdvertReviewActivity;
import com.TBI.afrocamgist.activity.DashboardActivity;
import com.TBI.afrocamgist.activity.EHealthActivity;
import com.TBI.afrocamgist.activity.EHealthCustomerDetailActivity;
import com.TBI.afrocamgist.home.VideosAdapter;
import com.TBI.afrocamgist.model.ehealth.EHealth;
import com.TBI.afrocamgist.model.ehealth.EHealthSubPlans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EHealthAdapterExpand extends BaseExpandableListAdapter {
    private Context _context;
    private ArrayList<EHealth> _listDataHeader; // header titles
    // child data in format of header title, child title
    private ArrayList<EHealth> _listDataChild;

    public OnPlanDetailClickListener listener;

    public interface OnPlanDetailClickListener {
        void onPlanDetailClick(EHealthSubPlans subPlans);
    }

    /*public EHealthAdapterExpand(Context context, List<String> listDataHeader,
                                HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }*/

    public EHealthAdapterExpand(Context context, ArrayList<EHealth> eHealths, OnPlanDetailClickListener listener) {
        this._context = context;
        this._listDataHeader = eHealths;
        this._listDataChild = eHealths;
        this.listener = listener;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {

        return this._listDataChild.get(groupPosition).getSubPlans().get(childPosititon);

        //return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final EHealthSubPlans eHealthSubPlans = (EHealthSubPlans) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_health_plan, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        Button btnPrice = (Button) convertView.findViewById(R.id.btnPrice);


        TextView lblViewMore = (TextView) convertView.findViewById(R.id.lblViewMore);

        lblViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(listener!=null){
                    listener.onPlanDetailClick(eHealthSubPlans);
                }

            }
        });

        txtListChild.setText(eHealthSubPlans.getName());
        //btnPrice.setText(eHealthSubPlans.getPrice() +" "+ eHealthSubPlans.getCurrency());
        btnPrice.setText("$ "+eHealthSubPlans.getPrice());


        btnPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(_context, EHealthCustomerDetailActivity.class);
                intent.putExtra("ehealth_plan_id",eHealthSubPlans.getId());
                intent.putExtra("ehealth_price",eHealthSubPlans.getPrice());
                intent.putExtra("ehealth_plan_name",eHealthSubPlans.getName());
                _context.startActivity(intent);

                //_context.startActivity(new Intent(_context, EHealthCustomerDetailActivity.class));

            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(groupPosition).getSubPlans().size();
        //return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        EHealth eHealth = (EHealth) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_health_plan, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        TextView planCount = (TextView) convertView.findViewById(R.id.planCount);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(eHealth.getName());

        int count = groupPosition+1;

        planCount.setText(""+count);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
