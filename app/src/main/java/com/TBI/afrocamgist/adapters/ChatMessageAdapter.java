package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.ViewPostImageActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.BottomPopup;
import com.TBI.afrocamgist.fragment.CommentBottomSheetFragment;
import com.TBI.afrocamgist.listener.DoubleClickListener;
import com.TBI.afrocamgist.model.chat.Message;
import com.TBI.afrocamgist.model.post.Post;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.GsonBuilder;
import com.meg7.widget.SvgImageView;

import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.DataObjectHolder> implements MenuAdapter.OnMenuItemClickListener {

    private ArrayList<Message> messages;
    private static final int SEND = 0, RECEIVE = 1;
    private Activity context;
    private OnMessageClickListener listener;
    private String otherUserProfile;
    private PopupWindow popup;

    public ChatMessageAdapter(ArrayList<Message> messages, Activity context, OnMessageClickListener listener, String profileImageUrl) {
        this.messages = messages;
        this.context = context;
        this.listener = listener;
        this.otherUserProfile = profileImageUrl;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view;

        switch (viewType) {

            case SEND:
                view = inflater.inflate(R.layout.item_message_send,viewGroup, false);
                break;
            case RECEIVE:
                view = inflater.inflate(R.layout.item_message_receive,viewGroup, false);
                break;
            default:
                view = inflater.inflate(R.layout.item_message_send,viewGroup, false);
                break;
        }

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {
        Message message = messages.get(position);

        Logger.d("Message11",new GsonBuilder().setPrettyPrinting().create().toJson(message));

        if ("".equals(message.getMessageImage())) {
            holder.imageMessage.setVisibility(View.GONE);
        } else {
            holder.imageMessage.setVisibility(View.VISIBLE);
            Glide.with(holder.imageMessage)
                    .load(UrlEndpoints.MEDIA_BASE_URL + message.getMessageImage())
                    .into(holder.imageMessage);

            new Zoomy.Builder(context)
                    .target(holder.imageMessage)
                    .enableImmersiveMode(false)
                    .tapListener(v -> context.startActivity(new Intent(context, ViewPostImageActivity.class)
                            .putExtra("position",0)
                            .putExtra("images",new ArrayList<String>(Arrays.asList(message.getMessageImage().split(","))))))
                    //.doubleTapListener(v -> holder.callMethod())
                    .register();

        }



        if ("".equals(message.getMessageText())) {
            holder.textMessage.setVisibility(View.GONE);
        } else {
            holder.textMessage.setVisibility(View.VISIBLE);
            holder.textMessage.setText(message.getMessageText());
        }

        if("read".equalsIgnoreCase(message.getMessageStatus())){
            holder.tickMark.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }else {
            holder.tickMark.setTextColor(context.getResources().getColor(R.color.darkGrey1));
        }

        holder.time.setText(Utils.getFormattedTimeForChatMessage(message.getCreatedDate()));

        if(message.isLiked()){
            holder.my_like.setVisibility(View.VISIBLE);
        }else {
            holder.my_like.setVisibility(View.GONE);
        }

        if(message.getMessage_reply_id()!=null){
            holder.llReplyMessage.setVisibility(View.VISIBLE);
            holder.textMessage.setText(message.getMessage_reply_text());
            holder.reply_message.setText(message.getMessageText());
        }else {
            holder.llReplyMessage.setVisibility(View.GONE);
        }


        if(message.getLike_count()!=null){
            if(message.isLiked() && message.getLike_count() == 2){
                holder.llLikeMessage.setVisibility(View.VISIBLE);
                if(message.getLikedBy()!=null && !message.getLikedBy().isEmpty()){
                    holder.my_like.setVisibility(View.VISIBLE);
                    holder.other_user_like.setVisibility(View.VISIBLE);
                    /*Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                            .into(holder.my_like);
                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + otherUserProfile)
                            .into(holder.other_user_like);*/

                    Glide.with(holder.my_like)
                            .asBitmap()
                            .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl() + "?width=50&height=50")
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    holder.my_like.setImageBitmap(resource);

                                    Glide.with(holder.my_like)
                                            .asBitmap()
                                            .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                                            .into(new SimpleTarget<Bitmap>() {
                                                @Override
                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                    holder.my_like.setImageBitmap(resource);
                                                }
                                            });
                                }
                            });


                    Glide.with(holder.other_user_like)
                            .asBitmap()
                            .load(UrlEndpoints.MEDIA_BASE_URL + otherUserProfile + "?width=50&height=50")
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    holder.other_user_like.setImageBitmap(resource);

                                    Glide.with(holder.other_user_like)
                                            .asBitmap()
                                            .load(UrlEndpoints.MEDIA_BASE_URL + otherUserProfile)
                                            .into(new SimpleTarget<Bitmap>() {
                                                @Override
                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                    holder.other_user_like.setImageBitmap(resource);
                                                }
                                            });
                                }
                            });
                }
            }else if(message.isLiked() && message.getLike_count() == 1){
                holder.llLikeMessage.setVisibility(View.VISIBLE);
                holder.my_like.setVisibility(View.VISIBLE);
                holder.other_user_like.setVisibility(View.GONE);

                Glide.with(holder.my_like)
                        .asBitmap()
                        .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl() + "?width=50&height=50")
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                holder.my_like.setImageBitmap(resource);

                                Glide.with(holder.my_like)
                                        .asBitmap()
                                        .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                holder.my_like.setImageBitmap(resource);
                                            }
                                        });
                            }
                        });

                /*Glide.with(context)
                        .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                        .into(holder.my_like);*/
            }else if(!message.isLiked() && message.getLike_count() == 1){
                holder.llLikeMessage.setVisibility(View.VISIBLE);
                holder.other_user_like.setVisibility(View.VISIBLE);
                holder.my_like.setVisibility(View.GONE);

                Glide.with(holder.other_user_like)
                        .asBitmap()
                        .load(UrlEndpoints.MEDIA_BASE_URL + otherUserProfile + "?width=50&height=50")
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                holder.other_user_like.setImageBitmap(resource);

                                Glide.with(holder.other_user_like)
                                        .asBitmap()
                                        .load(UrlEndpoints.MEDIA_BASE_URL + otherUserProfile)
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                holder.other_user_like.setImageBitmap(resource);
                                            }
                                        });
                            }
                        });

                /*Glide.with(context)
                        .load(UrlEndpoints.MEDIA_BASE_URL + otherUserProfile)
                        .into(holder.other_user_like);*/
            }else {
                holder.llLikeMessage.setVisibility(View.GONE);
            }
        }


        //edit,delete and reply menu in bottomsheet (long press)
        /*holder.cardMessage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(message.getFrom().equalsIgnoreCase("me")){
                    BottomPopup bottomPopup = new BottomPopup();
                    BottomPopup.newInstance(message).show(((AppCompatActivity) context).getSupportFragmentManager(), bottomPopup.getTag());
                }else {
                    if(listener!=null){
                        listener.onReceiveMessageReply(message.getMessageId(),message.getMessageText(),message.getFromId());
                    }
                }
                return true;
            }
        });*/

        //edit,delete and reply inside popup menu
        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showMenu(holder.imgEdit, holder.getAdapterPosition());

                if(message.getFrom().equalsIgnoreCase("me")){
                    BottomPopup bottomPopup = new BottomPopup();
                    BottomPopup.newInstance(message,"sentMessage").show(((AppCompatActivity) context).getSupportFragmentManager(), bottomPopup.getTag());
                }else {
                    BottomPopup bottomPopup = new BottomPopup();
                    BottomPopup.newInstance(message,"receivedMessage").show(((AppCompatActivity) context).getSupportFragmentManager(), bottomPopup.getTag());

                    /*if(listener!=null){
                        listener.onReceiveMessageReply(message.getMessageId(),message.getMessageText(),message.getFromId());
                    }*/
                }
            }
        });

        holder.cardMessage.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onDoubleClick() {
                if(listener!=null){
                    if(!message.isLiked() && message.getLike_count() == 1){
                        messages.get(holder.getAdapterPosition()).setLiked(true);
                        messages.get(holder.getAdapterPosition()).setLike_count(messages.get(holder.getAdapterPosition()).getLike_count()+1);

                        holder.llLikeMessage.setVisibility(View.VISIBLE);
                        holder.my_like.setVisibility(View.VISIBLE);

                        Glide.with(holder.my_like)
                                .asBitmap()
                                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl() + "?width=50&height=50")
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                        holder.my_like.setImageBitmap(resource);

                                        Glide.with(holder.my_like)
                                                .asBitmap()
                                                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                                                .into(new SimpleTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        holder.my_like.setImageBitmap(resource);
                                                    }
                                                });
                                    }
                                });

                        /*Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                                .into(holder.my_like);*/
                        listener.onMessageDoubleClick(message.getMessageId());
                    }else if(!message.isLiked() && message.getLike_count() == 0){
                        messages.get(holder.getAdapterPosition()).setLiked(true);
                        messages.get(holder.getAdapterPosition()).setLike_count(messages.get(holder.getAdapterPosition()).getLike_count()+1);

                        holder.llLikeMessage.setVisibility(View.VISIBLE);
                        holder.other_user_like.setVisibility(View.GONE);
                        holder.my_like.setVisibility(View.VISIBLE);

                        Glide.with(holder.my_like)
                                .asBitmap()
                                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl() + "?width=50&height=50")
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                        holder.my_like.setImageBitmap(resource);

                                        Glide.with(holder.my_like)
                                                .asBitmap()
                                                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                                                .into(new SimpleTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        holder.my_like.setImageBitmap(resource);
                                                    }
                                                });
                                    }
                                });

                       /* Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                                .into(holder.my_like);*/
                        listener.onMessageDoubleClick(message.getMessageId());
                    }

                }
            }

            @Override
            public void onSingleClick() {
                if(listener!=null){
                    if(message.isLiked() && message.getLike_count() == 2){
                        messages.get(holder.getAdapterPosition()).setLiked(false);
                        messages.get(holder.getAdapterPosition()).setLike_count(messages.get(holder.getAdapterPosition()).getLike_count()-1);

                        holder.my_like.setVisibility(View.GONE);
                        holder.other_user_like.setVisibility(View.VISIBLE);
                        listener.onMessageSingleClick(message.getMessageId());
                    }else if(message.isLiked() && message.getLike_count() == 1){
                        messages.get(holder.getAdapterPosition()).setLiked(false);
                        messages.get(holder.getAdapterPosition()).setLike_count(messages.get(holder.getAdapterPosition()).getLike_count()-1);

                        holder.my_like.setVisibility(View.GONE);
                        holder.other_user_like.setVisibility(View.GONE);
                        holder.llLikeMessage.setVisibility(View.GONE);
                        listener.onMessageSingleClick(message.getMessageId());
                    }

                }
            }
        });
    }

    private void showMenu(View menu, int position) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int popupWidth = 450;
        int popupHeight = LinearLayout.LayoutParams.WRAP_CONTENT;

        LinearLayout viewGroup = context.findViewById(R.id.menu_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.drop_down_menu, viewGroup);
        popupMenu(layout, position);

        popup = new PopupWindow(layout, popupWidth, popupHeight);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        if (position == (messages.size() - 1))
            popup.showAsDropDown(menu, 0, -300, Gravity.TOP);
        else
            popup.showAsDropDown(menu);
    }

    private void popupMenu(View layout, int position) {

        RecyclerView menu = layout.findViewById(R.id.menuList);
        menu.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        menu.setLayoutManager(mLayoutManager);
        MenuAdapter adapter;

        if(getItemViewType(position) == SEND){
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.chat_menu_sender))), position, this);
        }else {
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.chat_menu_receiver))), position, this);
        }
        menu.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        if (messages == null)
            return 0;
        else
            return messages.size();
    }

    public void fetchNewMessages(ArrayList<Message> messages) {
        this.messages = new ArrayList<>();
        this.messages.addAll(messages);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {

        if(LocalStorage.getUserDetails().getUserId().equals(messages.get(position).getFromId()))
            return SEND;
        else
            return RECEIVE;
    }

    @Override
    public void onMenuItemClicked(String item, int position) {
        if (popup != null && popup.isShowing())
            popup.dismiss();

        switch (item.toLowerCase()) {

            case "edit":
                listener.onDialogDismiss("edit",messages.get(position).getMessageId(),messages.get(position).getMessageText(),messages.get(position).getToId());
                Toast.makeText(context,"Edit",Toast.LENGTH_SHORT).show();
                break;
            case "delete":
                listener.onDialogDismiss("delete",messages.get(position).getMessageId(),messages.get(position).getMessageText(),messages.get(position).getToId());
                Toast.makeText(context,"Delete",Toast.LENGTH_SHORT).show();
                break;
            case "reply":
                listener.onDialogDismiss("reply",messages.get(position).getMessageId(),messages.get(position).getMessageText(),messages.get(position).getFromId());
                Toast.makeText(context,"Reply",Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }


    static class DataObjectHolder extends RecyclerView.ViewHolder {

        ImageView imageMessage,imgEdit;
        TextView textMessage, time, tickMark, reply_message;
        CardView cardMessage;
        LinearLayout llReplyMessage,llLikeMessage;
        //CircleImageView my_like,other_user_like;
        SvgImageView my_like,other_user_like;

        DataObjectHolder(View itemView) {
            super(itemView);
            imageMessage = itemView.findViewById(R.id.image_message);
            textMessage = itemView.findViewById(R.id.text_message);
            time = itemView.findViewById(R.id.time);
            tickMark = itemView.findViewById(R.id.tickMark);
            cardMessage = itemView.findViewById(R.id.cardMessage);
            other_user_like = itemView.findViewById(R.id.other_user_like);
            my_like = itemView.findViewById(R.id.my_like);
            llLikeMessage = itemView.findViewById(R.id.llLikeMessage);
            llReplyMessage = itemView.findViewById(R.id.ll_reply_message);
            reply_message = itemView.findViewById(R.id.reply_message);
            imgEdit = itemView.findViewById(R.id.imgEdit);

        }

    }

    public interface OnMessageClickListener{
        void onMessageSingleClick(int messageId);
        void onMessageDoubleClick(int messageId);
        void onReceiveMessageReply(Integer messageId, String messageText, Integer fromId);

        void onDialogDismiss(String action,int messageID, String messageText, int toId);
    }

}
