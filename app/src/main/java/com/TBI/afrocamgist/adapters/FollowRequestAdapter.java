package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.follow.Follow;
import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FollowRequestAdapter extends RecyclerView.Adapter<FollowRequestAdapter.DataObjectHolder> {

    private Activity context;
    private List<Follow> followRequests;
    private OnRequestClickListener listener;

    public FollowRequestAdapter(Activity context, List<Follow> followRequests, OnRequestClickListener listener) {
        this.context = context;
        this.followRequests = followRequests;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_follow_request, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        Follow follower = followRequests.get(position);

        String fullName = follower.getFirstName() + " " + follower.getLastName();
        holder.name.setText(fullName);

        if(follower.getFollowerIds()==null || follower.getFollowerIds().isEmpty()){
            String followers = "0 followers";
            holder.followers.setText(followers);
        }else {
            String followers = follower.getFollowerIds().size() + " followers";
            holder.followers.setText(followers);
        }

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + follower.getProfileImageUrl())
                .into(holder.profileImage);

        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onAcceptRequest(follower);
                }
            }
        });

        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDenyRequest(follower);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (followRequests == null)
            return 0;
        else
            return followRequests.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name, followers;
        CircleImageView profileImage;
        Button accept, reject;

        DataObjectHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            followers = itemView.findViewById(R.id.followers);
            profileImage = itemView.findViewById(R.id.profile_image);
            accept = itemView.findViewById(R.id.accept);
            reject = itemView.findViewById(R.id.reject);
        }
    }

    public interface OnRequestClickListener {
        void onAcceptRequest(Follow follower);
        void onDenyRequest(Follow follower);
    }

}
