package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.chat.Conversation;
import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.user.User;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ArchivedAdapter extends RecyclerView.Adapter<ArchivedAdapter.DataObjectHolder> {

    private Activity context;
    private ArrayList<Conversation> archivedList;
    private static Dialog popup;
    private OnItemClickListener listener;


    public ArchivedAdapter(Activity context, ArrayList<Conversation> conversations,OnItemClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.archivedList = conversations;
    }

    public void notifydata(ArrayList<Conversation> conversations){
        this.archivedList = conversations;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_archive_user, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        Conversation archivedUser = archivedList.get(position);

        String fullName = archivedUser.getFirstName() + " " + archivedUser.getLastName();
        holder.name.setText(fullName);


        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + archivedUser.getProfileImageUrl())
                .into(holder.profileImage);

        /*holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(archivedUser.getUserId()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class).putExtra("userId",archivedUser.getUserId()));
            }
        });*/

        holder.profile.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showAlert(context,listener,archivedUser);
                return true;
            }
        });

    }

    public static void showAlert(Activity activity, OnItemClickListener listener, Conversation archivedUser) {

        if (activity!=null && !activity.isFinishing()) {
            popup = new Dialog(activity, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_archived_remove);
            popup.setCancelable(true);
            popup.show();

            popup.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popup.dismiss();
                    if(listener!=null){
                        listener.onRemoveArchived(archivedUser);
                    }
                }
            });

            popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popup.dismiss();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (archivedList == null)
            return 0;
        else
            return archivedList.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name;
        CircleImageView profileImage;
        CardView profile;

        DataObjectHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            profileImage = itemView.findViewById(R.id.profile_image);
            profile = itemView.findViewById(R.id.profile);
        }
    }

    public interface OnItemClickListener {
        void onRemoveArchived(Conversation user);

    }

}
