package com.TBI.afrocamgist.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.finduser.UserDetails;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class FindUserAdapter extends RecyclerView.Adapter<FindUserAdapter.DataObjectHolder> {

    private ArrayList<UserDetails> userList;
    private OnProfileClickListener listener;

    public FindUserAdapter(ArrayList<UserDetails> userList, OnProfileClickListener listener) {
        this.userList = userList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_following, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        UserDetails user = userList.get(position);

        holder.name.setText(user.getFullName());

        String followersCount;
        if (user.getFollowerIds() == null)
            followersCount = "0 followers";
        else
            followersCount = user.getFollowerIds().size() + " followers";

        holder.followers.setText(followersCount);

        if(user.getBlockedByMe() != null){
            if (user.getBlockedByMe()) {
                holder.follow.setVisibility(View.GONE);
                holder.block.setVisibility(View.VISIBLE);
            } else {
                if ("follow".equals(user.getRequestButtons().get(0).getButtonText().toLowerCase())){
                    holder.follow.setChecked(false);
                } else{
                    holder.follow.setChecked(true);
                }

            }
        }



        Glide.with(holder.profileImage)
                .load(UrlEndpoints.MEDIA_BASE_URL + user.getProfileImageUrl())
                .into(holder.profileImage);

        holder.profile.setOnClickListener(v -> listener.onProfileClick(user.getUserId(), user));

        holder.follow.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (buttonView.isPressed())
                listener.onFollowAndUnfollowToggleClick(user.getUserId(), isChecked);
        });

        holder.block.setOnClickListener(v -> {
            Logger.e("LLLL_Block: ", String.valueOf(user.getBlockedByMe()));
            listener.blockUnblockUser(user.getUserId(), user.getBlockedByMe());
            holder.block.setVisibility(View.GONE);
            holder.follow.setVisibility(View.VISIBLE);
        });
    }

    @Override
    public int getItemCount() {
        if (userList == null)
            return 0;
        else
            return userList.size();
    }


    static class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView name, followers;
        CircleImageView profileImage;
        ToggleButton follow;
        CardView profile;
        TextView block;

        DataObjectHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            followers = itemView.findViewById(R.id.followers);
            block = itemView.findViewById(R.id.block);
            profileImage = itemView.findViewById(R.id.profile_image);
            follow = itemView.findViewById(R.id.follow);
            profile = itemView.findViewById(R.id.profile);
        }

    }

    public interface OnProfileClickListener {
        void onProfileClick(int userId, UserDetails userDetails);

        void onFollowAndUnfollowToggleClick(int userId, boolean isFollowed);

        void blockUnblockUser(int userId, boolean isBlock);
    }


}
