package com.TBI.afrocamgist.adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.AfroViewGroupPostActivity;
import com.TBI.afrocamgist.activity.CreateGroupActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.groups.Group;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;

public class AfroMyGroupAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Group> groupsList;
    private OnMyGroupClickListener listener;

    public AfroMyGroupAdapter(Context context, ArrayList<Group> groupsList, OnMyGroupClickListener listener) {
        this.context=context;
        this.groupsList=groupsList;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return groupsList.size();
    }

    @Override
    public Object getItem(int position) {
        return groupsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();

            if ("create group".equals(groupsList.get(position).getGroupTitle().toLowerCase())){
                convertView = inflater.inflate(R.layout.item_create_group, parent, false);
                holder.createGroup = convertView.findViewById(R.id.create_group);
                holder.createGroup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, CreateGroupActivity.class));
                    }
                });
            }
            else {
                convertView = inflater.inflate(R.layout.item_group_grid, parent, false);

                holder.groupIcon = convertView.findViewById(R.id.group_icon);
                holder.groupTitle = convertView.findViewById(R.id.group_title);
                holder.other = convertView.findViewById(R.id.other);
                holder.group = convertView.findViewById(R.id.group);

                Glide.with(holder.groupIcon)
                        .asBitmap()
                        .load(UrlEndpoints.MEDIA_BASE_URL + groupsList.get(position).getGroupImage() + "?width=50&height=50")
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                holder.groupIcon.setImageBitmap(resource);

                                Glide.with(holder.groupIcon)
                                        .asBitmap()
                                        .load(UrlEndpoints.MEDIA_BASE_URL + groupsList.get(position).getGroupImage())
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                holder.groupIcon.setImageBitmap(resource);
                                            }
                                        });
                            }
                        });

                /*Glide.with(context)
                        .load(UrlEndpoints.MEDIA_BASE_URL + groupsList.get(position).getGroupImage())
                        .into(holder.groupIcon);*/



                holder.groupTitle.setText(groupsList.get(position).getGroupTitle());
                holder.other.setVisibility(View.VISIBLE);
                holder.other.setText(R.string.delete);

                setClickListener(holder, position);

            }
        }

        return convertView;
    }

    private void setClickListener(ViewHolder holder, int position) {

        holder.group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, AfroViewGroupPostActivity.class).putExtra("group",groupsList.get(position)));
            }
        });

        holder.other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDeleteClicked(groupsList.get(position));
            }
        });
    }

    class ViewHolder {
        ImageView groupIcon;
        TextView groupTitle;
        Button other;
        CardView createGroup;
        RelativeLayout group;
    }

    public interface OnMyGroupClickListener {
        void onDeleteClicked(Group group);
    }
}
