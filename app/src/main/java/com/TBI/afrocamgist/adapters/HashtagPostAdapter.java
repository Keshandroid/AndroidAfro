package com.TBI.afrocamgist.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.util.DisplayMetrics;

import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.View.DataObjectHolder;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.PostLikedByActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.ReportPostActivity;
import com.TBI.afrocamgist.activity.SharePostActivity;
import com.TBI.afrocamgist.activity.ViewPostImageActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.home.VideosAdapter;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.post.Post;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.GsonBuilder;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;

public class HashtagPostAdapter extends RecyclerView.Adapter<HashtagPostAdapter.RecyclerObjectHolder> implements MenuAdapter.OnMenuItemClickListener,
                        TrendingHashtagAdapter.OnTrendingHashClickListener{

    private Activity context;
    private ArrayList<Post> posts;
    private PopupWindow popup;
    private OnPostClickListener listener;
    private Hashtag hash;
    private ArrayList<Hashtag> trendingHashtags;
    private TrendingHashtagAdapter adapter;
    private int progress;
    private boolean folowReq = false;
    private boolean isFollow = false;
    private static final int HASHTAG_DETAILS = 0, UPLOAD_POST = 1, TRENDING_HASHTAG = 2, POST = 3, POSTING = 4;
    public static boolean isMute = true;

    public HashtagPostAdapter(Activity context, ArrayList<Post> posts, Hashtag hash, OnPostClickListener listener) {
        this.context = context;
        this.posts = posts;
        this.hash = hash;
        this.trendingHashtags = new ArrayList<>();
        this.progress = 0;
        adapter = new TrendingHashtagAdapter(context, this.trendingHashtags, this);
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view;

        switch (viewType) {

            case HASHTAG_DETAILS:
                view = inflater.inflate(R.layout.item_hashtag_details, viewGroup, false);
                return new RecyclerObjectHolder(view);
            case UPLOAD_POST:
                view = inflater.inflate(R.layout.item_upload_post, viewGroup, false);
                return new RecyclerObjectHolder(view);
            case TRENDING_HASHTAG:
                view = inflater.inflate(R.layout.item_horizontal_list, viewGroup, false);
                return new RecyclerObjectHolder(view);
            case POSTING:
                view = inflater.inflate(R.layout.item_uploading_post, viewGroup, false);
                return new RecyclerObjectHolder(view);
            default:
                view = inflater.inflate(R.layout.item_group_post, viewGroup, false);
                return new DataObjectHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerObjectHolder holder, int position) {

        switch (getItemViewType(position)) {

            case HASHTAG_DETAILS:
                setHashtagDetailsData(holder);
                setHashtagClickListener(holder);
                break;
            case UPLOAD_POST:
                holder.write.setText(R.string.writeSomething);
                setUploadPostClickListener(holder);
                break;
            case TRENDING_HASHTAG:
                setTrendingHashtagRecyclerView(holder);
                break;
            case POSTING:
                showPostingView(holder);
                break;
            case POST:

                if ("shared".equals(posts.get(position).getPostType())) {

                    listener.onPostViewCount(posts.get(position).getPostId());

                    holder.sharedLayout.setVisibility(View.VISIBLE);
                    holder.share.setVisibility(View.GONE); //Cannot share a shared post (same in website)
                    holder.follow.setVisibility(View.GONE);

                    /*if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                        holder.sharedFollow.setVisibility(View.GONE);
                    } else {
                        holder.sharedFollow.setVisibility(View.VISIBLE);
                        if (posts.get(position) != null && posts.get(position).getFollowing() != null) {
                            holder.sharedFollow.setChecked(posts.get(position).getFollowing());
                            holder.sharedFollow.setEnabled(!posts.get(position).getFollowing());
                        } else {
                            holder.sharedFollow.setChecked(true);
                            holder.sharedFollow.setEnabled(false);
                        }
                    }*/

                    //New Code for shared post follow button
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                        holder.sharedFollow.setVisibility(View.GONE);
                    } else {
                        holder.sharedFollow.setVisibility(View.VISIBLE);

                        if (posts.get(position) != null && posts.get(position).getFollowing() != null) {
                            if (posts.get(position).getFollowing()) {
                                holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.orange));
                                holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                holder.sharedFollow.setText(context.getResources().getString(R.string.following));
                                holder.sharedFollow.setEnabled(false);
                            } else {
                                holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.blue));
                                holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_blue_border);
                                holder.sharedFollow.setText(context.getResources().getString(R.string.follow));
//                                Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                                holder.sharedFollow.setEnabled(true);

                                if(checkIsRequested(posts.get(position))){
                                    holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                    holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.colorAccent));
                                    holder.sharedFollow.setText(R.string.requested);
                                    holder.sharedFollow.setEnabled(false);
                                }
                            }
                        } else {
                            holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.blue));
                            holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_blue_border);
                            holder.sharedFollow.setText(context.getResources().getString(R.string.follow));
//                            Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                            holder.sharedFollow.setEnabled(true);
                        }
                    }

                    if ("video".equals(posts.get(position).getSharedPost().getPostType())) {
                        holder.socialShare.setVisibility(View.VISIBLE);
                        Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + posts.get(position).getSharedPost().getThumbnail())
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                .into(holder.thumbnail);
                    }else {
                        holder.socialShare.setVisibility(View.GONE);
                    }
                } else {

                    listener.onPostViewCount(posts.get(position).getPostId());

                    holder.sharedLayout.setVisibility(View.GONE);
                    holder.share.setVisibility(View.VISIBLE);
                    holder.sharedFollow.setVisibility(View.GONE);

                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(position).getUserId())) {
                        holder.follow.setVisibility(View.GONE);
                    } else {
                        holder.follow.setVisibility(View.VISIBLE);
                        if (posts.get(position)!=null && posts.get(position).getFollowing()!=null) {
                            if (posts.get(position).getFollowing()){
                                holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                                holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                holder.follow.setText(context.getResources().getString(R.string.following));
                                holder.follow.setEnabled(false);
                                isFollow = false;
                            } else {
                                holder.follow.setTextColor(context.getResources().getColor(R.color.blue));
                                holder.follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                                holder.follow.setText(context.getResources().getString(R.string.follow));
//                                Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                                holder.follow.setEnabled(true);
                                isFollow = true;

                                if(checkIsRequested(posts.get(position))){
                                    holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                                    holder.follow.setTextColor(context.getResources().getColor(R.color.colorAccent));
                                    holder.follow.setText(R.string.requested);
                                    isFollow = false;
                                }

                            }
                        } else {
                            holder.follow.setTextColor(context.getResources().getColor(R.color.blue));
                            holder.follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                            holder.follow.setText(context.getResources().getString(R.string.follow));
//                            Logger.e("LLLL_Folow: ", String.valueOf(!posts.get(position).getFollowing()));
                            holder.follow.setEnabled(true);
                            isFollow = true;
                        }
                    }

                    if ("video".equals(posts.get(position).getPostType())) {
                        holder.socialShare.setVisibility(View.VISIBLE);
                        Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + posts.get(position).getThumbnail())
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                .into(holder.thumbnail);
                    }else {
                        holder.socialShare.setVisibility(View.GONE);
                    }
                }

                setPostData(posts.get(position), holder, false);
                setClickListener(holder);
                break;
            default:
                break;
        }
    }

    public void setHashtagDetails(Hashtag hash) {
        this.hash = hash;
        notifyDataSetChanged();
    }

    private void setHashtagDetailsData(RecyclerObjectHolder holder) {

        if (hash != null) {

            Logger.d("hashDetail1",new GsonBuilder().setPrettyPrinting().create().toJson(hash));

            Glide.with(holder.hashtagCover)
                    .load(UrlEndpoints.MEDIA_BASE_URL + hash.getCoverImage())
                    .into(holder.hashtagCover);

            String hashtagSlug = "#" + hash.getHashtagSlug();
            holder.hashtag.setText(hashtagSlug);

            holder.postCount.setText(String.valueOf(hash.getPostCount()));

            if (hash.getFollowers() != null)
                holder.followers.setText(String.valueOf(hash.getFollowers().size()));
            else
                holder.followers.setText("-");

            if (hash.getFollowedByMe() != null) {
                if (hash.getFollowedByMe())
                    holder.followHashtag.setChecked(true);
                else
                    holder.followHashtag.setChecked(false);
            }

        }

    }

    private boolean checkIsRequested(Post profileUser) {
        if(profileUser!=null)
        {
            boolean isRequestSend = false;
            if(profileUser.getRequestButtons().get(0).getButtonText().equalsIgnoreCase("Requested")){
                isRequestSend = true;
            }
            return isRequestSend;
        }
        return false;
    }

    private void setHashtagClickListener(RecyclerObjectHolder holder) {

        holder.followHashtag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listener.onHashtagFollow(isChecked);
            }
        });

        holder.back.setOnClickListener(v -> context.onBackPressed());
    }

    private void setUploadPostClickListener(RecyclerObjectHolder holder) {

        Glide.with(holder.myProfilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                .into(holder.myProfilePicture);

        holder.write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCreatePostClicked();
                /*context.startActivityForResult(new Intent(context, CreatePostActivity.class)
                        .putExtra("postFor","hashtag")
                        .putExtra("hashtag",hash.getHashtagSlug()),100);*/
            }
        });
    }

    private void setTrendingHashtagRecyclerView(RecyclerObjectHolder holder) {

        holder.listTitle.setText(R.string.trendingHashtags);
        holder.trendingHashtagList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.trendingHashtagList.setAdapter(adapter);
    }

    public void setTrendingHashtag(ArrayList<Hashtag> trendingHashtags) {

        if (adapter != null) {
            this.trendingHashtags.addAll(trendingHashtags);
            adapter.notifyDataSetChanged();
        }
    }

    private void showPostingView(RecyclerObjectHolder holder) {

        holder.uploadProgress.setProgress(getUploadProgress());

    }

    public void setUploadProgress(Integer progress) {
        this.progress = progress;
    }

    private Integer getUploadProgress() {
        return this.progress;
    }

    private void setPostData(Post post, RecyclerObjectHolder holder, Boolean isSharedPost) {

        if ("shared".equals(post.getPostType())) {

            setSharedPostData(post, holder);
            setPostData(post.getSharedPost(), holder, true);

        } else {

            if(post.getUser_name()!=null && !post.getUser_name().isEmpty()){
                holder.name.setText(post.getUser_name());
            }else {
                String fullName = post.getFirstName() + " " + post.getLastName();
                holder.name.setText(fullName);
            }

            if(post.getTagged_id()!=null && !post.getTagged_id().isEmpty()){
                holder.icTag.setVisibility(View.VISIBLE);
            }else {
                holder.icTag.setVisibility(View.GONE);
            }

            holder.time.setText(Utils.getSocialStyleTime(post.getPostDate()));

            if (post.getPostLatLng() != null && !"".equals(post.getPostLatLng())) {
                holder.location.setVisibility(View.VISIBLE);
                holder.location.setText(post.getPostLocation());
            } else {
                holder.location.setVisibility(View.GONE);
            }

            if (!isSharedPost) {
                if (post != null) {

                    if(post.getLiked() != null){

                        if (post.getLiked()) {
                            holder.like.setChecked(true);
                            holder.likesCount.setTextColor(Color.parseColor("#FF7400"));
                        } else {
                            holder.like.setChecked(false);
                            holder.likesCount.setTextColor(Color.parseColor("#004F95"));
                        }

                        if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
                            holder.menu.setVisibility(View.VISIBLE);
                        } else {
                            holder.menu.setVisibility(View.GONE);
                        }
                        String likeCount = post.getLikeCount() + "";
                        holder.likesCount.setText(likeCount);
                        String commentCount = post.getCommentCount() + "";
                        holder.comment.setText(commentCount);


                    }
                }
            }

            if(post.getVideo_play_count()!=null){
                Logger.d("name_count",post.getFirstName()+"==="+post.getVideo_play_count());
                if(post.getVideo_play_count()==1){
                    holder.videoCount.setText(Utils.prettyCount(post.getVideo_play_count())+" "+"view");
                }else {
                    holder.videoCount.setText(Utils.prettyCount(post.getVideo_play_count())+" "+"views");
                }
            }
            holder.postText.setText(post.getPostText());
            Glide.with(context)
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                    .into(holder.profilePicture);
            setPostDetails(post, holder);
        }
    }

    private void setSharedPostData(Post post, RecyclerObjectHolder holder) {

        String fullName = post.getFirstName() + " " + post.getLastName();
        holder.sharedName.setText(fullName);
        holder.sharedTime.setText(Utils.getSocialStyleTime(post.getPostDate()));
        holder.sharedPostText.setText(post.getPostText());

        if (post.getPostLatLng() != null && !"".equals(post.getPostLatLng())) {
            holder.sharedLocation.setVisibility(View.VISIBLE);
            holder.sharedLocation.setText(post.getPostLocation());
        } else {
            holder.sharedLocation.setVisibility(View.GONE);
        }

        if (post.getLiked()) {
            holder.like.setChecked(true);
            holder.likesCount.setTextColor(Color.parseColor("#FF7400"));
        } else {
            holder.like.setChecked(false);
            holder.likesCount.setTextColor(Color.parseColor("#004F95"));
        }

        if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
            holder.menu.setVisibility(View.VISIBLE);
        } else {
            holder.menu.setVisibility(View.GONE);
        }
        String likeCount = post.getLikeCount() + "";
        holder.likesCount.setText(likeCount);
        String commentCount = post.getCommentCount() + "";
        holder.comment.setText(commentCount);

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(holder.sharedProfileImage);
    }

    private void setPostDetails(Post post, RecyclerObjectHolder holder) {

        switch (post.getPostType()) {

            case "text":
                holder.layoutOnlyImages.setVisibility(View.GONE);
                holder.videoLayout.setVisibility(View.GONE);

                if (post.getBackgroundImagePost()) {

                    holder.layoutPostWithBackgroundImage.setVisibility(View.VISIBLE);

                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getBackgroundImage())
                            .into(holder.backgroundImage);

                    holder.postTextOnImage.setText(post.getPostText());
                    holder.postText.setText("");
                } else {
                    holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);
                }

                break;
            case "image":
                holder.layoutOnlyImages.setVisibility(View.VISIBLE);
                holder.videoLayout.setVisibility(View.GONE);
                holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.placeholder);

                if (post.getPostImage().size() > 1) {
                    String count = (post.getPostImage().size() - 1) + "";

                    Glide.with(holder.postImage)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .placeholder(R.drawable.placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(holder.postImage);

                    Glide.with(context)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(1))
                            .into(holder.moreImages);

                    holder.mapPostImage.setVisibility(View.GONE);
                    holder.postImage.setVisibility(View.VISIBLE);
                    holder.moreImagesLayout.setVisibility(View.VISIBLE);

                    holder.imageCount.setText(count);
                } else {
                    if (post.getMapPost()) {

                        holder.mapPostImage.setVisibility(View.VISIBLE);
                        holder.postImage.setVisibility(View.GONE);

                        Glide.with(context)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                .apply(requestOptions)
                                .into(holder.mapPostImage);
                    } else {

                        holder.mapPostImage.setVisibility(View.GONE);
                        holder.postImage.setVisibility(View.VISIBLE);

                        Glide.with(holder.postImage)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                .placeholder(R.drawable.placeholder)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(holder.postImage);
                    }

                    holder.moreImagesLayout.setVisibility(View.GONE);
                }

                new Zoomy.Builder(context)
                        .target(holder.postImage)
                        .enableImmersiveMode(false)
                        .tapListener(v -> context.startActivity(new Intent(context, ViewPostImageActivity.class)
                                .putExtra("position", 0)
                                .putExtra("images", post.getPostImage())))
                        .doubleTapListener(v -> callMethod(holder))
                        .register();

                break;
            case "video":
                holder.layoutOnlyImages.setVisibility(View.GONE);
                holder.videoLayout.setVisibility(View.VISIBLE);
                holder.layoutPostWithBackgroundImage.setVisibility(View.GONE);
                holder.bind(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo()),post.getPostId());
                break;
            default:
                break;
        }
    }

    private void callMethod(RecyclerObjectHolder holder) {
        if(holder.like.isChecked()){
            holder.like.setChecked(false);
        }else {
            holder.like.setChecked(true);
        }
    }

    private void setClickListener(RecyclerObjectHolder holder) {

        final GestureDetector gd = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {

                holder.itemClicked("single click");
                /*if(helper!=null){
                    if(helper.getPlayWhenReady()){
                        pausePlayerToPlayAgain();
                    }else {
                        startPlayer();
                    }
                }*/
                return true;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {

                Logger.d("clicked99","double...11");

                if(holder.like.isChecked()){
                    holder.like.setChecked(false);
                }else {
                    holder.like.setChecked(true);
                }
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return true;
            }
        });



        holder.layoutPostWithBackgroundImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gd.onTouchEvent(event);
            }
        });

        holder.mapPostImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gd.onTouchEvent(event);
            }
        });

        holder.videoLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gd.onTouchEvent(event);
            }
        });


        holder.icTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()) && posts.get(holder.getAdapterPosition()).getSharedPost()!=null) {
                    if(posts.get(holder.getAdapterPosition()).getSharedPost().getTagged_id()!=null && !posts.get(holder.getAdapterPosition()).getSharedPost().getTagged_id().isEmpty()){
                        listener.onTagClicked(posts.get(holder.getAdapterPosition()).getSharedPost().getTagged_id());
                    }
                }else {
                    if(posts.get(holder.getAdapterPosition()).getTagged_id()!=null && !posts.get(holder.getAdapterPosition()).getTagged_id().isEmpty()){
                        listener.onTagClicked(posts.get(holder.getAdapterPosition()).getTagged_id());
                    }
                }*/
            }
        });

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()) && posts.get(holder.getAdapterPosition()).getSharedPost() != null) {
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getSharedPost().getUserId()))
                        context.startActivity(new Intent(context, MyProfileActivity.class));
                    else
                        context.startActivity(new Intent(context, ProfileActivity.class)
                                .putExtra("userId", posts.get(holder.getAdapterPosition()).getSharedPost().getUserId()));
                } else {
                    if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getUserId()))
                        context.startActivity(new Intent(context, MyProfileActivity.class));
                    else
                        context.startActivity(new Intent(context, ProfileActivity.class)
                                .putExtra("userId", posts.get(holder.getAdapterPosition()).getUserId()));
                }
            }
        });

        holder.sharedName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(posts.get(holder.getAdapterPosition()).getUserId()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class)
                            .putExtra("userId", posts.get(holder.getAdapterPosition()).getUserId()));
            }
        });

        holder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCommentClicked(posts.get(holder.getAdapterPosition()), holder.getAdapterPosition(),holder);
            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivityForResult(new Intent(context, SharePostActivity.class)
                        .putExtra("post", posts.get(holder.getAdapterPosition())), 100);
            }
        });

        holder.socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*String url = UrlEndpoints.BASE_SHARE_POST_URL + posts.get(holder.getAdapterPosition()).getPostId();
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
                i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                i.putExtra(Intent.EXTRA_TEXT, url);
                context.startActivity(Intent.createChooser(i, "Share URL"));*/

                String url = UrlEndpoints.BASE_SHARE_POST_URL + posts.get(holder.getAdapterPosition()).getPostId();

                if(listener!=null){
                    if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType())) {
                        listener.onDownloadClick(posts.get(holder.getAdapterPosition()).getSharedPost().getPostVideo(),url);
                    }else {
                        listener.onDownloadClick(posts.get(holder.getAdapterPosition()).getPostVideo(),url);
                    }
                }

            }
        });

        holder.report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ReportPostActivity.class).putExtra("postId", posts.get(holder.getAdapterPosition()).getPostId()));
            }
        });

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(holder.menu, holder.getAdapterPosition());
            }
        });

        holder.like.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    if (isChecked) {
                        holder.likesCount.setTextColor(Color.parseColor("#FF7400"));

                        int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                        posts.get(holder.getAdapterPosition()).setLikeCount(counter+1);

                        String likesCount = (Integer.valueOf(holder.likesCount.getText().toString()) + 1) + "";
                        holder.likesCount.setText(likesCount);
                    } else {
                        if (Integer.parseInt(holder.likesCount.getText().toString().trim()) > 0) {
                            holder.likesCount.setTextColor(Color.parseColor("#004F95"));

                            int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                            posts.get(holder.getAdapterPosition()).setLikeCount(counter - 1);

                            String likesCount = (Integer.parseInt(holder.likesCount.getText().toString().trim()) - 1) + "";
                            holder.likesCount.setText(likesCount);
                        }
                    }
                }else {
                    if(isChecked){

                        Logger.d("isChecked","clicked.....111");

                        holder.likesCount.setTextColor(Color.parseColor("#FF7400"));

                        int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                        posts.get(holder.getAdapterPosition()).setLikeCount(counter+1);

                        String likesCount = (Integer.valueOf(holder.likesCount.getText().toString()) + 1) + "";
                        holder.likesCount.setText(likesCount);
                    }else {

                        Logger.d("isChecked","clicked.....222");

                        if (Integer.parseInt(holder.likesCount.getText().toString().trim()) > 0) {
                            holder.likesCount.setTextColor(Color.parseColor("#004F95"));

                            int counter = posts.get(holder.getAdapterPosition()).getLikeCount();
                            posts.get(holder.getAdapterPosition()).setLikeCount(counter - 1);

                            String likesCount = (Integer.parseInt(holder.likesCount.getText().toString().trim()) - 1) + "";
                            holder.likesCount.setText(likesCount);
                        }
                    }
                }

                if (buttonView.isPressed()) {
                    posts.get(holder.getAdapterPosition()).setLiked(isChecked);
                    listener.onPostChecked(posts.get(holder.getAdapterPosition()));
                }else {
                    posts.get(holder.getAdapterPosition()).setLiked(isChecked);
                    listener.onPostChecked(posts.get(holder.getAdapterPosition()));
                }

            }
        });

        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFollow) {
                    if(posts.get(holder.getAdapterPosition()).getPrivateAccount()){
                        holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.follow.setText(context.getResources().getString(R.string.requested));
                        holder.follow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }else {
                        holder.follow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.follow.setText(context.getResources().getString(R.string.following));
                        holder.follow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }

                }
            }
        });

        /*holder.sharedFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && buttonView.isPressed()) {
                    holder.sharedFollow.setEnabled(false);
                    listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                }
            }
        });*/

        holder.sharedFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //New Code for shared follow click
                boolean isFollow = false;
                if(posts.get(holder.getAdapterPosition()) != null && posts.get(holder.getAdapterPosition()).getFollowing() != null){
                    if(posts.get(holder.getAdapterPosition()).getFollowing()) {
                        isFollow = false;
                    }else {
                        isFollow = true;
                        if(checkIsRequested(posts.get(holder.getAdapterPosition()))){
                            isFollow = false;
                        }
                    }
                }else {
                    isFollow = true;
                }

                if (isFollow) {
                    if(posts.get(holder.getAdapterPosition()).getPrivateAccount()){
                        holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.sharedFollow.setText(context.getResources().getString(R.string.requested));
                        holder.sharedFollow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }else {
                        holder.sharedFollow.setTextColor(context.getResources().getColor(R.color.orange));
                        holder.sharedFollow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        holder.sharedFollow.setText(context.getResources().getString(R.string.following));
                        holder.sharedFollow.setEnabled(false);
                        listener.onFollowClicked(posts.get(holder.getAdapterPosition()));
                    }
                }
            }
        });

        holder.likesCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!"0".equals(holder.likesCount.getText().toString())) {
                    context.startActivity(new Intent(context, PostLikedByActivity.class)
                            .putExtra("postId", posts.get(holder.getAdapterPosition()).getPostId()));
                }
            }
        });

        holder.moreImagesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> images;

                if ("shared".equals(posts.get(holder.getAdapterPosition()).getPostType()))
                    images = posts.get(holder.getAdapterPosition()).getSharedPost().getPostImage();
                else
                    images = posts.get(holder.getAdapterPosition()).getPostImage();

                context.startActivity(new Intent(context, ViewPostImageActivity.class)
                        .putExtra("position", 1)
                        .putExtra("images", images));
            }
        });
    }

    private void showMenu(View menu, int position) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int popupWidth = 450;
        int popupHeight = LinearLayout.LayoutParams.WRAP_CONTENT;

        LinearLayout viewGroup = context.findViewById(R.id.menu_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.drop_down_menu, viewGroup);
        popupMenu(layout, position);

        popup = new PopupWindow(layout, popupWidth, popupHeight);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        if (position == (posts.size() - 1))
            popup.showAsDropDown(menu, 0, -300, Gravity.TOP);
        else
            popup.showAsDropDown(menu);
    }

    private void popupMenu(View layout, int position) {

        RecyclerView menu = layout.findViewById(R.id.menuList);
        menu.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        menu.setLayoutManager(mLayoutManager);
        MenuAdapter adapter;
        if (posts.get(position).getMapPost())
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_map_menu))), position, this);
        else
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.post_menu))), position, this);
        menu.setAdapter(adapter);
    }

    @Override
    public void onMenuItemClicked(String item, int postPosition) {

        if (popup != null && popup.isShowing())
            popup.dismiss();

        switch (item.toLowerCase()) {

            case "edit":
                listener.onEditPost(posts.get(postPosition), postPosition);
                break;
            case "delete":
                listener.onDeletePost(posts.get(postPosition));
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {

        if (posts == null)
            return 0;
        else
            return posts.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (posts.get(position) == null){
            return POSTING;
        } else if (posts.size() > 2) {
            if (posts.get(2) == null) {
                if (position == 4)
                    return TRENDING_HASHTAG;
                else {
                    switch (position) {

                        case 0:
                            return HASHTAG_DETAILS;
                        case 1:
                            return UPLOAD_POST;
                        default:
                            return POST;
                    }
                }
            } else {
                switch (position) {

                    case 0:
                        return HASHTAG_DETAILS;
                    case 1:
                        return UPLOAD_POST;
                    case 3:
                        return TRENDING_HASHTAG;
                    default:
                        return POST;
                }
            }
        } else {
            switch (position) {
                case 0:
                    return HASHTAG_DETAILS;
                case 1:
                    return UPLOAD_POST;
                case 3:
                    return TRENDING_HASHTAG;
                default:
                    return POST;
            }
        }


    }

    @Override
    public void onHashTagClicked() {
        Logger.d("hashClicked","111");
    }

    public static class RecyclerObjectHolder extends RecyclerView.ViewHolder {

        ImageView postImage, moreImages, share, menu, thumbnail, backgroundImage,
                socialShare, report, hashtagCover, mapPostImage, back;
        CircleImageView myProfilePicture, profilePicture, sharedProfileImage, icTag;
        RelativeLayout /*layoutOnlyImages,*/ videoLayout, moreImagesLayout;
        FrameLayout layoutOnlyImages, thumbnailLayout;
        LinearLayout sharedLayout;
        RelativeLayout layoutPostWithBackgroundImage;
        TextView follow,videoCount,sharedFollow;
        ToggleButton like, followHashtag;
        TextView name, time, postText, imageCount, likesCount, comment, location, sharedName, sharedLocation, sharedTime,
                sharedPostText, write, listTitle, postTextOnImage, hashtag, postCount, followers;
        RecyclerView trendingHashtagList;
        Uri mediaUri;
        String singleClick;
        RoundedHorizontalProgressBar uploadProgress;
        int videoPostId;

        RecyclerObjectHolder(View itemView) {
            super(itemView);
            moreImages = itemView.findViewById(R.id.more_images);
            myProfilePicture = itemView.findViewById(R.id.profile_image);
            profilePicture = itemView.findViewById(R.id.profile_picture);
            sharedProfileImage = itemView.findViewById(R.id.shared_profile_image);
            share = itemView.findViewById(R.id.share);
            menu = itemView.findViewById(R.id.menu);
            layoutOnlyImages = itemView.findViewById(R.id.layout_only_images);
            moreImagesLayout = itemView.findViewById(R.id.more_images_layout);
            sharedLayout = itemView.findViewById(R.id.shared_layout);
            videoLayout = itemView.findViewById(R.id.video_layout);
            name = itemView.findViewById(R.id.name);
            time = itemView.findViewById(R.id.time);
            postText = itemView.findViewById(R.id.post_text);
            videoCount = itemView.findViewById(R.id.videoCount);
            sharedName = itemView.findViewById(R.id.shared_name);
            sharedLocation = itemView.findViewById(R.id.shared_location);
            sharedTime = itemView.findViewById(R.id.shared_time);
            sharedPostText = itemView.findViewById(R.id.shared_post_text);
            postImage = itemView.findViewById(R.id.post_image);
            imageCount = itemView.findViewById(R.id.image_count);
            likesCount = itemView.findViewById(R.id.likes_count);
            comment = itemView.findViewById(R.id.comment);
            location = itemView.findViewById(R.id.location);
            like = itemView.findViewById(R.id.like);
            follow = itemView.findViewById(R.id.follow);
            sharedFollow = itemView.findViewById(R.id.follow_shared);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            write = itemView.findViewById(R.id.write);
            listTitle = itemView.findViewById(R.id.list_title);
            trendingHashtagList = itemView.findViewById(R.id.horizontal_list);
            backgroundImage = itemView.findViewById(R.id.background_image);
            postTextOnImage = itemView.findViewById(R.id.post_text_on_image);
            layoutPostWithBackgroundImage = itemView.findViewById(R.id.text_with_image_background);
            socialShare = itemView.findViewById(R.id.social_share);
            report = itemView.findViewById(R.id.report);
            hashtagCover = itemView.findViewById(R.id.hashtag_cover);
            followHashtag = itemView.findViewById(R.id.follow_hashtag);
            hashtag = itemView.findViewById(R.id.hashtag);
            postCount = itemView.findViewById(R.id.post_count);
            followers = itemView.findViewById(R.id.followers_count);
            thumbnailLayout = itemView.findViewById(R.id.thumbnail_layout);
            mapPostImage = itemView.findViewById(R.id.post_map_image);
            uploadProgress = itemView.findViewById(R.id.upload_progress);
            back = itemView.findViewById(R.id.back);
            icTag = itemView.findViewById(R.id.icTag);
        }

        void bind(Uri media, int postId) {
            this.mediaUri = media;
            this.videoPostId = postId;
        }

        void itemClicked(String singleClick){
            this.singleClick = singleClick;
        }

    }

    class DataObjectHolder extends RecyclerObjectHolder implements ToroPlayer {

        PlayerView playerView;
        ExoPlayerViewHelper helper;
        Uri mediaUri;
        ToggleButton volume;
        String singleClick;



        private Handler handler;
        private Runnable runnable;
        int videoPostId;

        DataObjectHolder(View itemView) {
            super(itemView);
            playerView = itemView.findViewById(R.id.player);
            volume = itemView.findViewById(R.id.volume);
        }

        @NonNull
        @Override
        public View getPlayerView() {
            return playerView;
        }

        @NonNull
        @Override
        public PlaybackInfo getCurrentPlaybackInfo() {
            return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
        }

        @Override
        public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {
            thumbnailLayout.setVisibility(View.VISIBLE);
            if (helper == null) {
                helper = new ExoPlayerViewHelper(this, mediaUri);
            }
            helper.initialize(container, playbackInfo);
        }

        @Override
        public void play() {
            if (helper != null) {
                helper.play();
                playerView.getPlayer().setRepeatMode(Player.REPEAT_MODE_ALL);
                playerView.setKeepContentOnPlayerReset(true);
                playerView.setControllerAutoShow(false);
                playerView.setUseController(false);

                //Turn On/Off auto play videos
                if (Identity.getAutoPlayVideo(context)){
                    listener.onVideoCount(videoPostId);
                }else {
                    playerView.getPlayer().setPlayWhenReady(false);

                    handler = new Handler();
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            if(playerView!= null && playerView.getPlayer()!=null){
                                if(helper.isPlaying()){
                                    Logger.d("VideoPlay","playing...");
                                    listener.onVideoCount(videoPostId);
                                    handler.removeCallbacks(runnable);
                                }else {
                                    Logger.d("VideoPlay","Not playing...");
                                    handler.postDelayed(this, 1000);
                                }
                            }
                        }
                    };
                    handler.postDelayed(runnable, 0);
                }


                if (isMute) {
                    mute();
                    volume.setChecked(false);
                } else {
                    unMute();
                    volume.setChecked(true);
                }

                //mute();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        thumbnailLayout.setVisibility(View.GONE);
                    }
                }, 1500);
            }

        }

        @Override
        public void pause() {

            if(handler!=null){
                handler.removeCallbacks(runnable);
            }

            if (helper != null) helper.pause();
        }

        @Override
        public boolean isPlaying() {
            return helper != null && helper.isPlaying();
        }

        @Override
        public void release() {

            if(handler!=null){
                handler.removeCallbacks(runnable);
            }

            if (helper != null) {
                thumbnailLayout.setVisibility(View.VISIBLE);
                helper.release();
                helper = null;
            }
        }

        @Override
        public boolean wantsToPlay() {
            return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85;
        }

        @Override
        public int getPlayerOrder() {
            return getAdapterPosition();
        }

        @SuppressLint("ClickableViewAccessibility")
        void bind(Uri media, int postId) {
            this.mediaUri = media;
            this.videoPostId = postId;
            this.volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        unMute();
                    else
                        mute();
                }
            });
        }

        @SuppressLint("ClickableViewAccessibility")
        void itemClicked(String singleClick){
            this.singleClick = singleClick;
            Logger.d("clicked99","single...11");
            if(this.playerView!=null){
                if(this.playerView.getPlayer().getPlayWhenReady()){
                    this.playerView.getPlayer().setPlayWhenReady(false);
                    this.playerView.getPlayer().getPlaybackState();
                }else {
                    this.playerView.getPlayer().setPlayWhenReady(true);
                    this.playerView.getPlayer().getPlaybackState();
                }
            }

        }

        private void mute() {
            isMute = true;
            this.setVolume(0);
        }

        private void unMute() {
            isMute = false;
            this.setVolume(100);
        }

        private void setVolume(int amount) {
            final int max = 100;
            final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
            final float volume = (float) (1 - (numerator / Math.log(max)));

            if(this.helper !=null){
                this.helper.setVolume(volume);
            }
        }
    }

    public void onCommentDialogDismiss(int scrollPosition, int commentCount, RecyclerObjectHolder holder){
        if(holder!=null){
            holder.comment.setText("" + commentCount);
        }
    }

    public interface OnPostClickListener {
        void onPostChecked(Post post);

        void onEditPost(Post post, int position);

        void onDeletePost(Post post);

        void onHashtagFollow(boolean isFollowed);

        void onCommentClicked(Post post, int position, RecyclerObjectHolder holder);

        void onCreatePostClicked();

        void onFollowClicked(Post post);

        void onVideoCount(int videoPostId);

        void onPostViewCount(int postId);

        void onTagClicked(ArrayList<String> taggedIds);

        void onDownloadClick(String videoUrl, String postLink);
    }
}
