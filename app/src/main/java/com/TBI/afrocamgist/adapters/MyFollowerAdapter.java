package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.follow.Follow;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyFollowerAdapter extends RecyclerView.Adapter<MyFollowerAdapter.DataObjectHolder> {

    private Activity context;
    private ArrayList<Follow> followers;

    public MyFollowerAdapter(Activity context, ArrayList<Follow> followers) {
        this.context = context;
        this.followers = followers;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_follower, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        Follow follower = followers.get(position);

        String fullName = follower.getFirstName() + " " + follower.getLastName();
        holder.name.setText(fullName);

        String followers;
        if (follower.getFollowerIds()!=null)
            followers = follower.getFollowerIds().size() + " followers";
        else
            followers = "0 followers";

        holder.followers.setText(followers);

        Glide.with(context)
                .load(UrlEndpoints.MEDIA_BASE_URL + follower.getProfileImageUrl())
                .into(holder.profileImage);

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(follower.getUserId()))
                    context.startActivity(new Intent(context, MyProfileActivity.class));
                else
                    context.startActivity(new Intent(context, ProfileActivity.class).putExtra("userId",follower.getUserId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (followers == null)
            return 0;
        else
            return followers.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name, followers;
        CircleImageView profileImage;
        ToggleButton follow;

        DataObjectHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            followers = itemView.findViewById(R.id.followers);
            profileImage = itemView.findViewById(R.id.profile_image);
            follow = itemView.findViewById(R.id.follow);
        }
    }

}
