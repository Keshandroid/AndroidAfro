package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.notification.NotificationData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationAdapter extends BaseAdapter {

    private ArrayList<NotificationData> notificationList;
    private LayoutInflater layoutInflater;

    public NotificationAdapter(Activity context, ArrayList<NotificationData> notificationList) {
        layoutInflater = LayoutInflater.from(context);
        this.notificationList = notificationList;
    }

    @Override
    public int getCount() {
        if (notificationList==null)
            return 0;
        else
            return notificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_notification, parent, false);
            holder = new ViewHolder();

            holder.profilePicture = convertView.findViewById(R.id.profile_picture);
            holder.notification = convertView.findViewById(R.id.notification);
            holder.time = convertView.findViewById(R.id.time);
            holder.read = convertView.findViewById(R.id.read);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (notificationList.get(position).getNotificationDetails().getUserDetails()!=null) {
            Glide.with(holder.profilePicture)
                    .load(UrlEndpoints.MEDIA_BASE_URL + notificationList.get(position).getNotificationDetails().getUserDetails().getProfileImageUrl())
                    .into(holder.profilePicture);
        }

        if (notificationList.get(position).getNotificationDetails().getText()!=null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.notification.setText(Html.fromHtml(notificationList.get(position).getNotificationDetails().getText(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.notification.setText(Html.fromHtml(notificationList.get(position).getNotificationDetails().getText()));
            }
        }

        holder.time.setText(Utils.getSocialStyleTime(notificationList.get(position).getCreatedDate()));

        if ("read".equals(notificationList.get(position).getNotificationStatus().toLowerCase()))
            holder.read.setVisibility(View.GONE);
        else
            holder.read.setVisibility(View.VISIBLE);

        return convertView;
    }

    class ViewHolder {
        CircleImageView profilePicture;
        TextView notification, time;
        View read;
    }
}
