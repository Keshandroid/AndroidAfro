package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.model.hashtag.Hashtag;

import java.util.ArrayList;

public class FollowingHashtagAdapter extends RecyclerView.Adapter<FollowingHashtagAdapter.DataObjectHolder>{

    private ArrayList<Hashtag> hashtagList;
    private OnHashtagClickListener listener;

    public FollowingHashtagAdapter(ArrayList<Hashtag> hashtagList, OnHashtagClickListener listener) {
        this.hashtagList = hashtagList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_following_hashtag,viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {

        Hashtag hashtag = hashtagList.get(holder.getAdapterPosition());

        if (hashtag!=null) {

            String hashtagSlug = "#" + hashtag.getHashtagSlug();
            holder.hashtag.setText(hashtagSlug);

            String followersCount = hashtag.getFollowers().size() + " followers";
            holder.followers.setText(followersCount);

            holder.follow.setChecked(true);
        }

        holder.follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listener.onFollowOrUnfollowToggleClick(isChecked, hashtag);
            }
        });

        holder.hashtag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onHashtagClick(hashtag);
            }
        });

    }

    @Override
    public int getItemCount() {

        if(hashtagList==null)
            return 0;
        else
            return hashtagList.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView hashtag, followers;
        ToggleButton follow;

        DataObjectHolder(View itemView) {
            super(itemView);
            hashtag = itemView.findViewById(R.id.hashtag);
            followers = itemView.findViewById(R.id.followers);
            follow = itemView.findViewById(R.id.follow);
        }
    }

    public interface OnHashtagClickListener {
        void onFollowOrUnfollowToggleClick(boolean isFollowed, Hashtag hashtag);
        void onHashtagClick(Hashtag hashtag);
    }
}
