package com.TBI.afrocamgist.adapters;

import android.content.Context;
import android.graphics.ColorMatrixColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.OpenCamera;
import com.TBI.afrocamgist.model.filter.Filter;

import java.util.List;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.ViewHolder> {
    private int mCurrentPosition = 0;

    private Context mContext;
    private List<Filter> filterList;
    private OpenCamera camera;

    public FiltersAdapter(Context openCamera1, List<Filter> abc, OpenCamera camera) {
        mContext = openCamera1;
        filterList = abc;
        this.camera = camera;
    }


    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();

        View view = LayoutInflater.from(mContext).inflate(R.layout.item_filter, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Filter filter = filterList.get(position);

        holder.filterTitle.setText(filter.getTitle());
        if (position == 0) {
            holder.filterImageView.setColorFilter(null);
        } else {
            holder.filterImageView.setColorFilter(new ColorMatrixColorFilter(filter.getColorMatrix()));
        }


        if (mCurrentPosition == position) {
            holder.filterChecked.setVisibility(View.VISIBLE);
        } else
            holder.filterChecked.setVisibility(View.GONE);


        holder.filterImageView.setTag(position);
        holder.filterImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos = (int) view.getTag();
                mCurrentPosition = pos;
                notifyDataSetChanged();
                if (camera != null) {
                    camera.changeCurrentFilter(pos);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (filterList==null)
            return 0;
        return filterList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView filterTitle;
        ImageView filterImageView;
        ImageView filterChecked;

        public ViewHolder(View itemView) {
            super(itemView);
            filterTitle = itemView.findViewById(R.id.text_view_filter_title);
            filterImageView = itemView.findViewById(R.id.image_view_filter);
            filterChecked = itemView.findViewById(R.id.image_view_filter_checked);
        }
    }
}