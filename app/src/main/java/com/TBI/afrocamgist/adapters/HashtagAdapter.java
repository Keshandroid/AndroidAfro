package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class HashtagAdapter extends RecyclerView.Adapter<HashtagAdapter.DataObjectHolder> {

    private static final int HASHTAG = 0, LOADING = 1;
    private ArrayList<Hashtag> hashtagList;
    private OnHashtagClickListener listener;

    public HashtagAdapter(ArrayList<Hashtag> hashtagList, OnHashtagClickListener listener) {
        this.hashtagList = hashtagList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view;

        switch (viewType) {

            case HASHTAG:
                view = inflater.inflate(R.layout.item_hashtag,viewGroup, false);
                break;
            case LOADING:
                view = inflater.inflate(R.layout.item_loading,viewGroup, false);
                break;
            default:
                view = inflater.inflate(R.layout.item_hashtag,viewGroup, false);
                break;
        }

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        switch (getItemViewType(position)) {

            case HASHTAG:
                setHashtagView(holder);
                break;
            case LOADING:
                showLoadingView(holder);
                break;
            default:
                setHashtagView(holder);
                break;
        }
    }

    private void showLoadingView(DataObjectHolder holder) {}

    private void setHashtagView(DataObjectHolder holder) {

        Hashtag hashtag = hashtagList.get(holder.getAdapterPosition());

        if (hashtag!=null) {

            String hashtagSlug = "#" + hashtag.getHashtagSlug();
            holder.hashtag.setText(hashtagSlug);

            String followersCount = hashtag.getFollowers().size() + " followers";
            holder.followers.setText(followersCount);

            if (hashtag.getFollowedByMe())
                holder.follow.setChecked(true);
            else
                holder.follow.setChecked(false);

            Glide.with(holder.coverImage)
                    .load(UrlEndpoints.MEDIA_BASE_URL + hashtag.getCoverImage())
                    .into(holder.coverImage);
        }

        holder.follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isPressed()){
                    listener.onFollowOrUnfollowToggleClick(isChecked, hashtag);
                }

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onHashtagClick(hashtag);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (hashtagList==null)
            return 0;
        else
            return hashtagList.size();
    }

    @Override
    public int getItemViewType(int position) {

        if(hashtagList.get(position)==null)
            return LOADING;
        else
            return HASHTAG;
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        ImageView coverImage;
        TextView hashtag, followers;
        ToggleButton follow;

        DataObjectHolder(View itemView) {
            super(itemView);
            coverImage = itemView.findViewById(R.id.cover_image);
            hashtag = itemView.findViewById(R.id.hashtag);
            followers = itemView.findViewById(R.id.followers);
            follow = itemView.findViewById(R.id.follow);
        }
    }

    public interface OnHashtagClickListener {
        void onFollowOrUnfollowToggleClick(boolean isFollowed, Hashtag hashtag);
        void onHashtagClick(Hashtag hashtag);
    }


}
