package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.photos.Photo;
import com.TBI.afrocamgist.model.post.Post;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class PhotoGalleryAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private ArrayList<Photo> photos;

    public PhotoGalleryAdapter(Activity context, ArrayList<Photo> photos) {
        this.photos = photos;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(photos==null)
            return 0;
        else
            return photos.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        View view = inflater.inflate(R.layout.item_photo_gallery, container, false);

        ImageView photo = view.findViewById(R.id.photo);
        ImageView remove = view.findViewById(R.id.remove);

        Glide.with(photo)
                .load(UrlEndpoints.MEDIA_BASE_URL + photos.get(position).getImageUrl())
                .into(photo);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
