package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.chat.CallLog;
import com.TBI.afrocamgist.model.notification.NotificationData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CallLogAdapter extends BaseAdapter {

    private ArrayList<CallLog> callLogsList;
    private LayoutInflater layoutInflater;

    private OnCallClickListener listener;

    public CallLogAdapter(OnCallClickListener listener, Activity context, ArrayList<CallLog> callLogsList) {
        this.listener = listener;
        layoutInflater = LayoutInflater.from(context);
        this.callLogsList = callLogsList;
    }

    @Override
    public int getCount() {
        if (callLogsList==null)
            return 0;
        else
            return callLogsList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_call_log, parent, false);
            holder = new ViewHolder();

            holder.profilePicture = convertView.findViewById(R.id.profile_picture);
            holder.username = convertView.findViewById(R.id.username);
            holder.time = convertView.findViewById(R.id.time);
            holder.callImg = convertView.findViewById(R.id.callImg);
            holder.callDuration = convertView.findViewById(R.id.callDuration);
            holder.rlCall = convertView.findViewById(R.id.rlCall);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        CallLog callLog = callLogsList.get(position);

        if(callLog.getCall_type()!=null){
            if(callLog.getCall_status().equalsIgnoreCase("missed")){
                holder.callImg.setBackgroundResource(R.drawable.ic_missed_call);
            }else if(callLog.getCall_type().equalsIgnoreCase("incomming")){
                holder.callImg.setBackgroundResource(R.drawable.ic_incoming_call);
            }else if(callLog.getCall_type().equalsIgnoreCase("outgoing")){
                holder.callImg.setBackgroundResource(R.drawable.ic_outgoing_call);
            }

            if(callLog.getCall_duration()!=null){
                holder.callDuration.setText(callLog.getCall_type() + " call, " + callLog.getCall_duration());
            }
        }

        if(callLog.getCall_id()!=null){
            holder.rlCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null){
                        if(callLog.getCallerID()!=null) {
                            if (callLog.getCallerID().equals(LocalStorage.getUserDetails().getUserId())) {
                                if(callLog.getReceiverUser()!=null){
                                    String oppositePersonName = callLog.getReceiverUser().getFirst_name() + " " + callLog.getReceiverUser().getLast_name();
                                    String oppositePersonProfilePic = UrlEndpoints.MEDIA_BASE_URL + callLog.getReceiverUser().getProfile_image_url();

                                    listener.onCallClicked(callLog.getCall_id(),
                                            callLog.getReceiver(),
                                            oppositePersonName,
                                            oppositePersonProfilePic);
                                }
                            }else {
                                if(callLog.getCallUser()!=null){
                                    String oppositePersonName = callLog.getCallUser().getFirst_name() + " " + callLog.getCallUser().getLast_name();
                                    String oppositePersonProfilePic = UrlEndpoints.MEDIA_BASE_URL + callLog.getCallUser().getProfile_image_url();

                                    listener.onCallClicked(callLog.getCall_id(),
                                            callLog.getCallerID(),
                                            oppositePersonName,
                                            oppositePersonProfilePic);
                                }
                            }
                        }
                    }
                }
            });
        }

        if(callLog.getCallerID()!=null){
            if(callLog.getCallerID().equals(LocalStorage.getUserDetails().getUserId())){
                if (callLog.getReceiverUser().getProfile_image_url()!=null) {
                    Glide.with(holder.profilePicture)
                            .load(UrlEndpoints.MEDIA_BASE_URL + callLog.getReceiverUser().getProfile_image_url())
                            .into(holder.profilePicture);
                }

                if (callLog.getReceiverUser()!=null) {
                    holder.username.setText(callLog.getReceiverUser().getFirst_name() + " "+callLog.getReceiverUser().getLast_name());
                }

                holder.time.setText(Utils.getSocialStyleTime(callLog.getCall_time()));


            }else {
                if (callLog.getCallUser().getProfile_image_url()!=null) {
                    Glide.with(holder.profilePicture)
                            .load(UrlEndpoints.MEDIA_BASE_URL + callLog.getCallUser().getProfile_image_url())
                            .into(holder.profilePicture);
                }

                if (callLog.getCallUser()!=null) {
                    holder.username.setText(callLog.getCallUser().getFirst_name() + " "+callLog.getCallUser().getLast_name());
                }

                holder.time.setText(Utils.getSocialStyleTime(callLog.getCall_time()));

            }

        }

        return convertView;
    }

    class ViewHolder {
        CircleImageView profilePicture;
        TextView username, time, callDuration;
        View callImg;
        RelativeLayout rlCall;
    }

    public interface OnCallClickListener{
        void onCallClicked(Integer callId, Integer receiverId, String personName, String personProfilePic);
    }

}
