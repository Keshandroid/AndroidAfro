package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.InsLoadingView;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.MainStoryPostActivity;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.story.StoryUserData;
import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import static com.TBI.afrocamgist.InsLoadingView.Status.CLICKED;
import static com.TBI.afrocamgist.InsLoadingView.Status.LOADING;
import static com.TBI.afrocamgist.InsLoadingView.Status.UNCLICKED;

public class MainStoryAdapter extends RecyclerView.Adapter<MainStoryAdapter.DataObjectHolder> {

    //story
    private ArrayList<ArrayList<StoryUserData>> posts;

    //popular posts
    //private ArrayList<Post> posts;

    private Activity context;

    public MainStoryAdapter(Activity context, ArrayList<ArrayList<StoryUserData>> posts) {
        this.context = context;
        this.posts = posts;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_main_story, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        //story

        ArrayList<StoryUserData> people = posts.get(position);
        Glide.with(holder.profilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + people.get(0).getProfileImageUrl())
                .into(holder.profilePicture);

        //popular posts

        //Post people = posts.get(position);
        /*Glide.with(holder.profilePicture)
                .load(UrlEndpoints.MEDIA_BASE_URL + people.getProfileImageUrl())
                .into(holder.profilePicture);*/

        Logger.d("peopleData", new GsonBuilder().setPrettyPrinting().create().toJson(people));

        /*holder.profilePicture.setCircleDuration(2000);
        holder.profilePicture.setRotateDuration(10000);
        holder.profilePicture.setStartColor(Color.YELLOW);
        holder.profilePicture.setEndColor(Color.BLUE);*/

        holder.profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (holder.profilePicture.getStatus()) {
                    case UNCLICKED:
                        holder.profilePicture.setStatus(LOADING);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //for single post
                                //context.startActivity(new Intent(context, StorySinglePostActivity.class).putExtra("singlePost", people));

                                //for storyline
                                context.startActivity(new Intent(context, MainStoryPostActivity.class).putExtra("posts", people).putExtra("allPost",posts).putExtra("postPosition",position));

                                //for multiple popular posts
                                //context.startActivity(new Intent(context, StoryPopularPostActivity.class).putExtra("posts", posts).putExtra("postPosition",position));

                                holder.profilePicture.setStatus(CLICKED);
                            }
                        }, 1000);

                        break;
                    /*case LOADING:
                        holder.profilePicture.setStatus(CLICKED);
                        break;*/
                    case CLICKED:
                        //context.startActivity(new Intent(context, StorySinglePostActivity.class).putExtra("singlePost", people));
                        context.startActivity(new Intent(context, MainStoryPostActivity.class).putExtra("posts", people).putExtra("allPost",posts).putExtra("postPosition",position));
                        //context.startActivity(new Intent(context, StoryPopularPostActivity.class).putExtra("posts", posts).putExtra("postPosition",position));


                }
            }
        });

        holder.profilePicture.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });

    }


    @Override
    public int getItemCount() {

        if (posts==null)
            return 0;
        else
            return posts.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {

        private InsLoadingView profilePicture;

        DataObjectHolder(View itemView) {
            super(itemView);
            profilePicture = itemView.findViewById(R.id.profile_picture);
        }
    }
}
