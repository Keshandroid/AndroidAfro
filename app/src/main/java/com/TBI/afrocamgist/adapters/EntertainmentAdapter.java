package com.TBI.afrocamgist.adapters;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.Post;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class EntertainmentAdapter extends RecyclerView.Adapter<EntertainmentAdapter.DataObjectHolder> {

    private static final int ENTERTAINMENT = 0, LOADING = 1;
    private ArrayList<Post> entertainmentPosts;
    private OnEntertainmentPostClickListener listener;

    private boolean isLoaderVisible = false;

    public EntertainmentAdapter(ArrayList<Post> entertainmentPosts, OnEntertainmentPostClickListener listener) {
        this.entertainmentPosts = entertainmentPosts;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view;

        switch (viewType) {

            case LOADING:
                view = inflater.inflate(R.layout.item_loading, viewGroup, false);
                return new DataObjectHolder(view);
            default:
                view = inflater.inflate(R.layout.item_entertainment, viewGroup, false);
                return new DataObjectHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        switch (getItemViewType(position)) {

            case LOADING:
                showLoadingView(holder);
                break;
            default:
                setEntertainmentView(holder);
                break;
        }
    }

    private void showLoadingView(DataObjectHolder holder) {
    }

    private void setEntertainmentView(DataObjectHolder holder) {

//        Logger.e("LLLL_EnterSie: ", String.valueOf(entertainmentPosts.size()));
        if (entertainmentPosts.size() != 0) {

            Post entertainmentPost = entertainmentPosts.get(holder.getAdapterPosition());

            if ("image".equals(entertainmentPost.getPostType())) {
                if (entertainmentPost.getPostImage().size() != 0) {
                    Glide.with(holder.postImage)
                            .load(UrlEndpoints.MEDIA_BASE_URL + entertainmentPost.getPostImage().get(0))
                            .into(holder.postImage);
                }
                holder.vid_image.setVisibility(View.GONE);
            } else if ("video".equals(entertainmentPost.getPostType())) {

                Glide.with(holder.postImage)
                        .load(UrlEndpoints.MEDIA_BASE_URL + entertainmentPost.getThumbnail())
                        .into(holder.postImage);
                holder.vid_image.setVisibility(View.VISIBLE);
            }

            holder.likes.setText(String.valueOf(entertainmentPost.getLikeCount()));
            holder.comments.setText(String.valueOf(entertainmentPost.getCommentCount()));

            holder.share.setOnClickListener(v -> listener.onViewPostClicked(entertainmentPost));

            holder.postImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener != null) {
                        listener.onPostImageClicked(holder.getAdapterPosition());
                    }
                }
            });

            holder.postImage.setOnClickListener(v -> listener.onPostImageClicked(holder.getAdapterPosition()));
        }
    }

    @Override
    public int getItemCount() {
        if (entertainmentPosts == null)
            return 0;
        else
            return entertainmentPosts.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (entertainmentPosts.get(position) == null)
            return position == entertainmentPosts.size() - 1 ? LOADING : ENTERTAINMENT;
        else
            return ENTERTAINMENT;
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        ImageView postImage, share, vid_image;
        TextView likes, comments;

        DataObjectHolder(View itemView) {
            super(itemView);
            postImage = itemView.findViewById(R.id.post_image);
            share = itemView.findViewById(R.id.share);
            vid_image = itemView.findViewById(R.id.vid_image);
            likes = itemView.findViewById(R.id.likes);
            comments = itemView.findViewById(R.id.comments);

        }
    }

    public class ViewHolder extends BaseViewHolder {
        ImageView postImage, share, vid_image;
        TextView likes, comments;

        ViewHolder(View itemView) {
            super(itemView);
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            postImage = itemView.findViewById(R.id.post_image);
            share = itemView.findViewById(R.id.share);
            vid_image = itemView.findViewById(R.id.vid_image);
            likes = itemView.findViewById(R.id.likes);
            comments = itemView.findViewById(R.id.comments);


//            Logger.e("LLLL_EnterSie: ", String.valueOf(entertainmentPosts.size()));
            if (entertainmentPosts.size() != 0) {

                Post entertainmentPost = entertainmentPosts.get(getAdapterPosition());

                if ("image".equals(entertainmentPost.getPostType())) {
                    if (entertainmentPost.getPostImage().size() != 0) {
                        Glide.with(postImage)
                                .load(UrlEndpoints.MEDIA_BASE_URL + entertainmentPost.getPostImage().get(0))
                                .into(postImage);
                    }
                    vid_image.setVisibility(View.GONE);
                } else if ("video".equals(entertainmentPost.getPostType())) {
                    if (entertainmentPost.getPostImage().size() != 0) {
                        Glide.with(postImage)
//                            .load(UrlEndpoints.MEDIA_BASE_URL + entertainmentPost.getThumbnail())
                                .load(UrlEndpoints.MEDIA_BASE_URL + entertainmentPost.getPostImage().get(0))
                                .into(postImage);
                        vid_image.setVisibility(View.VISIBLE);
                    }
                }

                likes.setText(String.valueOf(entertainmentPost.getLikeCount()));
                comments.setText(String.valueOf(entertainmentPost.getCommentCount()));

                share.setOnClickListener(v -> listener.onViewPostClicked(entertainmentPost));

                postImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onPostImageClicked(getAdapterPosition());
                        }
                    }
                });

                //postImage.setOnClickListener(v -> listener.onPostImageClicked(getAdapterPosition()));
            }
        }
    }

    public static class ProgressHolder extends BaseViewHolder {
        ProgressHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void clear() {
        }
    }

    public void addItems(List<Post> postItems) {
        entertainmentPosts.addAll(postItems);
        notifyDataSetChanged();
    }

    public void addLoading() {
        isLoaderVisible = true;
        entertainmentPosts.add(new Post());
        notifyItemInserted(entertainmentPosts.size() - 2);
    }

    public void removeLoading() {
        isLoaderVisible = false;
        int position = entertainmentPosts.size() - 1;
        Post item = getItem(position);
        if (item != null) {
            entertainmentPosts.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Post getItem(int position) {
        return entertainmentPosts.get(position);
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public interface OnEntertainmentPostClickListener {
        void onViewPostClicked(Post post);

        void onPostImageClicked(int position);
    }
}

