package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.background.BackgroundImage;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class BackgroundImageAdapter extends RecyclerView.Adapter<BackgroundImageAdapter.DataObjectHolder> {

    private ArrayList<BackgroundImage> images;
    private OnBackgroundImageClickListener listener;

    public BackgroundImageAdapter(ArrayList<BackgroundImage> images, OnBackgroundImageClickListener listener) {
        this.images = images;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_background_image, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new RoundedCorners(15));

        Glide.with(holder.backgroundImage)
                .load(UrlEndpoints.MEDIA_BASE_URL + images.get(position).getPath())
                .apply(requestOptions)
                .into(holder.backgroundImage);

        holder.backgroundImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBackgroundImageClick(images.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (images ==null)
            return 0;
        else
            return images.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        ImageView backgroundImage;

        DataObjectHolder(View itemView) {
            super(itemView);
            backgroundImage = itemView.findViewById(R.id.background_image);
        }
    }

    public interface OnBackgroundImageClickListener {
        void onBackgroundImageClick(BackgroundImage backgroundImage);
    }
}
