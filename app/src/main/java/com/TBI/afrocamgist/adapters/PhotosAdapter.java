package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.photos.Photo;
import com.bumptech.glide.Glide;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.DataObjectHolder> {

    private ArrayList<Photo> photos;
    private OnPhotoClickListener listener;

    public PhotosAdapter(ArrayList<Photo> photos, OnPhotoClickListener listener){
        this.photos = photos;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_photo, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        Glide.with(holder.photo)
                .load(UrlEndpoints.MEDIA_BASE_URL + photos.get(position).getImageUrl())
                .into(holder.photo);

        ViewGroup.LayoutParams layoutParams = holder.photo.getLayoutParams();
        if (layoutParams instanceof FlexboxLayoutManager.LayoutParams) {
            ((FlexboxLayoutManager.LayoutParams) layoutParams).setFlexGrow(1f);
            ((FlexboxLayoutManager.LayoutParams) layoutParams).setAlignSelf(AlignItems.BASELINE);
        }

        holder.photo.setOnClickListener(v -> listener.onPhotoClicked(photos,holder.getAdapterPosition()));
        holder.remove.setOnClickListener(v -> listener.onDeletePost(photos.get(position)));
    }

    @Override
    public int getItemCount() {
        if (photos==null)
            return 0;
        else
            return photos.size();
    }


    static class DataObjectHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        ImageView remove;

        DataObjectHolder(View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.photo);
            remove = itemView.findViewById(R.id.remove);
        }
    }

    public interface OnPhotoClickListener {
        void onPhotoClicked(ArrayList<Photo> photos, int position);
        void onDeletePost(Photo photo);
    }
}
