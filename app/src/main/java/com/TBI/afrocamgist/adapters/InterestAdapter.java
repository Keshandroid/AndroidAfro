package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.R;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;

public class InterestAdapter extends RecyclerView.Adapter<InterestAdapter.DataObjectHolder> {

    private ArrayList<String> afroInterests;

    public InterestAdapter(ArrayList<String> afroInterests) {
        this.afroInterests = afroInterests;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_afro_interest, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        holder.interest.setTextOn(afroInterests.get(position));
        holder.interest.setTextOff(afroInterests.get(position));
        holder.interest.setChecked(true);
        holder.interest.setClickable(false);

        ViewGroup.LayoutParams layoutParams = holder.interest.getLayoutParams();
        if (layoutParams instanceof FlexboxLayoutManager.LayoutParams) {
            ((FlexboxLayoutManager.LayoutParams) layoutParams).setFlexGrow(1f);
            ((FlexboxLayoutManager.LayoutParams) layoutParams).setAlignSelf(AlignItems.BASELINE);
        }

    }

    @Override
    public int getItemCount() {
        if (afroInterests == null)
            return 0;
        else
            return afroInterests.size();
    }


    static class DataObjectHolder extends RecyclerView.ViewHolder {
        ToggleButton interest;

        DataObjectHolder(View itemView) {
            super(itemView);
            interest = itemView.findViewById(R.id.interest);
        }
    }
}
