package com.TBI.afrocamgist.adapters;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.groups.Group;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FindGroupAdapter extends RecyclerView.Adapter<FindGroupAdapter.DataObjectHolder> {

    private ArrayList<Group> groups;
    private OnGroupClickListener listener;

    public FindGroupAdapter(ArrayList<Group> groups, OnGroupClickListener listener) {
        this.groups = groups;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_find_group, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        Group group = groups.get(position);

        holder.groupName.setText(group.getGroupTitle().trim());

        Glide.with(holder.groupImage)
                .load(UrlEndpoints.MEDIA_BASE_URL + group.getGroupImage())
                .into(holder.groupImage);

        String totalGroupMember = group.getGroupMembers().size() + " members";
        holder.groupMembers.setText(totalGroupMember);

        if ("join group".equals(group.getRequestButtons().get(0).getButtonText().toLowerCase())) {
            holder.groupRequest.setChecked(false);
        } else if ("group admin".equals(group.getRequestButtons().get(0).getButtonText().toLowerCase())) {
            holder.groupRequest.setTextOn("Admin");
            holder.groupRequest.setChecked(true);
            holder.groupRequest.setClickable(false);
        } else {
            holder.groupRequest.setTextOn("Leave");
            holder.groupRequest.setChecked(true);
            holder.groupRequest.setClickable(true);
        }

        holder.groupRequest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed())
                    listener.onGroupJoinOrLeaveToggleClick(group, isChecked);
            }
        });

        holder.group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onGroupClick(group);
            }
        });

    }

    @Override
    public int getItemCount() {

        if (groups==null)
            return 0;
        else
            return groups.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {

        CardView group;
        TextView groupName, groupMembers;
        CircleImageView groupImage;
        ToggleButton groupRequest;

        DataObjectHolder(View itemView) {
            super(itemView);
            group = itemView.findViewById(R.id.group);
            groupName = itemView.findViewById(R.id.group_name);
            groupMembers = itemView.findViewById(R.id.group_members);
            groupImage = itemView.findViewById(R.id.group_image);
            groupRequest = itemView.findViewById(R.id.group_request);
        }

    }

    public interface OnGroupClickListener{
        void onGroupClick(Group group);
        void onGroupJoinOrLeaveToggleClick(Group group, boolean isJoining);
    }
}
