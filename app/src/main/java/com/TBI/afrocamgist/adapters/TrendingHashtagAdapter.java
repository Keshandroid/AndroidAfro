package com.TBI.afrocamgist.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.HashtagPostsActivity;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TrendingHashtagAdapter extends RecyclerView.Adapter<TrendingHashtagAdapter.DataObjectHolder> {

    private ArrayList<Hashtag> trendingHashtags;
    private Activity context;
    private OnTrendingHashClickListener listener;

    TrendingHashtagAdapter(Activity context, ArrayList<Hashtag> trendingHashtags, OnTrendingHashClickListener listener) {
        this.context = context;
        this.trendingHashtags = trendingHashtags;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_trending_hashtag, viewGroup, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        Hashtag hash = trendingHashtags.get(position);

        Logger.d("hash123",new GsonBuilder().setPrettyPrinting().create().toJson(hash));

        String hashtagSlug = "#" + hash.getHashtagSlug();
        holder.hashtag.setText(hashtagSlug);

        String followersCount = hash.getFollowers() == null ? "0 followers" : (hash.getFollowers().size() + " followers");
        holder.followers.setText(followersCount);

        if (hash.getFollowedByMe() != null)
            holder.follow.setChecked(hash.getFollowedByMe());

        holder.follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    followHashtag(hash);
                    trendingHashtags.get(position).setFollowedByMe(true);
                } else{
                    unFollowHashtag(hash);
                    trendingHashtags.get(position).setFollowedByMe(false);
                }

            }
        });

        holder.hashtag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Logger.d("hashtag99",""+new GsonBuilder().setPrettyPrinting().create().toJson(hash));

                context.startActivity(new Intent(context, HashtagPostsActivity.class).putExtra("hashtag",hash));
            }
        });

    }

    private void followHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.FOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                listener.onHashTagClicked();
            }
        });

    }

    private void unFollowHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.UNFOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                listener.onHashTagClicked();
            }
        });

    }

    @Override
    public int getItemCount() {

        if (trendingHashtags == null)
            return 0;
        else
            return trendingHashtags.size();
    }

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView hashtag, followers;
        ToggleButton follow;

        DataObjectHolder(View itemView) {
            super(itemView);
            hashtag = itemView.findViewById(R.id.hashtag);
            followers = itemView.findViewById(R.id.followers);
            follow = itemView.findViewById(R.id.follow);
        }
    }

    public interface OnTrendingHashClickListener {
        void onHashTagClicked();
    }

}

