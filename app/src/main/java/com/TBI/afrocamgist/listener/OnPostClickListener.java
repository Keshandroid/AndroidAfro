package com.TBI.afrocamgist.listener;

import com.TBI.afrocamgist.model.post.Post;

import java.util.ArrayList;

public interface OnPostClickListener {
    void onPostChecked(Post post);

    void onEditPost(Post post, int position);

    void onDeletePost(Post post);

    void onHidePost(Post post);

    void onCommentClicked(Post post, int position);

    void onCreatePostClicked();

    void onFollowClicked(Post post);
    void onPlayItem(int position);

    void onVideoCount(int videoPostId);
    void onPostViewCount(int postId);
    void onPostView(int postId);

    void onDownloadClick(String videoUrl, String postLink);

    void onTagClicked(ArrayList<String> taggedIds);

}
