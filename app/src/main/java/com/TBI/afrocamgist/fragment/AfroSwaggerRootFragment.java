package com.TBI.afrocamgist.fragment;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroSwaggerRootFragment extends Fragment {

    private String openCamera;

    private NewAfroSwaggerFragment afroSwaggerFragment;
//    private GraphQLAfroSwaggerFragment afroSwaggerFragment;

    public AfroSwaggerRootFragment() {
        // Required empty public constructor
    }

    public AfroSwaggerRootFragment(String openCamera) {
        this.openCamera = openCamera;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_swagger_root, container, false);

        //Original
        /*if (openCamera!=null) {
            FragmentTransaction transaction;
            if (getFragmentManager() != null) {
                transaction = getFragmentManager().beginTransaction();
                afroSwaggerFragment = new AfroSwaggerFragment("openCamera");
                transaction.replace(R.id.layout_afro_swagger, afroSwaggerFragment);
                transaction.commitAllowingStateLoss();
            }
            openCamera = null;
        } else {
            FragmentTransaction transaction;
            if (getFragmentManager() != null) {
                transaction = getFragmentManager().beginTransaction();
                afroSwaggerFragment = new AfroSwaggerFragment();
                transaction.replace(R.id.layout_afro_swagger, afroSwaggerFragment);
                transaction.commitAllowingStateLoss();
            }
        }*/

        //New Design
        if (openCamera!=null) {
            FragmentTransaction transaction;
            if (getChildFragmentManager() != null) {
                transaction = getChildFragmentManager().beginTransaction();

                afroSwaggerFragment = new NewAfroSwaggerFragment("openCamera"); //normal API
//                afroSwaggerFragment = new GraphQLAfroSwaggerFragment("openCamera"); //GraphQL


                transaction.replace(R.id.layout_afro_swagger, afroSwaggerFragment);
                transaction.commitAllowingStateLoss();
            }
            openCamera = null;
        } else {
            FragmentTransaction transaction;
            if (getChildFragmentManager() != null) {
                transaction = getChildFragmentManager().beginTransaction();

                afroSwaggerFragment = new NewAfroSwaggerFragment(); // normal API
//                afroSwaggerFragment = new GraphQLAfroSwaggerFragment(); //GraphQL

                transaction.replace(R.id.layout_afro_swagger, afroSwaggerFragment);
                transaction.commitAllowingStateLoss();
            }
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.e("LLLLL_Swagrer: ","Resume:  .. ");
        afroSwaggerFragment.onResumeFragment();
    }

    @Override
    public void onPause() {
        super.onPause();
        afroSwaggerFragment.onPauseFragment();
        Logger.e("LLLLL_Swagrer: ","Pause:  .. ");
    }

    public void refreshFragment(){
        afroSwaggerFragment.refreshSwagger();
    }

}
