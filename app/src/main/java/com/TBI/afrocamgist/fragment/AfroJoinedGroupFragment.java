package com.TBI.afrocamgist.fragment;


import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.AfroGroupInvitationAdapter;
import com.TBI.afrocamgist.adapters.AfroJoinedGroupAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.home.VideosAdapter;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.groups.GroupDetails;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroJoinedGroupFragment extends Fragment implements AfroJoinedGroupAdapter.OnLeaveGroupClickListener{

    private KProgressHUD hud;
    private GridView joinedGroups;
    private TextView noGroupsJoined;
    private SwipeRefreshLayout swipeContainer;
    private LinearLayout llParent;

    ArrayList<Group> joinedAndInvitationList = new ArrayList<>();
    private AfroJoinedGroupAdapter afroJoinedGroupAdapter;


    public AfroJoinedGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_joined_group, container, false);

        noGroupsJoined = view.findViewById(R.id.no_joined_groups);
        initView(view);
        initSwipeRefresh();

        joinedGroups.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (joinedGroups.getChildAt(0) != null) {
                    swipeContainer.setEnabled(joinedGroups.getFirstVisiblePosition() == 0 && joinedGroups.getChildAt(0).getTop() == 0);
                }
            }
        });

        afroJoinedGroupAdapter = new AfroJoinedGroupAdapter(getActivity(),joinedAndInvitationList,this);
        joinedGroups.setAdapter(afroJoinedGroupAdapter);

        if (Utils.isConnected()){
            getPendingGroupInvitations();
            getAfroJoinedGroups();
        }


        return view;
    }

    private void initView(View view) {

        joinedGroups = view.findViewById(R.id.joinedGroups);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        llParent= view.findViewById(R.id.llParent);

    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected()){
                if(joinedAndInvitationList!=null){
                    joinedAndInvitationList.clear();
                }
                getPendingGroupInvitations();
                getAfroJoinedGroups();
            } else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(getActivity(), getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void getAfroJoinedGroups() {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.JOINED_GROUPS, response -> {
            swipeContainer.setRefreshing(false);
//            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        GroupDetails groupDetails = new Gson().fromJson(response, GroupDetails.class);

                        if (groupDetails.getGroups()!=null && groupDetails.getGroups().size() > 0) {

                            joinedAndInvitationList.addAll(groupDetails.getGroups());
//                            joinedGroups.setAdapter(afroJoinedGroupAdapter);

                            afroJoinedGroupAdapter.notifyDataSetChanged();

                        } else {
                            joinedGroups.setVisibility(View.GONE);
                            noGroupsJoined.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    @Override
    public void onLeaveGroup(Group group) {
        showConfirmLeaveGroupAlert(group);
    }

    private void showConfirmLeaveGroupAlert(Group group) {

        Dialog popup = new Dialog(getActivity(), R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        TextView txtMessage =popup.findViewById(R.id.message);
        txtMessage.setText(getString(R.string.leave_group_alert_message));
        TextView txtLeave =popup.findViewById(R.id.delete);
        txtLeave.setText(getString(R.string.str_leave));
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    leaveGroup(group.getGroupId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void leaveGroup(Integer groupId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.LEAVE_GROUP + groupId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        joinedAndInvitationList.clear();
                        getPendingGroupInvitations();
                        getAfroJoinedGroups();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }


    @Override
    public void onGroupInvitationAcceptClick(Group group) {
        acceptGroupRequest(group.getGroupId());
    }

    private void acceptGroupRequest(Integer groupId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ACCEPT_GROUP + groupId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        joinedAndInvitationList.clear();
                        getPendingGroupInvitations();
                        getAfroJoinedGroups();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void getPendingGroupInvitations() {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.INVITED_GROUPS, response -> {
            swipeContainer.setRefreshing(false);
//            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    Log.d("groupInvitations",""+object.toString());

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        GroupDetails groupDetails = new Gson().fromJson(response, GroupDetails.class);

                        if (groupDetails.getGroups()!=null && groupDetails.getGroups().size() > 0) {
                            joinedAndInvitationList.addAll(groupDetails.getGroups());

                            afroJoinedGroupAdapter.notifyDataSetChanged();


//                            joinedGroups.setAdapter(afroJoinedGroupAdapter);
//                            afroJoinedGroupAdapter.notifyDataSetChanged();

                        } else {
//                            groupInvitations.setVisibility(View.GONE);
//                            noGroupInvitations.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }






}
