package com.TBI.afrocamgist.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.TBI.afrocamgist.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroExplorerFragment extends Fragment implements View.OnClickListener {

    private TextView entertainment, hashtags;
    public static String from = "";

    public AfroExplorerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_explorer, container, false);

        initView(view);
        setClickListener(view);
        if (from.equals("")) {
            from = "";
            entertainment.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background_selected));
            entertainment.setTextColor(getResources().getColor(R.color.white));

            hashtags.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
            hashtags.setTextColor(getResources().getColor(R.color.blue));

            //loadFragment(new AfroEntertainmentFragment());
            loadFragment(new AfroEntertainmentFragmentNew());
        }else {
            from = "hash";
            hashtags.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background_selected));
            hashtags.setTextColor(getResources().getColor(R.color.white));

            entertainment.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
            entertainment.setTextColor(getResources().getColor(R.color.blue));

            loadFragment(new AfroHashtagFragment());
        }

        return view;
    }

    private void initView(View view) {

        entertainment = view.findViewById(R.id.entertainment);
        hashtags = view.findViewById(R.id.hashtags);
    }

    private void setClickListener(View view) {

        view.findViewById(R.id.entertainment).setOnClickListener(this);
        view.findViewById(R.id.hashtags).setOnClickListener(this);
    }

    private void loadFragment(Fragment fragment) {

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_explorer, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.entertainment:
                from = "";
                entertainment.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background_selected));
                entertainment.setTextColor(getResources().getColor(R.color.white));

                hashtags.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                hashtags.setTextColor(getResources().getColor(R.color.blue));

                //loadFragment(new AfroEntertainmentFragment());
                loadFragment(new AfroEntertainmentFragmentNew());

                break;
            case R.id.hashtags:
                from = "hash";
                hashtags.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background_selected));
                hashtags.setTextColor(getResources().getColor(R.color.white));

                entertainment.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                entertainment.setTextColor(getResources().getColor(R.color.blue));

                loadFragment(new AfroHashtagFragment());

                break;
            default:
                break;
        }
    }
}
