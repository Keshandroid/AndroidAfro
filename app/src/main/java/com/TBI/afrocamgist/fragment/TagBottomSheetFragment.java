package com.TBI.afrocamgist.fragment;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.TagUser.SomeOne;
import com.TBI.afrocamgist.TagUser.TagUserModel;
import com.TBI.afrocamgist.adapters.TagBottomsheetAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TagBottomSheetFragment extends BottomSheetDialogFragment{

    TagUserModel tagUserModel;
    RecyclerView rcvTaggedUsers;
    private TagBottomsheetAdapter tagBottomsheetAdapter;
    public ArrayList<Integer> taggedIds = new ArrayList<>();

    public static TagBottomSheetFragment newInstance(ArrayList<String> taggedIds) {
        TagBottomSheetFragment tagBottomSheetFragment = new TagBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taggedIds", taggedIds);
        tagBottomSheetFragment.setArguments(bundle);
        return tagBottomSheetFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tag_bottomsheet, container, false);

        taggedIds = (ArrayList<Integer>) getArguments().getSerializable("taggedIds");
        getAllUserData();

        rcvTaggedUsers = v.findViewById(R.id.rcvTaggedUsers);

        return v;
    }

    private void getAllUserData(){

        Call<TagUserModel> call = BaseServices.getAPI().create(ParameterServices.class).getAllUsers();
        call.enqueue(new Callback<TagUserModel>() {
            @Override
            public void onResponse(@NotNull Call<TagUserModel> call, @NotNull Response<TagUserModel> response) {

                Logger.d("userData", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
                tagUserModel = response.body();
                if(response.isSuccessful()){
                    if(tagUserModel!=null){
                        if(tagUserModel.getAllUsersData().size()>0) {
                            ArrayList<SomeOne> finalTaggedData = checkTagUser(tagUserModel.getAllUsersData());

                            tagBottomsheetAdapter = new TagBottomsheetAdapter(finalTaggedData, getContext());
                            rcvTaggedUsers.setAdapter(tagBottomsheetAdapter);
                            rcvTaggedUsers.setLayoutManager(new LinearLayoutManager(getActivity()));
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<TagUserModel> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    private ArrayList<SomeOne> checkTagUser(ArrayList<SomeOne> allUsersData){
        ArrayList<SomeOne> results = new ArrayList<>();

        for(int i=0; i<allUsersData.size();i++){
            for (int j=0;j<taggedIds.size();j++){
                if (allUsersData.get(i).getUser_id() == taggedIds.get(j)) {
                    SomeOne someOne = new SomeOne();
                    someOne.setUserName(allUsersData.get(i).getUserName());
                    someOne.setUser_id(allUsersData.get(i).getUser_id());
                    someOne.setFirstName(allUsersData.get(i).getFirstName());
                    someOne.setLastName(allUsersData.get(i).getLastName());
                    someOne.setId(allUsersData.get(i).getId());
                    someOne.setFullName(allUsersData.get(i).getFullName());
                    results.add(someOne);
                }
            }
        }
        return results;
    }


}
