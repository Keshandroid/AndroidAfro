package com.TBI.afrocamgist.fragment;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroTalentRootFragment extends Fragment {

    private String openCamera;
    private AfroTalentFragment afroTalentFragment;

    public AfroTalentRootFragment() {
        // Required empty public constructor
    }

    public AfroTalentRootFragment(String openCamera) {
        this.openCamera = openCamera;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_talent_root, container, false);

        if (openCamera==null) {
            FragmentTransaction transaction;
            if (getFragmentManager() != null) {
                transaction = getFragmentManager().beginTransaction();
                afroTalentFragment = new AfroTalentFragment();
                transaction.replace(R.id.layout_afro_talent, afroTalentFragment);
                transaction.commitAllowingStateLoss();
            }
        } else {
            FragmentTransaction transaction;
            if (getFragmentManager() != null) {
                transaction = getFragmentManager().beginTransaction();
                afroTalentFragment = new AfroTalentFragment("openCamera");
                transaction.replace(R.id.layout_afro_talent, new AfroTalentFragment("openCamera"));
                transaction.commitAllowingStateLoss();
            }
            openCamera = null;
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.e("LLLLL_Swagrer: ","Resume:  .. ");
        afroTalentFragment.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        afroTalentFragment.onPause();
        Logger.e("LLLLL_Swagrer: ","Pause:  .. ");
    }

}
