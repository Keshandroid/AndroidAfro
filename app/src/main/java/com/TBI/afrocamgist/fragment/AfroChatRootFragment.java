package com.TBI.afrocamgist.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroChatRootFragment extends Fragment {

    // socket chat
    private AfroSocketChatFragment afroSocketChatFragment;

    public AfroChatRootFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_chat_root, container, false);

        FragmentTransaction transaction;
        if (getFragmentManager() != null) {
            transaction = getFragmentManager().beginTransaction();


            // normal chat
//            transaction.replace(R.id.layout_afro_chat, new AfroChatFragment());

            //KK
            // socket chat
            afroSocketChatFragment = new AfroSocketChatFragment();
            transaction.replace(R.id.layout_afro_chat, afroSocketChatFragment);


            // change the socket chat fragment and socket chat activity to use socket.IO
            // Uncomment code in application class and base activity to use socket.IO

            transaction.commitAllowingStateLoss();
        }

        return view;
    }


    // socket chat
    @Override
    public void onResume() {
        super.onResume();
        Logger.e("LLLLL_Chat: ","Resume:  .. ");
        afroSocketChatFragment.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Logger.e("LLLLL_Chat: ","Pause:  .. ");
        afroSocketChatFragment.onPause();
    }

}
