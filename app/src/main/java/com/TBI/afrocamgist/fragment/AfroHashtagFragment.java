package com.TBI.afrocamgist.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.HashtagPostsActivity;
import com.TBI.afrocamgist.activity.SearchHashtagActivity;
import com.TBI.afrocamgist.activity.ViewEntertainmentImageActivity;
import com.TBI.afrocamgist.adapters.HashtagAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.hashtag.HashtagDetails;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroHashtagFragment extends Fragment implements HashtagAdapter.OnHashtagClickListener {

    private KProgressHUD hud;
    private RecyclerView hashtagList;
    private SwipeRefreshLayout swipeContainer;
    private int nextPage = 1;
    private boolean isLoading = false;
    private HashtagAdapter hashtagAdapter;
    private ArrayList<Hashtag> hashtags = new ArrayList<>();
    protected LocalBroadcastManager localBroadcastManager;
    public AfroHashtagFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
        localBroadcastManager.registerReceiver(mStateReceiver,
                new IntentFilter(HashtagPostsActivity.NOTIFYSTATE));

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_hashtag, container, false);


        initView(view);
        initSwipeRefresh();
        setClickListener(view);

        if (Utils.isConnected())
            getFirstPageHashTags();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(mStateReceiver);
    }

    private void initView(View view) {

        hashtagList = view.findViewById(R.id.hashtag_list);
        swipeContainer = view.findViewById(R.id.swipeContainer);
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getFirstPageHashTags();
            else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(getActivity(), getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void setClickListener(View view) {

        view.findViewById(R.id.search).setOnClickListener(v -> startActivity(new Intent(getActivity(), SearchHashtagActivity.class)));
    }


    private void getFirstPageHashTags() {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        String url = UrlEndpoints.HASHTAGS + "?page=1";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            swipeContainer.setRefreshing(false);
//            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        HashtagDetails hashtagDetails = new Gson().fromJson(response, HashtagDetails.class);
                        nextPage = hashtagDetails.getNextPage();
                        if (hashtagDetails.getHashtagList().size() > 0) {
                            hashtags = new ArrayList<>();
                            hashtags.addAll(hashtagDetails.getHashtagList());
                            setHashtagRecyclerView();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setHashtagRecyclerView() {

        hashtagList.setLayoutManager(new GridLayoutManager(getActivity(),2));
        hashtagAdapter = new HashtagAdapter(hashtags, this);
        hashtagList.setAdapter(hashtagAdapter);
        hashtagList.setNestedScrollingEnabled(false);
        hashtagList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;

                if (recyclerView.getLayoutManager() != null) {
                    visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }



                if (!isLoading && dy > 0 && ((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 12))) {
                    isLoading = true;
                    hashtags.add(null);
                    hashtagAdapter.notifyItemInserted(hashtags.size() - 1);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hashtags.remove(hashtags.size() - 1);
                            hashtagAdapter.notifyItemRemoved(hashtags.size());
                            getHashtagNextPage();
                        }
                    }, 1000);
                }
            }
        });
    }

    private void getHashtagNextPage() {

        String url = UrlEndpoints.HASHTAGS + "?page=" + nextPage;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if (!"".equals(response)) {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        HashtagDetails hashtagDetails = new Gson().fromJson(response, HashtagDetails.class);
                        nextPage = hashtagDetails.getNextPage();
                        hashtags.addAll(hashtagDetails.getHashtagList());
                        hashtagAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    public void onFollowOrUnfollowToggleClick(boolean isFollowed, Hashtag hashtag) {

        if (isFollowed) {
            followHashtag(hashtag);
            updateFollowCount(isFollowed , hashtag.getId());
            updateFollowState(isFollowed , hashtag.getId(),null);
        }else {
            unFollowHashtag(hashtag);
            updateFollowCount(isFollowed , hashtag.getId());
            updateFollowState(isFollowed , hashtag.getId(),null);
        }
    }

    @Override
    public void onHashtagClick(Hashtag hashtag) {
        startActivity(new Intent(getActivity(), HashtagPostsActivity.class).putExtra("hashtag",hashtag));
    }

    private void followHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.FOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {}
        });

    }

    private void unFollowHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.UNFOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {}
        });

    }

    private BroadcastReceiver mStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String post_id = intent.getStringExtra(HashtagPostsActivity.TAG_Id);
            ArrayList<Integer> followerCount = intent.getIntegerArrayListExtra(HashtagPostsActivity.Follow_Count);
            boolean isfollow= intent.getBooleanExtra(HashtagPostsActivity.Follow_State,false);
           // int position = getPosition(post_id);
            updateFollowState(isfollow,post_id,followerCount);

        }
    };

    private void updateFollowState(boolean isFollowed, String tagid,ArrayList<Integer> followers){
        int position = getPosition(tagid);
        if(position!=-1){
            hashtags.get(position).setFollowedByMe(isFollowed);
            if(followers != null){
                hashtags.get(position).setFollowers(followers);
            }
            hashtagList.post(new Runnable()
            {
                @Override
                public void run() {
                    hashtagAdapter.notifyItemChanged(position);
                }
            });

        }
    }
    private void updateFollowCount(boolean isFollowed, String tagid){
        int position = getPosition(tagid);
        if(position!=-1){
            if(isFollowed){
                hashtags.get(position).getFollowers().add(LocalStorage.getUserDetails().getUserId());
            }else {
                hashtags.get(position).getFollowers().remove(LocalStorage.getUserDetails().getUserId());
            }
        }
    }


    private int getPosition(String tagid){
        int position=-1;
        for (int i = 0; i< hashtags.size();i++){
            if(tagid.equals(String.valueOf(hashtags.get(i).getId()))){
                return i;
            }
        }
        return position;
    }

}
