package com.TBI.afrocamgist.fragment;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.StoryConstants;
import com.TBI.afrocamgist.TagUser.TagToBeTagged;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.VideoPreLoadingService;
import com.TBI.afrocamgist.View.DataObjectHolder;
import com.TBI.afrocamgist.View.PlayerViewContainer;
import com.TBI.afrocamgist.activity.CommentsActivity;
import com.TBI.afrocamgist.activity.CreatePostActivity;
import com.TBI.afrocamgist.activity.EditPostActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.ViewEntertainmentImageActivity;
import com.TBI.afrocamgist.adapters.AfroTalentPostAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnPostClickListener;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.media.Media;
import com.TBI.afrocamgist.model.media.MediaDetails;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.story.StoryUserData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import im.ene.toro.widget.Container;
import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnMultiCompressListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.TBI.afrocamgist.activity.CreatePostActivity.location1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postText1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postType1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.tagUserList1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.video1;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroTalentFragment extends Fragment implements OnPostClickListener {

    private KProgressHUD hud;
    private int nextPage = 1;
    private boolean isLoading = false;
    private Container posts;
    private SwipeRefreshLayout swipeContainer;
    private AfroTalentPostAdapter talentPostAdapter;
    private ImageView scrollToTop;
    private ArrayList<Post> talentPosts = new ArrayList<>();
    private ArrayList<File> compressedImageFiles = new ArrayList<>();
    //private ArrayList<TagToBeTagged> tagUserList = new ArrayList<>();
    private List<String> tagUserList = new ArrayList<>();
    private static int SAVED_SCROLL_POSITION = -1;
    private File videoFile;
    private ArrayList<File> imageFiles = new ArrayList<>();
    private String postText = "", location="", openCamera;
    private int lastVideoPlayPosition=-1;
    protected LocalBroadcastManager localBroadcastManager;

    public static String NOTIFY_LIKE_AND_COMMENT="notify_like_and_comment";

    public AfroTalentFragment() {
        // Required empty public constructor
    }

    public AfroTalentFragment(String openCamera) {
        this.openCamera = openCamera;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_talent, container, false);
        localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
        localBroadcastManager.registerReceiver(mFollowStateReceiver,
                new IntentFilter(ProfileActivity.NITIFY_FOLLOW_STATE));

        localBroadcastManager.registerReceiver(mLikeSwaggerTalent,
                new IntentFilter(ViewEntertainmentImageActivity.NOTIFY_LIKE_SWAGGER_TALENT));

        if(openCamera!=null) {
            startActivityForResult(new Intent(getActivity(), CreatePostActivity.class)
                    .putExtra("openCamera", "openCamera")
                    .putExtra("postFor", "afrotalent"), 100);
            openCamera = null;
        }

        initView(view);
        initSwipeRefresh();
        initClickListener();

        if (Utils.isConnected())
            getTalentFirstPagePosts();

        return view;
    }

    private void initView(View view) {

        posts = view.findViewById(R.id.posts);
        scrollToTop = view.findViewById(R.id.scroll_to_top);
        swipeContainer = view.findViewById(R.id.swipeContainer);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(mFollowStateReceiver);
        localBroadcastManager.unregisterReceiver(mLikeSwaggerTalent);
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.e("LLLLL_Resume: ", "Swagger: ");
        if (posts != null && talentPostAdapter != null && lastVideoPlayPosition!=-1) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
            Logger.e("LLLLL_visible_Pos: ",layoutManager.findFirstVisibleItemPosition() +"     "+layoutManager.findFirstCompletelyVisibleItemPosition()+"      "+layoutManager.findLastVisibleItemPosition()+"     "+posts.getChildCount());
            talentPostAdapter.playvideo(posts.findViewHolderForLayoutPosition(lastVideoPlayPosition));
          //  talentPostAdapter.playvideo(posts);
//            posts.onWindowVisibilityChanged(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (posts != null && talentPostAdapter != null && lastVideoPlayPosition!=-1) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
            Logger.e("LLLLL_visible_Pos: ",layoutManager.findFirstVisibleItemPosition() +"     "+layoutManager.findFirstCompletelyVisibleItemPosition()+"      "+layoutManager.findLastVisibleItemPosition()+"     "+layoutManager.getChildCount());
            talentPostAdapter.pausevideo(posts.findViewHolderForLayoutPosition(lastVideoPlayPosition));
        }
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getTalentFirstPagePosts();
            else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(getActivity(), getString(R.string.no_internet_connection_available));
            }
        });
    }

    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible())
        {
            if (!isVisibleToUser)   // If we are becoming invisible, then...
            {
                //pause or stop video
            }

            if (isVisibleToUser)
            {
                //play your video
            }
        }
    }
    private void initClickListener() {

        scrollToTop.setOnClickListener(v -> {
            if (posts.getLayoutManager()!=null) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
                if (layoutManager.findFirstVisibleItemPosition() < 15)
                    posts.getLayoutManager().smoothScrollToPosition(posts, new RecyclerView.State(),0);
                else
                    posts.getLayoutManager().scrollToPosition(0);
            }
        });
    }

    private void getAfroTalentPopularPosts() {

        String url = UrlEndpoints.MOST_POPULAR + "?type=afrotalent";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            swipeContainer.setRefreshing(false);
//            hud.dismiss();
            if ("".equals(response)) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroTalent = new Gson().fromJson(response, PostDetails.class);

                        if (talentPostAdapter!=null && afroTalent.getPosts()!=null) {
                            //startPreLoadingService(afroTalent.getPosts());
                            talentPostAdapter.setPopularPost(afroTalent.getPosts());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }

        });
    }

    private void startPreLoadingService(ArrayList<Post> allPost) {

        ArrayList<String> videoList = new ArrayList<>();

        for (int i=0;i<allPost.size();i++){
            if(allPost.get(i).getPostType().equalsIgnoreCase("video")){
                String url = UrlEndpoints.MEDIA_BASE_URL + allPost.get(i).getPostVideo();
                videoList.add(url);
            }
        }

        Logger.d("talent_cache_popular",new GsonBuilder().setPrettyPrinting().create().toJson(videoList));

        Intent preloadingServiceIntent = new Intent(getContext(), VideoPreLoadingService.class);
        preloadingServiceIntent.putStringArrayListExtra(StoryConstants.VIDEO_LIST, videoList);
        getActivity().startService(preloadingServiceIntent);
    }

    private void getTalentFirstPagePosts() {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        String url = UrlEndpoints.AFROTALENT_POST + "?page=1";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            getAfroTalentPopularPosts();
            if ("".equals(response)) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroTalent = new Gson().fromJson(response, PostDetails.class);
                        nextPage = afroTalent.getNextPage();
                        if (afroTalent.getPosts().size() > 0) {
                            talentPosts = new ArrayList<>();
                            talentPosts.addAll(afroTalent.getPosts());
                            talentPosts.add(0,new Post());
                            talentPosts.add(2, new Post());
                            setPostsRecyclerView();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    private void setPostsRecyclerView() {

        posts.setLayoutManager(new LinearLayoutManager(getActivity()));
        talentPostAdapter = new AfroTalentPostAdapter(getActivity(), talentPosts, this);
        posts.setAdapter(talentPostAdapter);
        posts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = 0, totalItemCount = 0, pastVisibleItem = 0;

                if (recyclerView.getLayoutManager() != null) {
                    visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    pastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }

                if (pastVisibleItem > 1) {
                    scrollToTop.setVisibility(View.VISIBLE);
                } else {
                    scrollToTop.setVisibility(View.GONE);
                }

                if (!isLoading && dy > 0 && ((visibleItemCount + pastVisibleItem) >= (totalItemCount - 7))) {
                    isLoading = true;
//                    talentPosts.add(null);
//                    talentPostAdapter.notifyItemInserted(talentPosts.size() - 1);

                    getTalentNextPagePosts();

                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            talentPosts.remove(talentPosts.size() - 1);
                            talentPostAdapter.notifyItemRemoved(talentPosts.size());
                            getTalentNextPagePosts();
                        }
                    }, 1000);*/
                }
            }
        });
    }

    private void getTalentNextPagePosts() {

        String url = UrlEndpoints.AFROTALENT_POST + "?page=" + nextPage;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if (!"".equals(response)) {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroTalent = new Gson().fromJson(response, PostDetails.class);

//                        talentPosts.remove(talentPosts.size() - 1);
//                        talentPostAdapter.notifyItemRemoved(talentPosts.size());

                        if (afroTalent.getPosts()!=null && afroTalent.getPosts().size() > 0) {
                            nextPage = afroTalent.getNextPage();
                            talentPosts.addAll(afroTalent.getPosts());
                            talentPostAdapter.notifyDataSetChanged();
                        }
                        isLoading = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    @Override
    public void onDeletePost(Post post) {
        showConfirmDeletePostAlert(post);
    }

    @Override
    public void onHidePost(Post post) {
        showConfirmHidePostAlert(post);
    }

    private void showConfirmHidePostAlert(Post post) {

        Dialog popup = new Dialog(getActivity(), R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        TextView message = popup.findViewById(R.id.message);
        TextView delete = popup.findViewById(R.id.delete);

        delete.setText("Hide");
        message.setText("Are you sure you want to hide the post ?");

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    hidePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void showConfirmDeletePostAlert(Post post) {

        Dialog popup = new Dialog(getActivity(), R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    deletePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void deletePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.post_deleted_successfully), Toast.LENGTH_LONG).show();
                        getTalentFirstPagePosts();
                    }

                } catch (Exception e) {
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                    e.printStackTrace();
                }
            }
        });
    }

    private void hidePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId + UrlEndpoints.HIDE;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {
                    Logger.e("LLLLLL_HideRes: ",response);
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.str_post_hide), Toast.LENGTH_LONG).show();
                        getTalentFirstPagePosts();
                    }

                } catch (Exception e) {
//                    Logger.e("LLLLLL_Hide: ",e.getMessage());
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onPostChecked(Post post) {
        likeAndUnlikePost(post.getPostId());
        notifyEntertainmentPosts();
    }

    private void likeAndUnlikePost(Integer postId) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id",postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    }else {
                        //talentPostAdapter.notifyDataSetChanged();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    private void notifyEntertainmentPosts() {
        Intent intent = new Intent(NOTIFY_LIKE_AND_COMMENT);
        localBroadcastManager.sendBroadcast(intent);
    }

    @Override
    public void onFollowClicked(Post post) {
        followUser(post.getUserId());
        setFollowAllUser(post.getUserId(),true);
    }

    private void setFollowAllUser(int userid, boolean isfollow){
        try {
            for (int i = 0; i< talentPosts.size();i++){
                if(talentPosts.get(i).getUserId() !=null && userid == talentPosts.get(i).getUserId()){
/*
                    talentPosts.get(i).setFollowing(isfollow);
                    talentPostAdapter.notifyItemChanged(i);
*/
                    //edited
                    if(talentPosts.get(i).getPrivateAccount()){

                        if(isfollow){
                            talentPosts.get(i).getRequestButtons().get(0).setButtonText("Requested");
                            talentPosts.get(i).setFollowing(false);
                        }else {
                            talentPosts.get(i).getRequestButtons().get(0).setButtonText("Request");
                            talentPosts.get(i).setFollowing(false);
                        }

                    }else {
                        talentPosts.get(i).setFollowing(isfollow);
                    }
                    //edited
                    talentPostAdapter.notifyItemChanged(i);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onPlayItem(int position) {
        Logger.e("LLLLLL_PlayItem: ", "Talent"+position);
         lastVideoPlayPosition = position;
    }

    @Override
    public void onPostViewCount(int postId) {
        callPostViewCount(postId);
    }

    @Override
    public void onPostView(int postId) {

    }

    @Override
    public void onTagClicked(ArrayList<String> taggedIds) {
//        TagBottomSheetFragment tagBottomSheetFragment = new TagBottomSheetFragment();
//        tagBottomSheetFragment.show(getFragmentManager(), "ModalBottomSheet");

        TagBottomSheetFragment.newInstance(taggedIds).show(getFragmentManager(), "ModalBottomSheet");
    }

    private void callPostViewCount(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getPostViewCount(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onVideoCount(int videoPostId) {
        Logger.e("VIDEO_POST_ID1", "222"+videoPostId);
        callVideoCountNew(videoPostId);
    }

    private void callVideoCountNew(int videoPostId){
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getVideoCount(videoPostId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("ViewCount",""+jsonObject.get("counter"));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {}
        });
    }

    @Override
    public void onCreatePostClicked() {
        startActivityForResult(new Intent(getActivity(), CreatePostActivity.class).putExtra("postFor", "afrotalent"), 100);
    }



    private void createPost(Intent data) {
        if (data!=null){
        CreatePostActivity.PostType postType = (CreatePostActivity.PostType) data.getSerializableExtra("postType");

        switch (postType) {

            case IMAGE:
                createImagePost(data);
                break;
            case VIDEO:
                createVideoPost(data);
                break;
            default:
                break;
        }
        } else {
            if (postType1 == CreatePostActivity.PostType.VIDEO) {
                createVideoPost((Intent) null);
            }
        }
    }

    private void createImagePost(Intent data) {
        createImageFile(data.getStringArrayListExtra("images"));
//        uploadImages();
    }

    private void createImageFile(ArrayList<String> imagePaths) {

        imageFiles = new ArrayList<>();
        for (int i = 0; i < imagePaths.size(); i++) {

            try {
                imageFiles.add(new File(imagePaths.get(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (getActivity()!=null) {
            Luban.compress(getActivity(), imageFiles)
                    .putGear(Luban.CUSTOM_GEAR)
                    .launch(new OnMultiCompressListener() {
                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onSuccess(List<File> fileList) {
                            compressedImageFiles = new ArrayList<>(fileList);
                            /*for (File image : imageFiles)
                                image.delete();*/
                            uploadImages();
                        }
                        @Override
                        public void onError(Throwable e) {

                        }
                    });
        }
    }

    private void uploadImages() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (File photo : compressedImageFiles) {
            builder.addFormDataPart(Constants.IMAGES, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload";

        Webservices.getData(getActivity(), Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {
                            for (File image : compressedImageFiles)
                                image.delete();
                            createImagePost(mediaDetails.getMediaList());
                        } else {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int)(100 * bytesRead / contentLength);
            Logger.d("Percentage",percentage+"");
            if (talentPostAdapter!=null) {
                talentPostAdapter.setUploadProgress(percentage);
                if (getActivity()!=null)
                    getActivity().runOnUiThread(() -> talentPostAdapter.notifyItemChanged(1));
            }
        });
    }

    private void createImagePost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createImagePostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(getActivity(), getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    if (talentPosts!=null) {
                        talentPosts.remove(1);
                        talentPostAdapter.notifyDataSetChanged();
                    }
                    getTalentFirstPagePosts();
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    private JSONObject createImagePostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            request.put("post_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "afrotalent");

            JSONArray images = new JSONArray();
            for (Media media : mediaList) {
                images.put(media.getPath());
            }

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("post_type", "image");
            request.put("post_image", images);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void createVideoPost(Intent data) {
        if (data == null) {
            createVideoFile(video1);
        }else {
            createVideoFile(data.getStringExtra("video"));
        }
        uploadVideo();
    }

    private void createVideoFile(String videoPath) {

        try {
            videoFile = new File(videoPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadVideo() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, videoFile.getName(), RequestBody.create(MediaType.parse("video/*"), videoFile));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload-video";

        Webservices.getData(getActivity(), Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {
                            createVideoPost(mediaDetails.getMediaList());
                        } else {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int)(100 * bytesRead / contentLength);
            Logger.d("Percentage",percentage+"");
            if (talentPostAdapter!=null) {
                talentPostAdapter.setUploadProgress(percentage);
                if (getActivity()!=null)
                    getActivity().runOnUiThread(() -> talentPostAdapter.notifyItemChanged(1));
            }
        });
    }

    private void createVideoPost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createVideoPostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(getActivity(), getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    if (talentPosts!=null) {
                        talentPosts.remove(1);
                        talentPostAdapter.notifyDataSetChanged();
                    }
                    postText1 = "";
                    location1 = "";
                    video1 = "";
                    tagUserList1.clear();
                    getTalentFirstPagePosts();
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    private JSONObject createVideoPostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("post_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "afrotalent");
            request.put("post_type", "video");
            request.put("post_video", mediaList.get(0).getPath());
            request.put("thumbnail", mediaList.get(0).getThumbnails().get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void updatePostText(String postText) {
        if (talentPosts.size() > 0) {
            talentPosts.get(SAVED_SCROLL_POSITION).setPostText(postText);

            if (talentPostAdapter!=null) {
                talentPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }

        }
    }

    @Override
    public void onEditPost(Post post, int position) {

        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(getActivity(), EditPostActivity.class)
                .putExtra("post", post), 102);
    }

    private void updateCommentCount(ArrayList<Comment> comments) {

        if (talentPosts.size() > 0) {
            int commentCount = 0;
            for(Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments()!=null && comment.getSubComments().size() > 0){
                    commentCount += comment.getSubComments().size();
                }
            }
            getPostDetails(talentPosts.get(SAVED_SCROLL_POSITION).getPostId());
            talentPosts.get(SAVED_SCROLL_POSITION).setComments(comments);
            talentPosts.get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);

            if (talentPostAdapter!=null) {
                talentPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }
        }
    }

    private void getPostDetails(int postId) {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            //hud.dismiss();
            try {
                Logger.e("LLLLL_JSON: ",response);
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), "Sorry Message Deleted..");
                } else {
                    Post post = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), Post.class);
                    talentPosts.get(SAVED_SCROLL_POSITION).setLikeCount(post.getLikeCount());
                    if (talentPostAdapter!=null) {
                        talentPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
                    }
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }


    @Override
    public void onCommentClicked(Post post, int position) {
        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(getActivity(), CommentsActivity.class)
                .putExtra("comments",post.getComments())
                .putExtra("postId",post.getPostId()),101);
    }

    private BroadcastReceiver mFollowStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int user_id = intent.getIntExtra(ProfileActivity.User_Id,0);
            boolean isfollow = intent.getBooleanExtra(ProfileActivity.IsFollow,false);
            setFollowAllUser(user_id,isfollow);
        }
    };

    private BroadcastReceiver mLikeSwaggerTalent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if (Utils.isConnected())
                getTalentFirstPagePosts();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (video1 != null && !video1.equals("") && data == null) {
            if (talentPosts != null) {
                talentPosts.add(1, null);
                talentPostAdapter.notifyDataSetChanged();
            }
            postText = postText1;
            location = location1;
            tagUserList = tagUserList1;
            createPost(null);
        }

        if (requestCode == 100 && resultCode == RESULT_OK) {
            if (data!=null && data.getSerializableExtra("postType")!=null) {
                if (talentPosts!=null) {
                    talentPosts.add(1, null);
                    talentPostAdapter.notifyDataSetChanged();
                }
                postText = data.getStringExtra("postText");
                location = data.getStringExtra("location");
                tagUserList = (ArrayList<String>) data.getSerializableExtra("tagUserList");
                createPost(data);
            } else if (video1!=null && !video1.equals("") && data == null) {
                if (talentPosts!=null) {
                    talentPosts.add(1, null);
                    talentPostAdapter.notifyDataSetChanged();
                }
                postText = postText1;
                location = location1;
                createPost(null);
            } else {
                getTalentFirstPagePosts();
            }
        } else if (requestCode == 101 && resultCode == RESULT_OK) {
            if (data.getSerializableExtra("comments")!=null) {
                notifyEntertainmentPosts();
                updateCommentCount((ArrayList<Comment>) data.getSerializableExtra("comments"));
            }
        } else if (requestCode == 102 && resultCode == RESULT_OK) {

            if (data.getStringExtra("postText")!=null) {
                updatePostText(data.getStringExtra("postText"));
            }
        }
    }

    @Override
    public void onDownloadClick(String videoUrl, String postLink) {

    }
}
