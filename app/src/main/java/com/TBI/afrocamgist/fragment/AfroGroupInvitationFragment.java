package com.TBI.afrocamgist.fragment;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.AfroGroupInvitationAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.groups.GroupDetails;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroGroupInvitationFragment extends Fragment implements AfroGroupInvitationAdapter.OnGroupClickListener {

    private KProgressHUD hud;
    private GridView groupInvitations;
    private TextView noGroupInvitations;
    private SwipeRefreshLayout swipeContainer;

    public AfroGroupInvitationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_group_invitation, container, false);

        noGroupInvitations = view.findViewById(R.id.no_group_invitations);
        initView(view);
        initSwipeRefresh();

        if (Utils.isConnected())
            getPendingGroupInvitations();

        return view;
    }

    private void initView(View view) {

        groupInvitations = view.findViewById(R.id.invitations);
        swipeContainer = view.findViewById(R.id.swipeContainer);
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getPendingGroupInvitations();
            else
                Utils.showAlert(getActivity(),getString(R.string.please_check_your_internet_connection));
        });
    }

    private void getPendingGroupInvitations() {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.INVITED_GROUPS, response -> {
            swipeContainer.setRefreshing(false);
//            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        GroupDetails groupDetails = new Gson().fromJson(response, GroupDetails.class);

                        if (groupDetails.getGroups()!=null && groupDetails.getGroups().size() > 0) {
                            groupInvitations.setAdapter(new AfroGroupInvitationAdapter(getActivity(),groupDetails.getGroups(),this));
                        } else {
                            groupInvitations.setVisibility(View.GONE);
                            noGroupInvitations.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    @Override
    public void onGroupInvitationAcceptClick(Group group) {
        acceptGroupRequest(group.getGroupId());
    }

    private void acceptGroupRequest(Integer groupId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ACCEPT_GROUP + groupId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        getPendingGroupInvitations();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }
}
