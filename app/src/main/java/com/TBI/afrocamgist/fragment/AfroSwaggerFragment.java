package com.TBI.afrocamgist.fragment;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.BuildConfig;
import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.StoryConstants;
import com.TBI.afrocamgist.TagUser.SomeOneAdapter;
import com.TBI.afrocamgist.TagUser.TagToBeTagged;
import com.TBI.afrocamgist.TagUser.TagUserModel;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.VideoPreLoadingService;
import com.TBI.afrocamgist.activity.CommentsActivity;
import com.TBI.afrocamgist.activity.CreatePostActivity;
import com.TBI.afrocamgist.activity.DashboardActivity;
import com.TBI.afrocamgist.activity.EditPostActivity;
import com.TBI.afrocamgist.activity.MainStoryPostActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.TagPhotoActivity;
import com.TBI.afrocamgist.activity.ViewEntertainmentImageActivity;
import com.TBI.afrocamgist.adapters.AfroPopularPostAdapter;
import com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter;
import com.TBI.afrocamgist.adapters.MainStoryAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnPostClickListener;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.advertisement.Advertisement;
import com.TBI.afrocamgist.model.advertisement.Announcement;
import com.TBI.afrocamgist.model.chat.MessageDetails;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.media.Media;
import com.TBI.afrocamgist.model.media.MediaDetails;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.story.StoryDetail;
import com.TBI.afrocamgist.model.story.StoryUserData;
import com.TBI.afrocamgist.model.suggestedpeople.SuggestedPeopleDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import im.ene.toro.widget.Container;
import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnMultiCompressListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.TBI.afrocamgist.activity.CommentsActivity.isComment;
import static com.TBI.afrocamgist.activity.CreatePostActivity.location1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postCategory1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postText1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postType1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.tagUserList1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.video1;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroSwaggerFragment extends Fragment implements OnPostClickListener {

    private KProgressHUD hud;
    private int nextPage = 1;
    private boolean isLoading = false;
    private RecyclerView rcvStory;
    private Container posts;
    private SwipeRefreshLayout swipeContainer;
    private AfroSwaggerPostAdapter swaggerPostAdapter;
    private MainStoryAdapter mainStoryAdapter;
    private ImageView scrollToTop,imgClose;
    private RelativeLayout alertMessageLayout;
    private ArrayList<Post> swaggerPosts = new ArrayList<>();
    private static int SAVED_SCROLL_POSITION = -1;
    private File videoFile;
    private ArrayList<File> imageFiles = new ArrayList<>();
    private ArrayList<File> compressedImageFiles = new ArrayList<>();
    //private ArrayList<TagToBeTagged> tagUserList = new ArrayList<>();
    private List<String> tagUserList = new ArrayList<>();
    private Connectivity mConnectivity;
    private String postText = "", location = "", openCamera, postCategory = "";
    Boolean userFlag = null;
    private int lastVideoPlayPosition=-1;
    protected LocalBroadcastManager localBroadcastManager;
    private Button btnNotShowAgain,btnOkay;
    private TextView txtDescription;
    private Handler handler;

    public static String NOTIFY_LIKE_AND_COMMENT="notify_like_and_comment";

    private ArrayList<ArrayList<StoryUserData>> storyUserData = new ArrayList<ArrayList<StoryUserData>>();


    //Download Video from url
    String fileN = null ;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123;
    boolean result;
    ProgressDialog mProgressDialog;
    private String waterMarkVideo;
    private Dialog popup;
    private String shareOrDownload = "";

    public AfroSwaggerFragment() {
        // Required empty public constructor
    }

    AfroSwaggerFragment(String openCamera) {
        this.openCamera = openCamera;
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();

        Logger.e("LLLLL_Resume: ", "Swagger: ");
        if (posts != null && swaggerPostAdapter != null && lastVideoPlayPosition!=-1) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
                Logger.e("LLLLL_visible_Pos: ",layoutManager.findFirstVisibleItemPosition() +"     "+layoutManager.findFirstCompletelyVisibleItemPosition()+"      "+layoutManager.findLastVisibleItemPosition()+"     "+posts.getChildCount());
                 swaggerPostAdapter.playvideo(posts.findViewHolderForLayoutPosition(lastVideoPlayPosition));
//                swaggerPostAdapter.playvideo(posts);
                //posts.onWindowVisibilityChanged(View.VISIBLE);
            }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (posts != null && swaggerPostAdapter != null && lastVideoPlayPosition!=-1) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
            Logger.e("LLLLL_visible_Pos: ",layoutManager.findFirstVisibleItemPosition() +"     "+layoutManager.findFirstCompletelyVisibleItemPosition()+"      "+layoutManager.findLastVisibleItemPosition()+"     "+layoutManager.getChildCount());
            swaggerPostAdapter.pausevideo(posts.findViewHolderForLayoutPosition(lastVideoPlayPosition));

            //swaggerPostAdapter.pausevideo(posts);
          //  posts.onWindowVisibilityChanged(View.GONE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(mFollowStateReceiver);
        localBroadcastManager.unregisterReceiver(mLikeSwaggerTalent);
        localBroadcastManager.unregisterReceiver(mStorylineFollowStateReceiver);
        localBroadcastManager.unregisterReceiver(mNotifyMainStoryList);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Logger.e("LLLLL_Resume_is: ", String.valueOf(isVisibleToUser));
        } else {
            Logger.e("LLLLL_Resume_else: ", String.valueOf(isVisibleToUser));
        }
    }

    @Override
    public boolean getUserVisibleHint() {
        Logger.e("LLLLL_Vis: ", String.valueOf(super.getUserVisibleHint()));
        return super.getUserVisibleHint();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Utils.isConnected()){
            preLoadStoryVideos();
        }

        localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());

        localBroadcastManager.registerReceiver(mFollowStateReceiver,
                new IntentFilter(ProfileActivity.NITIFY_FOLLOW_STATE));

        localBroadcastManager.registerReceiver(mLikeSwaggerTalent,
                new IntentFilter(ViewEntertainmentImageActivity.NOTIFY_LIKE_SWAGGER_TALENT));

        localBroadcastManager.registerReceiver(mStorylineFollowStateReceiver,
                new IntentFilter(MainStoryPostActivity.NOTIFY_FOLLOW_STATE_STORYLINE));

        localBroadcastManager.registerReceiver(mNotifyMainStoryList,
                new IntentFilter(MainStoryPostActivity.NOTIFY_MAIN_STORY_LIST));

        if (Utils.isConnected()) {
            getSwaggerFirstPagePosts(true);
            getAdvertisements();
        } else {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
            Utils.showAlert(getActivity(), getString(R.string.no_internet_connection_available));
        }


    }


//    @Override public void onViewStateRestored(@Nullable Bundle bundle) {
//        super.onViewStateRestored(bundle);
//        // Only called on first View creation/recreation.
//        if (userFlag != null) {
//            swaggerPostAdapter
//            swaggerPostAdapter.setUserVisibleHint(userFlag);
//            userFlag = null;
//        }
//    }
//
//    @Override public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (swaggerPostAdapter != null) {
//            swaggerPostAdapter.setUserVisibleHint(isVisibleToUser);
//            userFlag = null;
//        } else {
//            userFlag = isVisibleToUser;
//        }
//    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_swagger, container, false);

        if (openCamera != null) {
            startActivityForResult(new Intent(getActivity(), CreatePostActivity.class)
                    .putExtra("openCamera", "openCamera")
                    .putExtra("postFor", "afroswagger"), 100);
            openCamera = null;
        }

        initView(view);
        initSwipeRefresh();
        initClickListener();
        //setStoryRecyclerview();

        return view;
    }

    private void initView(View view) {

        //rcvStory = view.findViewById(R.id.horizontal_story_list);
        posts = view.findViewById(R.id.posts);
        scrollToTop = view.findViewById(R.id.scroll_to_top);
        swipeContainer = view.findViewById(R.id.swipeContainer);

        imgClose = view.findViewById(R.id.imgClose);
        btnNotShowAgain = view.findViewById(R.id.btnNotShowAgain);
        btnOkay = view.findViewById(R.id.btnOkay);
        txtDescription = view.findViewById(R.id.txtDescription);
        alertMessageLayout = view.findViewById(R.id.alertMessage);

        posts.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initSwipeRefresh() {
        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected()) {
                //setStoryRecyclerview();
                getSwaggerFirstPagePosts(false);
                getAdvertisements();
            } else {
                if (swipeContainer != null) {
                    swipeContainer.setRefreshing(false);
                }
                Utils.showAlert(getActivity(), getString(R.string.no_internet_connection_available));
            }
        });
    }

    private void initClickListener() {
        scrollToTop.setOnClickListener(v -> {
            if (posts.getLayoutManager() != null) {

                LinearLayoutManager layoutManager = (LinearLayoutManager) posts.getLayoutManager();
                if (layoutManager.findFirstVisibleItemPosition() < 15)
                    posts.getLayoutManager().smoothScrollToPosition(posts, new RecyclerView.State(), 0);
                else
                    posts.getLayoutManager().scrollToPosition(0);

                AfroSwaggerPostAdapter.CURRENT_ADVERTISEMENT = 0;
                AfroSwaggerPostAdapter.LAST_ADVERTISEMENT_POSITION = 0;
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertMessageLayout.setVisibility(View.GONE);
            }
        });

        btnNotShowAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Identity.setAnnouncementPopup(getContext(),true);
                alertMessageLayout.setVisibility(View.GONE);
            }
        });

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertMessageLayout.setVisibility(View.GONE);
            }
        });

    }

    private void getSuggestedPeopleList() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.SUGGESTED_PEOPLE, response -> {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
//            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        SuggestedPeopleDetails suggestedPeopleDetails = new Gson().fromJson(response, SuggestedPeopleDetails.class);

                        if (swaggerPostAdapter != null && suggestedPeopleDetails.getSuggestedPeopleList() != null) {
                            swaggerPostAdapter.setSuggestedPeople(suggestedPeopleDetails.getSuggestedPeopleList());
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }

        });
    }

    private void getStoryList() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.STORYLINE, response -> {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }

            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    storyUserData.clear();
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        StoryDetail storyDetail = new Gson().fromJson(response, StoryDetail.class);

                        Logger.d("storyDetail",new GsonBuilder().setPrettyPrinting().create().toJson(storyDetail.getStoryUserData()));

                        //remove comment (Important)
                        if (swaggerPostAdapter != null && storyDetail.getStoryUserData() != null) {
                            storyUserData = storyDetail.getStoryUserData();
                            swaggerPostAdapter.setStoryList(storyUserData);
                            swaggerPostAdapter.notifyItemChanged(0);
                        }
                    }

                } catch (Exception e) {
                    Logger.d("STORY99","111"+e.getLocalizedMessage());
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }

        });
    }

    private void preLoadStoryVideos() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.STORYLINE, response -> {
            if ("".equals(response)) {
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                    } else {
                        StoryDetail storyDetail = new Gson().fromJson(response, StoryDetail.class);

                        //remove comment (Important)
                        if (storyDetail.getStoryUserData() != null) {
                            startPreLoadingService(storyDetail.getStoryUserData());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getAdvertisements() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.ADS, response -> {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    Object json = new JSONTokener(response).nextValue();

                    if (json instanceof JSONObject) {
                        JSONObject object = new JSONObject(response);
                    } else {
                        Type listType = new TypeToken<ArrayList<Advertisement>>() {
                        }.getType();
                        ArrayList<Advertisement> advertisements = new Gson().fromJson(response, listType);

                        if (advertisements.size() > 0) {
                            if (swaggerPostAdapter != null && advertisements != null) {
                                swaggerPostAdapter.setAdvertisementList(advertisements);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    private void getAnnouncement() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.ANNOUNCEMENT, response -> {
            if ("".equals(response)) {
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                    } else {
                        Announcement announcement = new Gson().fromJson(response, Announcement.class);

                        //remove comment (Important)
                        if (announcement.getAnnouncementData() != null) {
                            if(announcement.getAnnouncementData().get(0).getStatus() == 1){
                                txtDescription.setText(announcement.getAnnouncementData().get(0).getMessage());
                                showAnnouncement();
                            }else {
                                alertMessageLayout.setVisibility(View.GONE);
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showAnnouncement() {
        if(!Identity.getAnnouncementPopup(getContext())){
            alertMessageLayout.setVisibility(View.VISIBLE);
        }else {
            alertMessageLayout.setVisibility(View.GONE);
        }
    }

    private void getSwaggerFirstPagePosts(boolean needToLoader) {
        if(needToLoader) {
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();
        }

        String url = UrlEndpoints.AFROSWAGGER_POST + "?page=1";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Logger.d("LOGIN_TOKEN",""+LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            getSuggestedPeopleList();
            getStoryList();
            getAnnouncement();
            if(needToLoader)
            hud.dismiss();
            if ("".equals(response)) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
            } else {
                try {
                    Logger.e("LLLLLL)_Res: ", response);

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroSwagger = new Gson().fromJson(response, PostDetails.class);

                        nextPage = afroSwagger.getNextPage();
                        if (afroSwagger.getPosts().size() > 0) {
                            swaggerPosts = new ArrayList<>();
                            swaggerPosts.addAll(afroSwagger.getPosts());
                            swaggerPosts.add(0, new Post());
                            swaggerPosts.add(1, new Post());
                            swaggerPosts.add(3, new Post());
                            setPostsRecyclerView();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    private void getSwaggerFirstPagePostsRefresh(boolean needToLoader) {
        if(needToLoader) {
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();
        }

        String url = UrlEndpoints.AFROSWAGGER_POST + "?page=1";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Logger.d("LOGIN_TOKEN",""+LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            getSuggestedPeopleList();
            if(needToLoader)
                hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    Logger.e("LLLLLL)_Res: ", response);

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroSwagger = new Gson().fromJson(response, PostDetails.class);

                        //nextPage = afroSwagger.getNextPage();
                        if (afroSwagger.getPosts().size() > 0) {

                            Post addedPost = afroSwagger.getPosts().get(0);

                            //ArrayList<Post> allPostsList = new ArrayList<>(swaggerPostAdapter.getPostsList());

                            for(int i=0;i<swaggerPosts.size();i++){
                                Logger.d("LoadPost","111 ==:== "+ new GsonBuilder().setPrettyPrinting().create().toJson(swaggerPosts.get(i)));
                            }

                            swaggerPosts.remove(swaggerPosts.size()-1);
                            swaggerPosts.remove(2);
                            swaggerPosts.add(1,addedPost);
                            swaggerPosts.add(2, new Post());
                            swaggerPostAdapter.notifyDataSetChanged();

                            for(int i=0;i<swaggerPosts.size();i++){
                                Logger.d("LoadPost","222 ==:== " + new GsonBuilder().setPrettyPrinting().create().toJson(swaggerPosts.get(i)));
                            }



                            //swaggerPostAdapter.addPost(addedPost,allPostsList);

                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    private void setPostsRecyclerView() {

        Logger.d("swaggerSize",""+swaggerPosts.size());

        swaggerPostAdapter = new AfroSwaggerPostAdapter(getActivity(), swaggerPosts, this);
        swaggerPostAdapter.setHasStableIds(true);
        posts.setAdapter(swaggerPostAdapter);
        posts.setNestedScrollingEnabled(false);

        posts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = 0, totalItemCount = 0, pastVisibleItem = 0;

                if (recyclerView.getLayoutManager() != null) {
                    visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    pastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }

                if (pastVisibleItem > 1) {
                    scrollToTop.setVisibility(View.VISIBLE);
                } else {
                    scrollToTop.setVisibility(View.GONE);
                }

               /* Logger.d("recyclerviewScroll","nextpage : "+ nextPage);
                Logger.d("recyclerviewScroll","dy : "+ dy);
                Logger.d("recyclerviewScroll","visibleItemCount : "+ visibleItemCount);
                Logger.d("recyclerviewScroll","pastVisibleItem : "+ pastVisibleItem);
                Logger.d("recyclerviewScroll","totalItemCount : "+ totalItemCount);
                Logger.d("recyclerviewScroll","((visibleItemCount + pastVisibleItem) >= (totalItemCount - 7)) : " +
                        ((visibleItemCount + pastVisibleItem) >= (totalItemCount - 7)));*/

                if (!isLoading && dy > 0 && ((visibleItemCount + pastVisibleItem) >= (totalItemCount - 7))) {
                    isLoading = true;
//                    swaggerPosts.add(null);
//                    swaggerPostAdapter.notifyItemInserted(swaggerPosts.size() - 1);

                    getSwaggerNextPagePosts();
                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            swaggerPosts.remove(swaggerPosts.size() - 1);
                            swaggerPostAdapter.notifyItemRemoved(swaggerPosts.size());
                            getSwaggerNextPagePosts();
                        }
                    }, 1000);*/
                }
            }
        });
    }

    private void startPreLoadingService(ArrayList<ArrayList<StoryUserData>> allPost) {

        ArrayList<String> videoList = new ArrayList<>();

        for (int i=0;i<allPost.size();i++){
            for (int j=0;j<allPost.get(i).size();j++){
                if(allPost.get(i).get(j).getStoryType().equalsIgnoreCase("video")){
                    String url = UrlEndpoints.MEDIA_BASE_URL + allPost.get(i).get(j).getStoryVideo();
                    videoList.add(url);
                }
            }
        }

        Logger.d("videoUrls",new GsonBuilder().setPrettyPrinting().create().toJson(videoList));

        Intent preloadingServiceIntent = new Intent(getActivity(), VideoPreLoadingService.class);
        preloadingServiceIntent.putStringArrayListExtra(StoryConstants.VIDEO_LIST, videoList);
        getActivity().startService(preloadingServiceIntent);
    }

    private void getSwaggerNextPagePosts() {

        String url = UrlEndpoints.AFROSWAGGER_POST + "?page=" + nextPage;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if (!"".equals(response)) {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroSwagger = new Gson().fromJson(response, PostDetails.class);

//                        swaggerPosts.remove(swaggerPosts.size() - 1);
//                        swaggerPostAdapter.notifyItemRemoved(swaggerPosts.size());

                        if (afroSwagger.getPosts() != null && afroSwagger.getPosts().size() > 0) {
                            nextPage = afroSwagger.getNextPage();
                            swaggerPosts.addAll(afroSwagger.getPosts());
                            if (!isComment)
                                swaggerPostAdapter.notifyDataSetChanged();
                            else {
                                swaggerPostAdapter.notifyDataSetChanged();
                               // posts.getLayoutManager().scrollToPosition(SAVED_SCROLL_POSITION);
                            }
                        }
                        isLoading = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    @Override
    public void onDeletePost(Post post) {
        showConfirmDeletePostAlert(post);
    }

    private BroadcastReceiver mFollowStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int user_id = intent.getIntExtra(ProfileActivity.User_Id,0);
            boolean isfollow = intent.getBooleanExtra(ProfileActivity.IsFollow,false);
            setFollowAllUser(user_id,isfollow);
        }
    };

    private BroadcastReceiver mNotifyMainStoryList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (Utils.isConnected())
                getStoryList();
        }
    };


    private BroadcastReceiver mLikeSwaggerTalent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if (Utils.isConnected())
                getSwaggerFirstPagePosts(false);
        }
    };

    private BroadcastReceiver mStorylineFollowStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int user_id = intent.getIntExtra(MainStoryPostActivity.User_Id,0);
            boolean isfollow = intent.getBooleanExtra(MainStoryPostActivity.IsFollow,false);
            setStorylineFollowAllUser(user_id,isfollow);
        }
    };

    @Override
    public void onHidePost(Post post) {
        showConfirmHidePostAlert(post);
    }

    private void showConfirmDeletePostAlert(Post post) {

        Dialog popup = new Dialog(getActivity(), R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    deletePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void showConfirmHidePostAlert(Post post) {

        Dialog popup = new Dialog(getActivity(), R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        TextView message = popup.findViewById(R.id.message);
        TextView delete = popup.findViewById(R.id.delete);

        delete.setText("Hide");
        message.setText("Are you sure you want to hide the post ?");

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    hidePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void deletePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.post_deleted_successfully), Toast.LENGTH_LONG).show();
                        getSwaggerFirstPagePosts(true);
                        getAdvertisements();
                    }

                } catch (Exception e) {
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                    e.printStackTrace();
                }
            }
        });
    }

    private void hidePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId + UrlEndpoints.HIDE;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {
                    Logger.e("LLLLLL_HideRes: ", response);
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.str_post_hide), Toast.LENGTH_LONG).show();
                        getSwaggerFirstPagePosts(true);
                        getAdvertisements();
                    }

                } catch (Exception e) {
//                    Logger.e("LLLLLL_Hide: ",e.getMessage());
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onPostChecked(Post post) {
        likeAndUnlikePost(post.getPostId());
        notifyEntertainmentPosts();
    }

    private void likeAndUnlikePost(Integer postId) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id", postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Logger.d("post_id99KKKKK",""+postId);

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    }else {
                        swaggerPostAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    @Override
    public void onFollowClicked(Post post) {
        followUser(post.getUserId(),post);
        setFollowAllUser(post.getUserId(),true);
    }

    @Override
    public void onPlayItem(int position) {
        Logger.e("LLLLLL_PlayItem: ", ""+position);
        lastVideoPlayPosition = position;
    }

    @Override
    public void onPostViewCount(int postId) {
        callPostViewCount(postId);
    }

    @Override
    public void onPostView(int postId) {
        callPostView(postId);
    }

    @Override
    public void onTagClicked(ArrayList<String> taggedIds) {
//        TagBottomSheetFragment tagBottomSheetFragment = new TagBottomSheetFragment();
//        tagBottomSheetFragment.show(getFragmentManager(), "ModalBottomSheet");

        TagBottomSheetFragment.newInstance(taggedIds).show(getFragmentManager(), "ModalBottomSheet");
    }

    private void callPostViewCount(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getPostViewCount(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    private void callPostView(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getPostView(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("onPostView",""+jsonObject.toString());
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onVideoCount(int videoPostId) {
        Logger.e("VIDEO_POST_ID1", "111"+videoPostId);
        callVideoCountNew(videoPostId);
    }

    private void callVideoCountNew(int videoPostId){
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getVideoCount(videoPostId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("ViewCount",""+jsonObject.get("counter"));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }


    private void setFollowAllUser(int userid, boolean isfollow){
        try {
            for (int i = 0; i< swaggerPosts.size();i++){
                if(swaggerPosts.get(i).getUserId() !=null && userid == swaggerPosts.get(i).getUserId()){
                    /*swaggerPosts.get(i).setFollowing(isfollow);
                    swaggerPostAdapter.notifyItemChanged(i);*/


                    //edited
                    if(swaggerPosts.get(i).getPrivateAccount()){

                        if(isfollow){
                            swaggerPosts.get(i).getRequestButtons().get(0).setButtonText("Requested");
                            swaggerPosts.get(i).setFollowing(false);
                        }else {
                            swaggerPosts.get(i).getRequestButtons().get(0).setButtonText("Request");
                            swaggerPosts.get(i).setFollowing(false);
                        }

                    }else {
                        swaggerPosts.get(i).setFollowing(isfollow);
                    }
                    //edited
                    swaggerPostAdapter.notifyItemChanged(i);

                }
            }
            //swaggerPostAdapter.notifyDataSetChanged();

        }catch (Exception e){
        }

    }

    private void setStorylineFollowAllUser(int userid, boolean isfollow){
        try {
            for (int i=0;i<storyUserData.size();i++){
                for (int j=0;j<storyUserData.get(i).size();j++){
                    if(storyUserData.get(i).get(j).getUserId() != null && userid == storyUserData.get(i).get(j).getUserId()){
                        if(storyUserData.get(i).get(j).getPrivateAccount()){

                            if(isfollow){
                                storyUserData.get(i).get(j).getRequestButtons().get(0).setButtonText("Requested");
                                storyUserData.get(i).get(j).setFollowing(false);
                            }else {
                                storyUserData.get(i).get(j).getRequestButtons().get(0).setButtonText("Request");
                                storyUserData.get(i).get(j).setFollowing(false);
                            }

                        }else {
                            storyUserData.get(i).get(j).setFollowing(isfollow);
                        }
                        swaggerPostAdapter.notifyDataSetChanged();
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void followUser(Integer userId, Post post) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {

            }
        });
    }

    @Override
    public void onCreatePostClicked() {
        startActivityForResult(new Intent(getActivity(), CreatePostActivity.class).putExtra("postFor", "afroswagger"), 100);
    }

    private void createPost(Intent data) {

        if (data != null) {
            CreatePostActivity.PostType postType = (CreatePostActivity.PostType) data.getSerializableExtra("postType");
            switch (postType) {
                case IMAGE:
                    createImagePost(data);
                    break;
                case VIDEO:
                    createVideoPost(data);
                    break;
                default:
                    break;
            }
        } else {
            if (postType1 == CreatePostActivity.PostType.VIDEO) {
                createVideoPost((Intent) null);
            }
        }
    }

    private void createImagePost(Intent data) {
        createImageFile(data.getStringArrayListExtra("images"));
//        uploadImages();
    }

    private void createImageFile(ArrayList<String> imagePaths) {

        imageFiles = new ArrayList<>();
        for (int i = 0; i < imagePaths.size(); i++) {

            try {
                imageFiles.add(new File(imagePaths.get(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (getActivity() != null) {
            Luban.compress(getActivity(), imageFiles)
                    .putGear(Luban.CUSTOM_GEAR)
                    .launch(new OnMultiCompressListener() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onSuccess(List<File> fileList) {
                            compressedImageFiles = new ArrayList<>(fileList);
                            /*for (File image : imageFiles)
                                image.delete();*/
                            uploadImages();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Logger.d("Percentage", e.getMessage() + "");
                        }
                    });
        }
    }

    private void uploadImages() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (File photo : compressedImageFiles) {
            builder.addFormDataPart(Constants.IMAGES, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload";

        Webservices.getData(getActivity(), Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    Logger.d("uploadResponse", object.toString());

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {

                            for (File image : compressedImageFiles)
                                image.delete();


                            Logger.d("post_category",postCategory);
                            if(postCategory.equalsIgnoreCase("normal_post")){
                                createImagePost(mediaDetails.getMediaList());
                            }else {
                                createStoryImagePost(mediaDetails.getMediaList());
                            }
                        } else {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Logger.d("exception1",""+ Log.getStackTraceString(e));
                    Logger.d("exception1",""+e.getLocalizedMessage());
                    //Utils.showAlert(getActivity(), response);
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int) (100 * bytesRead / contentLength);
            Logger.d("Percentage", percentage + "");
            if (swaggerPostAdapter != null) {
                swaggerPostAdapter.setUploadProgress(percentage);
                if (getActivity() != null)
                    getActivity().runOnUiThread(() -> swaggerPostAdapter.notifyItemChanged(2));
            }
        });
    }

    private void createImagePost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createImagePostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(getActivity(), getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    if (swaggerPosts != null) {
                        swaggerPosts.remove(1);
                        swaggerPostAdapter.notifyDataSetChanged();
                    }
                    getSwaggerFirstPagePosts(true);
                    getAdvertisements();
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

  /*  private void createStoryImagePost(ArrayList<Media> mediaList) {

        Logger.d("STORY_IMAGES","111");

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createStoryImagePostJsonRequest(mediaList), headers, UrlEndpoints.STORYLINE, response -> {
            try {

                JSONObject object = new JSONObject(response);

                Logger.d("STORY_IMAGES","111"+object.toString());

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(getActivity(), "Story Added successfully", Toast.LENGTH_LONG).show();
                    if (swaggerPosts != null) {
                        swaggerPosts.remove(1);
                        swaggerPostAdapter.notifyDataSetChanged();
                    }
                    getSwaggerFirstPagePosts(true);
                    getAdvertisements();
                }

            } catch (Exception e) {
                Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }*/

    private void createStoryImagePost(ArrayList<Media> mediaList){

        JsonObject requestObject = createStoryImagePostJsonRequest(mediaList);

        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getStorylineUsers(requestObject);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {

                JsonObject jsonObject = response.body();

                Logger.d("jsonObject", new GsonBuilder().setPrettyPrinting().create().toJson(jsonObject));
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        if (jsonObject.has(Constants.MESSAGE)) {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        } else {
                            Toast.makeText(getActivity(), "Story Added successfully", Toast.LENGTH_LONG).show();
                            if (swaggerPosts != null) {
                                swaggerPosts.remove(1);
                                swaggerPostAdapter.notifyDataSetChanged();
                            }
                            getSwaggerFirstPagePosts(true);
                            getAdvertisements();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    private JSONObject createImagePostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            request.put("post_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "afroswagger");

            JSONArray images = new JSONArray();
            for (Media media : mediaList) {
                images.put(media.getPath());
            }

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("post_type", "image");
            request.put("post_image", images);

            Logger.d("imagelistRequest", new GsonBuilder().setPrettyPrinting().create().toJson(request));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private JsonObject createStoryImagePostJsonRequest(ArrayList<Media> mediaList) {

        JsonObject request = new JsonObject();
        try {

            request.addProperty("story_text", postText);
            request.addProperty("post_lat_long", location);
            request.addProperty("posted_for", "afroswagger");

            JsonArray images = new JsonArray();
            for (Media media : mediaList) {
                images.add(media.getPath());
            }

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JsonArray taggedUsers = new JsonArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.add(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.add("tagged_id",taggedUsers);
            }

            request.addProperty("story_type", "image");
            request.add("story_image", images);

            Logger.d("StoryImagelistRequest", new GsonBuilder().setPrettyPrinting().create().toJson(request));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void createVideoPost(Intent data) {
        if (data == null) {
            Logger.d("DataNull","111");
            createVideoFile(video1);
        } else {
            Logger.d("DataNull","222");
            createVideoFile(data.getStringExtra("video"));
        }
//        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/Video");
//        if (!wallpaperDirectory.exists())
//            wallpaperDirectory.mkdir();
//        File f = new File(wallpaperDirectory, "VID_" + Calendar.getInstance().getTimeInMillis() + ".mp4");
        uploadVideo();
    }


    private void createVideoFile(String videoPath) {
        try {
            videoFile = new File(videoPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadVideo() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, videoFile.getName(), RequestBody.create(MediaType.parse("video/*"), videoFile));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload-video";
        Logger.e("LLLL_Path: ", videoFile.getAbsolutePath());
        Webservices.getData(getActivity(), Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    Logger.e("LLLLLL_Swagger_Res: ", response);
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {
                            if (videoFile != null)
                                videoFile.delete();
                            if (swaggerPostAdapter != null) {
                                swaggerPostAdapter.setUploadProgress(-1);
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(() -> swaggerPostAdapter.notifyItemChanged(2));
                            }

                            if(postCategory.equalsIgnoreCase("normal_post")){
                                createVideoPost(mediaDetails.getMediaList());
                            }else {
                                createStoryVideoPost(mediaDetails.getMediaList());
                            }


                        } else {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        }, (bytesRead, contentLength) -> {
            Logger.e("LLLL_Byte: ", bytesRead + "     " + contentLength);
            int percentage = (int) ((100 * bytesRead) / contentLength);
            Logger.e("LLLL_Percentage", percentage + "");
            if (swaggerPostAdapter != null) {
                swaggerPostAdapter.setUploadProgress(percentage);
                if (getActivity() != null)
                    getActivity().runOnUiThread(() -> swaggerPostAdapter.notifyItemChanged(2));
            }
        });
    }

    private void createVideoPost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createVideoPostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(getActivity(), getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    Logger.e("LLLL_Res2_frag: ", response);
                    if (swaggerPosts != null) {
                        swaggerPosts.remove(1);
                        postText1 = "";
                        location1 = "";
                        video1 = "";
                        tagUserList1.clear();
                        postCategory1 = "";

                        //getSwaggerFirstPagePostsRefresh(false);

                        getSwaggerFirstPagePosts(true);
                        swaggerPostAdapter.notifyDataSetChanged();
                    }

                    getAdvertisements();
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });

    }

    private void createStoryVideoPost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createStoryVideoPostJsonRequest(mediaList), headers, UrlEndpoints.STORYLINE, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(getActivity(), "Story Added successfully", Toast.LENGTH_LONG).show();
                    Logger.e("LLLL_Res2_frag: ", response);
                    if (swaggerPosts != null) {
                        swaggerPosts.remove(1);
                        postText1 = "";
                        location1 = "";
                        video1 = "";
                        tagUserList1.clear();
                        postCategory1 = "";

                        //getSwaggerFirstPagePostsRefresh(false);

                        getSwaggerFirstPagePosts(true);
                        swaggerPostAdapter.notifyDataSetChanged();
                    }

                    getAdvertisements();
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });

    }

    private JSONObject createVideoPostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("post_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "afroswagger");
            request.put("post_type", "video");
            request.put("post_video", mediaList.get(0).getPath());
            request.put("thumbnail", mediaList.get(0).getThumbnails().get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private JSONObject createStoryVideoPostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("story_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "afroswagger");
            request.put("story_type", "video");
            request.put("story_video", mediaList.get(0).getPath());
            request.put("thumbnail", mediaList.get(0).getThumbnails().get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void updatePostText(String postText) {
        if (swaggerPosts.size() > 0) {
            swaggerPosts.get(SAVED_SCROLL_POSITION).setPostText(postText);

            if (swaggerPostAdapter != null) {
                swaggerPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }

        }
    }

    @Override
    public void onEditPost(Post post, int position) {

        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(getActivity(), EditPostActivity.class)
                .putExtra("post", post), 102);
    }

    private void updateCommentCount(ArrayList<Comment> comments) {

        if (swaggerPosts.size() > 0) {
            int commentCount = 0;
            for (Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments() != null && comment.getSubComments().size() > 0) {
                    commentCount += comment.getSubComments().size();
                }
            }
            getPostDetails(swaggerPosts.get(SAVED_SCROLL_POSITION).getPostId());
            swaggerPosts.get(SAVED_SCROLL_POSITION).setComments(comments);
            swaggerPosts.get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);

            if (swaggerPostAdapter != null) {
                swaggerPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }
        }
    }

    private void getPostDetails(int postId) {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            //hud.dismiss();
            try {
                Logger.e("LLLLL_JSON: ",response);
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), "Sorry Message Deleted..");
                } else {
                    Post post = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), Post.class);
                    swaggerPosts.get(SAVED_SCROLL_POSITION).setLikeCount(post.getLikeCount());
                    if (swaggerPostAdapter != null) {
                        swaggerPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
                    }
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onCommentClicked(Post post, int position) {

        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(getActivity(), CommentsActivity.class)
                .putExtra("comments", post.getComments())
                .putExtra("postId", post.getPostId()), 101);

    }

    private void notifyEntertainmentPosts() {
        Intent intent = new Intent(NOTIFY_LIKE_AND_COMMENT);
        localBroadcastManager.sendBroadcast(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (video1 != null && !video1.equals("") && data == null) {

            Logger.d("POSTING111","loading...1");

            if (swaggerPosts != null) {
                swaggerPosts.add(2, null);
                swaggerPostAdapter.notifyDataSetChanged();
            }
            postText = postText1;
            location = location1;
            tagUserList = tagUserList1;
            postCategory = postCategory1;
            createPost(null);
        }

        if (requestCode == 100 && resultCode == RESULT_OK) {
            if (data != null && data.getSerializableExtra("postType") != null) {
                if (swaggerPosts != null) {
                    swaggerPosts.add(2, null);
                    swaggerPostAdapter.notifyDataSetChanged();
                }
                Logger.d("POSTING111","loading...2");

                postText = data.getStringExtra("postText");
                location = data.getStringExtra("location");
                tagUserList = (ArrayList<String>) data.getSerializableExtra("tagUserList");
                postCategory = data.getStringExtra("postCategory");
                createPost(data);
            } else if (video1 != null && !video1.equals("") && data == null) {
                if (swaggerPosts != null) {
                    swaggerPosts.add(2, null);
                    swaggerPostAdapter.notifyDataSetChanged();
                }
                Logger.d("POSTING111","loading...3");

                postText = postText1;
                location = location1;
                postCategory = postCategory1;
                createPost(null);
            } else {
                getSwaggerFirstPagePosts(true);
                getAdvertisements();
            }
        } else if (requestCode == 101 && resultCode == RESULT_OK) {
            if (data.getSerializableExtra("comments") != null) {
                notifyEntertainmentPosts();
                updateCommentCount((ArrayList<Comment>) data.getSerializableExtra("comments"));
            }
        } else if (requestCode == 102 && resultCode == RESULT_OK) {
            if (data.getStringExtra("postText") != null) {
                updatePostText(data.getStringExtra("postText"));
            }
        }
    }

    @Override
    public void onDownloadClick(String videoUrl, String postLink) {
        Logger.d("vidUrl", ""+videoUrl);
        showShareSelectionDialog(getContext(),videoUrl,postLink);
    }


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    // DownloadTask for downloding video from URL
    public class DownloadTask extends AsyncTask<String, Integer, String> {
        private Context context;
        private PowerManager.WakeLock mWakeLock;
        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                java.net.URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                input = connection.getInputStream();


                String filePath = sUrl[0];
                fileN = filePath.substring(filePath.lastIndexOf("/")+1);
                //fileN = "Afrocamgist_" + UUID.randomUUID().toString().substring(0, 10) + ".mp4";
                File filename = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/", fileN);
                output = new FileOutputStream(filename);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();

            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);

            mProgressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();

            if (result != null){
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "Video downloaded", Toast.LENGTH_SHORT).show();
                deleteWatermarkVideoInServer();
            }

            MediaScannerConnection.scanFile(getContext(),
                    new String[]{Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/AfrocamgistVideos/" + fileN}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String newpath, Uri newuri) {
                            Logger.i("ExternalStorage", "Scanned " + newpath + ":");
                            Logger.i("ExternalStorage", "-> uri=" + newuri);
                        }
                    });

            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + fileN;
            shareVideoToSocialMedia(filePath);
        }
    }

    //Get watermarked video from url
    private void callWatermarkVideoAPI(String videoUrl){
        waterMarkVideo = "";
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setLabel("Preparing...")
                .setCancellable(false)
                .show();

        JSONObject params = new JSONObject();
        try {
            params.put("video", videoUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.WATERMARK_VIDEO, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    }else {
                        if(object.getBoolean("result")){
                            waterMarkVideo = object.getString("video_path");
                            String watermarkVideo = UrlEndpoints.MEDIA_BASE_URL + object.getString("video_path");
                            newDownload(watermarkVideo);
                        }else {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //delete watermark video from server
    private void deleteWatermarkVideoInServer(){
        JSONObject params = new JSONObject();
        try {
            params.put("video", waterMarkVideo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.DELETE_WATERMARK_VIDEO;

        Webservices.getData(Webservices.Method.DELETE, params, headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.d("waterMark99","success");
                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showShareSelectionDialog(Context context, String videoUrl, String postLink) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_social_share);
            popup.setCancelable(true);
            popup.show();

            RelativeLayout download = popup.findViewById(R.id.rlDownload);
            RelativeLayout share = popup.findViewById(R.id.rlShare);


            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "download";
                    downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "share";
                    shareVideoLink(postLink);
                    //downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

        }
    }

    private void downloadVideo(String videoUrl){
        String originalFilename = videoUrl.substring(videoUrl.lastIndexOf("/")+1);
        result = checkPermission();
        if(result){
            checkFolder();
            if (!isConnectingToInternet(getContext())) {
                Toast.makeText(getContext(), "Please Connect to Internet", Toast.LENGTH_LONG).show();
                return;
            }

            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename);
            if(file.exists()){
                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename;
                shareVideoToSocialMedia(filePath);
            }else {
                callWatermarkVideoAPI(videoUrl);
            }
        }
    }

    private void shareVideoLink(String videoUrl){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
        i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
        i.putExtra(Intent.EXTRA_TEXT, videoUrl);
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    private void shareVideoToSocialMedia(String filePath){

        MediaScannerConnection.scanFile(getContext(), new String[] { filePath },

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                        shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, "Afrocamgist");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);


                        startActivity(Intent.createChooser(shareIntent,
                                "Afrocamgist"));

                    }
                });
    }

    //hare you can start downloding video
    public void newDownload(String url) {
        final DownloadTask downloadTask = new DownloadTask(getContext());
        downloadTask.execute(url);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public void checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }

    //Here you can check App Permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkFolder();
                } else {
                    //code for deny
                    checkAgain();
                }
                break;
        }
    }

    //hare you can check folfer whare you want to store download Video
    public void checkFolder() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/";
        File dir = new File(path);
        boolean isDirectoryCreated = dir.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated = dir.mkdir();
        }
        if (isDirectoryCreated) {
            // do something
            Logger.d("Folder", "Already Created");
        }
    }

}
