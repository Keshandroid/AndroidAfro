package com.TBI.afrocamgist.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.CommentAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.comment.Comment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StoryCommentBottomSheetFragment extends BottomSheetDialogFragment implements CommentAdapter.OnCommentClickListener{

    public static ArrayList<Comment> comments = new ArrayList<>();
    private int postId = 0;
    private EditText userComment;
    private CommentAdapter adapter;
    private KProgressHUD hud;
    private TextView replyTo;
    private int parentPosition = -1;
    private Comment parentComment;
    private int subCommentPosition = -1;
    private Comment subComment;
    RecyclerView commentList;
    private boolean isEditComment = false;
    public static boolean isComment = true,isCommentPosted = false;
    private boolean isApiCall = false;

    private View itemView;

    private DialogDismissListener mListener;

    public StoryCommentBottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (context instanceof DialogDismissListener) {
            mListener = (DialogDismissListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public static StoryCommentBottomSheetFragment newInstance(ArrayList<Comment> comment, Integer postId) {
        StoryCommentBottomSheetFragment bottomSheetFragment = new StoryCommentBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("comments", comment);
        bundle.putInt("postId",postId);
        bottomSheetFragment .setArguments(bundle);

        return bottomSheetFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comment_bottomsheet, container, false);
    }

    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        /*BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);

            }
        });
        return dialog;*/
        return new BottomSheetDialog(requireContext(), getTheme());  //set your created theme here

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.itemView = view;

        comments = (ArrayList<Comment>) getArguments().getSerializable("comments");
        postId = getArguments().getInt("postId");

        initView();
        initClickListener();
        initTextChangeListener();
        setRecyclerView();
    }

    private void initView() {

        userComment = itemView.findViewById(R.id.user_comment);
        replyTo = itemView.findViewById(R.id.reply_to);
    }

    private void initClickListener() {

        itemView.findViewById(R.id.comment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isConnected()) {
                    if (!userComment.getText().toString().equals("")) {
                        if(!isApiCall) {
                            if (isEditComment)
                                editComment();
                            else if (parentPosition == -1)
                                postComment();
                            else {
                                replyToComment();
                            }
                        }
                    }
                } else {
                    Utils.showAlert(getActivity(), getString(R.string.no_internet_connection_available));
                }
            }
        });

        itemView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemView.findViewById(R.id.replying_layout).setVisibility(View.GONE);
                userComment.setText("");
                parentPosition = -1;
            }
        });

        itemView.findViewById(R.id.back).setOnClickListener(v -> {
            //onBackPressed();
        });
    }

    //set comments listing view
    private void setRecyclerView() {

        if (comments!=null && comments.size() > 0) {
            commentList = itemView.findViewById(R.id.comments);
            commentList.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new CommentAdapter(getActivity(), comments, this);
            commentList.setAdapter(adapter);

            commentList.scrollToPosition(comments.size()-1);

            itemView.findViewById(R.id.comments).setVisibility(View.VISIBLE);
            itemView.findViewById(R.id.no_comments).setVisibility(View.GONE);

        } else {

            itemView.findViewById(R.id.comments).setVisibility(View.GONE);
            itemView.findViewById(R.id.no_comments).setVisibility(View.VISIBLE);
        }
    }

    //add comment to post API
    private void postComment() {

        Logger.d("comment11","--story_id--"+postId);

        JSONObject request = new JSONObject();
        try {
            request.put("story_id",postId);
            request.put("comment_text",userComment.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        isApiCall = true;
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.STORY_COMMENTS, response -> {
            isApiCall = false;
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    isCommentPosted = true;
                    addComment(object.getJSONObject("data").getInt("comment_id"));
                }

            } catch (Exception e) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    //display new comment on view
    private void addComment(int commentId) {

        Comment comment = new Comment();

        comment.setCommentId(commentId);
        comment.setCommentText(userComment.getText().toString());
        comment.setCommentedBy(LocalStorage.getUserDetails());
        comment.setLiked(false);

        comments.add(comment);
        if (adapter==null){
            setRecyclerView();
        } else {
            adapter.notifyDataSetChanged();
            commentList.scrollToPosition(comments.size() - 1);
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        itemView.findViewById(R.id.comments).setVisibility(View.VISIBLE);
        itemView.findViewById(R.id.no_comments).setVisibility(View.GONE);

        userComment.setText("");
    }

    //reply to comment API call
    private void replyToComment() {

        JSONObject request = new JSONObject();
        try {
            request.put("story_id",postId);
            request.put("comment_text",userComment.getText().toString());
            request.put("comment_parent_id",parentComment.getCommentId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        isApiCall = true;
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.STORY_COMMENTS, response -> {
            try {
                isApiCall = false;
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    isCommentPosted = true;
                    addReply(object.getJSONObject("data").getInt("comment_id"));
                }

            } catch (Exception e) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    //display reply on listing view
    private void addReply(int commentId) {

        Comment comment = new Comment();

        comment.setCommentId(commentId);
        comment.setCommentText(userComment.getText().toString());
        comment.setCommentedBy(LocalStorage.getUserDetails());
        comment.setLiked(false);

        ArrayList<Comment> subComments = new ArrayList<>();

        if (parentComment.getSubComments()!=null)
            subComments = parentComment.getSubComments();

        subComments.add(comment);
        parentComment.setSubComments(subComments);

        comments.set(parentPosition, parentComment);
        commentList.scrollToPosition(parentPosition);

        adapter.notifyDataSetChanged();

        parentPosition = -1;

        userComment.setText("");
    }

    //edit comment API call
    private void editComment() {

        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(false)
                .show();

        JSONObject request = new JSONObject();
        try {
            request.put("comment_text",userComment.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        isApiCall = true;
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.STORY_COMMENTS + "/" + (subComment==null ? parentComment.getCommentId() : subComment.getCommentId());

        Webservices.getData(Webservices.Method.PUT, request,headers, url, response -> {
            hud.dismiss();
            isApiCall = false;
            try {

                JSONObject object = new JSONObject(response);

                Logger.d("editCommentStory",""+object.toString());

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    isCommentPosted = true;
                    updateEditedComment();
                }

            } catch (Exception e) {
                Logger.d("executed111","yes...");
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    //update view after comment edit
    private void updateEditedComment() {

        if (subComment==null) {
            parentComment.setCommentText(userComment.getText().toString());
        } else {
            parentComment.getSubComments().get(subCommentPosition).setCommentText(userComment.getText().toString());
        }
        comments.set(parentPosition,parentComment);

        parentPosition = -1;
        isEditComment = false;
        adapter.notifyDataSetChanged();

        userComment.setText("");
    }

    @Override
    public void onLikeCommentClicked(Comment comment) {

        Logger.d("clickedItem", new GsonBuilder().setPrettyPrinting().create().toJson(comment));

        likeComment(comment.getCommentId());

    }

    @Override
    public void onReplyClicked(int position,Comment parentComment, Comment comment) {

        String replyingTo = "Replying to ";
        parentPosition = position;
        this.parentComment = parentComment;

        if (comment==null) {
            String reply = "@" + parentComment.getCommentedBy().getFirstName() + " " + parentComment.getCommentedBy().getLastName() + " -";
            replyingTo += parentComment.getCommentedBy().getFirstName() + " " + parentComment.getCommentedBy().getLastName();
            userComment.setText(reply);
            userComment.setSelection(reply.length());
        } else {
            String reply = "@" + comment.getCommentedBy().getFirstName() + " " + comment.getCommentedBy().getLastName() + " -";
            replyingTo += comment.getCommentedBy().getFirstName() + " " + comment.getCommentedBy().getLastName();
            userComment.setText(reply);
            userComment.setSelection(reply.length());
        }

        itemView.findViewById(R.id.replying_layout).setVisibility(View.VISIBLE);
        replyTo.setText(replyingTo);
    }

    @Override
    public void onDeleteCommentClicked(int parentPosition, Comment parentComment, int position, Comment comment) {

        if(Utils.isConnected())
            deleteComment(parentPosition,parentComment,position,comment);
        else
            Utils.showAlert(getActivity(), getString(R.string.no_internet_connection_available));
    }

    //delete comment API call
    private void deleteComment(int parentPosition, Comment parentComment, int position, Comment comment) {

        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.STORY_COMMENTS + "/" + (comment==null ? parentComment.getCommentId() : comment.getCommentId());

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(),headers, url, response -> {
            hud.dismiss();
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    isCommentPosted = true;
                    removeComment(parentPosition, parentComment, position, comment);
                }

            } catch (Exception e) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    //remove deleted comment from view
    private void removeComment(int parentPosition, Comment parentComment, int position, Comment comment) {

        if (comment == null) {
            comments.remove(parentPosition);
            adapter.notifyDataSetChanged();

            if (comments.size() == 0) {
                itemView.findViewById(R.id.comments).setVisibility(View.GONE);
                itemView.findViewById(R.id.no_comments).setVisibility(View.VISIBLE);
            }

        } else {

            ArrayList<Comment> subComments = parentComment.getSubComments();
            subComments.remove(position);

            parentComment.setSubComments(subComments);

            comments.set(parentPosition,parentComment);

            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onEditCommentClicked(int parentPosition, Comment parentComment, int position, Comment comment) {

        this.parentPosition = parentPosition;
        this.parentComment = parentComment;
        subCommentPosition = position;
        subComment = comment;
        isEditComment = true;

        if (comment==null) {
            userComment.setText(parentComment.getCommentText());
            userComment.setSelection(parentComment.getCommentText().length());
        } else {
            userComment.setText(comment.getCommentText());
            userComment.setSelection(comment.getCommentText().length());
        }

    }

    private void initTextChangeListener() {

        userComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ("".equals(userComment.getText().toString())) {
                    itemView.findViewById(R.id.replying_layout).setVisibility(View.GONE);
                    parentPosition = -1;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //like comment API call
    private void likeComment(int commentId) {

        JSONObject request = new JSONObject();
        try {
            request.put("comment_id",commentId);
            request.put("like_type","storycomment");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Logger.d("comment_id",""+commentId);
        Logger.d("Token1", LocalStorage.getLoginToken());

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.STORY_LIKE_UNLIKE, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {

                    Logger.d("commentLike",new GsonBuilder().setPrettyPrinting().create().toJson(object));


                    isCommentPosted = true;
                }

            } catch (Exception e) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        isComment = true;
        mListener.onDialogDismiss(comments);
    }

    public interface DialogDismissListener {
        void onDialogDismiss(ArrayList<Comment> comments);
    }

    /*
    @Override
    public void onBackPressed() {
        if (isCommentPosted){
            isComment = true;
            Intent intent = new Intent();
            intent.putExtra("comments",comments);
            setResult(RESULT_OK,intent);
            finish();
        } else {
            isComment = true;
            super.onBackPressed();
        }
    }
*/
}
