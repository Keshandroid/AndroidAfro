package com.TBI.afrocamgist.fragment;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;

import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.ArchivedUserActivity;
import com.TBI.afrocamgist.activity.ChatActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.SocketChatActivity;
import com.TBI.afrocamgist.adapters.AfroChatAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.chat.Conversation;
import com.TBI.afrocamgist.model.chat.ConversationDetails;
import com.TBI.afrocamgist.model.user.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.Ack;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroChatFragment extends Fragment implements AfroChatAdapter.OnChatItemClickListener, ConnectivityListener {

    private KProgressHUD hud;
    private RecyclerView conversationList;
    private SwipeRefreshLayout swipeContainer;
    private Handler handler;
    private AfroChatAdapter afroChatAdapter;
    protected LocalBroadcastManager localBroadcastManager;
    private User user;
    private ArrayList<Conversation> conversations = new ArrayList<>();

    private LinearLayout llArchived;
    private Dialog popup;

    public static String NOTIFY_TAB_COUNTER="notify_tab_counter";

    public AfroChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_chat, container, false);

        localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
        localBroadcastManager.registerReceiver(mNotifyCounter,
                new IntentFilter(ChatActivity.NOTIFY_COUNTER));

        initView(view);
        initSwipeRefresh();

        if (Utils.isConnected())
            getConversationListNew();

        llArchived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ArchivedUserActivity.class));
            }
        });

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utils.isConnected()) {
            //getConversationListNew();
            setNotificationCounter();
            checkIsArchivedMessagesAvailable();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initView(View view) {

        swipeContainer = view.findViewById(R.id.swipeContainer);
        conversationList = view.findViewById(R.id.conversation_list);
        llArchived = view.findViewById(R.id.llArchived);
    }

    private BroadcastReceiver mNotifyCounter = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getConversationListNew();
        }
    };

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getConversationList();
            else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(getActivity(), getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void getConversationList() {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MESSAGES, response -> {
            swipeContainer.setRefreshing(false);
//            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        ConversationDetails conversationDetails = new Gson().fromJson(response, ConversationDetails.class);
                        if (conversationDetails.getConversationList().size() > 0) {
                            conversations = new ArrayList<>();
                            conversations.addAll(conversationDetails.getConversationList());
                            setConversationList();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void setConversationList() {
        if(afroChatAdapter==null){
            conversationList.setLayoutManager(new LinearLayoutManager(getActivity()));
            afroChatAdapter = new AfroChatAdapter(conversations, this,getContext());
            conversationList.setAdapter(afroChatAdapter);
        }else{
            afroChatAdapter.notifydata(conversations);
        }
    }

    private void setNotificationCounter() {
        handler = new Handler();
        handler.postDelayed(runnable, 1000);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
//            Logger.d("ChatConversation", "Called");
            if (getActivity()!=null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (afroChatAdapter!=null) {

                            Log.d("isRunning1","yes........");

                            getConversationListNew();
                        }
                        // Code to run on UI thread
                    }
                });
            }
            handler.postDelayed(this, 1000);
        }
    };

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (handler != null)
            handler.removeCallbacks(runnable);
    }

    @Override
    public void onChatItem(User user, int position, boolean loading) {
        startActivity(new Intent(getActivity(), ChatActivity.class).putExtra("user",user).putExtra("loading",loading));
    }

    @Override
    public void onMessagReceived(User user) {
        this.user = user;
        //getConversation();
    }

    @Override
    public void onLongClickItem(User user) {
        showDialog(getContext(),LocalStorage.getUserDetails().getUserId(),user.getUserId());
    }

    public void showDialog(Context context, Integer fromId, Integer toId) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_archived_delete);
            popup.setCancelable(true);
            popup.show();

            RelativeLayout archived = popup.findViewById(R.id.rlArchived);
            RelativeLayout delete = popup.findViewById(R.id.rlDelete);

            archived.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();
                    callArchived(fromId,toId);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callDelete(fromId,toId);
                    popup.dismiss();

                }
            });

        }
    }

    private void callDelete(Integer fromId, Integer toId){

    /*    ArrayList<Integer> userIds = new ArrayList<>();
        userIds.add(toId);

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(userIds.get(0));

        JSONObject data = new JSONObject();
        try {
            data.put("from_id", fromId);
            data.put("to_id", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.deleteUserfromMessageList, data, new Ack() {
                @Override
                public void call(Object... args) {
                    swipeContainer.setRefreshing(false);
                    Log.i("SocketEvent", "removeArchiveMessages with Ack()");
                    //Log.i("SocketEvent", "Message List = "+ Arrays.toString(args));// print complete response
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            *//*if (args[1] != null) {
                                JSONObject jsonObject = (JSONObject) args[1];

                                ConversationDetails conversationDetails = new Gson().fromJson(String.valueOf(jsonObject), ConversationDetails.class);

                                if (conversationDetails != null) {
                                    if (conversationDetails.getConversationList().size() > 0) {
                                        conversations = new ArrayList<>();
                                        conversations.addAll(conversationDetails.getConversationList());
                                        setConversationList();
                                    }
                                }
                            }*//*
                        }
                    });

                }
            });
        }*/





        ArrayList<Integer> userIds = new ArrayList<>();
        userIds.add(toId);

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(userIds.get(0));

        JSONObject request = new JSONObject();
        try {
            request.put("from_id", fromId);
            request.put("to_id", jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());


        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.REMOVE_USERS_FROM_MESSAGE_LIST, response -> {
            try {
                JSONObject object = new JSONObject(response);
                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Log.d("ARCHIVED_REMOVE",new GsonBuilder().setPrettyPrinting().create().toJson(object));
                    checkIsArchivedMessagesAvailable();
                }

            } catch (Exception e) {
                Logger.d("executed111","yes...");
                e.printStackTrace();
            }
        });


    }

    private void callArchived(Integer fromId, Integer toId){

        /*ArrayList<Integer> userIds = new ArrayList<>();
        userIds.add(toId);

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(userIds.get(0));

        JSONObject data = new JSONObject();
        try {
            data.put("from_id", fromId);
            data.put("to_id", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            Log.i("SocketEvent", "params : "+new GsonBuilder().setPrettyPrinting().create().toJson(data));

            UrlEndpoints.socketIOClient.emit(UrlEndpoints.archiveMessages, data, new Ack() {
                @Override
                public void call(Object... args) {
                    swipeContainer.setRefreshing(false);
                    Log.i("SocketEvent", "ArchivedMessage with Ack()");
                    Log.i("SocketEvent", "ArchivedMessage = "+ Arrays.toString(args));// print complete response
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            checkIsArchivedMessagesAvailable();
                        }
                    });

                }
            });
        }*/




        ArrayList<Integer> userIds = new ArrayList<>();
        userIds.add(toId);

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(userIds.get(0));

        JSONObject request = new JSONObject();
        try {
            request.put("from_id", fromId);
            request.put("to_id", jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());


        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.ARCHIVED_USERS, response -> {
            try {
                JSONObject object = new JSONObject(response);
                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Log.d("ARCHIVED_ADD",new GsonBuilder().setPrettyPrinting().create().toJson(object));
                    checkIsArchivedMessagesAvailable();
                }

            } catch (Exception e) {
                Logger.d("executed111","yes...");
                e.printStackTrace();
            }
        });



    }

    private void checkIsArchivedMessagesAvailable(){
        /*JSONObject data = new JSONObject();
        try {
            data.put("from_id", LocalStorage.getUserDetails().getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.getArchiveMessages, data, new Ack() {
                @Override
                public void call(Object... args) {
                    swipeContainer.setRefreshing(false);
                    Log.i("SocketEvent", "GetArchivedMessages with Ack()");
                    Log.i("SocketEvent", "Message List = "+ Arrays.toString(args));// print complete response
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (args[1] != null) {
                                try {
                                    JSONObject jsonObject = (JSONObject) args[1];

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    if(jsonArray.length() > 0){
                                        llArchived.setVisibility(View.VISIBLE);
                                    }else {
                                        llArchived.setVisibility(View.GONE);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });

                }
            });
        }*/


        JSONObject request = new JSONObject();
        try {
            request.put("from_id", LocalStorage.getUserDetails().getUserId());
            request.put("type", "archivedAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());


        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.GET_ARCHIVED_USERS, response -> {
            try {
                JSONObject object = new JSONObject(response);
                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Log.d("ARCHIVED_GET",new GsonBuilder().setPrettyPrinting().create().toJson(object));
                    ConversationDetails conversationDetails = new Gson().fromJson(response, ConversationDetails.class);
                    if(conversationDetails.getConversationList().size() > 0){
                        llArchived.setVisibility(View.VISIBLE);
                    }else {
                        llArchived.setVisibility(View.GONE);
                    }
                }

            } catch (Exception e) {
                Logger.d("executed111","yes...");
                e.printStackTrace();
            }
        });


    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected){
            setNotificationCounter();
        }

    }

    private void getConversationListNew(){

        Call<ConversationDetails> call = BaseServices.getAPI().create(ParameterServices.class).getMessagesList();
        call.enqueue(new Callback<ConversationDetails>() {
            @Override
            public void onResponse(@NotNull Call<ConversationDetails> call, @NotNull Response<ConversationDetails> response) {

//                Logger.d("message123", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
                ConversationDetails conversationDetails = response.body();
                if(response.isSuccessful()){
                    if(conversationDetails!=null){
                        if (conversationDetails.getConversationList().size() > 0) {
                            conversations = new ArrayList<>();
                            conversations.addAll(conversationDetails.getConversationList());
                            setConversationList();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ConversationDetails> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        /*if (handler != null)
            handler.removeCallbacks(runnable);*/
    }
}
