package com.TBI.afrocamgist.fragment;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.FindGroupActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroGroupFragment extends Fragment implements View.OnClickListener {

    private TextView myGroups, joinedGroups, invitations;

    public AfroGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_group, container, false);

        initView(view);
        setClickListener(view);
        loadFragment(new AfroAllForumFragment());
        return view;
    }

    private void initView(View view) {

        myGroups = view.findViewById(R.id.myGroups);
        joinedGroups = view.findViewById(R.id.joinedGroups);
        invitations = view.findViewById(R.id.invitations);
    }

    private void setClickListener(View view) {

        view.findViewById(R.id.myGroups).setOnClickListener(this);
        view.findViewById(R.id.joinedGroups).setOnClickListener(this);
        view.findViewById(R.id.invitations).setOnClickListener(this);
        view.findViewById(R.id.search).setOnClickListener(this);
    }

    private void loadFragment(Fragment fragment) {

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_groups,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        /*FragmentTransaction transaction;
        if (getFragmentManager() != null) {
            transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.layout_groups, fragment);
            transaction.commitAllowingStateLoss();
        }*/
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.myGroups:

                //original
                /*myGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background_selected));
                myGroups.setTextColor(getResources().getColor(R.color.white));

                joinedGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                joinedGroups.setTextColor(getResources().getColor(R.color.blue));
                invitations.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                invitations.setTextColor(getResources().getColor(R.color.blue));

                loadFragment(new AfroMyGroupsFragment());*/


                myGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background_selected));
                myGroups.setTextColor(getResources().getColor(R.color.white));

                joinedGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                joinedGroups.setTextColor(getResources().getColor(R.color.blue));
                invitations.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                invitations.setTextColor(getResources().getColor(R.color.blue));

                loadFragment(new AfroAllForumFragment());

                break;
            case R.id.joinedGroups:

                joinedGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background_selected));
                joinedGroups.setTextColor(getResources().getColor(R.color.white));

                myGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                myGroups.setTextColor(getResources().getColor(R.color.blue));
                invitations.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                invitations.setTextColor(getResources().getColor(R.color.blue));

                loadFragment(new AfroJoinedGroupFragment());

                break;
            case R.id.invitations:

                //original
                /*invitations.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background_selected));
                invitations.setTextColor(getResources().getColor(R.color.white));

                myGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                myGroups.setTextColor(getResources().getColor(R.color.blue));
                joinedGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                joinedGroups.setTextColor(getResources().getColor(R.color.blue));

                loadFragment(new AfroGroupInvitationFragment());*/

                invitations.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background_selected));
                invitations.setTextColor(getResources().getColor(R.color.white));

                joinedGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                joinedGroups.setTextColor(getResources().getColor(R.color.blue));
                myGroups.setBackground(getResources().getDrawable(R.drawable.afro_group_tab_background));
                myGroups.setTextColor(getResources().getColor(R.color.blue));

                loadFragment(new AfroMyGroupsFragment());

                break;
            case R.id.search:
                startActivity(new Intent(getActivity(), FindGroupActivity.class));
                break;
            default:
                break;
        }
    }
}
