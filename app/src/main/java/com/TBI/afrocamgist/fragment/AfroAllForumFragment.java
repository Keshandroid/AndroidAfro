package com.TBI.afrocamgist.fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.AfroViewGroupPostActivity;
import com.TBI.afrocamgist.activity.FindGroupActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.adapters.AfroJoinedGroupAdapter;
import com.TBI.afrocamgist.adapters.FindGroupAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.groups.GroupDetails;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AfroAllForumFragment extends Fragment implements FindGroupAdapter.OnGroupClickListener {

    private KProgressHUD hud;
    private RecyclerView allForum;
    private TextView noGroupsJoined;

    private SwipeRefreshLayout swipeContainer;
    private LinearLayout llParent;

    public static String NOTIFY_JOIN_STATE_FORUM="NOTIFY_JOIN_STATE_FORUM";
    protected LocalBroadcastManager localBroadcastManager;


    private String query = "";

    public AfroAllForumFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        localBroadcastManager = LocalBroadcastManager.getInstance(getContext());

        localBroadcastManager.registerReceiver(mJoinStateReceiver,
                new IntentFilter(NOTIFY_JOIN_STATE_FORUM));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(mJoinStateReceiver);

    }

    private BroadcastReceiver mJoinStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Utils.isConnected()) {
                getGroups();
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_all_forum, container, false);


        noGroupsJoined = view.findViewById(R.id.no_joined_groups);
        initView(view);
        initSwipeRefresh();

        if (Utils.isConnected()) {
            getGroups();
        }


        return view;
    }

    private void initView(View view) {

        allForum = view.findViewById(R.id.allForum);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        llParent = view.findViewById(R.id.llParent);

    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected()) {
                getGroups();
            } else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(getActivity(), getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void getGroups() {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.FIND_GROUPS + query;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                //hud.dismiss();
                swipeContainer.setRefreshing(false);
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        GroupDetails groupDetails = new Gson().fromJson(response, GroupDetails.class);
                        if (groupDetails.getGroups()!=null) {
                            setGroupList(groupDetails.getGroups());
                        }
                    }

                } catch (Exception e){
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setGroupList(ArrayList<Group> groups) {

        allForum.setLayoutManager(new LinearLayoutManager(getContext()));
        allForum.setAdapter(new FindGroupAdapter(groups,this));
    }

    @Override
    public void onGroupClick(Group group) {
        startActivity(new Intent(getContext(), AfroViewGroupPostActivity.class).putExtra("group",group));

    }

    @Override
    public void onGroupJoinOrLeaveToggleClick(Group group, boolean isJoining) {
        if (isJoining)
            joinGroup(group.getGroupId());
        else
            leaveGroup(group.getGroupId());
    }

    private void joinGroup(Integer groupId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.JOIN_GROUP + groupId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e){
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    //leave group API call
    private void leaveGroup(Integer groupId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.LEAVE_GROUP + groupId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e){
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }
    
}