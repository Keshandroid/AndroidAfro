package com.TBI.afrocamgist.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.ViewEntertainmentImageActivity;
import com.TBI.afrocamgist.activity.ViewPostActivity;
import com.TBI.afrocamgist.adapters.EntertainmentAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.TBI.afrocamgist.fragment.AfroEntertainmentFragment.PaginationListener.PAGE_START;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroEntertainmentFragment extends Fragment implements EntertainmentAdapter.OnEntertainmentPostClickListener, ConnectivityListener {

    private KProgressHUD hud;
    private Connectivity mConnectivity;
    private RecyclerView entertainmentList;
    private SwipeRefreshLayout swipeContainer;
    private int nextPage = 1;
    private int count=-1;
    private boolean isLoading = false;
    private EntertainmentAdapter entertainmentAdapter;
    private ArrayList<Post> entertainmentPosts = new ArrayList<>();
    private ArrayList<String> entertainmentPostImages = new ArrayList<>();
    protected LocalBroadcastManager localBroadcastManager;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private int totalPage = 10;
    int itemCount = 0;

    public AfroEntertainmentFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_entertainment, container, false);

        initView(view);
        initSwipeRefresh();
        initInternetConnectivityCheckListener();

        /*if (Utils.isConnected())
                getFirstPageEntertainmentPosts(true);
            else {
                if (getActivity() != null)
                    Utils.showAlert(getActivity(), "Please check your internet connection");
            }*/

        return view;
    }

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
        localBroadcastManager.registerReceiver(mCountReceiver,
                new IntentFilter(ViewEntertainmentImageActivity.NITIFYSTATE));
        localBroadcastManager.registerReceiver(mDataReceiver,
                new IntentFilter(ViewEntertainmentImageActivity.NITIFYDATA));



        localBroadcastManager.registerReceiver(mLikeCommentReceiver,
                new IntentFilter(AfroSwaggerFragment.NOTIFY_LIKE_AND_COMMENT));


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
        localBroadcastManager.unregisterReceiver(mCountReceiver);
        localBroadcastManager.unregisterReceiver(mDataReceiver);
        localBroadcastManager.unregisterReceiver(mLikeCommentReceiver);
    }

    private void initView(View view) {
        entertainmentList = view.findViewById(R.id.entertainment_list);
        swipeContainer = view.findViewById(R.id.swipeContainer);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud!=null && hud.isShowing()){
                hud.dismiss();
            }
            Utils.closeAlert();
            getFirstPageEntertainmentPosts(true);
        } else {
            //Utils.showAlert(getActivity(), "No internet connection available.");
        }
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getFirstPageEntertainmentPosts(true);
            else {
                swipeContainer.setRefreshing(false);
                if (getActivity() != null)
                    Utils.showAlert(getActivity(), getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void getUpdate(){
        String url = UrlEndpoints.ENTERTAINMENT + "?page=1";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            swipeContainer.setRefreshing(false);
            if ("" .equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails entertainmentPostDetails = new Gson().fromJson(response, PostDetails.class);
                        nextPage = entertainmentPostDetails.getNextPage();
                        if (entertainmentPostDetails.getPosts().size() > 0) {
                            Toast.makeText(getContext(), "djfjks", Toast.LENGTH_SHORT).show();
                            getActivity().runOnUiThread(() -> entertainmentAdapter.notifyDataSetChanged());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));

                }
            }
        });
    }

    private void getFirstPageEntertainmentPosts(boolean showProgress) {

        if(showProgress){
            /*hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();*/
        }

        String url = UrlEndpoints.ENTERTAINMENT + "?page=1";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            swipeContainer.setRefreshing(false);
            if ("" .equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        if(showProgress){
                            //hud.dismiss();
                        }
                        PostDetails entertainmentPostDetails = new Gson().fromJson(response, PostDetails.class);
                        nextPage = entertainmentPostDetails.getNextPage();
                        if (entertainmentPostDetails.getPosts().size() > 0) {
                            entertainmentPosts = new ArrayList<>();
                            entertainmentPosts.addAll(entertainmentPostDetails.getPosts());
                            createEntertainmentPostImages();
                            setEntertainmentRecyclerView();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setEntertainmentRecyclerView() {
        entertainmentList.setLayoutManager(new GridLayoutManager(getActivity(),2));
        entertainmentAdapter = new EntertainmentAdapter(entertainmentPosts, this);
        entertainmentList.setAdapter(entertainmentAdapter);
        entertainmentList.setNestedScrollingEnabled(false);
        entertainmentList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;
                if (recyclerView.getLayoutManager() != null) {
                    visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }


                if (!isLoading && dy > 0 && ((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 10))) {
                    isLoading = true;
                    entertainmentPosts.add(null);
                    entertainmentAdapter.notifyItemInserted(entertainmentPosts.size() - 1);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            entertainmentPosts.remove(entertainmentPosts.size() - 1);
                            entertainmentAdapter.notifyItemRemoved(entertainmentPosts.size());
                            getHashtagNextPage();
                        }
                    }, 500);
                }
            }
        });
    }

    private void getHashtagNextPage() {

        if(count!=0) {
            String url = UrlEndpoints.ENTERTAINMENT + "?page=" + nextPage;
            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
                if (!"".equals(response)) {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                        } else {
                            PostDetails entertainmentPostDetails = new Gson().fromJson(response, PostDetails.class);
                            nextPage = entertainmentPostDetails.getNextPage();
                            count = entertainmentPostDetails.getCount();
                            entertainmentPosts.addAll(entertainmentPostDetails.getPosts());
                            entertainmentAdapter.notifyDataSetChanged();
                            createEntertainmentPostImages();
                            notifyData(entertainmentPostImages, entertainmentPosts);
                            //onDataUpdate.onData(entertainmentPostImages,entertainmentPostDetails.getPosts());
                            isLoading = false;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));

                    }
                }
            });
        }
    }

    private void createEntertainmentPostImages() {
        entertainmentPostImages = new ArrayList<>();
        for (Post entertainmentPost : entertainmentPosts) {
            if ("image" .equals(entertainmentPost.getPostType())) {
                if (entertainmentPost.getPostImage().size() != 0)
                    entertainmentPostImages.add(entertainmentPost.getPostImage().get(0));
            } else if ("video" .equals(entertainmentPost.getPostType())) {
                entertainmentPostImages.add(entertainmentPost.getThumbnail());
            }
        }
    }

    @Override
    public void onViewPostClicked(Post post) {
        startActivity(new Intent(getActivity(), ViewPostActivity.class).putExtra("postId", post.getPostId()));
    }

    @Override
    public void onPostImageClicked(int position) {
        startActivity(new Intent(getActivity(), ViewEntertainmentImageActivity.class)
                .putExtra("images", entertainmentPostImages)
                .putExtra("posts", entertainmentPosts)
                .putExtra("position", position));
    }

    public abstract class PaginationListener extends RecyclerView.OnScrollListener {
        public static final int PAGE_START = 1;
        @NonNull
        private GridLayoutManager layoutManager;
        /**
         * Set scrolling threshold here (for now i'm assuming 10 item in one page)
         */
        private static final int PAGE_SIZE = 10;
        /**
         * Supporting only LinearLayoutManager for now.
         * @param layoutManager
         */
        public PaginationListener(@NonNull GridLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = entertainmentPosts.size() - 4;
            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItems();
                }
            }
        }
        protected abstract void loadMoreItems();
        public abstract boolean isLastPage();
        public abstract boolean isLoading();
    }

        private BroadcastReceiver mCountReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String post_id = intent.getStringExtra(ViewEntertainmentImageActivity.Post_Id);
            int comment_count= Integer.parseInt(intent.getStringExtra(ViewEntertainmentImageActivity.Comment_Count));
            int like_count= Integer.parseInt(intent.getStringExtra(ViewEntertainmentImageActivity.Like_Count));
            Boolean islike= intent.getBooleanExtra(ViewEntertainmentImageActivity.IsLike,false);
            int position = getPosition(post_id);
            if (position!=-1) {
                entertainmentPosts.get(position).setLikeCount(like_count);
                entertainmentPosts.get(position).setCommentCount(comment_count);
                entertainmentPosts.get(position).setLiked(islike);
                entertainmentAdapter.notifyItemChanged(position);
            }
        }
    };

    private BroadcastReceiver mDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            getHashtagNextPage();
        }
    };

    private BroadcastReceiver mLikeCommentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (Utils.isConnected())
                getFirstPageEntertainmentPosts(false);
        }
    };

    protected void notifyData(ArrayList<String> imagelist, ArrayList<Post> posts){
        Intent intent = new Intent(ViewEntertainmentImageActivity.NITIFY_DATA_RECEIVE);
        intent.putExtra("image", imagelist)
                .putExtra("post", posts);
        localBroadcastManager.sendBroadcast(intent);
    }

    private int getPosition(String postid){
        int position=-1;
        for (int i = 0; i< entertainmentPosts.size();i++){
            if(postid.equals(String.valueOf(entertainmentPosts.get(i).getPostId()))){
                return i;
            }
        }
        return position;
    }



}
