package com.TBI.afrocamgist.fragment;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.TBI.afrocamgist.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroGroupRootFragment extends Fragment {


    public AfroGroupRootFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_group_root, container, false);

        FragmentTransaction transaction;
        if (getFragmentManager() != null) {
            transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.layout_afro_group, new AfroGroupFragment());
            transaction.commitAllowingStateLoss();
        }

        return view;
    }

}
