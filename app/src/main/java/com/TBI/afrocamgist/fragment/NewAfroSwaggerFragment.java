package com.TBI.afrocamgist.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.AESHelper;
import com.TBI.afrocamgist.GetPostsQuery;
import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.StoryConstants;
import com.TBI.afrocamgist.SwipeHelper;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.VideoPreLoadingService;
import com.TBI.afrocamgist.activity.CreatePostActivity;
import com.TBI.afrocamgist.activity.EditPostActivity;
import com.TBI.afrocamgist.activity.LoginActivity;
import com.TBI.afrocamgist.activity.MyProfileActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.SettingsActivity;
import com.TBI.afrocamgist.activity.SharePostActivity;
import com.TBI.afrocamgist.activity.ViewEntertainmentImageActivity;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Server;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.home.VideosAdapter;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.advertisement.Advertisement;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.media.Media;
import com.TBI.afrocamgist.model.media.MediaDetails;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.user.User;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.rx3.Rx3Apollo;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import im.ene.toro.widget.Container;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.socket.client.Ack;
import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnMultiCompressListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.TBI.afrocamgist.activity.CreatePostActivity.font_face1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.location1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postCategory1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postText1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postType1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.tagUserList1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.video1;
import static com.TBI.afrocamgist.app.AfrocamgistApplication.isPostUploading;

public class NewAfroSwaggerFragment extends Fragment implements VideosAdapter.OnSwaggerPostClickListener,SwaggerCommentBottomSheetFragment.DialogDismissListener {

    private KProgressHUD hud;
    private int nextPage = 1;
    private boolean isLoading = false;
    //ViewPager2 videoViewPager;
    private Container mainList;
    VideosAdapter videosAdapter;
    private SwipeRefreshLayout swipeContainer;
    private String postText = "", location = "", openCamera, postCategory = "", imageType="", font_face = "";
    //private ArrayList<TagToBeTagged> tagUserList = new ArrayList<>();

    private List<String> tagUserList = new ArrayList<>();

    private ArrayList<File> imageFiles = new ArrayList<>();
    private ArrayList<File> compressedImageFiles = new ArrayList<>();
    private File videoFile;

    private int lastVideoPlayPosition=-1;
    private SimpleExoPlayer exoPlayer;
    private Post post;
    private static int SAVED_SCROLL_POSITION = -1;
    protected boolean mIsVisibleToUser;
    private String openFromSwipe = "";
    private static String mVideoUrl;

    //Download Video from url
    String fileN = null;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1234;
    boolean result;
    ProgressDialog mProgressDialog;
    private String waterMarkVideo;
    private Dialog popup;
    private String shareOrDownload = "";

    private ArrayList<Post> swaggerPosts = new ArrayList<>();
    SnapHelper snapHelper;
    private int position;

    //post upload progressbar
    private int progress;
    RoundedHorizontalProgressBar uploadProgress;
    private TextView progress_title;
    private String progressTitle = "Posting...";
    private CardView cardProgressbar;
    protected LocalBroadcastManager localBroadcastManager;
    private ProgressBar progress_bar;

    public NewAfroSwaggerFragment() {
        // Required empty public constructor
    }

    NewAfroSwaggerFragment(String openCamera) {
        this.openCamera = openCamera;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_afro_swagger_new, container, false);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (openCamera != null) {
            startActivityForResult(new Intent(getActivity(), CreatePostActivity.class)
                    .putExtra("openCamera", "openCamera")
                    .putExtra("postFor", "afroswagger"), 100);
            openCamera = null;
        }

        initView(view);
        initSwipeRefresh();

        localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());


        localBroadcastManager.registerReceiver(mCreateImagePostDataReceiver,
                new IntentFilter(AfroCreatePostFragment.CREATE_IMAGE_POST_DATA));

        localBroadcastManager.registerReceiver(mCreateVideoPostDataReceiver,
                new IntentFilter(AfroCreatePostFragment.CREATE_VIDEO_POST_DATA));

        localBroadcastManager.registerReceiver(mRefreshSwaggerPostsReceiver,
                new IntentFilter(AfroCreatePostFragment.REFRESH_SWAGGER_POSTS));

        localBroadcastManager.registerReceiver(mFollowStateReceiver,
                new IntentFilter(ProfileActivity.NITIFY_FOLLOW_STATE));

        localBroadcastManager.registerReceiver(mCountReceiver,
                new IntentFilter(ViewEntertainmentImageActivity.NITIFYSTATE));

//        localBroadcastManager.registerReceiver(mLikeSwaggerTalent,
//                new IntentFilter(ViewEntertainmentImageActivity.NOTIFY_LIKE_SWAGGER_TALENT));

//        localBroadcastManager.registerReceiver(mStorylineFollowStateReceiver,
//                new IntentFilter(MainStoryPostActivity.NOTIFY_FOLLOW_STATE_STORYLINE));
//
//        localBroadcastManager.registerReceiver(mNotifyMainStoryList,
//                new IntentFilter(MainStoryPostActivity.NOTIFY_MAIN_STORY_LIST));


        if (Utils.isConnected()) {
            getSwaggerFirstPagePosts(true);
            getAdvertisements();
        }else {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
            //Utils.showAlert(getActivity(), "No internet connection available.");
        }

    }

    public void refreshSwagger(){
        if (Utils.isConnected()) {
            getSwaggerFirstPagePosts(true);
            getAdvertisements();
        }
    }

    private void initView(View view) {

        //videoViewPager = view.findViewById(R.id.videosViewPager);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        mainList = view.findViewById(R.id.main_list);

        uploadProgress = view.findViewById(R.id.upload_progress);
        progress_title = view.findViewById(R.id.progress_title);
        cardProgressbar = view.findViewById(R.id.cardProgressbar);

        progress_bar = view.findViewById(R.id.progress_bar);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initSwipeRefresh() {
        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected()) {
                //setStoryRecyclerview();
                getSwaggerFirstPagePosts(false);
                getAdvertisements();
            } else {
                if (swipeContainer != null) {
                    swipeContainer.setRefreshing(false);
                }
                Utils.showAlert(getActivity(), getString(R.string.no_internet_connection_available));
            }
        });
    }

    private BroadcastReceiver mFollowStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int user_id = intent.getIntExtra(ProfileActivity.User_Id,0);
            boolean isfollow = intent.getBooleanExtra(ProfileActivity.IsFollow,false);
            Logger.d("isFollow","called..12");
            setFollowAllUser(user_id,isfollow,"");
        }
    };

    private BroadcastReceiver mCreateImagePostDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getSerializableExtra("postType") != null) {
                if (swaggerPosts != null) {
                    //swaggerPosts.add(2, null); //important
                    //videosAdapter.notifyDataSetChanged();

                    cardProgressbar.setVisibility(View.VISIBLE);
                    showPostingView();
                }
                Logger.d("POSTING111","loading...2");

                postText = intent.getStringExtra("postText");
                location = intent.getStringExtra("location");
                tagUserList = (ArrayList<String>) intent.getSerializableExtra("tagUserList");
                postCategory = intent.getStringExtra("postCategory");
                imageType = intent.getStringExtra("imageType");
                font_face = intent.getStringExtra("font_face");
                createPost(intent);
            }

        }
    };

    private BroadcastReceiver mCreateVideoPostDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getSerializableExtra("postType") != null) {
                if (swaggerPosts != null) {
                    cardProgressbar.setVisibility(View.VISIBLE);
                    showPostingView();
                }
                Logger.d("POSTING111","loading...2");

                postText = intent.getStringExtra("postText");
                location = intent.getStringExtra("location");
                tagUserList = (ArrayList<String>) intent.getSerializableExtra("tagUserList");
                postCategory = intent.getStringExtra("postCategory");
                imageType = intent.getStringExtra("imageType");
                font_face = intent.getStringExtra("font_face");
                createPost(intent);
            }

        }
    };

    private BroadcastReceiver mRefreshSwaggerPostsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getSwaggerFirstPagePosts(true);
            getAdvertisements();
        }
    };


    /*private BroadcastReceiver mNotifyMainStoryList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (Utils.isConnected())
                getStoryList();
        }
    };*/


   /* private BroadcastReceiver mLikeSwaggerTalent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if (Utils.isConnected())
                getSwaggerFirstPagePosts(false);
        }
    };*/

    private BroadcastReceiver mCountReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String post_id = intent.getStringExtra(ViewEntertainmentImageActivity.Post_Id);
            int comment_count= Integer.parseInt(intent.getStringExtra(ViewEntertainmentImageActivity.Comment_Count));
            int like_count= Integer.parseInt(intent.getStringExtra(ViewEntertainmentImageActivity.Like_Count));
            Boolean islike= intent.getBooleanExtra(ViewEntertainmentImageActivity.IsLike,false);
            int position = getPosition(post_id);
            if (position!=-1) {
                swaggerPosts.get(position).setLikeCount(like_count);
                swaggerPosts.get(position).setCommentCount(comment_count);
                swaggerPosts.get(position).setLiked(islike);
                videosAdapter.notifyItemChanged(position);
            }
        }
    };

    private int getPosition(String postid){
        int position=-1;
        for (int i = 0; i< swaggerPosts.size();i++){
            if(postid.equals(String.valueOf(swaggerPosts.get(i).getPostId()))){
                return i;
            }
        }
        return position;
    }

    /*private BroadcastReceiver mStorylineFollowStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int user_id = intent.getIntExtra(MainStoryPostActivity.User_Id,0);
            boolean isfollow = intent.getBooleanExtra(MainStoryPostActivity.IsFollow,false);
            setStorylineFollowAllUser(user_id,isfollow);
        }
    };*/

    private void getSwaggerFirstPagePosts(boolean needToLoader) {

        //Enable back button here
        isPostUploading = false;


        // <<<<<<<<<--------------- use this to show loader in swagger screen --------------->>>>>>>>>>
        if(needToLoader) {
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();
        }

        String url = UrlEndpoints.AFROSWAGGER_POST + "?page=1";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Logger.d("LOGIN_TOKEN",""+LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {

            // <<<<<<<<<--------------- use this to show loader in swagger screen --------------->>>>>>>>>>
            if(needToLoader)
                hud.dismiss();

            if(!Identity.getDialogGuidePreference(getContext())){
                showGuideVideo(getActivity());
            }

            if ("".equals(response)) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
            } else {
                try {

                    //new swagger response
                    /*JSONObject object = new JSONObject(response);
                    Logger.e("LLLLLL_Res: ",response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    }else {
                        if(object.getString(Constants.DATA) != null){
                            if(object.getString(Constants.NEXT_PAGE) != null){
                                nextPage = object.getInt(Constants.NEXT_PAGE);
                            }
                            String encryptedUserData = object.getString(Constants.DATA);
                            decryptSwaggerData(encryptedUserData);
                        }
                    }*/



                    //old swagger response
                    Logger.e("LLLLLL_SWAGRes: ", response);
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroSwagger = new Gson().fromJson(response, PostDetails.class);
                        if (afroSwagger.getPosts().size() > 0) {
                            nextPage = afroSwagger.getNextPage();
                            startPreLoadingService(afroSwagger.getPosts());
                            swaggerPosts = new ArrayList<>();
                            swaggerPosts.addAll(afroSwagger.getPosts());

                            setVideosAdapter();
                            setRecyclerView();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void decryptSwaggerData(String encryptedUserData){
        AESHelper aesHelper = new AESHelper();
        String decryptedData = aesHelper.decryptData(encryptedUserData);

        try {
           // JSONArray array = new JSONArray(decryptedData);
            parseJSON(decryptedData);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("My App", "Could not parse malformed JSON: \"" + decryptedData + "\"");
        }

    }

    private void parseJSON(String decryptedData) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Post>>(){}.getType();
        ArrayList<Post> afroSwagger = gson.fromJson(decryptedData, type);

        if (afroSwagger.size() > 0) {

            startPreLoadingService(afroSwagger);
            swaggerPosts = new ArrayList<>();
            swaggerPosts.addAll(afroSwagger);

            setVideosAdapter();
            setRecyclerView();
        }


    }

    private void showGuideVideo(Activity activity) {

        if (activity!=null && !activity.isFinishing()) {
            popup = new Dialog(activity, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_guide_user);
            popup.setCancelable(false);
            popup.show();


            TextView okButton = popup.findViewById(R.id.ok);

            VideoView videoView = popup.findViewById(R.id.videoGuide);
            String path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.slow_guide_video;
            videoView.setVideoURI(Uri.parse(path));
            videoView.start();

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                }
            });

            /*videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    popup.dismiss();
                    Identity.setDialogGuidePreference(activity,true);
                }
            });*/

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();
                    Identity.setDialogGuidePreference(activity,true);

                }
            });

        }

    }

    private void getAdvertisements() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.ADS, response -> {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }

            Log.d("responseAdvert",""+response.toString());

            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    Object json = new JSONTokener(response).nextValue();

                    if (json instanceof JSONObject) {
                        JSONObject object = new JSONObject(response);
                    } else {
                        Type listType = new TypeToken<ArrayList<Advertisement>>() {
                        }.getType();
                        ArrayList<Advertisement> advertisements = new Gson().fromJson(response, listType);

                        if (advertisements.size() > 0) {
                            if (videosAdapter != null && advertisements != null) {
                                videosAdapter.setAdvertisementList(advertisements);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    private void getSwaggerNextPagePosts() {

        progress_bar.setVisibility(View.VISIBLE);

        String url = UrlEndpoints.AFROSWAGGER_POST + "?page=" + nextPage;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            isLoading = false;
            if (!"".equals(response)) {

                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroSwagger = new Gson().fromJson(response, PostDetails.class);

                        if (afroSwagger.getPosts() != null && afroSwagger.getPosts().size() > 0) {
                            nextPage = afroSwagger.getNextPage();
                            startPreLoadingService(afroSwagger.getPosts());
                            swaggerPosts.addAll(afroSwagger.getPosts());

                            videosAdapter.notifyDataSetChanged();
                            progress_bar.setVisibility(View.GONE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /*private void setViewPager() {
        Logger.d("swaggerSize",""+swaggerPosts.size());
        videosAdapter = new VideosAdapter(getActivity(),swaggerPosts,this);
        videoViewPager.setAdapter(videosAdapter);

        videoViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);

                Logger.d("ALL_POST_SIZE","=="+videosAdapter.getItemCount());

                if (position == videosAdapter.getItemCount() - 3) {
                    Logger.d("PAGE_POSITION","=="+position);
                    if(!isLoading){
                        Logger.d("NEXT_PAGE","=="+nextPage);
                        isLoading = true;
                        getSwaggerNextPagePosts();
                    }
                }


                Logger.d("PAGE_SELECTED","=="+position);
                Logger.d("PAGE_SELECTED","==POSITION_PIXEL=="+positionOffsetPixels);
                Logger.d("PAGE_SELECTED","==POSITION_OFFSET=="+positionOffset);

            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                try {
                    videosAdapter.loadItemData(position);
                }catch (Exception e){
                    e.printStackTrace();
                }

                if(post.getPostType().equalsIgnoreCase("video")){
                    if(exoPlayer!=null){
                        startPlayer(exoPlayer);
                    }
                }else {
                    if(exoPlayer!=null){
                        pausePlayer(exoPlayer);
                    }
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);

                Logger.d("itemScrolled","yes.....");

            }
        });
    }*/

    private void setVideosAdapter(){
        ViewCompat.setNestedScrollingEnabled(mainList, false);
        mainList.setNestedScrollingEnabled(false);
        mainList.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mainList.setItemAnimator(new DefaultItemAnimator());
        videosAdapter = new VideosAdapter(getActivity(),swaggerPosts,this);
        mainList.setAdapter(videosAdapter);

        snapHelper = new PagerSnapHelper();
        mainList.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(mainList);



       /* mainList.addOnItemTouchListener(new MyTouchListener(getActivity(),
                mainList,
                new MyTouchListener.OnTouchActionListener() {
                    @Override
                    public void onLeftSwipe(View view, int position) {

                        Log.d("position99 left ::: ", ""+position);

                        if (LocalStorage.getUserDetails().getUserId().equals(swaggerPosts.get(position).getUserId()))
                            startActivityForResult(new Intent(getActivity(), MyProfileActivity.class)
                                    .putExtra("fromSwipe","fromSwipe"),99);
                        else
                            startActivityForResult(new Intent(getActivity(), ProfileActivity.class)
                                    .putExtra("fromSwipe","fromSwipe")
                                    .putExtra("userId", swaggerPosts.get(position).getUserId()),99);
                    }

                    @Override
                    public void onRightSwipe(View view, int position) {
                        Log.d("position99 right ::: ", ""+position);
                    }

                    @Override
                    public void onClick(View view, int position) {
                        Log.d("position99 click ::: ", ""+position);
                    }

                }));*/





        ItemTouchHelper.Callback callback=new SwipeHelper(videosAdapter, getContext());
        ItemTouchHelper helper=new ItemTouchHelper(callback);
        helper.attachToRecyclerView(mainList);
    }



    private void setRecyclerView() {
        mainList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE) {
                    View centerView = snapHelper.findSnapView(recyclerView.getLayoutManager());
                    position = recyclerView.getLayoutManager().getPosition(centerView);
                    Logger.e("Snapped Item Position:",""+position);
                    if ((position +3) <= swaggerPosts.size()-1) {
                        Logger.e("LLLLL_Left: ",""+position);
                        if (mainList.getLayoutManager() != null) {
                            //setLikesAndCommentsData(position); // modified
                        }
                    }else{
                        if (!isLoading) {
                            isLoading = true;
                            Logger.e("LLLLL_Left: ","Else :"+position);
                            //getHashtagNextPage();
                            getSwaggerNextPagePosts();
                        }
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });



    }

    private void startPreLoadingService(ArrayList<Post> posts) {

        ArrayList<String> videoList = new ArrayList<>();

        for (int i=0;i<posts.size();i++){
            if(posts.get(i).getPostType().equalsIgnoreCase("video")){
                String url = UrlEndpoints.MEDIA_BASE_URL + posts.get(i).getPostVideo();
                videoList.add(url);
            }

            if(posts.get(i).getPostType().equalsIgnoreCase("shared")){
                if(posts.get(i).getSharedPost() != null){
                    if(posts.get(i).getSharedPost().getPostType().equalsIgnoreCase("video")){
                        String url = UrlEndpoints.MEDIA_BASE_URL + posts.get(i).getSharedPost().getPostVideo();
                        videoList.add(url);
                    }
                }
            }

        }

        Logger.d("videoUrls",new GsonBuilder().setPrettyPrinting().create().toJson(videoList));

        Intent preloadingServiceIntent = new Intent(getActivity(), VideoPreLoadingService.class);
        preloadingServiceIntent.putStringArrayListExtra(StoryConstants.VIDEO_LIST, videoList);
        getActivity().startService(preloadingServiceIntent);
    }

    @Override
    public void onPlayItem(int adapterPosition) {
        Logger.d("playerStateFrom99: ", "lastVideoPlayPosition = "+adapterPosition);
        lastVideoPlayPosition = adapterPosition;
    }

    @Override
    public void onPauseVideo(Post post, SimpleExoPlayer simpleExoPlayer) {
        /*if(videosAdapter!=null){
            this.exoPlayer = simpleExoPlayer;
            this.post = post;
        }*/
    }

    @Override
    public void onPostChecked(Post post,int position, boolean isDoubleClicked) {
        likeAndUnlikePost(post.getPostId(),position,post,isDoubleClicked);
    }

    private void likeAndUnlikePost(Integer postId, int position, Post post, boolean isDoubleClicked) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id", postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    }else {

                        /*if(isDoubleClicked){
                            if(post.getLiked()){
                                if(post.getLikeCount()>0){
                                    swaggerPosts.get(position).setLiked(false);
                                    swaggerPosts.get(position).setLikeCount(post.getLikeCount() - 1);
                                }
                            }else {
                                swaggerPosts.get(position).setLiked(true);
                                swaggerPosts.get(position).setLikeCount(post.getLikeCount() + 1);
                            }
                            videosAdapter.loadItemData(position);
                        }else {
                            videosAdapter.loadItemData(position);
                        }*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }

    @Override
    public void onFollowClicked(Post post) {
        followUser(post.getUserId(),post);
        setFollowAllUser(post.getUserId(),true,post.getId());
    }

    private void followUser(Integer userId, Post post) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {

            }
        });
    }

    private void setFollowAllUser(int userid, boolean isfollow, String id){

        Logger.d("Broadcast:22",""+userid);
        Logger.d("Broadcast:22",""+isfollow);

        try {
            for (int i = 0; i< swaggerPosts.size();i++){
                if(swaggerPosts.get(i).getUserId() !=null && userid == swaggerPosts.get(i).getUserId()){
                    /*swaggerPosts.get(i).setFollowing(isfollow);
                    swaggerPostAdapter.notifyItemChanged(i);*/


                    //edited
                    if(swaggerPosts.get(i).getPrivateAccount()){

                        if(isfollow){
                            swaggerPosts.get(i).getRequestButtons().get(0).setButtonText("Requested");
                            swaggerPosts.get(i).setFollowing(false);
                        }else {
                            swaggerPosts.get(i).getRequestButtons().get(0).setButtonText("Request");
                            swaggerPosts.get(i).setFollowing(false);
                        }

                    }else {
                        swaggerPosts.get(i).setFollowing(isfollow);
                    }
                    //edited

                    //videosAdapter.notifyItemChanged(i);

                    if(!id.equalsIgnoreCase(swaggerPosts.get(i).getId())){
                        Logger.d("notifyData1","called..");
                        videosAdapter.notifyItemChanged(i);
                    }

                }
            }
            //swaggerPostAdapter.notifyDataSetChanged();

        }catch (Exception e){
        }

    }

    @Override
    public void onCommentClicked(Post post, int position, VideosAdapter.VideoViewHolder holder) {

        SAVED_SCROLL_POSITION = position;
        SwaggerCommentBottomSheetFragment swaggerCommentBottomSheetFragment = new SwaggerCommentBottomSheetFragment();
        SwaggerCommentBottomSheetFragment.newInstance(holder, post.getComments(),post.getPostId(),this).show(getChildFragmentManager(), swaggerCommentBottomSheetFragment.getTag());
    }

    @Override
    public void onDialogDismiss(ArrayList<Comment> comments, VideosAdapter.VideoViewHolder holder) {
        if(comments!=null){
            updateCommentCount(comments,holder);
            //notifyMainStoryList();
        }
    }

    private void updateCommentCount(ArrayList<Comment> comments,VideosAdapter.VideoViewHolder holder) {

        if (swaggerPosts.size() > 0) {
            int commentCount = 0;
            for (Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments() != null && comment.getSubComments().size() > 0) {
                    commentCount += comment.getSubComments().size();
                }
            }
            swaggerPosts.get(SAVED_SCROLL_POSITION).setComments(comments);
            swaggerPosts.get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);


            /*if (videosAdapter != null) {
                videosAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }*/

            if(videosAdapter!=null){
                videosAdapter.onCommentDialogDismiss(SAVED_SCROLL_POSITION,commentCount,holder);
            }

        }
    }

    @Override
    public void onReportClicked(Integer userId) {
        showReportUserDialog(userId);
    }

    private void showReportUserDialog(Integer userId) {

        Dialog popup = new Dialog(getContext(), R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_deactivate);
        popup.setCancelable(true);
        popup.show();

        TextView message = popup.findViewById(R.id.message);
        message.setTextColor(getResources().getColor(R.color.blue));

        String msg = "Are you sure you want to Report this user ?";
        message.setText(msg);

        TextView confirm = popup.findViewById(R.id.okay);
        confirm.setTextColor(getResources().getColor(R.color.blue));
        confirm.setText("Ok");

        TextView discard = popup.findViewById(R.id.cancel);
        discard.setText("Cancel");


        popup.findViewById(R.id.okay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                onResume();
                callReport(userId);

            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                onResume();

            }
        });


        popup.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                onResume();
            }
        });

    }

    private void callReport(Integer userId){

        JSONObject request = new JSONObject();
        try {
            request.put("report_reason","");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.CHAT_REPORT + userId + "/report";

        Webservices.getData(Webservices.Method.POST,request, headers, url, response -> {
            onResume();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if(object.has(Constants.MESSAGE)){
                        Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                    }else {
                        Toast.makeText(getActivity(),getString(R.string.user_reported_successfully),Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    public void onProfileClicked(Integer userId) {
        if (LocalStorage.getUserDetails().getUserId().equals(userId))
            startActivity(new Intent(getContext(), MyProfileActivity.class));
        else
            startActivity(new Intent(getContext(), ProfileActivity.class)
                    .putExtra("userId", userId));
    }

    @Override
    public void onMentionedClicked(String username) {

        Logger.d("onMentionedClicked",""+username);

        getUserIdFromUsername(username);
    }

    private void getUserIdFromUsername(String username){
        /*OkHttpClient client = new OkHttpClient().newBuilder().build();
        Request request = new Request.Builder()
                .url("https://manager.staging.afrocamgist.com/api/users/userbyusername?user_name=" + username)
                .method("GET", null)
                .build();
        try {
            okhttp3.Response response = client.newCall(request).execute();
            Logger.d("usernameResponse", new GsonBuilder().setPrettyPrinting().create().toJson(response));
        } catch (IOException e) {
            e.printStackTrace();
        }*/


        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getUserIdByUsername(username);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        //Logger.d("ViewCount",""+jsonObject.get("counter"));
                        Logger.d("usernameResponse", new GsonBuilder().setPrettyPrinting().create().toJson(jsonObject));

                        JsonElement element = jsonObject.get("data");

                        if (!(element instanceof JsonNull)) {
                            JsonObject data = jsonObject.getAsJsonObject("data");
                            String id = String.valueOf(data.get("user_id"));
                            int userID=Integer.parseInt(id);
                            Logger.d("usernameResponse",""+userID);

                            if (LocalStorage.getUserDetails().getUserId().equals(userID))
                                startActivity(new Intent(getContext(), MyProfileActivity.class));
                            else
                                startActivity(new Intent(getContext(), ProfileActivity.class)
                                        .putExtra("userId", userID));
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    /*private void getUserIdFromUsername(String username) {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.USER_BY_USERNAME + "?user_name=" + username;

        *//*JSONObject params = new JSONObject();
        try {
            params.put("user_name",username);
        } catch (JSONException e) {
            e.printStackTrace();
        }*//*

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    Logger.d("usernameResponse", new GsonBuilder().setPrettyPrinting().create().toJson(object));

                } catch (Exception e) {
                    //Utils.showAlert(ProfileActivity.this, "Oops something went wrong....");
                    e.printStackTrace();
                }
            }
        });
    }*/

    @Override
    public void onPostView(Integer postId) {
        callPostView(postId);
    }

    private void callPostView(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getPostView(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("onPostView",""+jsonObject.toString());
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onVideoCount(Integer postId) {
        callVideoCountNew(postId);
    }

    private void callVideoCountNew(int videoPostId){
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getVideoCount(videoPostId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("ViewCount",""+jsonObject.get("counter"));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void openDetailScreen(RecyclerView.ViewHolder viewHolder) {
        if(viewHolder!=null){
            if (LocalStorage.getUserDetails().getUserId().equals(swaggerPosts.get(viewHolder.getAdapterPosition()).getUserId()))
                startActivityForResult(new Intent(getActivity(), MyProfileActivity.class)
                        .putExtra("fromSwipe","fromSwipe"),99);
            else
                startActivityForResult(new Intent(getActivity(), ProfileActivity.class)
                        .putExtra("fromSwipe","fromSwipe")
                        .putExtra("userId", swaggerPosts.get(viewHolder.getAdapterPosition()).getUserId()),99);
        }
    }

    @Override
    public void onPostViewCount(Integer postId) {
        callPostViewCount(postId);
    }

    private void callPostViewCount(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getPostViewCount(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onCreatePostClicked() {
        startActivityForResult(new Intent(getActivity(), CreatePostActivity.class).putExtra("postFor", "afroswagger"), 100);
    }

    private void createPost(Intent data) {

        if (data != null) {
            CreatePostActivity.PostType postType = (CreatePostActivity.PostType) data.getSerializableExtra("postType");
            switch (postType) {
                case IMAGE:
                    createImagePost(data);
                    break;
                case VIDEO:
                    createVideoPost(data);
                    break;
                default:
                    break;
            }
        } else {
            if (postType1 == CreatePostActivity.PostType.VIDEO) {
                createVideoPost((Intent) null);
            }
        }
    }

    private void createImagePost(Intent data) {
        createImageFile(data.getStringArrayListExtra("images"));
//        uploadImages();
    }

    private void createImageFile(ArrayList<String> imagePaths) {

        imageFiles = new ArrayList<>();

        for (int i = 0; i < imagePaths.size(); i++) {

            try {
                imageFiles.add(new File(imagePaths.get(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (getActivity() != null) {
            Luban.compress(getActivity(), imageFiles)
                    .putGear(Luban.CUSTOM_GEAR)
                    .launch(new OnMultiCompressListener() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onSuccess(List<File> fileList) {
                            compressedImageFiles = new ArrayList<>(fileList);
                            for (File image : imageFiles)
                                image.delete();
                            uploadImages();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Logger.d("Percentage", e.getMessage() + "");
                        }
                    });
        }
    }

    private void uploadImages() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (File photo : compressedImageFiles) {
            builder.addFormDataPart(Constants.IMAGES, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload";

        Webservices.getData(getActivity(), Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    Logger.d("uploadResponse", object.toString());

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {

                            for (File image : compressedImageFiles)
                                image.delete();


                            Logger.d("post_category",postCategory);
                            if(postCategory.equalsIgnoreCase("normal_post")){
                                createImagePost(mediaDetails.getMediaList());
                            }else {
                                createStoryImagePost(mediaDetails.getMediaList());
                            }
                        } else {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Logger.d("exception1",""+ Log.getStackTraceString(e));
                    Logger.d("exception1",""+e.getLocalizedMessage());
                    //Utils.showAlert(getActivity(), response);
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int) (100 * bytesRead / contentLength);
            Logger.d("Percentage", percentage + "");

            isPostUploading = true;

            //if (swaggerPostAdapter != null) { // important
                setUploadProgress(percentage);
                if (getActivity() != null)
                    getActivity().runOnUiThread(this::showPostingView); //important
            //}
        });
    }

    public void setUploadProgress(Integer progress) {
        this.progress = progress;
    }

    private Integer getUploadProgress() {
        return this.progress;
    }

    private String getProgressTitle() {
        return this.progressTitle;
    }

    public void setProgressTitle(String progressTitle) {
        this.progressTitle = progressTitle;
    }

    private void showPostingView() {
        if (getUploadProgress() != -1) {
            uploadProgress.setProgress(getUploadProgress());
            progress_title.setText(getProgressTitle());
            if (uploadProgress.getProgress() == 100) {
                uploadProgress.setVisibility(View.GONE);
                progress_title.setText("Finishing...");
            }
        } else {
            uploadProgress.setVisibility(View.GONE);
            progress_title.setText("Finishing...");
        }
    }

    private void createImagePost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createImagePostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(getActivity(), getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    if (swaggerPosts != null) {
                        //swaggerPosts.remove(1); // important
                        cardProgressbar.setVisibility(View.GONE);
                        videosAdapter.notifyDataSetChanged();
                    }
                    getSwaggerFirstPagePosts(true);
                    getAdvertisements();
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    private void createStoryImagePost(ArrayList<Media> mediaList){

        JsonObject requestObject = createStoryImagePostJsonRequest(mediaList);

        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getStorylineUsers(requestObject);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {

                JsonObject jsonObject = response.body();

                Logger.d("jsonObject", new GsonBuilder().setPrettyPrinting().create().toJson(jsonObject));
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        if (jsonObject.has(Constants.MESSAGE)) {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        } else {
                            Toast.makeText(getActivity(), "Story Added successfully", Toast.LENGTH_LONG).show();
                            if (swaggerPosts != null) {
                                //swaggerPosts.remove(1); //important
                                cardProgressbar.setVisibility(View.GONE);
                                videosAdapter.notifyDataSetChanged();
                            }
                            getSwaggerFirstPagePosts(true);
                            getAdvertisements();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    private JSONObject createImagePostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            request.put("post_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "afroswagger");
            request.put("font_face",font_face);

            if(!imageType.equalsIgnoreCase("")){
                request.put("image_size",imageType);
                Logger.d("imageTypeParam",""+imageType);
            }

            JSONArray images = new JSONArray();
            for (Media media : mediaList) {
                images.put(media.getPath());
            }

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("post_type", "image");
            request.put("post_image", images);

            Logger.d("imagelistRequest", new GsonBuilder().setPrettyPrinting().create().toJson(request));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private JsonObject createStoryImagePostJsonRequest(ArrayList<Media> mediaList) {

        JsonObject request = new JsonObject();
        try {

            request.addProperty("story_text", postText);
            request.addProperty("post_lat_long", location);
            request.addProperty("posted_for", "afroswagger");

            JsonArray images = new JsonArray();
            for (Media media : mediaList) {
                images.add(media.getPath());
            }

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JsonArray taggedUsers = new JsonArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.add(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.add("tagged_id",taggedUsers);
            }

            request.addProperty("story_type", "image");
            request.add("story_image", images);

            Logger.d("StoryImagelistRequest", new GsonBuilder().setPrettyPrinting().create().toJson(request));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void createVideoPost(Intent data) {
        if (data == null) {
            Logger.d("DataNull","111");
            createVideoFile(video1);
        } else {
            Logger.d("DataNull","222");
            createVideoFile(data.getStringExtra("video"));
        }
//        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/Video");
//        if (!wallpaperDirectory.exists())
//            wallpaperDirectory.mkdir();
//        File f = new File(wallpaperDirectory, "VID_" + Calendar.getInstance().getTimeInMillis() + ".mp4");
        uploadVideo();
    }


    private void createVideoFile(String videoPath) {
        try {
            videoFile = new File(videoPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadVideo() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, videoFile.getName(), RequestBody.create(MediaType.parse("video/*"), videoFile));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload-video";
        Logger.e("LLLL_Path: ", videoFile.getAbsolutePath());
        Webservices.getData(getActivity(), Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    Logger.e("LLLLLL_Swagger_Res: ", response);
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {
                            if (videoFile != null)
                                videoFile.delete();
                            //if (videosAdapter != null) {
                                setUploadProgress(-1);
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(this::showPostingView);
                            //}

                            if(postCategory.equalsIgnoreCase("normal_post")){
                                createVideoPost(mediaDetails.getMediaList());
                            }else {
                                createStoryVideoPost(mediaDetails.getMediaList());
                            }
                        } else {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        }, (bytesRead, contentLength) -> {
            Logger.e("LLLL_Byte: ", bytesRead + "     " + contentLength);
            int percentage = (int) ((100 * bytesRead) / contentLength);

            //Disable Back Button here because post is uploading
            isPostUploading = true;

            Logger.e("LLLL_Percentage", percentage + "");
            //if (swaggerPostAdapter != null) {
                setUploadProgress(percentage);
                if (getActivity() != null)
                    getActivity().runOnUiThread(this::showPostingView);
            //}
        });
    }

    private void createVideoPost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createVideoPostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(getActivity(), getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    Logger.e("LLLL_Res2_frag: ", response);
                    if (swaggerPosts != null) {

                        cardProgressbar.setVisibility(View.GONE);

                        postText1 = "";
                        location1 = "";
                        video1 = "";
                        if(tagUserList1 != null){
                            tagUserList1.clear();
                        }
                        postCategory1 = "";
                        font_face1 = "";

                        getSwaggerFirstPagePosts(true);
                        videosAdapter.notifyDataSetChanged();
                    }

                    getAdvertisements();
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    private void createStoryVideoPost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createStoryVideoPostJsonRequest(mediaList), headers, UrlEndpoints.STORYLINE, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(getActivity(), "Story Added successfully", Toast.LENGTH_LONG).show();
                    Logger.e("LLLL_Res2_frag: ", response);
                    if (swaggerPosts != null) {
                        //swaggerPosts.remove(1);

                        cardProgressbar.setVisibility(View.GONE);

                        postText1 = "";
                        location1 = "";
                        video1 = "";
                        if(tagUserList1 != null){
                            tagUserList1.clear();
                        }
                        postCategory1 = "";
                        font_face1 = "";

                        getSwaggerFirstPagePosts(true);
                        videosAdapter.notifyDataSetChanged();
                    }

                    getAdvertisements();
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });

    }

    private JSONObject createVideoPostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("post_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "afroswagger");
            request.put("font_face",font_face);
            request.put("post_type", "video");
            request.put("post_video", mediaList.get(0).getPath());
            request.put("thumbnail", mediaList.get(0).getThumbnails().get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private JSONObject createStoryVideoPostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("story_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "afroswagger");
            request.put("story_type", "video");
            request.put("story_video", mediaList.get(0).getPath());
            request.put("thumbnail", mediaList.get(0).getThumbnails().get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    @Override
    public void onShareClicked(Post post, Activity context) {
        if(context!=null){
            startActivityForResult(new Intent(context, SharePostActivity.class).putExtra("post", post), 100);
        }
    }

    @Override
    public void onDownloadClick(String videoUrl, String postLink) {
        mVideoUrl = videoUrl;
        showShareSelectionDialog(getContext(),videoUrl,postLink);
    }

    @Override
    public void onEditPost(Post post, int position) {
        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(getActivity(), EditPostActivity.class)
                .putExtra("post", post), 102);
    }

    @Override
    public void onDeletePost(Post post) {
        showConfirmDeletePostAlert(post);
    }

    @Override
    public void onHidePost(Post post) {
        showConfirmHidePostAlert(post);
    }

    private void showConfirmHidePostAlert(Post post) {

        Dialog popup = new Dialog(getActivity(), R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        TextView message = popup.findViewById(R.id.message);
        TextView delete = popup.findViewById(R.id.delete);

        delete.setText("Hide");
        message.setText("Are you sure you want to hide the post ?");

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    hidePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void hidePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId + UrlEndpoints.HIDE;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {
                    Logger.e("LLLLLL_HideRes: ", response);
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.str_post_hide), Toast.LENGTH_LONG).show();
                        getSwaggerFirstPagePosts(true);
                        getAdvertisements();
                    }

                } catch (Exception e) {
//                    Logger.e("LLLLLL_Hide: ",e.getMessage());
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                    e.printStackTrace();
                }
            }
        });
    }

    private void showConfirmDeletePostAlert(Post post) {

        Dialog popup = new Dialog(getActivity(), R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    deletePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void deletePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.post_deleted_successfully), Toast.LENGTH_LONG).show();
                        getSwaggerFirstPagePosts(true);
                        getAdvertisements();
                    }

                } catch (Exception e) {
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                    e.printStackTrace();
                }
            }
        });
    }

    public void showShareSelectionDialog(Context context, String videoUrl, String postLink) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_social_share);
            popup.setCancelable(true);
            popup.show();

            RelativeLayout download = popup.findViewById(R.id.rlDownload);
            RelativeLayout share = popup.findViewById(R.id.rlShare);

            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "download";
                    downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "share";
                    popup.dismiss();
                    shareVideoLink(postLink, context);

                }
            });

        }
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    // DownloadTask for downloding video from URL
    public class DownloadTask extends AsyncTask<String, Integer, String> {
        private Context context;
        private PowerManager.WakeLock mWakeLock;
        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                java.net.URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                input = connection.getInputStream();


                String filePath = sUrl[0];
                fileN = filePath.substring(filePath.lastIndexOf("/")+1);
                //fileN = "Afrocamgist_" + UUID.randomUUID().toString().substring(0, 10) + ".mp4";

                //original
                //File filename = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/", fileN);

                File filename = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath() + "/AfrocamgistVideos/", fileN);


                output = new FileOutputStream(filename);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();

            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);

            mProgressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();

            if (result != null){
                Toast.makeText(context, getString(R.string.str_download_err) + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, getString(R.string.str_video_downloaded), Toast.LENGTH_SHORT).show();
                deleteWatermarkVideoInServer();
            }

            MediaScannerConnection.scanFile(getContext(),
                    new String[]{
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath()
                                    + "/AfrocamgistVideos/" + fileN}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String newpath, Uri newuri) {
                            Logger.i("ExternalStorage", "Scanned " + newpath + ":");
                            Logger.i("ExternalStorage", "-> uri=" + newuri);
                        }
                    });

            //original
            /*MediaScannerConnection.scanFile(getContext(),
                    new String[]{
                            Environment.getExternalStorageDirectory().getAbsolutePath()
                                    + "/AfrocamgistVideos/" + fileN}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String newpath, Uri newuri) {
                            Logger.i("ExternalStorage", "Scanned " + newpath + ":");
                            Logger.i("ExternalStorage", "-> uri=" + newuri);
                        }
                    });*/

            //original
            //String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + fileN;

            //new
            String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath() + "/AfrocamgistVideos/" + fileN;

            shareVideoToSocialMedia(filePath);
        }
    }

    //Get watermarked video from url
    private void callWatermarkVideoAPI(String videoUrl){
        waterMarkVideo = "";
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setLabel("Preparing...")
                .setCancellable(false)
                .show();

        JSONObject params = new JSONObject();
        try {
            params.put("video", videoUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.WATERMARK_VIDEO, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    }else {
                        if(object.getBoolean("result")){
                            waterMarkVideo = object.getString("video_path");
                            String watermarkVideo = UrlEndpoints.MEDIA_BASE_URL + object.getString("video_path");
                            newDownload(watermarkVideo);
                        }else {
                            Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //delete watermark video from server
    private void deleteWatermarkVideoInServer(){
        JSONObject params = new JSONObject();
        try {
            params.put("video", waterMarkVideo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.DELETE_WATERMARK_VIDEO;

        Webservices.getData(Webservices.Method.DELETE, params, headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.d("waterMark99","success");
                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void downloadVideo(String videoUrl){
        String originalFilename = videoUrl.substring(videoUrl.lastIndexOf("/")+1);
        result = checkPermission();
        if(result){
            checkFolder();
            if (!isConnectingToInternet(getContext())) {
                Toast.makeText(getContext(), getString(R.string.please_check_your_internet_connection), Toast.LENGTH_LONG).show();
                return;
            }

            //new
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename);
            if(file.exists()){
                String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename;
                shareVideoToSocialMedia(filePath);
            }else {
                callWatermarkVideoAPI(videoUrl);
            }


            //original
            /*File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename);
            if(file.exists()){
                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename;
                shareVideoToSocialMedia(filePath);
            }else {
                callWatermarkVideoAPI(videoUrl);
            }*/
        }
    }

    private void shareVideoLink(String videoUrl, Context context){
        //extraShareTextDialog(videoUrl, context);

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
        i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
        i.putExtra(Intent.EXTRA_TEXT, videoUrl);
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    public void extraShareTextDialog(String videoUrl, Context context) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_extra_share_text);
            popup.setCancelable(true);
            popup.show();

            EditText edtExtraText = popup.findViewById(R.id.edtExtraText);
            TextView btnSharePost = popup.findViewById(R.id.btnSharePost);

            btnSharePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
                    i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                    i.putExtra(Intent.EXTRA_TEXT, videoUrl + "  \n \n " + edtExtraText.getText().toString()  + "  \n \n");
                    startActivity(Intent.createChooser(i, "Share URL"));

                    popup.dismiss();
                }
            });

        }
    }

    private void shareVideoToSocialMedia(String filePath){

        MediaScannerConnection.scanFile(getContext(), new String[] { filePath },

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                        shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, "Afrocamgist");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                        startActivity(Intent.createChooser(shareIntent,
                                "Afrocamgist"));

                    }
                });
    }

    //hare you can start downloding video
    public void newDownload(String url) {
        final DownloadTask downloadTask = new DownloadTask(getContext());
        downloadTask.execute(url);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                /*if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }*/

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Permission necessary");
                alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();

                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public void checkAgain() {
        /*if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Logger.d("videoUrl09","should request rationale.....");
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            Logger.d("videoUrl09","below 23.....");
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }*/
        Logger.d("videoUrl09","should request rationale.....");
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
            }
        });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    //Here you can check App Permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                /*if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Logger.d("videoUrl09","allow fragment.....");

                    checkFolder();
                    if(mVideoUrl!=null){
                        downloadVideo(mVideoUrl);
                    }

                } else {
                    //code for deny
                    Logger.d("videoUrl09","deny fragment.....");

                    checkAgain();
                }*/

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Logger.d("videoUrl09","allow fragment.....");

                    checkFolder();
                    if(mVideoUrl!=null){
                        downloadVideo(mVideoUrl);
                    }
                } else {
                    //code for deny
                    Logger.d("videoUrl09","deny fragment.....");
                    //checkAgain();
                    checkPermission();
                }

                break;
        }
    }

    //hare you can check folfer whare you want to store download Video
    public void checkFolder() {

        //new
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath() + "/AfrocamgistVideos/";

        //original
        //String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/";
        File dir = new File(path);
        boolean isDirectoryCreated = dir.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated = dir.mkdir();
        }
        if (isDirectoryCreated) {
            // do something

            Logger.d("Folder", "Already Created");
        }
    }

    private void updatePostText(String postText) {
        if (swaggerPosts.size() > 0) {
            swaggerPosts.get(SAVED_SCROLL_POSITION).setPostText(postText);
            if (videosAdapter != null) {
                videosAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }
        }
    }

    private void pausePlayer(SimpleExoPlayer simpleExoPlayer){
        if (simpleExoPlayer != null) {
            Logger.d("pause111",""+"test1.......");
            simpleExoPlayer.setPlayWhenReady(false);
            simpleExoPlayer.getPlaybackState();
            simpleExoPlayer.seekTo(0);
        }
    }

    public void startPlayer(SimpleExoPlayer simpleExoPlayer) {
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(true);
            simpleExoPlayer.getPlaybackState();
        }
    }

    public void onResumeFragment(){
        Logger.d("playerStateFrom99","==Outside Resumed....==");

        if(videosAdapter!=null){
            if(openFromSwipe.equalsIgnoreCase("fromSwipe")){
                Logger.d("playerStateFrom99","==on Resume called from swipe...==");
                videosAdapter.notifyDataSetChanged();
                openFromSwipe = "";
            }
            if (videosAdapter.helper!=null){
                Logger.d("playerStateFrom99","==Resumed==");
                videosAdapter.helper.setPlayWhenReady(true);
                videosAdapter.helper.getPlaybackState();
            }else {
                Logger.d("playerStateFrom99","==Notified==");
                //videosAdapter.notifyDataSetChanged(); // original
                //new code
                if (mainList != null && videosAdapter != null) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mainList.getLayoutManager();
                    Logger.d("playerStateFrom99",layoutManager.findFirstVisibleItemPosition() +"     "+layoutManager.findFirstCompletelyVisibleItemPosition()+"      "+layoutManager.findLastVisibleItemPosition()+"     "+mainList.getChildCount());
                    videosAdapter.createPlayer(mainList.findViewHolderForLayoutPosition(lastVideoPlayPosition));
                }

            }

        }
    }

    public void onPauseFragment(){
        Logger.d("playerStateFrom99","==Outside Paused....==");

        if(videosAdapter!=null){
            if (videosAdapter.helper!=null){
                Logger.d("playerStateFrom99","==Paused....==");
                videosAdapter.helper.setPlayWhenReady(false);
                videosAdapter.helper.getPlaybackState();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Logger.d("FragmentState","==Destroyed1==");
        if(videosAdapter!=null){
            if (videosAdapter.helper!=null){
                Logger.d("FragmentState","==Destroyed==");
                videosAdapter.helper.setPlayWhenReady(false);
                videosAdapter.helper.getPlaybackState();
            }
        }
        localBroadcastManager.unregisterReceiver(mFollowStateReceiver);
        localBroadcastManager.unregisterReceiver(mCountReceiver);
        localBroadcastManager.unregisterReceiver(mCreateImagePostDataReceiver);
        localBroadcastManager.unregisterReceiver(mCreateVideoPostDataReceiver);
        localBroadcastManager.unregisterReceiver(mRefreshSwaggerPostsReceiver);
//        localBroadcastManager.unregisterReceiver(mLikeSwaggerTalent);
//        localBroadcastManager.unregisterReceiver(mStorylineFollowStateReceiver);
//        localBroadcastManager.unregisterReceiver(mNotifyMainStoryList);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (video1 != null && !video1.equals("") && data == null) {

            Logger.d("POSTING111","loading...1");
            if (swaggerPosts != null) {
                //swaggerPosts.add(2, null);
                //videosAdapter.notifyDataSetChanged();
                cardProgressbar.setVisibility(View.VISIBLE);
                showPostingView();
            }
            postText = postText1;
            location = location1;
            tagUserList = tagUserList1;
            postCategory = postCategory1;
            font_face = font_face1;
            createPost(null);
        }

        if (requestCode == 100 && resultCode == RESULT_OK) {
            if (data != null && data.getSerializableExtra("postType") != null) {
                if (swaggerPosts != null) {
                    //swaggerPosts.add(2, null); //important
                    //videosAdapter.notifyDataSetChanged();

                    cardProgressbar.setVisibility(View.VISIBLE);
                    showPostingView();
                }
                Logger.d("POSTING111","loading...2");

                postText = data.getStringExtra("postText");
                location = data.getStringExtra("location");
                tagUserList = (ArrayList<String>) data.getSerializableExtra("tagUserList");
                postCategory = data.getStringExtra("postCategory");
                imageType = data.getStringExtra("imageType");
                font_face = data.getStringExtra("font_face");
                createPost(data);
            } else if (video1 != null && !video1.equals("") && data == null) {
                if (swaggerPosts != null) {
                    //swaggerPosts.add(2, null); //important
                    //videosAdapter.notifyDataSetChanged();

                    cardProgressbar.setVisibility(View.VISIBLE);
                    showPostingView();
                }
                Logger.d("POSTING111","loading...3");

                postText = postText1;
                location = location1;
                postCategory = postCategory1;
                font_face = font_face1;
                createPost(null);
            } else {
                getSwaggerFirstPagePosts(true);
                getAdvertisements();
            }
        }else if (requestCode == 102 && resultCode == RESULT_OK) {
            if (data.getStringExtra("postText") != null) {
                updatePostText(data.getStringExtra("postText"));
            }
        }else if(requestCode == 99 && resultCode == RESULT_OK){

            Logger.d("FragmentState","onActivityResult called...");

            Bundle bundle = data.getExtras();
            //WHAT TO DO TO GET THE BUNDLE VALUES//
            String fromSwipe = bundle.getString("fromSwipe");
            if(fromSwipe.equalsIgnoreCase("fromSwipe")){
                openFromSwipe = "fromSwipe";
            }
        }
    }

}
