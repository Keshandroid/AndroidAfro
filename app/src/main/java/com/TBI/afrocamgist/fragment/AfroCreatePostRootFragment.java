package com.TBI.afrocamgist.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.TBI.afrocamgist.R;

public class AfroCreatePostRootFragment extends Fragment {

    private AfroCreatePostFragment afroCreatePostFragment;

    public AfroCreatePostRootFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_create_post_root, container, false);

        FragmentTransaction transaction;
        if (getFragmentManager() != null) {
            transaction = getFragmentManager().beginTransaction();

            afroCreatePostFragment = new AfroCreatePostFragment();

            transaction.replace(R.id.layout_afro_create_post, afroCreatePostFragment);
            transaction.commitAllowingStateLoss();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d("isRefreshed","21332");

        /*FragmentTransaction transaction;
        if (getFragmentManager() != null) {
            transaction = getFragmentManager().beginTransaction();
            afroCreatePostFragment = new AfroCreatePostFragment();
            transaction.replace(R.id.layout_afro_create_post, afroCreatePostFragment);
            transaction.commitAllowingStateLoss();
        }*/
    }

    @Override
    public void onPause() {
        super.onPause();

        afroCreatePostFragment.onPause();
    }
}
