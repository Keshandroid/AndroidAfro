package com.TBI.afrocamgist.fragment;


import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.AfroMyGroupAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.groups.GroupDetails;
import com.TBI.afrocamgist.model.post.Post;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class AfroMyGroupsFragment extends Fragment implements AfroMyGroupAdapter.OnMyGroupClickListener {

    private KProgressHUD hud;
    private GridView myGroups;
    private SwipeRefreshLayout swipeContainer;

    public AfroMyGroupsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_afro_my_groups, container, false);

        initView(view);
        initSwipeRefresh();

        if (Utils.isConnected())
            getAfroMyGroups();

        return view;
    }

    private void initView(View view) {

        myGroups = view.findViewById(R.id.myGroups);
        swipeContainer = view.findViewById(R.id.swipeContainer);
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getAfroMyGroups();
            else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(getActivity(), getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void getAfroMyGroups() {

        /*hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MY_GROUPS, response -> {
            swipeContainer.setRefreshing(false);
//            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        GroupDetails groupDetails = new Gson().fromJson(response, GroupDetails.class);

                        ArrayList<Group> myGroupList = new ArrayList<>();
                        Group group = new Group();
                        group.setGroupTitle("create group");
                        myGroupList.add(group);
                        myGroupList.addAll(groupDetails.getGroups());
                        groupDetails.setGroups(myGroupList);

                        /*ArrayList<Group> myGroupList = groupDetails.getGroups();
                        Group group = new Group();
                        group.setGroupTitle("create group");
                        myGroupList.add(group);
                        groupDetails.setGroups(myGroupList);*/
                        if (groupDetails.getGroups()!=null) {
                            myGroups.setAdapter(new AfroMyGroupAdapter(getActivity(), groupDetails.getGroups(), this));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    if(getActivity() != null){
                        Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                    }
                }
            }

        });
    }

    @Override
    public void onDeleteClicked(Group group) {
        showConfirmDeleteGroupAlert(group);
    }

    private void showConfirmDeleteGroupAlert(Group group) {

        Dialog popup = new Dialog(getActivity(), R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        TextView txtMessage =popup.findViewById(R.id.message);
        txtMessage.setText(getString(R.string.delete_group_alert_message));
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    deleteGroup(group.getGroupId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }


    private void deleteGroup(Integer groupId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.DELETE_GROUP + groupId;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                        getAfroMyGroups();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }
}
