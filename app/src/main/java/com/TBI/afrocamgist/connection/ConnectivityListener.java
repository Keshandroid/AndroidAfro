package com.TBI.afrocamgist.connection;

public interface ConnectivityListener {

    void onInternetConnectivityChanged(boolean isConnected);
}
