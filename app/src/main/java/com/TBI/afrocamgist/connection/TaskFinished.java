package com.TBI.afrocamgist.connection;

interface TaskFinished<T> {
    void onTaskFinished(T data);
}
