package com.TBI.afrocamgist.constants;

public class PrefKeys {

    public static String KEY_TOKEN = "fireToken";
    public static String KEY_USER_LOGIN = "userLogin";
    public static String KEY_USER_NOT_REGISTERED = "userNotRegistered";
    public static String KEY_USER_PASS = "userPass";
    public static String KEY_USER_PROFILE = "userProfile";
    public static String KEY_INTRODUCE = "userIntroduce";
    public static String KEY_USER_LOGIN_PREF = "userLoginPref";

    public static String KEY_LOGIN = "login";
    public static String KEY_SECRET = "secretKey";
    public static String IS_VIDEO_SCREEN_OPEN = "isVideoScreenOpen";
    public static String KEY_LOGIN_PREF = "loginPref";
    public static String KEY_VIDEO_PREF = "videoPref";

    public static String KEY_USER = "user";
    public static String KEY_USER_PREF = "userPref";

    public static String KEY_POPULAR_POST_IMAGES = "popularPostImages";
    public static String KEY_POPULAR_POST_IMAGES_PREF = "popularPostImagesPref";

    public static String KEY_POPULAR_POST = "popularPost";
    public static String KEY_POPULAR_POST_PREF = "popularPostPref";

    //advert
    public static String KEY_ADVERT_PREF = "advertPref";
    public static String KEY_ADVERT_TRANSACTION_ID = "transaction_id";
    public static String KEY_ADVERT_POST_ID = "post_id";
    public static String KEY_ADVERT_DURATION = "duration";
    public static String KEY_ADVERT_AMOUNT = "amount";
    public static String KEY_ADVERT_AUDIENCE_ID = "audience_id";
    public static String KEY_ADVERT_URL = "url";
    public static String KEY_ADVERT_GOAL_TYPE = "goal_type";
    public static String KEY_ADVERT_AUDIENCE_NAME = "audience_name";
    public static String KEY_ADVERT_COUNTRIES = "countries";
    public static String KEY_ADVERT_AGE_RANGE = "ageRange";
    public static String KEY_ADVERT_BUDGET_DURATION = "budgetDuration";
    public static String KEY_ADVERT_BUDGET_AMOUNT = "budgetAmount";


}
