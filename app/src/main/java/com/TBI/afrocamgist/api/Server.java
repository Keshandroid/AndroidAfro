package com.TBI.afrocamgist.api;

import androidx.annotation.NonNull;

import com.TBI.afrocamgist.GetPostsQuery;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.ApolloQueryCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.rx3.Rx3Apollo;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class Server {
    private static ApolloClient apolloClient;

    public static Observable<Response<GetPostsQuery.Data>> afroSwaggerPosts(int page) {

        GetPostsQuery getPostsQuery = GetPostsQuery.builder().page(page).build();

        ApolloQueryCall<GetPostsQuery.Data> call = getApolloClient()
                .query(getPostsQuery);

        return Rx3Apollo.from(call)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter((dataResponse -> dataResponse.getData() != null));
    }

    private static ApolloClient getApolloClient() {
        if (apolloClient == null) {
            String serverUrl = UrlEndpoints.GRAPH_QL_URL;
            //OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
            OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor(){
                @NotNull
                @Override
                public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                    Request newRequest  = chain.request().newBuilder()
                            .addHeader("Authorization", " Bearer " + LocalStorage.getLoginToken())
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();
            apolloClient = ApolloClient.builder()
                    .serverUrl(serverUrl)
                    .okHttpClient(okHttpClient)
                    .build();
        }

        return apolloClient;
    }
}
