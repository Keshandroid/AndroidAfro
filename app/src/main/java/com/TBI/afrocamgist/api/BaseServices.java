package com.TBI.afrocamgist.api;

import androidx.annotation.NonNull;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.google.gson.internal.$Gson$Preconditions;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseServices {

    private static Retrofit retrofit = null;

    public static Retrofit getAPI() {
        if (retrofit==null) {

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor(){
                @NotNull
                @Override
                public Response intercept(@NonNull Chain chain) throws IOException {
                    Request newRequest  = chain.request().newBuilder()
                            .addHeader("Authorization", " Bearer " + LocalStorage.getLoginToken())
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(UrlEndpoints.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
