package com.TBI.afrocamgist.bottomsheet;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.CommentAdapter;
import com.TBI.afrocamgist.adapters.HashtagPostAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.comment.Comment;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EntertainmentCommentBottomSheetFragment extends BottomSheetDialogFragment implements CommentAdapter.OnCommentClickListener{

    public static ArrayList<Comment> comments = new ArrayList<>();
    private TextView emoji1,emoji2,emoji3,emoji4,emoji5,emoji6,emoji7;
    private int postId = 0;
    private EditText userComment;
    private CommentAdapter adapter;
    private KProgressHUD hud;
    private TextView replyTo;
    private int parentPosition = -1;
    private Comment parentComment;
    private int subCommentPosition = -1;
    private Comment subComment;
    RecyclerView commentList;
    private boolean isEditComment = false;
    public static boolean isComment = true,isCommentPosted = false;
    private boolean isApiCall = false;
    public static final String LAST_TEXT = "mEditTextEntertainment";

    private View itemView;

    private static DialogDismissListener mListener;

    public EntertainmentCommentBottomSheetFragment() {
        // Required empty public constructor
    }

   /* @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (context instanceof DialogDismissListener) {
            mListener = (DialogDismissListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/


    public static EntertainmentCommentBottomSheetFragment newInstance(ArrayList<Comment> comment, Integer postId, DialogDismissListener listener) {

        mListener = listener;

        EntertainmentCommentBottomSheetFragment bottomSheetFragment = new EntertainmentCommentBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("comments", comment);
        bundle.putInt("postId",postId);
        bottomSheetFragment.setArguments(bundle);

        return bottomSheetFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comment_bottomsheet, container, false);
    }

    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        /*BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);

            }
        });
        return dialog;*/
        return new BottomSheetDialog(requireContext(), getTheme());  //set your created theme here

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.itemView = view;

        comments = (ArrayList<Comment>) getArguments().getSerializable("comments");
        postId = getArguments().getInt("postId");

        Log.d("comment09", new GsonBuilder().setPrettyPrinting().create().toJson(comments));

        initView();
        setEmoji();
        initClickListener();
        initTextChangeListener();
        setRecyclerView();
    }

    private void initView() {

        userComment = itemView.findViewById(R.id.user_comment);
        replyTo = itemView.findViewById(R.id.reply_to);

        emoji1 = itemView.findViewById(R.id.emoji1);
        emoji2 = itemView.findViewById(R.id.emoji2);
        emoji3 = itemView.findViewById(R.id.emoji3);
        emoji4 = itemView.findViewById(R.id.emoji4);
        emoji5 = itemView.findViewById(R.id.emoji5);
        emoji6 = itemView.findViewById(R.id.emoji6);
        emoji7 = itemView.findViewById(R.id.emoji7);

    }

    private void setEmoji(){
        emoji1.setText(getEmojiByUnicode(0x2764));
        emoji2.setText(getEmojiByUnicode(0x1F64C));
        emoji3.setText(getEmojiByUnicode(0x1F525));
        emoji4.setText(getEmojiByUnicode(0x1F44F));
        emoji5.setText(getEmojiByUnicode(0x1F622));
        emoji6.setText(getEmojiByUnicode(0x1F60D));
        emoji7.setText(getEmojiByUnicode(0x1F602));
    }

    public String getEmojiByUnicode(int unicode){
        return new String(Character.toChars(unicode));
    }

    private void initClickListener() {

        emoji1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComment.getText().insert(userComment.getSelectionStart(), emoji1.getText().toString());
            }
        });

        emoji2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComment.getText().insert(userComment.getSelectionStart(), emoji2.getText().toString());
            }
        });

        emoji3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComment.getText().insert(userComment.getSelectionStart(), emoji3.getText().toString());
            }
        });

        emoji4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComment.getText().insert(userComment.getSelectionStart(), emoji4.getText().toString());
            }
        });

        emoji5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComment.getText().insert(userComment.getSelectionStart(), emoji5.getText().toString());
            }
        });

        emoji6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComment.getText().insert(userComment.getSelectionStart(), emoji6.getText().toString());
            }
        });

        emoji7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userComment.getText().insert(userComment.getSelectionStart(), emoji7.getText().toString());
            }
        });

        userComment.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        itemView.findViewById(R.id.comment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isConnected()) {
                    if (!userComment.getText().toString().equals("")) {
                        if(!isApiCall) {
                            if (isEditComment)
                                editComment();
                            else if (parentPosition == -1)
                                postComment();
                            else {
                                replyToComment();
                            }
                        }
                    }
                } else {
                    Utils.showAlert(getActivity(), "No internet connection available.");
                }
            }
        });

        itemView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemView.findViewById(R.id.replying_layout).setVisibility(View.GONE);
                userComment.setText("");
                parentPosition = -1;
            }
        });

        itemView.findViewById(R.id.back).setOnClickListener(v -> {
            //onBackPressed();
        });
    }

    //set comments listing view
    private void setRecyclerView() {

        if (comments!=null && comments.size() > 0) {
            commentList = itemView.findViewById(R.id.comments);
            commentList.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new CommentAdapter(getActivity(), comments, this);
            commentList.setAdapter(adapter);

            commentList.scrollToPosition(comments.size()-1);

            itemView.findViewById(R.id.comments).setVisibility(View.VISIBLE);
            itemView.findViewById(R.id.no_comments).setVisibility(View.GONE);

        } else {

            itemView.findViewById(R.id.comments).setVisibility(View.GONE);
            itemView.findViewById(R.id.no_comments).setVisibility(View.VISIBLE);
        }
    }

    //add comment to post API
    private void postComment() {

        Log.d("comment11","--post_id--"+postId);

        JSONObject request = new JSONObject();
        try {
            request.put("post_id",postId);
            request.put("comment_text",userComment.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        isApiCall = true;
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.COMMENTS, response -> {
            isApiCall = false;
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    isCommentPosted = true;
                    addComment(object.getJSONObject("data").getInt("comment_id"));
                }

            } catch (Exception e) {
                Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    //display new comment on view
    private void addComment(int commentId) {

        Comment comment = new Comment();

        comment.setCommentId(commentId);
        comment.setCommentText(userComment.getText().toString());
        comment.setCommentedBy(LocalStorage.getUserDetails());
        comment.setLiked(false);

        comments.add(comment);
        if (adapter==null){
            setRecyclerView();
        } else {
            adapter.notifyDataSetChanged();
            commentList.scrollToPosition(comments.size() - 1);
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        itemView.findViewById(R.id.comments).setVisibility(View.VISIBLE);
        itemView.findViewById(R.id.no_comments).setVisibility(View.GONE);

        userComment.setText("");
    }

    //reply to comment API call
    private void replyToComment() {

        JSONObject request = new JSONObject();
        try {
            request.put("post_id",postId);
            request.put("comment_text",userComment.getText().toString());
            request.put("comment_parent_id",parentComment.getCommentId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        isApiCall = true;
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.COMMENTS, response -> {
            try {
                isApiCall = false;
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    isCommentPosted = true;
                    addReply(object.getJSONObject("data").getInt("comment_id"));
                }

            } catch (Exception e) {
                Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    //display reply on listing view
    private void addReply(int commentId) {

        Comment comment = new Comment();

        comment.setCommentId(commentId);
        comment.setCommentText(userComment.getText().toString());
        comment.setCommentedBy(LocalStorage.getUserDetails());
        comment.setLiked(false);

        ArrayList<Comment> subComments = new ArrayList<>();

        if (parentComment.getSubComments()!=null)
            subComments = parentComment.getSubComments();

        subComments.add(comment);
        parentComment.setSubComments(subComments);

        comments.set(parentPosition, parentComment);
        commentList.scrollToPosition(parentPosition);

        adapter.notifyDataSetChanged();

        parentPosition = -1;

        userComment.setText("");
    }

    //edit comment API call
    private void editComment() {

        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(false)
                .show();

        JSONObject request = new JSONObject();
        try {
            request.put("comment_text",userComment.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        isApiCall = true;
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.COMMENTS + "/" + (subComment==null ? parentComment.getCommentId() : subComment.getCommentId());

        Webservices.getData(Webservices.Method.PUT, request,headers, url, response -> {
            hud.dismiss();
            isApiCall = false;
            try {

                JSONObject object = new JSONObject(response);

                Log.d("editCommentSwagger",""+object.toString());

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    isCommentPosted = true;
                    updateEditedComment();
                }

            } catch (Exception e) {
                Log.d("executed111","yes...");
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    //update view after comment edit
    private void updateEditedComment() {

        if (subComment==null) {
            parentComment.setCommentText(userComment.getText().toString());
        } else {
            parentComment.getSubComments().get(subCommentPosition).setCommentText(userComment.getText().toString());
        }
        comments.set(parentPosition,parentComment);

        parentPosition = -1;
        isEditComment = false;
        adapter.notifyDataSetChanged();

        userComment.setText("");
    }

    @Override
    public void onLikeCommentClicked(Comment comment) {

        Log.d("clickedItem", new GsonBuilder().setPrettyPrinting().create().toJson(comment));

        likeComment(comment.getCommentId());

    }

    @Override
    public void onReplyClicked(int position,Comment parentComment, Comment comment) {

        String replyingTo = "Replying to ";
        parentPosition = position;
        this.parentComment = parentComment;

        if (comment==null) {
            String reply = "@" + parentComment.getCommentedBy().getFirstName() + " " + parentComment.getCommentedBy().getLastName() + " -";
            replyingTo += parentComment.getCommentedBy().getFirstName() + " " + parentComment.getCommentedBy().getLastName();
            userComment.setText(reply);
            userComment.setSelection(reply.length());
        } else {
            String reply = "@" + comment.getCommentedBy().getFirstName() + " " + comment.getCommentedBy().getLastName() + " -";
            replyingTo += comment.getCommentedBy().getFirstName() + " " + comment.getCommentedBy().getLastName();
            userComment.setText(reply);
            userComment.setSelection(reply.length());
        }

        itemView.findViewById(R.id.replying_layout).setVisibility(View.VISIBLE);
        replyTo.setText(replyingTo);
    }

    @Override
    public void onDeleteCommentClicked(int parentPosition, Comment parentComment, int position, Comment comment) {

        if(Utils.isConnected())
            deleteComment(parentPosition,parentComment,position,comment);
        else
            Utils.showAlert(getActivity(), "No internet connection available.");
    }

    //delete comment API call
    private void deleteComment(int parentPosition, Comment parentComment, int position, Comment comment) {

        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.COMMENTS + "/" + (comment==null ? parentComment.getCommentId() : comment.getCommentId());

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(),headers, url, response -> {
            hud.dismiss();
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {
                    isCommentPosted = true;
                    removeComment(parentPosition, parentComment, position, comment);
                }

            } catch (Exception e) {
                Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    //remove deleted comment from view
    private void removeComment(int parentPosition, Comment parentComment, int position, Comment comment) {

        if (comment == null) {
            comments.remove(parentPosition);
            adapter.notifyDataSetChanged();

            if (comments.size() == 0) {
                itemView.findViewById(R.id.comments).setVisibility(View.GONE);
                itemView.findViewById(R.id.no_comments).setVisibility(View.VISIBLE);
            }

        } else {

            ArrayList<Comment> subComments = parentComment.getSubComments();
            subComments.remove(position);

            parentComment.setSubComments(subComments);

            comments.set(parentPosition,parentComment);

            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onEditCommentClicked(int parentPosition, Comment parentComment, int position, Comment comment) {

        this.parentPosition = parentPosition;
        this.parentComment = parentComment;
        subCommentPosition = position;
        subComment = comment;
        isEditComment = true;

        if (comment==null) {
            userComment.setText(parentComment.getCommentText());
            userComment.setSelection(parentComment.getCommentText().length());
        } else {
            userComment.setText(comment.getCommentText());
            userComment.setSelection(comment.getCommentText().length());
        }

    }

    private void initTextChangeListener() {
        final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        userComment.setText(pref.getString(LAST_TEXT, ""));

        userComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ("".equals(userComment.getText().toString())) {
                    itemView.findViewById(R.id.replying_layout).setVisibility(View.GONE);
                    parentPosition = -1;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                pref.edit().putString(LAST_TEXT, s.toString()).apply();

            }
        });
    }

    //like comment API call
    private void likeComment(int commentId) {

        JSONObject request = new JSONObject();
        try {
            request.put("comment_id",commentId);
            request.put("like_type","comment");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("comment_id",""+commentId);
        Log.d("Token1", LocalStorage.getLoginToken());

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                } else {

                    Log.d("commentLike",new GsonBuilder().setPrettyPrinting().create().toJson(object));


                    isCommentPosted = true;
                }

            } catch (Exception e) {
                Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        isComment = true;
        mListener.onDialogDismiss(comments);
    }

    public interface DialogDismissListener {
        void onDialogDismiss(ArrayList<Comment> comments);
    }

    /*
    @Override
    public void onBackPressed() {
        if (isCommentPosted){
            isComment = true;
            Intent intent = new Intent();
            intent.putExtra("comments",comments);
            setResult(RESULT_OK,intent);
            finish();
        } else {
            isComment = true;
            super.onBackPressed();
        }
    }
*/
}
