package com.TBI.afrocamgist.View;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter;
import com.TBI.afrocamgist.fragment.AfroSwaggerFragment;
import com.TBI.afrocamgist.fragment.AfroTalentFragment;
import com.TBI.afrocamgist.listener.OnPostClickListener;
import com.TBI.afrocamgist.model.post.Post;
import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.video.VideoListener;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;


import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.exoplayer.Playable;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;


import static com.TBI.afrocamgist.activity.DashboardActivity.viewPager;
import static com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter.isMute;

public class DataObjectHolder extends RecyclerView.ViewHolder implements ToroPlayer {

    public LinearLayout sharedLayout;
    public ImageView postImage, moreImages, share, menu, thumbnail, backgroundImage, socialShare, report, mapPostImage, search, adImage;
    public CircleImageView profilePicture, sharedProfileImage,icTag;
    public RelativeLayout videoLayout, moreImagesLayout;
    public FrameLayout layoutOnlyImages;
    public FrameLayout thumbnailLayout;
    public RelativeLayout layoutPostWithBackgroundImage;
    public TextView follow,more,less,sharedFollow;
    public ToggleButton like;
    public TextView name, time, location, imageCount, likesCount, comment, sharedName, sharedTime,
            sharedPostText, sharedLocation, postTextOnImage, videoCount;
    public Uri mediaUri;
    public int videoPostId;
    public AutoLinkTextView postText;
    public ProgressBar imageLoadingProgressBar;

    public ImageView imgIcPlay;
    public ProgressBar vidProgressBar;

    public PlayerView playerView;
    public ExoPlayerViewHelper helper;
    public ToggleButton volume;
    public static int currentPosition = 1;
    OnPostClickListener listener;
    private Handler handler;
    private Runnable runnable;

    public Context context;

    public DataObjectHolder(View itemView, OnPostClickListener listener, Context context) {
        super(itemView);

        thumbnailLayout = itemView.findViewById(R.id.thumbnail_layout);

        imgIcPlay = itemView.findViewById(R.id.imgIcPlay);
        vidProgressBar = itemView.findViewById(R.id.vidProgressBar);

        this.listener = listener;
        this.context = context;
        playerView = itemView.findViewById(R.id.player);
        volume = itemView.findViewById(R.id.volume);
        sharedLayout = itemView.findViewById(R.id.shared_layout);
        sharedProfileImage = itemView.findViewById(R.id.shared_profile_image);
        sharedName = itemView.findViewById(R.id.shared_name);
        sharedFollow = itemView.findViewById(R.id.follow_shared);
        sharedTime = itemView.findViewById(R.id.shared_time);
        sharedLocation = itemView.findViewById(R.id.shared_location);
        sharedPostText = itemView.findViewById(R.id.shared_post_text);
        name = itemView.findViewById(R.id.name);
        time = itemView.findViewById(R.id.time);
        follow = itemView.findViewById(R.id.follow);
        location = itemView.findViewById(R.id.location);
        videoLayout = itemView.findViewById(R.id.video_layout);
        thumbnail = itemView.findViewById(R.id.thumbnail);
        layoutOnlyImages = itemView.findViewById(R.id.layout_only_images);
        imageLoadingProgressBar = itemView.findViewById(R.id.progress);
        postImage = itemView.findViewById(R.id.post_image);
        mapPostImage = itemView.findViewById(R.id.post_map_image);
        moreImagesLayout = itemView.findViewById(R.id.more_images_layout);
        moreImages = itemView.findViewById(R.id.more_images);
        imageCount = itemView.findViewById(R.id.image_count);
        likesCount = itemView.findViewById(R.id.likes_count);
        layoutPostWithBackgroundImage = itemView.findViewById(R.id.text_with_image_background);
        backgroundImage = itemView.findViewById(R.id.background_image);
        postTextOnImage = itemView.findViewById(R.id.post_text_on_image);
        profilePicture = itemView.findViewById(R.id.profile_picture);
        postText = itemView.findViewById(R.id.post_text);
        videoCount = itemView.findViewById(R.id.videoCount);
        like = itemView.findViewById(R.id.like);
        comment = itemView.findViewById(R.id.comment);
        socialShare = itemView.findViewById(R.id.social_share);
        share = itemView.findViewById(R.id.share);
        report = itemView.findViewById(R.id.report);
        menu = itemView.findViewById(R.id.menu);
        more = itemView.findViewById(R.id.more);
        less = itemView.findViewById(R.id.less);
        icTag = itemView.findViewById(R.id.icTag);
    }


    @NonNull
    @Override
    public View getPlayerView() {
        return playerView;
    }

    @NonNull
    @Override
    public PlaybackInfo getCurrentPlaybackInfo() {
        Log.e("LLLLLL_Count_Page1: ", String.valueOf(viewPager.getCurrentItem()));
        return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
    }

    @Override
    public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {


        thumbnailLayout.setVisibility(View.VISIBLE);
        String type="";
        if((playerView.getTag()!=null)){
            type=(String)playerView.getTag();

        }
        if(type.equals("video")|| type.equals("")){
            if (helper == null) {
                helper = new ExoPlayerViewHelper(this, mediaUri);
            }
            helper.initialize(container,playbackInfo);
        }

    }

    private boolean canPlayRightNow() {
        if(context instanceof AppCompatActivity)
        {
            FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentById(R.id.layout_afro_swagger);

            FragmentManager fragmentManager2 = ((AppCompatActivity)context).getSupportFragmentManager();
            Fragment fragment2 = fragmentManager2.findFragmentById(R.id.layout_afro_talent);


            if(fragment instanceof AfroSwaggerFragment)
            {
                return fragment.isResumed();
            } else if(fragment2 instanceof AfroTalentFragment){
                return fragment2.isResumed();
            }
        }
        return true;
    }

    @Override
    public void play() {
        if (helper != null) {
                if (!helper.isPlaying()) {
                    //imgIcPlay.setVisibility(View.GONE);
                    //vidProgressBar.setVisibility(View.VISIBLE);


                    //helper.play();
                    if(canPlayRightNow()) {
                        helper.play();
                        playerView.getPlayer().setRepeatMode(Player.REPEAT_MODE_ALL);
                        playerView.setKeepContentOnPlayerReset(true);



                        //Turn On/Off auto play videos
                        if (Identity.getAutoPlayVideo(context)){
                            listener.onPlayItem(getAdapterPosition());
                            listener.onVideoCount(videoPostId);
                        }else {
                            playerView.getPlayer().setPlayWhenReady(false);

                            handler = new Handler();
                            runnable = new Runnable() {
                                @Override
                                public void run() {
                                    if(playerView!= null && playerView.getPlayer()!=null){
                                        if(helper.isPlaying()){
                                            Log.d("VideoPlay","playing...");
                                            listener.onVideoCount(videoPostId);
                                            handler.removeCallbacks(runnable);
                                        }else {
                                            Log.d("VideoPlay","Not playing...");
                                            handler.postDelayed(this, 1000);
                                        }
                                    }
                                }
                            };
                            handler.postDelayed(runnable, 0);
                        }







                        /*handler = new Handler();
                        runnable = new Runnable() {
                            @Override
                            public void run() {
                                if(playerView!= null && playerView.getPlayer()!=null){
                                    int seconds = (int) (playerView.getPlayer().getCurrentPosition()/1000);

                                    //to stop handler after 3 seconds(execute else part and remove callbacks in if part).
                                    if(seconds==3){
                                        listener.onVideoCount(videoPostId);
                                        //handler.removeCallbacks(runnable);
                                    }else{
                                        //handler.postDelayed(this, 1000);
                                    }
                                    handler.postDelayed(this, 1000);
                                }
                            }
                        };
                        handler.postDelayed(runnable, 0);*/


                    } else {
                        Log.e(DataObjectHolder.class.getSimpleName(),getAdapterPosition()+" Item Not In Resume State");
                    }

                    if (isMute) {
                        mute();
                        volume.setChecked(false);
                    } else {
                        unMute();
                        volume.setChecked(true);
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            thumbnailLayout.setVisibility(View.GONE);
                        }
                    }, 1500);
                } else {
                    helper.pause();
                }
            }


    }

    @Override
    public void pause() {
        if(handler!=null){
            handler.removeCallbacks(runnable);
        }

        if (helper != null) helper.pause();

    }

    @Override
    public boolean isPlaying() {
        return helper != null && helper.isPlaying();
    }

    @Override
    public void release() {

        if(handler!=null){
            handler.removeCallbacks(runnable);
        }

        if (helper != null) {
            thumbnailLayout.setVisibility(View.VISIBLE);
            //imgIcPlay.setVisibility(View.VISIBLE);
            //vidProgressBar.setVisibility(View.GONE);
            helper.release();
            helper = null;
        }
    }

    @Override
    public boolean wantsToPlay() {
        return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85;
    }

    @Override
    public int getPlayerOrder() {
        return getAdapterPosition();
    }

    @SuppressLint("ClickableViewAccessibility")
    public void bind(Uri media, int currentPos, int postId) {
        currentPosition = currentPos;
        Log.e("LLLLLL_Count: ", String.valueOf(currentPos));
        this.mediaUri = media;
        this.videoPostId = postId;
        Log.e("VIDEO_POST_ID1", "222"+videoPostId);

        /*if(mediaUri!=null && videoPostId!=0){
            listener.onVideoCount(videoPostId);
        }*/

        this.volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    unMute();
                else
                    mute();
            }
        });
    }

    private void mute() {
        isMute = true;
        this.setVolume(0);
    }

    private void unMute() {
        isMute = false;
        this.setVolume(100);
    }

    private void setVolume(int amount) {
        final int max = 100;
        final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
        final float volume = (float) (1 - (numerator / Math.log(max)));
        try {
            if (helper != null) {
                this.helper.setVolume(volume);
            }
        }catch (Exception e){

        }

    }



}
