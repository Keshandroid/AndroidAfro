package com.TBI.afrocamgist;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.home.VideosAdapter;

import org.jetbrains.annotations.NotNull;

public class SwipeHelper extends ItemTouchHelper.SimpleCallback {
    VideosAdapter adapter;
    Context context;
    ColorDrawable background;

    public SwipeHelper(int dragDirs, int swipeDirs) {
        super(dragDirs, swipeDirs);
    }

    public SwipeHelper(VideosAdapter adapter, Context context) {
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT);
        this.adapter = adapter;
        this.context = context;
        background = new ColorDrawable(context.getResources().getColor(R.color.black));
    }

    @Override
    public boolean onMove(@NotNull RecyclerView recyclerView, @NotNull RecyclerView.ViewHolder viewHolder, @NotNull RecyclerView.ViewHolder target) {
        Log.d("swipeHelper9","== ON move");
        return false;
    }

    @Override
    public void onSwiped(@NotNull RecyclerView.ViewHolder viewHolder, int direction) {
        Log.d("swipeHelper9","== ON swiped");
        //adapter.openDetailActivity(viewHolder);
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {

        Log.d("swipeHelper9","== flags movement");

        if("normal".equalsIgnoreCase((String) viewHolder.itemView.getTag())) {
            //return makeMovementFlags(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT); // swipe left and right both
            return makeMovementFlags(0, ItemTouchHelper.LEFT);
        } else {
            return 0;
        }
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        Log.d("swipeHelper9","== ON child draw");

        View itemView = viewHolder.itemView;

        if (dX > 0) {
            Log.d("swipeHelper9","Right Swipe");
            //background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX), itemView.getBottom());
        } else if (dX < 0) {
            Logger.d("swipeHelper9","Left Swipe");
            background.setBounds(itemView.getRight() + ((int) dX), itemView.getTop(), itemView.getRight(), itemView.getBottom());
            if(!Constants.ALLOW_OPEN_PROFILE){
                adapter.openDetailActivity(viewHolder);
                Constants.ALLOW_OPEN_PROFILE = true;
            }
        } else {
            background.setBounds(0, 0, 0, 0);
        }
        background.draw(c);
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }
}
