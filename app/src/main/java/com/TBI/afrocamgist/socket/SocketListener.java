package com.TBI.afrocamgist.socket;

public interface SocketListener {

    void isSocketConnected(boolean connected);
    void isSocketReConnected();
    void onEvent(String event, Object... arg0);
}
