package com.TBI.afrocamgist;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.TBI.afrocamgist.app.AfrocamgistApplication;
import com.TBI.afrocamgist.utils.KeyPairBoolData;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class Utils {

    private static Dialog popup;
    private static Dialog popup1;

    public static boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) AfrocamgistApplication.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null)
            return false;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
    }

    public static boolean isInternetConnected(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null;
        } catch (Exception e2) {
            return false;
        }
    }

    public static void showAlertWithFinish(Activity activity, String message) {

        if (activity != null && !activity.isFinishing()) {
            popup = new Dialog(activity, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_error);
            popup.setCancelable(true);
            popup.show();

            TextView error = popup.findViewById(R.id.message);
            error.setText(message);
            popup.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popup.dismiss();
                    activity.finish();
                }
            });
        }
    }

    public static void showAlert(Activity activity, String message) {

        if (activity != null && !activity.isFinishing()) {
            popup = new Dialog(activity, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_error);
            popup.setCancelable(true);
            popup.show();

            TextView error = popup.findViewById(R.id.message);
            error.setText(message);
            popup.findViewById(R.id.ok).setOnClickListener(v ->
                    popup.dismiss());
        }
    }


    public static void showSuccessAlert(Activity activity, String message) {

        if (activity != null && !activity.isFinishing()) {
            popup1 = new Dialog(activity, R.style.DialogCustom);
            popup1.setContentView(R.layout.dialog_error);
            popup1.setCancelable(true);
            popup1.show();

            ImageView error1 = popup1.findViewById(R.id.error);
            error1.setVisibility(View.GONE);

            ImageView success = popup1.findViewById(R.id.success);
            success.setVisibility(View.VISIBLE);

            TextView error = popup1.findViewById(R.id.message);
            error.setText(message);

            popup1.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup1.dismiss();
                }
            });
        }
    }

    public static void showComingSoonDialog(Context context) {

        popup1 = new Dialog(context, R.style.DialogCustom);
        popup1.setContentView(R.layout.dialog_coming_soon);
        popup1.setCancelable(true);
        popup1.show();

        popup1.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup1.dismiss();
            }
        });
    }

    public static void closeSuccessAlert() {
        if (popup1 != null && popup1.isShowing()) {
            popup1.dismiss();
        }
    }

    public static void closeAlert() {
        try {
            if (popup != null && popup.isShowing()) {
                popup.dismiss();
            }
        } catch (Exception e) {

        }


    }

    public static int getCurrentCountryCode(Activity context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryIso;
        if (telephonyManager == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                countryIso = context.getResources().getConfiguration().getLocales().get(0).getCountry();
            } else {
                countryIso = context.getResources().getConfiguration().locale.getCountry();
            }
            if (countryIso != null && countryIso.length() == 2) {
                return PhoneNumberUtil.getInstance().getCountryCodeForRegion(countryIso.toUpperCase());
            } else {
                return 0;
            }
        } else {
            if (telephonyManager.getSimCountryIso() == null || telephonyManager.getSimCountryIso().isEmpty())
                countryIso = telephonyManager.getNetworkCountryIso().toUpperCase();
            else
                countryIso = telephonyManager.getSimCountryIso().toUpperCase();

            return PhoneNumberUtil.getInstance().getCountryCodeForRegion(countryIso);
        }

    }

    public static String prettyCount(Integer number) {
        char[] suffix = {' ', 'k', 'M', 'B', 'T', 'P', 'E'};
        long numValue = number.longValue();
        int value = (int) Math.floor(Math.log10(numValue));
        int base = value / 3;
        if (value >= 3 && base < suffix.length) {
            return new DecimalFormat("#0.0").format(numValue / Math.pow(10, base * 3)) + suffix[base];
        } else {
            return new DecimalFormat("#,##0").format(numValue);
        }
    }

    public static String getSocialStyleTime(String date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        long time = 0;
        try {
            time = sdf.parse(date).getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

        //return TimeAgo.using(time).replace("about","");
        return TimeAgo.using(time)
                .replace("about", "")
                .replace("hour", "hr")
                .replace("minutes", "min");
    }

    public static String getFormattedDate(String time) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        long timeInMilliseconds = 0L;
        try {
            Date mDate = sdf.parse(time);
            if (mDate != null) {
                timeInMilliseconds = mDate.getTime();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(timeInMilliseconds);
        smsTime.setTimeZone(TimeZone.getDefault());
        //smsTime.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));

        Calendar now = Calendar.getInstance();
        now.setTimeZone(TimeZone.getDefault());
        //now.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "dd/MM/yyyy";

        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return DateFormat.format(timeFormatString, smsTime).toString();
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return "Yesterday";
        } else {
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        }
    }

    public static String getFormattedTimeForChatMessage(String time) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        long timeInMilliseconds = 0L;
        try {
            Date mDate = sdf.parse(time);
            timeInMilliseconds = mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(timeInMilliseconds);
        smsTime.setTimeZone(TimeZone.getDefault());

        Calendar now = Calendar.getInstance();
        now.setTimeZone(TimeZone.getDefault());

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "dd/MM/yyyy";

        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return DateFormat.format(timeFormatString, smsTime).toString();
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return "Yesterday at " + DateFormat.format(timeFormatString, smsTime).toString();
        } else {
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        }
    }

    public static String convertDateFormat(String inputDate) {

        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        inputDateFormat.setTimeZone(TimeZone.getDefault());
        outputDateFormat.setTimeZone(TimeZone.getDefault());

        String outputDate = "";
        try {
            outputDate = outputDateFormat.format(inputDateFormat.parse(inputDate));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return outputDate;
    }

    public static String convertDateFormat1(String inputDate) {

        SimpleDateFormat inputDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        inputDateFormat.setTimeZone(TimeZone.getDefault());
        outputDateFormat.setTimeZone(TimeZone.getDefault());

        String outputDate = "";
        try {
            outputDate = outputDateFormat.format(inputDateFormat.parse(inputDate));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return outputDate;
    }

    public static void showKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) AfrocamgistApplication.getInstance().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public static boolean isUrlValid(String enteredUrl) {
        return Patterns.WEB_URL.matcher(enteredUrl).matches() && URLUtil.isValidUrl(enteredUrl);
    }

    /*public static List<KeyPairBoolData> getCountryList() {

        List<KeyPairBoolData> countryList = new ArrayList<>();
        KeyPairBoolData data;
        data = new KeyPairBoolData();
        data.setName("Afghanistan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Afghanistan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Albania");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Algeria");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Andorra");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Angola");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Antigua & Deps");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Argentina");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Armenia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Australia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Austria");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Azerbaijan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Bahamas");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Bahrain");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Bangladesh");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Barbados");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Belarus");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Belgium");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Belize");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Benin");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Bhutan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Bolivia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Bosnia Herzegovina");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Botswana");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Brazil");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Brunei");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Bulgaria");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Burkina");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Burundi");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Cambodia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Cameroon");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Canada");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Cape Verde");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Central African Rep");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Chad");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Chile");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("China");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Colombia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Comoros");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Congo");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Congo {Democratic Rep}");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Costa Rica");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Croatia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Cuba");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Cyprus");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Czech Republic");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Denmark");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Djibouti");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Dominica");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Dominican Republic");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("East Timor");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Ecuador");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Egypt");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("El Salvador");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Equatorial Guinea");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Eritrea");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Estonia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Ethiopia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Fiji");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Finland");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("France");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Gabon");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Gambia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Georgia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Germany");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Ghana");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Greece");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Grenada");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Guatemala");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Guinea");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Guinea-Bissau");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Guyana");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Haiti");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Honduras");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Hungary");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Iceland");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("India");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Indonesia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Iran");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Iraq");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Ireland {Republic}");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Israel");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Italy");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Ivory Coast");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Jamaica");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Japan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Jordan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Kazakhstan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Kenya");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Kiribati");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Korea North");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Korea South");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Kosovo");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Kuwait");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Kyrgyzstan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Laos");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Latvia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Lebanon");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Lesotho");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Liberia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Libya");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Liechtenstein");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Lithuania");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Luxembourg");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Macedonia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Madagascar");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Malawi");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Malaysia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Maldives");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Mali");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Malta");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Marshall Islands");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Mauritania");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Mauritius");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Mexico");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Micronesia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Moldova");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Monaco");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Mongolia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Montenegro");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Morocco");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Mozambique");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Myanmar, {Burma}");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Namibia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Nauru");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Nepal");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Netherlands");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("New Zealand");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Nicaragua");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Niger");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Nigeria");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Norway");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Oman");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Pakistan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Palau");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Panama");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Papua New Guinea");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Paraguay");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Peru");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Philippines");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Poland");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Portugal");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Qatar");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Romania");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Russian Federation");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Rwanda");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("St Kitts & Nevis");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("St Lucia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Saint Vincent & the Grenadines");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Samoa");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("San Marino");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Sao Tome & Principe");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Saudi Arabia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Senegal");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Serbia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Seychelles");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Sierra Leone");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Singapore");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Slovakia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Slovenia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Solomon Islands");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Somalia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("South Africa");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("South Sudan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Spain");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Sri Lanka");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Sudan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Suriname");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Swaziland");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Sweden");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Switzerland");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Syria");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Taiwan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Tajikistan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Tanzania");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Thailand");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Togo");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Tonga");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Trinidad & Tobago");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Tunisia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Turkey");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Turkmenistan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Tuvalu");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Uganda");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Ukraine");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("United Arab Emirates");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("United Kingdom");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("United States");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Uruguay");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Uzbekistan");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Vanuatu");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Vatican City");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Venezuela");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Vietnam");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Yemen");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Zambia");
        countryList.add(data);

        data = new KeyPairBoolData();
        data.setName("Zimbabwe");
        countryList.add(data);

        return countryList;

    }*/

    public static List<KeyPairBoolData> getCountryList(Context context) {

        List<String> countriesFromArray = Arrays.asList(context.getResources().getStringArray(R.array.countries));

        List<KeyPairBoolData> countryList = new ArrayList<>();
        KeyPairBoolData data;
        for (int i = 0; i < countriesFromArray.size(); i++) {
            data = new KeyPairBoolData();
            data.setName(countriesFromArray.get(i));
            countryList.add(data);
        }

        return countryList;

    }

    public static List<KeyPairBoolData> getSelectedCountryList(Context context, ArrayList<String> country) {

        List<String> countriesFromArray = Arrays.asList(context.getResources().getStringArray(R.array.countries));

        List<KeyPairBoolData> countryList = new ArrayList<>();
        KeyPairBoolData data;

        for (int i = 0; i < countriesFromArray.size(); i++) {

            for (int j=0;j<country.size();j++){
                if(countriesFromArray.get(i).equalsIgnoreCase(country.get(j))){
                    data = new KeyPairBoolData();
                    data.setName(countriesFromArray.get(i));
                    data.setSelected(true);
                    countryList.add(data);
                }else {
                    data = new KeyPairBoolData();
                    data.setName(countriesFromArray.get(i));
                    data.setSelected(false);
                    countryList.add(data);
                }
            }

        }


        return countryList;

    }

    public static List<KeyPairBoolData> getInterestsList(Context context) {

        List<String> interestFromArray = Arrays.asList(context.getResources().getStringArray(R.array.afroInterest));

        List<KeyPairBoolData> interestList = new ArrayList<>();
        KeyPairBoolData data;
        for (int i = 0; i < interestFromArray.size(); i++) {
            data = new KeyPairBoolData();
            data.setName(interestFromArray.get(i));
            interestList.add(data);
        }

        return interestList;

    }

    public static List<KeyPairBoolData> getSelectedInterestsList(Context context, ArrayList<String> interests) {

        List<String> interestFromArray = Arrays.asList(context.getResources().getStringArray(R.array.afroInterest));

        List<KeyPairBoolData> interestList = new ArrayList<>();
        KeyPairBoolData data;

        for (int i = 0; i < interestFromArray.size(); i++) {

            for (int j=0;j<interests.size();j++){
                if(interestFromArray.get(i).equalsIgnoreCase(interests.get(j))){
                    data = new KeyPairBoolData();
                    data.setName(interestFromArray.get(i));
                    data.setSelected(true);
                    interestList.add(data);
                }else {
                    data = new KeyPairBoolData();
                    data.setName(interestFromArray.get(i));
                    data.setSelected(false);
                    interestList.add(data);
                }
            }


        }

        return interestList;

    }

}
