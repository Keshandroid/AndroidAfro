package com.TBI.afrocamgist.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.view.View;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.PhotosAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.media.MediaDetails;
import com.TBI.afrocamgist.model.photos.Photo;
import com.TBI.afrocamgist.model.photos.PhotoDetails;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.connection.Connectivity;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PhotosActivity extends BaseActivity
        implements PhotosAdapter.OnPhotoClickListener, ConnectivityListener {

    private KProgressHUD hud;
    private Connectivity mConnectivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        setClickListener();
        initInternetConnectivityCheckListener();
    }

    private void setClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.upload).setOnClickListener(view -> openImagePicker());
    }

    private void initInternetConnectivityCheckListener() {
        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud!=null && hud.isShowing()){
                hud.dismiss();
            }
            Utils.closeAlert();
            getPhotoList();
        } else {
            Utils.showAlert(PhotosActivity.this, getString(R.string.no_internet_connection_available));
        }
    }

    private void openImagePicker() {

        new ImagePicker.Builder(this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.JPG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .allowOnlineImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            assert mPaths != null;
            Logger.d("Images", mPaths.toString());
            uploadImage(mPaths);
        }
    }

    private void uploadImage(ArrayList<String> mPaths) {

        File photo = new File(mPaths.get(0));

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PHOTOS;

        Webservices.getData(this, Webservices.Method.POST, builder, headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(PhotosActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                   if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(PhotosActivity.this, object.getString(Constants.MESSAGE));
                    }
                    Utils.showSuccessAlert(PhotosActivity.this, getString(R.string.image_is_uploaded_successfully));
                    getPhotoList();

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(PhotosActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void getPhotoList() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.PHOTO_COLLECTION, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(PhotosActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(PhotosActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PhotoDetails photoDetails = new Gson().fromJson(response, PhotoDetails.class);
                        if (photoDetails.getPhotos()==null || photoDetails.getPhotos().isEmpty()) {
                            findViewById(R.id.photo_list).setVisibility(View.GONE);
                            findViewById(R.id.no_photos).setVisibility(View.VISIBLE);
                        } else {
                            setPhotoList(photoDetails.getPhotos());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(PhotosActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });

    }

    private void setPhotoList(ArrayList<Photo> photos) {
        RecyclerView photoList = findViewById(R.id.photo_list);
        if(photoList.getVisibility()==View.GONE){
            findViewById(R.id.photo_list).setVisibility(View.VISIBLE);
            findViewById(R.id.no_photos).setVisibility(View.GONE);
        }
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setAlignItems(AlignItems.FLEX_START);
        photoList.setLayoutManager(new GridLayoutManager(PhotosActivity.this,2));
        photoList.setAdapter(new PhotosAdapter(photos,this));
    }

    @Override
    public void onPhotoClicked(ArrayList<Photo> photos, int position) {
        startActivity(new Intent(this,PhotoGalleryActivity.class)
            .putExtra("photos",photos)
            .putExtra("position",position));
    }

    @Override
    public void onDeletePost(Photo photo) {

        Dialog popup = new Dialog(PhotosActivity.this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        TextView message = popup.findViewById(R.id.message);
        message.setText("Are you sure you want to delete the photo ?");

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected()){
                    hud = KProgressHUD.create(PhotosActivity.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setDimAmount(0.5f)
                            .setCancellable(true)
                            .show();

                    Map<String, String> headers = new HashMap<>();
                    headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
                    Logger.e("LLLL_Del_Id: ",photo.getId());

                    Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, UrlEndpoints.PHOTO_DEL + photo.getUserPhotoId(), response -> {
                        hud.dismiss();
                        Logger.e("LLLL_Del_Res: ",response);
                        if ("".equals(response)) {
                            Utils.showAlert(PhotosActivity.this, getString(R.string.opps_something_went_wrong));
                        } else {
                            try {
                                JSONObject object = new JSONObject(response);

                                if (object.has(Constants.MESSAGE)) {
                                    Utils.showAlert(PhotosActivity.this, object.getString(Constants.MESSAGE));
                                    getPhotoList();
                                } else {
                                    if (object.getBoolean("status")){
                                        getPhotoList();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
//                    Logger.e("LLLLLL_Del: ", Objects.requireNonNull(e.getMessage()));
                                Utils.showAlert(PhotosActivity.this, getString(R.string.opps_something_went_wrong));
                            }
                        }
                    });
                }
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
    }
}
