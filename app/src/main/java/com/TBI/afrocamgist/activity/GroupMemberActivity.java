package com.TBI.afrocamgist.activity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.FollowingAdapter;
import com.TBI.afrocamgist.adapters.GroupMembersAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.groups.GroupDetails;
import com.TBI.afrocamgist.model.groups.GroupMemberData;
import com.TBI.afrocamgist.model.groups.GroupMemberDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GroupMemberActivity extends BaseActivity {

    private ArrayList<Follow> groupMemberList = new ArrayList<>();
    private int groupId;
    private TextView followingCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member);

        groupId = getIntent().getIntExtra("groupId",0);

        initView();
        getGroupMemberList();
        setClickListener();

        ((SwipeRefreshLayout)findViewById(R.id.swipeContainer)).setRefreshing(false);
        findViewById(R.id.swipeContainer).setEnabled(false);
    }

    private void initView() {
        followingCount = findViewById(R.id.group_member_count);
    }

    private void getGroupMemberList() {

        KProgressHUD hud = KProgressHUD.create(GroupMemberActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.GROUP_MEMBER_LIST + groupId +"/memberlist";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(GroupMemberActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(GroupMemberActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        GroupMemberDetails groupMemberDetails = new Gson().fromJson(response, GroupMemberDetails.class);
                        if (groupMemberDetails.getGroupMemberList()!=null) {
                            setRecyclerView(groupMemberDetails.getGroupMemberList());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(GroupMemberActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    private void setRecyclerView(ArrayList<GroupMemberData> groupMemberList) {

        String count = groupMemberList.isEmpty() ? "0" : groupMemberList.size() + "";
        followingCount.setText(count);

        RecyclerView following = findViewById(R.id.group_member_list);
        following.setLayoutManager(new LinearLayoutManager(this));
        following.setAdapter(new GroupMembersAdapter(this,groupMemberList));
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }
}
