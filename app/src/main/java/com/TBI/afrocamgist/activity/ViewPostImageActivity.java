package com.TBI.afrocamgist.activity;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;

import android.view.WindowManager;

import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.PostImageAdapter;

import java.util.ArrayList;

import ooo.oxo.library.widget.PullBackLayout;

public class ViewPostImageActivity extends BaseActivity implements PullBackLayout.Callback {

    private ArrayList<String> images = new ArrayList<>();
    private int position;
    private boolean isPullCanceled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_post_image);

        if (getIntent().getSerializableExtra("images")!=null) {
            images = (ArrayList<String>) getIntent().getSerializableExtra("images");
        }

        position = getIntent().getIntExtra("position",0);

        Logger.d("images111",""+images.get(0) + "pos : " + position);

        initImageList();
        setClickListener();
    }

    private void initImageList() {
        ViewPager imageList = findViewById(R.id.image_list);
        imageList.setAdapter(new PostImageAdapter(this, images));
        imageList.setCurrentItem(position);
    }

    private void setClickListener() {

        findViewById(R.id.close).setOnClickListener(v -> onBackPressed());
        ((PullBackLayout)findViewById(R.id.pull)).setCallback(this);
    }

    @Override
    public void onPullStart() {
        isPullCanceled = false;
    }

    @Override
    public void onPull(float v) {
        /*if(isPullCanceled){
            findViewById(R.id.gallery_layout).setBackgroundColor(Color.parseColor("#ff000000"));
        }else {
            findViewById(R.id.gallery_layout).setBackgroundColor(Color.parseColor("#BC000000"));
        }*/
    }

    @Override
    public void onPullCancel() {
        isPullCanceled = true;
    }

    @Override
    public void onPullComplete() {
        supportFinishAfterTransition();
        overridePendingTransition(0,0);
    }
}
