package com.TBI.afrocamgist.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.AfroChatAdapter;
import com.TBI.afrocamgist.adapters.ArchivedAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.chat.Conversation;
import com.TBI.afrocamgist.model.chat.ConversationDetails;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.user.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.Ack;

public class ArchivedUserActivity extends BaseActivity implements ArchivedAdapter.OnItemClickListener {

    private ArchivedAdapter archivedAdapter;
    RecyclerView recyclerView;
    private ArrayList<Conversation> archivedList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archived_user);

        initView();
        getArchiveMessages();
        //setRecyclerView(archivedList);
        setClickListener();

        ((SwipeRefreshLayout)findViewById(R.id.swipeContainer)).setRefreshing(false);
        findViewById(R.id.swipeContainer).setEnabled(false);
    }

    private void getArchiveMessages(){
        //Socket API
        JSONObject data = new JSONObject();
        try {
            data.put("from_id", LocalStorage.getUserDetails().getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.getArchiveMessages, data, new Ack() {
                @Override
                public void call(Object... args) {
                    //swipeContainer.setRefreshing(false);
                    Log.i("SocketEvent", "GetArchivedMessages with Ack()");
                    //Log.i("SocketEvent", "Message List = "+ Arrays.toString(args));// print complete response
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (args[1] != null) {
                                JSONObject jsonObject = (JSONObject) args[1];

                                ConversationDetails conversationDetails = new Gson().fromJson(String.valueOf(jsonObject), ConversationDetails.class);

                                if (conversationDetails != null) {
                                    if (conversationDetails.getConversationList().size() > 0) {
                                        archivedList = new ArrayList<>();
                                        archivedList.addAll(conversationDetails.getConversationList());
                                        setRecyclerView(archivedList);
                                    }else {
                                        finish();
                                    }
                                }

                            }
                        }
                    });

                }
            });
        }




        //Normal API
        /*JSONObject request = new JSONObject();
        try {
            request.put("from_id", LocalStorage.getUserDetails().getUserId());
            request.put("type", "archivedAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());


        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.GET_ARCHIVED_USERS, response -> {
            try {
                JSONObject object = new JSONObject(response);
                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(ArchivedUserActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Log.d("ARCHIVED_GET",new GsonBuilder().setPrettyPrinting().create().toJson(object));

                    ConversationDetails conversationDetails = new Gson().fromJson(response, ConversationDetails.class);
                    if (conversationDetails != null) {
                        if (conversationDetails.getConversationList().size() > 0) {
                            archivedList = new ArrayList<>();
                            archivedList.addAll(conversationDetails.getConversationList());
                            setRecyclerView(archivedList);
                        }else {
                            finish();
                        }
                    }
                }

            } catch (Exception e) {
                Logger.d("executed111","yes...");
                e.printStackTrace();
            }
        });*/




    }

    private void initView() {
        recyclerView = findViewById(R.id.archive_list);
        //TextView followerCount = findViewById(R.id.follower_count);
        //String count = archivedList== null ? "0" : archivedList.size() + "";
        //followerCount.setText(count);
    }

    //set followers list in the view
    private void setRecyclerView(ArrayList<Conversation> archivedUser) {
        if(archivedAdapter==null){
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            archivedAdapter = new ArchivedAdapter( this,archivedUser, this);
            recyclerView.setAdapter(archivedAdapter);
        }else {
            archivedAdapter.notifydata(archivedUser);

        }

    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }

    @Override
    public void onRemoveArchived(Conversation user) {
        callRemoveArchived(LocalStorage.getUserDetails().getUserId(),user.getUserId());
    }

    private void callRemoveArchived(Integer fromId, Integer toId){

        //Socket API
        ArrayList<Integer> userIds = new ArrayList<>();
        userIds.add(toId);

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(userIds.get(0));

        JSONObject data = new JSONObject();
        try {
            data.put("from_id", fromId);
            data.put("to_id", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            Log.i("SocketEvent", "params : "+new GsonBuilder().setPrettyPrinting().create().toJson(data));

            UrlEndpoints.socketIOClient.emit(UrlEndpoints.removeArchiveMessages, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.i("SocketEvent", "RemoveArchived with Ack()");
                    //Log.i("SocketEvent", "RemoveArchived Data = "+ Arrays.toString(args));// print complete response
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            getArchiveMessages();
                        }
                    });
                }
            });
        }



        //Normal API
        /*ArrayList<Integer> userIds = new ArrayList<>();
        userIds.add(toId);

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(userIds.get(0));

        JSONObject request = new JSONObject();
        try {
            request.put("from_id", fromId);
            request.put("to_id", jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());


        Webservices.getData(Webservices.Method.POST, request,headers, UrlEndpoints.REMOVE_ARCHIVED_USERS, response -> {
            try {
                JSONObject object = new JSONObject(response);
                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(ArchivedUserActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Log.d("ARCHIVED_REMOVE",new GsonBuilder().setPrettyPrinting().create().toJson(object));

                    getArchiveMessages();
                }

            } catch (Exception e) {
                Logger.d("executed111","yes...");
                e.printStackTrace();
            }
        });*/


    }



}
