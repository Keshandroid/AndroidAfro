package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.ToolDotProgress;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.ChatMessageAdapter;
import com.TBI.afrocamgist.afrocalls.RTCActivity;
import com.TBI.afrocamgist.afrocalls.RTCAudioActivity;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.BottomPopup;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.chat.Message;
import com.TBI.afrocamgist.model.chat.MessageDetails;
import com.TBI.afrocamgist.model.notification.NotificationDetails;
import com.TBI.afrocamgist.model.user.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.kaopiz.kprogresshud.KProgressHUD;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Ack;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SocketChatActivity extends BaseActivity implements View.OnClickListener, BottomPopup.DialogDismissListener, ChatMessageAdapter.OnMessageClickListener, IChatEventListener {

    private User user;
    private ImageView more, startVideoCall, startAudioCall;
    private LinearLayout ll_more, ll_report, ll_cancel, llClearChat, llDeleteForBoth;
    private CircleImageView profilePicture;
    private TextView name, tv_null, replyTo, tv_please_wait;
    private EditText message;
    private int messageCount = 0;
    private int lastMessageId = 0;
    private int currentScrollPosition = 0;
    private boolean hasReceivedNewMessages = false;
    private Handler handler;
    private ArrayList<Message> messages = new ArrayList<>();
    public static final int CAMERA_INTENT = 100;
    private boolean isApiCall = false, isLoader;


    private FrameLayout flUserBlockedView;
    private TextView tvButtonUnblock;
    private KProgressHUD hud, hudChat;
    private ToolDotProgress dots_progress;

    protected LocalBroadcastManager localBroadcastManager;
    public static String NOTIFY_COUNTER = "notify_counter";

    private boolean isEditMessage = false, isReplyMessage = false;
    private int messageId, toId;

    ChatMessageAdapter adapter = null;
    RecyclerView chatList;

    private boolean isLoading = false;
    private boolean isScroll = false;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public static void expand(final View v) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.MATCH_PARENT // original wrap_content
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        //a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density)); // original
        a.setDuration(100);
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        //a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density)); // original
        a.setDuration(100);
        v.startAnimation(a);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);

        if (getIntent().getSerializableExtra("user") != null)
            user = (User) getIntent().getSerializableExtra("user");

        isLoader = getIntent().getBooleanExtra("loading", false);

        Logger.d("isLoader", "" + isLoader);

        Log.d("USER_ID",""+user.getUserId());

        ChatActivityUserWraper.getInstance().setUser(user);

        initView();
        initClickListener();

        setData();

        //old-code
        /*if (Utils.isConnected()) {
            getConversation(2000);
        } else
            Utils.showAlert(SocketChatActivity.this, getString(R.string.please_check_your_internet_connection));*/
    }

    private void initView() {

        startVideoCall = findViewById(R.id.startVideoCall);
        startAudioCall = findViewById(R.id.startAudioCall);
        replyTo = findViewById(R.id.reply_to);
        more = findViewById(R.id.more);
        ll_more = findViewById(R.id.ll_more);
        ll_report = findViewById(R.id.llReport);
        llClearChat = findViewById(R.id.llClearChat);
        llDeleteForBoth = findViewById(R.id.llDeleteForBoth);
        ll_cancel = findViewById(R.id.ll_cancel);
        dots_progress = findViewById(R.id.dots_progress);

        profilePicture = findViewById(R.id.profile_picture);
        name = findViewById(R.id.name);
        message = findViewById(R.id.message);
        tv_null = findViewById(R.id.tv_null);
        chatList = findViewById(R.id.chat_list);
        //tv_please_wait = findViewById(R.id.tv_please_wait);

        flUserBlockedView = findViewById(R.id.flUserBlockedView);

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true);

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_more.getVisibility() == View.VISIBLE)
                    collapse(ll_more);
                else
                    expand(ll_more);
            }
        });

        llClearChat.setOnClickListener(v -> {
            deleteForMe();
            collapse(ll_more);
        });

        llDeleteForBoth.setOnClickListener(v -> {
            deleteForBoth();
            collapse(ll_more);
        });

        ll_report.setOnClickListener(v -> {
            callReport();
            collapse(ll_more);
        });

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapse(ll_more);
            }
        });

        startVideoCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails() != null) {
                    if (LocalStorage.getUserDetails().getFirstName() != null && LocalStorage.getUserDetails().getProfileImageUrl() != null) {

                        //callIosNotification("audio",""+user.getUserId());
                        sendIOSCallNotificationSocket("video", user.getUserId());

                    }
                }


            }
        });
        startAudioCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails() != null) {
                    if (LocalStorage.getUserDetails().getFirstName() != null && LocalStorage.getUserDetails().getProfileImageUrl() != null) {

                        //callIosNotification("video",""+user.getUserId());
                        sendIOSCallNotificationSocket("audio", user.getUserId());

                    }
                }
            }
        });
    }


    private void sendIOSCallNotificationSocket(String calltype, Integer toId) {

        String oppositePersonName = LocalStorage.getUserDetails().getFirstName();
        String oppositePersonProfilePic = LocalStorage.getUserDetails().getProfileImageUrl() != null ? UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl() : "";

        JSONObject data = new JSONObject();
        try {
            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", toId);
            data.put("calltype", calltype);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("SocketEvent", "REQ : " + new GsonBuilder().setPrettyPrinting().create().toJson(data));

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.callNotification, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.i("SocketEvent", "sendIOSCallNotification with Ack()");
                    Log.i("SocketEvent", "CallNotification List = " + Arrays.toString(args));// print complete response
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (args[0] != null) {

                                Log.i("SocketEvent", "sendIOSCallNotification with Ack(res)");


                                /*try {
                                    JSONObject jsonObject = (JSONObject) args[1];
                                    int callID = jsonObject.getInt("call_id");

                                    if(calltype.equalsIgnoreCase("audio")){
                                        initAudioCall(user.getUserId(), oppositePersonName, oppositePersonProfilePic, "audio", callID);
                                    }else if(calltype.equalsIgnoreCase("video")){
                                        initVideoCall(user.getUserId(), oppositePersonName, oppositePersonProfilePic, "video", callID);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }*/
                            }
                        }
                    });

                }
            });
        }
    }

    private void callIosNotification(String calltype, String toId) {

        JSONObject request = new JSONObject();
        try {
            request.put("voip_token", "");
            request.put("to_id", toId);
            request.put("calltype", calltype);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Log.d("CALL_RESPONSE_REQ", new GsonBuilder().setPrettyPrinting().create().toJson(request));


        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.IOS_CALL_NOTIFICATION, response -> {
            if ("".equals(response)) {
                Utils.showAlert(SocketChatActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    Log.d("CALL_RESPONSE", new GsonBuilder().setPrettyPrinting().create().toJson(object));

                   /* if(object.has(Constants.MESSAGE)){
                        Utils.showAlert(ChatActivity.this, getString(R.string.opps_something_went_wrong));
                    }else {
                        Toast.makeText(ChatActivity.this,getString(R.string.user_reported_successfully),Toast.LENGTH_SHORT).show();
                    }*/
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SocketChatActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void initVideoCall(Integer userId, String oppositePersonName, String oppositePersonProfilePic, String callType, int callId) {
        if (userId != null) {
            db.collection("calls").document("" + userId).get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {

                            if (documentSnapshot.get("type") == "OFFER" || documentSnapshot.get("type") == "offer" ||
                                    documentSnapshot.get("type") == "ANSWER" || documentSnapshot.get("type") == "answer" ||
                                    documentSnapshot.get("type") == "END_CALL" || documentSnapshot.get("type") == "end_call") {

                                Toast.makeText(SocketChatActivity.this, "Line is Busy", Toast.LENGTH_SHORT).show();

                            } else {
                                Intent intent = new Intent(SocketChatActivity.this, RTCActivity.class);
                                intent.putExtra("meetingID", "" + userId);
                                intent.putExtra("oppositePersonName", oppositePersonName);
                                intent.putExtra("oppositePersonProfilePic", oppositePersonProfilePic);
                                intent.putExtra("isJoin", false);
                                intent.putExtra("callType", callType);
                                intent.putExtra("call_id", ""+callId);
                                startActivity(intent);
                            }

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("ChatActivity", "calling exception : " + e.getLocalizedMessage());
                        }
                    });
        }
    }

    private void initAudioCall(Integer userId, String oppositePersonName, String oppositePersonProfilePic, String callType, int callId) {
        if (userId != null) {
            db.collection("calls").document("" + userId).get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {

                            if (documentSnapshot.get("type") == "OFFER" || documentSnapshot.get("type") == "offer" ||
                                    documentSnapshot.get("type") == "ANSWER" || documentSnapshot.get("type") == "answer" ||
                                    documentSnapshot.get("type") == "END_CALL" || documentSnapshot.get("type") == "end_call") {

                                Toast.makeText(SocketChatActivity.this, "User is having call with someone else", Toast.LENGTH_SHORT).show();

                            } else {

                                String userName = user.getFirstName() + " " + user.getLastName();
                                String userImage = UrlEndpoints.MEDIA_BASE_URL + user.getProfileImageUrl();


                                Intent intent = new Intent(SocketChatActivity.this, RTCAudioActivity.class);
                                intent.putExtra("meetingID", "" + userId);
                                intent.putExtra("oppositePersonName", oppositePersonName);
                                intent.putExtra("oppositePersonProfilePic", oppositePersonProfilePic);
                                intent.putExtra("isJoin", false);
                                intent.putExtra("callType", callType);
                                intent.putExtra("userName", userName);
                                intent.putExtra("userImage", userImage);
                                intent.putExtra("call_id", ""+callId);
                                startActivity(intent);
                            }


                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("ChatActivity", "calling exception : " + e.getLocalizedMessage());
                        }
                    });
        }
    }

    private void deleteForMe() {
        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", user.getUserId());
            data.put("delete_for", 1);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.deleteAllMessages, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.d("SocketEvent", "ACK() deleteAllMessages");

                    getSocketMessages();

                }
            });

        }
    }

    private void deleteForBoth() {
        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", user.getUserId());
            data.put("delete_for", 2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.deleteAllMessages, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.d("SocketEvent", "ACK() deleteAllMessages");

                    getSocketMessages();

                }
            });

        }
    }

    private void initClickListener() {

        findViewById(R.id.select_image).setOnClickListener(this);
        findViewById(R.id.send).setOnClickListener(this);
        findViewById(R.id.cancel).setOnClickListener(this);
        findViewById(R.id.new_messages).setOnClickListener(this);
        findViewById(R.id.name).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);

        findViewById(R.id.tvButtonUnblock).setOnClickListener(this);

    }

    private void callReport() {

        JSONObject request = new JSONObject();
        try {
            request.put("report_reason", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.CHAT_REPORT + user.getUserId() + "/report";

        Webservices.getData(Webservices.Method.POST, request, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(SocketChatActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SocketChatActivity.this, getString(R.string.opps_something_went_wrong));
                    } else {
                        Toast.makeText(SocketChatActivity.this, getString(R.string.user_reported_successfully), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SocketChatActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    //set profile data
    private void setData() {

        if (user != null) {

            String fullName = user.getFirstName() + " " + user.getLastName();
            name.setText(fullName);

            Glide.with(profilePicture)
                    .asBitmap()
                    .load(UrlEndpoints.MEDIA_BASE_URL + user.getProfileImageUrl() + "?width=50&height=50")
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            profilePicture.setImageBitmap(resource);

                            Glide.with(profilePicture)
                                    .asBitmap()
                                    .load(UrlEndpoints.MEDIA_BASE_URL + user.getProfileImageUrl())
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            profilePicture.setImageBitmap(resource);
                                        }
                                    });
                        }
                    });

            socketConnectEvent();

        }
    }


    @Override
    public void isSocketConnected(boolean connected) {
        super.isSocketConnected(connected);
        if (connected) {

            Log.d("socketConnect", "" + connected);

            isScroll = true;

            /*HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getSimpleName());
            if (homeFragment != null) {
                homeFragment.socketConnectEvent();
            }*/


        }

    }

    public void socketConnectEvent() {
        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getSocketRoom();
                }
            });
        }
    }

    private void getSocketRoom() {
        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", user.getUserId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.SocketRoom, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.d("SocketEvent", "ACK() Room");

                    getSocketMessages();

                }
            });

        }
    }

    private void getSocketMessages() {

        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", user.getUserId());
            data.put("page", 2);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.GetMessages, data);
        }


    }

    private void getSocketMessagesNew() {

        //working
        //dots_progress.setVisibility(View.VISIBLE);

        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", user.getUserId());
            data.put("page", 2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.GetMessages, data);
        }

    }


    public void saveMessagesToLocal(Context context, ArrayList<Message> messages) {
        SharedPreferences mPrefs = context.getSharedPreferences("ChatData", context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(messages);
        prefsEditor.putString("" + user.getUserId(), json);
        prefsEditor.commit();
    }

    public ArrayList<Message> getAllMessages(Context context) {
        ArrayList<Message> savedMessages = new ArrayList<Message>();
        Logger.d("userId111", "" + user.getUserId());
        SharedPreferences mPrefs = context.getSharedPreferences("ChatData", context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("" + user.getUserId(), "");
        if (json.isEmpty()) {
            savedMessages = new ArrayList<Message>();
        } else {
            Type type = new TypeToken<List<Message>>() {
            }.getType();
            savedMessages = gson.fromJson(json, type);
        }

        return savedMessages;
    }

    private void getConversation(int timeInterval) {
        handler = new Handler();
        handler.postDelayed(runnable, timeInterval);
    }

    boolean isRunning = true;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (Utils.isConnected()) {
                Logger.d("handler99", "000");
                //refreshConversationNew();
                getSocketMessages();
            }
            if (isRunning) {
                handler.postDelayed(this, 2000);
            } else {
                handler.removeCallbacks(runnable);
            }

        }
    };

    //set chat list view
    private void setMessagesView(ArrayList<Message> messages) {
        if (adapter == null) {
            Logger.d("NULL11", "====111");
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setStackFromEnd(true);

            chatList.setLayoutManager(linearLayoutManager);
            //adapter = new ChatMessageAdapter(getAllMessages(this),this,this,user.getProfileImageUrl());
            adapter = new ChatMessageAdapter(messages, this, this, user.getProfileImageUrl());
            chatList.setAdapter(adapter);
            chatList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (recyclerView.getLayoutManager() != null)
                        currentScrollPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();

                    if (hasReceivedNewMessages && currentScrollPosition == (messageCount - 2)) {
                        findViewById(R.id.new_messages).setVisibility(View.GONE);
                        hasReceivedNewMessages = false;
                        adapter.fetchNewMessages(SocketChatActivity.this.messages);
                    }


                    //Call next pagination
                    /*int visibleItemCount = 0, totalItemCount = 0, pastVisibleItem = 0;

                    if (recyclerView.getLayoutManager() != null) {
                        visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                        totalItemCount = recyclerView.getLayoutManager().getItemCount();
                        pastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                    }

                    Logger.d("recyclerviewScroll","nextpage : "+ nextPage);
                    Logger.d("recyclerviewScroll","dy : "+ dy);
                    Logger.d("recyclerviewScroll","visibleItemCount : "+ visibleItemCount);
                    Logger.d("recyclerviewScroll","pastVisibleItem : "+ pastVisibleItem);
                    Logger.d("recyclerviewScroll","totalItemCount : "+ totalItemCount);
                    Logger.d("recyclerviewScroll","((visibleItemCount + pastVisibleItem) >= (totalItemCount - 2)) : " +
                            ((visibleItemCount + pastVisibleItem) >= (totalItemCount - 2)));


                    if (!isLoading && dy < 0 && pastVisibleItem == 0) {

                        Logger.d("NEXT_PAGE11","called.......");

                        isLoading = true;
                        getConversationNext();
                    }*/


                }
            });
        } else {
            Logger.d("NULL11", "====222");
            adapter.fetchNewMessages(messages);

            if (isScroll) {
                chatList.scrollToPosition(messages.size() - 1);
            }
        }

        if (messages.size() == 0) {
            tv_null.setVisibility(View.VISIBLE);
            chatList.setVisibility(View.GONE);
        } else {
            tv_null.setVisibility(View.GONE);
            chatList.setVisibility(View.VISIBLE);
        }

        if (user.getBlockedByMe()) {
            flUserBlockedView.setVisibility(View.VISIBLE);
        } else {
            flUserBlockedView.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            Logger.d("Images", mPaths.toString());
            showTextAndImage(mPaths);
        }

    }

    private void showTextAndImage(ArrayList<String> mPaths) {
        startActivity(new Intent(this, SendImageMessageActivity.class)
                .putExtra("images", mPaths)
                .putExtra("user", user));
    }


    private boolean hasPermissions(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions)
                if (ActivityCompat.checkSelfPermission(SocketChatActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == CAMERA_INTENT) {
                openImagePicker();
            }
        } else {
            Toast.makeText(SocketChatActivity.this, getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
        }
    }

    private void openImagePicker() {
        new ImagePicker.Builder(this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.select_image:
                String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

                if (!hasPermissions(PERMISSIONS)) {
                    ActivityCompat.requestPermissions(SocketChatActivity.this, PERMISSIONS, CAMERA_INTENT);
                } else {
                    openImagePicker();
                }
                break;
            case R.id.send:

                if (user.getBlockedByMe())
                    return;

                if (Utils.isConnected()) {
                    if (!"".equals(message.getText().toString())) {

                        if (isEditMessage) {
                            socketEditMessage();
                        } else if (isReplyMessage) {
                            socketReplyMessage();
                        } else {
                            socketSendMessage();
                        }

                    } else {
                        Utils.showAlert(SocketChatActivity.this, getString(R.string.please_enter_message));
                    }

                } else {
                    Utils.showAlert(SocketChatActivity.this, getString(R.string.please_check_your_internet_connection));
                }
                break;
            case R.id.new_messages:
                //setMessagesView(messages);

                //setMessagesView(false);
                findViewById(R.id.new_messages).setVisibility(View.GONE);
                hasReceivedNewMessages = false;
                messages = new ArrayList<>();
                break;
            case R.id.name:
                startActivity(new Intent(SocketChatActivity.this, ProfileActivity.class).putExtra("userId", user.getUserId()));
                break;
            case R.id.back:
                onBackPressed();
                break;
            case R.id.tvButtonUnblock:
                if (Utils.isConnected()) {
                    unBlockUser();
                } else {
                    Utils.showAlert(SocketChatActivity.this, getString(R.string.please_check_your_internet_connection));
                }
                break;
            case R.id.cancel:
                findViewById(R.id.replying_layout).setVisibility(View.GONE);
                message.setText("");
                isEditMessage = false;
                isReplyMessage = false;
                break;
            default:
                break;
        }
    }

    private void unBlockUser() {
        hud.show();
        String url = "";
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        url = UrlEndpoints.PROFILE + "/" + user.getUserId() + "/unblock";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                if (hud.isShowing()) {
                    hud.dismiss();
                }
                try {
                    JSONObject object = new JSONObject(response);
                    Logger.e("LLLLL_Res: ", response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SocketChatActivity.this, object.getString(Constants.MESSAGE));
                    }
                    if (object.getString(Constants.MESSAGE).equalsIgnoreCase("Unblock Successful")) {
                        user.setBlockedByMe(false);
                    } else {
                        user.setBlockedByMe(true);
                    }

                    //setMessagesView(false);
                    //setMessagesView(messages);

                } catch (Exception e) {
                    Utils.showAlert(SocketChatActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

/*
    @Override
    public void onBackPressed() {

        *//*if(isTaskRoot()){
            Logger.d("test111","999");
            isNotification = false;
            startActivity(new Intent(ChatActivity.this, DashboardActivity.class));
            finish();
        }else {
            Logger.d("test111","000");
            super.onBackPressed();
        }*//*

     *//*if (isNotification) {
            Logger.d("test111","999");
            isNotification = false;
            startActivity(new Intent(ChatActivity.this, DashboardActivity.class));
            finish();
        } else {
            Logger.d("test111","000");
            super.onBackPressed();
        }*//*

    }*/

    private void socketEditMessage() {
        JSONObject data = new JSONObject();
        try {
            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("message_id", messageId);
            data.put("to_id", user.getUserId());
            data.put("message", message.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.EditMessage, data, new Ack() {
                @Override
                public void call(Object... args) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SocketEvent", "edit-message with Ack()");
                            isScroll = false;
                            isEditMessage = false;
                            message.setText("");
                            getSocketMessages();
                        }
                    });
                }
            });
        }
    }

    private void socketDeleteMessage() {
        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", user.getUserId());
            data.put("message_id", messageId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.deleteMessage, data, new Ack() {
                @Override
                public void call(Object... args) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SocketEvent", "delete-message with Ack()");
                            isScroll = false;
                            getSocketMessages();
                        }
                    });
                }
            });
        }
    }

    private void socketReplyMessage() {
        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", user.getUserId());
            data.put("message_id", messageId);
            data.put("message_text", message.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.replyMessage, data, new Ack() {
                @Override
                public void call(Object... args) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SocketEvent", "reply-message with Ack()");
                            isScroll = true;
                            isReplyMessage = false;
                            findViewById(R.id.replying_layout).setVisibility(View.GONE);
                            message.setText("");
                            getSocketMessages();
                        }
                    });
                }
            });
        }
    }

    private void socketMessageLikeDislike(int messageId) {
        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("message_id", messageId);
            data.put("to_id", user.getUserId());
            data.put("like_type", "message");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.likeMessage, data, new Ack() {
                @Override
                public void call(Object... args) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SocketEvent", "like-message with Ack()");
                            isScroll = false;
                            getSocketMessages();
                        }
                    });
                }
            });
        }
    }

    private void socketSendMessage() {
        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", user.getUserId());
            data.put("message", message.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.SendMessage, data, new Ack() {
                @Override
                public void call(Object... args) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SocketEvent", "send-message with Ack()");
                            message.setText("");
                            isScroll = true;
                            getSocketMessages();
                        }
                    });


                    /*runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Log.i("SocketEvent", " == 999 == "+ Arrays.toString(args));
                                JSONObject object = (JSONObject) args[1];
                                Log.d("sendmessage1",new GsonBuilder().setPrettyPrinting().create().toJson(object));

                            }catch (Exception e){
                                e.printStackTrace();
                                Utils.showAlert(SocketChatActivity.this, getString(R.string.opps_something_went_wrong));
                            }
                        }
                    });*/


                }
            });
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        UrlEndpoints.isChatOpen = false;
        isRunning = false;
        if (handler != null)
            handler.removeCallbacks(runnable);
    }

    @Override
    protected void onPause() {
        super.onPause();
        chatEventListener = null;
        UrlEndpoints.isChatOpen = false;
    }

    //old-code
   /* @Override
    protected void onRestart() {
        super.onRestart();
        if (Utils.isConnected()) {
            getConversation(2000);
        }
    }*/

    @Override
    public void onMessageSingleClick(int messageId) {
        Logger.d("MESSAGE_CLICK", "SINGLE===" + messageId);
        //callMessageLikeDislike(messageId);
        socketMessageLikeDislike(messageId);
    }

    @Override
    public void onMessageDoubleClick(int messageId) {
        Logger.d("MESSAGE_CLICK", "DOUBLE===" + messageId);
        //callMessageLikeDislike(messageId);
        socketMessageLikeDislike(messageId);
    }

    @Override
    public void parseResponseinMain(String event, JSONObject jsonObject) {
        super.parseResponseinMain(event, jsonObject);
        Log.e("SocketEvent", "parseResponseinmain Invoked");
        switch (event) {
            case UrlEndpoints.GetMessages: {

                //working
                //dots_progress.setVisibility(View.GONE);

                Log.d("SocketEvent", "======  333  ===== SocketChatActivity");

                try {
                    if (jsonObject.has(Constants.MESSAGE)) {
                        Utils.showAlert(SocketChatActivity.this, jsonObject.getString(Constants.MESSAGE));
                    } else {
                        MessageDetails messageDetails = new Gson().fromJson(String.valueOf(jsonObject), MessageDetails.class);
                        if (messageDetails.getMessages().size() > 0) {

                            messageCount = messageDetails.getMessages().size();
                            lastMessageId = messageDetails.getMessages().get(messageCount - 1).getMessageId();

                            Logger.d("AllMessages", new GsonBuilder().setPrettyPrinting().create().toJson(messageDetails.getMessages()));

                            //saveMessagesToLocal(SocketChatActivity.this,messageDetails.getMessages());
                            //isScroll = true;
                            setMessagesView(messageDetails.getMessages());

                        } else {
                            if (messages.size() == 0) {
                                tv_null.setVisibility(View.VISIBLE);
                                chatList.setVisibility(View.GONE);
                            } else {
                                tv_null.setVisibility(View.GONE);
                                chatList.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SocketChatActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
            break;

            case UrlEndpoints.SendMessage: {
                if (UrlEndpoints.isChatOpen) {
                    isScroll = true;
                    getSocketMessages();
                }
            }
            break;

            case UrlEndpoints.EditMessage: {
                isScroll = false;
                getSocketMessages();
            }
            break;

            case UrlEndpoints.likeMessage: {
                isScroll = false;
                getSocketMessages();
            }
            break;

            case UrlEndpoints.replyMessage: {
                isScroll = true;
                getSocketMessages();
            }
            break;

            case UrlEndpoints.deleteMessage: {
                isScroll = false;
                getSocketMessages();
            }
            break;

        }
    }

    @Override
    public void onSocketEvent(String event, JSONObject payload) {
        switch (event) {
            case UrlEndpoints.GetMessages: {

                //working
                //dots_progress.setVisibility(View.GONE);

                try {
                    if (payload.has(Constants.MESSAGE)) {
                        Utils.showAlert(SocketChatActivity.this, payload.getString(Constants.MESSAGE));
                    } else {
                        MessageDetails messageDetails = new Gson().fromJson(String.valueOf(payload), MessageDetails.class);

                        if (messageDetails.getMessages().size() > 0) {

                            messageCount = messageDetails.getMessages().size();
                            lastMessageId = messageDetails.getMessages().get(messageCount - 1).getMessageId();

                            Logger.d("AllMessages", new GsonBuilder().setPrettyPrinting().create().toJson(messageDetails.getMessages()));

                            //saveMessagesToLocal(SocketChatActivity.this,messageDetails.getMessages());
                            //isScroll = true;
                            setMessagesView(messageDetails.getMessages());

                        } else {
                            if (messages.size() == 0) {
                                tv_null.setVisibility(View.VISIBLE);
                                chatList.setVisibility(View.GONE);
                            } else {
                                tv_null.setVisibility(View.GONE);
                                chatList.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                } catch (Exception e) {

                    Log.e("SocketEvent", "onSocketEvent Invoked 333");

                    e.printStackTrace();
                    Utils.showAlert(SocketChatActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
            break;

            case UrlEndpoints.SendMessage: {
                isScroll = true;
                getSocketMessages();
            }
            break;

            case UrlEndpoints.EditMessage: {
                isScroll = false;
                getSocketMessages();
            }
            break;

            case UrlEndpoints.likeMessage: {
                isScroll = false;
                getSocketMessages();
            }
            break;

            case UrlEndpoints.replyMessage: {
                isScroll = true;
                getSocketMessages();
            }
            break;

            case UrlEndpoints.deleteMessage: {
                isScroll = false;
                getSocketMessages();
            }
            break;

        }
    }

    public static class ChatActivityUserWraper {
        private static ChatActivityUserWraper instance;

        public static ChatActivityUserWraper getInstance() {
            if (instance == null)
                instance = new ChatActivityUserWraper();
            return instance;
        }

        private User user;

        public void setUser(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        public void setBlockedByMe(Integer userId, boolean blockedByMe) {
            Logger.e(ChatActivityUserWraper.class.getSimpleName(), "setBlockedByMe userId : " + userId + " blockByMe : " + blockedByMe);
            if (userId.equals(user.getUserId())) {
                Logger.e(ChatActivityUserWraper.class.getSimpleName(), "setBlockedByMe value changed ");
                user.setBlockedByMe(blockedByMe);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        UrlEndpoints.isChatOpen = true;
        chatEventListener = SocketChatActivity.this;

        if (ChatActivityUserWraper.getInstance().getUser() != null)
            user = ChatActivityUserWraper.getInstance().getUser();

        //show direct from server

        isScroll = true;

        runOnUiThread(new Runnable() {
            public void run() {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        getSocketMessages();
                    }
                }, 5000);

            }
        });


        //old-code
        /*if(getAllMessages(this)!=null && !getAllMessages(this).isEmpty()){
            setMessagesView(false);
        }else {
            getConversationNew();
        }*/

    }

    protected void notifyCounter() {
        Intent intent = new Intent(NOTIFY_COUNTER);
        localBroadcastManager.sendBroadcast(intent);
    }

    @Override
    public void onDialogDismiss(String action, int messageID, String messageText, int toId) {

        if (action.equalsIgnoreCase("edit")) {
            isEditMessage = true;
            isReplyMessage = false;
            this.messageId = messageID;
            this.toId = toId;
            message.setText(messageText);
        } else if (action.equalsIgnoreCase("delete")) {
            this.messageId = messageID;
            this.toId = toId;
            //deleteMessage();
            socketDeleteMessage();
        } else if (action.equalsIgnoreCase("reply")) {
            isReplyMessage = true;
            isEditMessage = false;
            this.messageId = messageID;
            this.toId = toId;
            if (messageText != null && !messageText.equalsIgnoreCase("")) {
                onReplyClicked(messageText);
            }

        }

    }

    @Override
    public void onReceiveMessageReply(Integer messageId, String messageText, Integer fromId) {
        isReplyMessage = true;
        isEditMessage = false;
        this.messageId = messageId;
        this.toId = fromId;
        if (messageText != null && !messageText.equalsIgnoreCase("")) {
            onReplyClicked(messageText);
        }
    }

    private void onReplyClicked(String messageText) {
        findViewById(R.id.replying_layout).setVisibility(View.VISIBLE);
        replyTo.setText(messageText);
    }


}
