package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import androidx.appcompat.widget.AppCompatSpinner;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.SpinnerAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.groups.Group;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

//Edit group details screen
public class EditGroupActivity extends BaseActivity implements View.OnClickListener {

    private EditText groupName, groupDescription;
    private TextView groupMembers;
    private AppCompatSpinner topic;
    private CircleImageView groupImage;
    private ImageView groupCover;
    private RadioButton privateGroup, publicGroup;
    private KProgressHUD hud;
    private ArrayList<String> topicList;
    private Group group;
    private File photo;
    private boolean isGroupPhotoChanged = true;
    public static final int CAMERA_INTENT_PROFILE = 100;
    public static final int CAMERA_INTENT_COVER = 101;
    private static final String IMAGE_DIRECTORY = "/Afrocamgist";
    private boolean isGroupDetailsEdited = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);
        initView();

        if (getIntent().getSerializableExtra("group")!=null)
            group = (Group) getIntent().getSerializableExtra("group");

        if (Utils.isConnected())
            getGroupDetails();
        else
            Utils.showAlert(EditGroupActivity.this, getString(R.string.no_internet_connection_available));

        initSpinnerAdapter();
        initClickListener();
    }

    private void getGroupDetails() {

        if (group!=null) {

            hud = KProgressHUD.create(EditGroupActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/json");
            headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            String url = UrlEndpoints.GROUP + "/" + group.getGroupId();

            Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
                @Override
                public void onResponse(String response) {
                    hud.dismiss();
                    try {

                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(EditGroupActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            group = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), Group.class);
                            setGroupDetails();
                        }

                        setIsGroupEditable();

                    } catch (Exception e) {
                        Utils.showAlert(EditGroupActivity.this, getString(R.string.opps_something_went_wrong));
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void setGroupDetails() {

        if (group != null) {

            String title = group.getGroupTitle();
            ((TextView)findViewById(R.id.title)).setText(title);

            groupName.setText(group.getGroupTitle());
            groupDescription.setText(group.getGroupDescription());

            Glide.with(this)
                    .load(UrlEndpoints.MEDIA_BASE_URL + group.getGroupImage())
                    .into(groupImage);

            Glide.with(this)
                    .load(UrlEndpoints.MEDIA_BASE_URL + group.getGroupCoverImage())
                    .into(groupCover);

            privateGroup.setChecked(group.getPrivate());
            publicGroup.setChecked(!group.getPrivate());

            String groupMembersCount = String.valueOf(group.getGroupMembers().size());
            groupMembers.setText(groupMembersCount);
        }
    }

    private void setIsGroupEditable() {

        if (group.getGroupAdmins().contains(LocalStorage.getUserDetails().getUserId())) {
            groupName.setClickable(true);
            groupName.setFocusable(true);
            groupDescription.setClickable(true);
            groupDescription.setFocusable(true);
            privateGroup.setClickable(true);
            privateGroup.setFocusable(true);
            publicGroup.setClickable(true);
            publicGroup.setFocusable(true);
            topic.setEnabled(true);
            groupCover.setClickable(false);
            groupCover.setFocusable(false);
            groupImage.setClickable(false);
            groupImage.setFocusable(false);
            findViewById(R.id.save).setVisibility(View.VISIBLE);
            findViewById(R.id.upload_image).setVisibility(View.VISIBLE);
            findViewById(R.id.upload_cover).setVisibility(View.VISIBLE);
        } else {
            groupName.setClickable(false);
            groupName.setFocusable(false);
            groupDescription.setClickable(false);
            groupDescription.setFocusable(false);
            privateGroup.setClickable(false);
            privateGroup.setFocusable(false);
            publicGroup.setClickable(false);
            publicGroup.setFocusable(false);
            topic.setEnabled(false);
            groupCover.setClickable(true);
            groupCover.setFocusable(true);
            groupImage.setClickable(true);
            groupImage.setFocusable(true);
            findViewById(R.id.save).setVisibility(View.GONE);
            findViewById(R.id.upload_image).setVisibility(View.GONE);
            findViewById(R.id.upload_cover).setVisibility(View.GONE);
        }
    }

    private void initView() {

        groupName = findViewById(R.id.group_name);
        groupDescription = findViewById(R.id.group_description);
        topic = findViewById(R.id.topic);
        privateGroup = findViewById(R.id.pri);
        publicGroup = findViewById(R.id.pub);
        groupImage = findViewById(R.id.group_image);
        groupCover = findViewById(R.id.group_cover);
        groupMembers = findViewById(R.id.group_members);
    }

    private void initSpinnerAdapter() {

        topicList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.topic)));
        topic.setAdapter(new SpinnerAdapter(this, topicList));

        if (group != null) {
            int selectedPosition = topicList.indexOf(group.getGroupCategory());
            topic.setSelection(selectedPosition);
        }

    }

    private void initClickListener() {

        findViewById(R.id.invite_friends).setOnClickListener(this);
        findViewById(R.id.save).setOnClickListener(this);
        findViewById(R.id.upload_image).setOnClickListener(this);
        findViewById(R.id.upload_cover).setOnClickListener(this);
        findViewById(R.id.group_cover).setOnClickListener(this);
        findViewById(R.id.group_image).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
    }

    private Boolean isValidInput() {

        if (groupName.getText().toString().isEmpty()) {
            Utils.showAlert(EditGroupActivity.this, getString(R.string.enter_group_name));
            return false;
        } else if (groupDescription.getText().toString().isEmpty()) {
            Utils.showAlert(EditGroupActivity.this, getString(R.string.enter_group_details));
            return false;
        } else {
            return true;
        }
    }

    private void editGroupDetails() {

        if (group == null) {
            Utils.showAlert(EditGroupActivity.this, getString(R.string.opps_something_went_wrong_please_try_again_later));
        } else {

            hud = KProgressHUD.create(EditGroupActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/json");
            headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            String url = UrlEndpoints.GROUP + "/" + group.getGroupId();

            Webservices.getData(Webservices.Method.PUT, createGroupJsonRequest(), headers, url, new OnTaskCompleted() {
                @Override
                public void onResponse(String response) {
                    hud.dismiss();
                    try {

                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(EditGroupActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            isGroupDetailsEdited = true;
                            Toast.makeText(EditGroupActivity.this, getString(R.string.group_details_updated_successfully), Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        Utils.showAlert(EditGroupActivity.this, getString(R.string.opps_something_went_wrong));
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private JSONObject createGroupJsonRequest() {

        JSONObject createGroupJsonObject = new JSONObject();
        try {
            createGroupJsonObject.put("group_title", groupName.getText().toString());
            createGroupJsonObject.put("group_category", topicList.get(topic.getSelectedItemPosition()));
            createGroupJsonObject.put("private", privateGroup.isChecked());
            createGroupJsonObject.put("group_description", groupDescription.getText().toString());
            return createGroupJsonObject;
        } catch (Exception e) {
            e.printStackTrace();
            return createGroupJsonObject;
        }
    }

    private boolean hasPermissions(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions)
                if (ActivityCompat.checkSelfPermission(EditGroupActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == CAMERA_INTENT_PROFILE) {
                isGroupPhotoChanged = true;
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setCropShape(CropImageView.CropShape.OVAL).start(EditGroupActivity.this);
            } else {
                isGroupPhotoChanged = false;
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start(EditGroupActivity.this);
            }
        } else {
            Toast.makeText(EditGroupActivity.this, getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (isGroupPhotoChanged) {
                    Glide.with(this)
                            .load(resultUri)
                            .apply(RequestOptions.circleCropTransform())
                            .into(groupImage);
                } else {
                    Glide.with(this)
                            .load(resultUri)
                            .into(groupCover);
                }
                createImageFile(resultUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(EditGroupActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    }

    private void createImageFile(Uri uri) {

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            File directory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);

            if (!directory.exists()) {
                directory.mkdirs();
            }

            try {
                photo = new File(directory, Calendar.getInstance().getTimeInMillis() + ".jpg");
                FileOutputStream fo = new FileOutputStream(photo);
                fo.write(bytes.toByteArray());
                fo.close();
                saveProfilePhoto();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveProfilePhoto() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE,"multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.GROUP + "/" + group.getGroupId() + "/" + (isGroupPhotoChanged ? UrlEndpoints.UPDATE_GROUP_IMAGE : UrlEndpoints.UPDATE_GROUP_COVER);

        Webservices.getData(this, Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(EditGroupActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(EditGroupActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        isGroupDetailsEdited = true;
                        Toast.makeText(EditGroupActivity.this,getString(R.string.picture_updated_successfully),Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(EditGroupActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.invite_friends:
                startActivity(new Intent(EditGroupActivity.this, InviteFriendsActivity.class).putExtra("group",group));
                break;
            case R.id.save:
                if (Utils.isConnected()) {
                    if (isValidInput()) {
                        editGroupDetails();
                    }
                } else {
                    Utils.showAlert(EditGroupActivity.this, getString(R.string.no_internet_connection_available));
                }
                break;
            case R.id.upload_image:
                String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

                if (!hasPermissions(PERMISSIONS)) {
                    ActivityCompat.requestPermissions(EditGroupActivity.this, PERMISSIONS, CAMERA_INTENT_PROFILE);
                } else {
                    isGroupPhotoChanged = true;
                    CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setCropShape(CropImageView.CropShape.OVAL).start(EditGroupActivity.this);
                }
                break;
            case R.id.upload_cover:
                String[] COVER_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

                if (!hasPermissions(COVER_PERMISSIONS)) {
                    ActivityCompat.requestPermissions(EditGroupActivity.this, COVER_PERMISSIONS, CAMERA_INTENT_COVER);
                } else {
                    isGroupPhotoChanged = false;
                    CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start(EditGroupActivity.this);
                }
                break;
            case R.id.group_cover:
                ArrayList<String> groupCovers = new ArrayList<>();
                groupCovers.add(group.getGroupCoverImage());
                startActivity(new Intent(this, ViewPostImageActivity.class)
                        .putExtra("images",groupCovers));
                break;
            case R.id.group_image:
                ArrayList<String> groupImages = new ArrayList<>();
                groupImages.add(group.getGroupImage());
                startActivity(new Intent(this, ViewPostImageActivity.class).putExtra("images",groupImages));
                break;
            case R.id.back:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isGroupDetailsEdited){
            setResult(RESULT_OK);
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
