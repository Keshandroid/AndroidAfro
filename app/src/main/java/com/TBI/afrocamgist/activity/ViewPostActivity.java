package com.TBI.afrocamgist.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.PersistableBundle;
import android.util.DisplayMetrics;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.MenuAdapter;
import com.TBI.afrocamgist.adapters.MyProfilePostAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.bottomsheet.SingleCommentBottomSheetFragment;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.SwaggerCommentBottomSheetFragment;
import com.TBI.afrocamgist.home.VideosAdapter;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.chat.ConversationDetails;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostBaseResponse;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.exoplayer.ExoPlayable;
import im.ene.toro.exoplayer.Playable;
import im.ene.toro.exoplayer.ToroExo;
import im.ene.toro.media.VolumeInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.TBI.afrocamgist.activity.SplashActivity.isNotification;
import static com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter.isMute;

public class ViewPostActivity extends BaseActivity implements MenuAdapter.OnMenuItemClickListener,SingleCommentBottomSheetFragment.DialogDismissListener {

    private CircleImageView sharedProfileImage, profilePicture;
    private TextView postText, likesCount, comment, location, sharedName, sharedLocation, sharedTime, sharedPostText;
    private TextView name, time, imageCount, postTextOnImage;
    private PlayerView player;
    private ImageView thumbnail, postImage, moreImages, backgroundImage, mapPostImage;
    private Playable playable;
    private ToggleButton like, follow, volume;
    private KProgressHUD hud;
    private Integer postId;
    private Post post;
    private PopupWindow popup;
    private CardView cv_post;

    private static int SAVED_SCROLL_POSITION = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        try {
            if (getIntent() != null && getIntent().getData() != null) {
                Uri data = getIntent().getData();
                String param1 = data.getLastPathSegment();
                postId = Integer.valueOf(param1);
                isNotification = true;
            } else {

                //postId = getIntent().getIntExtra("postId", 0);

                if(getIntent().getSerializableExtra("singlePost")!=null){
                    post = (Post) getIntent().getSerializableExtra("singlePost");
                }else {
                    postId = getIntent().getIntExtra("postId", 0);
                }
            }
        } catch (Exception e){
            Intent intent = new Intent(ViewPostActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }


        initView();
        setClickListener();

        if (Utils.isConnected()) {
            //getPostDetails();

            if (post != null) {
                Logger.d("post000", "222:" + new GsonBuilder().setPrettyPrinting().create().toJson(post));
                cv_post.setVisibility(View.VISIBLE);
                setPostData(post, false);
            } else {
                //getPostDetails();
                getPostDetailsNew();
            }

        }

        else
            Utils.showAlert(ViewPostActivity.this, getString(R.string.no_internet_connection_available));
    }

    private void initView() {

        moreImages = findViewById(R.id.more_images);
        profilePicture = findViewById(R.id.profile_picture);
        sharedProfileImage = findViewById(R.id.shared_profile_image);
        name = findViewById(R.id.name);
        time = findViewById(R.id.time);
        postText = findViewById(R.id.post_text);
        sharedName = findViewById(R.id.shared_name);
        sharedLocation = findViewById(R.id.shared_location);
        sharedTime = findViewById(R.id.shared_time);
        sharedPostText = findViewById(R.id.shared_post_text);
        postImage = findViewById(R.id.post_image);
        imageCount = findViewById(R.id.image_count);
        likesCount = findViewById(R.id.likes_count);
        comment = findViewById(R.id.comment);
        location = findViewById(R.id.location);
        like = findViewById(R.id.like);
        follow = findViewById(R.id.follow);
        thumbnail = findViewById(R.id.thumbnail);
        backgroundImage = findViewById(R.id.background_image);
        postTextOnImage = findViewById(R.id.post_text_on_image);
        mapPostImage = findViewById(R.id.post_map_image);
        player = findViewById(R.id.player);
        volume = findViewById(R.id.volume);
        cv_post = findViewById(R.id.cv_post);
    }

    private void setClickListener() {

        profilePicture.setOnClickListener(v -> {
            if ("shared".equals(post.getPostType()) && post.getSharedPost() != null) {
                if (LocalStorage.getUserDetails().getUserId().equals(post.getSharedPost().getUserId()))
                    startActivity(new Intent(ViewPostActivity.this, MyProfileActivity.class));
                else
                    startActivity(new Intent(ViewPostActivity.this, ProfileActivity.class)
                            .putExtra("userId", post.getSharedPost().getUserId()));
            } else {
                if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId()))
                    startActivity(new Intent(ViewPostActivity.this, MyProfileActivity.class));
                else
                    startActivity(new Intent(ViewPostActivity.this, ProfileActivity.class)
                            .putExtra("userId", post.getUserId()));
            }
        });

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("shared".equals(post.getPostType()) && post.getSharedPost() != null) {
                    if (LocalStorage.getUserDetails().getUserId().equals(post.getSharedPost().getUserId()))
                        startActivity(new Intent(ViewPostActivity.this, MyProfileActivity.class));
                    else
                        startActivity(new Intent(ViewPostActivity.this, ProfileActivity.class)
                                .putExtra("userId", post.getSharedPost().getUserId()));
                } else {
                    if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId()))
                        startActivity(new Intent(ViewPostActivity.this, MyProfileActivity.class));
                    else
                        startActivity(new Intent(ViewPostActivity.this, ProfileActivity.class)
                                .putExtra("userId", post.getUserId()));
                }
            }
        });

        sharedProfileImage.setOnClickListener(v -> {
            if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId()))
                startActivity(new Intent(ViewPostActivity.this, MyProfileActivity.class));
            else
                startActivity(new Intent(ViewPostActivity.this, ProfileActivity.class)
                        .putExtra("userId", post.getUserId()));
        });

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Open Comment Activity
                /*startActivityForResult(new Intent(ViewPostActivity.this, CommentsActivity.class)
                        .putExtra("comments", post.getComments())
                        .putExtra("postId", post.getPostId()), 100);*/

                // Open Comment Dialog
                //SAVED_SCROLL_POSITION = position;
                SingleCommentBottomSheetFragment singleCommentBottomSheetFragment = new SingleCommentBottomSheetFragment();
                SingleCommentBottomSheetFragment.newInstance(post.getComments(),post.getPostId(),ViewPostActivity.this).show(getSupportFragmentManager(), singleCommentBottomSheetFragment.getTag());

            }
        });

        findViewById(R.id.share).setOnClickListener(v -> startActivity(new Intent(ViewPostActivity.this, SharePostActivity.class).putExtra("post", post)));

        findViewById(R.id.social_share).setOnClickListener(v -> {

            String url = UrlEndpoints.BASE_SHARE_POST_URL + post.getPostId();

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
            i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
            i.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(i, "Share URL"));
        });

        findViewById(R.id.report).setOnClickListener(v -> startActivity(new Intent(ViewPostActivity.this, ReportPostActivity.class).putExtra("postId", post.getPostId())));

        findViewById(R.id.menu).setOnClickListener(v -> showMenu(findViewById(R.id.menu)));

        like.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (buttonView.isPressed()) {
                if (isChecked) {
                    likesCount.setTextColor(Color.parseColor("#FF7400"));

                    int counter = post.getLikeCount();
                    post.setLikeCount(counter+1);

                    String likes = (Integer.valueOf(likesCount.getText().toString()) + 1) + "";
                    likesCount.setText(likes);
                } else {
                    if (Integer.parseInt(likesCount.getText().toString().trim()) > 0){
                        likesCount.setTextColor(Color.parseColor("#004F95"));

                        int counter = post.getLikeCount();
                        post.setLikeCount(counter-1);

                        String likes = (Integer.valueOf(likesCount.getText().toString()) - 1) + "";
                        likesCount.setText(likes);
                    }
                }

                post.setLiked(isChecked);
                likeAndUnlikePost(post.getPostId());
            }
        });

        follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && buttonView.isPressed()) {
                    follow.setEnabled(false);
                    followUser(post.getUserId());
                }
            }
        });

        likesCount.setOnClickListener(v -> {

            if (!"0".equals(likesCount.getText().toString())) {
                startActivity(new Intent(ViewPostActivity.this, PostLikedByActivity.class)
                        .putExtra("postId", post.getPostId()));
            }
        });

        findViewById(R.id.more_images_layout).setOnClickListener(v -> {

            ArrayList<String> images;

            if ("shared".equals(post.getPostType()))
                images = post.getSharedPost().getPostImage();
            else
                images = post.getPostImage();

            startActivity(new Intent(ViewPostActivity.this, ViewPostImageActivity.class)
                    .putExtra("position", 1)
                    .putExtra("images", images));
        });

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());

        volume.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                unMute();
            else
                mute();
        });
    }

    private void showMenu(View menu) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int popupWidth = 450;
        int popupHeight = LinearLayout.LayoutParams.WRAP_CONTENT;

        LinearLayout viewGroup = findViewById(R.id.menu_layout);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.drop_down_menu, viewGroup);
        popupMenu(layout);

        popup = new PopupWindow(layout, popupWidth, popupHeight);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        popup.showAsDropDown(menu);
    }

    private void popupMenu(View layout) {

        RecyclerView menu = layout.findViewById(R.id.menuList);
        menu.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        menu.setLayoutManager(mLayoutManager);
        MenuAdapter adapter;
        if (post.getMapPost())
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.post_map_menu))), 0, this);
        else
            adapter = new MenuAdapter(new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.post_menu))), 0, this);
        menu.setAdapter(adapter);
    }



    private void getPostDetails() {

        hud = KProgressHUD.create(ViewPostActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            hud.dismiss();
            try {
                Logger.e("LLLLL_JSON: ",response);
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(ViewPostActivity.this, getString(R.string.sorry_post_deleted));
                    cv_post.setVisibility(View.GONE);
                } else {
                    cv_post.setVisibility(View.VISIBLE);
                    post = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), Post.class);

                    setPostData(post, false);
                }

            } catch (Exception e) {
                Utils.showAlert(ViewPostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private void getPostDetailsNew(){

        hud = KProgressHUD.create(ViewPostActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Call<PostBaseResponse> call = BaseServices.getAPI().create(ParameterServices.class).getPostDetail(postId);
        call.enqueue(new Callback<PostBaseResponse>() {
            @Override
            public void onResponse(@NotNull Call<PostBaseResponse> call, @NotNull Response<PostBaseResponse> response) {
                hud.dismiss();
                Logger.d("PostDetails", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
                PostBaseResponse postBaseResponse = response.body();
                if(response.isSuccessful()){
                    if(postBaseResponse!=null){
                        if(postBaseResponse.getPostData()!=null){
                            cv_post.setVisibility(View.VISIBLE);
                            post = postBaseResponse.getPostData();
                            setPostData(postBaseResponse.getPostData(), false);
                        }else {
                            Utils.showAlert(ViewPostActivity.this, getString(R.string.sorry_post_deleted));
                            cv_post.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<PostBaseResponse> call, @NotNull Throwable t) {
                hud.dismiss();
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }


    private void setPostData(Post post, Boolean isSharedPost) {

        if ("shared".equals(post.getPostType())) {
            setSharedPostData(post);
            setPostData(post.getSharedPost(), true);
            follow.setVisibility(View.GONE);

        } else {
            String fullName = post.getFirstName() + " " + post.getLastName();
            name.setText(fullName);
            time.setText(Utils.getSocialStyleTime(post.getPostDate()));

            if (post.getPostLatLng()!=null && !"".equals(post.getPostLatLng())) {
                location.setVisibility(View.VISIBLE);
                location.setText(post.getPostLocation());
            } else {
                location.setVisibility(View.GONE);
            }

            if (!isSharedPost) {
                if (post.getLiked()) {
                    like.setChecked(true);
                    likesCount.setTextColor(Color.parseColor("#FF7400"));
                } else {
                    like.setChecked(false);
                    likesCount.setTextColor(Color.parseColor("#004F95"));
                }

                if (post.getFollowing()!=null) {
                    if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
                        follow.setVisibility(View.GONE);
                    } else {
                        follow.setVisibility(View.VISIBLE);
                        follow.setChecked(post.getFollowing());
                        follow.setEnabled(!post.getFollowing());
                    }
                }

                if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
                    findViewById(R.id.menu).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.menu).setVisibility(View.GONE);
                }
                String likeCount = post.getLikeCount() + "";
                likesCount.setText(likeCount);
                String commentCount = post.getCommentCount() + "";
                comment.setText(commentCount);
            }

            postText.setText(post.getPostText());
            Glide.with(getApplicationContext())
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                    .into(profilePicture);
            setPostDetails(post);
        }
    }

    private void setSharedPostData(Post post) {

        findViewById(R.id.shared_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.share).setVisibility(View.GONE); //Cannot share a shared post (same in website)

        String fullName = post.getFirstName() + " " + post.getLastName();
        sharedName.setText(fullName);
        sharedTime.setText(Utils.getSocialStyleTime(post.getPostDate()));
        sharedPostText.setText(post.getPostText());

        if (post.getPostLatLng()!=null && !"".equals(post.getPostLatLng())) {
            sharedLocation.setVisibility(View.VISIBLE);
            sharedLocation.setText(post.getPostLocation());
        } else {
            sharedLocation.setVisibility(View.GONE);
        }

        if (post.getLiked()) {
            like.setChecked(true);
            likesCount.setTextColor(Color.parseColor("#FF7400"));
        } else {
            like.setChecked(false);
            likesCount.setTextColor(Color.parseColor("#004F95"));
        }

        if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
            findViewById(R.id.menu).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.menu).setVisibility(View.GONE);
        }
        String likeCount = post.getLikeCount() + "";
        likesCount.setText(likeCount);
        String commentCount = post.getCommentCount() + "";
        comment.setText(commentCount);

        Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(sharedProfileImage);
    }

    private void setPostDetails(Post post) {

        switch (post.getPostType()) {

            case "text":
                findViewById(R.id.layout_only_images).setVisibility(View.GONE);
                findViewById(R.id.video_layout).setVisibility(View.GONE);

                if (post.getBackgroundImagePost()) {

                    findViewById(R.id.text_with_image_background).setVisibility(View.VISIBLE);

                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getBackgroundImage())
                            .into(backgroundImage);

                    postTextOnImage.setText(post.getPostText());
                    postText.setText("");
                } else {
                    findViewById(R.id.text_with_image_background).setVisibility(View.GONE);
                }

                break;
            case "image":
                findViewById(R.id.layout_only_images).setVisibility(View.VISIBLE);
                findViewById(R.id.video_layout).setVisibility(View.GONE);
                findViewById(R.id.text_with_image_background).setVisibility(View.GONE);

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.placeholder);

                if (post.getPostImage().size() > 1) {
                    String count = (post.getPostImage().size() - 1) + "";

                    Glide.with(postImage)
                            .asBitmap()
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0) + "?width=50&height=50")
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    postImage.setImageBitmap(resource);

                                    Glide.with(postImage)
                                            .asBitmap()
                                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                            .into(new SimpleTarget<Bitmap>() {
                                                @Override
                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                    postImage.setImageBitmap(resource);
                                                }
                                            });
                                }
                            });

                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(1))
                            .into(moreImages);

                    findViewById(R.id.more_images_layout).setVisibility(View.VISIBLE);
                    imageCount.setText(count);
                } else {
                    if (post.getMapPost()) {

                        mapPostImage.setVisibility(View.VISIBLE);
                        postImage.setVisibility(View.GONE);

                        Glide.with(this)
//                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0) + "?width=300&height=300")
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                .into(mapPostImage);
                    } else {

                        mapPostImage.setVisibility(View.GONE);
                        postImage.setVisibility(View.VISIBLE);

                        Glide.with(postImage)
                                .asBitmap()
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0) + "?width=50&height=50")
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                        postImage.setImageBitmap(resource);

                                        Glide.with(postImage)
                                                .asBitmap()
                                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                                .into(new SimpleTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        postImage.setImageBitmap(resource);
                                                    }
                                                });
                                    }
                                });
                    }

                    findViewById(R.id.more_images_layout).setVisibility(View.GONE);
                }

                Zoomy.Builder builder = new Zoomy.Builder(this)
                        .target(postImage)
                        .tapListener(v -> startActivity(new Intent(ViewPostActivity.this, ViewPostImageActivity.class)
                                .putExtra("position", 0)
                                .putExtra("images", post.getPostImage())))
                        .enableImmersiveMode(false);

                builder.register();

                break;
            case "video":
                findViewById(R.id.layout_only_images).setVisibility(View.GONE);
                findViewById(R.id.video_layout).setVisibility(View.VISIBLE);
                findViewById(R.id.text_with_image_background).setVisibility(View.GONE);

                Glide.with(this)
                        .load(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail())
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .into(thumbnail);

                playVideo(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo()));

                /*Glide.with(this)
                        .load(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail())
                        .into(thumbnail);*/
                break;
            default:
                break;
        }
    }

    private void playVideo(Uri mediaUri) {

        playable = new ExoPlayable(ToroExo.with(this).getDefaultCreator(), mediaUri, null);
        playable.prepare(true);
        playable.setPlayerView(player);
        player.getPlayer().setRepeatMode(Player.REPEAT_MODE_ALL);
        if (isMute) {
            mute();
            volume.setChecked(false);
        } else {
            unMute();
            volume.setChecked(true);
        }
        if (!playable.isPlaying()) {
            playable.play();
            new Handler().postDelayed(() -> findViewById(R.id.thumbnail_layout).setVisibility(View.GONE), 1000);
        }
    }

    private void mute() {
        isMute = true;
        if(this.playable != null){
            this.playable.setVolumeInfo(new VolumeInfo(true, 0));
        }

    }

    private void unMute() {
        isMute = false;
        if(this.playable!=null){
            this.playable.setVolumeInfo(new VolumeInfo(false, 1));
        }

    }

    /*private void setVolume(int amount) {
        final int max = 100;
        final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
        final float volume = (float) (1 - (numerator / Math.log(max)));

        this.playable.setVolumeInfo(new VolumeInfo(true,0));
    }*/

    private void likeAndUnlikePost(Integer postId) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id", postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(ViewPostActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ViewPostActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(ViewPostActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {});
    }

    private void showConfirmDeletePostAlert(Post post) {

        Dialog popup = new Dialog(ViewPostActivity.this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(v -> {
            popup.dismiss();
            if (Utils.isConnected())
                deletePost(post.getPostId());
        });

        popup.findViewById(R.id.cancel).setOnClickListener(v -> popup.dismiss());
    }

    private void deletePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(ViewPostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(ViewPostActivity.this, getString(R.string.post_deleted_successfully), Toast.LENGTH_LONG).show();
                    finish();
                }

            } catch (Exception e) {
                Utils.showAlert(ViewPostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (playable != null) {
            playable.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (playable != null) {
            playable.setPlayerView(null);
            playable.release();
            playable = null;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Utils.isConnected()){
           //getPostDetails();

            if(post!=null){
                Logger.d("post000","111:"+ new GsonBuilder().setPrettyPrinting().create().toJson(post));
                cv_post.setVisibility(View.VISIBLE);
                setPostData(post, false);
            }else {
                //getPostDetails();
                getPostDetailsNew();
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (playable != null)
            playable.setPlayerView(null);

        super.onBackPressed();

        /*if (isTaskRoot()){
            startActivity(new Intent(ViewPostActivity.this, DashboardActivity.class));
            finish();
        } else {
            super.onBackPressed();
        }*/

        /*if (isNotification){
            startActivity(new Intent(ViewPostActivity.this, DashboardActivity.class));
            finish();
        } else {
            super.onBackPressed();
        }*/

    }

    @Override
    public void onMenuItemClicked(String item, int position) {
        if (popup != null && popup.isShowing())
            popup.dismiss();

        switch (item.toLowerCase()) {

            case "edit":
                onEditPost(post);
                break;
            case "delete":
                onDeletePost();
                break;
            default:
                break;
        }
    }

    private void onEditPost(Post post){
        startActivityForResult(new Intent(this, EditPostActivity.class)
                .putExtra("post", post), 102);
    }

    private void onDeletePost(){
        showConfirmDeletePostAlert(post);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Logger.d("commentReturn", "success");

        if (requestCode == 102 && resultCode == RESULT_OK) {
            if (data.getStringExtra("postText")!=null) {
                updatePostText(data.getStringExtra("postText"));
            }
        }else if (requestCode == 100 && resultCode == RESULT_OK) {

           /* if (data.getSerializableExtra("comments")!=null) {
                updateCommentCount((ArrayList<Comment>) data.getSerializableExtra("comments"));
            }*/
        }
    }

    private void updateCommentCount(ArrayList<Comment> comments) {

        if (comments.size() > 0) {
            int commentCount = 0;
            for(Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments()!=null && comment.getSubComments().size() > 0){
                    commentCount += comment.getSubComments().size();
                }
            }

            post.setComments(comments);
            post.setCommentCount(commentCount);

            comment.setText(String.valueOf(commentCount));

        }
    }

    private void updatePostText(String postText) {
        Logger.d("PostText1",""+postText);
        post.setPostText(postText);
    }

    @Override
    public void onDialogDismiss(ArrayList<Comment> comments) {
        updateCommentCount(comments);
    }
}


