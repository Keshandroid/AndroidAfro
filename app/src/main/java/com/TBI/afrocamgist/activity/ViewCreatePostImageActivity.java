package com.TBI.afrocamgist.activity;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.viewpager.widget.ViewPager;

import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.PostImageAdapter;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ooo.oxo.library.widget.PullBackLayout;

public class ViewCreatePostImageActivity extends BaseActivity implements PullBackLayout.Callback {

    private ArrayList<String> images = new ArrayList<>();
    private int position;
    private String imageType="";
    private boolean isPullCanceled = false;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_create_post_image);

        if (getIntent().getSerializableExtra("images")!=null) {
            images = (ArrayList<String>) getIntent().getSerializableExtra("images");
        }

        position = getIntent().getIntExtra("position",0);
        imageType = getIntent().getStringExtra("imageType");

        Logger.d("images111",""+images.get(0) + "pos : " + position);

        initImageList();
        setClickListener();
    }

    private void initImageList() {
        /*ViewPager imageList = findViewById(R.id.image_list);
        imageList.setAdapter(new PostImageAdapter(this, images));
        imageList.setCurrentItem(position);*/

        image = findViewById(R.id.image);


        if(imageType.equalsIgnoreCase("normal")){
            image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }else {
            image.setScaleType(ImageView.ScaleType.CENTER_CROP); // to display crop image
        }

        Glide.with(this)
                .load(images.get(0))
                .into(image);
    }

    private void setClickListener() {

        findViewById(R.id.close).setOnClickListener(v -> onBackPressed());
        ((PullBackLayout)findViewById(R.id.pull)).setCallback(this);
    }

    @Override
    public void onPullStart() {
        isPullCanceled = false;
    }

    @Override
    public void onPull(float v) {
        /*if(isPullCanceled){
            findViewById(R.id.gallery_layout).setBackgroundColor(Color.parseColor("#ff000000"));
        }else {
            findViewById(R.id.gallery_layout).setBackgroundColor(Color.parseColor("#BC000000"));
        }*/
    }

    @Override
    public void onPullCancel() {
        isPullCanceled = true;
    }

    @Override
    public void onPullComplete() {
        supportFinishAfterTransition();
        overridePendingTransition(0,0);
    }
}
