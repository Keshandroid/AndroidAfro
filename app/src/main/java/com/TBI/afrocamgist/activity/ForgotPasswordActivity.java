package com.TBI.afrocamgist.activity;

import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.google.gson.GsonBuilder;
import com.hbb20.CountryCodePicker;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends BaseActivity {

    private EditText email;
    private KProgressHUD hud;
    private CountryCodePicker countryCodePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initView();
        setClickListener();
    }

    private void initView() {
        email = findViewById(R.id.email);
        countryCodePicker = findViewById(R.id.countryCodePicker);
    }

    private void setClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.requestOTP).setOnClickListener(v -> {

            appendLog("======================BIGIN======================" );
            appendLog("==========send OTP clicked=====");

            if (email.getText().toString().trim().equals("")) {
                appendLog("==========    email is empty   =====");

                Utils.showAlert(ForgotPasswordActivity.this,getString(R.string.please_enter_email_or_contact_number));
            } else {
                if (Utils.isConnected())
                    sendOTP();
                else
                    Utils.showAlert(ForgotPasswordActivity.this, getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void sendOTP() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Log.d("sendOTP","REQUEST : "+new GsonBuilder().setPrettyPrinting().create().toJson(createJsonRequest()));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE,"application/json");

        appendLog("==========    Send OTP api call   =====");
        appendLog("request parameters : " + new GsonBuilder().setPrettyPrinting().create().toJson(createJsonRequest()));

        Webservices.getData(Webservices.Method.POST,createJsonRequest(),headers, UrlEndpoints.REQUEST_PASSWORD_RESET, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(ForgotPasswordActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    Log.d("sendOTP","RESPONSE : "+new GsonBuilder().setPrettyPrinting().create().toJson(object));


                    appendLog("response : " + object.toString());

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ForgotPasswordActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        String newUserName;
                        if (Patterns.PHONE.matcher(email.getText().toString()).matches()){
                            /*newUserName = email.getText().charAt(0) == '0'
                                    ? email.getText().toString().substring(1) : email.getText().toString();*/

                            newUserName = countryCodePicker.getSelectedCountryCode() + (email.getText().charAt(0) == '0'
                                    ? email.getText().toString().substring(1) : email.getText().toString());

                        } else{
                            newUserName = email.getText().toString().trim();
                        }

                        startActivity(new Intent(ForgotPasswordActivity.this, ResetPasswordActivity.class)
                            .putExtra("username",newUserName));
                    }

                } catch (Exception e) {
                    appendLog("" + e.toString());
                    e.printStackTrace();
                    Utils.showAlert(ForgotPasswordActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private JSONObject createJsonRequest() {
        JSONObject object = new JSONObject();
        try {
            String newUserName;
            if (Patterns.PHONE.matcher(email.getText().toString()).matches()){
                /*newUserName = Utils.getCurrentCountryCode(this) + (email.getText().charAt(0) == '0'
                        ? email.getText().toString().substring(1) : email.getText().toString());*/

                newUserName = countryCodePicker.getSelectedCountryCode() + (email.getText().charAt(0) == '0'
                        ? email.getText().toString().substring(1) : email.getText().toString());

                Log.d("sendOTP"," 111 : " +newUserName);


            } else{

                newUserName = email.getText().toString().trim();

                Log.d("sendOTP"," 222 : " +newUserName);

            }

            object.put("username",newUserName);
            return object;
        } catch (Exception e){
            e.printStackTrace();
            return object;
        }
    }
}
