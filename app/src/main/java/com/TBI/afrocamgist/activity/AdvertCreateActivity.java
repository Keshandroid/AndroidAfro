package com.TBI.afrocamgist.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.AdvertListAdapter;
import com.TBI.afrocamgist.adapters.EntertainmentAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.advert.Advert;
import com.TBI.afrocamgist.model.advert.AdvertPost;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdvertCreateActivity extends BaseActivity implements AdvertListAdapter.OnAdvertPostClickListener{

    Activity activity = this;
    private int nextPage = 1;
    private ChooseImageForAPostRecyclerAdapter adapter;
    private RecyclerView recycler_choose_image_for_post,advert_recyclerview;
    private ArrayList<Post> posts = new ArrayList<>();
    private String postId = "";
    private AdvertListAdapter advertListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert_create);


        /*Get advert  (uncomment this to get e-health plans and Un-Hide the xml main layout & Recyclerview)*/
        findViews();
        //getUserFirstPagePosts();
        initClickListener();
        //getAdverts();
    }

    private void findViews() {
        advert_recyclerview = findViewById(R.id.advert_recyclerview);
    }

    private void initClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
        findViewById(R.id.choose_a_post).setOnClickListener(v -> {
            //getUserFirstPagePosts();

            displayChooseAPostPopUp(posts);
        });
    }

    private void getUserFirstPagePosts() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/posts?page=" + nextPage;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if ("".equals(response)) {
                //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AdvertCreateActivity.this, object.getString(Constants.MESSAGE));
                    } else {

                        posts.clear();

                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);
                        if (postDetails.getPosts().size() > 0) {
                            Logger.d("myProfilePostSize",postDetails.getPosts().size()+"");
                            posts.addAll(postDetails.getPosts());
                            adapter.notifyDataSetChanged();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void getAdverts(){
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ADVERT_GET;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if ("".equals(response)) {
                //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AdvertCreateActivity.this, object.getString(Constants.MESSAGE));
                    } else {

                        Logger.d("GetAdvert",new GsonBuilder().setPrettyPrinting().create().toJson(response));

                        AdvertPost advertPost = new Gson().fromJson(response, AdvertPost.class);
                        if (advertPost.getPosts().size() > 0) {
                            setAdvertRecyclerView(advertPost.getPosts());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void displayChooseAPostPopUp(ArrayList<Post> posts) {

        if(posts!=null){
            final Dialog popup = new Dialog(this, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_choose_a_post);
            popup.show();
            ImageView img_preview = popup.findViewById(R.id.img_preview);
            recycler_choose_image_for_post = popup.findViewById(R.id.recycler_choose_image_for_post);
            recycler_choose_image_for_post.setLayoutManager(new GridLayoutManager(this, 3/*, RecyclerView.VERTICAL, false*/));

            adapter = new ChooseImageForAPostRecyclerAdapter(posts, new ChooseImageForAPostRecyclerAdapterImageClickListener() {
                @Override
                public void onImageClick(String imgUrl, String postID) {

                    Logger.d("imgURL",""+imgUrl);

                    postId = postID;

                    Glide.with(activity)
                            .load(UrlEndpoints.MEDIA_BASE_URL + imgUrl)
                            .into(img_preview);
                }
            });
            recycler_choose_image_for_post.setAdapter(adapter);

            popup.findViewById(R.id.img_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();

                    LocalStorage.setAdvertPostId(postId);

                    startActivity(new Intent(activity, AdvertSelectGoalActivity.class));
                }
            });
        }

    }

    @Override
    public void onViewPostClicked(Advert post) {

    }

    @Override
    public void onPostImageClicked(Advert advertPost) {
        startActivity(new Intent(AdvertCreateActivity.this, AdvertDetailActivity.class)
                .putExtra("advertPost", advertPost));
    }

    public class ChooseImageForAPostRecyclerAdapter extends RecyclerView.Adapter<ChooseImageForAPostRecyclerAdapter.DataObjectHolder> {

        private ArrayList<Post> imageList;
        private ChooseImageForAPostRecyclerAdapterImageClickListener listener;

        public ChooseImageForAPostRecyclerAdapter(ArrayList<Post> afroInterests, ChooseImageForAPostRecyclerAdapterImageClickListener listener) {
            this.imageList = afroInterests;
            this.listener = listener;
        }

        @NonNull
        @Override
        public ChooseImageForAPostRecyclerAdapter.DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_choose_image, viewGroup, false);

            return new ChooseImageForAPostRecyclerAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

            //check for image
            setPostData(imageList.get(position), holder);

            holder.background_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    switch (imageList.get(position).getPostType()) {
                        case "image":
                            if (imageList.get(position).getPostImage().size() > 1) {
                                listener.onImageClick(imageList.get(position).getPostImage().get(0),imageList.get(position).getPostId().toString());

                            }else {
                                if (imageList.get(position).getMapPost()) {
                                    listener.onImageClick(imageList.get(position).getPostImage().get(0),imageList.get(position).getPostId().toString());
                                }else {
                                    listener.onImageClick(imageList.get(position).getPostImage().get(0),imageList.get(position).getPostId().toString());
                                }

                            }

                            break;
                        case "video":
                            if(imageList.get(position).getThumbnail()!=null && !imageList.get(position).getThumbnail().isEmpty()) {
                                listener.onImageClick(imageList.get(position).getThumbnail(),imageList.get(position).getPostId().toString());
                            }
                            break;
                        default:
                            break;

                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return imageList.size();
        }


        class DataObjectHolder extends RecyclerView.ViewHolder {
            ImageView background_image;

            DataObjectHolder(View itemView) {
                super(itemView);
                background_image = itemView.findViewById(R.id.background_image);
            }
        }

        public void setPostData(Post post, DataObjectHolder holder){
            switch (post.getPostType()) {

                case "image":
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.placeholder);

                    if (post.getPostImage().size() > 1) {
                        Glide.with(holder.background_image)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                .placeholder(R.drawable.placeholder)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(holder.background_image);

                    } else {

                        if (post.getMapPost()) {

                            Glide.with(activity)
                                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                    .apply(requestOptions)
                                    .into(holder.background_image);
                        } else {
                            Glide.with(holder.background_image)
                                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                                    .placeholder(R.drawable.placeholder)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(holder.background_image);
                        }
                    }

                    break;
                case "video":
                    if(post.getThumbnail()!=null && !post.getThumbnail().isEmpty()){
                        Glide.with(holder.background_image)
                                .load(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail())
                                .placeholder(R.drawable.placeholder)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(holder.background_image);
                    }
                    break;
                default:
                    break;
            }
        }

    }

    private void setAdvertRecyclerView(ArrayList<Advert> advertList) {
        advert_recyclerview.setLayoutManager(new GridLayoutManager(this,2));
        advertListAdapter = new AdvertListAdapter(advertList, this,this);
        advert_recyclerview.setAdapter(advertListAdapter);
        advert_recyclerview.setNestedScrollingEnabled(false);

    }

    public interface ChooseImageForAPostRecyclerAdapterImageClickListener {
        void onImageClick(String imgUrl, String postID);
    }

}