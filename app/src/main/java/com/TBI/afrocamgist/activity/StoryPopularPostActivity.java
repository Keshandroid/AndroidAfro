package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.app.AfrocamgistApplication;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.CommentBottomSheetFragment;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.post.Post;
import com.bolaware.viewstimerstory.MainStory.StoryMomentz;
import com.bolaware.viewstimerstory.Momentz;
import com.bolaware.viewstimerstory.MomentzCallback;
import com.bolaware.viewstimerstory.MomentzView;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoryPopularPostActivity extends BaseActivity implements MomentzCallback,CommentBottomSheetFragment.DialogDismissListener {

    private Momentz objMomentz;
    private String postType;

    private ArrayList<Post> popularPosts = new ArrayList<>();
    private static int SAVED_SCROLL_POSITION = -1;

    //Views
    private SimpleExoPlayer simpleExoPlayer;
    private ImageView storyImage,share,socialShare,report;
    private PlayerView playerView;
    private TextView txtUserName,likesCount,comment,follow,postText;
    private CircleImageView profile_picture;
    private ToggleButton like;
    private RelativeLayout relProfile,relControllers;
    private int position;

    //Precache videos
    CacheDataSourceFactory cacheDataSourceFactory = null;
    SimpleCache simpleCache = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_popular_post);

        if (getIntent().getSerializableExtra("posts")!=null)
            popularPosts = (ArrayList<Post>) getIntent().getSerializableExtra("posts");

        position = getIntent().getIntExtra("postPosition",0);

        ViewGroup container = (ViewGroup) findViewById(R.id.containerStory);

/*
        // show a textview
        TextView textView = new TextView(this);
        textView.setText("Hello, You can display TextViews");
        textView.setTextSize(20f);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.parseColor("#000000"));

        //show a customView
        View customView = LayoutInflater.from(this).inflate(R.layout.story_custom_view, null);

        // show an imageview be loaded from file
        ImageView locallyLoadedImageView = new  ImageView(this);
        locallyLoadedImageView.setImageDrawable(
                ContextCompat.getDrawable(
                        this,
                        R.drawable.travel_cover
                )
        );



        //image to be loaded from the internet
        ImageView internetLoadedImageView = new ImageView(this);

        //video to be loaded from the internet
        VideoView internetLoadedVideo = new VideoView(this);

        List<MomentzView> listOfViews = new ArrayList<>();

        MomentzView momentzView = new MomentzView(textView,5);
        MomentzView momentzView2 = new MomentzView(customView,5);
        MomentzView momentzView3 = new MomentzView(locallyLoadedImageView,6);
        MomentzView momentzView4 = new MomentzView(internetLoadedImageView,10);
        MomentzView momentzView5 = new MomentzView(internetLoadedVideo,60);
        listOfViews.add(momentzView);
        listOfViews.add(momentzView2);
        listOfViews.add(momentzView3);
        listOfViews.add(momentzView4);
        listOfViews.add(momentzView5);


 */

        /*List<MomentzView> listOfViews = new ArrayList<>();
        for (int i=0;i<popularPosts.size();i++){
            if(popularPosts.get(i).getPostType().equalsIgnoreCase("image")) {
                ImageView internetLoadedImageView = new ImageView(this);
                MomentzView image = new MomentzView(internetLoadedImageView,10);
                listOfViews.add(image);
            }else if (popularPosts.get(i).getPostType().equalsIgnoreCase("video")){
                PlayerView internetLoadedVideo = new PlayerView(this);
                MomentzView video = new MomentzView(internetLoadedVideo,60);
                listOfViews.add(video);
            }
        }

        new Momentz(StoryPopularPostActivity.this,listOfViews,container,this).start();*/


        List<MomentzView> listOfViews = new ArrayList<>();
        for (int i=0;i<popularPosts.size();i++){
            View customView = LayoutInflater.from(this).inflate(R.layout.story_custom_view, null);
            MomentzView momentzView = new MomentzView(customView,8);
            listOfViews.add(momentzView);
        }

        new Momentz(StoryPopularPostActivity.this,listOfViews,container,this, position).start();


    }

    @Override
    public void done(boolean finishActivity) {
        //pauseExoplayerVideo();
        pausePlayer();
        if(finishActivity){
            finish();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            objMomentz.pause(true);
        }catch (IndexOutOfBoundsException e){
            Logger.d("IndexOutOfBound===",""+e.getLocalizedMessage());
        }
        //pauseExoplayerVideo();
        //pausePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Logger.d("onDestroy","destroyed....");
        //pauseExoplayerVideo();
        pausePlayer();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(objMomentz!=null){
            objMomentz.resume();
        }
        if(postType!=null){
            Logger.d("POST_TYPE1",""+postType);

            if(postType.equalsIgnoreCase("video")){
                startPlayer();
            }
        }
    }

    @Override
    public void onNextCalled(@NotNull View view, @NotNull Momentz momentz, int index) {
        /*pauseExoplayerVideo();
        if (view instanceof PlayerView) {
            momentz.pause(true);
            //playVideo((VideoView) view, index, momentz);
            initializePlayer((PlayerView) view, index, momentz);
        } else if ((view instanceof ImageView) && (((ImageView) view).getDrawable() == null)) {
            momentz.pause(true);
            Picasso.get()
                    .load(UrlEndpoints.MEDIA_BASE_URL + popularPosts.get(index).getPostImage().get(0))
                    .into((ImageView) view, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            //do smth when picture is loaded successfully
                            momentz.resume();
                            //Toast.makeText(StoryPopularPostActivity.this,"Image loaded from the internet",Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(Exception ex) {
                            //do smth when there is picture loading error
                            Toast.makeText(StoryPopularPostActivity.this,ex.getLocalizedMessage()+"",Toast.LENGTH_LONG).show();
                        }
                    });

        }*/

        this.objMomentz = momentz;
        Post post = popularPosts.get(index);

        //pauseExoplayerVideo();
        pausePlayer();
        initViews(view);
        setPostData(post);
        setClickListener(post,index,momentz);

        if(post.getPostType().equalsIgnoreCase("image")){
            postType = "IMAGE";
            momentz.pause(true);
            callPopularPostViewCount(post.getPostId());

            storyImage.setVisibility(View.VISIBLE);
            playerView.setVisibility(View.GONE);
            Picasso.get()
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                    .into(storyImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            //do smth when picture is loaded successfully
                            momentz.resume();
                            relControllers.setVisibility(View.VISIBLE);
                            //Toast.makeText(StoryPopularPostActivity.this,"Image loaded from the internet",Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(Exception ex) {
                            //do smth when there is picture loading error
                            Toast.makeText(StoryPopularPostActivity.this,ex.getLocalizedMessage()+"",Toast.LENGTH_LONG).show();
                        }
                    });
        }else if(post.getPostType().equalsIgnoreCase("video")){
            postType = "VIDEO";
            momentz.pause(true);
            callPopularPostViewCount(post.getPostId());
            storyImage.setVisibility(View.GONE);
            playerView.setVisibility(View.VISIBLE);

            //initializePlayer(playerView, index, momentz); //Play video directly from URL
            initPlayer(playerView,index,momentz); // play video from catch memory
        }
    }

    private void initPlayer(PlayerView playerView, int index, Momentz momentz){
        relControllers.setVisibility(View.VISIBLE);
        simpleExoPlayer = newSimpleExoPlayer();

        String videoUrl = UrlEndpoints.MEDIA_BASE_URL + popularPosts.get(index).getPostVideo();
        String userAgent = Util.getUserAgent(this, "Afrocamgist");

        simpleCache = AfrocamgistApplication.simpleCache;
        cacheDataSourceFactory =
                new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(userAgent), CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

        MediaSource mediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(videoUrl));

        playerView.setPlayer(simpleExoPlayer);
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.seekTo(0);
        simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);
        simpleExoPlayer.prepare(mediaSource,true,false);

        simpleExoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                super.onPlayerStateChanged(playWhenReady, playbackState);
                if (playWhenReady && playbackState == Player.STATE_READY) {
                    momentz.editDurationAndResume(index, (int) ((simpleExoPlayer.getDuration()) / 1000));
                } else if (playWhenReady) {
                } else {
                }
            }
        });

    }

    private SimpleExoPlayer newSimpleExoPlayer() {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
        return ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
    }

    private void callPopularPostViewCount(int postId) {

        Logger.d("postId99",""+postId);

        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getPopularPostViewCount(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("postViewCount9", new GsonBuilder().setPrettyPrinting().create().toJson(jsonObject));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    private void initViews(View view) {
        storyImage = view.findViewById(R.id.storyImage);
        playerView = view.findViewById(R.id.player);
        txtUserName = view.findViewById(R.id.txtUserName);
        profile_picture = view.findViewById(R.id.profile_picture);
        like = view.findViewById(R.id.like);
        likesCount = view.findViewById(R.id.likes_count);
        comment = view.findViewById(R.id.comment);
        share = view.findViewById(R.id.share);
        socialShare = view.findViewById(R.id.social_share);
        report = view.findViewById(R.id.report);
        follow = view.findViewById(R.id.follow);
        postText = view.findViewById(R.id.post_text);
        relProfile = view.findViewById(R.id.relProfile);
        relControllers = view.findViewById(R.id.relControllers);
    }

    private void setPostData(Post post) {

        if(post.getUser_name()!=null && !post.getUser_name().isEmpty()){
            txtUserName.setText(post.getUser_name());
        }else {
            String fullName = post.getFirstName() + " " + post.getLastName();
            txtUserName.setText(fullName);
        }
        Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(profile_picture);

        if (post.getLiked()) {
            like.setChecked(true);
            likesCount.setTextColor(Color.parseColor("#FF7400"));
        } else {
            like.setChecked(false);
            likesCount.setTextColor(Color.parseColor("#ffffff"));
        }

        String likeCount = post.getLikeCount() + "";
        likesCount.setText(likeCount);
        String commentCount = post.getCommentCount() + "";
        comment.setText(commentCount);



        if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
            follow.setVisibility(View.GONE);
        } else {
            follow.setVisibility(View.VISIBLE);
            if (post != null && post.getFollowing() != null) {
                if (post.getFollowing()) {
                    follow.setTextColor(getResources().getColor(R.color.orange));
                    follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                    follow.setText(getResources().getString(R.string.following));
                    follow.setEnabled(false);
                } else {
                    follow.setTextColor(getResources().getColor(R.color.blue));
                    follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                    follow.setText(getResources().getString(R.string.follow));
                    follow.setEnabled(true);

                    if(checkIsRequested(post)){
                        follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        follow.setTextColor(getResources().getColor(R.color.colorAccent));
                        follow.setText(R.string.requested);
                        follow.setEnabled(false);
                    }

                }
            } else {
                follow.setTextColor(getResources().getColor(R.color.blue));
                follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                follow.setText(getResources().getString(R.string.follow));
                follow.setEnabled(true);
            }
        }
        postText.setText(post.getPostText());
    }

    private void setClickListener(Post post, int index, Momentz momentz) {

        like.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    if (isChecked) {
                        likesCount.setTextColor(Color.parseColor("#FF7400"));

                        int counter = post.getLikeCount();
                        post.setLikeCount(counter + 1);

                        String likeCount = (Integer.valueOf(likesCount.getText().toString()) + 1) + "";
                        likesCount.setText(likeCount);
                    } else {
                        if (Integer.valueOf(likesCount.getText().toString()) > 0) {
                            likesCount.setTextColor(Color.parseColor("#ffffff"));

                            int counter = post.getLikeCount();
                            post.setLikeCount(counter - 1);

                            String likeCount = (Integer.parseInt(likesCount.getText().toString().trim()) - 1) + "";
                            likesCount.setText(likeCount);
                        }
                    }
                }

                if (buttonView.isPressed()) {
                    post.setLiked(isChecked);
                    onPostChecked(post);
                }
            }
        });
        
        follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFollow = false;
                if(post != null && post.getFollowing() != null){
                    if(post.getFollowing()) {
                        isFollow = false;
                    }else {
                        isFollow = true;
                        if(checkIsRequested(post)){
                            isFollow = false;
                        }
                    }
                }else {
                    isFollow = true;
                }

                if (isFollow) {
                    if(post.getPrivateAccount()) {
                        follow.setTextColor(getResources().getColor(R.color.orange));
                        follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        follow.setText(getResources().getString(R.string.requested));
                        follow.setEnabled(false);
                        onFollowClicked(post);
                    }else {
                        follow.setTextColor(getResources().getColor(R.color.orange));
                        follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        follow.setText(getResources().getString(R.string.following));
                        follow.setEnabled(false);
                        onFollowClicked(post);
                    }


                }
            }
        });

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCommentClicked(post,index,momentz);
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pausePlayerToPlayAgain();
                startActivityForResult(new Intent(StoryPopularPostActivity.this, SharePostActivity.class).putExtra("post",post),100);
            }
        });

        socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = UrlEndpoints.BASE_SHARE_POST_URL + post.getPostId();
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TITLE,"Afrocamgist");
                i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                i.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(i, "Share URL"));
            }
        });

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pausePlayerToPlayAgain();
                startActivity(new Intent(StoryPopularPostActivity.this, ReportPostActivity.class).putExtra("postId",post.getPostId()));
            }
        });

        relProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pausePlayerToPlayAgain();
                if ("shared".equals(post.getPostType()) && post.getSharedPost()!=null){
                    if (LocalStorage.getUserDetails().getUserId().equals(post.getSharedPost().getUserId()))
                        startActivity(new Intent(StoryPopularPostActivity.this, MyProfileActivity.class));
                    else
                        startActivity(new Intent(StoryPopularPostActivity.this, ProfileActivity.class)
                                .putExtra("userId",post.getSharedPost().getUserId()));
                } else {
                    if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId()))
                        startActivity(new Intent(StoryPopularPostActivity.this, MyProfileActivity.class));
                    else
                        startActivity(new Intent(StoryPopularPostActivity.this, ProfileActivity.class)
                                .putExtra("userId", post.getUserId()));
                }
            }
        });
        
    }

    private void playVideo(VideoView videoView, int index, Momentz momentz) {

        Logger.d("vidURL1",""+UrlEndpoints.MEDIA_BASE_URL + popularPosts.get(index).getPostVideo());

        String str = UrlEndpoints.MEDIA_BASE_URL + popularPosts.get(index).getPostVideo();
        Uri uri = Uri.parse(str);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();

        videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    // Here the video starts
                    momentz.editDurationAndResume(index, (videoView.getDuration()) / 1000);
                    //Toast.makeText(StoryPopularPostActivity.this,"Video loaded from the internet",Toast.LENGTH_LONG).show();
                    return true;
                }
                return false;
            }
        });

    }

    private void initializePlayer(PlayerView playerView, int index, Momentz momentz){

        relControllers.setVisibility(View.VISIBLE);

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        simpleExoPlayer.setPlayWhenReady(true);
        playerView.setPlayer(simpleExoPlayer);
        playerView.hideController();

        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, Util.getUserAgent(this, "Afrocamgist"));

        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        String str = UrlEndpoints.MEDIA_BASE_URL + popularPosts.get(index).getPostVideo();
        Uri videoUri = Uri.parse(str);
        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory, extractorsFactory, null, null);
        simpleExoPlayer.prepare(videoSource);

        simpleExoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                super.onPlayerStateChanged(playWhenReady, playbackState);
                if (playWhenReady && playbackState == Player.STATE_READY) {
                    // media actually playing
                    momentz.editDurationAndResume(index, (int) ((simpleExoPlayer.getDuration()) / 1000));
                } else if (playWhenReady) {
                    // might be idle (plays after prepare()),
                    // buffering (plays when data available)
                    // or ended (plays when seek away from end)
                } else {
                    // simpleExoPlayer paused in any state
                }
            }
        });
    }




    private boolean checkIsRequested(Post profileUser) {
        if(profileUser!=null)
        {
            boolean isRequestSend = false;
            if(profileUser.getRequestButtons().get(0).getButtonText().equalsIgnoreCase("Requested")){
                isRequestSend = true;
            }
            return isRequestSend;
        }
        return false;
    }

    public void onFollowClicked(Post post) {
        followUser(post.getUserId());
        setFollowAllUser(post.getUserId(),true);
    }

    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
            }
        });
    }

    private void setFollowAllUser(int userid, boolean isfollow){
        try {
            for (int i = 0; i< popularPosts.size();i++){
                if(popularPosts.get(i).getUserId() !=null && userid == popularPosts.get(i).getUserId()){
                    /*popularPosts.get(i).setFollowing(isfollow);
                    popularPostAdapter.notifyItemChanged(i);*/

                    //edited
                    if(popularPosts.get(i).getPrivateAccount()){

                        if(isfollow){
                            popularPosts.get(i).getRequestButtons().get(0).setButtonText("Requested");
                            popularPosts.get(i).setFollowing(false);
                        }else {
                            popularPosts.get(i).getRequestButtons().get(0).setButtonText("Request");
                            popularPosts.get(i).setFollowing(false);
                        }

                    }else {
                        popularPosts.get(i).setFollowing(isfollow);
                    }
                    //edited


                }
            }

        }catch (Exception e){
        }
    }

    public void onPostChecked(Post post) {
        likeAndUnlikePost(post.getPostId());
    }

    private void likeAndUnlikePost(Integer postId) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id",postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(StoryPopularPostActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(StoryPopularPostActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(StoryPopularPostActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    public void onCommentClicked(Post post, int position, Momentz momentz) {
        try {
            objMomentz.pause(true);
        }catch (IndexOutOfBoundsException e){
            Logger.d("IndexOutOfBound===",""+e.getLocalizedMessage());
        }
        pausePlayerToPlayAgain();

        //Open Comment in DialogFragment
        SAVED_SCROLL_POSITION = position;
        CommentBottomSheetFragment commentBottomSheetFragment = new CommentBottomSheetFragment();
        CommentBottomSheetFragment.newInstance(post.getComments(),post.getPostId()).show(getSupportFragmentManager(), commentBottomSheetFragment.getTag());

        //Open Comment in Activity
        /*startActivityForResult(new Intent(this, CommentsActivity.class)
                .putExtra("comments",post.getComments())
                .putExtra("postId",post.getPostId()),101);*/
    }

    private void pausePlayer(){
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(false);
            simpleExoPlayer.getPlaybackState();
            simpleExoPlayer.seekTo(0);
            simpleExoPlayer.release();
        }
    }

    private void pausePlayerToPlayAgain(){
        Logger.d("pause111",""+"test2");
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(false);
            simpleExoPlayer.getPlaybackState();
            simpleExoPlayer.seekTo(0);
        }
    }

    private void startPlayer(){
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(true);
            simpleExoPlayer.getPlaybackState();
        }
    }

    private void updateCommentCount(ArrayList<Comment> comments) {

        if (popularPosts.size() > 0) {
            int commentCount = 0;
            for(Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments()!=null && comment.getSubComments().size() > 0){
                    commentCount += comment.getSubComments().size();
                }
            }
            popularPosts.get(SAVED_SCROLL_POSITION).setComments(comments);
            popularPosts.get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);

            comment.setText("" + commentCount);

            /*if (popularPostAdapter!=null) {
                popularPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }*/
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {

            //Implemented

            /*if (data.getSerializableExtra("comments")!=null) {
                updateCommentCount((ArrayList<Comment>) data.getSerializableExtra("comments"));
            }*/
        } else if (requestCode == 102 && resultCode == RESULT_OK) {

            //remaining

            /*if (data.getStringExtra("postText")!=null) {
                updatePostText(data.getStringExtra("postText"));
            }*/
        }
    }

    @Override
    public void onDialogDismiss(ArrayList<Comment> comments) {
        onResume();
        if(comments!=null){
            updateCommentCount(comments);
        }

    }
}
