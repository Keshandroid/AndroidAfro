package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.TBI.afrocamgist.CacheService;
import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.NonSwipeableViewPager;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.NavigationListAdapter;
import com.TBI.afrocamgist.afrocalls.Restarter;
import com.TBI.afrocamgist.afrocalls.VideoCallService;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.AfroChatRootFragment;
import com.TBI.afrocamgist.fragment.AfroExplorerRootFragment;
import com.TBI.afrocamgist.fragment.AfroGroupRootFragment;
import com.TBI.afrocamgist.fragment.AfroSwaggerRootFragment;
import com.TBI.afrocamgist.fragment.AfroCreatePostRootFragment;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.media.Media;
import com.TBI.afrocamgist.model.media.MediaDetails;
import com.TBI.afrocamgist.model.notification.NotificationCounter;
import com.TBI.afrocamgist.model.user.User;
import com.abedelazizshe.lightcompressorlibrary.CompressionListener;
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor;
import com.abedelazizshe.lightcompressorlibrary.VideoQuality;
import com.abedelazizshe.lightcompressorlibrary.config.Configuration;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.connection.Connectivity;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.util.IOUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.appupdate.testing.FakeAppUpdateManager;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Ack;
import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnMultiCompressListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.TBI.afrocamgist.app.AfrocamgistApplication.isPostUploading;
import static net.alhazmy13.mediapicker.Video.VideoPicker.EXTRA_VIDEO_PATH;


public class DashboardActivity extends BaseActivity implements ConnectivityListener, View.OnClickListener {

    Intent mServiceIntent;
    private VideoCallService mYourService;

    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;

    private TabLayout tabLayout;
    public static NonSwipeableViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.select_afro_swagger,
            //R.drawable.select_afro_talent,  //Remove Talent change
            R.drawable.select_afro_explorer,
            R.drawable.select_afro_create_post,
            R.drawable.select_afro_group,
            R.drawable.select_afro_chat
    };

    ViewPagerAdapter adapter;

    private ArrayList<String> menuItems = new ArrayList<>();
    private DrawerLayout drawer;
    private TextView name, profileStrength;
    public TextView barTitle;
    private RoundedHorizontalProgressBar strength;
    private CircleImageView profileImage;
    private TextView notificationCounter;
    private Handler handler;
    private Connectivity mConnectivity;
    private Integer noInternetConnectionCounter = 0, messageCount = 0;
    public static final int CAMERA_INTENT_PROFILE = 100;
    private static final String IMAGE_DIRECTORY = "/Afrocamgist";
    private KProgressHUD hud;
    private ArrayList<Follow> followingList = new ArrayList<>();
    private AppEventsLogger logger;
    private Switch switchAutoPlayVideo;
    private Dialog popup;
    private ImageView topLogo;


    protected LocalBroadcastManager localBroadcastManager;

    //outside intent
    private ArrayList<File> imageFiles = new ArrayList<>();
    private ArrayList<File> compressedImageFiles = new ArrayList<>();
    private KProgressHUD hudVideoTrim,hudvideo;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123;
    boolean result;

    private File videoFile,tempvideo;

    //in-app update
    private AppUpdateManager appUpdateManager;
    private static final int IMMEDIATE_APP_UPDATE_REQ_CODE  = 124;
    private InstallStateUpdatedListener installStateUpdatedListener;


    //immediate update
    /*private void checkUpdate() {

        com.google.android.play.core.tasks.Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                startUpdateFlow(appUpdateInfo);
            } else if  (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS){
                startUpdateFlow(appUpdateInfo);
            }
        });
    }*/

    //flexible update
    private void checkUpdate() {

        com.google.android.play.core.tasks.Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                Log.d("update_type","flexible");

                startUpdateFlow(appUpdateInfo);
            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                Log.d("update_type","downloaded");
                popupSnackBarForCompleteUpdate();
            }else {
                Log.d("update_type","other");
            }
        });
    }

    //flexible flow
    private void startUpdateFlow(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.FLEXIBLE, this, IMMEDIATE_APP_UPDATE_REQ_CODE);
        } catch (IntentSender.SendIntentException e) {
            Log.d("update_type","error : "+e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    //immediate flow
    /*private void startUpdateFlow(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.IMMEDIATE, this, IMMEDIATE_APP_UPDATE_REQ_CODE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }*/

    private void popupSnackBarForCompleteUpdate() {
        Snackbar.make(findViewById(android.R.id.content).getRootView(), "New app is ready!", Snackbar.LENGTH_INDEFINITE)
                .setAction("Install", view -> {
                    if (appUpdateManager != null) {
                        appUpdateManager.completeUpdate();
                    }
                })
                .setActionTextColor(getResources().getColor(R.color.opaqueBlue))
                .show();
    }

    private void removeInstallStateUpdateListener() {
        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(installStateUpdatedListener);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("Service status", "Running");
                return true;
            }
        }
        Log.i ("Service status", "Not running");
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //start video call background service and register service/receiver in Manifiest
        //startCallBackgroundService();


        //in-app update
        appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
        installStateUpdatedListener = state -> {
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                Toast.makeText(getApplicationContext(), "DOWNLOADEDDDD: state: " + state.installStatus(), Toast.LENGTH_LONG).show();

                popupSnackBarForCompleteUpdate();
            } else if (state.installStatus() == InstallStatus.INSTALLED) {
                Toast.makeText(getApplicationContext(), "INSTALLEDDDD: state: " + state.installStatus(), Toast.LENGTH_LONG).show();
                removeInstallStateUpdateListener();
            } else {
                Toast.makeText(getApplicationContext(), "InstallStateUpdatedListener: state: " + state.installStatus(), Toast.LENGTH_LONG).show();
            }
        };
        appUpdateManager.registerListener(installStateUpdatedListener);
        checkUpdate();

        //Start cache service
        Intent stickyService = new Intent(this, CacheService.class);
        startService(stickyService);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(mNotifyDrawerProfileDetail,
                new IntentFilter(SettingsActivity.NOTIFY_DRAWER_PROFILE_DETAIL));

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        logger = AppEventsLogger.newLogger(DashboardActivity.this);
        logSentFriendRequestEvent();

        // Make bubble screen the main screen
//        if (getIntent().getSerializableExtra("posts_bubble")!=null)
//            startActivity(new Intent(DashboardActivity.this, BubbleActivity.class).putExtra("posts_bubble",getIntent().getSerializableExtra("posts_bubble")));

        //perform action based on the action performed in the bubbles popular post view.
        if (getIntent().getSerializableExtra("posts")!=null)
            startActivity(new Intent(DashboardActivity.this, PopularPostsActivity.class).putExtra("posts",getIntent().getSerializableExtra("posts")));

        if (getIntent().getIntExtra("postId",0) != 0)
            startActivity(new Intent(DashboardActivity.this, ViewPostActivity.class).putExtra("postId", getIntent().getIntExtra("postId",0)));


        //KK
        //If notification received and user data is not null than open chat activity
        if (getIntent().getSerializableExtra("user") != null)
//            startActivity(new Intent(DashboardActivity.this, ChatActivity.class).putExtra("user", getIntent().getSerializableExtra("user")));
            startActivity(new Intent(DashboardActivity.this, SocketChatActivity.class).putExtra("user", getIntent().getSerializableExtra("user")));

        //If notification received and user data is not null than open Profile activity
        if (getIntent().getIntExtra("userId",0) != 0)
            startActivity(new Intent(DashboardActivity.this, ProfileActivity.class).putExtra("userId", getIntent().getIntExtra("userId",0)));


        //outside intent code start
        checkOutsideIntent();


        if (getIntent().getStringExtra("Path") != null){
            showTextAndVideo(getIntent().getStringExtra("Path"));
        }

        initView();
        setupViewPager();
        tabLayout.setupWithViewPager(viewPager);
        setupTab();
        setNavigationDrawerMenu();
        initClickListener();

        /*if (Utils.isConnected())
            getUserProfile();
        getNotificationCounter();
        setNotificationCounter();*/

        setData();
        initInternetConnectivityCheckListener();

        if(!Identity.getAutoPlayVideoPreference(DashboardActivity.this)){
            showAutoPlayPopup();
        }

    }

    private void startCallBackgroundService() {
        mYourService = new VideoCallService();
        mServiceIntent = new Intent(this, mYourService.getClass());
        if (!isMyServiceRunning(mYourService.getClass())) {
            startService(mServiceIntent);
        }
    }

    private void stopCallBackgroundService(){
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartservice");
        broadcastIntent.setClass(this, Restarter.class);
        this.sendBroadcast(broadcastIntent);

    }


    private void checkOutsideIntent(){
        Intent intent = getIntent();
        String type = intent.getType();
        String action = intent.getAction();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            result = checkPermission();
            if(result){
                if (type.startsWith("image/")) {
                    handleSendImage(intent);
                }
                if(type.startsWith("video/")){
                    Logger.d("SEND_TYPE1","video...");
                    handleSendVideo(intent);
                }
            }
        }
    }

    private void handleSendVideo(Intent intent){
        Uri videoUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (videoUri != null) {

            //List<String> mPaths = Matisse.obtainPathResult(intent);
            Logger.d("Video99","999" + getFilePathFromURI(this,videoUri));

            ArrayList<String> videoPaths = new ArrayList<>();
            videoPaths.add(getFilePathFromURI(this,videoUri));

            checkVideoLength(videoPaths);

        } else {
            Toast.makeText(this, "Error occurred, URI is invalid", Toast.LENGTH_LONG).show();
        }
    }

    private void checkVideoLength(ArrayList<String> videoPaths){

        hudVideoTrim = KProgressHUD.create(DashboardActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setDetailsLabel("Please wait")
                .setCancellable(false)
                .show();

        Logger.e("LLLLL_Time: ", "000");
        //ArrayList<String> mPaths = data.getStringArrayListExtra(EXTRA_VIDEO_PATH);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(DashboardActivity.this, Uri.parse(videoPaths.get(0)));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long durationMs = Long.parseLong(time);
        long minutes = TimeUnit.MILLISECONDS.toSeconds(durationMs);
        retriever.release();
        Logger.e("LLLLL_Time: ", String.valueOf(minutes));

        if (minutes < 60) {
            if(hudVideoTrim!=null){
                hudVideoTrim.dismiss();
            }
            showTextAndVideo(videoPaths.get(0));
        } else {
            if(hudVideoTrim!=null){
                hudVideoTrim.dismiss();
            }
            Logger.e("LLLLL_Android_V: ", String.valueOf(Build.VERSION.SDK_INT));
            Intent intent = new Intent(DashboardActivity.this, VideoTrimmarActivity.class);
            intent.putExtra(EXTRA_VIDEO_PATH, videoPaths.get(0));
            intent.putExtra("postStoryFor","afroswagger");
            intent.putExtra("isNormalPost","normalPost");
            intent.putExtra("outsideIntent","video");
            startActivity(intent);
            finish();
        }
    }

    public void showTextAndVideo(String videoPaths) {

        ArrayList<Uri> videoURIList = new ArrayList<>();
        Uri finalUri = Uri.parse(videoPaths);
        videoURIList.add(finalUri);

        Logger.d("vid_path_111",""+videoPaths);

        File file = new File(videoPaths);
        long length = file.length();
        length = length/1024;
        //Toast.makeText(this, "Video size:"+length+"KB", Toast.LENGTH_LONG).show();

        if(length>20000){

            //File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/Video"); //old code
            File wallpaperDirectory = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + "/Video"); // new code


            if (!wallpaperDirectory.exists())
                wallpaperDirectory.mkdir();

            File f = new File(wallpaperDirectory, "VID_" + Calendar.getInstance().getTimeInMillis() + ".mp4");

            VideoCompressor.start(getApplicationContext(),videoURIList,true, Environment.DIRECTORY_DOWNLOADS,
                    new CompressionListener() {
                @Override
                public void onCancelled(int index) {
                    // On Cancelled
                    hudvideo.dismiss();
                }

                @Override
                public void onProgress(int index, float percent) {
                    // Update UI with progress value
                    runOnUiThread(new Runnable() {
                        public void run() {
                            hudvideo.setDetailsLabel("Compressing Video:"+Math.round(percent)+"%");
                            Logger.e("LLLLL_Progress: ",""+percent);
                        }
                    });
                }

                @Override
                public void onFailure(int index, @NotNull String failureMessage) {
                    Logger.e("LLLLL_Fail: ","Fail");

                }

                @Override
                public void onSuccess(int index, long size, @Nullable String path) {
                    hudvideo.dismiss();

                    //playVideo(f.getAbsolutePath());

                    createVideoPost(f.getAbsolutePath());

                    tempvideo = new File(videoPaths);

                    /*if (tempvideo != null)
                        tempvideo.delete();*/

                    float length = f.length() / 1024f; // Size in KB
                    String value;
                    if (length >= 1024)
                        value = length / 1024f + " MB";
                    else
                        value = length + " KB";
                    String text = String.format(Locale.US, "Output: \nName:"+f.getName()+" \nSize: "+ value);
                    Logger.e("LLLLL_End: ",text);

                    float length1 = new File(videoPaths).length() / 1024f;
                    String value1;
                    if (length1 >= 1024)
                        value1 = length1 / 1024f + " MB";
                    else
                        value1 = length1 + " KB";
                    String text1 = String.format(Locale.US, "Input: \nName:"+new File(videoPaths).getName()+" \nSize: "+ value1);
                    Logger.e("LLLLL_End: ",text1);
                }

                @Override
                public void onStart(int index) {
                    hudvideo = KProgressHUD.create(DashboardActivity.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setDimAmount(0.5f)
                            .setMaxProgress(100)
                            .setDetailsLabel("Compressing Video")
                            .setCancellable(true)
                            .show();
                }

            }, new Configuration(
                    VideoQuality.VERY_HIGH,
                    24, /*frameRate: int, or null*/
                    false, /*isMinBitrateCheckEnabled*/
                    null, /*videoBitrate: int, or null*/
                    false, /*disableAudio: Boolean, or null*/
                    false, /*keepOriginalResolution: Boolean, or null*/
                    360.0, /*videoWidth: Double, or null*/
                    480.0 /*videoHeight: Double, or null*/
            ));


        }else {
            //playVideo(videoPaths);

            createVideoPost(videoPaths);

        }
    }

    private void createVideoPost(String videoPath) {

        /*createVideoFile(videoPath);
        uploadVideo();*/

        extraShareTextDialogVideo(videoPath,this);

    }

    public void extraShareTextDialogVideo(String videoPath, Context context) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_extra_share_text);
            popup.setCancelable(true);
            popup.show();

            EditText edtExtraText = popup.findViewById(R.id.edtExtraText);
            TextView btnSharePost = popup.findViewById(R.id.btnSharePost);

            btnSharePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    createVideoFile(videoPath);
                    uploadVideo(edtExtraText.getText().toString());

                    popup.dismiss();
                }
            });

        }
    }

    public void extraShareTextDialogImage(ArrayList<Media> mediaList, Context context) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_extra_share_text);
            popup.setCancelable(true);
            popup.show();

            EditText edtExtraText = popup.findViewById(R.id.edtExtraText);
            TextView btnSharePost = popup.findViewById(R.id.btnSharePost);

            btnSharePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    createImagePost(mediaList,edtExtraText.getText().toString());

                    popup.dismiss();
                }
            });

        }
    }

    private void uploadVideo(String postTextExtra) {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, videoFile.getName(), RequestBody.create(MediaType.parse("video/*"), videoFile));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload-video";
        Logger.e("LLLL_Path: ", videoFile.getAbsolutePath());
        Webservices.getData(DashboardActivity.this, Webservices.Method.POST, builder, headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(DashboardActivity.this, "Oops something went wrong....");
            } else {
                try {
                    Logger.e("LLLLLL_Swagger_Res: ", response);
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(DashboardActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {
                            if (videoFile != null)
                                //videoFile.delete();

                            createVideoPost(mediaDetails.getMediaList(), postTextExtra);


                        } else {
                            Utils.showAlert(DashboardActivity.this, getString(R.string.opps_something_went_wrong_please_try_again_later));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        }, (bytesRead, contentLength) -> {
            Logger.e("LLLL_Byte: ", bytesRead + "     " + contentLength);
            int percentage = (int) ((100 * bytesRead) / contentLength);
            Logger.e("LLLL_Percentage", percentage + "");
            /*if (swaggerPostAdapter != null) {
                swaggerPostAdapter.setUploadProgress(percentage);
                if (getActivity() != null)
                    getActivity().runOnUiThread(() -> swaggerPostAdapter.notifyItemChanged(2));
            }*/
        });
    }

    private void createVideoPost(ArrayList<Media> mediaList, String postTextExtra) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createVideoPostJsonRequest(mediaList, postTextExtra), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(DashboardActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(DashboardActivity.this, getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    Logger.e("LLLL_Res2_frag: ", response);

                    if (viewPager!=null && viewPager.getAdapter()!=null){
                        viewPager.getAdapter().notifyDataSetChanged();
                        setupTab();
                    }
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });

    }

    private JSONObject createVideoPostJsonRequest(ArrayList<Media> mediaList, String postTextExtra) {

        JSONObject request = new JSONObject();
        try {

           /* if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (TagToBeTagged tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged.getUserId());
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }*/

            request.put("post_text", ""+postTextExtra);
            request.put("post_lat_long", "");
            request.put("posted_for", "afroswagger");
            request.put("post_type", "video");
            request.put("post_video", mediaList.get(0).getPath());
            request.put("thumbnail", mediaList.get(0).getThumbnails().get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void createVideoFile(String videoPath) {
        try {
            videoFile = new File(videoPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {

            //List<String> mPaths = Matisse.obtainPathResult(intent);
            Logger.d("Images99","999" + getFilePathFromURI(this,imageUri));

            ArrayList<String> imagePaths = new ArrayList<>();
            imagePaths.add(getFilePathFromURI(this,imageUri));

            createImageFile(imagePaths);
        } else {
            Toast.makeText(this, "Error occurred, URI is invalid", Toast.LENGTH_LONG).show();
        }

    }

    private void createImageFile(ArrayList<String> imagePaths) {

        imageFiles = new ArrayList<>();

        for (int i = 0; i < imagePaths.size(); i++) {

            try {
                imageFiles.add(new File(imagePaths.get(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Luban.compress(this, imageFiles)
                .putGear(Luban.CUSTOM_GEAR)
                .launch(new OnMultiCompressListener() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onSuccess(List<File> fileList) {
                        compressedImageFiles = new ArrayList<>(fileList);
                        for (File image : imageFiles)
                            //image.delete();
                        uploadImages();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.d("Percentage", e.getMessage() + "");
                    }
                });
    }

    private void uploadImages() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (File photo : compressedImageFiles) {
            builder.addFormDataPart(Constants.IMAGES, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload";

        Webservices.getData(this, Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(this, getString(R.string.opps_something_went_wrong));
            } else {
                hud.dismiss();
                try {
                    JSONObject object = new JSONObject(response);

                    Logger.d("uploadResponse", object.toString());

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(this, object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {

                            for (File image : compressedImageFiles)
                                //image.delete();


                            //createImagePost(mediaDetails.getMediaList());
                                extraShareTextDialogImage(mediaDetails.getMediaList(), this);
                        } else {
                            Utils.showAlert(this, getString(R.string.opps_something_went_wrong_please_try_again_later));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Logger.d("exception1",""+ Log.getStackTraceString(e));
                    Logger.d("exception1",""+e.getLocalizedMessage());
                    //Utils.showAlert(getActivity(), response);
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int) (100 * bytesRead / contentLength);
            Logger.d("Percentage", percentage + "");
            //if (swaggerPostAdapter != null) { // important
//            setUploadProgress(percentage);
//            if (getActivity() != null)
//                getActivity().runOnUiThread(this::showPostingView); //important
            //}
        });
    }

    private void createImagePost(ArrayList<Media> mediaList, String extraImageText) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createImagePostJsonRequest(mediaList,extraImageText), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(this, getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();

                    if (viewPager!=null && viewPager.getAdapter()!=null){
                        viewPager.getAdapter().notifyDataSetChanged();
                        setupTab();
                    }

                    /*if (swaggerPosts != null) {
                        //swaggerPosts.remove(1); // important
                        cardProgressbar.setVisibility(View.GONE);
                        videosAdapter.notifyDataSetChanged();
                    }
                    getSwaggerFirstPagePosts(true);
                    getAdvertisements();*/
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    private JSONObject createImagePostJsonRequest(ArrayList<Media> mediaList, String extraImageText) {

        JSONObject request = new JSONObject();
        try {

            request.put("post_text", ""+ extraImageText);
            request.put("post_lat_long", "");
            request.put("posted_for", "afroswagger");

            JSONArray images = new JSONArray();
            for (Media media : mediaList) {
                images.put(media.getPath());
            }

            /*if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (TagToBeTagged tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged.getUserId());
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }*/

            request.put("post_type", "image");
            request.put("post_image", images);

            Logger.d("imagelistRequest", new GsonBuilder().setPrettyPrinting().create().toJson(request));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    /*public static String getRealPathFromURI(Context context, Uri contentUri) {
         Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //checkFolder();
                    Logger.d("videoUrl09","allow.....");
                    checkOutsideIntent();
                } else {
                    //code for deny
                    Logger.d("videoUrl09","deny.....");
                    checkAgain();
                }
                break;
        }
    }

    public void checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(DashboardActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(DashboardActivity.this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(DashboardActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(DashboardActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
            //File copyFile = new File(TEMP_DATH + File.separator + fileName);
            File copyFile = new File(context.getFilesDir() + File.separator + fileName);
            copy(context, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copyStream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initView() {

        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tabs);
        drawer = findViewById(R.id.drawer_layout);
        name = findViewById(R.id.name);
        profileImage = findViewById(R.id.profile_image);
        profileStrength = findViewById(R.id.profileStrength);
        strength = findViewById(R.id.strength);
        barTitle = findViewById(R.id.barTitle);
        notificationCounter = findViewById(R.id.notification_counter);
        switchAutoPlayVideo = findViewById(R.id.switchAutoPlayVideo);
        topLogo = findViewById(R.id.logo);
    }

    private BroadcastReceiver mNotifyDrawerProfileDetail = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setData();
        }
    };

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    private void showAutoPlayPopup(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showInfo(DashboardActivity.this);
            }
        }, 1000 * 60);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            noInternetConnectionCounter = 0;
            if (viewPager != null && viewPager.getAdapter() != null) {
                //viewPager.getAdapter().notifyDataSetChanged();
                //setupTab();
                //getUserProfile();
                //getNotificationCounter();
                getNotificationCounterNew();
                setNotificationCounter();
            }
            Utils.closeAlert();
        } else {
            if (noInternetConnectionCounter == 0) {
                //Utils.showAlert(DashboardActivity.this, "No internet connection available.");
                noInternetConnectionCounter++;
            }
        }
    }


    private void getUserProfile() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.PROFILE, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    Log.d("userProfile1",new GsonBuilder().setPrettyPrinting().create().toJson(object));

                    if (!object.has(Constants.MESSAGE)) {
                        User user = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), User.class);
                        LocalStorage.saveUserDetails(user);
                        followingList.clear();
                        followingList.addAll(user.getFollowingsList());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //check for notification counter on regular interval
    private void setNotificationCounter() {
        handler = new Handler();
        handler.postDelayed(runnable, 1000);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
//            Logger.d("notificationCount", "Called");
            //getNotificationCounter();
            getNotificationCounterNew();
            handler.postDelayed(this, 1000);
        }
    };

    //get notification counter from API
    private void getNotificationCounter() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.NOTIFICATION_COUNTER, response -> {
            if (!"".equals(response)) {
                try {
                    JSONObject object = new JSONObject(response);

                    if (!object.has(Constants.MESSAGE)) {

                        JSONObject data = object.getJSONObject("data");
                        int counter = data.getInt("notification_count");
                        messageCount = data.getJSONObject("navCounters").getInt("messages");

                        /*Objects.requireNonNull(tabLayout.getTabAt(3)).setCustomView(null); //Remove Talent change
                        Objects.requireNonNull(tabLayout.getTabAt(3)).setCustomView(setChatTabIcon(tabIcons[3])); //Remove Talent change*/

                        Objects.requireNonNull(tabLayout.getTabAt(4)).setCustomView(null); //Remove Talent change
                        Objects.requireNonNull(tabLayout.getTabAt(4)).setCustomView(setChatTabIcon(tabIcons[4])); //Remove Talent change

                        if (counter > 0) {
                            notificationCounter.setVisibility(View.VISIBLE);
                            if (counter > 99)
                                notificationCounter.setText("99+");
                            else
                                notificationCounter.setText(String.valueOf(counter));
                        } else {
                            notificationCounter.setVisibility(View.GONE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //Get Notification Counter New API call
    private void getNotificationCounterNew(){

        Call<NotificationCounter> call = BaseServices.getAPI().create(ParameterServices.class).getNotificationCounter();
        call.enqueue(new Callback<NotificationCounter>() {
            @Override
            public void onResponse(@NotNull Call<NotificationCounter> call, @NotNull Response<NotificationCounter> response) {

                NotificationCounter object = response.body();
                Logger.d("NotificationCounter99", new GsonBuilder().setPrettyPrinting().create().toJson(object));
                if(response.isSuccessful()){
                    if(object!=null && object.getStatus()){
                        try {

                            int counter = object.getNotificationData().getNotification_count();
                            messageCount = object.getNotificationData().getNavCounters().getMessages();

                            Log.i("messageCount", "messageCount 111" + messageCount);

                            /*Objects.requireNonNull(tabLayout.getTabAt(3)).setCustomView(null); //Remove Talent change
                            Objects.requireNonNull(tabLayout.getTabAt(3)).setCustomView(setChatTabIcon(tabIcons[3])); //Remove Talent change*/

                            Objects.requireNonNull(tabLayout.getTabAt(4)).setCustomView(null); //Remove Talent change
                            Objects.requireNonNull(tabLayout.getTabAt(4)).setCustomView(setChatTabIcon(tabIcons[4])); //Remove Talent change

                            if (counter > 0) {
                                notificationCounter.setVisibility(View.VISIBLE);
                                if (counter > 99)
                                    notificationCounter.setText("99+");
                                else
                                    notificationCounter.setText(String.valueOf(counter));
                            } else {
                                notificationCounter.setVisibility(View.GONE);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<NotificationCounter> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    //set profile data in side menu
    private void setData() {

        if (LocalStorage.getUserDetails() != null) {
            String fullName = LocalStorage.getUserDetails().getFirstName() + " " + LocalStorage.getUserDetails().getLastName();
            String profileStrengthPercentage = LocalStorage.getUserDetails().getProfileStrength() + "%";

            name.setText(fullName);
            profileStrength.setText(profileStrengthPercentage);
            if (LocalStorage.getUserDetails().getProfileStrength() != null)
                strength.setProgress(LocalStorage.getUserDetails().getProfileStrength());

            Glide.with(this)
                    .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                    .into(profileImage);
        }
    }

    private void setupTab() {
        Objects.requireNonNull(tabLayout.getTabAt(0)).setCustomView(setTabIcons(tabIcons[0]));
        Objects.requireNonNull(tabLayout.getTabAt(1)).setCustomView(setTabIcons(tabIcons[1]));
        Objects.requireNonNull(tabLayout.getTabAt(2)).setCustomView(setTabIcons(tabIcons[2]));
        Objects.requireNonNull(tabLayout.getTabAt(3)).setCustomView(setTabIcons(tabIcons[3]));
        Objects.requireNonNull(tabLayout.getTabAt(4)).setCustomView(setTabIcons(tabIcons[4]));
        //Objects.requireNonNull(tabLayout.getTabAt(4)).setCustomView(setChatTabIcon(tabIcons[4])); //Remove Talent change

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setBarTitle(tab.getPosition());
                if ((tab.getPosition() == 0) || (tab.getPosition() == 0)) { //Remove Talent change (add tab position 1 in second condition)
                    Logger.d("tabPosition1",""+tab.getPosition());
                    findViewById(R.id.searchChat).setVisibility(View.GONE);
//                    findViewById(R.id.callLogButton).setVisibility(View.GONE); //SET VISIBILITY to use callog
                    findViewById(R.id.camera).setVisibility(View.VISIBLE);
                } else if (tab.getPosition() == 4) { //Remove Talent change
                    findViewById(R.id.camera).setVisibility(View.GONE);
                    findViewById(R.id.searchChat).setVisibility(View.VISIBLE);
//                    findViewById(R.id.callLogButton).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.searchChat).setVisibility(View.GONE);
//                    findViewById(R.id.callLogButton).setVisibility(View.GONE);
                    findViewById(R.id.camera).setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private View setTabIcons(int icon) {

        View view = getLayoutInflater().inflate(R.layout.item_tab, null);
        view.findViewById(R.id.icon).setBackgroundResource(icon);

        return view;
    }

    private View setChatTabIcon(int icon) {

        View view = getLayoutInflater().inflate(R.layout.item_tab_chat, null);
        view.findViewById(R.id.icon).setBackgroundResource(icon);

        if (messageCount == 0)
            view.findViewById(R.id.message_count).setVisibility(View.GONE);
        else {
            if (messageCount > 99)
                ((TextView) view.findViewById(R.id.message_count)).setText("99+");
            else
                ((TextView) view.findViewById(R.id.message_count)).setText(String.valueOf(messageCount));
        }

        return view;
    }

    private void setBarTitle(int position) {

        // Original code (Swagger,Talent,Group,Explorer,Chat) //Remove Talent change
        /*switch (position) {

            case 0:
                barTitle.setText(R.string.afroSwagger);
                break;
            case 1:
                barTitle.setText(R.string.afroTalent);
                break;
            case 2:
                barTitle.setText(R.string.afroGroup);
                break;
            case 3:
                barTitle.setText(R.string.afroExplorer);
                break;
            case 4:
                barTitle.setText(R.string.afroChat);
                break;
            default:
                barTitle.setText("");
                break;
        }*/

        // Original code (Swagger,Group,Explorer,Chat)
        switch (position) {

            case 0:
                barTitle.setText(R.string.afroSwagger);
                break;
            case 1:
                barTitle.setText(R.string.afroExplorer);
                break;
            case 2:
                barTitle.setText(R.string.afroCreatePost);
                break;
            case 3:
                barTitle.setText(R.string.afroGroup);
                break;
            case 4:
                barTitle.setText(R.string.afroChat);
                break;
            default:
                barTitle.setText("");
                break;
        }

    }

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AfroSwaggerRootFragment());
        //adapter.addFragment(new AfroTalentRootFragment());  //Remove Talent change
        adapter.addFragment(new AfroExplorerRootFragment());
        adapter.addFragment(new AfroCreatePostRootFragment());
        adapter.addFragment(new AfroGroupRootFragment());
        adapter.addFragment(new AfroChatRootFragment());

        /* the ViewPager requires a minimum of 1 as OffscreenPageLimit */
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);

        viewPager.setAdapter(adapter);

        viewPager.setOffscreenPageLimit(limit);

        if (getIntent().getStringExtra("group")!=null)
            viewPager.setCurrentItem(1); //Remove Talent change
    }

    private void initClickListener() {

        findViewById(R.id.camera).setOnClickListener(this);
        findViewById(R.id.searchChat).setOnClickListener(this);
        findViewById(R.id.callLogButton).setOnClickListener(this);
        findViewById(R.id.notification).setOnClickListener(this);
        findViewById(R.id.menu).setOnClickListener(this);
        findViewById(R.id.my_profile).setOnClickListener(this);
        findViewById(R.id.profile_image).setOnClickListener(this);

        if(Identity.getAutoPlayVideo(DashboardActivity.this)){
            switchAutoPlayVideo.setChecked(true);
        }else {
            switchAutoPlayVideo.setChecked(false);
        }

        switchAutoPlayVideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Identity.setAutoPlayVideo(DashboardActivity.this,true);
                }else {
                    Identity.setAutoPlayVideo(DashboardActivity.this,false);
                }
            }
        });

        topLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Refresh swagger fragment on click
                AfroSwaggerRootFragment fragment = (AfroSwaggerRootFragment) viewPager
                        .getAdapter()
                        .instantiateItem(viewPager, viewPager.getCurrentItem());
                fragment.refreshFragment();

            }
        });

    }

    private boolean hasPermissions(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions)
                if (ActivityCompat.checkSelfPermission(DashboardActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //call onActivityResult in viewpager fragments
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Glide.with(this)
                        .load(resultUri)
                        .apply(RequestOptions.circleCropTransform())
                        .into(profileImage);
                createProfileImageFile(resultUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(DashboardActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == 100 && resultCode == RESULT_OK) {
            if (viewPager!=null && viewPager.getAdapter()!=null){
                viewPager.getAdapter().notifyDataSetChanged();
                setupTab();
            }
        }

        if (requestCode == IMMEDIATE_APP_UPDATE_REQ_CODE) {
            if (resultCode == RESULT_CANCELED) {
                //Toast.makeText(getApplicationContext(), "Update canceled by user! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
                this.finishAffinity();
            } else if (resultCode == RESULT_OK) {
                //Toast.makeText(getApplicationContext(),"Update success! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getApplicationContext(), "Update Failed! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
                checkUpdate();
            }
        }

    }

    private void createProfileImageFile(Uri uri) {

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            //File directory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY); // old code
            File directory = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + IMAGE_DIRECTORY); // new code


            if (!directory.exists()) {
                directory.mkdirs();
            }

            try {
                File photo = new File(directory, Calendar.getInstance().getTimeInMillis() + ".jpg");
                FileOutputStream fo = new FileOutputStream(photo);
                fo.write(bytes.toByteArray());
                fo.close();
                saveProfilePhoto(photo);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //update user profile photo API
    private void saveProfilePhoto(File photo) {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(this, Webservices.Method.POST, builder, headers, UrlEndpoints.UPDATE_PROFILE_PICTURE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(DashboardActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(DashboardActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(DashboardActivity.this, getString(R.string.picture_updated_successfully), Toast.LENGTH_LONG).show();
                        setupViewPager();
                        setupTab();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(DashboardActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setNavigationDrawerMenu() {

        ListView navigationList = findViewById(R.id.nav_list);

        menuItems.add("Home");
        menuItems.add("Photo");
        menuItems.add("Manage Advert");
        menuItems.add("E-Health");
        menuItems.add("Settings");
        menuItems.add("Find Users");
        menuItems.add("Find Forums");
        menuItems.add("Follow Requests");
        menuItems.add("Language");
        menuItems.add("Terms & Policies");
        menuItems.add("Logout");

        navigationList.setAdapter(new NavigationListAdapter(this, menuItems));
        navigationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (menuItems.get(position).toLowerCase()) {

                    case "home":
                        viewPager.setCurrentItem(0);
                        break;
                    case "photo":
                        startActivity(new Intent(DashboardActivity.this, PhotosActivity.class));
                        break;
                    case "manage advert":
                        startActivity(new Intent(DashboardActivity.this, AdvertCreateActivity.class));
                        break;

                    case "e-health":
                        startActivity(new Intent(DashboardActivity.this, EHealthActivity.class));
                        break;
                    case "settings":
                        startActivity(new Intent(DashboardActivity.this, SettingsActivity.class));
                        break;
                    case "find users":
                        startActivity(new Intent(DashboardActivity.this, FindUserActivity.class).putExtra("openFromScreen","findUserMenu"));
                        break;
                    case "find forums":
                        startActivity(new Intent(DashboardActivity.this, FindGroupActivity.class));
                        break;
                    case "follow requests":
                        startActivity(new Intent(DashboardActivity.this, FollowRequestsActivity.class));
                        break;
                    case "language":
                        startActivity(new Intent(DashboardActivity.this, LanguageActivity.class));
                        break;
                    case "terms & policies":
                        startActivity(new Intent(DashboardActivity.this, TNCActivity.class));
                        break;
                    case "logout":
                        LoginManager.getInstance().logOut();
                        googleSignOut();
                        logOUT();
                        break;
                    default:
                        break;
                }

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        });
    }

    private void googleSignOut(){
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //updateUI(null);
                    }
                });
    }

    private void logOUT(){
        hud = KProgressHUD.create(DashboardActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        //remove sharedpreferences
        removeSharedPreferences();

        JSONObject params = new JSONObject();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, params, headers, UrlEndpoints.LOGOUT, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(DashboardActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    Logger.e("LLLL_Res: ",response);
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("status")) {
                        LocalStorage.setIsUserLoggedIn(false);
                        Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Utils.showAlert(DashboardActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(DashboardActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void removeSharedPreferences(){

        //PreferenceManager.getDefaultSharedPreferences(this).edit().clear().apply();

        SharedPreferences settings = this.getSharedPreferences("ChatData", Context.MODE_PRIVATE);
        settings.edit().clear().apply();

        SharedPreferences mySPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = mySPrefs.edit();
        editor.remove("AUTO_PLAY_VIDEO_PREFERENCE");
        editor.remove("DIALOG_GUIDE_PREFERENCE");
        editor.apply();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.camera:

                //Remove Talent change (original code)

                /*ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
                int selectedTabPosition = 0;
                if (viewPager!=null) {
                    if (viewPager.getCurrentItem() == 0) {
                        adapter.addFragment(new AfroSwaggerRootFragment("openCamera"));
                        adapter.addFragment(new AfroTalentRootFragment());
                        selectedTabPosition = 0;
                    } else {
                        adapter.addFragment(new AfroSwaggerRootFragment());
                        adapter.addFragment(new AfroTalentRootFragment("openCamera"));
                        selectedTabPosition = 1;
                    }
                } else {
                    adapter.addFragment(new AfroSwaggerRootFragment());
                    adapter.addFragment(new AfroTalentRootFragment());
                }
                adapter.addFragment(new AfroGroupRootFragment());
                adapter.addFragment(new AfroExplorerRootFragment());
                adapter.addFragment(new AfroChatRootFragment());

                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(selectedTabPosition);
                setupTab();*/

                // new code
                ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
                int selectedTabPosition = 0;
                if (viewPager!=null) {
                    if (viewPager.getCurrentItem() == 0) {
                        adapter.addFragment(new AfroSwaggerRootFragment("openCamera"));
                        //adapter.addFragment(new AfroTalentRootFragment());
                        selectedTabPosition = 0;
                    } /*else {
                        adapter.addFragment(new AfroSwaggerRootFragment());
                        adapter.addFragment(new AfroTalentRootFragment("openCamera"));
                        selectedTabPosition = 1;
                    }*/
                } else {
                    adapter.addFragment(new AfroSwaggerRootFragment());
                    //adapter.addFragment(new AfroTalentRootFragment());
                }
                adapter.addFragment(new AfroExplorerRootFragment());
                adapter.addFragment(new AfroCreatePostRootFragment());
                adapter.addFragment(new AfroGroupRootFragment());
                adapter.addFragment(new AfroChatRootFragment());

                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(selectedTabPosition);
                setupTab();

                break;
            case R.id.notification:
                startActivity(new Intent(DashboardActivity.this, NotificationActivity.class));
                //startActivity(new Intent(DashboardActivity.this, SocketNotificationActivity.class));


                break;
            case R.id.menu:
                if (Utils.isConnected())
                    getUserProfile();
                drawer.openDrawer(GravityCompat.START);
                break;
            case R.id.my_profile:
                startActivity(new Intent(DashboardActivity.this, MyProfileActivity.class));
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.profile_image:
                String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

                if (!hasPermissions(PERMISSIONS)) {
                    ActivityCompat.requestPermissions(DashboardActivity.this, PERMISSIONS, CAMERA_INTENT_PROFILE);
                } else {
                    CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setCropShape(CropImageView.CropShape.OVAL).start(DashboardActivity.this);
                }
                break;
            case R.id.searchChat:
                //startActivity(new Intent(DashboardActivity.this, FollowingActivity.class).putExtra("followings", followingList).putExtra("from", "Chat"));
                startActivity(new Intent(DashboardActivity.this, FindUserActivity.class).putExtra("openFromScreen","search"));
                break;
            case R.id.callLogButton:
                startActivity(new Intent(DashboardActivity.this, CallLogActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            //check if post is uploading
            if(!isPostUploading) {
                finishAffinity();
            }
        }
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        @SuppressLint("WrongConstant")
        ViewPagerAdapter(FragmentManager manager) {
            super(manager, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @Override
        @NonNull
        public Fragment getItem(int position) {

            if(position==0){
                findViewById(R.id.searchChat).setVisibility(View.GONE);
//                findViewById(R.id.callLogButton).setVisibility(View.GONE);
                findViewById(R.id.camera).setVisibility(View.VISIBLE);
            }
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

    }

    @Override
    public void onStop() {
        //stop callBackgroundService
        //stopCallBackgroundService();
        super.onStop();
        if (handler != null)
            handler.removeCallbacks(runnable);

        if (mConnectivity!=null)
            mConnectivity.removeInternetConnectivityChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        localBroadcastManager.unregisterReceiver(mNotifyDrawerProfileDetail);
        //deleteCache(this);

        //disconnect socket
        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.disconnect();
        }
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getExternalCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        noInternetConnectionCounter = 0;
        /*if(isNotification){
            viewPager.setCurrentItem(0);
            isNotification = false;
        }*/



//        if(mConnectivity!=null)
////            mConnectivity.addInternetConnectivityListener(this);
////        else {
////            mConnectivity = Connectivity.getInstance();
////            mConnectivity.addInternetConnectivityListener(this);
////        }



        //Modified
        if (Utils.isConnected()) {
            //getUserProfile();
            //getNotificationCounter();
            getNotificationCounterNew();
            setNotificationCounter();
            /*if (viewPager!=null && viewPager.getAdapter()!=null){
                viewPager.getAdapter().notifyDataSetChanged();
                setupTab();
            }*/
        }
        //Modified

    }

    public void logSentFriendRequestEvent () {
        Bundle parameters = new Bundle();
        parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "MainActivity");
        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, 1, parameters);
    }

    public void showInfo(Activity activity) {

        if (activity!=null && !activity.isFinishing()) {
            popup = new Dialog(activity, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_info);
            popup.setCancelable(false);
            popup.show();

            SwitchCompat switchAutoPlayVideoDialog = popup.findViewById(R.id.switchAutoPlayVideoDialog);
            TextView okButton = popup.findViewById(R.id.ok);

            if(Identity.getAutoPlayVideo(activity)){
                switchAutoPlayVideoDialog.setChecked(true);
            }else {
                switchAutoPlayVideoDialog.setChecked(false);
            }

            switchAutoPlayVideoDialog.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        Identity.setAutoPlayVideo(activity,true);
                        switchAutoPlayVideo.setChecked(true);
                    }else {
                        Identity.setAutoPlayVideo(activity,false);
                        switchAutoPlayVideo.setChecked(false);
                    }
                }
            });

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Identity.setAutoPlayVideoPreference(activity,true);
                    popup.dismiss();
                }
            });

        }
    }

    public void onClickFragmentTab(int position){
        viewPager.setCurrentItem(position);

    }

}