package com.TBI.afrocamgist.activity;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.TBI.afrocamgist.ErrorReporter;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.LocaleManager;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.afrocalls.CallBroadcastReceive;
import com.TBI.afrocamgist.afrocalls.IncomingAudioCallScreen;
import com.TBI.afrocamgist.afrocalls.IncomingCallScreen;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.socket.SocketHandler;
import com.TBI.afrocamgist.socket.SocketListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.SetOptions;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import id.zelory.compressor.Compressor;
import io.socket.client.Manager;
import io.socket.client.Socket;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class BaseActivity extends AppCompatActivity implements SocketListener {
    public static final long DISCONNECT_TIMEOUT = 600000; // 5 min = 5 * 60 * 1000 ms
    private static int count = 0;
    private static Handler handler = new Handler();
    public static boolean isBackground = false;

    public static final String ANDROID_CHANNEL_ID = "com.TBI.afrocamgist";
    public static final String ANDROID_CHANNEL_NAME = "ANDROID CHANNEL";

    private BroadcastReceiver mLangaugeChangedReceiver;

    //socket-start
    public boolean isSocketDisconnectManage = true;
    private ObjectMapper mapper = null;
    private static final Lock lock = new ReentrantLock();
    JSONObject jsonObject;
    String events;
    private SocketHandler socketHandler;
    public static boolean isAppWentToBg = false;
    public static boolean isWindowFocused = false;
    public static boolean isBackPressed = false;

    public String getFragmentTag(Class fragment) {
        return fragment.getSimpleName().trim();
    }
    //socket-end

    private static Runnable runnable;
    private static Handler disconnectHandler = new Handler(msg -> {
        // todo
        return true;
    });

    static public IChatEventListener chatEventListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        ErrorReporter errReporter = new ErrorReporter();
        errReporter.Init(BaseActivity.this);
        errReporter.CheckErrorAndSendMail(BaseActivity.this);

        mLangaugeChangedReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(final Context context, final Intent intent) {
                startActivity(getIntent());
                finish();
            }
        };

        // Register receiver
        registerReceiver(mLangaugeChangedReceiver, new IntentFilter("Language.changed"));


        //socket init
        //uncomment this to use Socket.IO
        initView();


        /*call feature start*/

        //Check incoming call
//        isIncomingCall();
        //remove call notification if call not exist
//        checkIfCallExist();

        /*call feature end*/

    }

    private void checkIfCallerDetailAvailable(String callType) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("calls").document("" + LocalStorage.getUserDetails().getUserId())
                .collection("oppositePerson")
                .document("detail")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot != null && documentSnapshot.exists()) {
                            String oppositePersonName = "", profileImage = "", call_id = "";
                            if (documentSnapshot.get("oppositePersonName") != null) {
                                oppositePersonName = (String) documentSnapshot.get("oppositePersonName");
                            }
                            if (documentSnapshot.get("oppositePersonProfilePic") != null) {
                                profileImage = (String) documentSnapshot.get("oppositePersonProfilePic");
                            }
                            if (documentSnapshot.get("call_id") != null) {
                                call_id = (String) documentSnapshot.get("call_id");
                            }

                            if (oppositePersonName != null && !oppositePersonName.isEmpty() &&
                                    call_id != null && !call_id.isEmpty()) {

                                //getBitmapAsyncAndDoWork(callType, oppositePersonName,profileImage);

                                setNotification(callType, oppositePersonName, profileImage, call_id);
                            }


                        } else {
                            Log.d(getClass().getSimpleName(), "Current data: null");
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getSimpleName(), "Exception: " + e.getLocalizedMessage());

            }
        });
    }


    private void checkIfCallExist() {
        Log.d("BaseActivity","checkIfCallExist : "+"called...");

        if (LocalStorage.getUserDetails() != null) {
            if (LocalStorage.getUserDetails().getUserId() != null) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                final DocumentReference docRef = db.collection("calls").document("" + LocalStorage.getUserDetails().getUserId());
                docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("IS_EXIST", "Listen failed.", e);
                            return;
                        }

                        if (snapshot != null && snapshot.exists()) {

                        } else {
                            Log.d("IS_EXIST", "Current data: null");
                            removeCallNotification();

                        }
                    }
                });
            }
        }
    }

    private void removeCallNotification() {
        NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        manager.cancelAll();
    }

    private void isIncomingCall() {
        Log.d("BaseActivity","isIncomingCall : "+"called...");

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if (LocalStorage.getUserDetails() != null) {
            if (LocalStorage.getUserDetails().getUserId() != null) {
                db.collection("calls")
                        .document("" + LocalStorage.getUserDetails().getUserId())
                        .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                            @Override
                            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                                if (error != null) {
                                    Log.e("CallService", "Listen failed." + error.toString());
                                    return;
                                }

                                if (value != null && value.exists()) {
                                    if (value.get("type") != null) {

                                        String typeValue = (String) value.get("type");

                                        if (typeValue != null) {
                                            if (typeValue.equalsIgnoreCase("OFFER")) {
                                                Log.e("callType", "111 : " + typeValue);

                                                checkCallType();

                                            } else if (typeValue.equalsIgnoreCase("ANSWER")) {

                                            } else if (typeValue.equalsIgnoreCase("END_CALL")) {

                                            }
                                        }


                                    }
                                } else {
                                    LocalStorage.setIsVideoScreenOpen(false);
                                    Log.d("CallService", "Current data: null");
                                }

                            }
                        });
            }
        }
    }

    private void checkCallType() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("calls").document("" + LocalStorage.getUserDetails().getUserId())
                .collection("callType")
                .document("detail")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot != null && documentSnapshot.exists()) {
                            if (documentSnapshot.get("callType") != null) {

                                String callType = (String) documentSnapshot.get("callType");

                                if (callType != null) {
                                    if (callType.equalsIgnoreCase("video")) {

                                        Log.d("CallService", "Video:"+LocalStorage.getIsVideoScreenOpen());

                                        if (!LocalStorage.getIsVideoScreenOpen()) {
                                            LocalStorage.setIsVideoScreenOpen(true);

                                            Log.d("CallService", "video call received....");

                                            checkIfCallerDetailAvailable("video");

                                            //setNotification("video");

                                            /*Intent startActivity = new Intent();
                                            startActivity.setClass(getApplicationContext(), IncomingCallScreen.class);
                                            startActivity.setAction(IncomingCallScreen.class.getName());
                                            startActivity.setFlags(
                                                    Intent.FLAG_ACTIVITY_NEW_TASK
                                                            | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                            getApplicationContext().startActivity(startActivity);*/

                                        }


                                    } else if (callType.equalsIgnoreCase("audio")) {
                                        Log.d("CallService", "Audio:"+LocalStorage.getIsVideoScreenOpen());

                                        if (!LocalStorage.getIsVideoScreenOpen()) {
                                            LocalStorage.setIsVideoScreenOpen(true);
                                            Log.d("CallService", "Audio call received....");


                                            checkIfCallerDetailAvailable("audio");





                                            /*Intent startActivity = new Intent();
                                            startActivity.setClass(getApplicationContext(), IncomingAudioCallScreen.class);
                                            startActivity.setAction(IncomingCallScreen.class.getName());
                                            startActivity.setFlags(
                                                    Intent.FLAG_ACTIVITY_NEW_TASK
                                                            | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                            getApplicationContext().startActivity(startActivity);*/

                                        }
                                    }
                                }
                            }


                        } else {
                            //LocalStorage.setIsVideoScreenOpen(false);
                            Log.d("CallService", "Current call type data: null");
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getSimpleName(), "Exception: " + e.getLocalizedMessage());

            }
        });
    }

    private void setNotification(String callType, String oppositePersonName, String bitmap, String call_id) {

        if (callType.equalsIgnoreCase("audio")) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();
                Uri soundUri = Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.afro_ringtone);
                notificationChannel.setSound(soundUri, audioAttributes);


                notificationChannel.setDescription("Channel description");
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableVibration(true);
                notificationManager.createNotificationChannel(notificationChannel);
            }


            Intent intent1 = new Intent(getBaseContext(), CallBroadcastReceive.class);
            intent1.putExtra("Answer", true);
            intent1.putExtra("callType", "audio");
            intent1.putExtra("oppositePersonName", oppositePersonName);
            intent1.putExtra("profileImage", bitmap);
            intent1.putExtra("call_id",call_id);
            intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent1 = PendingIntent.getActivity(getBaseContext(), 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent intent2 = new Intent(getBaseContext(), CallBroadcastReceive.class);
            intent2.putExtra("Reject", false);
            intent2.putExtra("callType", "audio");
            intent2.putExtra("call_id",call_id);
            intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent2 = PendingIntent.getActivity(getBaseContext(), 1, intent2, PendingIntent.FLAG_UPDATE_CURRENT);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, IncomingAudioCallScreen.class), PendingIntent.FLAG_UPDATE_CURRENT);


            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext(), NOTIFICATION_CHANNEL_ID);
            notificationBuilder.setAutoCancel(true)
                    //.setDefaults(Notification.DEFAULT_ALL)
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setCategory(Notification.CATEGORY_CALL)
                    .setOngoing(true)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.afrocamgist_logo)
                    //.setLargeIcon(bitmap)
                    .setTicker("Hearty365")
                    //.setSound()
                    .addAction(R.drawable.afrocamgist_logo, "Answer", pendingIntent1)
                    .addAction(R.drawable.afrocamgist_logo, "Reject", pendingIntent2)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentTitle("Incoming voice call from " + oppositePersonName)
                    .setContentText("Pick up the phone")
                    .setContentIntent(contentIntent)
                    //.setFullScreenIntent(pendingIntent,true)
                    .setContentInfo("Info");

            notificationManager.notify(/*notification id*/1, notificationBuilder.build());

            //setCallStatusToFirestore();

        } else if (callType.equalsIgnoreCase("video")) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();
                Uri soundUri = Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.afro_ringtone);
                notificationChannel.setSound(soundUri, audioAttributes);

                notificationChannel.setDescription("Channel description");
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableVibration(true);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            Intent intent1 = new Intent(getBaseContext(), CallBroadcastReceive.class);
            intent1.putExtra("Answer", true);
            intent1.putExtra("callType", "video");
            intent1.putExtra("call_id",call_id);
            intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent1 = PendingIntent.getActivity(getBaseContext(), 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent intent2 = new Intent(getBaseContext(), CallBroadcastReceive.class);
            intent2.putExtra("Reject", false);
            intent2.putExtra("callType", "video");
            intent2.putExtra("call_id",call_id);
            intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent2 = PendingIntent.getActivity(getBaseContext(), 1, intent2, PendingIntent.FLAG_UPDATE_CURRENT);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, IncomingCallScreen.class), PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext(), NOTIFICATION_CHANNEL_ID);
            notificationBuilder.setAutoCancel(true)
                    //.setDefaults(Notification.DEFAULT_ALL)
                    .setCategory(Notification.CATEGORY_CALL)
                    .setOngoing(true)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.afrocamgist_logo)
                    .setTicker("Hearty365")
                    //.setLargeIcon(bitmap)
                    .addAction(R.drawable.afrocamgist_logo, "Answer", pendingIntent1)
                    .addAction(R.drawable.afrocamgist_logo, "Reject", pendingIntent2)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentIntent(contentIntent)
                    .setContentTitle("Incoming video call from " + oppositePersonName)
                    .setContentText("Pick up the phone")
                    //.setFullScreenIntent(pendingIntent,true)
                    .setContentInfo("Info");

            notificationManager.notify(/*notification id*/1, notificationBuilder.build());
            //setCallStatusToFirestore();
        }


    }

    /*private void setCallStatusToFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("calls")
                .document(""+LocalStorage.getUserDetails().getUserId()).collection("callType")
                .document("detail")
                .update("callStatus", "Ringing")
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e("TAG", "callStatus: success");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("TAG", "callStatus: Error");
                    }
                });
    }*/

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    /*private void getBitmapAsyncAndDoWork(String callType, String oppositePersonName, String profileImage) {

        final Bitmap[] bitmap = {null};

        Glide.with(getApplicationContext())
                .asBitmap()
                .load(profileImage)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {

                        bitmap[0] = resource;
                        // TODO Do some work: pass this bitmap
                        setNotification(callType, oppositePersonName,bitmap[0]);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
    }*/

    private void initView() {
        //realm = Realm.getDefaultInstance();

        if (UrlEndpoints.socketIOClient != null) {
            if (UrlEndpoints.socketIOClient.connected()) {
                socketHandler = new SocketHandler(BaseActivity.this);
                socketHandler.setSocketListener(BaseActivity.this);
            } else {
                getSocketIoClient();
            }
        } else {
            getSocketIoClient();
        }
    }

    /**********
     * Socket management
     */
    public Socket getSocketIoClient() {
        if (UrlEndpoints.socketIOClient == null && !UrlEndpoints.isSocketConnecting) {
            connectSocket();
        } else if (UrlEndpoints.socketIOClient != null) {
            if (!UrlEndpoints.socketIOClient.connected()) {
                connectSocket();
            }
        } else {
            socketHandler = new SocketHandler(BaseActivity.this);
            socketHandler.setSocketListener(BaseActivity.this);
        }
        return UrlEndpoints.socketIOClient;
    }

    public void connectSocket() {
        try {
            /*String userID = PreferenceData.getString(PreferenceData.PREF_USER_ID);
            if (userID.length() > 0) {
                SocketHandler socketHandler = new SocketHandler(SocketBaseActivity.this);
                socketHandler.setSocketListener(SocketBaseActivity.this);
                if (Utility.isInternetConnected(SocketBaseActivity.this)) {
                    socketHandler.connectToSocket();
                }
            }*/


            if (LocalStorage.getUserDetails().getUserId() != null) {
                SocketHandler socketHandler = new SocketHandler(BaseActivity.this);
                socketHandler.setSocketListener(BaseActivity.this);
                if (Utils.isInternetConnected(BaseActivity.this)) {
                    socketHandler.connectToSocket();
                }
            }


        } catch (Exception e) {

            Log.d("socketConnect9", "" + e.toString());

            e.printStackTrace();
        }
    }

    @Override
    public void isSocketConnected(boolean connected) {
        Log.v("SocketBaseActivity", "isSocketConnected:" + connected);
    }

    @Override
    public void isSocketReConnected() {
        jsonObject = new JSONObject();
        events = Manager.EVENT_RECONNECT;

        uiHandler.post(runnableSocket);
    }

    @Override
    public void onEvent(String event, Object... arg0) {
        events = event;
        jsonObject = (JSONObject) arg0[0];

        uiHandler.post(runnableSocket);
    }

    Handler uiHandler = new Handler(Looper.getMainLooper());

    public Runnable runnableSocket = new Runnable() {
        @Override
        public void run() {
            if (events != null && jsonObject != null) {
                if (chatEventListener != null) {
                    chatEventListener.onSocketEvent(events, jsonObject);
                } else {
                    parseResponseinMain(events, jsonObject);
                }
            }
        }
    };

    public void parseResponseinMain(String event, JSONObject jsonObject) {

    }


    public synchronized ObjectMapper getMapper() {
        if (mapper != null) {
            return mapper;
        }
        try {
            lock.lock();
            if (mapper == null) {
                mapper = new ObjectMapper();
                mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES,
                        false);
            }
            lock.unlock();
        } catch (Exception ex) {

        }
        return mapper;
    }

    private static Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
//            Logger.e("LLLL_MyApp", "3600min or up " + count);
            // Perform any required operation on disconnect
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    public void onUserInteraction() {
//        Logger.e("LLLL_MyApp", "for "+isBackground);
        if (isBackground) {
            resetDisconnectTimer();
            if (runnable != null) {
                handler.removeCallbacks(runnable);
//                Logger.e("LLLL_MyApp", "back");
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        count = count + 1;
//                        Logger.e("LLLL_MyApp", String.valueOf(count));
                        handler.postDelayed(this, 60000);
                    }
                };
            } else {
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        count = count + 1;
//                        Logger.e("LLLL_MyApp", String.valueOf(count));
                        handler.postDelayed(this, 60000);
                    }
                };
            }
            handler.postDelayed(runnable, 60000);
        }
    }

    @Override
    public void onTrimMemory(int level) {
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            isBackground = true;
        }
        super.onTrimMemory(level);
    }

    @Override
    public void onResume() {
        super.onResume();
       /* if (count != 0) {
            if (count >= 5) {
                if (LocalStorage.getIsUserLoggedIn())
                    getAfroPopularPosts();
                else{
                    Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            } else {
//                Logger.e("LLLL_MyApp", "5min or less " + count);
            }
            count = 0;
        }*/

        //======= Working code of 5min refresh logic ========
//        if (count != 0) {
//            if (count >= 5 && !isNotification) {
//                if (LocalStorage.getIsUserLoggedIn())
//                    getAfroPopularPosts();
//                else{
//                    Logger.e("LLLL_MyApp", "5min or more + false " + count);
//                    Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
//            } else if(isNotification){
//                Logger.e("LLLL_MyApp", "5min or less or more + true" + count);
//            }
//            count = 0;
//        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Unregister receiver
        if (mLangaugeChangedReceiver != null) {
            try {
                unregisterReceiver(mLangaugeChangedReceiver);
                mLangaugeChangedReceiver = null;
            } catch (final Exception e) {
            }
        }

    }

    private void getAfroPopularPosts() {
        Logger.e("LLLL_Bare: ", LocalStorage.getLoginToken());
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MOST_POPULAR, response -> {
            if ("".equals(response)) {
                Utils.showAlert(BaseActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(BaseActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);

                        //Intent intent = new Intent(BaseActivity.this, BubbleActivity.class);
                        Intent intent = new Intent(BaseActivity.this, DashboardActivity.class);

                        if (postDetails.getPosts() != null && postDetails.getPosts().size() != 0) {
                            LocalStorage.savePopularPosts(postDetails.getPosts());
                            getAndStorePopularPostsImages(postDetails.getPosts());
                            intent.putExtra("posts_bubble", postDetails.getPosts());
                        }

                        startActivity(intent);
                        finish();
                    }

                } catch (Exception e) {
//                    Logger.e("LLLL_Data: ",e.getMessage());
                    e.printStackTrace();
                    Utils.showAlert(BaseActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    private void getAndStorePopularPostsImages(ArrayList<Post> posts) {
        try {
            new GetAndStoreImage(posts).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetAndStoreImage extends AsyncTask<Void, Void, ArrayList<String>> {

        private ArrayList<Post> posts;

        GetAndStoreImage(ArrayList<Post> posts) {
            this.posts = posts;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<String> images) {
            super.onPostExecute(images);
            LocalStorage.savePopularPostsImage(images);
        }

        @Override
        protected ArrayList<String> doInBackground(Void... voids) {

            ArrayList<String> drawables = new ArrayList<>();

            for (Post post : posts) {

                String src;

                if ("video".equals(post.getPostType())) {
                    src = UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail() + "?width=200&height=200";
                    BitmapDrawable drawable = getBitmapFromURL(src);
                    if (drawable != null)
                        drawables.add(convertDrawableToString(drawable));
                    else
                        drawables.add(null);
                } else if ("image".equals(post.getPostType())) {
                    src = UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0) + "?width=200&height=200";
                    BitmapDrawable drawable = getBitmapFromURL(src);
                    if (drawable != null)
                        drawables.add(convertDrawableToString(drawable));
                    else
                        drawables.add(null);
                } else {
                    drawables.add(null);
                }
            }

            return drawables;
        }

        private String convertDrawableToString(BitmapDrawable drawable) {

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            drawable.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, outputStream); //bm is the bitmap object
            byte[] b = outputStream.toByteArray();
            return Base64.encodeToString(b, Base64.DEFAULT);
        }
    }

    public BitmapDrawable getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return new BitmapDrawable(Resources.getSystem(), getCompressedBitmap(BitmapFactory.decodeStream(input)));
        } catch (Exception e) {
            return null;
        }
    }

    private Bitmap getCompressedBitmap(Bitmap image) {

        File imageFile = new File(Environment.getExternalStorageDirectory(), Calendar.getInstance().getTimeInMillis() + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            image.compress(Bitmap.CompressFormat.JPEG, 60, os);
            os.flush();
            os.close();

            Bitmap file = new Compressor(BaseActivity.this)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setQuality(60)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    /*.setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())*/
                    .compressToBitmap(imageFile);

            imageFile.delete();

            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return image;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.ALLOW_OPEN_PROFILE = false;
        Log.d("swipeHelper9onBack", Constants.ALLOW_OPEN_PROFILE + "");
    }

    public void appendLog(String text) {

        try {
            File root = new File(Environment.getExternalStorageDirectory(), "AfrocamgistLogs");
            if (!root.exists()) {
                root.mkdirs();
            }
            String fileName = "android_forgot_password_" + ".txt";
            File gpxfile = new File(root, fileName);
            BufferedWriter buf = new BufferedWriter(new FileWriter(gpxfile, true));
            buf.append(text);
            buf.newLine();
            buf.close();

            //uploadLogFileToServer(BaseActivity.this,fileName);

        } catch (IOException e) {
            e.printStackTrace();
        }

        /*File logFile = new File("sdcard/log9.txt");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
    }

    private void uploadLogFileToServer(Context _context, String fileName) {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistLogs/";
        File crashLogFile = new File(path + fileName);
        if (crashLogFile.exists()) {
            Log.d("response99", "exists....");
            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            builder.addFormDataPart(Constants.MEDIA, crashLogFile.getName(), RequestBody.create(MediaType.parse("text/plain"), crashLogFile));

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
            //headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            Webservices.getData((Activity) _context, Webservices.Method.POST, builder, headers, UrlEndpoints.UPLOAD_CRASH_FILE, response -> {
                if ("".equals(response)) {
                    Utils.showAlert((Activity) _context, "Oops something went wrong....");
                } else {
                    clearLogFile();
                    try {
                        Log.d("response99", "" + response.toString());
                        JSONObject object = new JSONObject(response);
                        if (object.has(Constants.MESSAGE)) {

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        //Utils.showAlert(getActivity(), "Oops something went wrong....");
                    }
                }
            });
        }
    }

    private void clearLogFile() {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "AfrocamgistLogs");
            if (!root.exists()) {
                root.mkdirs();
            }
            String fileName = "android_forgot_password_" + ".txt";
            File gpxfile = new File(root, fileName);
            PrintWriter writer = new PrintWriter(gpxfile);
            writer.print("");
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void uploadFile() {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "AfrocamgistLogs");
            if (!root.exists()) {
                root.mkdirs();
            }

            // Source File
            String fileName = "android_forgot_password_" + ".txt";
            File gpxfile = new File(root, fileName);

            Random generator = new Random();
            int random = generator.nextInt(99999);

            //Destination file
            String destinationFile = "android_forgot_password_" + random + ".txt";
            File gpxfile2 = new File(root, destinationFile);

            copy(gpxfile, gpxfile2);

            uploadLogFileToServer(BaseActivity.this, destinationFile);

        } catch (Exception e) {
            e.printStackTrace();
        }


        //uploadLogFileToServer(BaseActivity.this,fileName);
    }

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }
}