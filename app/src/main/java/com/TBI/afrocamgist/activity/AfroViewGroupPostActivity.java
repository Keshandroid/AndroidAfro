package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.os.PowerManager;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.TagUser.TagToBeTagged;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.AfroGroupPostAdapter;
import com.TBI.afrocamgist.adapters.AfroMyGroupAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.bottomsheet.GroupCommentBottomSheetFragment;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.SwaggerCommentBottomSheetFragment;
import com.TBI.afrocamgist.fragment.TagBottomSheetFragment;
import com.TBI.afrocamgist.home.VideosAdapter;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.groups.GroupDetails;
import com.TBI.afrocamgist.model.media.Media;
import com.TBI.afrocamgist.model.media.MediaDetails;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import im.ene.toro.widget.Container;
import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnMultiCompressListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.TBI.afrocamgist.activity.CreatePostActivity.location1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postText1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postType1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.tagUserList1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.video1;
import static com.TBI.afrocamgist.fragment.AfroAllForumFragment.NOTIFY_JOIN_STATE_FORUM;

public class AfroViewGroupPostActivity extends BaseActivity implements AfroGroupPostAdapter.OnGroupClickListener,
        GroupCommentBottomSheetFragment.DialogDismissListener{

    private KProgressHUD hud;
    private Container groupPosts;
    private Connectivity mConnectivity;
    private SwipeRefreshLayout swipeContainer;
    private int nextPage = 1;
    private boolean isLoading = false;
    private AfroGroupPostAdapter groupPostAdapter;
    private ImageView scrollToTop, settings;
    private ArrayList<Post> posts = new ArrayList<>();
    private Group group;
    private static int SAVED_SCROLL_POSITION = -1;
    private File videoFile;
    private ArrayList<File> imageFiles = new ArrayList<>();
    private ArrayList<File> compressedImageFiles = new ArrayList<>();
    //private ArrayList<TagToBeTagged> tagUserList = new ArrayList<>();

    private List<String> tagUserList = new ArrayList<>();

    private String postText = "", location="";
    private  String notification_type="";

    protected LocalBroadcastManager localBroadcastManager;

    //Download Video from url
    String fileN = null ;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123;
    boolean result;
    ProgressDialog mProgressDialog;
    private String waterMarkVideo;
    private Dialog popup;
    private String shareOrDownload = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afro_view_group_post);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        /*localBroadcastManager.registerReceiver(mFollowStateReceiver,
                new IntentFilter(ProfileActivity.NITIFY_FOLLOW_STATE));*/

        initView();
        initSwipeRefresh();

        initClickListener();
        setData();

        //initInternetConnectivityCheckListener();

        if (Utils.isConnected()) {
            getGroupDetails();
            getFirstPageGroupPosts();
        }else{
            Utils.showAlert(this, "Please check your internet connection");
        }

    }

    //Initialise all the view
    private void initView() {

        groupPosts = findViewById(R.id.group_posts);
        scrollToTop = findViewById(R.id.scroll_to_top);
        settings = findViewById(R.id.settings);
        swipeContainer = findViewById(R.id.swipeContainer);
    }

    /*private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }*/

    /*@Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud!=null && hud.isShowing()){
                hud.dismiss();
            }
            Utils.closeAlert();
            getGroupDetails();
            getFirstPageGroupPosts();
        } else {
            Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.no_internet_connection_available));
        }
    }*/

    //Initialise swipe refresh
    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected()) {
                getGroupDetails();
                getFirstPageGroupPosts();
            } else
                Utils.showAlert(this, getString(R.string.please_check_your_internet_connection));
        });
    }

    protected void notifyJoinState(){
        Intent intent = new Intent(NOTIFY_JOIN_STATE_FORUM);
        localBroadcastManager.sendBroadcast(intent);
    }

    //Initialise all click listeners
    private void initClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.settings).setOnClickListener(v -> startActivityForResult(new Intent(this, EditGroupActivity.class)
                .putExtra("group",group),100));

        scrollToTop.setOnClickListener(v -> {
            if (groupPosts.getLayoutManager()!=null) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) groupPosts.getLayoutManager();
                if (layoutManager.findFirstVisibleItemPosition() < 15)
                    groupPosts.getLayoutManager().smoothScrollToPosition(groupPosts, new RecyclerView.State(),0);
                else
                    groupPosts.getLayoutManager().scrollToPosition(0);
            }
        });
    }

    //set data to be displayed on the repective views
    private void setData() {

        if (getIntent().getSerializableExtra("group") != null) {
            group = (Group) getIntent().getSerializableExtra("group");
            if (group.getGroupAdmins()==null) {
                settings.setImageDrawable(getResources().getDrawable(R.drawable.ic_view));
            } else {
                if (group.getGroupAdmins().contains(LocalStorage.getUserDetails().getUserId())) {
                    settings.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_black));
                } else {
                    settings.setImageDrawable(getResources().getDrawable(R.drawable.ic_view));
                }
            }
        }
        if(getIntent().hasExtra("notification_type")){
            notification_type = getIntent().getStringExtra("notification_type");
        }
    }

    //get first page posts of the particular group
    private void getFirstPageGroupPosts() {

        if (group != null) {

            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();

            String url = UrlEndpoints.GROUP_POSTS + group.getGroupId() + "?page=1";

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            Logger.d("groupId1",""+group.getGroupId());
            Logger.d("TOKEN",""+LocalStorage.getLoginToken());

            Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
                swipeContainer.setRefreshing(false);
                hud.dismiss();
                if ("".equals(response)) {
                    Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                } else {
                    try {
                        JSONObject object = new JSONObject(response);

                        posts = new ArrayList<>();
                        posts.add(0, new Post());
                        if (!object.has(Constants.MESSAGE)) {
                            PostDetails groupPostsDetails = new Gson().fromJson(response, PostDetails.class);
                            nextPage = groupPostsDetails.getNextPage();
                            posts.add(1, new Post());
                            if (groupPostsDetails.getPosts().size() > 0) {
                                posts.addAll(groupPostsDetails.getPosts());
                            }
                        }
                        setRecyclerView();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            });
        }
    }

    //set the posts fetched from server in the list
    private void setRecyclerView() {

        groupPosts.setLayoutManager(new LinearLayoutManager(this));
        groupPostAdapter = new AfroGroupPostAdapter(this, posts, group, this);
        groupPosts.setAdapter(groupPostAdapter);
        groupPosts.setNestedScrollingEnabled(false);
        groupPosts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = 0, totalItemCount = 0, pastVisibleItem = 0;

                if (recyclerView.getLayoutManager() != null) {
                    visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    pastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }

                if (pastVisibleItem > 2) {
                    scrollToTop.setVisibility(View.VISIBLE);
                } else {
                    scrollToTop.setVisibility(View.GONE);
                }

                if (!isLoading && dy > 0 && ((visibleItemCount + pastVisibleItem) >= totalItemCount)) {
                    isLoading = true;
//                    posts.add(null);
//                    groupPostAdapter.notifyItemInserted(posts.size() - 1);


                    getGroupNextPagePosts();

                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            posts.remove(posts.size() - 1);
                            groupPostAdapter.notifyItemRemoved(posts.size());
                            getGroupNextPagePosts();
                        }
                    }, 1000);*/
                }
            }
        });
    }

    //get the next page posts from the server and append to the existing list in the view
    private void getGroupNextPagePosts() {

        String url = UrlEndpoints.GROUP_POSTS + group.getGroupId() + "?page=" + nextPage;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if (!"".equals(response)) {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails groupPostDetails = new Gson().fromJson(response, PostDetails.class);

//                        posts.remove(posts.size() - 1);
//                        groupPostAdapter.notifyItemRemoved(posts.size());

                        if (groupPostDetails.getPosts()!=null && groupPostDetails.getPosts().size() > 0) {
                            nextPage = groupPostDetails.getNextPage();
                            posts.addAll(groupPostDetails.getPosts());
                            groupPostAdapter.notifyDataSetChanged();
                        }
                        isLoading = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    //fetch group details from the server
    private void getGroupDetails() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/json");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.GROUP + "/" + group.getGroupId();

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        group = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), Group.class);
                        setGroupDetails(group);
                    }

                } catch (Exception e) {
                    Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    //set fetched group details on the view
    private void setGroupDetails(Group group) {

        //Logger.d("groupMemberDetail", new GsonBuilder().setPrettyPrinting().create().toJson(group.getGroupMembersDetails()));

        String title = group.getGroupTitle();
        ((TextView)findViewById(R.id.title)).setText(title);

        if (groupPostAdapter!=null)
            groupPostAdapter.setGroupDetails(group);
    }

    @Override
    public void onDeletePost(Post post) {
        showConfirmDeletePostAlert(post);
    }

    private void showConfirmDeletePostAlert(Post post) {

        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    deletePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    //method to delete particular post from the list using the post id.
    private void deletePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(AfroViewGroupPostActivity.this, getString(R.string.post_deleted_successfully), Toast.LENGTH_LONG).show();
                        getFirstPageGroupPosts();
                    }

                } catch (Exception e) {
                    Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onJoinGroupClick() {
        joinGroup();
    }

    @Override
    public void onLeaveGroupClick() {
        showConfirmLeaveGroupAlert();
    }

    private void showConfirmLeaveGroupAlert() {
        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        TextView txtMessage =popup.findViewById(R.id.message);
        txtMessage.setText(getString(R.string.leave_group_alert_message));
        TextView txtLeave =popup.findViewById(R.id.delete);
        txtLeave.setText(getString(R.string.str_leave));
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    leaveGroup();
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    //join the group
    private void joinGroup() {
        if (group != null) {
            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            String url = (notification_type.equals(Constants.GROUP_INVITE) ? UrlEndpoints.ACCEPT_GROUP : UrlEndpoints.JOIN_GROUP) + group.getGroupId();

            Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                        }

                        notifyJoinState();

                        getGroupDetails();
                        getFirstPageGroupPosts();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            });
        }
    }

    //leave the group
    private void leaveGroup() {

        if (group != null) {

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            String url = UrlEndpoints.LEAVE_GROUP + group.getGroupId();

            Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                        }

                        notifyJoinState();

                        getGroupDetails();
                        getFirstPageGroupPosts();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            });
        }
    }

    @Override
    public void onVideoCount(int videoPostId) {
        Logger.e("VIDEO_POST_ID1", "111"+videoPostId);
        callVideoCountNew(videoPostId);
    }

    @Override
    public void onPostViewCount(int postId) {
        callPostViewCount(postId);
    }

    private void callPostViewCount(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getPostViewCount(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onTagClicked(ArrayList<String> taggedIds) {
        TagBottomSheetFragment.newInstance(taggedIds).show(getSupportFragmentManager(), "ModalBottomSheet");
    }

    private void callVideoCountNew(int videoPostId){
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getVideoCount(videoPostId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("ViewCount",""+jsonObject.get("counter"));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onPostChecked(Post post) {
        likeAndUnlikePost(post.getPostId());
    }

    //method to like and unlike the group post
    private void likeAndUnlikePost(Integer postId) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id",postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    /*private BroadcastReceiver mFollowStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int user_id = intent.getIntExtra(ProfileActivity.User_Id,0);
            boolean isfollow = intent.getBooleanExtra(ProfileActivity.IsFollow,false);
            setFollowAllUser(user_id,isfollow);
        }
    };*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }*/

        //localBroadcastManager.unregisterReceiver(mFollowStateReceiver);
    }

    @Override
    public void onFollowClicked(Post post) {
        followUser(post.getUserId());
        //setFollowAllUser(post.getUserId(),true);
    }

    @Override
    public void onGroupMemberClicked() {
        startActivity(new Intent(AfroViewGroupPostActivity.this, GroupMemberActivity.class).putExtra("groupId", group.getGroupId()));
    }


    //follow user
    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {}
        });
    }

    private void setFollowAllUser(int userid, boolean isfollow){
        try {
            for (int i = 0; i< posts.size();i++){
                if(posts.get(i).getUserId() !=null && userid == posts.get(i).getUserId()){
                    if(posts.get(i).getPrivateAccount()){

                        if(isfollow){
                            posts.get(i).getRequestButtons().get(0).setButtonText("Requested");
                            posts.get(i).setFollowing(false);
                        }else {
                            posts.get(i).getRequestButtons().get(0).setButtonText("Request");
                            posts.get(i).setFollowing(false);
                        }

                    }else {
                        posts.get(i).setFollowing(isfollow);
                    }
                    groupPostAdapter.notifyItemChanged(i);
                }
            }
            //groupPostAdapter.notifyDataSetChanged();

        }catch (Exception e){
        }

    }

    private void updatePostText(String postText) {
        if (posts.size() > 0) {
            posts.get(SAVED_SCROLL_POSITION).setPostText(postText);

            if (groupPostAdapter!=null) {
                groupPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }

        }
    }

    @Override
    public void onEditPost(Post post, int position) {

        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(this, EditPostActivity.class)
                .putExtra("post", post), 102);
    }

    @Override
    public void onHidePost(Post post) {

    }

    private void showConfirmHidePostAlert(Post post) {

        Dialog popup = new Dialog(AfroViewGroupPostActivity.this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        TextView message = popup.findViewById(R.id.message);
        TextView delete = popup.findViewById(R.id.delete);

        delete.setText(getString(R.string.hide));
        message.setText(getString(R.string.are_you_sure_you_want_to_hide_the_post));

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    hidePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void hidePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId + UrlEndpoints.HIDE;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {
                    Logger.e("LLLLLL_HideRes: ",response);
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(AfroViewGroupPostActivity.this, "Post hide", Toast.LENGTH_LONG).show();
                        getFirstPageGroupPosts();
                    }

                } catch (Exception e) {
//                    Logger.e("LLLLLL_Hide: ",e.getMessage());
                    Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onCommentClicked(Post post, int position, AfroGroupPostAdapter.RecyclerObjectHolder holder) {

        /*SAVED_SCROLL_POSITION = position;
        startActivityForResult(new Intent(this, CommentsActivity.class)
                .putExtra("comments",post.getComments())
                .putExtra("postId",post.getPostId()),101);*/

        SAVED_SCROLL_POSITION = position;
        GroupCommentBottomSheetFragment groupCommentBottomSheetFragment = new GroupCommentBottomSheetFragment();
        GroupCommentBottomSheetFragment.newInstance(holder, post.getComments(),post.getPostId(),AfroViewGroupPostActivity.this).show(getSupportFragmentManager(), groupCommentBottomSheetFragment.getTag());
    }

    @Override
    public void onCreatePostClicked(Integer groupId) {
        startActivityForResult(new Intent(this, CreatePostActivity.class)
                .putExtra("postFor","group")
                .putExtra("groupId", groupId),100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (video1 != null && !video1.equals("") && data == null) {
            if (posts != null) {
                posts.add(2, null);
                groupPostAdapter.notifyDataSetChanged();
            }
            postText = postText1;
            location = location1;
            tagUserList = tagUserList1;
            createPost(null);
        }

        if (requestCode == 100 && resultCode == RESULT_OK) {
            if (data != null && data.getSerializableExtra("postType")!=null) {
                if (posts!=null) {
                    posts.add(2, null);
                    groupPostAdapter.notifyDataSetChanged();
                }
                postText = data.getStringExtra("postText");
                location = data.getStringExtra("location");
                tagUserList = (ArrayList<String>) data.getSerializableExtra("tagUserList");
                createPost(data);
            } else if (postText1!=null && !postText1.equals("") && data == null) {
                if (posts!=null) {
                    posts.add(2, null);
                    groupPostAdapter.notifyDataSetChanged();
                }
                postText = postText1;
                location = location1;
                createPost(null);
            } else {
                if (Utils.isConnected()) {
                    getGroupDetails();
                    getFirstPageGroupPosts();
                }
            }
        } else if (requestCode == 101 && resultCode == RESULT_OK) {
            /*if (data!=null && data.getSerializableExtra("comments")!=null) {
                updateCommentCount((ArrayList<Comment>) data.getSerializableExtra("comments"));
            }*/
        } else if (requestCode == 102 && resultCode == RESULT_OK) {
            if (data != null && data.getStringExtra("postText")!=null) {
                updatePostText(data.getStringExtra("postText"));
            }
        }
    }


    private void createPost(Intent data) {

        if (data!=null) {
            CreatePostActivity.PostType postType = (CreatePostActivity.PostType) data.getSerializableExtra("postType");
            switch (postType) {

                case IMAGE:
                    createImagePost(data);
                    break;
                case VIDEO:
                    createVideoPost(data);
                    break;
                default:
                    break;
            }
        } else {
            if (postType1 == CreatePostActivity.PostType.VIDEO) {
                createVideoPost((Intent) null);
            }
        }
    }

    private void createImagePost(Intent data) {
        createImageFile(data.getStringArrayListExtra("images"));
//        uploadImages();
    }

    //create compressed image file for upload
    private void createImageFile(ArrayList<String> imagePaths) {

        imageFiles = new ArrayList<>();
        for (int i = 0; i < imagePaths.size(); i++) {

            try {
                imageFiles.add(new File(imagePaths.get(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Luban.compress(this, imageFiles)
                .putGear(Luban.CUSTOM_GEAR)
                .launch(new OnMultiCompressListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(List<File> fileList) {
                        compressedImageFiles = new ArrayList<>(fileList);
                        for (File image : imageFiles)
                            image.delete();
                        uploadImages();
                    }
                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    //upload images to server
    private void uploadImages() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (File photo : compressedImageFiles) {
            builder.addFormDataPart(Constants.IMAGES, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload";

        Webservices.getData(AfroViewGroupPostActivity.this, Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {
                            for (File image : compressedImageFiles)
                                image.delete();
                            createImagePost(mediaDetails.getMediaList());
                        } else {
                            Utils.showAlert(AfroViewGroupPostActivity.this, "Opps!! Something went wrong please try again later.");
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int)(100 * bytesRead / contentLength);
            Logger.d("Percentage",percentage+"");
            if (groupPostAdapter!=null) {
                groupPostAdapter.setUploadProgress(percentage);
                runOnUiThread(() -> groupPostAdapter.notifyItemChanged(1));
            }
        });
    }

    //create image post using the media array from the response of upload image API
    private void createImagePost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createImagePostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(AfroViewGroupPostActivity.this, "Post created successfully", Toast.LENGTH_LONG).show();
                    if (posts!=null) {
                        posts.remove(2);
                        groupPostAdapter.notifyDataSetChanged();
                    }
                    getGroupDetails();
                    getFirstPageGroupPosts();
                }

            } catch (Exception e) {
                Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    //create JSON params for the image post API
    private JSONObject createImagePostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            request.put("post_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "group");
            request.put("group_id", group.getGroupId());

            JSONArray images = new JSONArray();
            for (Media media : mediaList) {
                images.put(media.getPath());
            }

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("post_type", "image");
            request.put("post_image", images);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void createVideoPost(Intent data) {
        if (data == null) {
            createVideoFile(video1);
        } else {
            createVideoFile(data.getStringExtra("video"));
        }

        uploadVideo();
    }

    private void createVideoFile(String videoPath) {

        try {
            videoFile = new File(videoPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadVideo() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, videoFile.getName(), RequestBody.create(MediaType.parse("video/*"), videoFile));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload-video";

        Webservices.getData(AfroViewGroupPostActivity.this, Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {

                                if (videoFile != null)
                                    videoFile.delete();
                                if (groupPostAdapter != null) {
                                    groupPostAdapter.setUploadProgress(-1);
                                    runOnUiThread(() -> groupPostAdapter.notifyItemChanged(1));
                                }
                            createVideoPost(mediaDetails.getMediaList());
                        } else {
                            Utils.showAlert(AfroViewGroupPostActivity.this, "Opps!! Something went wrong please try again later.");
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int)(100 * bytesRead / contentLength);
            Logger.d("Percentage",percentage+"");
            if (groupPostAdapter!=null) {
                groupPostAdapter.setUploadProgress(percentage);
                runOnUiThread(() -> groupPostAdapter.notifyItemChanged(2));
            }
        });
    }

    private void createVideoPost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createVideoPostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(AfroViewGroupPostActivity.this, "Post created successfully", Toast.LENGTH_LONG).show();
                    if (posts!=null) {
                        posts.remove(2);
                        groupPostAdapter.notifyDataSetChanged();
                    }
                    getGroupDetails();
                    getFirstPageGroupPosts();
                }

            } catch (Exception e) {
                Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private JSONObject createVideoPostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("post_text", postText);
            request.put("post_lat_long", location);
            request.put("posted_for", "group");
            request.put("group_id", group.getGroupId());
            request.put("post_type", "video");
            request.put("post_video", mediaList.get(0).getPath());
            request.put("thumbnail", mediaList.get(0).getThumbnails().get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    //update comment count on the view if any comment is added on a post.
    private void updateCommentCount(ArrayList<Comment> comments, AfroGroupPostAdapter.RecyclerObjectHolder holder) {

        if (posts.size() > 0) {
            int commentCount = 0;
            for(Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments()!=null && comment.getSubComments().size() > 0){
                    commentCount += comment.getSubComments().size();
                }
            }
            posts.get(SAVED_SCROLL_POSITION).setComments(comments);
            posts.get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);

            /*if (groupPostAdapter!=null) {
                groupPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }*/

            if(groupPostAdapter!=null){
                groupPostAdapter.onCommentDialogDismiss(SAVED_SCROLL_POSITION,commentCount,holder);
            }

        }
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().hasExtra("notification_type") && getIntent().getStringExtra("createGroup")==null)
            super.onBackPressed();
        else {
            startActivity(new Intent(AfroViewGroupPostActivity.this, DashboardActivity.class)
                    .putExtra("group", "group")
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }

    @Override
    public void onDownloadClick(String videoUrl, String postLink) {
        Logger.d("vidUrl", ""+videoUrl);
        showShareSelectionDialog(AfroViewGroupPostActivity.this,videoUrl,postLink);
    }


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onDialogDismiss(ArrayList<Comment> comments, AfroGroupPostAdapter.RecyclerObjectHolder holder) {
        updateCommentCount(comments,holder);
    }

    // DownloadTask for downloding video from URL
    public class DownloadTask extends AsyncTask<String, Integer, String> {
        private Context context;
        private PowerManager.WakeLock mWakeLock;
        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                java.net.URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                input = connection.getInputStream();


                String filePath = sUrl[0];
                fileN = filePath.substring(filePath.lastIndexOf("/")+1);
                //fileN = "Afrocamgist_" + UUID.randomUUID().toString().substring(0, 10) + ".mp4";
                File filename = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/", fileN);
                output = new FileOutputStream(filename);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();

            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
           mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);

            mProgressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();

            if (result != null){
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "Video downloaded", Toast.LENGTH_SHORT).show();
                deleteWatermarkVideoInServer();
            }

            MediaScannerConnection.scanFile(AfroViewGroupPostActivity.this,
                    new String[]{Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/AfrocamgistVideos/" + fileN}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String newpath, Uri newuri) {
                            Logger.i("ExternalStorage", "Scanned " + newpath + ":");
                            Logger.i("ExternalStorage", "-> uri=" + newuri);
                        }
                    });

            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + fileN;
            shareVideoToSocialMedia(filePath);
        }
    }

    //Get watermarked video from url
    private void callWatermarkVideoAPI(String videoUrl){
        waterMarkVideo = "";
        hud = KProgressHUD.create(AfroViewGroupPostActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setLabel("Preparing...")
                .setCancellable(false)
                .show();

        JSONObject params = new JSONObject();
        try {
            params.put("video", videoUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.WATERMARK_VIDEO, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(AfroViewGroupPostActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        if(object.getBoolean("result")){
                            waterMarkVideo = object.getString("video_path");
                            String watermarkVideo = UrlEndpoints.MEDIA_BASE_URL + object.getString("video_path");
                            newDownload(watermarkVideo);
                        }else {
                            Utils.showAlert(AfroViewGroupPostActivity.this, "Failed to get video from the server");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //delete watermark video from server
    private void deleteWatermarkVideoInServer(){
        JSONObject params = new JSONObject();
        try {
            params.put("video", waterMarkVideo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.DELETE_WATERMARK_VIDEO;

        Webservices.getData(Webservices.Method.DELETE, params, headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.d("waterMark99","success");
                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AfroViewGroupPostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showShareSelectionDialog(Context context, String videoUrl, String postLink) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_social_share);
            popup.setCancelable(true);
            popup.show();

            RelativeLayout download = popup.findViewById(R.id.rlDownload);
            RelativeLayout share = popup.findViewById(R.id.rlShare);


            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "download";
                    downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "share";
                    shareVideoLink(postLink);
                    //downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

        }
    }

    private void downloadVideo(String videoUrl){
        String originalFilename = videoUrl.substring(videoUrl.lastIndexOf("/")+1);
        result = checkPermission();
        if(result){
            checkFolder();
            if (!isConnectingToInternet(AfroViewGroupPostActivity.this)) {
                Toast.makeText(AfroViewGroupPostActivity.this, "Please Connect to Internet", Toast.LENGTH_LONG).show();
                return;
            }

            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename);
            if(file.exists()){
                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename;
                shareVideoToSocialMedia(filePath);
            }else {
                callWatermarkVideoAPI(videoUrl);
            }
        }
    }

    private void shareVideoLink(String videoUrl){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
        i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
        i.putExtra(Intent.EXTRA_TEXT, videoUrl);
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    private void shareVideoToSocialMedia(String filePath){

        MediaScannerConnection.scanFile(AfroViewGroupPostActivity.this, new String[] { filePath },

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                        shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, "Afrocamgist");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);


                        startActivity(Intent.createChooser(shareIntent,
                                "Afrocamgist"));

                    }
                });
    }

    //hare you can start downloding video
    public void newDownload(String url) {
        final DownloadTask downloadTask = new DownloadTask(AfroViewGroupPostActivity.this);
        downloadTask.execute(url);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(AfroViewGroupPostActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(AfroViewGroupPostActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AfroViewGroupPostActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(AfroViewGroupPostActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(AfroViewGroupPostActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public void checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(AfroViewGroupPostActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AfroViewGroupPostActivity.this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(AfroViewGroupPostActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(AfroViewGroupPostActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }

    //Here you can check App Permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkFolder();
                } else {
                    //code for deny
                    checkAgain();
                }
                break;
        }
    }

    //hare you can check folfer whare you want to store download Video
    public void checkFolder() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/";
        File dir = new File(path);
        boolean isDirectoryCreated = dir.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated = dir.mkdir();
        }
        if (isDirectoryCreated) {
            // do something
            Logger.d("Folder", "Already Created");
        }
    }
    
}
