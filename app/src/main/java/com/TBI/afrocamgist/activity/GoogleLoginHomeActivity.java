package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Params;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.user.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GoogleLoginHomeActivity extends BaseActivity implements View.OnClickListener{

    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;

    SignInButton signInButton;
    Button btnSignOut;
    private ImageView imgProfilePic;
    private TextView txtName, txtEmail;
    private static final int RC_SIGN_IN =7;
    private static final String TAG = GoogleLoginHomeActivity.class.getSimpleName();

    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_login_home);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);

        btnSignOut = (Button) findViewById(R.id.btn_sign_out);
        btnSignOut.setOnClickListener(this);
        imgProfilePic = (ImageView) findViewById(R.id.imgProfilePic);
        txtName = (TextView) findViewById(R.id.txtName);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.btn_sign_out:
                signOut();
                break;
        }
    }

    private void signIn() {
        //  Toast.makeText(this,"signIn method called",Toast.LENGTH_SHORT).show();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //  Toast.makeText(this,"OnActivityResultCalled"+requestCode,Toast.LENGTH_SHORT).show();
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            String personName = account.getDisplayName();
            String personPhotoUrl = account.getPhotoUrl().toString();
            String email = account.getEmail();
            String googleId = account.getId();
            txtName.setText(personName);
            txtEmail.setText(email);
            Glide.with(getApplicationContext()).load(personPhotoUrl)
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfilePic);
            // Signed in successfully, show authenticated UI.

            login(account);

            //updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Logger.w(TAG, "signInResult:failed code=" + e.getStatusCode()+"\n"+e.getLocalizedMessage()+"\nMEssg: "+e.getMessage());

            login(null);
            //updateUI(null);
        }
    }
    private void updateUI(GoogleSignInAccount account) {

        if(account==null)
        {
            // Toast.makeText(this, "not signed in", Toast.LENGTH_SHORT).show();
            signInButton.setVisibility(View.VISIBLE);
            btnSignOut.setVisibility(View.GONE);
            imgProfilePic.setVisibility(View.GONE);
            txtEmail.setVisibility(View.GONE);
            txtName.setVisibility(View.GONE);
            Toast.makeText(this,"please sign in",Toast.LENGTH_SHORT).show();
        }
        else {
            signInButton.setVisibility(View.GONE);
            imgProfilePic.setVisibility(View.VISIBLE);
            txtEmail.setVisibility(View.VISIBLE);
            txtName.setVisibility(View.VISIBLE);
            btnSignOut.setVisibility(View.VISIBLE);
            Toast.makeText(this,"you are signed in", Toast.LENGTH_SHORT).show();
        }

    }
    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                //updateUI(null);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        //updateUI(account);
        login(account);
    }

    private void login(GoogleSignInAccount account) {

        if(account!=null){
            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();

            String[] splitString = account.getDisplayName().split("\\s+");
            Logger.d("first_last","=="+splitString[0]+"=="+splitString[1]);

            ArrayList<Params> params = new ArrayList<>();
            params.add(new Params("google_id", account.getId()));
            params.add(new Params("first_name", splitString[0]));
            params.add(new Params("last_name", splitString[1]));
            params.add(new Params("email", account.getEmail()));
            params.add(new Params("username", account.getId()));
            params.add(new Params("type", "google"));

            Logger.d("paramGoogle",""+params.toString()+""+params);


            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

            Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LOGIN, response -> {
                hud.dismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    Logger.e("LLLLLL_Res: ",response);

                    LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                    //LocalStorage.setUserPass(password.getText().toString().trim());
                    User user = new Gson().fromJson(object.getJSONObject(Constants.USER).toString(), User.class);
                    LocalStorage.saveUserDetails(user);

                    Toast.makeText(GoogleLoginHomeActivity.this, "User logged in successfully", Toast.LENGTH_LONG).show();
                    getAfroPopularPosts();
                    if ("google".equals(user.getRegisteredWith())) {
                        if (user.getIntroduced()) {
                            LocalStorage.setIsUserLoggedIn(true);
                            proceedToDashboard();
                        } else {
                            LocalStorage.setIsUserLoggedIn(true);
                            startActivity(new Intent(GoogleLoginHomeActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                        }
                    } else {
                        if (LocalStorage.getIsUserLoggedIn()) {
                            LocalStorage.setIsUserLoggedIn(true);
                            proceedToDashboard();
                        } else {
                            LocalStorage.setIsUserLoggedIn(true);
                            startActivity(new Intent(GoogleLoginHomeActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                        }
                    }

                } catch (Exception e) {
                    try {
                        JSONObject object = new JSONObject(response);
                        Logger.e("LLLLLL_Data: ",response);
                        Utils.showAlert(GoogleLoginHomeActivity.this, getString(R.string.please_enter_valid_email_or_password));
                    } catch (JSONException e1) {
                        Utils.showAlert(GoogleLoginHomeActivity.this, getString(R.string.opps_something_went_wrong));
                        e1.printStackTrace();
                    }
                }
            });
        }
    }

    private void proceedToDashboard() {
        //make bubble screen the main screen
        /*Intent intent = new Intent(GoogleLoginHomeActivity.this, BubbleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();*/

        Intent intent = new Intent(GoogleLoginHomeActivity.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void getAfroPopularPosts() {
        Logger.e("LLLL_Bare: ",LocalStorage.getLoginToken());
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MOST_POPULAR, response -> {
            if ("".equals(response)) {
                Utils.showAlert(GoogleLoginHomeActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(GoogleLoginHomeActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);

                        if (postDetails.getPosts() != null && postDetails.getPosts().size() != 0) {
                            LocalStorage.savePopularPosts(postDetails.getPosts());
                        }

                    }

                } catch (Exception e) {
//                    Logger.e("LLLL_Data: ",e.getMessage());
                    e.printStackTrace();
                    Utils.showAlert(GoogleLoginHomeActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

}
