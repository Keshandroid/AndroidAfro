package com.TBI.afrocamgist.activity;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.MyFollowerAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.follow.FollowDetails;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyFollowersActivity extends BaseActivity {

    private KProgressHUD hud;
    private TextView followerCount;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follower);

        initView();
        initSwipeRefresh();
        setClickListener();

        if (Utils.isConnected())
            getFollowersList();
        else
            Utils.showAlert(this,getString(R.string.please_check_your_internet_connection));
    }

    private void initView() {

        followerCount = findViewById(R.id.follower_count);
        swipeContainer = findViewById(R.id.swipeContainer);
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getFollowersList();
            else
                Utils.showAlert(this,getString(R.string.please_check_your_internet_connection));
        });
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private void getFollowersList() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.FOLLOWERS, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                swipeContainer.setRefreshing(false);
                hud.dismiss();
                if ("".equals(response)) {
                    Utils.showAlert(MyFollowersActivity.this, getString(R.string.opps_something_went_wrong));
                } else {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(MyFollowersActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            FollowDetails followDetails = new Gson().fromJson(response,FollowDetails.class);
                            String count = followDetails.getFollowList()== null ? "0" : followDetails.getFollowList().size() + "";
                            followerCount.setText(count);
                            if (followDetails.getFollowList()==null || followDetails.getFollowList().isEmpty()) {
                                findViewById(R.id.no_followers).setVisibility(View.VISIBLE);
                                findViewById(R.id.follower_list).setVisibility(View.GONE);
                            } else {
                                setRecyclerView(followDetails.getFollowList());
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showAlert(MyFollowersActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            }
        });
    }

    private void setRecyclerView(ArrayList<Follow> followers) {

        RecyclerView followerList = findViewById(R.id.follower_list);
        followerList.setLayoutManager(new LinearLayoutManager(this));
        followerList.setAdapter(new MyFollowerAdapter(this, followers));
    }
}
