package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.ProfilePostAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.bottomsheet.ProfileCommentBottomSheetFragment;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.AfroSwaggerFragment;
import com.TBI.afrocamgist.fragment.SwaggerCommentBottomSheetFragment;
import com.TBI.afrocamgist.fragment.TagBottomSheetFragment;
import com.TBI.afrocamgist.home.VideosAdapter;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.follow.FollowDetails;
import com.TBI.afrocamgist.model.otheruser.OtherUser;
import com.TBI.afrocamgist.model.otheruser.RequestButton;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.suggestedpeople.SuggestedPeopleDetails;
import com.TBI.bubblesview.Constant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import im.ene.toro.widget.Container;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.TBI.afrocamgist.activity.SplashActivity.isNotification;
import static com.TBI.afrocamgist.fragment.AfroCreatePostFragment.REFRESH_SWAGGER_POSTS;

public class ProfileActivity extends BaseActivity implements ConnectivityListener, ProfilePostAdapter.OnPostClickListener,
        ProfileCommentBottomSheetFragment.DialogDismissListener{

    private Container profilePosts;
    private ImageView scrollToTop;
    private ImageView more;
    private TextView tvBlock;
    private LinearLayout ll_cancel;
    private LinearLayout ll_more,follow;
    private SwipeRefreshLayout swipeContainer;
    private KProgressHUD hud;
    private Connectivity mConnectivity;
    private Integer userId;
    private String fromSwipe="";
    private ProfilePostAdapter profilePostAdapter;
    private int nextPage = 1;
    private boolean isLoading = false;
    public static String NITIFY_FOLLOW_STATE="notify_follow_state";

    public static String User_Id="user_id";
    public static String IsFollow="isfollow";
    public static String POST_ID="post_id";
    private static int SAVED_SCROLL_POSITION = -1;
    private ArrayList<Post> posts = new ArrayList<>();
    protected LocalBroadcastManager localBroadcastManager;
    private Button accept,reject;

    private boolean idAvailable = false;

    //Download Video from url
    String fileN = null ;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123;
    boolean result;
    ProgressDialog mProgressDialog;
    private String waterMarkVideo;
    private Dialog popup;
    private String shareOrDownload = "";
    private boolean isFollow;

    private Dialog popup1;

    public static void expand(final View v) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        userId = getIntent().getIntExtra("userId", 0);

        if(getIntent().getStringExtra("fromSwipe") !=null){
            fromSwipe = getIntent().getStringExtra("fromSwipe");
        }

        Logger.d("AFROCAMGIST_LOG","userId="+userId);

        initView();
        initSwipeRefresh();

        /*if (Utils.isConnected()) {
            getUserProfile();
        } else {
            swipeContainer.setRefreshing(false);
            Utils.showAlert(ProfileActivity.this, "No internet connection available.");
        }*/

        initClickListener();
        initInternetConnectivityCheckListener();
        setPostsRecyclerView();

    }

    private void checkFollowState(OtherUser profileUser) {
        if(profileUser.getIsMyFriend()!=null){
            if (profileUser.getIsMyFriend()){
                isFollow = true;
            } else {
                isFollow = false;
                if(checkIsRequested(profileUser))
                {
                    isFollow = true;
                }
            }
        }
    }

    private boolean checkIsRequested(OtherUser profileUser) {
        if(profileUser!=null)
        {
            boolean isRequestSend = false;
            for (RequestButton requestButton : profileUser.getRequestButtons()) {
                if(requestButton.getButtonText().equalsIgnoreCase("Follow Requested"))
                {
                    isRequestSend = true;
                    break;
                }
            }
            return isRequestSend;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        profilePostAdapter.notifyDataSetChanged();
    }

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    private void initView() {

        profilePosts = findViewById(R.id.profile_posts);
        scrollToTop = findViewById(R.id.scroll_to_top);
        swipeContainer = findViewById(R.id.swipeContainer);
        more = findViewById(R.id.more);
        ll_more = findViewById(R.id.ll_more);
        tvBlock = findViewById(R.id.tvBlock);
        ll_cancel = findViewById(R.id.ll_cancel);
        accept = findViewById(R.id.accept);
        reject = findViewById(R.id.reject);
        follow = findViewById(R.id.follow);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestAccept(userId);
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDeny(userId);
            }
        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_more.getVisibility() == View.VISIBLE)
                    collapse(ll_more);
                else
                    expand(ll_more);
            }
        });

        tvBlock.setOnClickListener(v -> {
            if (tvBlock.getText().toString().trim().equals("Block")) {
                blockUnblockUser(false);
            } else {
                blockUnblockUser(true);
            }
        });

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapse(ll_more);
            }
        });

        //Logger.e("LLLLL_Ids: ", userId + "      " + LocalStorage.getUserDetails().getUserId());

        if (userId.equals(LocalStorage.getUserDetails().getUserId())) {
            more.setVisibility(View.GONE);
        } else {
            more.setVisibility(View.VISIBLE);
        }

        profilePosts.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected()) {
                getUserProfile();
            } else {
                swipeContainer.setRefreshing(false);
                //Utils.showAlert(ProfileActivity.this, getString(R.string.no_internet_connection_available));
            }
        });
    }

    @Override
    public void onBackPressed() {


        //super.onBackPressed();


        if (isTaskRoot()){
            isNotification = false;
            startActivity(new Intent(ProfileActivity.this, DashboardActivity.class));
            finish();
        } else {
            if(fromSwipe.equalsIgnoreCase("fromSwipe")){
                Bundle bundle = new Bundle();
                bundle.putString("fromSwipe", "fromSwipe");

                Intent mIntent = new Intent();
                mIntent.putExtras(bundle);
                setResult(RESULT_OK, mIntent);
                notifyFollowState(isFollow);
                super.onBackPressed();
            }else {
                notifyFollowState(isFollow);
                super.onBackPressed();
            }

        }

        /*if (isNotification){
            isNotification = false;
            startActivity(new Intent(ProfileActivity.this, DashboardActivity.class));
            finish();
        } else {
            super.onBackPressed();
        }*/
    }

    private void initClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());

        scrollToTop.setOnClickListener(v -> {
            if (profilePosts.getLayoutManager() != null) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) profilePosts.getLayoutManager();
                if (layoutManager.findFirstVisibleItemPosition() < 15)
                    profilePosts.getLayoutManager().smoothScrollToPosition(profilePosts, new RecyclerView.State(), 0);
                else
                    profilePosts.getLayoutManager().scrollToPosition(0);
            }
        });
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud != null && hud.isShowing()) {
                hud.dismiss();
            }
            Utils.closeAlert();
            getUserProfile();
        } else{
            //Utils.showAlert(ProfileActivity.this, getString(R.string.no_internet_connection_available));
        }

    }

    private void getUserProfile() {
        nextPage = 1;
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId;

        Logger.d("userId00",""+userId);
        Logger.d("LocalToken",""+LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                swipeContainer.setRefreshing(false);
                hud.dismiss();
                getUserFirstPagePosts();
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.e("UserProfile1", response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        OtherUser user = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), OtherUser.class);
                        String title = user.getFirstName() + " " + user.getLastName();
                        ((TextView) findViewById(R.id.title)).setText(title);

                        Logger.e("LLLLL_Block: ", String.valueOf(user.getBlockedByMe()));
                        if (!user.getBlockedByMe()) {
                            tvBlock.setText("Block");
                        } else {
                            tvBlock.setText("Unblock");
                        }

                        Logger.d("userId",LocalStorage.getUserDetails().getUserId()+"");

                        if(user.getFollowingIds()!=null && !user.getFollowingIds().isEmpty()){
                            if(user.getFollowingIds().contains(LocalStorage.getUserDetails().getUserId())){
                                idAvailable = true;
                            }else {
                                idAvailable = false;
                            }
                        }

                        getFollowRequestList();

                        if (profilePostAdapter != null) {
                            profilePostAdapter.setProfileData(user);
                        }


                        if(user!=null){
                            if(user.getIsMyFriend()!=null){
                                checkFollowState(user);
                            }
                        }

                    }

                } catch (Exception e) {
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    Logger.d("AFROCAMGIST_LOG","follow request error..."+e.getLocalizedMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private void setPostsRecyclerView() {

        posts = new ArrayList<>();
        posts.add(0, new Post());
        posts.add(1, new Post());
        posts.add(2, new Post());
        profilePostAdapter = new ProfilePostAdapter(this, posts, this);
        //profilePosts.setNestedScrollingEnabled(false);
        profilePosts.setAdapter(profilePostAdapter);
        profilePosts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = 0, totalItemCount = 0, pastVisibleItem = 0;

                if (recyclerView.getLayoutManager() != null) {
                    visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    pastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }

                if (pastVisibleItem > 1) {
                    scrollToTop.setVisibility(View.VISIBLE);
                } else {
                    scrollToTop.setVisibility(View.GONE);
                }

                if (!isLoading && dy > 0 && ((visibleItemCount + pastVisibleItem) >= (totalItemCount - 1))) {
                    isLoading = true;
//                    swaggerPosts.add(null);
//                    swaggerPostAdapter.notifyItemInserted(swaggerPosts.size() - 1);

                    getUserProfileNextPagePosts();

                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            swaggerPosts.remove(swaggerPosts.size() - 1);
                            swaggerPostAdapter.notifyItemRemoved(swaggerPosts.size());
                            getSwaggerNextPagePosts();
                        }
                    }, 1000);*/
                }
            }
        });
    }

    private void getUserFirstPagePosts() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/posts?page=" + nextPage;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            getSuggestedPeopleList();
            if ("".equals(response)) {
                Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                Logger.d("firstPagePosts","called...2");
                try {
                    JSONObject object = new JSONObject(response);

                    Logger.d("firstPagePosts","called...3"+ object.toString());

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);
                        nextPage = postDetails.getNextPage();
                        if (postDetails.getPosts().size() > 0) {

                            Logger.d("profilePostSize",postDetails.getPosts().size()+"");

                            posts.addAll(postDetails.getPosts());
                            profilePostAdapter.notifyDataSetChanged();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void getSuggestedPeopleList() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.SUGGESTED_PEOPLE, response -> {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
//            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        SuggestedPeopleDetails suggestedPeopleDetails = new Gson().fromJson(response, SuggestedPeopleDetails.class);

                        if (profilePostAdapter != null && suggestedPeopleDetails.getSuggestedPeopleList() != null) {
                            profilePostAdapter.setSuggestedPeople(suggestedPeopleDetails.getSuggestedPeopleList());
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    private void getUserProfileNextPagePosts() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/posts?page=" + nextPage;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);
                        if (postDetails.getPosts() != null && postDetails.getPosts().size() > 0) {
                            nextPage = postDetails.getNextPage();
                            posts.addAll(postDetails.getPosts());
                            profilePostAdapter.notifyDataSetChanged();
                        }
                        isLoading = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    public void onDeletePost(Post post) {
        showConfirmDeletePostAlert(post);
    }

    private void showConfirmDeletePostAlert(Post post) {

        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    deletePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void deletePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(ProfileActivity.this, getString(R.string.post_deleted_successfully), Toast.LENGTH_LONG).show();
                        getUserProfile();
                        nextPage = 1;
                        setPostsRecyclerView();
                    }



                } catch (Exception e) {
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onPostChecked(Post post) {
        likeAndUnlikePost(post.getPostId());
    }

    private void likeAndUnlikePost(Integer postId) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id", postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {
            /*if (data.getSerializableExtra("comments") != null) {
                updateCommentCount((ArrayList<Comment>) data.getSerializableExtra("comments"));
            }*/
        } else if (requestCode == 102 && resultCode == RESULT_OK) {

            if (data.getStringExtra("postText") != null) {
                updatePostText(data.getStringExtra("postText"));
            }
        }
    }

    private void updatePostText(String postText) {
        if (posts.size() > 0) {
            posts.get(SAVED_SCROLL_POSITION).setPostText(postText);

            if (profilePostAdapter != null) {
                profilePostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }

        }
    }

    @Override
    public void onEditPost(Post post, int position) {

        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(this, EditPostActivity.class)
                .putExtra("post", post), 102);
    }

    private void updateCommentCount(ArrayList<Comment> comments,ProfilePostAdapter.RecyclerObjectHolder holder) {
        if (posts.size() > 0) {
            int commentCount = 0;
            for (Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments() != null && comment.getSubComments().size() > 0) {
                    commentCount += comment.getSubComments().size();
                }
            }
            posts.get(SAVED_SCROLL_POSITION).setComments(comments);
            posts.get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);

            /*if (profilePostAdapter != null) {
                profilePostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }*/

            if(profilePostAdapter!=null){
                profilePostAdapter.onCommentDialogDismiss(SAVED_SCROLL_POSITION,commentCount,holder);
            }

        }
    }

    @Override
    public void onCommentClicked(Post post, int position, ProfilePostAdapter.RecyclerObjectHolder holder) {

        /*SAVED_SCROLL_POSITION = position;
        startActivityForResult(new Intent(this, CommentsActivity.class)
                .putExtra("comments", post.getComments())
                .putExtra("postId", post.getPostId()), 101);*/

        SAVED_SCROLL_POSITION = position;
        ProfileCommentBottomSheetFragment profileCommentBottomSheetFragment = new ProfileCommentBottomSheetFragment();
        ProfileCommentBottomSheetFragment.newInstance(holder, post.getComments(),post.getPostId(),ProfileActivity.this).show(getSupportFragmentManager(), profileCommentBottomSheetFragment.getTag());

    }

    @Override
    public void onVideoCount(int videoPostId) {
        Logger.e("VIDEO_POST_ID1", "111"+videoPostId);
        callVideoCountNew(videoPostId);
    }

    @Override
    public void onPostViewCount(int postId) {
        callPostViewCount(postId);
    }

    private void callPostViewCount(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getPostViewCount(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onTagClicked(ArrayList<String> taggedIds) {
        TagBottomSheetFragment.newInstance(taggedIds).show(getSupportFragmentManager(), "ModalBottomSheet");
    }

    private void callVideoCountNew(int videoPostId){
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getVideoCount(videoPostId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("ViewCount",""+jsonObject.get("counter"));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }


    @Override
    public void onFollowUser() {
        followUser();
        this.isFollow = true;
        //notifyFollowState(true);
    }

    private void followUser() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    Logger.d("", new GsonBuilder().setPrettyPrinting().create().toJson(object));

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        getUserProfile();
                        setPostsRecyclerView();
                    }

                } catch (Exception e) {
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onUnfollowUser() {
        unfollowUser();
        this.isFollow = false;
        //notifyFollowState(false);
    }

    private void unfollowUser() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/cancel-follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        getUserProfile();
                        setPostsRecyclerView();
                    }

                } catch (Exception e) {
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onAddRoleModel() {
        addRoleModel();
    }

    private void addRoleModel() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/add-friend";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRemoveRoleModel() {
        removeRoleModel();
    }

    @Override
    public void cancelFollowRequest() {
        cancelFollowUser();
        this.isFollow = false;
        //notifyFollowState(false);
    }

    private void cancelFollowUser() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/cancel-follow-request";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    Logger.d("responseUnfollow", new GsonBuilder().setPrettyPrinting().create().toJson(object));


                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        getUserProfile();
                        setPostsRecyclerView();
                    }

                } catch (Exception e) {
                    //Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void removeRoleModel() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/cancel-friend";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void blockUnblockUser(boolean isBlock) {
        String url = "";
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        if (!isBlock)
            url = UrlEndpoints.PROFILE + "/" + userId + "/block";
        else
            url = UrlEndpoints.PROFILE + "/" + userId + "/unblock";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.e("LLLLL_Res: ", response);
                    if (object.has(Constants.MESSAGE)) {

                        refreshSwaggerPostsBroadcast();

                        showAlertBlock(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    }

                        if (!object.getString(Constants.MESSAGE).equalsIgnoreCase("Unblock Successful")) {
                            tvBlock.setText("Block");
                        } else {
                            tvBlock.setText("Unblock");
                        }

                    if (object.getString(Constants.MESSAGE).equalsIgnoreCase("Block Successful")) {
                        if (ChatActivity.ChatActivityUserWraper.getInstance().getUser() != null) {
                            ChatActivity.ChatActivityUserWraper.getInstance().setBlockedByMe(userId, true);
                        }
                    }
                    if (object.getString(Constants.MESSAGE).equalsIgnoreCase("Unblock Successful")) {
                        if (ChatActivity.ChatActivityUserWraper.getInstance().getUser() != null) {
                            ChatActivity.ChatActivityUserWraper.getInstance().setBlockedByMe(userId, false);
                        }
                    }

                    getUserProfile();

                    collapse(ll_more);


                } catch (Exception e) {
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    public void refreshSwaggerPostsBroadcast() {
        Intent intent = new Intent(REFRESH_SWAGGER_POSTS);
        localBroadcastManager.sendBroadcast(intent);

    }

    public void showAlertBlock(Activity activity, String message) {

        if (activity != null && !activity.isFinishing()) {
            popup1 = new Dialog(activity, R.style.DialogCustom);
            popup1.setContentView(R.layout.dialog_error);
            popup1.setCancelable(true);
            popup1.show();

            TextView error = popup1.findViewById(R.id.message);
            error.setText(message);
            popup1.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(message.equalsIgnoreCase("Block Successful")){
                        finish();
                    }else {
                        popup1.dismiss();
                    }
                }
            });

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean containsUserId(final List<Follow> list, final int userId){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return list.stream().filter(o -> o.getUserId() == userId).findFirst().isPresent();
        }else {
            for(int i=0; i<list.size(); i++){
                if(list.get(i).getUserId() == userId){
                    return true;
                }
            }
        }
        return false;
    }

    private void getFollowRequestList() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.FOLLOW_REQUESTS, new OnTaskCompleted() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(String response) {
                if ("".equals(response)) {
                    Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                } else {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            FollowDetails followDetails = new Gson().fromJson(response,FollowDetails.class);

                            if (followDetails.getFollowList()==null || followDetails.getFollowList().isEmpty()) {
                                findViewById(R.id.no_follow_request).setVisibility(View.VISIBLE);
                                findViewById(R.id.follow_request_list).setVisibility(View.GONE);
                            } else {
                                List<Follow> followList = followDetails.getFollowList();

                                if(containsUserId(followList,userId)){
                                    if(!idAvailable){
                                        follow.setVisibility(View.VISIBLE);
                                    }else {
                                        follow.setVisibility(View.GONE);
                                    }
                                }else {
                                    follow.setVisibility(View.GONE);
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        //Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            }
        });
    }
    
    private void requestAccept(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/confirm-follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        //getFollowRequestList();
                        follow.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    //Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void requestDeny(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/cancel-follow-request";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        //getFollowRequestList();
                        follow.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    //Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
    }

    protected void notifyFollowState(boolean isFollow){
        Intent intent = new Intent(NITIFY_FOLLOW_STATE);
        intent.putExtra(User_Id, userId);
        intent.putExtra(IsFollow, isFollow);
        localBroadcastManager.sendBroadcast(intent);
    }

    @Override
    public void onDownloadClick(String videoUrl, String postLink) {
        Logger.d("vidUrl", ""+videoUrl);
        showShareSelectionDialog(ProfileActivity.this,videoUrl,postLink);
    }


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onDialogDismiss(ArrayList<Comment> comments, ProfilePostAdapter.RecyclerObjectHolder holder) {
        if(comments!=null){
            updateCommentCount(comments,holder);
        }
    }

    // DownloadTask for downloding video from URL
    public class DownloadTask extends AsyncTask<String, Integer, String> {
        private Context context;
        private PowerManager.WakeLock mWakeLock;
        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                java.net.URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                input = connection.getInputStream();


                String filePath = sUrl[0];
                fileN = filePath.substring(filePath.lastIndexOf("/")+1);
                //fileN = "Afrocamgist_" + UUID.randomUUID().toString().substring(0, 10) + ".mp4";
                File filename = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/", fileN);
                output = new FileOutputStream(filename);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();

            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);

            mProgressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();

            if (result != null){
                Toast.makeText(context, getString(R.string.str_download_err) + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, getString(R.string.str_video_downloaded), Toast.LENGTH_SHORT).show();
                deleteWatermarkVideoInServer();
            }

            MediaScannerConnection.scanFile(ProfileActivity.this,
                    new String[]{Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/AfrocamgistVideos/" + fileN}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String newpath, Uri newuri) {
                            Logger.i("ExternalStorage", "Scanned " + newpath + ":");
                            Logger.i("ExternalStorage", "-> uri=" + newuri);
                        }
                    });

            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + fileN;
            shareVideoToSocialMedia(filePath);
        }
    }

    //Get watermarked video from url
    private void callWatermarkVideoAPI(String videoUrl){
        waterMarkVideo = "";
        hud = KProgressHUD.create(ProfileActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setLabel("Preparing...")
                .setCancellable(false)
                .show();

        JSONObject params = new JSONObject();
        try {
            params.put("video", videoUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.WATERMARK_VIDEO, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        if(object.getBoolean("result")){
                            waterMarkVideo = object.getString("video_path");
                            String watermarkVideo = UrlEndpoints.MEDIA_BASE_URL + object.getString("video_path");
                            newDownload(watermarkVideo);
                        }else {
                            Utils.showAlert(ProfileActivity.this, getString(R.string.opps_something_went_wrong));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //delete watermark video from server
    private void deleteWatermarkVideoInServer(){
        JSONObject params = new JSONObject();
        try {
            params.put("video", waterMarkVideo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.DELETE_WATERMARK_VIDEO;

        Webservices.getData(Webservices.Method.DELETE, params, headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.d("waterMark99","success");
                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(ProfileActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showShareSelectionDialog(Context context, String videoUrl, String postLink) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_social_share);
            popup.setCancelable(true);
            popup.show();

            RelativeLayout download = popup.findViewById(R.id.rlDownload);
            RelativeLayout share = popup.findViewById(R.id.rlShare);


            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "download";
                    downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "share";
                    shareVideoLink(postLink);
                    //downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

        }
    }

    private void downloadVideo(String videoUrl){
        String originalFilename = videoUrl.substring(videoUrl.lastIndexOf("/")+1);
        result = checkPermission();
        if(result){
            checkFolder();
            if (!isConnectingToInternet(ProfileActivity.this)) {
                Toast.makeText(ProfileActivity.this, getString(R.string.please_check_your_internet_connection), Toast.LENGTH_LONG).show();
                return;
            }

            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename);
            if(file.exists()){
                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename;
                shareVideoToSocialMedia(filePath);
            }else {
                callWatermarkVideoAPI(videoUrl);
            }
        }
    }

    private void shareVideoLink(String videoUrl){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
        i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
        i.putExtra(Intent.EXTRA_TEXT, videoUrl);
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    private void shareVideoToSocialMedia(String filePath){

        MediaScannerConnection.scanFile(ProfileActivity.this, new String[] { filePath },

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                        shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, "Afrocamgist");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);


                        startActivity(Intent.createChooser(shareIntent, "Afrocamgist"));

                    }
                });
    }

    //hare you can start downloding video
    public void newDownload(String url) {
        final DownloadTask downloadTask = new DownloadTask(ProfileActivity.this);
        downloadTask.execute(url);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ProfileActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public void checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ProfileActivity.this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }

    //Here you can check App Permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkFolder();
                } else {
                    //code for deny
                    checkAgain();
                }
                break;
        }
    }

    //here you can check folder where you want to store download Video
    public void checkFolder() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/";
        File dir = new File(path);
        boolean isDirectoryCreated = dir.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated = dir.mkdir();
        }
        if (isDirectoryCreated) {
            // do something
            Logger.d("Folder", "Already Created");
        }
    }

}
