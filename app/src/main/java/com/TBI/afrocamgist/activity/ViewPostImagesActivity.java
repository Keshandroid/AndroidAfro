package com.TBI.afrocamgist.activity;


import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.ViewPostImageAdapter;

import java.util.ArrayList;

public class ViewPostImagesActivity extends BaseActivity {

    private ArrayList<String> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post_images);

        if (getIntent().getSerializableExtra("images")!=null) {
            images = (ArrayList<String>) getIntent().getSerializableExtra("images");
        }

        initImagesListView();
        initClickListener();
    }

    private void initImagesListView() {

        RecyclerView imageList = findViewById(R.id.image_list);
        imageList.setLayoutManager(new LinearLayoutManager(this));
        imageList.setAdapter(new ViewPostImageAdapter(images));
    }

    private void initClickListener() {

        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }
}
