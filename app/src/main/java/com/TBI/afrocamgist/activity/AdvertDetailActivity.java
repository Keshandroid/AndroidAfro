package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.advert.Advert;
import com.TBI.afrocamgist.model.post.Post;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class AdvertDetailActivity extends BaseActivity {

    private TextView txtName,txtAgeRange,txtGender,txtCountries,txtInterests,txtBudget,txtDuration,advertStatus;
    private ImageView post_image,vid_image,statusImg;

    private Advert advertPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert_detail);
        findViews();


        if (getIntent().getSerializableExtra("advertPost")!=null){
            advertPosts = (Advert) getIntent().getSerializableExtra("advertPost");
            Log.d("advertDetail",new GsonBuilder().setPrettyPrinting().create().toJson(advertPosts));
        }

        if(advertPosts!=null){
            txtName.setText(advertPosts.getAudience().get(0).getName());
            txtAgeRange.setText(advertPosts.getAudience().get(0).getAge_range());

            if(advertPosts.getAudience().get(0).getGender().equalsIgnoreCase("m")){
                txtGender.setText("Male");
            }else if (advertPosts.getAudience().get(0).getGender().equalsIgnoreCase("f")){
                txtGender.setText("Female");
            }else {
                txtGender.setText("All");
            }

            if(advertPosts.getAudience().get(0).getCountry()!=null && !advertPosts.getAudience().get(0).getCountry().isEmpty()){
                txtCountries.setText(advertPosts.getAudience().get(0).getCountry().toString());
            }

            if(advertPosts.getAudience().get(0).getInterests()!=null && !advertPosts.getAudience().get(0).getInterests().isEmpty()){
                txtInterests.setText(advertPosts.getAudience().get(0).getInterests().toString());
            }

            txtBudget.setText(advertPosts.getBudget().get(0).getBudget() + " $");
            txtDuration.setText(advertPosts.getBudget().get(0).getDuration() + " Days");

            advertStatus.setText(advertPosts.getStatus());

            if(advertPosts.getStatus().equalsIgnoreCase("active")){
                statusImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_active));
            }else if (advertPosts.getStatus().equalsIgnoreCase("inactive")){
                statusImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive));
            }else if(advertPosts.getStatus().equalsIgnoreCase("review")){
                statusImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_in_review));
            }

            if ("image".equals(advertPosts.getPosts().get(0).getPostType())) {
                if (advertPosts.getPosts().get(0).getPostImage().size() != 0) {
                    Glide.with(post_image)
                            .load(UrlEndpoints.MEDIA_BASE_URL + advertPosts.getPosts().get(0).getPostImage().get(0))
                            .into(post_image);
                }
                vid_image.setVisibility(View.GONE);
            } else if ("video".equals(advertPosts.getPosts().get(0).getPostType())) {

                Glide.with(post_image)
                        .load(UrlEndpoints.MEDIA_BASE_URL + advertPosts.getPosts().get(0).getThumbnail())
                        .into(post_image);
                vid_image.setVisibility(View.VISIBLE);
            }



            post_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(AdvertDetailActivity.this, ViewPostActivity.class).putExtra("postId", advertPosts.getPosts().get(0).getPostId()));
                }
            });

        }

    }

    private void findViews() {
        post_image = findViewById(R.id.post_image);
        vid_image = findViewById(R.id.vid_image);
        statusImg = findViewById(R.id.statusImg);

        txtName = findViewById(R.id.txtName);
        txtAgeRange = findViewById(R.id.txtAgeRange);
        txtGender = findViewById(R.id.txtGender);
        txtCountries = findViewById(R.id.txtCountries);
        txtInterests = findViewById(R.id.txtInterests);
        txtBudget = findViewById(R.id.txtBudget);
        txtDuration =findViewById(R.id.txtDuration);
        advertStatus = findViewById(R.id.advertStatus);

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }


}
