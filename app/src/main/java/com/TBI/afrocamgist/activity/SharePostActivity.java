package com.TBI.afrocamgist.activity;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.SpinnerAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.groups.GroupDetails;
import com.TBI.afrocamgist.model.post.Post;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.exoplayer.ExoPlayable;
import im.ene.toro.exoplayer.Playable;
import im.ene.toro.exoplayer.ToroExo;
import im.ene.toro.media.VolumeInfo;

public class SharePostActivity extends BaseActivity {

    private CircleImageView profileImage, profilePicture;
    private EditText sharePostText;
    private TextView name, time, postText, imageCount, originalPostTextOnImage;
    private PlayerView player;
    private ImageView thumbnail, postImage, moreImages, backgroundImage;
    private Spinner sectionSpinner, groupSpinner;
    private ArrayList<String> sections = new ArrayList<>();
    private ArrayList<String> groups = new ArrayList<>();
    private ArrayList<Integer> groupIds = new ArrayList<>();
    private Post post;
    private Playable playable;
    private ToggleButton volume;
    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_post);

        if (getIntent().getSerializableExtra("post") != null) {
            post = (Post) getIntent().getSerializableExtra("post");
        }

        initView();
        if (post != null)
            setPostData();

        initSectionSpinner();
        initClickListener();

        if (Utils.isConnected())
            getMyMembershipGroups();
    }

    private void initView() {

        moreImages = findViewById(R.id.more_images);
        profilePicture = findViewById(R.id.profile_picture);
        profileImage = findViewById(R.id.profile_image);
        name = findViewById(R.id.name);
        time = findViewById(R.id.time);
        sharePostText = findViewById(R.id.share_post_text);
        postText = findViewById(R.id.post_text);
        postImage = findViewById(R.id.post_image);
        imageCount = findViewById(R.id.image_count);
        thumbnail = findViewById(R.id.thumbnail);
        sectionSpinner = findViewById(R.id.section_spinner);
        groupSpinner = findViewById(R.id.group_spinner);
        backgroundImage = findViewById(R.id.background_image);
        originalPostTextOnImage = findViewById(R.id.original_post_text_on_image);
        player = findViewById(R.id.player);
        volume = findViewById(R.id.volume);
    }

    private void getMyMembershipGroups() {

        hud = KProgressHUD.create(SharePostActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MY_MEMBERSHIP_GROUPS, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                hud.dismiss();
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SharePostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        GroupDetails groupDetails = new Gson().fromJson(response, GroupDetails.class);
                        ArrayList<Group> listGroups = groupDetails.getGroups();

                        if (listGroups != null) {

                            for (int i = 0; i < listGroups.size(); i++) {
                                groups.add(listGroups.get(i).getGroupTitle());
                                groupIds.add(listGroups.get(i).getGroupId());
                            }
                        }
                    }

                    initGroupSpinner();

                } catch (Exception e) {
                    Utils.showAlert(SharePostActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSectionSpinner() {

        sections = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.section)));
        sectionSpinner.setAdapter(new SpinnerAdapter(this, sections));
        sectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1)
                    groupSpinner.setVisibility(View.VISIBLE);
                else
                    groupSpinner.setVisibility(View.GONE);

                if(position == 1){
                    if(groups!=null){
                        if(!groups.isEmpty()){
                            findViewById(R.id.share).setEnabled(true);
                        }else {
                            findViewById(R.id.share).setEnabled(false);
                        }
                    }
                }else {
                    findViewById(R.id.share).setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initGroupSpinner() {

        groupSpinner.setAdapter(new SpinnerAdapter(this, groups));
    }

    private void initClickListener() {

        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isConnected())
                    sharePost();
                else
                    Utils.showAlert(SharePostActivity.this, getString(R.string.no_internet_connection_available));
            }
        });

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());

        volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    unMute();
                else
                    mute();
            }
        });
    }

    private void sharePost() {

        hud = KProgressHUD.create(SharePostActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createJsonRequest(), headers, UrlEndpoints.POSTS, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                hud.dismiss();
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SharePostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(SharePostActivity.this, getString(R.string.post_shared_successfully), Toast.LENGTH_LONG).show();
                        setResult(RESULT_OK);
                        finish();
                    }

                    initGroupSpinner();

                } catch (Exception e) {
                    Utils.showAlert(SharePostActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private JSONObject createJsonRequest() {

        JSONObject object = new JSONObject();
        try {
            object.put("share_post_id", post.getPostId());
            object.put("post_text", sharePostText.getText().toString());
            object.put("posted_for", getPostedFor());

            if (sectionSpinner.getSelectedItemPosition() == 2)
                object.put("group_id", groupIds.get(groupSpinner.getSelectedItemPosition()));
            else
                object.put("group_id", "");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return object;
    }

    private String getPostedFor() {

        switch (sections.get(sectionSpinner.getSelectedItemPosition())) {

            case "Afro Swagger":
                return "afroswagger";
            /*case "Afro Talent":
                return "afrotalent";*/
            case "Afro Group":
                return "group";
            default:
                return "";
        }
    }

    private void setPostData() {

        String fullName = post.getFirstName() + " " + post.getLastName();
        name.setText(fullName);
        time.setText(Utils.getSocialStyleTime(post.getPostDate()));
        postText.setText(post.getPostText());
        Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(profilePicture);

        Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                .into(profileImage);

        setPostDetails();
    }

    private void setPostDetails() {

        switch (post.getPostType()) {

            case "text":
                findViewById(R.id.layout_only_images).setVisibility(View.GONE);
                findViewById(R.id.video_layout).setVisibility(View.GONE);

                if (post.getBackgroundImagePost()){

                    findViewById(R.id.text_with_image_background).setVisibility(View.VISIBLE);

                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getBackgroundImage())
                            .into(backgroundImage);

                    originalPostTextOnImage.setText(post.getPostText());
                    postText.setText("");
                } else {
                    findViewById(R.id.text_with_image_background).setVisibility(View.GONE);
                }

                break;
            case "image":
                findViewById(R.id.layout_only_images).setVisibility(View.VISIBLE);
                findViewById(R.id.video_layout).setVisibility(View.GONE);

                RequestOptions requestOptions = new RequestOptions();
                requestOptions = requestOptions.transforms(new RoundedCorners(50));

                if (post.getPostImage().size() > 1) {
                    String count = (post.getPostImage().size() - 1) + "";
                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .apply(requestOptions)
                            .into(postImage);

                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(1))
                            .into(moreImages);

                    imageCount.setText(count);
                } else {
                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .apply(requestOptions)
                            .into(postImage);
                    findViewById(R.id.more_images_layout).setVisibility(View.GONE);
                }

                Zoomy.Builder builder = new Zoomy.Builder(this)
                        .target(postImage)
                        .enableImmersiveMode(false);

                builder.register();

                break;
            case "video":
                findViewById(R.id.layout_only_images).setVisibility(View.GONE);
                findViewById(R.id.video_layout).setVisibility(View.VISIBLE);
                playVideo(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo()));
                Glide.with(this)
                        .load(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail())
                        .into(thumbnail);
                break;
            default:
                break;
        }
    }

    private void playVideo(Uri mediaUri) {

        playable = new ExoPlayable(ToroExo.with(this).getDefaultCreator(), mediaUri, null);
        playable.prepare(true);
        playable.setPlayerView(player);
        mute();
        if (!playable.isPlaying()) {
            playable.play();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    findViewById(R.id.thumbnail_layout).setVisibility(View.GONE);
                }
            }, 1500);
        }
    }

    private void mute() {
        this.playable.setVolumeInfo(new VolumeInfo(true,0));
    }

    private void unMute() {
        this.playable.setVolumeInfo(new VolumeInfo(false,1));
    }

    /*private void setVolume(int amount) {
        final int max = 100;
        final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
        final float volume = (float) (1 - (numerator / Math.log(max)));

        this.playable.setVolumeInfo(new VolumeInfo(true,0));
    }*/

    @Override
    public void onStop() {
        super.onStop();
        if (playable != null) {
            playable.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (playable != null) {
            playable.setPlayerView(null);
            playable.release();
            playable = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (playable != null)
            playable.setPlayerView(null);
    }
}
