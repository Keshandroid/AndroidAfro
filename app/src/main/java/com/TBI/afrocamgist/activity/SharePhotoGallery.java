package com.TBI.afrocamgist.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.loader.content.CursorLoader;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.TagUser.TagToBeTagged;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.media.Media;
import com.TBI.afrocamgist.model.media.MediaDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.zhihu.matisse.Matisse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnMultiCompressListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SharePhotoGallery extends BaseActivity{

    private KProgressHUD hud;
    private ArrayList<File> imageFiles = new ArrayList<>();
    private ArrayList<File> compressedImageFiles = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getSerializableExtra("imagePaths") != null){
            createImageFile((ArrayList<String>) getIntent().getSerializableExtra("imagePaths"));
        }
        /*Intent intent = getIntent();
        String type = intent.getType();
        String action = intent.getAction();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendImage(intent);
            }
        }*/
        /*if (Intent.ACTION_SEND_MULTIPLE.equals(action) && getIntent().hasExtra(Intent.EXTRA_STREAM)) {
            ArrayList<Parcelable> list = getIntent().getParcelableArrayListExtra(Intent.EXTRA_STREAM);
            Intent i = new Intent(this, RequestListingActivity.class);
            i.putExtra(PARAM_MULTIPLE_IMAGE, list);
            startActivity(i);
            finish();
        }*/
    }

    /*private void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {

            //List<String> mPaths = Matisse.obtainPathResult(intent);
            Logger.d("Images99","999" + getRealPathFromURI(this,imageUri));


            ArrayList<String> imagePaths = new ArrayList<>();
            imagePaths.add(getRealPathFromURI(this,imageUri));

            createImageFile(imagePaths);

            *//*Intent i = new Intent(this, CreatePostActivity.class);
            i.putExtra("postType", "IMAGE");
            i.putExtra("postText", "");
            i.putExtra("images", imagePaths);
            i.putExtra("location", "");
            i.putExtra("tagUserList",new ArrayList<TagToBeTagged>());

            i.putExtra("postCategory", "normal_post");
            startActivity(i);
            finish();*//*



            *//*Intent i = new Intent(this, CreatePostActivity.class);
            i.putExtra(PARAM_IMAGE, imageUri.toString());
            startActivity(i);
            finish();*//*
        } else {
            Toast.makeText(this, "Error occured, URI is invalid", Toast.LENGTH_LONG).show();
        }
    }*/

    private void createImageFile(ArrayList<String> imagePaths) {

        imageFiles = new ArrayList<>();

        for (int i = 0; i < imagePaths.size(); i++) {

            try {
                imageFiles.add(new File(imagePaths.get(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Luban.compress(this, imageFiles)
                .putGear(Luban.CUSTOM_GEAR)
                .launch(new OnMultiCompressListener() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onSuccess(List<File> fileList) {
                        compressedImageFiles = new ArrayList<>(fileList);
                        for (File image : imageFiles)
                            image.delete();
                        uploadImages();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.d("Percentage", e.getMessage() + "");
                    }
                });
    }

    private void uploadImages() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (File photo : compressedImageFiles) {
            builder.addFormDataPart(Constants.IMAGES, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload";

        Webservices.getData(this, Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(this, getString(R.string.opps_something_went_wrong));
            } else {
                hud.dismiss();
                try {
                    JSONObject object = new JSONObject(response);

                    Logger.d("uploadResponse", object.toString());

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(this, object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {

                            for (File image : compressedImageFiles)
                                image.delete();


                            createImagePost(mediaDetails.getMediaList());
                        } else {
                            Utils.showAlert(this, getString(R.string.opps_something_went_wrong));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Logger.d("exception1",""+ Log.getStackTraceString(e));
                    Logger.d("exception1",""+e.getLocalizedMessage());
                    //Utils.showAlert(getActivity(), response);
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int) (100 * bytesRead / contentLength);
            Logger.d("Percentage", percentage + "");
            //if (swaggerPostAdapter != null) { // important
//            setUploadProgress(percentage);
//            if (getActivity() != null)
//                getActivity().runOnUiThread(this::showPostingView); //important
            //}
        });
    }

    private void createImagePost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createImagePostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(this, getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    finish();
                    /*if (swaggerPosts != null) {
                        //swaggerPosts.remove(1); // important
                        cardProgressbar.setVisibility(View.GONE);
                        videosAdapter.notifyDataSetChanged();
                    }
                    getSwaggerFirstPagePosts(true);
                    getAdvertisements();*/
                }

            } catch (Exception e) {
                //Utils.showAlert(getActivity(), "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    private JSONObject createImagePostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            request.put("post_text", "");
            request.put("post_lat_long", "");
            request.put("posted_for", "afroswagger");

            JSONArray images = new JSONArray();
            for (Media media : mediaList) {
                images.put(media.getPath());
            }

            /*if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (TagToBeTagged tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged.getUserId());
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }*/

            request.put("post_type", "image");
            request.put("post_image", images);

            Logger.d("imagelistRequest", new GsonBuilder().setPrettyPrinting().create().toJson(request));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

}
