package com.TBI.afrocamgist.activity;


import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.PostLikedByAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.finduser.UserDetails;
import com.TBI.afrocamgist.model.finduser.UserList;
import com.TBI.afrocamgist.model.post.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PostLikedByActivity extends BaseActivity {

    private int postId;
    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_liked_by);

        postId = getIntent().getIntExtra("postId",0);
        setClickListener();
        getPostLikedBy();
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private void getPostLikedBy() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        String url = UrlEndpoints.POSTS + "/" + postId + "/likes";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(PostLikedByActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(PostLikedByActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        UserList userList = new Gson().fromJson(response, UserList.class);

                        if (userList.getUsers()==null || userList.getUsers().size()==0) {
                            findViewById(R.id.no_likes).setVisibility(View.VISIBLE);
                            findViewById(R.id.liked_by_list).setVisibility(View.GONE);
                        } else {
                            findViewById(R.id.no_likes).setVisibility(View.GONE);
                            findViewById(R.id.liked_by_list).setVisibility(View.VISIBLE);
                            setPostLikedByList(userList.getUsers());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(PostLikedByActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setPostLikedByList(ArrayList<UserDetails> users) {

        RecyclerView likedByList = findViewById(R.id.liked_by_list);
        likedByList.setLayoutManager(new LinearLayoutManager(this));
        likedByList.setAdapter(new PostLikedByAdapter(this,users));
    }
}
