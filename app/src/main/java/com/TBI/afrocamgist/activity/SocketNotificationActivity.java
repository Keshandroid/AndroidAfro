package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.NotificationAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.notification.Notification;
import com.TBI.afrocamgist.model.notification.NotificationData;
import com.TBI.afrocamgist.model.notification.NotificationDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.Ack;

public class SocketNotificationActivity extends BaseActivity implements ConnectivityListener {

    private KProgressHUD hud;
    private SwipeRefreshLayout swipeContainer;
    private Connectivity mConnectivity;
    private boolean isAnyNotificationRead = false;
    private boolean loadingMore = false;

    private ArrayList<NotificationData> notificationsList = new ArrayList<>();
    private NotificationAdapter notificationAdapter;

    private int nextPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        setClickListener();
        initSwipeRefresh();
        initInternetConnectivityCheckListener();
    }

    private void initSwipeRefresh() {

        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                //getNotifications();
                getSocketNotifications();
            else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(this, getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    @Override
    public void isSocketConnected(boolean connected) {
        super.isSocketConnected(connected);
        if (connected) {

        }

    }


    /*@Override
    public void parseResponseInMain(String event, JSONObject argument) {
        super.parseResponseInMain(event, argument);
        switch (event) {
            case UrlEndpoints.GetNotifications: {

                Log.d("SocketNotification9","response : "+argument.toString());

                *//*boolean isChatOpen = false;
                ChatFragment chatFragment = (ChatFragment) getSupportFragmentManager().findFragmentByTag(ChatFragment.class.getSimpleName());
                if (chatFragment != null) {
                    isChatOpen = chatFragment.newMessageResponse(argument);
                }
                if (!isChatOpen) {
                    HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getSimpleName());
                    if (homeFragment != null) {
                        homeFragment.newMessageResponse(argument);
                    }
                }*//*
            }
            break;


        }
    }*/

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud!=null && hud.isShowing()){
                hud.dismiss();
            }
            Utils.closeAlert();
            //getNotifications();
            getSocketNotifications();
        } else {
            //Utils.showAlert(NotificationActivity.this, "No internet connection available.");
        }
    }

    private void setClickListener() {

        findViewById(R.id.mark_all_read).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAllNotificationRead();
            }
        });

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private void getSocketNotifications(){

        JSONObject data = new JSONObject();
        try {
            data.put("user_id", LocalStorage.getUserDetails().getUserId());
            data.put("page",1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.GetNotifications, data, new Ack() {
                @Override
                public void call(Object... args) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Log.i("SocketLog", "Notifications List = "+ Arrays.toString(args));

                            JSONObject notificationList = (JSONObject) args[1];

                            NotificationDetails notificationDetails = new Gson().fromJson(String.valueOf(notificationList), NotificationDetails.class);

                            if (notificationDetails.getNotificationList() != null) {

                                if(notificationDetails.getNotificationList().size()>0){
                                    notificationsList.addAll(notificationDetails.getNotificationList());
                                    nextPage = notificationDetails.getNextPage();

                                    setNotificationList();
                                }

                            }
                        }
                    });


                }
            });
        }


    }

   /* private void getNotifications() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.NOTIFICATIONS + "?page=1";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            swipeContainer.setRefreshing(false);
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(SocketNotificationActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SocketNotificationActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        NotificationDetails notificationDetails = new Gson().fromJson(response, NotificationDetails.class);

                        if (notificationDetails.getNotificationList() != null) {

                            if(notificationDetails.getNotificationList().size()>0){
                                notificationsList.addAll(notificationDetails.getNotificationList());
                                nextPage = notificationDetails.getNextPage();
                                setNotificationList();
                            }

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SocketNotificationActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }*/

    private void getSocketNotificationsNextPage(){
        loadingMore = true;

        Log.d("nextpage90",""+nextPage);

        JSONObject data = new JSONObject();
        try {
            data.put("user_id", LocalStorage.getUserDetails().getUserId());
            data.put("page",nextPage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        UrlEndpoints.socketIOClient.emit(UrlEndpoints.GetNotifications, data, new Ack() {
            @Override
            public void call(Object... args) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Log.i("SocketLog", "Notifications next page = "+ Arrays.toString(args));

                        JSONObject notificationList = (JSONObject) args[1];

                        NotificationDetails notificationDetails = new Gson().fromJson(String.valueOf(notificationList), NotificationDetails.class);

                        if (notificationDetails.getNotificationList() != null) {
                            if(notificationDetails.getNotificationList().size()>0){
                                loadingMore = false;
                                nextPage = notificationDetails.getNextPage();
                                notificationsList.addAll(notificationDetails.getNotificationList());
                                notificationAdapter.notifyDataSetChanged();
                            }

                        }
                    }
                });


            }
        });
    }

    /*private void getNotificationsNextPage() {

        loadingMore = true;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.NOTIFICATIONS + "?page=" + nextPage;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            swipeContainer.setRefreshing(false);
            if ("".equals(response)) {
                Utils.showAlert(SocketNotificationActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SocketNotificationActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        NotificationDetails notificationDetails = new Gson().fromJson(response, NotificationDetails.class);

                        if (notificationDetails.getNotificationList() != null) {
                            if(notificationDetails.getNotificationList().size()>0){
                                loadingMore = false;
                                nextPage = notificationDetails.getNextPage();
                                notificationsList.addAll(notificationDetails.getNotificationList());
                                notificationAdapter.notifyDataSetChanged();
                            }

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SocketNotificationActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }*/

    private void setNotificationList() {

        ListView notifications = findViewById(R.id.notification_list);
        notificationAdapter = new NotificationAdapter(this,notificationsList);
        notifications.setAdapter(notificationAdapter);
        notifications.setOnItemClickListener((parent, view, position, id) -> {

            Notification notification = notificationsList.get(position).getNotificationDetails();
            if (!"read".equals(notificationsList.get(position).getNotificationStatus().toLowerCase())) {
                isAnyNotificationRead = true;
                markNotificationAsRead(notificationsList.get(position).getNotificationId());
            }

            redirectFromNotification(notification,notificationsList.get(position));

        });

        notifications.setOnScrollListener(new AbsListView.OnScrollListener(){

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if((lastInScreen == totalItemCount) && !(loadingMore)){
                    //getNotificationsNextPage();
                    getSocketNotificationsNextPage();
                }
            }
        });

    }

    private void redirectFromNotification(Notification notification,NotificationData notificationData) {

        Logger.d("NotificationRawData", new GsonBuilder().setPrettyPrinting().create().toJson(notification));

        if (notification.getNotificationRawDetails()!=null) {
            if (notification.getNotificationRawDetails().getGroupId() != null) {
                Group group = new Group();
                group.setGroupId(notification.getNotificationRawDetails().getGroupId());
                if(notificationData!=null){
                    Intent intent =new Intent(SocketNotificationActivity.this, AfroViewGroupPostActivity.class);
                    intent.putExtra("group", group);
                    if(notificationData.getNotificationType().equals(Constants.GROUP_INVITE)){
                        intent.putExtra("notification_type", notificationData.getNotificationType());
                    }
                    startActivity(intent);
                }

            } else if (notification.getNotificationRawDetails().getPostId() != null) {
                startActivity(new Intent(SocketNotificationActivity.this, ViewPostActivity.class)
                        .putExtra("postId", notification.getNotificationRawDetails().getPostId()));
            } else if (notification.getNotificationRawDetails().getUserId() != null) {

                if (LocalStorage.getUserDetails().getUserId().equals(notification.getNotificationRawDetails().getUserId()))
                    startActivity(new Intent(SocketNotificationActivity.this, MyProfileActivity.class));
                else
                    startActivity(new Intent(SocketNotificationActivity.this, ProfileActivity.class)
                            .putExtra("userId", notification.getNotificationRawDetails().getUserId()));
            }
        }
    }

    private void markNotificationAsRead(Integer notificationId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.NOTIFICATIONS + "/read/" + notificationId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {});
    }

    private void markAllNotificationRead() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MARK_ALL_NOTIFICATIONS_READ, response -> {
            if ("".equals(response)) {
                Utils.showAlert(SocketNotificationActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SocketNotificationActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        //getNotifications();
                        getSocketNotifications();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SocketNotificationActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // refresh when return back to notification screen from detail page
        /*if (Utils.isConnected() && isAnyNotificationRead)
            getNotifications();*/

    }

}
