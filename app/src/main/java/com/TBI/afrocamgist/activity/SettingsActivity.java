package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.AfroInterestAdapter;
import com.TBI.afrocamgist.adapters.SpinnerAdapter;
import com.TBI.afrocamgist.api.Params;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.user.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.facebook.login.LoginManager;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.TBI.afrocamgist.activity.SplashActivity.isNotification;

public class SettingsActivity extends BaseActivity implements View.OnClickListener, ConnectivityListener {

    private Spinner religionSpinner, careerSpinner, countrySpinner;
    private Connectivity mConnectivity;
    private KProgressHUD hud;
    private EditText firstName, lastName, email, phone, state, dateOfBirth, password, uniqueUsername, edtProfileTitle;
    private RadioButton male, female, pub, pri;
    private CircleImageView profileImage;
    private ImageView profileCover, imgDeactivate;
    private RecyclerView interestList;
    private static final String IMAGE_DIRECTORY = "/Afrocamgist";
    private File photo;
    private boolean isProfilePhotoChanged = false;
    private ArrayList<String> religionList = new ArrayList<>();
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayList<String> careerList = new ArrayList<>();
    private ArrayList<String> afroInterests = new ArrayList<>();
    public static final int CAMERA_INTENT_PROFILE = 100;
    public static final int CAMERA_INTENT_COVER = 101;
    public static ArrayList<String> selectedInterests = new ArrayList<>();
    public static boolean isImageSet = false;
    public static boolean isCoverImageSet = false;

    private User user;

    protected LocalBroadcastManager localBroadcastManager;
    public static String NOTIFY_DRAWER_PROFILE_DETAIL = "notify_drawer_profile_detail";

    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        if (getIntent().getStringExtra("fromSignUp") != null) {
            findViewById(R.id.back).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.title)).setText(R.string.tellUsSomethingAboutYourself);
        }

        initView();

        if (Utils.isConnected()) {
            getSettings();
        }

        initClickListener();
        initSpinner();
        initInternetConnectivityCheckListener();
    }

    private void initView() {
        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.mobile);
        state = findViewById(R.id.state);
        dateOfBirth = findViewById(R.id.dateOfBirth);
        password = findViewById(R.id.password);
        male = findViewById(R.id.male);
        female = findViewById(R.id.female);
        pub = findViewById(R.id.pub);
        pri = findViewById(R.id.pri);
        religionSpinner = findViewById(R.id.religionspinner);
        careerSpinner = findViewById(R.id.careerspinner);
        countrySpinner = findViewById(R.id.countryspinner);
        profileImage = findViewById(R.id.profileImage);
        profileCover = findViewById(R.id.profile_cover);
        interestList = findViewById(R.id.interest_list);
        uniqueUsername = findViewById(R.id.uniqueUsername);
        edtProfileTitle = findViewById(R.id.edtProfileTitle);
        imgDeactivate = findViewById(R.id.imgDeactivate);

    }

    private void initClickListener() {

        findViewById(R.id.upload).setOnClickListener(this);
        findViewById(R.id.upload_cover).setOnClickListener(this);
        findViewById(R.id.update).setOnClickListener(this);
        findViewById(R.id.dateOfBirth).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.imgDeactivate).setOnClickListener(this);
    }

    private void initSpinner() {

        religionList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.religion)));
        countryList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.countries)));
        careerList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.career)));

        setSpinnerAdapter(religionList, countryList, careerList);
    }

    private void initInterestList(ArrayList<String> selectedInterests) {

        afroInterests = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.afroInterest)));

        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        interestList.setLayoutManager(layoutManager);
        interestList.setAdapter(new AfroInterestAdapter(afroInterests, selectedInterests));
    }

    private void setSpinnerAdapter(ArrayList<String> religionList, ArrayList<String> countryList, ArrayList<String> careerList) {

        religionSpinner.setAdapter(new SpinnerAdapter(this, religionList));
        careerSpinner.setAdapter(new SpinnerAdapter(this, careerList));
        countrySpinner.setAdapter(new SpinnerAdapter(this, countryList));
    }

    private void initInternetConnectivityCheckListener() {
        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud != null && hud.isShowing()) {
                hud.dismiss();
            }
            Utils.closeAlert();
            //getSettings();
        } else {
            Utils.showAlert(SettingsActivity.this, getString(R.string.no_internet_connection_available));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == CAMERA_INTENT_PROFILE) {
                isProfilePhotoChanged = true;
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setCropShape(CropImageView.CropShape.OVAL).start(SettingsActivity.this);
            } else {
                isProfilePhotoChanged = false;
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start(SettingsActivity.this);
            }
        } else {
            Toast.makeText(SettingsActivity.this, getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (isProfilePhotoChanged) {
                    Glide.with(this)
                            .load(resultUri)
                            .apply(RequestOptions.circleCropTransform())
                            .into(profileImage);

                } else {
                    Glide.with(this)
                            .load(resultUri)
                            .into(profileCover);
                }
                createImageFile(resultUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(SettingsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void createImageFile(Uri uri) {

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            //File directory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY); // old code
            File directory = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + IMAGE_DIRECTORY); // new code

            if (!directory.exists()) {
                directory.mkdirs();
            }

            try {
                photo = new File(directory, Calendar.getInstance().getTimeInMillis() + ".jpg");
                FileOutputStream fo = new FileOutputStream(photo);
                fo.write(bytes.toByteArray());
                fo.close();

                saveProfilePhoto();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveProfilePhoto() {

        hud = KProgressHUD.create(SettingsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = isProfilePhotoChanged ? UrlEndpoints.UPDATE_PROFILE_PICTURE : UrlEndpoints.UPDATE_PROFILE_COVER;

        Webservices.getData(this, Webservices.Method.POST, builder, headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(SettingsActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SettingsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        if (isProfilePhotoChanged)
                            isImageSet = true;
                        else
                            isCoverImageSet = true;
                        Toast.makeText(SettingsActivity.this, getString(R.string.picture_updated_successfully), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(SettingsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private boolean hasPermissions(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions)
                if (ActivityCompat.checkSelfPermission(SettingsActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
        }
        return true;
    }

    private void updateSettings() {
        ArrayList<Params> params = new ArrayList<>();
        if (firstName.getText().toString().trim().equals("")) {
            Utils.showAlert(SettingsActivity.this, getString(R.string.first_name_is_required));
            firstName.setFocusable(true);
        } else if (lastName.getText().toString().trim().equals("")) {
            Utils.showAlert(SettingsActivity.this, getString(R.string.last_name_is_required));
            lastName.setFocusable(true);
        } /*else if (state.getText().toString().trim().equals("")){
            Utils.showAlert(SettingsActivity.this, "State is required.");
            state.setFocusable(true);
        }*/ else if (dateOfBirth.getText().toString().trim().equals("")) {
            dateOfBirth.setError(null);
            Utils.showAlert(SettingsActivity.this, getString(R.string.date_of_birth_required));
            dateOfBirth.setFocusable(true);
        } else if (selectedInterests.size() <= 0) {
            Utils.showAlert(SettingsActivity.this, getString(R.string.please_select_interest));
        } /*else if (password.getText().toString().trim().equals("")){
            Utils.showAlert(SettingsActivity.this, "Password is required.");
            password.setFocusable(true);
        } */ else {

            hud = KProgressHUD.create(SettingsActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();

            params.add(new Params(Constants.FIRST_NAME, firstName.getText().toString().trim()));
            params.add(new Params(Constants.LAST_NAME, lastName.getText().toString().trim()));
            params.add(new Params(Constants.USER_NAME_UNIQUE, uniqueUsername.getText().toString().trim()));
            if (edtProfileTitle.getText().toString() != null && edtProfileTitle.getText().toString().length() > 0)
                params.add(new Params(Constants.PROFILE_TITLE, edtProfileTitle.getText().toString().trim()));

            params.add(new Params(Constants.GENDER, male.isChecked() ? "M" : "F"));
            params.add(new Params(Constants.CONTACT_NUMBER, phone.getText().toString().trim()));
            params.add(new Params(Constants.RELIGION, religionList.get(religionSpinner.getSelectedItemPosition())));

            for (String interest : selectedInterests)
                params.add(new Params(Constants.SPORTS_INTERESTS, interest));

            // params.add(new Params(Constants.CAREER, careerList.get(careerSpinner.getSelectedItemPosition())));
            params.add(new Params(Constants.NATIONALITY, countryList.get(countrySpinner.getSelectedItemPosition())));
            //params.add(new Params(Constants.STATE, state.getText().toString()));
            params.add(new Params(Constants.DATE_OF_BIRTH, Utils.convertDateFormat1(dateOfBirth.getText().toString())));
            params.add(new Params(Constants.PRIVATE, pri.isChecked() + ""));
            params.add(new Params(Constants.INTRODUCED, "true"));

            Logger.d("SelectedInterests", new JSONArray(selectedInterests).toString());

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");
            headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            Logger.d("paramSetting", "" + params.toString());

            Webservices.getData(Webservices.Method.PUT, params, headers, UrlEndpoints.SETTINGS, new OnTaskCompleted() {
                @Override
                public void onResponse(String response) {
                    hud.dismiss();
                    if ("".equals(response)) {
                        Utils.showAlert(SettingsActivity.this, getString(R.string.opps_something_went_wrong));
                    } else {
                        try {
                            JSONObject object = new JSONObject(response);
                            Logger.e("SETTING_9", ": 222");
                            if (object.has(Constants.MESSAGE)) {
                                Utils.showAlert(SettingsActivity.this, object.getString(Constants.MESSAGE));
                            } else {
                                Toast.makeText(SettingsActivity.this, getString(R.string.user_updated_successfully), Toast.LENGTH_LONG).show();
                                if (getIntent().getStringExtra("fromSignUp") != null) {
                                    getUserProfile();
                                    Logger.e("LLLLL_Res: ", response);
                                    LocalStorage.setIsUserLoggedIn(true);
                                    LocalStorage.setProfileImage(true);
                                    startActivity(new Intent(SettingsActivity.this, DashboardActivity.class));
                                    finish();
                                } else {
                                    getUserProfile();
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Logger.e("SETTING_9", ": 111");
                            Utils.showAlert(SettingsActivity.this, getString(R.string.opps_something_went_wrong));
                        }
                    }
                }
            });
        }
    }

    private void notifyNavigationDrawerProfile() {
        Intent intent = new Intent(NOTIFY_DRAWER_PROFILE_DETAIL);
        localBroadcastManager.sendBroadcast(intent);
    }

    private void getSettings() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.SETTINGS, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                Logger.e("LLLLLL_res: ", response);
                //hud.dismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SettingsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        user = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), User.class);
                        setData(user);
                    }

                } catch (Exception e) {
                    Utils.showAlert(SettingsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void setData(User user) {

        firstName.setText(user.getFirstName());
        edtProfileTitle.setText(user.getProfile_title());
        lastName.setText(user.getLastName());
        email.setText(user.getEmail());
        phone.setText(user.getContactNumber());

        if (user.getUser_name() != null) {
            uniqueUsername.setText(user.getUser_name());
        }


        /*if (!LocalStorage.getUserPass().equals("")){
            password.setText(LocalStorage.getUserPass());
        }*/

        if ("contact_number".equals(user.getRegisteredWith())) {
            phone.setFocusable(false);
            phone.setEnabled(false);
            email.setFocusable(true);
            email.setEnabled(true);
        } else {
            phone.setFocusable(true);
            phone.setEnabled(true);
            email.setFocusable(false);
            email.setEnabled(false);
        }

        if (user.getState() != null)
            state.setText(user.getState());
        if (user.getDateOfBirth() != null && !user.getDateOfBirth().isEmpty())
            dateOfBirth.setText(Utils.convertDateFormat(user.getDateOfBirth()));

        if (user.getProfileImageUrl() != null && !user.getProfileImageUrl().equals("")) {
            if (!user.getProfileImageUrl().equals("profile_default.jpg")) {
                isImageSet = true;
                isProfilePhotoChanged = true;
            }
        }

        if (user.getProfileCoverUrl() != null && !user.getProfileCoverUrl().equals("")) {
            isCoverImageSet = true;
            isProfilePhotoChanged = true;
        }

        /*Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + user.getProfileImageUrl())
                .into(profileImage);*/

        Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + user.getProfileCoverUrl())
                .into(profileCover);

        Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + user.getProfileImageUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        hud.dismiss();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        hud.dismiss();
                        return false;
                    }
                }).into(profileImage);


        if (user.getGender() != null && !user.getGender().isEmpty()) {
            if ("M".equals(user.getGender())) {
                male.setChecked(true);
            } else {
                female.setChecked(true);
            }
        }

        if (user.getPrivate() != null) {
            if (user.getPrivate()) {
                pri.setChecked(true);
            } else {
                pub.setChecked(true);
            }
        }

        if (user.getReligion() != null) {
            int selectedReligionPosition = religionList.indexOf(user.getReligion());
            if (selectedReligionPosition > 0 && selectedReligionPosition < religionList.size())
                religionSpinner.setSelection(selectedReligionPosition);
        }

        if (user.getNationality() != null) {
            int selectedCountryPosition = countryList.indexOf(user.getNationality());
            if (selectedCountryPosition > 0 && selectedCountryPosition < countryList.size())
                countrySpinner.setSelection(selectedCountryPosition);
        }

        if (user.getCareerInterest() != null) {
            int selectedCareerPosition = careerList.indexOf(user.getCareerInterest());
            if (selectedCareerPosition > 0 && selectedCareerPosition < careerList.size())
                careerSpinner.setSelection(selectedCareerPosition);
        }

        if (user.getSportsInterests() != null) {
            initInterestList(user.getSportsInterests());
        } else {
            initInterestList(new ArrayList<>());
        }
    }

    private void showDatePickerDialog() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.CustomDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String date = (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "-" + (month < 10 ? "0" + (month + 1) : (month + 1)) + "-" + year;
                dateOfBirth.setText(date);
            }
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -13);
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePickerDialog.setCancelable(true);
        datePickerDialog.show();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.update:

                if (uniqueUsername.getText().toString().trim().equalsIgnoreCase("")) {
                    if (Utils.isConnected()) {
                        if (isImageSet) {
                            updateSettings();
                        } else {
                            Utils.showAlert(SettingsActivity.this, getString(R.string.please_upload_profile_image));
                        }
                    } else {
                        //Utils.showAlert(SettingsActivity.this, getString(R.string.no_internet_connection_available));
                    }
                } else {
                    if (user != null) {
                        if (user.getUser_name() != null) {
                            if (uniqueUsername.getText().toString().equalsIgnoreCase(user.getUser_name())) {
                                if (Utils.isConnected()) {
                                    if (isImageSet) {
                                        updateSettings();
                                    } else {
                                        Utils.showAlert(SettingsActivity.this, getString(R.string.please_upload_profile_image));
                                    }
                                } else {
                                    //Utils.showAlert(SettingsActivity.this, getString(R.string.no_internet_connection_available));
                                }
                            } else {
                                isValidUniqueUsername();
                            }
                        } else {
                            isValidUniqueUsername();
                        }
                    }
                }


                //isValidUniqueUsername();

                /*if (Utils.isConnected()) {
                    if (isImageSet) {
                        updateSettings();
                    } else {
                        Utils.showAlert(SettingsActivity.this, "Please Upload Profile Image..");
                    }
                } else {
                    Utils.showAlert(SettingsActivity.this, "No internet connection available.");
                }*/
                break;
            case R.id.upload:
                String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

                if (!hasPermissions(PERMISSIONS)) {
                    ActivityCompat.requestPermissions(SettingsActivity.this, PERMISSIONS, CAMERA_INTENT_PROFILE);
                } else {
                    isProfilePhotoChanged = true;
                    CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setCropShape(CropImageView.CropShape.OVAL).start(SettingsActivity.this);
                }
                break;
            case R.id.upload_cover:
                String[] COVER_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

                if (!hasPermissions(COVER_PERMISSIONS)) {
                    ActivityCompat.requestPermissions(SettingsActivity.this, COVER_PERMISSIONS, CAMERA_INTENT_COVER);
                } else {
                    isProfilePhotoChanged = false;
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(SettingsActivity.this);
                }
                break;
            case R.id.dateOfBirth:
                showDatePickerDialog();
                break;
            case R.id.back:
                onBackPressed();
                break;

            case R.id.imgDeactivate:
                showAccountDeactivateConfirmationDialog();
                break;

            default:
                break;
        }
    }

    private void showAccountDeactivateConfirmationDialog() {

        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_deactivate);
        popup.setCancelable(true);
        popup.show();

        TextView message = popup.findViewById(R.id.message);
        message.setTextColor(getResources().getColor(R.color.blue));
        String msg = getString(R.string.are_you_sure_you_want_to_deactivate_this_account);
        message.setText(msg);

        TextView confirm = popup.findViewById(R.id.okay);
        confirm.setTextColor(getResources().getColor(R.color.blue));
        confirm.setText("Ok");

        TextView discard = popup.findViewById(R.id.cancel);
        discard.setText("Cancel");

        popup.findViewById(R.id.okay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();
                googleSignOut();
                deactivateAccount();
                popup.dismiss();
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void googleSignOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //updateUI(null);
                    }
                });
    }

    public void deactivateAccount() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());
        Webservices.getData(Webservices.Method.PUT, new JSONObject(), headers, UrlEndpoints.DEACTIVATE_ACCOUNT, response -> {
            try {
                JSONObject object = new JSONObject(response);
                if (object.has("status")) {
                    if (object.getBoolean("status")) {
                        logOUT();
                    } else if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SettingsActivity.this, object.getString(Constants.MESSAGE));
                    }
                } else if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(SettingsActivity.this, object.getString(Constants.MESSAGE));
                }

            } catch (Exception e) {
                //Utils.showAlert(CommentsActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private void logOUT() {

        hud = KProgressHUD.create(SettingsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        //remove sharedpreferences
        removeSharedPreferences();


        JSONObject params = new JSONObject();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, params, headers, UrlEndpoints.LOGOUT, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(SettingsActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    Logger.e("LLLL_Res: ", response);
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("status")) {
                        LocalStorage.setIsUserLoggedIn(false);
                        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Utils.showAlert(SettingsActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SettingsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void removeSharedPreferences() {
        SharedPreferences settings = this.getSharedPreferences("ChatData", Context.MODE_PRIVATE);
        settings.edit().clear().apply();

        SharedPreferences mySPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = mySPrefs.edit();
        editor.remove("AUTO_PLAY_VIDEO_PREFERENCE");
        editor.apply();
    }

    private void isValidUniqueUsername() {

        ArrayList<Params> params = new ArrayList<>();
        params.add(new Params(Constants.USER_NAME_UNIQUE, uniqueUsername.getText().toString().trim()));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.CHECK_USERNAME, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);

                    Logger.d("usernameResponse", new GsonBuilder().setPrettyPrinting().create().toJson(object));

                    if (object.has(Constants.MESSAGE)) {
                        uniqueUsername.setError(object.getString(Constants.MESSAGE));
                    } else {

                        Drawable img = uniqueUsername.getContext().getResources().getDrawable(R.drawable.ic_success);
                        uniqueUsername.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);

                        if (Utils.isConnected()) {
                            if (isImageSet) {
                                updateSettings();
                            } else {
                                Utils.showAlert(SettingsActivity.this, getString(R.string.please_upload_profile_image));
                            }
                        } else {
                            //Utils.showAlert(SettingsActivity.this, getString(R.string.no_internet_connection_available));
                        }
                    }

                } catch (Exception e) {
                    Logger.e("LLLLL_Signup: ", e.getMessage());
                    Utils.showAlert(SettingsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isNotification) {
            isNotification = false;
            startActivity(new Intent(SettingsActivity.this, DashboardActivity.class));
            finish();
        } else {
            if (getIntent().getStringExtra("fromSignUp") != null)
                finishAffinity();
            else
                super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
    }

    private void getUserProfile() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.PROFILE, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (!object.has(Constants.MESSAGE)) {
                        User user = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), User.class);
                        LocalStorage.saveUserDetails(user);
                        notifyNavigationDrawerProfile();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}