package com.TBI.afrocamgist.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;


import com.TBI.afrocamgist.AESHelper;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.LocaleManager;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Params;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.notification.NotificationDetails;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.user.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hbb20.CountryCodePicker;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.DigestException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import io.socket.client.Ack;
import io.socket.emitter.Emitter;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText username, password;
    private KProgressHUD hud;
    private CountryCodePicker countryCodePicker;
    Dialog dialog;
    private CardView cardLanguage;
    private TextView languageText;

    //Google Login
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN =7;
    private static final String TAG = GoogleLoginHomeActivity.class.getSimpleName();

    //fb login
    LoginButton loginButton;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setBackgroundDrawableResource(R.drawable.login_bg);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initView();
        initClickListener();

        Logger.d("FIREBASE_TOKEN",""+LocalStorage.getToken());

        //Google Login
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().requestProfile().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //fb login
        boolean loggedOut = AccessToken.getCurrentAccessToken() == null;
        if (!loggedOut) {
            //Picasso.get().load(Profile.getCurrentProfile().getProfilePictureUri(200, 200)).into(imageView);
            Logger.d("TAG", "Username is: " + Profile.getCurrentProfile().getName());
            //Using Graph API
            getUserProfile(AccessToken.getCurrentAccessToken());
        }

        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                //loginResult.getAccessToken();
                //loginResult.getRecentlyDeniedPermissions()
                //loginResult.getRecentlyGrantedPermissions()

                boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                Logger.d("API123", loggedIn + " ??");
                getUserProfile(AccessToken.getCurrentAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });


        // Language Change
        String languageName = LocaleManager.getLanguagePref(this);
        if(languageName.equalsIgnoreCase("en")){
            languageText.setText("English");
        }else if(languageName.equalsIgnoreCase("fr")){
            languageText.setText("French");
        }else if(languageName.equalsIgnoreCase("ar")){
            languageText.setText("Arabic");
        }else if(languageName.equalsIgnoreCase("es")){
            languageText.setText("Spanish");
        }else if(languageName.equalsIgnoreCase("pt")){
            languageText.setText("Portuguese");
        }

    }

    private void openLanguageSelectionDialog() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_language_selection);
        dialog.setTitle("");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        RadioGroup radioGroup = dialog.findViewById(R.id.radioGroup);
        RadioButton radioEnglish = (RadioButton) dialog.findViewById(R.id.radioEnglish);
        RadioButton radioFrench = (RadioButton) dialog.findViewById(R.id.radioFrench);
        RadioButton radioArabic = (RadioButton) dialog.findViewById(R.id.radioArabic);
        RadioButton radioSpanish = (RadioButton) dialog.findViewById(R.id.radioSpanish);
        RadioButton radioPortuguese = (RadioButton) dialog.findViewById(R.id.radioPortuguese);

        String languageName = LocaleManager.getLanguagePref(this);
        if(languageName.equalsIgnoreCase("en")){
            radioEnglish.setChecked(true);
        }else if(languageName.equalsIgnoreCase("fr")){
            radioFrench.setChecked(true);
        }else if(languageName.equalsIgnoreCase("ar")){
            radioArabic.setChecked(true);
        }else if(languageName.equalsIgnoreCase("es")){
            radioSpanish.setChecked(true);
        }else if(languageName.equalsIgnoreCase("pt")){
            radioPortuguese.setChecked(true);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                String selectedLanguage = checkedRadioButton.getText().toString();

                if(selectedLanguage.equalsIgnoreCase("English")){
                    setNewLocale(LoginActivity.this, LocaleManager.ENGLISH);
                }else if(selectedLanguage.equalsIgnoreCase("French")){
                    setNewLocale(LoginActivity.this, LocaleManager.FRENCH);
                }else if(selectedLanguage.equalsIgnoreCase("Arabic")){
                    setNewLocale(LoginActivity.this, LocaleManager.ARABIC);
                }else if(selectedLanguage.equalsIgnoreCase("Spanish")){
                    setNewLocale(LoginActivity.this, LocaleManager.SPANISH);
                }else if(selectedLanguage.equalsIgnoreCase("Portuguese")){
                    setNewLocale(LoginActivity.this, LocaleManager.PORTUGUESE);
                }

                dialog.dismiss();

                //Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

            }
        });

        dialog.show();
    }

    private void setNewLocale(AppCompatActivity mContext, @LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(this, language);
        /*Intent intent = new Intent(this, SplashActivity.class);
        this.startActivity(intent);
        this.finishAffinity();*/
        Intent intent = mContext.getIntent();
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

    }

    private void initView() {
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        cardLanguage = findViewById(R.id.cardLanguage);
        languageText = findViewById(R.id.languageText);
        countryCodePicker = findViewById(R.id.countryCodePicker);
        loginButton = findViewById(R.id.login_button);
    }

    private void initClickListener() {
        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.register).setOnClickListener(this);
        findViewById(R.id.forgotPassword).setOnClickListener(this);
        findViewById(R.id.cardLanguage).setOnClickListener(this);
        findViewById(R.id.googleSignIn).setOnClickListener(this);
        //findViewById(R.id.login_button).setOnClickListener(this);
    }

    private boolean isValidInput() {

        if ("".equals(username.getText().toString())) {
            Utils.showAlert(LoginActivity.this, getString(R.string.str_invalid_email_or_mobile));
            return false;
        } else if ("".equals(password.getText().toString())) {
            Utils.showAlert(LoginActivity.this, getString(R.string.str_invalid_password));
            return false;
        } else {
            return true;
        }
    }

    private void login() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        String newUserName;
        if (Patterns.PHONE.matcher(username.getText().toString()).matches()){
         /*newUserName = Utils.getCurrentCountryCode(this) + (username.getText().charAt(0) == '0'
                    ? username.getText().toString().substring(1) : username.getText().toString());*/

            newUserName = countryCodePicker.getSelectedCountryCode() + (username.getText().charAt(0) == '0'
                    ? username.getText().toString().substring(1) : username.getText().toString());

            //Logger.d("USERNAME_99", "Username is: " + newUserName);
        } else{
            newUserName = username.getText().toString().trim();
            //Logger.d("USERNAME_99", "Email is: " + newUserName);
        }

        Logger.d("newUsername",""+newUserName);

        //ArrayList<Params> params = new ArrayList<>();

        try {
            JSONObject params = new JSONObject();
            params.put(Constants.USER_NAME, newUserName);
            params.put(Constants.PASSWORD, password.getText().toString().trim());
            params.put(Constants.FIREBASE_TOKEN, LocalStorage.getToken());

            //add device information
            try {
                PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);

                JSONObject deviceArray = new JSONObject();
                deviceArray.put("version_name",pi.versionName);
                deviceArray.put( "version_number",currentVersionNumber(LoginActivity.this));
                deviceArray.put("model",Build.MODEL);
                deviceArray.put("version_release",Build.VERSION.RELEASE);
                deviceArray.put("brand",Build.BRAND);
                deviceArray.put("device",Build.DEVICE);
                params.put(Constants.LOGIN_DEVICE_DETAIL, deviceArray);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

            Log.d("loginRequest",""+new GsonBuilder().setPrettyPrinting().create().toJson(params));

            Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LOGIN, response -> {
                hud.dismiss();
                try {

                    // Staging login current
                    /*JSONObject object = new JSONObject(response);
                    Logger.e("LLLLLL_Res: ",response);
                    if(object.getString(Constants.USER) != null){

                        LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                        LocalStorage.setUserPass(password.getText().toString().trim());

                        String encryptedUserData = object.getString(Constants.USER);
                        callEncryptEvent(encryptedUserData);
                    }*/




                    // Production login current

                    JSONObject object = new JSONObject(response);

                    Logger.e("LLLLLL_Res: ",response);

                    LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                    LocalStorage.setUserPass(password.getText().toString().trim());
                    User user = new Gson().fromJson(object.getJSONObject(Constants.USER).toString(), User.class);
                    LocalStorage.saveUserDetails(user);


                    Toast.makeText(LoginActivity.this, getString(R.string.user_logged_in_successfully), Toast.LENGTH_LONG).show();
                    getAfroPopularPosts();

                    if (user.getContactNumber().equals("")) {
                        LocalStorage.setIsUserLoggedIn(true);
                        startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                    } else {
                        if ("email".equals(user.getRegisteredWith())) {
                            if (user.getIntroduced()) {
                                LocalStorage.setIsUserLoggedIn(true);
                                proceedToDashboard();
                            } else {
                                LocalStorage.setIsUserLoggedIn(true);
                                startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                            }
                        } else {
                            if (LocalStorage.getIsUserLoggedIn()) {
                                LocalStorage.setIsUserLoggedIn(true);
                                proceedToDashboard();
                            } else {
                                LocalStorage.setIsUserLoggedIn(true);
                                startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                            }
                        }
                    }

                } catch (Exception e) {
                    try {
                        JSONObject object = new JSONObject(response);
                        Logger.e("LLLLLL_Data: ",response);
                        e.printStackTrace();
                        Utils.showAlert(LoginActivity.this, getString(R.string.please_enter_valid_email_or_password));
                    } catch (JSONException e1) {
                        Utils.showAlert(LoginActivity.this, getString(R.string.opps_something_went_wrong));
                        e1.printStackTrace();
                    }
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }


    }

    /*@Override
    public void isSocketConnected(boolean connected) {
        super.isSocketConnected(connected);
        if (connected) {
        }

    }


    @Override
    public void parseResponseInMain(String event, JSONObject argument) {
        super.parseResponseInMain(event, argument);
        switch (event) {
            case UrlEndpoints.GetNotifications: {

                Log.d("SocketNotification9","response : "+argument.toString());

            }
            break;


        }
    }*/

    private void callEncryptEvent(String encryptedUserData){

        Log.d("encryptData",""+encryptedUserData);

        UrlEndpoints.socketIOClient.emit("getEncKeys",new JSONObject(), new Ack() {
            @Override
            public void call(Object... args) {

                runOnUiThread(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void run() {

                        if(args[1] != null){

                            JSONObject jsonObject = (JSONObject) args[1];
                            try {
                                LocalStorage.saveSecretKey(jsonObject.getString("k"));
                                Log.d("secret_key",""+LocalStorage.getSecretKey());
                                decryptLoginData(encryptedUserData);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                });

            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void decryptLoginData(String encryptedUserData){
        AESHelper aesHelper = new AESHelper();
        String decryptedData = aesHelper.decryptData(encryptedUserData);

        try {
            JSONObject object = new JSONObject(decryptedData);

            User user = new Gson().fromJson(object.toString(), User.class);
            LocalStorage.saveUserDetails(user);

            Toast.makeText(LoginActivity.this, getString(R.string.user_logged_in_successfully), Toast.LENGTH_LONG).show();
            getAfroPopularPosts();

            if (!user.getContactNumber().equals("")) {
                if ("email".equals(user.getRegisteredWith())) {
                    if (user.getIntroduced()) {
                        LocalStorage.setIsUserLoggedIn(true);
                        proceedToDashboard();
                    } else {
                        LocalStorage.setIsUserLoggedIn(true);
                        startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                    }
                } else {
                    if (LocalStorage.getIsUserLoggedIn()) {
                        LocalStorage.setIsUserLoggedIn(true);
                        proceedToDashboard();
                    } else {
                        LocalStorage.setIsUserLoggedIn(true);
                        startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                    }
                }
            } else {
                LocalStorage.setIsUserLoggedIn(true);
                startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("My App", "Could not parse malformed JSON: \"" + decryptedData + "\"");
        }

    }

    public String currentVersionNumber(Context a) {
        PackageManager pm = a.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo("com.TBI.afrocamgist",
                    PackageManager.GET_SIGNATURES);
            return pi.versionName
                    + (pi.versionCode > 0 ? " (" + pi.versionCode + ")"
                    : "");
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.login:
                if (isValidInput()) {
                    if (Utils.isConnected()) {
                        login();
                    } else {
                        Utils.showAlert(LoginActivity.this,getString(R.string.please_check_your_internet_connection));
                    }
                }
                break;
            case R.id.register:
                //finish();
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                break;
            case R.id.forgotPassword:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;

            case R.id.cardLanguage:
                openLanguageSelectionDialog();
                break;

            case R.id.googleSignIn:
                //startActivity(new Intent(LoginActivity.this, GoogleLoginHomeActivity.class));
                signIn();
                break;

            /*case R.id.login_button:
                startActivity(new Intent(LoginActivity.this, FbLoginActivity.class));
                break;*/

            default:
                break;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        googleLoginAPI(account);
    }

    private boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(LoginActivity.this) != null;
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            /*String personPhotoUrl = account.getPhotoUrl().toString();
            Glide.with(getApplicationContext()).load(personPhotoUrl)
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfilePic);*/
            // Signed in successfully, show authenticated UI.
            googleLoginAPI(account);
        } catch (ApiException e) {
            Logger.w(TAG, "signInResult:failed code=" + e.getStatusCode()+"\n"+e.getLocalizedMessage()+"\nMEssg: "+e.getMessage());
            googleLoginAPI(null);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void googleLoginAPI(GoogleSignInAccount account) {

        if(account!=null) {
            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();

            if (account.getDisplayName() != null) {
                String[] splitString = account.getDisplayName().split("\\s+");
                Logger.d("first_last", "==" + splitString[0] + "==" + splitString[1]);

                ArrayList<Params> params = new ArrayList<>();
                params.add(new Params("google_id", account.getId()));
                params.add(new Params("first_name", splitString[0]));
                params.add(new Params("last_name", splitString[1]));
                params.add(new Params("email", account.getEmail()));
                //params.add(new Params("username", account.getId()));
                params.add(new Params("type", "google"));
                params.add(new Params(Constants.FIREBASE_TOKEN, LocalStorage.getToken()));

                Logger.d("paramGoogle", "" + params.toString() + "" + params);

                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

                Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LOGIN, response -> {
                    hud.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        Logger.e("LLLLLL_Res: ", response);

                        LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                        //LocalStorage.setUserPass(password.getText().toString().trim());
                        User user = new Gson().fromJson(object.getJSONObject(Constants.USER).toString(), User.class);
                        LocalStorage.saveUserDetails(user);

                        Toast.makeText(LoginActivity.this, getString(R.string.user_logged_in_successfully), Toast.LENGTH_LONG).show();
                        getAfroPopularPosts();

                        if (user.getContactNumber() != null) {
                            if (!user.getContactNumber().equals("")) {
                                if ("google".equals(user.getRegisteredWith())) {
                                    if (user.getIntroduced()) {
                                        LocalStorage.setIsUserLoggedIn(true);
                                        proceedToDashboard();
                                    } else {
                                        LocalStorage.setIsUserLoggedIn(true);
                                        startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                    }
                                } else {
                                    if (LocalStorage.getIsUserLoggedIn()) {
                                        LocalStorage.setIsUserLoggedIn(true);
                                        proceedToDashboard();
                                    } else {
                                        LocalStorage.setIsUserLoggedIn(true);
                                        startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                    }
                                }
                            } else {
                                LocalStorage.setIsUserLoggedIn(true);
                                startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                            }
                        } else {
                            LocalStorage.setIsUserLoggedIn(true);
                            startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                        }


                    } catch (Exception e) {
                        try {
                            JSONObject object = new JSONObject(response);
                            Logger.e("LLLLLL_Data: ", response);
                            LoginManager.getInstance().logOut();
                            googleSignOut();
                            Utils.showAlert(LoginActivity.this, getString(R.string.please_enter_valid_email_or_password));
                        } catch (JSONException e1) {
                            Utils.showAlert(LoginActivity.this, getString(R.string.opps_something_went_wrong));
                            e1.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    private void googleSignOut(){
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //updateUI(null);
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void fbLoginAPI(String id, String first_name, String last_name, String email) {

        if(id!=null){
            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();


            ArrayList<Params> params = new ArrayList<>();
            params.add(new Params("facebook_id", id));
            params.add(new Params("first_name", first_name));
            params.add(new Params("last_name", last_name));
            params.add(new Params("email", email));
            //params.add(new Params("username", id));
            params.add(new Params("type", "facebook"));
            params.add(new Params(Constants.FIREBASE_TOKEN, LocalStorage.getToken()));

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

            Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LOGIN, response -> {
                hud.dismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    Logger.e("LLLLLL_Res: ",response);

                    LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                    //LocalStorage.setUserPass(password.getText().toString().trim());
                    User user = new Gson().fromJson(object.getJSONObject(Constants.USER).toString(), User.class);
                    LocalStorage.saveUserDetails(user);

                    Toast.makeText(LoginActivity.this, getString(R.string.user_logged_in_successfully), Toast.LENGTH_LONG).show();
                    getAfroPopularPosts();

                    if(user.getContactNumber()!=null){
                        if (!user.getContactNumber().equals("")) {
                            if ("facebook".equals(user.getRegisteredWith())) {
                                if (user.getIntroduced()) {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    proceedToDashboard();
                                } else {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            } else {
                                if (LocalStorage.getIsUserLoggedIn()) {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    proceedToDashboard();
                                } else {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            }
                        } else {
                            LocalStorage.setIsUserLoggedIn(true);
                            startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                        }
                    }else {
                        LocalStorage.setIsUserLoggedIn(true);
                        startActivity(new Intent(LoginActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                    }

                } catch (Exception e) {
                    try {
                        JSONObject object = new JSONObject(response);
                        Logger.e("LLLLLL_Data: ",response);
                        Utils.showAlert(LoginActivity.this, getString(R.string.please_enter_valid_email_or_password));
                    } catch (JSONException e1) {
                        Utils.showAlert(LoginActivity.this, getString(R.string.opps_something_went_wrong));
                        e1.printStackTrace();
                    }
                }
            });
        }
    }

    private void proceedToDashboard() {
        //old code
        /*Intent intent = new Intent(LoginActivity.this, BubbleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();*/

        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getAfroPopularPosts() {
        Logger.e("LLLL_Bare: ",LocalStorage.getLoginToken());
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MOST_POPULAR, response -> {
            if ("".equals(response)) {
                Utils.showAlert(LoginActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {

                    //new popular posts response
                    /*JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(LoginActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        if(object.getString(Constants.DATA) != null){

                            String encryptedUserData = object.getString(Constants.DATA);
                            decryptPopularPostsData(encryptedUserData);
                        }

                    }*/

                    //old popular posts response
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(LoginActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);

                        if (postDetails.getPosts() != null && postDetails.getPosts().size() != 0) {
                            LocalStorage.savePopularPosts(postDetails.getPosts());
                        }

                    }

                } catch (Exception e) {
//                    Logger.e("LLLL_Data: ",e.getMessage());
                    e.printStackTrace();
                    Utils.showAlert(LoginActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void decryptPopularPostsData(String encryptedUserData){
        AESHelper aesHelper = new AESHelper();
        String decryptedData = aesHelper.decryptData(encryptedUserData);

        try {
            JSONObject object = new JSONObject(decryptedData);

            PostDetails postDetails = new Gson().fromJson(String.valueOf(object), PostDetails.class);

            if (postDetails.getPosts() != null && postDetails.getPosts().size() != 0) {
                LocalStorage.savePopularPosts(postDetails.getPosts());
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("My App", "Could not parse malformed JSON: \"" + decryptedData + "\"");
        }

    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Logger.d("TAG", object.toString());
                        try {

                            if(object.has("email")){
                                //String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                                //Picasso.get().load(image_url).into(imageView);

                                String id = object.getString("id");
                                String first_name = object.getString("first_name");
                                String last_name = object.getString("last_name");
                                String email = object.getString("email");

                                //Logger.d("FB_DATA_1","=="+first_name+"=="+last_name+"=="+email+"=="+id);

                                fbLoginAPI(id,first_name,last_name,email);

                            }else {
                                Toast.makeText(LoginActivity.this,"Your facebook account doesn't have email",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Logger.d("FbException",""+e.getLocalizedMessage());
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }

}
