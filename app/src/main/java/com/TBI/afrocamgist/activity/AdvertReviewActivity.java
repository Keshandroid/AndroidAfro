package com.TBI.afrocamgist.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.utils.KeyPairBoolData;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.stripe.android.paymentsheet.PaymentSheet;
import com.stripe.android.paymentsheet.PaymentSheetResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class AdvertReviewActivity extends BaseActivity implements View.OnClickListener {

    TextView txt_value_duration, txt_value_goal,txt_value_audience;
    private KProgressHUD hud;

    //pay
    private static final String TAG = "AdvertReviewActivity";
    private String paymentIntentClientSecret;
    private PaymentSheet paymentSheet;
    Button btn_pay;
    private static Dialog popup1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert_review);
        initClickListener();
        txt_value_duration.setText("" + getIntent().getStringExtra("dollar").toString() + "/" + getIntent().getStringExtra("days").toString());

        txt_value_goal.setText(LocalStorage.getAdvertGoalType()+ " | " +LocalStorage.getAdvertURL());

        txt_value_audience.setText(LocalStorage.getAdvertAudienceName() + " | " + LocalStorage.getAdvertAudienceAgeRange() + " | " + LocalStorage.getAdvertAudienceCountries());

        // Hook up the pay button
        btn_pay.setOnClickListener(this::onPayClicked);
        btn_pay.setEnabled(false);
        paymentSheet = new PaymentSheet(this, this::onPaymentSheetResult);
        fetchPaymentIntent();

    }

    private void initClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
        txt_value_duration = findViewById(R.id.txt_value_duration);
        txt_value_goal = findViewById(R.id.txt_value_goal);
        txt_value_audience = findViewById(R.id.txt_value_audience);
        btn_pay = findViewById(R.id.btn_pay);
        //btn_pay.setOnClickListener(this);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View view) {
    /*    if (view == btn_pay) {
            createAdvert();
        }*/
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createAdvertTransaction(String paymentIntentClientSecret){
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        JSONObject request = new JSONObject();
        try {
            request.put("transaction_id",paymentIntentClientSecret);
            request.put("post_id", LocalStorage.getAdvertPostId());
            request.put("audience_id", LocalStorage.getAdvertAudienceID());
            request.put("user_id",LocalStorage.getUserDetails().getUserId());

            JSONObject goal = new JSONObject();
            goal.put("url",LocalStorage.getAdvertURL());
            goal.put("goal_type",LocalStorage.getAdvertGoalType());
            request.put("goal",goal);

            JSONObject budget = new JSONObject();
            budget.put("duration",LocalStorage.getBudgetDuration());
            budget.put("amount",LocalStorage.getBudgetAmount());
            request.put("budget",budget);

            JSONObject order = new JSONObject();
            order.put("status","COMPLETED");
            order.put("id",paymentIntentClientSecret);
            request.put("order",order);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ADVERT_TRANSACTION_CREATE;

        Webservices.getData(Webservices.Method.POST, request, headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AdvertCreateActivity.this, object.getString(Constants.MESSAGE));
                    } else {

                        Logger.d("CreateTransaction",new GsonBuilder().setPrettyPrinting().create().toJson(response));
                        createAdvert(paymentIntentClientSecret);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createAdvert(String paymentIntentClientSecret){
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        JSONObject request = new JSONObject();
        try {
            request.put("transaction_id",paymentIntentClientSecret);
            request.put("post_id", LocalStorage.getAdvertPostId());
            request.put("audience_id", LocalStorage.getAdvertAudienceID());
            request.put("user_id",LocalStorage.getUserDetails().getUserId());

            JSONObject goal = new JSONObject();
            goal.put("url",LocalStorage.getAdvertURL());
            goal.put("goal_type",LocalStorage.getAdvertGoalType());
            request.put("goal",goal);

            JSONObject budget = new JSONObject();
            budget.put("duration",LocalStorage.getBudgetDuration());
            budget.put("amount",LocalStorage.getBudgetAmount());
            request.put("budget",budget);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ADVERT_CREATE;

        Webservices.getData(Webservices.Method.POST, request, headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                //Utils.showAlert(AdvertReviewActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if(object.has("status")){
                        if(object.getBoolean("status")){
                            Logger.d("CreateAdvert",new GsonBuilder().setPrettyPrinting().create().toJson(response));
                            Toast.makeText(AdvertReviewActivity.this,"Advert Created Successfully",Toast.LENGTH_LONG).show();

                            showAlertClick("Success","Advert successfully created and submitted for review. Please allow up to 24hrs for the approval");

                        }
                    }


                    /*if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AdvertReviewActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void showAlert(String title, @Nullable String message) {
        runOnUiThread(() -> {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("Ok", null)
                    .create();
            dialog.show();
        });
    }

    private void showAlertClick(String title, @Nullable String message) {
        runOnUiThread(() -> {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(AdvertReviewActivity.this, DashboardActivity.class));
                        }
                    })
                    .create();
            dialog.show();
        });
    }

    private void showToast(String message) {
        runOnUiThread(() -> Toast.makeText(this, message, Toast.LENGTH_LONG).show());
    }

    private void fetchPaymentIntent() {
        /*final String shoppingCartContent = "{\"items\": [ {\"id\":\"xl-tshirt\"}]}";

        final RequestBody requestBody = RequestBody.create(
                shoppingCartContent,
                MediaType.get("application/json; charset=utf-8")
        );*/

        JSONObject params = new JSONObject();
        try {
            params.put("amount",Integer.parseInt(LocalStorage.getBudgetAmount())*100);
            params.put("currency", "usd");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody requestBody = RequestBody.create(
                params.toString(),
                MediaType.get("application/json; charset=utf-8")
        );

        String url = UrlEndpoints.BASE_URL + UrlEndpoints.STRIPE_CREATE_PAYMENT_INTENT;


        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        new OkHttpClient()
                .newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        showAlert("Failed to load data", "Error: " + e.toString());
                    }

                    @Override
                    public void onResponse(
                            @NonNull Call call,
                            @NonNull Response response
                    ) throws IOException {
                        if (!response.isSuccessful()) {
                            showAlert(
                                    "Failed to load page",
                                    "Error: " + response.toString()
                            );
                        } else {
                            final JSONObject responseJson = parseResponse(response.body());
                            Log.i(TAG, "SECRET_KEY" + new GsonBuilder().setPrettyPrinting().create().toJson(responseJson));

                            paymentIntentClientSecret = responseJson.optString("clientSecret");
                            runOnUiThread(() -> btn_pay.setEnabled(true));
                            Log.i(TAG, "Retrieved PaymentIntent : " + paymentIntentClientSecret);

                        }
                    }
                });
    }

    /*private void fetchPaymentIntentAPI(){

        JSONObject request = new JSONObject();
        try {
            request.put("amount",500);
            request.put("currency", "USD");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.STRIPE_CREATE_PAYMENT_INTENT;

        Webservices.getData(Webservices.Method.POST, request ,headers, url, response -> {
            try {
                JSONObject object = new JSONObject(response);
                if (object.has(Constants.MESSAGE)) {
                    showAlert("Failed to load page", "Error: " + response.toString());
                } else {

                    final JSONObject responseJson = parseResponse(response.body());
                    paymentIntentClientSecret = responseJson.optString("clientSecret");
                    runOnUiThread(() -> payButton.setEnabled(true));
                    Log.i(TAG, "Retrieved PaymentIntent");

                }

            } catch (Exception e) {
                Logger.d("executed111","yes...");
                e.printStackTrace();
                showAlert("Failed to load data", "Error: " + e.toString());
            }
        });

    }*/

    private JSONObject parseResponse(ResponseBody responseBody) {
        if (responseBody != null) {
            try {
                return new JSONObject(responseBody.string());
            } catch (IOException | JSONException e) {
                Log.e(TAG, "Error parsing response", e);
            }
        }

        return new JSONObject();
    }

    private void onPayClicked(View view) {
        PaymentSheet.Configuration configuration = new PaymentSheet.Configuration("Example, Inc.");

        // Present Payment Sheet
        paymentSheet.presentWithPaymentIntent(paymentIntentClientSecret, configuration);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void onPaymentSheetResult(final PaymentSheetResult paymentSheetResult) {
        if (paymentSheetResult instanceof PaymentSheetResult.Completed) {

            Log.d(TAG,""+new GsonBuilder().setPrettyPrinting().create().toJson(paymentSheetResult));
            createAdvertTransaction(paymentIntentClientSecret);
            showToast("Payment complete!");
        } else if (paymentSheetResult instanceof PaymentSheetResult.Canceled) {
            Log.i(TAG, "Payment canceled!");
        } else if (paymentSheetResult instanceof PaymentSheetResult.Failed) {
            Throwable error = ((PaymentSheetResult.Failed) paymentSheetResult).getError();
            error.printStackTrace();
            showAlert("Payment failed", error.getLocalizedMessage());
        }
    }


}

