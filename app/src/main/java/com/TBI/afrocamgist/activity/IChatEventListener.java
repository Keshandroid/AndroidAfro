package com.TBI.afrocamgist.activity;

import org.json.JSONObject;

public interface IChatEventListener
{
    void onSocketEvent(String event, JSONObject payload);
}
