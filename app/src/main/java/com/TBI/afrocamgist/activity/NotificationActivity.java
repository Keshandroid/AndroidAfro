package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.AfroSwaggerPostAdapter;
import com.TBI.afrocamgist.adapters.NotificationAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.chat.MessageDetails;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.notification.Notification;
import com.TBI.afrocamgist.model.notification.NotificationData;
import com.TBI.afrocamgist.model.notification.NotificationDetails;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.model.post.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends BaseActivity implements ConnectivityListener {

    private KProgressHUD hud;
    private SwipeRefreshLayout swipeContainer;
    private Connectivity mConnectivity;
    private boolean isAnyNotificationRead = false;
    private boolean loadingMore = false;

    private ArrayList<NotificationData> notificationsList = new ArrayList<>();
    private NotificationAdapter notificationAdapter;

    private int nextPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        setClickListener();
        initSwipeRefresh();
        //initInternetConnectivityCheckListener();

    }

    @Override
    public void onResume() {
        super.onResume();
        if(Utils.isConnected()){
            getNotifications();
        }
    }

    private void initSwipeRefresh() {

        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getNotifications();
            else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(this, getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud!=null && hud.isShowing()){
                hud.dismiss();
            }
            Utils.closeAlert();
            getNotifications();
        } else {
            //Utils.showAlert(NotificationActivity.this, "No internet connection available.");
        }
    }

    private void setClickListener() {

        findViewById(R.id.mark_all_read).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAllNotificationRead();
            }
        });

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private void getNotifications() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.NOTIFICATIONS + "?page=1";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            swipeContainer.setRefreshing(false);
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(NotificationActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(NotificationActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        NotificationDetails notificationDetails = new Gson().fromJson(response, NotificationDetails.class);

                        if (notificationDetails.getNotificationList() != null) {

                            if(notificationDetails.getNotificationList().size()>0){
                                notificationsList.addAll(notificationDetails.getNotificationList());
                                nextPage = notificationDetails.getNextPage();
                                setNotificationList();
                            }

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(NotificationActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    private void getNotificationsNextPage() {

        loadingMore = true;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.NOTIFICATIONS + "?page=" + nextPage;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            swipeContainer.setRefreshing(false);
            if ("".equals(response)) {
                Utils.showAlert(NotificationActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(NotificationActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        NotificationDetails notificationDetails = new Gson().fromJson(response, NotificationDetails.class);

                        if (notificationDetails.getNotificationList() != null) {
                            if(notificationDetails.getNotificationList().size()>0){
                                loadingMore = false;
                                nextPage = notificationDetails.getNextPage();
                                notificationsList.addAll(notificationDetails.getNotificationList());
                                notificationAdapter.notifyDataSetChanged();
                            }

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(NotificationActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    private void setNotificationList() {

        ListView notifications = findViewById(R.id.notification_list);
        notificationAdapter = new NotificationAdapter(this,notificationsList);
        notifications.setAdapter(notificationAdapter);
        notifications.setOnItemClickListener((parent, view, position, id) -> {

            Notification notification = notificationsList.get(position).getNotificationDetails();
            if (!"read".equals(notificationsList.get(position).getNotificationStatus().toLowerCase())) {
                isAnyNotificationRead = true;
                markNotificationAsRead(notificationsList.get(position).getNotificationId());
            }

            redirectFromNotification(notification,notificationsList.get(position));

        });

        notifications.setOnScrollListener(new AbsListView.OnScrollListener(){

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if((lastInScreen == totalItemCount) && !(loadingMore)){
                    getNotificationsNextPage();
                }
            }
        });

    }

    private void redirectFromNotification(Notification notification,NotificationData notificationData) {

        Logger.d("NotificationRawData", new GsonBuilder().setPrettyPrinting().create().toJson(notification));

        if (notification.getNotificationRawDetails()!=null) {
            if (notification.getNotificationRawDetails().getGroupId() != null) {
                Group group = new Group();
                group.setGroupId(notification.getNotificationRawDetails().getGroupId());
                if(notificationData!=null){
                    Intent intent =new Intent(NotificationActivity.this, AfroViewGroupPostActivity.class);
                    intent.putExtra("group", group);
                    if(notificationData.getNotificationType().equals(Constants.GROUP_INVITE)){
                        intent.putExtra("notification_type", notificationData.getNotificationType());
                    }
                    startActivity(intent);
                }

            } else if (notification.getNotificationRawDetails().getPostId() != null) {
                startActivity(new Intent(NotificationActivity.this, ViewPostActivity.class)
                        .putExtra("postId", notification.getNotificationRawDetails().getPostId()));
            } else if (notification.getNotificationRawDetails().getUserId() != null) {

                if (LocalStorage.getUserDetails().getUserId().equals(notification.getNotificationRawDetails().getUserId()))
                    startActivity(new Intent(NotificationActivity.this, MyProfileActivity.class));
                else
                    startActivity(new Intent(NotificationActivity.this, ProfileActivity.class)
                            .putExtra("userId", notification.getNotificationRawDetails().getUserId()));
            }
        }
    }

    private void markNotificationAsRead(Integer notificationId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.NOTIFICATIONS + "/read/" + notificationId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {});
    }

    private void markAllNotificationRead() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MARK_ALL_NOTIFICATIONS_READ, response -> {
            if ("".equals(response)) {
                Utils.showAlert(NotificationActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(NotificationActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        getNotifications();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(NotificationActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // refresh when return back to notification screen from detail page
        /*if (Utils.isConnected() && isAnyNotificationRead)
            getNotifications();*/

    }

}
