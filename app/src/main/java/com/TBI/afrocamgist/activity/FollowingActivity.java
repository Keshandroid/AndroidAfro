package com.TBI.afrocamgist.activity;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.FollowingAdapter;
import com.TBI.afrocamgist.model.follow.Follow;

import java.util.ArrayList;

public class FollowingActivity extends BaseActivity {

    private ArrayList<Follow> followingsList = new ArrayList<>();
    private String from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);

        if (getIntent().getSerializableExtra("followings")!=null)
            followingsList = (ArrayList<Follow>) getIntent().getSerializableExtra("followings");

        from = getIntent().getStringExtra("from");

        initView();
        setRecyclerView(followingsList);
        setClickListener();

        ((SwipeRefreshLayout)findViewById(R.id.swipeContainer)).setRefreshing(false);
        findViewById(R.id.swipeContainer).setEnabled(false);
    }

    private void initView() {

        TextView followingCount = findViewById(R.id.following_count);
        String count = followingsList== null ? "0" : followingsList.size() + "";
        followingCount.setText(count);
    }

    //set followings user list in the view
    private void setRecyclerView(ArrayList<Follow> followings) {

        RecyclerView following = findViewById(R.id.following_list);
        following.setLayoutManager(new LinearLayoutManager(this));
        following.setAdapter(new FollowingAdapter(this,followings,from));
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }
}
