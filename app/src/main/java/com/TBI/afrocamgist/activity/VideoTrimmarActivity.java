package com.TBI.afrocamgist.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;


import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.fragment.AfroCreatePostFragment;
import com.github.tcking.giraffecompressor.GiraffeCompressor;
import com.kaopiz.kprogresshud.KProgressHUD;
//import com.nagihong.videocompressor.VideoCompressor;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;

import life.knowledge4.videotrimmer.K4LVideoTrimmer;
import life.knowledge4.videotrimmer.interfaces.OnK4LVideoListener;
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener;
import life.knowledge4.videotrimmer.utils.FileUtils;
import rx.Subscriber;

public class VideoTrimmarActivity extends BaseActivity implements OnTrimVideoListener, OnK4LVideoListener {

    K4LVideoTrimmer mVideoTrimmer;
    private KProgressHUD hud;
    private String postStoryFor,isNormalPost,outsideIntent,isOpenFromFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_trimmar);

        mVideoTrimmer = findViewById(R.id.timeLine);

        hud = KProgressHUD.create(VideoTrimmarActivity.this)
                                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                                    .setDimAmount(0.5f)
                                    .setDetailsLabel("Please wait")
                                    .setCancellable(true);

        Intent extraIntent = getIntent();
        String path = "";

        if (extraIntent != null) {
            path = extraIntent.getStringExtra("EXTRA_VIDEO_PATH");
            postStoryFor = getIntent().getStringExtra("postStoryFor");
            isNormalPost = getIntent().getStringExtra("isNormalPost");

            if(getIntent().getStringExtra("outsideIntent")!=null){
                outsideIntent = getIntent().getStringExtra("outsideIntent");
            }

            if(getIntent().getStringExtra("isOpenFromFragment")!=null){
                isOpenFromFragment = getIntent().getStringExtra("isOpenFromFragment");
            }

        }

        mVideoTrimmer = ((K4LVideoTrimmer) findViewById(R.id.timeLine));
        if (mVideoTrimmer != null) {
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setOnK4LVideoListener(this);
            mVideoTrimmer.setVideoURI(Uri.parse(path));
            mVideoTrimmer.setVideoInformationVisibility(true);
            if(isNormalPost.equalsIgnoreCase("normalPost")){
                mVideoTrimmer.setMaxDuration(60);
            }else {
                mVideoTrimmer.setMaxDuration(15);
            }

        }

    }


    @Override
    public void onTrimStarted() {
        Logger.d("TRIM1","111");
        //hud.show();
    }

    @Override
    public void getResult(final Uri uri) {
        Logger.d("TRIM1","222");
        //original
        //File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/Video");

        //old one before new compressor
        //File wallpaperDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/Video");

        //new one after new compressor
        File wallpaperDirectory = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + "/Video"); // new code



        if (!wallpaperDirectory.exists())
            wallpaperDirectory.mkdir();

        File f = new File(wallpaperDirectory, "VID_" + Calendar.getInstance().getTimeInMillis() + ".mp4");
        Logger.e("LLLLL_Android_V: ", String.valueOf(Build.VERSION.SDK_INT));


        if(outsideIntent !=null && outsideIntent.equalsIgnoreCase("video")){
            Intent intent = new Intent(VideoTrimmarActivity.this, DashboardActivity.class);
            intent.putExtra("Path", uri.getPath());
            intent.putExtra("postStoryFor",postStoryFor);
            intent.putExtra("isNormalPost",isNormalPost);
            startActivity(intent);
            finish();
        }else if(isOpenFromFragment != null && isOpenFromFragment.equalsIgnoreCase("isOpenFromFragment")){
            Intent intent = new Intent();
            intent.putExtra("Path", uri.getPath());
            intent.putExtra("postStoryFor",postStoryFor);
            intent.putExtra("isNormalPost",isNormalPost);
            setResult(RESULT_OK,intent);
            finish();

        }else {
            Intent intent = new Intent(VideoTrimmarActivity.this, CreatePostActivity.class);
            intent.putExtra("Path", uri.getPath());
            intent.putExtra("postStoryFor",postStoryFor);
            intent.putExtra("isNormalPost",isNormalPost);
            startActivity(intent);
            finish();
        }


//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
//            Logger.e("LLLLL_Android_V: ", String.valueOf(Build.VERSION.SDK_INT));
//            Intent intent = new Intent(VideoTrimmarActivity.this, CreatePostActivity.class);
//            intent.putExtra("Path", uri.getPath());
//            startActivity(intent);
//            finish();
//        } else {
//            runOnUiThread(() -> GiraffeCompressor.create() //two implementations: mediacodec and ffmpeg,default is mediacodec
//                    .input(new File(uri.getPath())) //set video to be compressed
//                    .output(f) //set compressed video output
//                    .bitRate(Integer.parseInt("2073600"))//set bitrate
//                    .resizeFactor(Float.parseFloat("1.0"))//set video resize factor
//                    .ready()
//                    .subscribe(new Subscriber<GiraffeCompressor.Result>() {
//                        @Override
//                        public void onCompleted() {
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
////                            Logger.e("LLLLLLL_FileSie_E: ",e.getMessage());
//                            e.printStackTrace();
//                        }
//
//                        @Override
//                        public void onStart() {
//                            super.onStart();
//                            hud = KProgressHUD.create(VideoTrimmarActivity.this)
//                                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                                    .setDimAmount(0.5f)
//                                    .setCancellable(true)
//                                    .show();
//                        }
//
//                        @Override
//                        public void onNext(GiraffeCompressor.Result s) {
//                            String msg = String.format("compress completed \ntake time:%s \nout put file:%s", s.getCostTime(), s.getOutput());
//                            File f = new File(s.getOutput());
//                            Logger.e("LLLLL_file: ",msg);
//
//                            float length = f.length() / 1024f; // Size in KB
//                            String value;
//                            if (length >= 1024)
//                                value = length / 1024f + " MB";
//                            else
//                                value = length + " KB";
//                            String text = String.format(Locale.US, "Output: \nName:"+f.getName()+" \nSize: "+ value);
//                            Logger.e("LLLLL_End: ",text);
//
//                            float length1 = new File(uri.getPath()).length() / 1024f;
//                            String value1;
//                            if (length1 >= 1024)
//                                value1 = length1 / 1024f + " MB";
//                            else
//                                value1 = length1 + " KB";
//                            String text1 = String.format(Locale.US, "Input: \nName:"+new File(uri.getPath()).getName()+" \nSize: "+ value1);
//                            Logger.e("LLLLL_End: ",text1);
//
//                            hud.dismiss();
//                            Intent intent = new Intent(VideoTrimmarActivity.this, CreatePostActivity.class);
//                            intent.putExtra("Path", f.getAbsolutePath());
//                            startActivity(intent);
//                            finish();
//
//                        }
//                    }));
//
//        }


    }

    public static String getPathFromGalleryUri(Context context, Uri contentUri) {
        Cursor cursor;
        String[] proj = {MediaStore.Images.Media.DATA};
        cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        if (null == cursor) return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);

        cursor.close();
        return result;
    }

    @Override
    public void cancelAction() {
        Logger.d("TRIM1","333");
        mVideoTrimmer.destroy();
        finish();
    }

    @Override
    public void onError(final String message) {
        Logger.d("TRIM1","444");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Logger.d("TRIM1","555");
                Toast.makeText(VideoTrimmarActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onVideoPrepared() {
        Logger.d("TRIM1","666");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Logger.d("TRIM1","777");
                Toast.makeText(VideoTrimmarActivity.this, "onVideoPrepared", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
