package com.TBI.afrocamgist.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.advert.Advert;
import com.TBI.afrocamgist.model.advert.AdvertPost;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.otheruser.OtherUser;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.utils.KeyPairBoolData;
import com.TBI.afrocamgist.utils.MultiSpinnerSearch;
import com.TBI.bubblesview.Constant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.mirko.rangeseekbar.OnRangeSeekBarListener;
import it.mirko.rangeseekbar.RangeSeekBar;

public class AdvertSelectTargetAudienceActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txt_min_age, txt_max_age, titleAudience,screenTitle,txtCreateNew;
    RangeSeekBar rangeSeekBarAge;
    Button btn_create,btn_delete,btn_edit;
    //RecyclerView recycler_view_target_audience;
    Activity activity = this;
    List<OtherUser> targetAudienceList;
    MultiSpinnerSearch countries_spinner, interest_spinner;
    List<KeyPairBoolData> countriesList, interestList;
    //List<String> audienceSpinnerList;
    //Spinner create_audience_spinner;
    Advert selectedAudience;
    EditText edt_audience_name;
    private RadioButton rbMale, rbFemale, rbAll;
    private KProgressHUD hud;
    private RelativeLayout rlCreateEditSpinner;
    private ImageView imgEditPen;

    private boolean isAudienceCreated = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert_select_target_audience);

        if(getIntent()!=null){
            if(getIntent().getSerializableExtra("selectedAudience")!=null){
                selectedAudience = (Advert) getIntent().getSerializableExtra("selectedAudience");
            }

            isAudienceCreated = getIntent().getBooleanExtra("isCreated",false);

        }



        initClickListener();

        //getTargetAudience();

        if(isAudienceCreated){
            //create_audience_spinner.setSelection(1);
            screenTitle.setText("Target Audience");
            titleAudience.setText("Existing target audience");
            rlCreateEditSpinner.setVisibility(View.GONE);
            txtCreateNew.setVisibility(View.GONE);

            setExistingAudienceData();
        }else {
            //create_audience_spinner.setSelection(0);
            screenTitle.setText("Target Audience");
            titleAudience.setText("Create your target audience");
            rlCreateEditSpinner.setVisibility(View.GONE);
            txtCreateNew.setVisibility(View.VISIBLE);

            setCreateAudienceData();
        }
    }

    private void initClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });

        txt_min_age = findViewById(R.id.txt_min_age);
        txt_max_age = findViewById(R.id.txt_max_age);
        rangeSeekBarAge = findViewById(R.id.rangeSeekBarAge);
        btn_create = findViewById(R.id.btn_create);
        btn_delete = findViewById(R.id.btn_delete);
        btn_edit = findViewById(R.id.btn_edit);
        imgEditPen = findViewById(R.id.imgEditPen);
        screenTitle = findViewById(R.id.screenTitle);
        titleAudience = findViewById(R.id.titleAudience);
        txtCreateNew = findViewById(R.id.txtCreateNew);

        rlCreateEditSpinner = findViewById(R.id.rlCreateEditSpinner);

        rbMale = findViewById(R.id.rbMale);
        rbFemale = findViewById(R.id.rbFemale);
        rbAll = findViewById(R.id.rbAll);

        countries_spinner = findViewById(R.id.countries_spinner);
        interest_spinner = findViewById(R.id.interest_spinner);
        //create_audience_spinner = findViewById(R.id.create_audience_spinner);
        edt_audience_name = findViewById(R.id.edt_audience_name);
//        recycler_view_target_audience = findViewById(R.id.recycler_view_target_audience);
//        recycler_view_target_audience.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));

        btn_create.setOnClickListener(this);
        btn_delete.setOnClickListener(this);
        btn_edit.setOnClickListener(this);

        rangeSeekBarAge.setStartProgress(13);
        rangeSeekBarAge.setEndProgress(65);

        rangeSeekBarAge.setOnRangeSeekBarListener(new OnRangeSeekBarListener() {
            @Override
            public void onRangeValues(RangeSeekBar rangeSeekBar, int start, int end) {
                txt_min_age.setText(start + "");
                txt_max_age.setText(end + "");
            }
        });

        setUpCountriesList();
        setUpInterestList();

        /*audienceSpinnerList = new ArrayList<>();
        audienceSpinnerList.add("Create Audience");
        audienceSpinnerList.add("Edit Audience");
        SpinnerAdapter audienceAdapter = new SpinnerAdapter(activity, audienceSpinnerList);
        create_audience_spinner.setAdapter(audienceAdapter);

        create_audience_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 1) {

                    btn_create.setVisibility(View.GONE);
                    btn_edit.setVisibility(View.VISIBLE);

                    if (selectedAudience != null) {

                        Logger.d("SELECTED_AUDIENCE",new GsonBuilder().setPrettyPrinting().create().toJson(selectedAudience));


                        //set audience name
                        if(selectedAudience.getName() != null && selectedAudience.getName().length() > 0){
                            edt_audience_name.setText(selectedAudience.getName());
                        }

                        //selected countries
                        if(selectedAudience.getCountry()!=null && !selectedAudience.getCountry().isEmpty()){
                            setUpSelectedCountriesList(selectedAudience.getCountry());
                        }

                        //selected interests
                        if(selectedAudience.getInterests()!=null && !selectedAudience.getInterests().isEmpty()){
                            setUpSelectedInterestsList(selectedAudience.getInterests());
                        }

                        //select Age range
                        if(selectedAudience.getMinAge()!=null && selectedAudience.getMaxAge()!=null){
                            rangeSeekBarAge.setStartProgress(Integer.parseInt(selectedAudience.getMinAge()));
                            rangeSeekBarAge.setEndProgress(Integer.parseInt(selectedAudience.getMaxAge()));

                            txt_min_age.setText(selectedAudience.getMinAge());
                            txt_max_age.setText(selectedAudience.getMaxAge());
                        }

                        //select gender
                        if(selectedAudience.getGender()!=null){
                            if(selectedAudience.getGender().toString().equalsIgnoreCase("m")){
                                rbMale.setChecked(true);
                            }else if(selectedAudience.getGender().toString().equalsIgnoreCase("f")){
                                rbFemale.setChecked(true);
                            }else {
                                rbAll.setChecked(true);
                            }
                        }

                    }
                } else {

                    btn_create.setVisibility(View.VISIBLE);
                    btn_edit.setVisibility(View.GONE);

                    edt_audience_name.setText("");
                    //setUpCountriesList();//empty list
                    //setUpInterestList();//empty list

                    Logger.d("SELECTED_AUDIENCE"," countries create "+countries_spinner.getSelectedItem().toString());


                    //reset seekbar
                    rangeSeekBarAge.setStartProgress(13);
                    rangeSeekBarAge.setEndProgress(65);
                    txt_min_age.setText("13");//default values
                    txt_max_age.setText("65");//default values
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

    }

    private void setExistingAudienceData(){
        imgEditPen.setVisibility(View.VISIBLE);
        btn_create.setVisibility(View.GONE);
        btn_edit.setVisibility(View.VISIBLE);
        btn_delete.setVisibility(View.VISIBLE);

        if (selectedAudience != null) {

            Logger.d("SELECTED_AUDIENCE",new GsonBuilder().setPrettyPrinting().create().toJson(selectedAudience));


            //set audience name
            if(selectedAudience.getName() != null && selectedAudience.getName().length() > 0){
                edt_audience_name.setText(selectedAudience.getName());
            }

            //selected countries
            if(selectedAudience.getCountry()!=null && !selectedAudience.getCountry().isEmpty()){
                setUpSelectedCountriesList(selectedAudience.getCountry());
            }

            //selected interests
            if(selectedAudience.getInterests()!=null && !selectedAudience.getInterests().isEmpty()){
                setUpSelectedInterestsList(selectedAudience.getInterests());
            }

            //select Age range
            if(selectedAudience.getMinAge()!=null && selectedAudience.getMaxAge()!=null){
                rangeSeekBarAge.setStartProgress(Integer.parseInt(selectedAudience.getMinAge()));
                rangeSeekBarAge.setEndProgress(Integer.parseInt(selectedAudience.getMaxAge()));

                txt_min_age.setText(selectedAudience.getMinAge());
                txt_max_age.setText(selectedAudience.getMaxAge());
            }

            //select gender
            if(selectedAudience.getGender()!=null){
                if(selectedAudience.getGender().toString().equalsIgnoreCase("m")){
                    rbMale.setChecked(true);
                }else if(selectedAudience.getGender().toString().equalsIgnoreCase("f")){
                    rbFemale.setChecked(true);
                }else {
                    rbAll.setChecked(true);
                }
            }

        }
    }

    private void setCreateAudienceData(){
        imgEditPen.setVisibility(View.GONE);
        btn_create.setVisibility(View.VISIBLE);
        btn_edit.setVisibility(View.GONE);
        btn_delete.setVisibility(View.GONE);

        edt_audience_name.setText("");
        edt_audience_name.requestFocus();
        //setUpCountriesList();//empty list
        //setUpInterestList();//empty list

        Logger.d("SELECTED_AUDIENCE"," countries create "+countries_spinner.getSelectedItem().toString());


        //reset seekbar
        rangeSeekBarAge.setStartProgress(13);
        rangeSeekBarAge.setEndProgress(65);
        txt_min_age.setText("13");//default values
        txt_max_age.setText("65");//default values
    }

    private void setUpCountriesList() {

        countries_spinner.setSearchEnabled(true);
        countries_spinner.setSearchHint("Select Countries");
        countries_spinner.setEmptyTitle("Not Data Found!");
        countries_spinner.setShowSelectAllButton(true);
        countries_spinner.setClearText("Close & Clear");

        countriesList = Utils.getCountryList(activity);

        countries_spinner.setItems(countriesList, items -> {
            //The followings are selected items.
            for (int i = 0; i < items.size(); i++) {
                Log.e("SELECTED_AUDIENCE", " listener "+ i + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
            }
        });

    }

    private void setUpSelectedCountriesList(ArrayList<String> country) {

        countries_spinner.setSearchEnabled(true);
        countries_spinner.setSearchHint("Select Countries");
        countries_spinner.setEmptyTitle("Not Data Found!");
        countries_spinner.setShowSelectAllButton(true);
        countries_spinner.setClearText("Close & Clear");

        countriesList = Utils.getSelectedCountryList(activity,country);

        countries_spinner.setItems(countriesList, items -> {
            //The followings are selected items.
            for (int i = 0; i < items.size(); i++) {
                Log.e("SELECTED_AUDIENCE", " listener "+ i + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
            }
        });
    }

    private void setUpInterestList() {

        interest_spinner.setSearchEnabled(true);
        interest_spinner.setSearchHint("Select Interests");
        interest_spinner.setEmptyTitle("Not Data Found!");
        interest_spinner.setShowSelectAllButton(true);
        interest_spinner.setClearText("Close & Clear");

        interestList = Utils.getInterestsList(activity);
        interest_spinner.setItems(interestList, items -> {
            //The followings are selected items.
            for (int i = 0; i < items.size(); i++) {
                Log.e("SELECTED_AUDIENCE", " listener "+ i + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
            }
        });

    }

    private void setUpSelectedInterestsList(ArrayList<String> interests){
        interest_spinner.setSearchEnabled(true);
        interest_spinner.setSearchHint("Select Interests");
        interest_spinner.setEmptyTitle("Not Data Found!");
        interest_spinner.setShowSelectAllButton(true);
        interest_spinner.setClearText("Close & Clear");

        interestList = Utils.getSelectedInterestsList(activity,interests);

        interest_spinner.setItems(interestList, items -> {
            //The followings are selected items.
            for (int i = 0; i < items.size(); i++) {
                Log.e("SELECTED_AUDIENCE", " listener "+ i + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
            }
        });
    }

    /*private void getTargetAudience(){
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ADVERT_GET_AUDIENCE;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if ("".equals(response)) {
                //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AdvertCreateActivity.this, object.getString(Constants.MESSAGE));
                    } else {

                        Logger.d("GetAudience",new GsonBuilder().setPrettyPrinting().create().toJson(response));

                        AdvertPost advertPost = new Gson().fromJson(response, AdvertPost.class);
                        if (advertPost.getPosts().size() > 0) {
                            setTargetAudienceData(advertPost.getPosts());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }*/





    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createAudience(){
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        JSONObject request = new JSONObject();
        try {
            request.put("age_range",txt_min_age.getText().toString() +"-"+txt_max_age.getText().toString());
            request.put("name", edt_audience_name.getText().toString());

            if(rbMale.isChecked()){
                request.put("gender", "male");
            }else if(rbFemale.isChecked()){
                request.put("gender", "female");
            }else if(rbAll.isChecked()){
                request.put("gender", "all");
            }


            //send country array
            JSONArray countryParam = new JSONArray();
            for (int i=0;i<countries_spinner.getSelectedItems().size();i++){
                KeyPairBoolData currentData = countries_spinner.getSelectedItems().get(i);
                countryParam.put(currentData.getName());
            }
            request.put("country", countryParam);


            //send interests array
            JSONArray interestParams = new JSONArray();
            for (int i=0;i<interest_spinner.getSelectedItems().size();i++){
                KeyPairBoolData currentData = interest_spinner.getSelectedItems().get(i);
                interestParams.put(currentData.getName());
            }
            request.put("interests", interestParams);


            /*String[] elementInterest = interest_spinner.getSelectedItems().toString().split(",");
            List<String> listInterests = Arrays.asList(elementInterest);
            ArrayList<String> listOfStringInterest = new ArrayList<String>(listInterests);
            JSONArray interestsParam = new JSONArray();
            interestsParam.put(listOfStringInterest);
            request.put("interests", interestsParam);*/


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ADVERT_CREATE_AUDIENCE;

        Webservices.getData(Webservices.Method.POST, request, headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AdvertCreateActivity.this, object.getString(Constants.MESSAGE));
                    } else {

                        if(object.has(Constants.DATA)){
                            Toast.makeText(this,"Audience Created",Toast.LENGTH_SHORT).show();

                            Advert advert = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), Advert.class);

                            Logger.d("CreateAudience",new GsonBuilder().setPrettyPrinting().create().toJson(advert));


                            if(advert!=null){
                                if(advert.getAudience_id()!=null){
                                    LocalStorage.setAdvertAudienceID(advert.getAudience_id());
                                    LocalStorage.setAdvertAudienceName(advert.getName());
                                    LocalStorage.setAdvertAudienceCountries(String.join(", ", advert.getCountry()));
                                    LocalStorage.setAdvertAudienceAgeRange(advert.getAge_range());

                                    Intent intent = new Intent(this, AdvertSelectDurationSpanActivity.class);
                                    startActivity(intent);
                                    finish();
                                }else {
                                    Toast.makeText(this,"Audience ID Not Found",Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(this,"Audience ID Not Found",Toast.LENGTH_SHORT).show();
                            }

                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void editAudience(){
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        JSONObject mainParams = new JSONObject();

        try {
            mainParams.put("audience_id",selectedAudience.getAudience_id());

            JSONObject request = new JSONObject();
            request.put("age_range",txt_min_age.getText().toString() +"-"+txt_max_age.getText().toString());
            request.put("name", edt_audience_name.getText().toString());

            if(rbMale.isChecked()){
                request.put("gender", "male");
            }else if(rbFemale.isChecked()){
                request.put("gender", "female");
            }else if(rbAll.isChecked()){
                request.put("gender", "all");
            }


            //send country array
            JSONArray countryParam = new JSONArray();
            for (int i=0;i<countries_spinner.getSelectedItems().size();i++){
                KeyPairBoolData currentData = countries_spinner.getSelectedItems().get(i);
                countryParam.put(currentData.getName());
            }
            request.put("country", countryParam);


            //send interests array
            JSONArray interestParams = new JSONArray();
            for (int i=0;i<interest_spinner.getSelectedItems().size();i++){
                KeyPairBoolData currentData = interest_spinner.getSelectedItems().get(i);
                interestParams.put(currentData.getName());
            }
            request.put("interests", interestParams);


            mainParams.put("update",request);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ADVERT_EDIT_AUDIENCE;

        Webservices.getData(Webservices.Method.PUT, mainParams, headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                hud.dismiss();
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(AdvertSelectTargetAudienceActivity.this, object.getString(Constants.MESSAGE));
                    } else {

                        if(object.has(Constants.DATA)){
                            Advert advert = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), Advert.class);

                            Logger.d("EditAudience",new GsonBuilder().setPrettyPrinting().create().toJson(advert));

                            if(advert!=null){
                                if(advert.getAudience_id()!=null){
                                    LocalStorage.setAdvertAudienceID(advert.getAudience_id());
                                    LocalStorage.setAdvertAudienceName(advert.getName());
                                    LocalStorage.setAdvertAudienceCountries(String.join(", ", advert.getCountry()));
                                    LocalStorage.setAdvertAudienceAgeRange(advert.getAge_range());

                                    Intent intent = new Intent(AdvertSelectTargetAudienceActivity.this, AdvertSelectDurationSpanActivity.class);
                                    startActivity(intent);
                                    finish();
                                }else {
                                    Toast.makeText(AdvertSelectTargetAudienceActivity.this,"Audience ID Not Found",Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(AdvertSelectTargetAudienceActivity.this,"Audience ID Not Found",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                } catch (Exception e) {
                    Utils.showAlert(AdvertSelectTargetAudienceActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }


    /*private void setTargetAudienceData(ArrayList<Advert> posts) {
        SelectTargetAdapter adapter = new SelectTargetAdapter(activity, posts);
        recycler_view_target_audience.setAdapter(adapter);
    }*/

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View view) {
        /*if (view == btn_next) {

            if(selectedAudience!=null){
                if(selectedAudience.getAudience_id()!=null){
                    LocalStorage.setAdvertAudienceID(selectedAudience.getAudience_id());
                    LocalStorage.setAdvertAudienceName(selectedAudience.getName());
                    LocalStorage.setAdvertAudienceCountries(String.join(", ", selectedAudience.getCountry()));
                    LocalStorage.setAdvertAudienceAgeRange(selectedAudience.getAge_range());




                    Intent intent = new Intent(this, AdvertSelectDurationSpanActivity.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(this,"Audience ID Not Found",Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this,"Audience ID Not Found",Toast.LENGTH_SHORT).show();
            }


        }else*/

        if(view == btn_create){
            validateData("create");
        }else if(view == btn_delete){
            deleteAudience();
        }else if(view == btn_edit){
            validateData("edit");
        }
    }

    private void deleteAudience() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ADVERT_DELETE_AUDIENCE + "/" + selectedAudience.getAudience_id() ;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(),headers, url, response -> {
            hud.dismiss();
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    if(object.getString(Constants.MESSAGE).equalsIgnoreCase("audience deleted")){
                        finish();
                        Toast.makeText(this,"Audience Deleted",Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Logger.d("DeleteAudience",new GsonBuilder().setPrettyPrinting().create().toJson(response));
                }

            } catch (Exception e) {
                Utils.showAlert(this, "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void validateData(String audienceType) {
        if(edt_audience_name.getText().toString().equals("")){
            Toast.makeText(this,"Please enter audience name",Toast.LENGTH_SHORT).show();
            return;
        }

        if (countries_spinner!=null){
            if(countries_spinner.getSelectedItems().toString().equals("")){
                return;
            }
        }

        if (interest_spinner!=null){
            if(interest_spinner.getSelectedItems().toString().equals("")){
                return;
            }
        }

        if(audienceType.equalsIgnoreCase("edit")){
            editAudience();
        }else {
            createAudience();
        }




    }

    /*public class SelectTargetAdapter extends RecyclerView.Adapter<SelectTargetAdapter.DataObjectHolder> {

        private Activity context;
        private ArrayList<Advert> users;
        int row_index;


        public SelectTargetAdapter(Activity context, ArrayList<Advert> users) {
            this.context = context;
            this.users = users;
            row_index = 0;
        }

        @NonNull
        @Override
        public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_select_target_audience, viewGroup, false);

            return new DataObjectHolder(view);
        }


        @Override
        public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

            Advert user = users.get(position);

            String fullName = user.getName();
            if (fullName != null && !fullName.isEmpty())
                holder.rbTargetAudience.setText(fullName);

            holder.rbTargetAudience.setChecked(holder.getAdapterPosition() == row_index);
            selectedAudience = users.get(row_index);

            holder.rbTargetAudience.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    create_audience_spinner.setSelection(0);
                    row_index = holder.getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return users.size();
        }

        class DataObjectHolder extends RecyclerView.ViewHolder {
            RadioButton rbTargetAudience;

            DataObjectHolder(View itemView) {
                super(itemView);
                rbTargetAudience = itemView.findViewById(R.id.rbTargetAudience);
            }
        }
    }

    public class SpinnerAdapter extends BaseAdapter {
        Context context;
        List<String> arrayList = new ArrayList<>();
        LayoutInflater layoutInflater;

        public SpinnerAdapter(Context context, List<String> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
            layoutInflater = LayoutInflater.from(this.context);
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.item_spinner_audience, parent, false);
                holder = new ViewHolder();

                holder.txt = (TextView) convertView.findViewById(R.id.txt);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.txt.setText(arrayList.get(position));
            return convertView;
        }

        class ViewHolder {
            TextView txt;
        }
    }*/

}