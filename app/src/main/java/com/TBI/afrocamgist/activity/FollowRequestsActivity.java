package com.TBI.afrocamgist.activity;


import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.FollowRequestAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.follow.FollowDetails;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.connection.Connectivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class FollowRequestsActivity extends BaseActivity implements ConnectivityListener,FollowRequestAdapter.OnRequestClickListener {

    private KProgressHUD hud;
    private Connectivity mConnectivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_requests);

        setClickListener();
        initInternetConnectivityCheckListener();
    }

    private void setClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud!=null && hud.isShowing()){
                hud.dismiss();
            }
            Utils.closeAlert();
            getFollowRequestList();
        } else {
            Utils.showAlert(FollowRequestsActivity.this, getString(R.string.no_internet_connection_available));
        }
    }

    private void getFollowRequestList() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.FOLLOW_REQUESTS, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                hud.dismiss();
                if ("".equals(response)) {
                    Utils.showAlert(FollowRequestsActivity.this, getString(R.string.opps_something_went_wrong));
                } else {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(FollowRequestsActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            FollowDetails followDetails = new Gson().fromJson(response,FollowDetails.class);

                            if (followDetails.getFollowList()==null || followDetails.getFollowList().isEmpty()) {
                                findViewById(R.id.no_follow_request).setVisibility(View.VISIBLE);
                                findViewById(R.id.follow_request_list).setVisibility(View.GONE);
                            } else {
                                List<Follow> followList = followDetails.getFollowList();


                                //Remove duplicate values from list

                                /*List<Follow> noRepeat = new ArrayList<Follow>();
                                for (Follow event : followList) {
                                    boolean isFound = false;
                                    for (Follow e : noRepeat) {
                                        if (e.getUserId().equals(event.getUserId()) || (e.equals(event))) {
                                            isFound = true;
                                            break;
                                        }
                                    }
                                    if (!isFound) noRepeat.add(event);
                                }*/

                                Logger.d("followRequest", new GsonBuilder().setPrettyPrinting().create().toJson(followDetails.getFollowList().size()));

                                setRecyclerView(followList);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showAlert(FollowRequestsActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            }
        });
    }

    private void setRecyclerView(List<Follow> followRequests) {

        RecyclerView following = findViewById(R.id.follow_request_list);
        following.setLayoutManager(new LinearLayoutManager(this));
        following.setAdapter(new FollowRequestAdapter(this,followRequests,this));
    }

    private void requestAccept(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/confirm-follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FollowRequestsActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        getFollowRequestList();
                    }

                } catch (Exception e) {
                    Utils.showAlert(FollowRequestsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void requestDeny(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/cancel-follow-request";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FollowRequestsActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        getFollowRequestList();
                    }

                } catch (Exception e) {
                    Utils.showAlert(FollowRequestsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
    }

    @Override
    public void onAcceptRequest(Follow follower) {
        requestAccept(follower.getUserId());
    }

    @Override
    public void onDenyRequest(Follow follower) {
        requestDeny(follower.getUserId());
    }
}
