package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.MyFollowingAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.follow.Follow;
import com.TBI.afrocamgist.model.follow.FollowDetails;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyFollowingsActivity extends BaseActivity implements MyFollowingAdapter.OnFollowingClickListener {

    private KProgressHUD hud;
    private TextView followingCount;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);
        initView();

        setClickListener();
        initSwipeRefresh();
//        if (Utils.isConnected())
//            getFollowingList();
//        else
//            Utils.showAlert(this, "Please check your internet connection");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utils.isConnected())
            getFollowingList();
        else
            Utils.showAlert(this,getString(R.string.please_check_your_internet_connection));
    }

    private void initView() {

        followingCount = findViewById(R.id.following_count);
        swipeContainer = findViewById(R.id.swipeContainer);
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getFollowingList();
            else
                Utils.showAlert(this, getString(R.string.please_check_your_internet_connection));
        });
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private void getFollowingList() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.FOLLOWING, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                swipeContainer.setRefreshing(false);
                hud.dismiss();
                if ("".equals(response)) {
                    Utils.showAlert(MyFollowingsActivity.this, getString(R.string.opps_something_went_wrong));
                } else {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(MyFollowingsActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            FollowDetails followDetails = new Gson().fromJson(response, FollowDetails.class);
                            String count = followDetails.getFollowList() == null ? "0" : followDetails.getFollowList().size() + "";
                            followingCount.setText(count);
                            if (followDetails.getFollowList() == null || followDetails.getFollowList().isEmpty()) {
                                findViewById(R.id.no_following).setVisibility(View.VISIBLE);
                                findViewById(R.id.following_list).setVisibility(View.GONE);
                            } else {
                                ArrayList<Follow> followList = followDetails.getFollowList();
                                followList.add(0, null);
                                setRecyclerView(followList);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showAlert(MyFollowingsActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            }
        });
    }

    private void setRecyclerView(ArrayList<Follow> followings) {

        RecyclerView following = findViewById(R.id.following_list);
        following.setLayoutManager(new LinearLayoutManager(this));
        following.setAdapter(new MyFollowingAdapter(this, followings, this));
    }

    @Override
    public void onProfileClick(Follow follower) {
        startActivity(new Intent(MyFollowingsActivity.this, ProfileActivity.class).putExtra("userId", follower.getUserId()));
    }

    @Override
    public void onFollowAndUnFollowToggleClick(Follow follower, Boolean isFollowed) {
        if (isFollowed)
            followUser(follower.getUserId());
        else
            unfollowUser(follower.getUserId());
    }

    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(MyFollowingsActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    Utils.showAlert(MyFollowingsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void unfollowUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/cancel-follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(MyFollowingsActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    Utils.showAlert(MyFollowingsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }
}
