package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.StoryConstants;
import com.TBI.afrocamgist.SwipeHelper;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.VideoPreLoadingService;
import com.TBI.afrocamgist.adapters.EHealthAdapterExpand;
import com.TBI.afrocamgist.adapters.EhealthVideosAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.home.VideosAdapter;
import com.TBI.afrocamgist.model.advert.AdvertPost;
import com.TBI.afrocamgist.model.ehealth.EHealth;
import com.TBI.afrocamgist.model.ehealth.EHealthData;
import com.TBI.afrocamgist.model.ehealth.EHealthSubPlans;
import com.TBI.afrocamgist.model.ehealth.Testimonials;
import com.TBI.afrocamgist.model.ehealth.TestimonialsData;
import com.TBI.afrocamgist.model.post.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import im.ene.toro.widget.Container;

public class EHealthActivity extends BaseActivity implements EHealthAdapterExpand.OnPlanDetailClickListener, EhealthVideosAdapter.OnTestimonialsClickListener {

    EHealthAdapterExpand listAdapter;
    ExpandableListView expListView;
//    List<String> listDataHeader;
//    HashMap<String, List<String>> listDataChild;

    //Load Videos
    private Container mainList;
    SnapHelper snapHelper;
    EhealthVideosAdapter ehealthVideosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_health);

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });

        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        mainList = (Container) findViewById(R.id.testimonialsVideoList);


        //get all afro e-health plans (uncomment this to get e-health plans and Un-Hide the xml layout)
        //getEHealthPlans();
        //getEHealthVideo();





        setExpandableListViewHeight(expListView, -1);

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                setExpandableListViewHeight(parent, groupPosition);

                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();

                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                /*Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                /*Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                /*Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();*/
                return false;
            }
        });

    }

    private void setExpandableListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        if (listAdapter == null) {
            return;
        }

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            view = listAdapter.getGroupView(i, false, view, listView);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
            if(((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                View listItem = null;
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    listItem = listAdapter.getChildView(i, j, false, listItem, listView);
                    listItem.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, View.MeasureSpec.UNSPECIFIED));
                    listItem.measure(
                            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                    totalHeight += listItem.getMeasuredHeight();
                }
            }
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void getEHealthPlans(){
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.GET_E_HEALTH_PLANS;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if ("".equals(response)) {
                //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AdvertCreateActivity.this, object.getString(Constants.MESSAGE));
                    } else {

                        Logger.d("GetEHealth","Plans : "+new GsonBuilder().setPrettyPrinting().create().toJson(response));

                        EHealthData eHealthData = new Gson().fromJson(response, EHealthData.class);
                        if (eHealthData.geteHealth().size() > 0) {
                            setEHealthData(eHealthData.geteHealth());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void getEHealthVideo(){
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.GET_E_HEALTH_VIDEO;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if ("".equals(response)) {
                //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AdvertCreateActivity.this, object.getString(Constants.MESSAGE));
                    } else {

                        Logger.d("GetEHealth","testimonialsData : "+new GsonBuilder().setPrettyPrinting().create().toJson(response));

                        TestimonialsData testimonialsData = new Gson().fromJson(response, TestimonialsData.class);
                        if (testimonialsData.getTestimonials().size() > 0) {
                            startPreLoadingService(testimonialsData.getTestimonials());

                            setTestimonialAdapter(testimonialsData.getTestimonials());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setTestimonialAdapter(ArrayList<Testimonials> testimonials){
        ViewCompat.setNestedScrollingEnabled(mainList, false);
        mainList.setNestedScrollingEnabled(false);
        mainList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mainList.setItemAnimator(new DefaultItemAnimator());
        ehealthVideosAdapter = new EhealthVideosAdapter(this,testimonials,this);
        mainList.setAdapter(ehealthVideosAdapter);

        snapHelper = new PagerSnapHelper();
        mainList.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(mainList);

    }


    private void setEHealthData(ArrayList<EHealth> eHealths) {

        listAdapter = new EHealthAdapterExpand(this, eHealths,this);
        expListView.setAdapter(listAdapter);

        for (int i=0;i<eHealths.size();i++){
            expListView.expandGroup(i);
            setExpandableListViewHeight(expListView,i+1);
        }

    }

    private void startPreLoadingService(ArrayList<Testimonials> testimonials) {

        ArrayList<String> videoList = new ArrayList<>();

        for (int i=0;i<testimonials.size();i++){
            if(testimonials.get(i).getTestimonial_video()!= null && !testimonials.get(i).getTestimonial_video().equals("")){
                String url = UrlEndpoints.MEDIA_BASE_URL + testimonials.get(i).getTestimonial_video();
                videoList.add(url);
            }
        }

        Logger.d("CACHING_DATA","testionialsCache : "+new GsonBuilder().setPrettyPrinting().create().toJson(videoList));

        Intent preloadingServiceIntent = new Intent(EHealthActivity.this, VideoPreLoadingService.class);
        preloadingServiceIntent.putStringArrayListExtra(StoryConstants.VIDEO_LIST, videoList);
        startService(preloadingServiceIntent);
    }

    @Override
    public void onPlanDetailClick(EHealthSubPlans subPlans) {

        Intent intent = new Intent(EHealthActivity.this, EHealthPlanDetailActivity.class);
        intent.putExtra("planName",subPlans.getName());
        intent.putExtra("planPrice",subPlans.getPrice());
        intent.putExtra("planDesc",subPlans.getDescription());
        intent.putExtra("planID",subPlans.getId());

        if(subPlans.getBenefits()!=null && !subPlans.getBenefits().isEmpty()){
            intent.putExtra("planBenefits",subPlans.getBenefits().get(0));
        }

        startActivity(intent);

        //setExpandableListViewHeight(expListView,position);
    }

    @Override
    public void onClickTestimonials(Testimonials post) {
        startActivity(new Intent(EHealthActivity.this, ViewTestimonialsVideoActivity.class)
                .putExtra("testimonials", post));

    }
}
