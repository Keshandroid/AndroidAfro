package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.EHealthAdapterExpand;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.ehealth.EHealth;
import com.TBI.afrocamgist.model.ehealth.EHealthData;
import com.TBI.afrocamgist.model.ehealth.EHealthSubPlans;
import com.TBI.afrocamgist.model.post.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EHealthPlanDetailActivity extends BaseActivity {

    private TextView textBenefits,textViewDesc;
    private TextView subPlanName,subPlanPrice,btnPrice;

    private String planBenefits,planDesc,planPrice,planName,planID;
    private Button bottomPriceBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_health_plan_detail);
        findViews();

        if (getIntent().getStringExtra("planID")!=null){
            planID = (String) getIntent().getStringExtra("planID");
        }

        if (getIntent().getStringExtra("planName")!=null){
            planName = (String) getIntent().getStringExtra("planName");
            subPlanName.setText(planName);
        }


        if (getIntent().getStringExtra("planPrice")!=null){
            planPrice = (String) getIntent().getStringExtra("planPrice");
            subPlanPrice.setText("Price : $ " +planPrice);
        }


        if (getIntent().getStringExtra("planDesc")!=null){
            planDesc = (String) getIntent().getStringExtra("planDesc");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textViewDesc.setText(Html.fromHtml(planDesc, Html.FROM_HTML_MODE_COMPACT));
            } else {
                textViewDesc.setText(Html.fromHtml(planDesc));
            }
        }


        if (getIntent().getStringExtra("planBenefits")!=null){
            planBenefits = (String) getIntent().getStringExtra("planBenefits");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textBenefits.setText(Html.fromHtml(planBenefits, Html.FROM_HTML_MODE_COMPACT));
            } else {
                textBenefits.setText(Html.fromHtml(planBenefits));
            }
        }

        btnPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(EHealthPlanDetailActivity.this, EHealthCustomerDetailActivity.class);
                intent.putExtra("ehealth_plan_id",planID);
                intent.putExtra("ehealth_price",planPrice);
                intent.putExtra("ehealth_plan_name",planName);
                startActivity(intent);

                //_context.startActivity(new Intent(_context, EHealthCustomerDetailActivity.class));

            }
        });

        bottomPriceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EHealthPlanDetailActivity.this, EHealthCustomerDetailActivity.class);
                intent.putExtra("ehealth_plan_id",planID);
                intent.putExtra("ehealth_price",planPrice);
                intent.putExtra("ehealth_plan_name",planName);
                startActivity(intent);
            }
        });


    }

    private void findViews() {
        textBenefits = findViewById(R.id.textBenefits);
        textViewDesc = findViewById(R.id.textViewDesc);
        subPlanName = findViewById(R.id.subPlanName);
        subPlanPrice = findViewById(R.id.subPlanPrice);
        btnPrice = findViewById(R.id.btnPrice);
        bottomPriceBtn = findViewById(R.id.bottomPriceBtn);

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }


}
