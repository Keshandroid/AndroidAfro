package com.TBI.afrocamgist.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends BaseActivity {

    private EditText otp, password;
    private KProgressHUD hud;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        username = getIntent().getStringExtra("username") == null ? "" : getIntent().getStringExtra("username");
        initView();
        setClickListener();

    }

    private void initView() {
        otp = findViewById(R.id.otp);
        password = findViewById(R.id.password);
    }

    private void setClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.reset).setOnClickListener(v -> {

            appendLog("reset password clicked ");

            if ("".equals(otp.getText().toString())) {
                Utils.showAlert(ResetPasswordActivity.this,getString(R.string.please_enter_otp));
            } else if ("".equals(password.getText().toString())) {
                Utils.showAlert(ResetPasswordActivity.this,getString(R.string.please_enter_password));
            } else {
                resetPassword();
            }
        });
    }

    private void resetPassword() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE,"application/json");

        Log.d("resetOTP","REQUEST : "+new GsonBuilder().setPrettyPrinting().create().toJson(createJsonRequest()));

        appendLog("==========Reset password OTP API call=====");
        appendLog("reset password parameters" + new GsonBuilder().setPrettyPrinting().create().toJson(createJsonRequest()));

        Webservices.getData(Webservices.Method.POST,createJsonRequest(),headers, UrlEndpoints.VERIFY_PASSWORD_RESET, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(ResetPasswordActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has("status")) {
                        appendLog("" +object.toString());
                        String message = object.getString(Constants.MESSAGE) + "!!\n" + "Please login again to continue.";
                        showAlert(message);
                    } else {
                        appendLog("" +object.toString());
                        appendLog("======================FINISH======================" );
                        Utils.showAlert(ResetPasswordActivity.this, object.getString(Constants.MESSAGE));
                    }
                    //uploadFile();  // upload forgot password log to the database
                } catch (Exception e) {
                    appendLog("" + e.toString());
                    //uploadFile(); // upload forgot password log to the database
                    e.printStackTrace();
                    Utils.showAlert(ResetPasswordActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private JSONObject createJsonRequest() {

        JSONObject object = new JSONObject();
        try {
            object.put("otp",otp.getText().toString());
            object.put("password",password.getText().toString().trim());
            object.put("username",username);
            return object;
        } catch (Exception e){
            e.printStackTrace();
            return object;
        }
    }

    private void showAlert(String message) {

        final Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dialog_error);
        popup.setCancelable(true);
        popup.show();

        TextView error = popup.findViewById(R.id.message);
        error.setText(message);

        popup.findViewById(R.id.error).setVisibility(View.GONE);
        popup.findViewById(R.id.success).setVisibility(View.VISIBLE);

        popup.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                startActivity(new Intent(ResetPasswordActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });
    }

}
