package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.post.Post;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.ui.PlayerView;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.exoplayer.ExoPlayable;
import im.ene.toro.exoplayer.Playable;
import im.ene.toro.exoplayer.ToroExo;
import im.ene.toro.media.VolumeInfo;

public class EditPostActivity extends BaseActivity {

    private CircleImageView profileImage, profilePicture;
    private EditText postText;
    private TextView name, time, originalPostText, imageCount, originalPostTextOnImage;
    private PlayerView player;
    private ImageView thumbnail, postImage, moreImages, backgroundImage;
    private Post post;
    private Playable playable;
    private ToggleButton volume;
    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);

        if (getIntent().getSerializableExtra("post") != null) {
            post = (Post) getIntent().getSerializableExtra("post");
        }

        initView();
        initClickListener();

        if (post != null)
            setPostData();
    }

    private void initView() {

        moreImages = findViewById(R.id.more_images);
        profilePicture = findViewById(R.id.profile_picture);
        profileImage = findViewById(R.id.profile_image);
        name = findViewById(R.id.name);
        time = findViewById(R.id.time);
        postText = findViewById(R.id.post_text);
        originalPostText = findViewById(R.id.original_post_text);
        postImage = findViewById(R.id.post_image);
        imageCount = findViewById(R.id.image_count);
        thumbnail = findViewById(R.id.thumbnail);
        backgroundImage = findViewById(R.id.background_image);
        originalPostTextOnImage = findViewById(R.id.original_post_text_on_image);
        player = findViewById(R.id.player);
        volume = findViewById(R.id.volume);
    }

    private void initClickListener() {

        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isConnected())
                    editPost();
                else
                    Utils.showAlert(EditPostActivity.this, getString(R.string.no_internet_connection_available));
            }
        });

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });

        volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    unMute();
                else
                    mute();
            }
        });
    }

    //edit post text API call.
    private void editPost() {

        hud = KProgressHUD.create(EditPostActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + post.getPostId();

        Webservices.getData(Webservices.Method.PUT, createJsonRequest(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                hud.dismiss();
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(EditPostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(EditPostActivity.this, getString(R.string.post_edited_successfully), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent();
                        intent.putExtra("postText",postText.getText().toString());
                        setResult(RESULT_OK,intent);
                        finish();
                    }

                } catch (Exception e) {
                    Utils.showAlert(EditPostActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private JSONObject createJsonRequest() {

        JSONObject object = new JSONObject();
        try {
            object.put("post_text", postText.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return object;
    }

    //set post data on view
    private void setPostData() {

        if ("shared".equals(post.getPostType())) {
            findViewById(R.id.other_user_profile_layout).setVisibility(View.VISIBLE);
            setSharedPostData(post.getSharedPost());
            setPostDetails(post.getSharedPost());
        } else {
            findViewById(R.id.other_user_profile_layout).setVisibility(View.GONE);
            if (post.getBackgroundImagePost())
                postText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(50)});
            setPostDetails(post);
        }

        postText.setText(post.getPostText());

        Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                .into(profileImage);
    }

    //set shared post data on view
    private void setSharedPostData(Post post) {

        originalPostText.setText(post.getPostText());
        Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(profilePicture);
    }

    //set post details based on the type of the post
    private void setPostDetails(Post post) {

        String fullName = post.getFirstName() + " " + post.getLastName();
        name.setText(fullName);
        time.setText(Utils.getSocialStyleTime(post.getPostDate()));

        switch (post.getPostType()) {

            case "text":
                findViewById(R.id.layout_only_images).setVisibility(View.GONE);
                findViewById(R.id.video_layout).setVisibility(View.GONE);

                if ("shared".equals(this.post.getPostType()) && post.getBackgroundImagePost()){

                    findViewById(R.id.text_with_image_background).setVisibility(View.VISIBLE);

                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getBackgroundImage())
                            .into(backgroundImage);

                    originalPostTextOnImage.setText(post.getPostText());
                    originalPostText.setText("");
                } else {
                    findViewById(R.id.text_with_image_background).setVisibility(View.GONE);
                }

                break;
            case "image":
                findViewById(R.id.layout_only_images).setVisibility(View.VISIBLE);
                findViewById(R.id.video_layout).setVisibility(View.GONE);

                RequestOptions requestOptions = new RequestOptions();
                requestOptions = requestOptions.transforms(new RoundedCorners(50));

                if (post.getPostImage().size() > 1) {
                    String count = (post.getPostImage().size() - 1) + "";
                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .apply(requestOptions)
                            .into(postImage);

                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(1))
                            .into(moreImages);

                    imageCount.setText(count);
                } else {
                    Glide.with(this)
                            .load(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0))
                            .apply(requestOptions)
                            .into(postImage);
                    findViewById(R.id.more_images_layout).setVisibility(View.GONE);
                }

                Zoomy.Builder builder = new Zoomy.Builder(this)
                        .target(postImage)
                        .enableImmersiveMode(false);

                builder.register();

                break;
            case "video":
                findViewById(R.id.layout_only_images).setVisibility(View.GONE);
                findViewById(R.id.video_layout).setVisibility(View.VISIBLE);
                playVideo(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + post.getPostVideo()));
                Glide.with(this)
                        .load(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail())
                        .into(thumbnail);
                break;
            default:
                break;
        }
    }

    private void playVideo(Uri mediaUri) {

        playable = new ExoPlayable(ToroExo.with(this).getDefaultCreator(), mediaUri, null);
        playable.prepare(true);
        playable.setPlayerView(player);
        mute();
        if (!playable.isPlaying()) {
            playable.play();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    findViewById(R.id.thumbnail_layout).setVisibility(View.GONE);
                }
            }, 1500);
        }
    }

    private void mute() {
        this.playable.setVolumeInfo(new VolumeInfo(true,0));
    }

    private void unMute() {
        this.playable.setVolumeInfo(new VolumeInfo(false,1));
    }

    /*private void setVolume(int amount) {
        final int max = 100;
        final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
        final float volume = (float) (1 - (numerator / Math.log(max)));

        this.playable.setVolumeInfo(new VolumeInfo(true,0));
    }*/

    @Override
    public void onStop() {
        super.onStop();
        if (playable != null) {
            playable.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (playable != null) {
            playable.setPlayerView(null);
            playable.release();
            playable = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (playable != null)
            playable.setPlayerView(null);
    }
}
