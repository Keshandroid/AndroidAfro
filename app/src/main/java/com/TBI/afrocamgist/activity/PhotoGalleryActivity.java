package com.TBI.afrocamgist.activity;

import androidx.viewpager.widget.ViewPager;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.PhotoGalleryAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.photos.Photo;
import com.TBI.afrocamgist.model.photos.PhotoDetails;
import com.TBI.afrocamgist.model.post.Post;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import ooo.oxo.library.widget.PullBackLayout;

public class PhotoGalleryActivity extends BaseActivity implements PullBackLayout.Callback {

    private ViewPager photoList;
    private boolean isPullCanceled = false;
    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_photo_gallery);

        initView();
        setPhotoAdapter();
        setClickListener();
    }

    private void initView() {

        photoList = findViewById(R.id.photo_list);
    }

    private void setPhotoAdapter() {

        int position = getIntent().getIntExtra("position",0);
        ArrayList<Photo> photos = (ArrayList<Photo>) getIntent().getSerializableExtra("photos");

        photoList.setAdapter(new PhotoGalleryAdapter(PhotoGalleryActivity.this,photos));
        photoList.setCurrentItem(position);
    }

    private void setClickListener() {
        findViewById(R.id.close).setOnClickListener(v -> onBackPressed());
        ((PullBackLayout)findViewById(R.id.pull)).setCallback(this);
    }

    @Override
    public void onPullStart() {
        isPullCanceled = false;
    }

    @Override
    public void onPull(float v) {
        /*if(isPullCanceled){
            findViewById(R.id.gallery_layout).setBackgroundColor(Color.parseColor("#ff000000"));
        }else {
            findViewById(R.id.gallery_layout).setBackgroundColor(Color.parseColor("#BC000000"));
        }*/
    }

    @Override
    public void onPullCancel() {
        isPullCanceled = true;
    }

    @Override
    public void onPullComplete() {
        supportFinishAfterTransition();
        overridePendingTransition(0,0);
    }

}
