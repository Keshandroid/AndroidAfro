
package com.TBI.afrocamgist.activity;



import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.R;

import com.TBI.afrocamgist.app.AfrocamgistApplication;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.ehealth.Testimonials;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;

import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.kaopiz.kprogresshud.KProgressHUD;

import im.ene.toro.ToroPlayer;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;


public class ViewTestimonialsVideoActivity extends BaseActivity {

    private KProgressHUD hud;
    private Testimonials testimonials;
   //private ImageView thumbnail_layout;
    private ImageView close;

    //exoplayer
    public SimpleExoPlayer helper;
    private PlayerView playerView;

    //Precache videos
    CacheDataSourceFactory cacheDataSourceFactory = null;
    SimpleCache simpleCache = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_testimonials_video);

        playerView = findViewById(R.id.playerTestimonials);
        //thumbnail_layout = findViewById(R.id.thumbnail);
        close = findViewById(R.id.close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (helper != null) {
                    helper.release();
                    helper = null;
                }
                finish();
            }
        });

        if (getIntent().getSerializableExtra("testimonials") != null) {
            testimonials = (Testimonials) getIntent().getSerializableExtra("testimonials");
        }

        Log.d("testimonials"," intenetData : " + testimonials.getTestimonial_video());

        //pre cache videos
        simpleCache = AfrocamgistApplication.simpleCache;
        String userAgent = Util.getUserAgent(this, "Afrocamgist");
        cacheDataSourceFactory =
                new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(userAgent), CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);


        if (helper == null) {
            helper = newSimpleExoPlayer(this);
        }


        MediaSource mediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + testimonials.getTestimonial_video()));
        playerView.setPlayer(helper);

        helper.seekTo(0);
        helper.prepare(mediaSource, true, false);

        helper.setPlayWhenReady(true);

        playerView.getPlayer().addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                super.onPlayerStateChanged(playWhenReady, playbackState);
                if (playWhenReady && playbackState == Player.STATE_READY) {
                    // media actually playing
                    //thumbnail_layout.setVisibility(View.GONE);
                } else if (playWhenReady) {
                    // might be idle (plays after prepare()),
                    // buffering (plays when data available)
                    // or ended (plays when seek away from end)
                } else {
                    // simpleExoPlayer paused in any state
                    //thumbnail_layout.setVisibility(View.VISIBLE);

                }

                if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED || !playWhenReady) {
                    playerView.setKeepScreenOn(false);
                } else { // STATE_READY, STATE_BUFFERING
                    // This prevents the screen from getting dim/lock
                    playerView.setKeepScreenOn(true);
                }


            }
        });

    }

    private SimpleExoPlayer newSimpleExoPlayer(Context context) {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
        return ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (helper != null) {
            helper.release();
            helper = null;
        }
    }

    /*@NonNull
    @Override
    public View getPlayerView() {
        return playerView;
    }

    @NonNull
    @Override
    public PlaybackInfo getCurrentPlaybackInfo() {
        return new PlaybackInfo();
    }

    @Override
    public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {
        if (helper == null) {
            helper = newSimpleExoPlayer(this);
        }
    }



    @Override
    public void play() {
        if (helper != null) {
            Log.e("LLLLLL_Play: ", String.valueOf(true));



            MediaSource mediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(UrlEndpoints.MEDIA_BASE_URL + testimonials.getTestimonial_video()));
            playerView.setPlayer(helper);

            helper.seekTo(0);
            helper.prepare(mediaSource, true, false);


            if (playerView.getPlayer() != null)
                playerView.getPlayer().setRepeatMode(Player.REPEAT_MODE_ONE);


            playerView.getPlayer().addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    super.onPlayerStateChanged(playWhenReady, playbackState);
                    if (playWhenReady && playbackState == Player.STATE_READY) {
                        // media actually playing
                        //thumbnail_layout.setVisibility(View.GONE);
                    } else if (playWhenReady) {
                        // might be idle (plays after prepare()),
                        // buffering (plays when data available)
                        // or ended (plays when seek away from end)
                    } else {
                        // simpleExoPlayer paused in any state
                        //thumbnail_layout.setVisibility(View.VISIBLE);

                    }

                    if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED || !playWhenReady) {
                        playerView.setKeepScreenOn(false);
                    } else { // STATE_READY, STATE_BUFFERING
                        // This prevents the screen from getting dim/lock
                        playerView.setKeepScreenOn(true);
                    }


                }
            });


            // For Mute and Unmute
                *//*if (isMute) {
                    mute();
                    volume.setChecked(false);
                } else {
                    unMute();
                    volume.setChecked(true);
                }*//*


        } else {
            Log.e("LLLLLL_Play: ", String.valueOf(false));
        }
    }

    @Override
    public void pause() {
        if (helper != null) {

            helper.setPlayWhenReady(false);
        }
    }

    @Override
    public boolean isPlaying() {
        return helper != null && helper.getPlayWhenReady();
    }

    @Override
    public void release() {
        if (helper != null) {
            Log.d("RELEASE9", "called...");
            //thumbnail_layout.setVisibility(View.VISIBLE);
            helper.release();
            helper = null;
        }
    }

    @Override
    public boolean wantsToPlay() {
        return false;
    }

    @Override
    public int getPlayerOrder() {
        return 0;
    }*/
}
