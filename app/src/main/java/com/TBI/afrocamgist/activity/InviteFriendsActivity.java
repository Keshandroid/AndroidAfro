package com.TBI.afrocamgist.activity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.InviteFriendsAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.invitefriends.InviteUserDetails;
import com.TBI.afrocamgist.model.otheruser.OtherUser;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InviteFriendsActivity extends BaseActivity implements InviteFriendsAdapter.OnInviteFriendClickListener{

    private KProgressHUD hud;
    private Group group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        setClickListener();

        if (getIntent().getSerializableExtra("group")!=null)
            group = (Group) getIntent().getSerializableExtra("group");

        if (Utils.isConnected())
            getFriendsListForGroupInvite();
        else
            Utils.showAlert(this,getString(R.string.please_check_your_internet_connection));
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private void getFriendsListForGroupInvite() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        String url = UrlEndpoints.GROUP + "/" + group.getGroupId() + "/invite-friends";

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(InviteFriendsActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(InviteFriendsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        InviteUserDetails userDetails = new Gson().fromJson(response, InviteUserDetails.class);

                        if (userDetails.getUserList()!=null && userDetails.getUserList().size() > 0) {
                            setRecyclerView(userDetails.getUserList());
                        } else {
                            findViewById(R.id.friend_list).setVisibility(View.GONE);
                            findViewById(R.id.no_friends).setVisibility(View.VISIBLE);
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(InviteFriendsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setRecyclerView(ArrayList<OtherUser> users) {

        RecyclerView friends = findViewById(R.id.friend_list);
        friends.setLayoutManager(new LinearLayoutManager(this));
        friends.setAdapter(new InviteFriendsAdapter(InviteFriendsActivity.this,users, this));
    }

    @Override
    public void onInviteFriendClick(OtherUser user) {
        inviteFriend(user.getUserId());
    }

    private void inviteFriend(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.GROUP + "/" + group.getGroupId() + "/invite/" +userId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(InviteFriendsActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(InviteFriendsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }
}
