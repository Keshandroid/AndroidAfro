package com.TBI.afrocamgist.activity;

import android.os.Bundle;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.user.User;
import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.Ack;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SendImageMessageActivity extends BaseActivity implements View.OnClickListener {

    private ImageView image;
    private ArrayList<String> images = new ArrayList<>();
    private User user;
    private EditText message;
    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_image_message);

        if (getIntent().getSerializableExtra("images")!=null)
            images = (ArrayList<String>) getIntent().getSerializableExtra("images");

        if (getIntent().getSerializableExtra("user")!=null)
            user = (User) getIntent().getSerializableExtra("user");

        initView();
        initClickListener();
        setData();
    }

    private void initView() {

        image = findViewById(R.id.image);
        message = findViewById(R.id.message);
    }

    private void initClickListener() {

        findViewById(R.id.cancel).setOnClickListener(this);
        findViewById(R.id.send).setOnClickListener(this);
    }

    private void setData() {

        if (images.size() > 0) {
            Glide.with(this)
                    .load(images.get(0))
                    .into(image);
        }
    }

    private void uploadImageMessage() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        File photo = new File(images.get(0));

        builder.addFormDataPart(Constants.MEDIA, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE,"multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.MESSAGES + "/upload";

        Webservices.getData(this, Webservices.Method.POST, builder, headers, url, response -> {

            Logger.d("uploadImage","111");


            if ("".equals(response)) {
                Logger.d("uploadImage","222");
                hud.dismiss();
                Utils.showAlert(SendImageMessageActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {

                    Logger.d("uploadImage","333"+new GsonBuilder().setPrettyPrinting().create().toJson(response));

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        hud.dismiss();
                        photo.delete();
                        Utils.showAlert(SendImageMessageActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        sendImageMessage(object.getJSONObject("data").getString("path"));
                        //socketSendImageMessage(object.getJSONObject("data").getString("path"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    hud.dismiss();
                    Utils.showAlert(SendImageMessageActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });

    }

    private void socketSendImageMessage(String imagePath){
        JSONObject data = new JSONObject();
        try {

            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", user.getUserId());
            data.put("message", message.getText().toString().trim());
            data.put("message_image",imagePath);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.SendMessage, data, new Ack() {
                @Override
                public void call(Object... args) {
                    hud.dismiss();

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SocketEvent","send-image with Ack()");
                            Log.d("SocketEvent","data : "+ args.length);
                            Log.d("SocketEvent","data : "+ args[1]);
                            message.setText("");
                            finish();
                        }
                    });
                }
            });
        }
    }

    private void sendImageMessage(String image) {

        JSONObject request = new JSONObject();
        try {
            request.put("message_text", message.getText().toString().trim());
            request.put("message_image", image);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.CONVERSATION + user.getUserId();

        Webservices.getData(Webservices.Method.POST, request, headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(SendImageMessageActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SendImageMessageActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        message.setText("");
                        finish();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SendImageMessageActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.cancel:
                finish();
                break;
            case R.id.send:
                if (Utils.isConnected()) {
                    uploadImageMessage();
                }
                break;
            default:
                break;
        }
    }
}
