package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.FindGroupAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.groups.GroupDetails;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.connection.Connectivity;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FindGroupActivity extends BaseActivity implements FindGroupAdapter.OnGroupClickListener, ConnectivityListener {

    private String query = "";
    private EditText search;
    private SwipeRefreshLayout swipeContainer;
    private Connectivity mConnectivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_group);
        initView();
        initSearchTextChangeListener();
        initSwipeRefresh();
        setClickListener();
        initInternetConnectivityCheckListener();

    }

    private void initView() {

        search = findViewById(R.id.search);
        swipeContainer = findViewById(R.id.swipeContainer);
    }

    private void initSearchTextChangeListener() {

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                //fetch group list based on the search input
                query = s.toString();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getGroups();
                    }
                },1000);
            }
        });
    }

    private void initSwipeRefresh() {
        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getGroups();
            else
                Utils.showAlert(this,getString(R.string.please_check_your_internet_connection));
        });
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            Utils.closeAlert();
            getGroups();
        } else {
            Utils.showAlert(FindGroupActivity.this, getString(R.string.no_internet_connection_available));
        }
    }

    //get list of all group API
    private void getGroups() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.FIND_GROUPS + query;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                swipeContainer.setRefreshing(false);
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FindGroupActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        GroupDetails groupDetails = new Gson().fromJson(response, GroupDetails.class);
                        if (groupDetails.getGroups()!=null) {
                            setGroupList(groupDetails.getGroups());
                        }
                    }

                } catch (Exception e){
                    e.printStackTrace();
                    Utils.showAlert(FindGroupActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    //set view of list of groups based on the API response
    private void setGroupList(ArrayList<Group> groups) {

        RecyclerView groupList = findViewById(R.id.group_list);
        groupList.setLayoutManager(new LinearLayoutManager(this));
        groupList.setAdapter(new FindGroupAdapter(groups,this));
    }

    @Override
    public void onGroupClick(Group group) {
        startActivity(new Intent(FindGroupActivity.this, AfroViewGroupPostActivity.class).putExtra("group",group));
    }

    @Override
    public void onGroupJoinOrLeaveToggleClick(Group group, boolean isJoining) {

        if (isJoining)
            joinGroup(group.getGroupId());
        else
            leaveGroup(group.getGroupId());
    }

    //join group API call
    private void joinGroup(Integer groupId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.JOIN_GROUP + groupId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FindGroupActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e){
                    e.printStackTrace();
                    Utils.showAlert(FindGroupActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    //leave group API call
    private void leaveGroup(Integer groupId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.LEAVE_GROUP + groupId;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FindGroupActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e){
                    e.printStackTrace();
                    Utils.showAlert(FindGroupActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
    }
}
