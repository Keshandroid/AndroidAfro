package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;

public class EHealthCustomerDetailActivity extends BaseActivity {

    private Button btnNext;
    private EditText firstName,lastName,edtEmail,address;
    private CheckBox checkBoxConfirm;

    private String eHealthPrice="",eHealthPlanID="",eHealthPlanName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_health_customer_detail);

        //intent values
        if(getIntent().getStringExtra("ehealth_plan_id") != null){
            eHealthPlanID = getIntent().getStringExtra("ehealth_plan_id");
        }

        if(getIntent().getStringExtra("ehealth_price") != null){
            eHealthPrice = getIntent().getStringExtra("ehealth_price");
        }

        if(getIntent().getStringExtra("ehealth_plan_name") != null){
            eHealthPlanName = getIntent().getStringExtra("ehealth_plan_name");
        }



        btnNext = findViewById(R.id.btnNext);
        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        edtEmail = findViewById(R.id.edtEmail);
        address = findViewById(R.id.address);
        checkBoxConfirm = findViewById(R.id.checkBoxConfirm);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateData();
            }
        });


        firstName.setText(LocalStorage.getUserDetails().getFirstName());
        lastName.setText(LocalStorage.getUserDetails().getLastName());
        edtEmail.setText(LocalStorage.getUserDetails().getEmail());


        checkBoxConfirm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    firstName.setText("");
                    lastName.setText("");
                    edtEmail.setText("");

                }else {
                    firstName.setText(LocalStorage.getUserDetails().getFirstName());
                    lastName.setText(LocalStorage.getUserDetails().getLastName());
                    edtEmail.setText(LocalStorage.getUserDetails().getEmail());
                }
            }
        });



    }

    private void validateData() {

        if(firstName.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(this,"First Name is empty",Toast.LENGTH_SHORT).show();
            return;
        }
        if(lastName.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(this,"Last Name is empty",Toast.LENGTH_SHORT).show();
            return;
        }
        if(edtEmail.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(this,"Email is empty",Toast.LENGTH_SHORT).show();
            return;
        }



        //all values are validated

        if(eHealthPlanID!=null && !eHealthPlanID.equals("") &&
                eHealthPrice!=null && !eHealthPrice.equals("") &&
                eHealthPlanName!=null && !eHealthPlanName.equals("")){

            Intent intent = new Intent(EHealthCustomerDetailActivity.this, FlutterWavePaymentActivity.class);
            intent.putExtra("ehealth_plan_id",eHealthPlanID);
            intent.putExtra("ehealth_price",eHealthPrice);
            intent.putExtra("ehealth_plan_name",eHealthPlanName);

            intent.putExtra("ehealth_first_name",firstName.getText().toString());
            intent.putExtra("ehealth_last_name",lastName.getText().toString());
            intent.putExtra("ehealth_email",edtEmail.getText().toString());
            startActivity(intent);
        }





        //startActivity(new Intent(EHealthCustomerDetailActivity.this, FlutterWavePaymentActivity.class));


    }


}
