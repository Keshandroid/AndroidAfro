package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.TagUser.TagToBeTagged;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.HashtagPostAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.bottomsheet.HashtagCommentBottomSheetFragment;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.SwaggerCommentBottomSheetFragment;
import com.TBI.afrocamgist.fragment.TagBottomSheetFragment;
import com.TBI.afrocamgist.home.VideosAdapter;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.hashtag.TrendingHashtagDetails;
import com.TBI.afrocamgist.model.media.Media;
import com.TBI.afrocamgist.model.media.MediaDetails;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import im.ene.toro.widget.Container;
import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnMultiCompressListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.TBI.afrocamgist.activity.CreatePostActivity.location1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postText1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.postType1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.tagUserList1;
import static com.TBI.afrocamgist.activity.CreatePostActivity.video1;

public class HashtagPostsActivity extends BaseActivity implements HashtagPostAdapter.OnPostClickListener, ConnectivityListener,
        HashtagCommentBottomSheetFragment.DialogDismissListener{

    private KProgressHUD hud;
    private TextView hashtag, followers;
    private Connectivity mConnectivity;
    private ToggleButton follow;
    private Container hashtagPosts;
    private SwipeRefreshLayout swipeContainer;
    private int nextPage = 1;
    private boolean isLoading = false;
    private HashtagPostAdapter hashtagPostAdapter;
    private ImageView scrollToTop;
    private ArrayList<Post> posts = new ArrayList<>();
    private Hashtag hash;
    private static int SAVED_SCROLL_POSITION = -1;
    public static String NOTIFYSTATE="notifyhastag";
    public static String TAG_Id="tag_id";
    public static String Follow_Count="follow_count";
    public static String Follow_State="follow_state";
    private File videoFile;
    private ArrayList<File> imageFiles = new ArrayList<>();
    private ArrayList<File> compressedImageFiles = new ArrayList<>();
    //private ArrayList<TagToBeTagged> tagUserList = new ArrayList<>();

    private List<String> tagUserList = new ArrayList<>();

    private String postText = "", location = "";
    protected LocalBroadcastManager localBroadcastManager;
    private int postid = -1;
    private boolean isFollow ;

    //Download Video from url
    String fileN = null ;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123;
    boolean result;
    ProgressDialog mProgressDialog;
    private String waterMarkVideo;
    private Dialog popup;
    private String shareOrDownload = "";

    @Override
    public void onResume() {
        super.onResume();
        /*if(hash!=null && !TextUtils.isEmpty(hash.getId())){
            getFirstPageHashtagPosts();
        }*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hashtag_posts);
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(mFollowStateReceiver,
                new IntentFilter(ProfileActivity.NITIFY_FOLLOW_STATE));
        initView();
        setCustomTitle();
        initSwipeRefresh();
        initInternetConnectivityCheckListener();
        initClickListener();

        /*if(hash!=null && !TextUtils.isEmpty(hash.getId())){
            getFirstPageHashtagPosts();
        }*/

//        if (Utils.isConnected())
////            getFirstPageHashtagPosts();
//        else
//            Utils.showAlert(this, "Please check your internet connection");

    }

    private void initView() {
        hashtagPosts = findViewById(R.id.hashtag_posts);
        hashtag = findViewById(R.id.hashtag);
        followers = findViewById(R.id.followers);
        follow = findViewById(R.id.follow);
        scrollToTop = findViewById(R.id.scroll_to_top);
        swipeContainer = findViewById(R.id.swipeContainer);
    }

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud!=null && hud.isShowing()){
                hud.dismiss();
            }
            Utils.closeAlert();
            if(hash!=null && !TextUtils.isEmpty(hash.getId())){
                getHashtagDetails();
                getFirstPageHashtagPosts();
            }
        } else {
            //Utils.showAlert(HashtagPostsActivity.this, "No internet connection available.");
        }
    }

    private void setCustomTitle() {
        hash = (Hashtag) getIntent().getSerializableExtra("hashtag");
        if(hash!=null && TextUtils.isEmpty(hash.getId())){
            if (Utils.isConnected()) {
                Logger.d("hash111","111");
                getHashtagDetails();
                //getFirstPageHashtagPosts();
            } else
                Utils.showAlert(this, getString(R.string.please_check_your_internet_connection));
        }else {
            if (hash != null) {
                isFollow = hash.getFollowedByMe();

                String hashtagSlug = "#" + getHashtag(hash.getHashtagSlug());

                Logger.d("hashtag2",hashtagSlug);

                //String hashtagSlug = "#" + hash.getHashtagSlug();
                hashtag.setText(hashtagSlug);
                ((TextView) findViewById(R.id.title)).setText(hashtagSlug);

                String followerCount = hash.getFollowers().size() + " followers";
                followers.setText(followerCount);

                if (hash.getFollowedByMe())
                    follow.setChecked(true);
                else
                    follow.setChecked(false);

                follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            followHashtag(hash);
                        } else {
                            unFollowHashtag(hash);
                        }
                    }
                });
            }
        }

    }

    private void initSwipeRefresh() {
        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected()) {
                getHashtagDetails();
                getFirstPageHashtagPosts();
            } else
                Utils.showAlert(this, getString(R.string.please_check_your_internet_connection));
        });
    }

    private void initClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
        scrollToTop.setOnClickListener(v -> {
            if (hashtagPosts.getLayoutManager() != null) {

                LinearLayoutManager layoutManager = (LinearLayoutManager) hashtagPosts.getLayoutManager();
                if (layoutManager.findFirstVisibleItemPosition() < 15)
                    hashtagPosts.getLayoutManager().smoothScrollToPosition(hashtagPosts, new RecyclerView.State(), 0);
                else
                    hashtagPosts.getLayoutManager().scrollToPosition(0);
            }
        });
    }

    private void getHashtagDetails() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/json");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.HASHTAGS + "/" + getHashtag(hash.getHashtagSlug());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.closeAlert();
                        Utils.showAlertWithFinish(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        if(TextUtils.isEmpty(hash.getId())){
                            getFirstPageHashtagPosts();
                        }
                        hash = new Gson().fromJson(object.toString(), Hashtag.class);
                        setHashtagDetails(hash);
                        notifyState();
                    }

                } catch (Exception e) {
                    Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void setHashtagDetails(Hashtag hash) {
        if (hashtagPostAdapter != null)
            hashtagPostAdapter.setHashtagDetails(hash);
    }

    private void getFirstPageHashtagPosts() {

        if (hash != null) {
            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();

            String url = UrlEndpoints.HASHTAG + "/" + getHashtag(hash.getHashtagSlug()) + "?page=1";

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
                swipeContainer.setRefreshing(false);
                hud.dismiss();
                getTrendingHashtags();
                if ("".equals(response)) {
                    Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                } else {
                    try {
                        JSONObject object = new JSONObject(response);

                        Logger.d("HashFirstPage", new GsonBuilder().setPrettyPrinting().create().toJson(object));

                        posts = new ArrayList<>();
                        posts.add(0, new Post());
                        posts.add(1, new Post());
                        if (object.has(Constants.MESSAGE)) {
                            Utils.closeAlert();
                            Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            PostDetails hashtagPostDetails = new Gson().fromJson(response, PostDetails.class);
                            nextPage = hashtagPostDetails.getNextPage();
                            if (hashtagPostDetails.getPosts().size() > 0) {
                                posts.addAll(hashtagPostDetails.getPosts());
                                posts.add(3, new Post());
                            }
                        }
                        setPostsRecyclerView();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            });
        }
    }

    private void setPostsRecyclerView() {

        hashtagPosts.setLayoutManager(new LinearLayoutManager(this));
        hashtagPostAdapter = new HashtagPostAdapter(this, posts, hash, this);
        hashtagPosts.setAdapter(hashtagPostAdapter);
        hashtagPosts.setNestedScrollingEnabled(false);
        if(postid != -1){
            int id =getPosition(String.valueOf(postid));
            if(id != -1){
                hashtagPosts.getLayoutManager().scrollToPosition(id);
            }
                postid = -1;
        }
        hashtagPosts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = 0, totalItemCount = 0, pastVisibleItem = 0;

                if (recyclerView.getLayoutManager() != null) {
                    visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    pastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }

                if (pastVisibleItem > 2) {
                    scrollToTop.setVisibility(View.VISIBLE);
                } else {
                    scrollToTop.setVisibility(View.GONE);
                }

                if (!isLoading && dy > 0 && ((visibleItemCount + pastVisibleItem) >= totalItemCount)) {
                    isLoading = true;
                    getHashtagNextPagePosts();

                }
            }
        });
    }

    private void getHashtagNextPagePosts() {

        String url = UrlEndpoints.HASHTAG + "/" + getHashtag(hash.getHashtagSlug()) + "?page=" + nextPage;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if (!"".equals(response)) {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails hashtagPostDetails = new Gson().fromJson(response, PostDetails.class);

//                        posts.remove(posts.size() - 1);
//                        hashtagPostAdapter.notifyItemRemoved(posts.size());

                        if (hashtagPostDetails.getPosts() != null && hashtagPostDetails.getPosts().size() > 0) {
                            nextPage = hashtagPostDetails.getNextPage();
                            posts.addAll(hashtagPostDetails.getPosts());
                            hashtagPostAdapter.notifyDataSetChanged();
                        }
                        isLoading = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void getTrendingHashtags() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.TRENDING_HASHTAGS, response -> {
            if (!"".equals(response)) {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        TrendingHashtagDetails trendingHashtagDetails = new Gson().fromJson(response, TrendingHashtagDetails.class);

                        if (hashtagPostAdapter != null && trendingHashtagDetails.getTrendingHashtags() != null
                                && trendingHashtagDetails.getTrendingHashtags().size() > 0) {
                            hashtagPostAdapter.setTrendingHashtag(trendingHashtagDetails.getTrendingHashtags());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    public void onHashtagFollow(boolean isFollowed) {
        hash.setFollowedByMe(isFollowed);
        isFollow = isFollowed;
        if (isFollowed) {
            followHashtag(hash);
            notifyState();
        }else {
            unFollowHashtag(hash);
            notifyState();
        }
    }

    private void followHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.FOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                getHashtagDetails();
            }
        });

    }

    private void unFollowHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.UNFOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                getHashtagDetails();
            }
        });

    }

    @Override
    public void onDeletePost(Post post) {
        showConfirmDeletePostAlert(post);
    }

    private void showConfirmDeletePostAlert(Post post) {

        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    deletePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void deletePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(HashtagPostsActivity.this, getString(R.string.post_deleted_successfully), Toast.LENGTH_LONG).show();
                        getHashtagDetails();
                        getFirstPageHashtagPosts();
                    }

                } catch (Exception e) {
                    Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onPostChecked(Post post) {
        likeAndUnlikePost(post.getPostId());
    }

    private void likeAndUnlikePost(Integer postId) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id", postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private BroadcastReceiver mFollowStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int user_id = intent.getIntExtra(ProfileActivity.User_Id,0);
            boolean isfollow = intent.getBooleanExtra(ProfileActivity.IsFollow,false);
            setFollowAllUser(user_id,isfollow);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
        localBroadcastManager.unregisterReceiver(mFollowStateReceiver);
    }

    @Override
    public void onFollowClicked(Post post) {
        followUser(post.getUserId());
        setFollowAllUser(post.getUserId(),true);
    }

    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
            }
        });
    }

    private void setFollowAllUser(int userid, boolean isfollow){
        try {
            for (int i = 0; i< posts.size();i++){
                if(posts.get(i).getUserId() !=null && userid == posts.get(i).getUserId()){
                    if(posts.get(i).getPrivateAccount()){
                        if(isfollow){
                            posts.get(i).getRequestButtons().get(0).setButtonText("Requested");
                            posts.get(i).setFollowing(false);
                        }else {
                            posts.get(i).getRequestButtons().get(0).setButtonText("Request");
                            posts.get(i).setFollowing(false);
                        }
                    }else {
                        posts.get(i).setFollowing(isfollow);
                    }
                    hashtagPostAdapter.notifyItemChanged(i);
                }
            }
            hashtagPostAdapter.notifyDataSetChanged();

        }catch (Exception e){
        }

    }

    private void updatePostText(String postText) {
        if (posts.size() > 0) {
            posts.get(SAVED_SCROLL_POSITION).setPostText(postText);

            if (hashtagPostAdapter != null) {
                hashtagPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }

        }
    }

    @Override
    public void onVideoCount(int videoPostId) {
        Logger.e("VIDEO_POST_ID1", "111"+videoPostId);
        callVideoCountNew(videoPostId);
    }

    @Override
    public void onPostViewCount(int postId) {
        callPostViewCount(postId);
    }

    private void callPostViewCount(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getPostViewCount(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onTagClicked(ArrayList<String> taggedIds) {
        TagBottomSheetFragment.newInstance(taggedIds).show(getSupportFragmentManager(), "ModalBottomSheet");
    }

    private void callVideoCountNew(int videoPostId){
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getVideoCount(videoPostId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("ViewCount",""+jsonObject.get("counter"));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onEditPost(Post post, int position) {

        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(this, EditPostActivity.class)
                .putExtra("post", post), 102);
    }

    @Override
    public void onCommentClicked(Post post, int position, HashtagPostAdapter.RecyclerObjectHolder holder) {

        /*SAVED_SCROLL_POSITION = position;
        startActivityForResult(new Intent(this, CommentsActivity.class)
                .putExtra("comments", post.getComments())
                .putExtra("postId", post.getPostId()), 101);*/

        SAVED_SCROLL_POSITION = position;
        HashtagCommentBottomSheetFragment hashtagCommentBottomSheetFragment = new HashtagCommentBottomSheetFragment();
        HashtagCommentBottomSheetFragment.newInstance(holder, post.getComments(),post.getPostId(),HashtagPostsActivity.this).show(getSupportFragmentManager(), hashtagCommentBottomSheetFragment.getTag());

    }

    @Override
    public void onCreatePostClicked() {
        startActivityForResult(new Intent(HashtagPostsActivity.this, CreatePostActivity.class)
                .putExtra("postFor", "hashtag")
                .putExtra("hashtag", hash.getHashtagSlug()), 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (video1 != null && !video1.equals("") && data == null) {
            if (posts != null) {
                posts.add(2, null);
                hashtagPostAdapter.notifyDataSetChanged();
            }
            postText = postText1;
            location = location1;
            tagUserList = tagUserList1;
            createPost(null);
        }

        if (requestCode == 100 && resultCode == RESULT_OK) {
            if (data != null && data.getSerializableExtra("postType") != null) {
                if (posts != null) {
                    posts.add(2, null);
                    hashtagPostAdapter.notifyDataSetChanged();
                }
                postText = data.getStringExtra("postText");
                location = data.getStringExtra("location");
                tagUserList = (ArrayList<String>) data.getSerializableExtra("tagUserList");
                createPost(data);
            } else if (video1 != null && !video1.equals("") && data == null) {
                if (posts != null) {
                    posts.add(2, null);
                    hashtagPostAdapter.notifyDataSetChanged();
                }
                postText = postText1;
                location = location1;
                createPost(null);
            } else {
                if (Utils.isConnected()) {
                    getHashtagDetails();
                    getFirstPageHashtagPosts();
                }
            }
        } else if (requestCode == 101 && resultCode == RESULT_OK) {
            /*if (data != null && data.getSerializableExtra("comments") != null) {
                updateCommentCount((ArrayList<Comment>) data.getSerializableExtra("comments"));
            }*/
        } else if (requestCode == 102 && resultCode == RESULT_OK) {

            if (data != null && data.getStringExtra("postText") != null) {
                updatePostText(data.getStringExtra("postText"));
            }
        }
    }

    private void createPost(Intent data) {
        if (data != null) {
            CreatePostActivity.PostType postType = (CreatePostActivity.PostType) data.getSerializableExtra("postType");

            switch (postType) {

                case IMAGE:
                    createImagePost(data);
                    break;
                case VIDEO:
                    createVideoPost(data);
                    break;
                default:
                    break;
            }
        } else {
            if (postType1 == CreatePostActivity.PostType.VIDEO) {
                createVideoPost((Intent) null);
            }
        }
    }

    private void createImagePost(Intent data) {
        createImageFile(data.getStringArrayListExtra("images"));
//        uploadImages();
    }

    private void createImageFile(ArrayList<String> imagePaths) {
        imageFiles = new ArrayList<>();
        for (int i = 0; i < imagePaths.size(); i++) {

            try {
                imageFiles.add(new File(imagePaths.get(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Luban.compress(this, imageFiles)
                .putGear(Luban.CUSTOM_GEAR)
                .launch(new OnMultiCompressListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(List<File> fileList) {
                        compressedImageFiles = new ArrayList<>(fileList);
                        /*for (File image : imageFiles)
                            image.delete();*/
                        uploadImages();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void uploadImages() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (File photo : compressedImageFiles) {
            builder.addFormDataPart(Constants.IMAGES, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload";

        Webservices.getData(HashtagPostsActivity.this, Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {
                            for (File image : compressedImageFiles)
                                image.delete();
                            createImagePost(mediaDetails.getMediaList());

                        } else {
                            Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int) (100 * bytesRead / contentLength);
            Logger.d("Percentage", percentage + "");
            if (hashtagPostAdapter != null) {
                hashtagPostAdapter.setUploadProgress(percentage);
                runOnUiThread(() -> hashtagPostAdapter.notifyItemChanged(1));
            }
        });
    }

    private void createImagePost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createImagePostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(HashtagPostsActivity.this, getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    if (posts != null) {
                        posts.remove(2);
                        hashtagPostAdapter.notifyDataSetChanged();
                    }
                    getHashtagDetails();
                    getFirstPageHashtagPosts();
                }

            } catch (Exception e) {
                Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private JSONObject createImagePostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            String post = postText + " #" + getHashtag(hash.getHashtagSlug());
            //String post = postText + " #" + hash.getHashtagSlug();
            request.put("post_text", post);
            request.put("post_lat_long", location);
            request.put("posted_for", "hashtag");

            JSONArray images = new JSONArray();
            for (Media media : mediaList) {
                images.put(media.getPath());
            }

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            request.put("post_type", "image");
            request.put("post_image", images);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void createVideoPost(Intent data) {
        if (data == null) {
            createVideoFile(video1);
        } else {
            createVideoFile(data.getStringExtra("video"));
        }
        uploadVideo();
    }

    private void createVideoFile(String videoPath) {

        try {
            videoFile = new File(videoPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadVideo() {

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(Constants.MEDIA, videoFile.getName(), RequestBody.create(MediaType.parse("video/*"), videoFile));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload-video";

        Webservices.getData(HashtagPostsActivity.this, Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {
                            createVideoPost(mediaDetails.getMediaList());
                        } else {
                            Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        }, (bytesRead, contentLength) -> {
            int percentage = (int) (100 * bytesRead / contentLength);
            Logger.d("Percentage", percentage + "");
            if (hashtagPostAdapter != null) {
                hashtagPostAdapter.setUploadProgress(percentage);
                runOnUiThread(() -> hashtagPostAdapter.notifyItemChanged(2));
            }
        });
    }

    private void createVideoPost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createVideoPostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(HashtagPostsActivity.this, getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    /*if (posts != null) {
                        posts.remove(2);
                        hashtagPostAdapter.notifyDataSetChanged();
                    }*/
                    postText1 = "";
                    location1 = "";
                    video1 = "";
                    tagUserList1.clear();
                    getHashtagDetails();
                    getFirstPageHashtagPosts();
                }

            } catch (Exception e) {
                Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private JSONObject createVideoPostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            if(tagUserList!=null && !tagUserList.isEmpty()){
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest",taggedUsers.toString()+"");
                request.put("tagged_id",taggedUsers);
            }

            String post = postText + " #" + getHashtag(hash.getHashtagSlug());
            //String post = postText + " #" + hash.getHashtagSlug();
            request.put("post_text", post);
            request.put("post_lat_long", location);
            request.put("posted_for", "hashtag");
            request.put("post_type", "video");
            request.put("post_video", mediaList.get(0).getPath());
            request.put("thumbnail", mediaList.get(0).getThumbnails().get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void updateCommentCount(ArrayList<Comment> comments,HashtagPostAdapter.RecyclerObjectHolder holder) {
        if (posts.size() > 0) {
            int commentCount = 0;
            for (Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments() != null && comment.getSubComments().size() > 0) {
                    commentCount += comment.getSubComments().size();
                }
            }
            posts.get(SAVED_SCROLL_POSITION).setComments(comments);
            posts.get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);
            postid = posts.get(SAVED_SCROLL_POSITION).getPostId();

            /*if (hashtagPostAdapter != null) {
                hashtagPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }*/

            if(hashtagPostAdapter!=null){
                hashtagPostAdapter.onCommentDialogDismiss(SAVED_SCROLL_POSITION,commentCount,holder);
            }
        }
    }

    protected void notifyState(){
        Intent intent = new Intent(NOTIFYSTATE);
        intent.putExtra(TAG_Id, hash.getId());
        intent.putExtra(Follow_Count, hash.getFollowers());
        intent.putExtra(Follow_State, isFollow);
        localBroadcastManager.sendBroadcast(intent);

    }

    private String getHashtag(String hashtag){

        Logger.d("hashtag3",""+hashtag + " Length : "+hashtag.length());

        String hashtagSlug = hashtag;
        if(hashtagSlug.contains("#")){
            Logger.d("hashtag4",""+hashtagSlug.substring(hashtagSlug.lastIndexOf("#") + 1));
            hashtagSlug = hashtagSlug.substring(hashtagSlug.lastIndexOf("#") + 1);
            return hashtagSlug.toLowerCase();
        }else {
            hashtagSlug = hashtag.toLowerCase();
            Logger.d("hashtag5",""+hashtagSlug);
        }
        return hashtagSlug;
    }

    private int getPosition(String tagid){
        int position=-1;
        for (int i = 0; i< posts.size();i++){
            if(posts.get(i) !=null){
                if(tagid.equals(String.valueOf(posts.get(i).getPostId()))){
                    return i;
                }
            }

        }
        return position;
    }




    @Override
    public void onDownloadClick(String videoUrl, String postLink) {
        Logger.d("vidUrl", ""+videoUrl);
        showShareSelectionDialog(HashtagPostsActivity.this,videoUrl,postLink);
    }


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onDialogDismiss(ArrayList<Comment> comments, HashtagPostAdapter.RecyclerObjectHolder holder) {
        if(comments!=null){
            updateCommentCount(comments,holder);
        }
    }

    // DownloadTask for downloding video from URL
    public class DownloadTask extends AsyncTask<String, Integer, String> {
        private Context context;
        private PowerManager.WakeLock mWakeLock;
        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                java.net.URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                input = connection.getInputStream();


                String filePath = sUrl[0];
                fileN = filePath.substring(filePath.lastIndexOf("/")+1);
                //fileN = "Afrocamgist_" + UUID.randomUUID().toString().substring(0, 10) + ".mp4";
                File filename = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/", fileN);
                output = new FileOutputStream(filename);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();

            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);

            mProgressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();

            if (result != null){
                Toast.makeText(context, getString(R.string.str_download_err) + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, getString(R.string.str_video_downloaded), Toast.LENGTH_SHORT).show();
                deleteWatermarkVideoInServer();
            }

            MediaScannerConnection.scanFile(HashtagPostsActivity.this,
                    new String[]{Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/AfrocamgistVideos/" + fileN}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String newpath, Uri newuri) {
                            Logger.i("ExternalStorage", "Scanned " + newpath + ":");
                            Logger.i("ExternalStorage", "-> uri=" + newuri);
                        }
                    });

            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + fileN;
            shareVideoToSocialMedia(filePath);
        }
    }

    //Get watermarked video from url
    private void callWatermarkVideoAPI(String videoUrl){
        waterMarkVideo = "";
        hud = KProgressHUD.create(HashtagPostsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setLabel("Preparing...")
                .setCancellable(false)
                .show();

        JSONObject params = new JSONObject();
        try {
            params.put("video", videoUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.WATERMARK_VIDEO, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        if(object.getBoolean("result")){
                            waterMarkVideo = object.getString("video_path");
                            String watermarkVideo = UrlEndpoints.MEDIA_BASE_URL + object.getString("video_path");
                            newDownload(watermarkVideo);
                        }else {
                            Utils.showAlert(HashtagPostsActivity.this, getString(R.string.opps_something_went_wrong));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //delete watermark video from server
    private void deleteWatermarkVideoInServer(){
        JSONObject params = new JSONObject();
        try {
            params.put("video", waterMarkVideo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.DELETE_WATERMARK_VIDEO;

        Webservices.getData(Webservices.Method.DELETE, params, headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.d("waterMark99","success");
                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(HashtagPostsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showShareSelectionDialog(Context context, String videoUrl, String postLink) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_social_share);
            popup.setCancelable(true);
            popup.show();

            RelativeLayout download = popup.findViewById(R.id.rlDownload);
            RelativeLayout share = popup.findViewById(R.id.rlShare);


            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "download";
                    downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "share";
                    shareVideoLink(postLink);
                    //downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

        }
    }

    private void downloadVideo(String videoUrl){
        String originalFilename = videoUrl.substring(videoUrl.lastIndexOf("/")+1);
        result = checkPermission();
        if(result){
            checkFolder();
            if (!isConnectingToInternet(HashtagPostsActivity.this)) {
                Toast.makeText(HashtagPostsActivity.this, getString(R.string.please_check_your_internet_connection), Toast.LENGTH_LONG).show();
                return;
            }

            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename);
            if(file.exists()){
                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename;
                shareVideoToSocialMedia(filePath);
            }else {
                callWatermarkVideoAPI(videoUrl);
            }
        }
    }

    private void shareVideoLink(String videoUrl){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
        i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
        i.putExtra(Intent.EXTRA_TEXT, videoUrl);
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    private void shareVideoToSocialMedia(String filePath){

        MediaScannerConnection.scanFile(HashtagPostsActivity.this, new String[] { filePath },

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                        shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, "Afrocamgist");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);


                        startActivity(Intent.createChooser(shareIntent,
                                "Afrocamgist"));

                    }
                });
    }

    //hare you can start downloding video
    public void newDownload(String url) {
        final DownloadTask downloadTask = new DownloadTask(HashtagPostsActivity.this);
        downloadTask.execute(url);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(HashtagPostsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(HashtagPostsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(HashtagPostsActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(HashtagPostsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(HashtagPostsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public void checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(HashtagPostsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(HashtagPostsActivity.this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(HashtagPostsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(HashtagPostsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }

    //Here you can check App Permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkFolder();
                } else {
                    //code for deny
                    checkAgain();
                }
                break;
        }
    }

    //hare you can check folfer whare you want to store download Video
    public void checkFolder() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/";
        File dir = new File(path);
        boolean isDirectoryCreated = dir.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated = dir.mkdir();
        }
        if (isDirectoryCreated) {
            // do something
            Logger.d("Folder", "Already Created");
        }
    }

}
