package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import androidx.appcompat.widget.AppCompatSpinner;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.SpinnerAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.groups.Group;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CreateGroupActivity extends BaseActivity implements View.OnClickListener {

    private AppCompatSpinner topic;
    private EditText groupName, description;
    private RadioButton privateGroup;
    private KProgressHUD hud;
    private ArrayList<String> topicList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
//                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.activity_create_group);

        initView();
        initSpinnerAdapter();
        initClickListener();

    }

    private void initView() {
        groupName = findViewById(R.id.group_name);
        description = findViewById(R.id.description);
        topic = findViewById(R.id.topic);
        privateGroup = findViewById(R.id.pri);
    }

    //initialise topic drop down view
    private void initSpinnerAdapter() {

        topicList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.topic)));
        topic.setAdapter(new SpinnerAdapter(this, topicList));
    }

    private void initClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
        findViewById(R.id.create_group).setOnClickListener(this);
        findViewById(R.id.invite_friends).setOnClickListener(this);
    }

    //validate user input
    private Boolean isValidInput() {

        if (groupName.getText().toString().isEmpty()) {
            Utils.showAlert(CreateGroupActivity.this, getString(R.string.enter_group_name));
            return false;
        } else if (description.getText().toString().isEmpty()) {
            Utils.showAlert(CreateGroupActivity.this, getString(R.string.enter_group_details));
            return false;
        } else {
            return true;
        }
    }

    //create group API
    private void createGroup() {
        hud = KProgressHUD.create(CreateGroupActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/json");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createGroupJsonRequest(), headers, UrlEndpoints.GROUP, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                hud.dismiss();
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(CreateGroupActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        Group group = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), Group.class);
                        startActivity(new Intent(CreateGroupActivity.this, AfroViewGroupPostActivity.class)
                                .putExtra("createGroup","createGroup")
                                .putExtra("group",group));
                        finish();
                    }

                } catch (Exception e) {
                    Utils.showAlert(CreateGroupActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private JSONObject createGroupJsonRequest() {

        JSONObject createGroupJsonObject = new JSONObject();
        try {
            createGroupJsonObject.put("group_title",groupName.getText().toString());
            createGroupJsonObject.put("group_category",topicList.get(topic.getSelectedItemPosition()));
            createGroupJsonObject.put("private", privateGroup.isChecked());
            createGroupJsonObject.put("group_description", description.getText().toString());
            return createGroupJsonObject;
        } catch (Exception e) {
            e.printStackTrace();
            return createGroupJsonObject;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.create_group:
                if (Utils.isConnected()) {
                    if (isValidInput()) {
                        createGroup();
                    }
                } else {
                    Utils.showAlert(CreateGroupActivity.this, getString(R.string.no_internet_connection_available));
                }
                break;
            case R.id.invite_friends:
                startActivity(new Intent(CreateGroupActivity.this, InviteFriendsActivity.class));
                break;
            default:
                break;

        }
    }
}
