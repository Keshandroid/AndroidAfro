package com.TBI.afrocamgist.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import androidx.core.text.HtmlCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Params;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.user.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ConfirmOTPActivity extends BaseActivity implements View.OnClickListener {

    private TextView note, txtResendOtp;
    private EditText otp;
    private KProgressHUD hud;
    private String username = "", registered_with = "";

    //Google Login
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 7;
    private static final String TAG = GoogleLoginHomeActivity.class.getSimpleName();

    //fb login
    LoginButton loginButton;
    CallbackManager callbackManager;

    /*@Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");

                Log.d("otp99","=="+message);

                if(otp!=null){
                    otp.setText(message);
                    otp.setSelection(otp.getText().length());
                    otp.setCursorVisible(true);
                    otp.requestFocus();
                }

                // message is the fetching OTP
            }
        }
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_otp);

        if (getIntent().getStringExtra("username") != null)
            username = getIntent().getStringExtra("username");

        if (getIntent().getStringExtra("registered_with") != null)
            registered_with = getIntent().getStringExtra("registered_with");

        initView();
        findViewById(R.id.googleSignIn).setOnClickListener(this);

        //Google Login
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //fb login
        boolean loggedOut = AccessToken.getCurrentAccessToken() == null;
        if (!loggedOut) {
            //Picasso.get().load(Profile.getCurrentProfile().getProfilePictureUri(200, 200)).into(imageView);
            Logger.d("TAG", "Username is: " + Profile.getCurrentProfile().getName());
            //Using Graph API
            getUserProfile(AccessToken.getCurrentAccessToken());
        }

        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                //loginResult.getAccessToken();
                //loginResult.getRecentlyDeniedPermissions()
                //loginResult.getRecentlyGrantedPermissions()

                boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                Logger.d("API123", loggedIn + " ??");
                getUserProfile(AccessToken.getCurrentAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });


        //OTP Verification
        String noteText = "Please enter the OTP sent to <b>\"" + username + "\"</b> to verify your account.";
        note.setText(HtmlCompat.fromHtml(noteText, HtmlCompat.FROM_HTML_MODE_COMPACT));

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.confirm_otp).setOnClickListener(v -> {
            if (otp.getText().toString().isEmpty())
                Utils.showAlert(this, getString(R.string.please_enter_valid_otp));
            else
                confirmOTP();
        });


        txtResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOTP();
            }
        });

    }

    private void initView() {

        otp = findViewById(R.id.otp);
        note = findViewById(R.id.note);
        loginButton = findViewById(R.id.login_button);
        txtResendOtp = findViewById(R.id.txtResendOtp);
    }

    //confirm otp API call.
    private void confirmOTP() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        ArrayList<Params> params = new ArrayList<>();
        params.add(new Params(Constants.USER_NAME, username));
        params.add(new Params(Constants.OTP, otp.getText().toString()));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.VERIFY_PHONE, response -> {
            hud.dismiss();
            try {
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(ConfirmOTPActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                    startActivity(new Intent(ConfirmOTPActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                    finish();
                }

            } catch (Exception e) {
//                Logger.e("LLLLL_Tok: ",e.getMessage());
                Utils.showAlert(ConfirmOTPActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    //Resend otp API call.
    private void resendOTP() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        ArrayList<Params> params = new ArrayList<>();
        params.add(new Params(Constants.USER_NAME, username));
        params.add(new Params("type", registered_with));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.RESEND_OTP, response -> {
            hud.dismiss();
            try {
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Toast.makeText(ConfirmOTPActivity.this, "" + object.getString(Constants.MESSAGE), Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
//                Logger.e("LLLLL_Tok: ",e.getMessage());
                Utils.showAlert(ConfirmOTPActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.googleSignIn) {
            signIn();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        googleLoginAPI(account);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            /*String personPhotoUrl = account.getPhotoUrl().toString();
            Glide.with(getApplicationContext()).load(personPhotoUrl)
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfilePic);*/
            // Signed in successfully, show authenticated UI.
            googleLoginAPI(account);
        } catch (ApiException e) {
            Logger.w(TAG, "signInResult:failed code=" + e.getStatusCode() + "\n" + e.getLocalizedMessage() + "\nMEssg: " + e.getMessage());
            googleLoginAPI(null);
        }
    }

    private void googleLoginAPI(GoogleSignInAccount account) {

        if (account != null) {
            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();

            String[] splitString = account.getDisplayName().split("\\s+");
            Logger.d("first_last", "==" + splitString[0] + "==" + splitString[1]);

            ArrayList<Params> params = new ArrayList<>();
            params.add(new Params("google_id", account.getId()));
            params.add(new Params("first_name", splitString[0]));
            params.add(new Params("last_name", splitString[1]));
            params.add(new Params("email", account.getEmail()));
            //params.add(new Params("username", account.getId()));
            params.add(new Params("type", "google"));
            params.add(new Params(Constants.FIREBASE_TOKEN, LocalStorage.getToken()));

            Logger.d("paramGoogle", "" + params.toString() + "" + params);

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

            Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LOGIN, response -> {
                hud.dismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    Logger.e("LLLLLL_Res: ", response);

                    LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                    //LocalStorage.setUserPass(password.getText().toString().trim());
                    User user = new Gson().fromJson(object.getJSONObject(Constants.USER).toString(), User.class);
                    LocalStorage.saveUserDetails(user);

                    Toast.makeText(ConfirmOTPActivity.this, getString(R.string.user_logged_in_successfully), Toast.LENGTH_LONG).show();
                    getAfroPopularPosts();

                    if (user.getContactNumber() != null) {
                        if (!user.getContactNumber().equals("")) {
                            if ("google".equals(user.getRegisteredWith())) {
                                if (user.getIntroduced()) {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    proceedToDashboard();
                                } else {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    startActivity(new Intent(ConfirmOTPActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            } else {
                                if (LocalStorage.getIsUserLoggedIn()) {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    proceedToDashboard();
                                } else {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    startActivity(new Intent(ConfirmOTPActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            }
                        } else {
                            LocalStorage.setIsUserLoggedIn(true);
                            startActivity(new Intent(ConfirmOTPActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                        }
                    } else {
                        LocalStorage.setIsUserLoggedIn(true);
                        startActivity(new Intent(ConfirmOTPActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                    }


                } catch (Exception e) {
                    try {
                        JSONObject object = new JSONObject(response);
                        Logger.e("LLLLLL_Data: ", response);
                        Utils.showAlert(ConfirmOTPActivity.this, getString(R.string.please_enter_valid_email_or_password));
                    } catch (JSONException e1) {
                        Utils.showAlert(ConfirmOTPActivity.this, getString(R.string.opps_something_went_wrong));
                        e1.printStackTrace();
                    }
                }
            });
        }
    }

    private void fbLoginAPI(String id, String first_name, String last_name, String email) {

        if (id != null) {
            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();


            ArrayList<Params> params = new ArrayList<>();
            params.add(new Params("facebook_id", id));
            params.add(new Params("first_name", first_name));
            params.add(new Params("last_name", last_name));
            params.add(new Params("email", email));
            //params.add(new Params("username", id));
            params.add(new Params("type", "facebook"));
            params.add(new Params(Constants.FIREBASE_TOKEN, LocalStorage.getToken()));

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

            Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LOGIN, response -> {
                hud.dismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    Logger.e("LLLLLL_Res: ", response);

                    LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                    //LocalStorage.setUserPass(password.getText().toString().trim());
                    User user = new Gson().fromJson(object.getJSONObject(Constants.USER).toString(), User.class);
                    LocalStorage.saveUserDetails(user);

                    Toast.makeText(ConfirmOTPActivity.this, getString(R.string.user_logged_in_successfully), Toast.LENGTH_LONG).show();
                    getAfroPopularPosts();

                    if (user.getContactNumber() != null) {
                        if (!user.getContactNumber().equals("")) {
                            if ("facebook".equals(user.getRegisteredWith())) {
                                if (user.getIntroduced()) {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    proceedToDashboard();
                                } else {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    startActivity(new Intent(ConfirmOTPActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            } else {
                                if (LocalStorage.getIsUserLoggedIn()) {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    proceedToDashboard();
                                } else {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    startActivity(new Intent(ConfirmOTPActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            }
                        } else {
                            LocalStorage.setIsUserLoggedIn(true);
                            startActivity(new Intent(ConfirmOTPActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                        }
                    } else {
                        LocalStorage.setIsUserLoggedIn(true);
                        startActivity(new Intent(ConfirmOTPActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                    }

                } catch (Exception e) {
                    try {
                        JSONObject object = new JSONObject(response);
                        Logger.e("LLLLLL_Data: ", response);
                        Utils.showAlert(ConfirmOTPActivity.this, getString(R.string.please_enter_valid_email_or_password));
                    } catch (JSONException e1) {
                        Utils.showAlert(ConfirmOTPActivity.this, getString(R.string.opps_something_went_wrong));
                        e1.printStackTrace();
                    }
                }
            });
        }
    }

    private void proceedToDashboard() {
        // Make bubble screen the main screen
        /*Intent intent = new Intent(ConfirmOTPActivity.this, BubbleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();*/

        Intent intent = new Intent(ConfirmOTPActivity.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void getAfroPopularPosts() {
        Logger.e("LLLL_Bare: ", LocalStorage.getLoginToken());
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MOST_POPULAR, response -> {
            if ("".equals(response)) {
                Utils.showAlert(ConfirmOTPActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ConfirmOTPActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);

                        if (postDetails.getPosts() != null && postDetails.getPosts().size() != 0) {
                            LocalStorage.savePopularPosts(postDetails.getPosts());
                        }
                    }

                } catch (Exception e) {
//                    Logger.e("LLLL_Data: ",e.getMessage());
                    e.printStackTrace();
                    Utils.showAlert(ConfirmOTPActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Logger.d("TAG", object.toString());
                        try {

                            if (object.has("email")) {
                                //String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                                //Picasso.get().load(image_url).into(imageView);

                                String id = object.getString("id");
                                String first_name = object.getString("first_name");
                                String last_name = object.getString("last_name");
                                String email = object.getString("email");

                                //Logger.d("FB_DATA_1","=="+first_name+"=="+last_name+"=="+email+"=="+id);

                                fbLoginAPI(id, first_name, last_name, email);

                            } else {
                                Toast.makeText(ConfirmOTPActivity.this, getString(R.string.your_facebook_account_doesnt_have_email), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Logger.d("FbException", "" + e.getLocalizedMessage());
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }
}