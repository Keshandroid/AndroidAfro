package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.util.Base64;
import android.view.View;
import android.view.ViewConfiguration;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.StoryConstants;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.VideoPreLoadingService;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.story.StoryDetail;
import com.TBI.afrocamgist.model.story.StoryUserData;
import com.TBI.bubblesview.BubbleClickListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.TBI.bubblesview.adapter.BubblesViewAdapter;
import com.TBI.bubblesview.model.BubbleItem;
import com.TBI.bubblesview.rendering.BubblesView;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import id.zelory.compressor.Compressor;


public class BubbleActivity extends BaseActivity {

    private BubblesView bubblesView;
    private ArrayList<String> menuItems = new ArrayList<>();
    private DrawerLayout drawer;
    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bubble);

        initView();
        /*fetch preloaded posts from previous splash activity and if not available
                then fetch from server again*/

        if (getIntent().getSerializableExtra("posts_bubble")==null) {
            if (Utils.isConnected())
                getAfroPopularPosts();
            else
                Utils.showAlert(this, getString(R.string.please_check_your_internet_connection));
        } else {
            ArrayList<Post> posts = (ArrayList<Post>) getIntent().getSerializableExtra("posts_bubble");
            setBubblesView(posts);
        }


        setClickListener();

    }

    private void initView() {

        bubblesView = findViewById(R.id.bubblesView);
    }

    private void setClickListener() {

        findViewById(R.id.logo).setOnClickListener(v -> {
            //startActivity(new Intent(BubbleActivity.this, DashboardActivity.class));
            //finish();

            onBackPressed();

        });
    }

    private void getAfroPopularPosts() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MOST_POPULAR, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(BubbleActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(BubbleActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);

                        if (postDetails.getPosts() != null) {
                            LocalStorage.savePopularPosts(postDetails.getPosts());
                            setBubblesView(postDetails.getPosts());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(BubbleActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    private void getAndStorePopularPostsImages(ArrayList<Post> posts) {
        try {
            new GetAndStoreImage(posts).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //set bubbles view with the popular post data.
    private void setBubblesView(ArrayList<Post> posts) {

        startPreLoadingService(posts);

        ViewConfiguration viewConfig = ViewConfiguration.get(this);

        bubblesView.setScaledTouchSlop(viewConfig.getScaledTouchSlop());
        bubblesView.setBubbleSize(30);
        bubblesView.setCenterImmediately(true);
        bubblesView.setZOrderOnTop(false);
        bubblesView.setAdapter(new BubblesViewAdapter() {
            @Override
            public int getTotalCount() {
                return posts.size();
            }

            @NonNull
            @Override
            public BubbleItem getItem(int position) {
                return setBubbleItem(position, posts);
            }
        });

        bubblesView.setListener(item -> {
            //startActivity(new Intent(BubbleActivity.this, DashboardActivity.class).putExtra("postId", item.getPostId()));


            /*startActivity(new Intent(BubbleActivity.this, ViewPostActivity.class).putExtra("postId", item.getPostId()));
            finish();*/

            Post post = getSinglePost(posts,item.getPostId());
            if(post != null){
                //startActivity(new Intent(BubbleActivity.this, ViewPostActivity.class).putExtra("singlePost", post));

                //Open single story popular post
                //startActivity(new Intent(BubbleActivity.this, StorySinglePostActivity.class).putExtra("singlePost", post));

                //Open all story popular posts
                startActivity(new Intent(BubbleActivity.this, StoryPopularPostActivity.class)
                        .putExtra("posts", posts)
                        .putExtra("postPosition", item.getPostPosition() ));

                finish();
            }

        });

        findViewById(R.id.view_posts).setOnClickListener(view -> {
            //Intent intent = new Intent(BubbleActivity.this, DashboardActivity.class);


            //Intent intent = new Intent(BubbleActivity.this, PopularPostsActivity.class);
            Intent intent = new Intent(BubbleActivity.this, StoryPopularPostActivity.class);

            intent.putExtra("posts", posts);
            startActivity(intent);
            finish();
        });

        bubblesView.onPause();
        bubblesView.onResume();

        YoYo.with(Techniques.Pulse)
                .duration(1500)
                .repeat(YoYo.INFINITE)
                .playOn(bubblesView);
    }

    private void startPreLoadingService(ArrayList<Post> allPost) {

        ArrayList<String> videoList = new ArrayList<>();

        for (int i=0;i<allPost.size();i++){
            if(allPost.get(i).getPostType().equalsIgnoreCase("video")){
                String url = UrlEndpoints.MEDIA_BASE_URL + allPost.get(i).getPostVideo();
                videoList.add(url);
            }
        }

        Logger.d("bubble_cache_popular",new GsonBuilder().setPrettyPrinting().create().toJson(videoList));

        Intent preloadingServiceIntent = new Intent(BubbleActivity.this, VideoPreLoadingService.class);
        preloadingServiceIntent.putStringArrayListExtra(StoryConstants.VIDEO_LIST, videoList);
        startService(preloadingServiceIntent);
    }


    public Post getSinglePost(ArrayList<Post> posts, Integer postId) {

        for(int i=0;i<posts.size();i++){
            if(posts.get(i).getPostId().equals(postId)){
                return posts.get(i);
            }
        }
        return null;
    }

    //set single bubbles data
    private BubbleItem setBubbleItem(int position, ArrayList<Post> posts) {

        Post post = posts.get(position);

        BubbleItem item = new BubbleItem();
        float area = (float) (2.0 - ((float) position / 5));

        item.setPostPosition(position);
        item.setArea(area);
        item.setPostId(post.getPostId());
        item.setSelected(true);

        try {
            if ("video".equals(post.getPostType())) {
                item.setIcon(getResources().getDrawable(R.drawable.ic_play));
                item.setOverlayAlpha(0.5f);
                if (LocalStorage.getPopularPostsImage()!=null && LocalStorage.getPopularPostsImage().get(position)!=null) {
                    byte[] imageAsBytes = Base64.decode(LocalStorage.getPopularPostsImage().get(position).getBytes(), Base64.DEFAULT);
                    item.setBackgroundImage(new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length)));
                } else
                    new GetImage(UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail() + "?width=200&height=200",item).execute().get();
            } else if ("image".equals(post.getPostType())) {
                if (LocalStorage.getPopularPostsImage()!=null && LocalStorage.getPopularPostsImage().get(position)!=null){
                    byte[] imageAsBytes = Base64.decode(LocalStorage.getPopularPostsImage().get(position).getBytes(), Base64.DEFAULT);
                    item.setBackgroundImage(new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length)));
                }else
                    new GetImage(UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0) + "?width=200&height=200", item).execute().get();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        item.setTitle(post.getPostText());
        item.setCustomData(post);
        item.setTextColor(ContextCompat.getColor(BubbleActivity.this, R.color.white));

        return item;
    }

    //store posts image locally for quick access next time.
    private class GetAndStoreImage extends AsyncTask<Void, Void, ArrayList<String>> {

        private ArrayList<Post> posts;

        GetAndStoreImage(ArrayList<Post> posts) {
            this.posts = posts;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<String> images) {
            super.onPostExecute(images);
            LocalStorage.savePopularPostsImage(images);
        }

        @Override
        protected ArrayList<String> doInBackground(Void... voids) {

            ArrayList<String> drawables = new ArrayList<>();

            for (Post post : posts) {

                String src;

                if ("video".equals(post.getPostType())) {
                    src = UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail() + "?width=200&height=200";
                    BitmapDrawable drawable = getBitmapFromURL(src);
                    if (drawable!=null)
                        drawables.add(convertDrawableToString(drawable));
                    else
                        drawables.add(null);
                } else if ("image".equals(post.getPostType())) {
                    src = UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0) + "?width=200&height=200";
                    BitmapDrawable drawable = getBitmapFromURL(src);
                    if (drawable!=null)
                        drawables.add(convertDrawableToString(drawable));
                    else
                        drawables.add(null);
                } else {
                    drawables.add(null);
                }
            }

            return drawables;
        }

        private String convertDrawableToString(BitmapDrawable drawable) {

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            drawable.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, outputStream); //bm is the bitmap object
            byte[] b = outputStream.toByteArray();
            return Base64.encodeToString(b, Base64.DEFAULT);
        }
    }

    private class GetImage extends AsyncTask<Void, Void, Void> {
        private String src;
        private BubbleItem item;

        GetImage(String src, BubbleItem item) {
            this.src = src;
            this.item = item;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        /*@Override
        protected void onPostExecute(BitmapDrawable image) {
            super.onPostExecute(image);
        }*/

        @Override
        protected Void doInBackground(Void... voids) {
            item.setBackgroundImage(getBitmapFromURL(src));
            return null;
        }
    }

    public BitmapDrawable getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return new BitmapDrawable(Resources.getSystem(), getCompressedBitmap(BitmapFactory.decodeStream(input)));
        } catch (Exception e) {
            // Logger exception
            return null;
        }
    }

    private Bitmap getCompressedBitmap(Bitmap image) {

        File imageFile = new File(Environment.getExternalStorageDirectory(), Calendar.getInstance().getTimeInMillis() + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            image.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();

            Bitmap file =  new Compressor(BubbleActivity.this)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setQuality(60)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToBitmap(imageFile);

           imageFile.delete();

            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return image;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
        bubblesView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        bubblesView.onPause();
    }
}
