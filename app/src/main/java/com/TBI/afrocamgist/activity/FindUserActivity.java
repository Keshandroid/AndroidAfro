package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.FindUserAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.finduser.UserDetails;
import com.TBI.afrocamgist.model.finduser.UserList;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.model.user.User;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FindUserActivity extends BaseActivity implements FindUserAdapter.OnProfileClickListener, ConnectivityListener {

    private String query = "",openFromScreen = "";
    private EditText search;
    private SwipeRefreshLayout swipeContainer;
    private Connectivity mConnectivity;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_user);

        if(getIntent().getStringExtra("openFromScreen") !=null){
            openFromScreen = getIntent().getStringExtra("openFromScreen");
        }

        initView();
        initSearchTextChangeListener();
        initSwipeRefresh();
        setClickListener();
        initInternetConnectivityCheckListener();
    }

    private void initView() {

        search = findViewById(R.id.search);
        swipeContainer = findViewById(R.id.swipeContainer);
    }

    private void initSearchTextChangeListener() {

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                //get list of users based on the search input
                query = s.toString();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Logger.d("SEARCH_API","called...");
                        getUsers();
                    }
                },1000);
            }
        });
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getUsers();
            else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(this, getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            Utils.closeAlert();
            getUsers();
        } else {
            Utils.showAlert(FindUserActivity.this, getString(R.string.no_internet_connection_available));
        }
    }

    //get list of all user API
    private void getUsers() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.FIND_USERS + query;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                swipeContainer.setRefreshing(false);
                try {

                    Logger.d("SEARCH_RESPONSE",""+response.toString());

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FindUserActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        UserList userList = new Gson().fromJson(response, UserList.class);
                        if (userList.getUsers()!=null) {
                            setUserList(userList.getUsers());
                        }
                    }

                } catch (Exception e){
                    e.printStackTrace();
                    Utils.showAlert(FindUserActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setUserList(ArrayList<UserDetails> users) {

        RecyclerView userList = findViewById(R.id.user_list);
        userList.setLayoutManager(new LinearLayoutManager(this));
        userList.setAdapter(new FindUserAdapter(users, this));
    }

    @Override
    public void onProfileClick(int userId, UserDetails userDetails) {
        if(userDetails != null && userDetails.getRequestButtons()!=null){

            this.user = new User();
            this.user.setFirstName(userDetails.getFirstName());
            this.user.setLastName(userDetails.getLastName());
            this.user.setUserId(userDetails.getUserId());
            this.user.setProfileImageUrl(userDetails.getProfileImageUrl());
            this.user.setBlockedByMe(userDetails.getBlockedByMe());

            if(!userDetails.getRequestButtons().isEmpty()){
                if(userDetails.getRequestButtons().get(0).getButtonText().equalsIgnoreCase("following")){
                    if(openFromScreen.equalsIgnoreCase("search")){
                        //KK
                        startActivity(new Intent(FindUserActivity.this, ChatActivity.class).putExtra("user", user));
//                        startActivity(new Intent(FindUserActivity.this, SocketChatActivity.class).putExtra("user", user));
                    }else {
                        startActivity(new Intent(FindUserActivity.this, ProfileActivity.class).putExtra("userId",userId));
                    }
                }else {
                    startActivity(new Intent(FindUserActivity.this, ProfileActivity.class).putExtra("userId",userId));
                }
            }
        }
    }

    @Override
    public void onFollowAndUnfollowToggleClick(int userId, boolean isFollowed) {

        if (isFollowed)
            followUser(userId);
        else
            unfollowUser(userId);

    }

    @Override
    public void blockUnblockUser(int userId, boolean isBlock) {
        String url = "";
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        if (!isBlock)
            url = UrlEndpoints.PROFILE + "/" + userId + "/block";
        else
            url = UrlEndpoints.PROFILE + "/" + userId + "/unblock";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.e("LLLLL_Res: ", response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FindUserActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    Utils.showAlert(FindUserActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FindUserActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    Utils.showAlert(FindUserActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    private void unfollowUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/cancel-follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FindUserActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    Utils.showAlert(FindUserActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnectivity != null) {
            mConnectivity.removeInternetConnectivityChangeListener(this);
        }
    }
}
