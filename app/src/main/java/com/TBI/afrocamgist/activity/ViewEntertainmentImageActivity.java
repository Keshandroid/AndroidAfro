
package com.TBI.afrocamgist.activity;


import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.OnSwipeTouchListener;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.ViewEnterPostAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.bottomsheet.EntertainmentCommentBottomSheetFragment;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.NewAfroSwaggerFragment;
import com.TBI.afrocamgist.fragment.SwaggerCommentBottomSheetFragment;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.post.Post;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.recyclerview.widget.SnapHelper;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.widget.Container;
import ooo.oxo.library.widget.PullBackLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewEntertainmentImageActivity extends BaseActivity implements PullBackLayout.Callback,ViewEnterPostAdapter.OnEntertainmentPostClickListener,
        EntertainmentCommentBottomSheetFragment.DialogDismissListener{

    private KProgressHUD hud;

    private TextView comments, likes_count, txtUserName, more,less, videoCount;
    private CircleImageView profileImage;
    private AutoLinkTextView postText;
    private ToggleButton likes;
    private RelativeLayout relProfile;
    private Container imageList;
    private ArrayList<Post> posts = new ArrayList<>();
    private ArrayList<String> images = new ArrayList<>();
    private int position;
    private ViewEnterPostAdapter enterPostAdapter;
    public static String Comment_Count="comment_count";
    public static String IsLike="islike";
    public static String Like_Count="like_count";
    public static String Post_Id="post_id";
    public static String NITIFYSTATE="notifystate";
    public static String NITIFYDATA="notifydata";
    public static String NITIFY_DATA_RECEIVE="notifydatareceiver";
    protected LocalBroadcastManager localBroadcastManager;
    private boolean isLoading = false;
    SnapHelper snapHelper;

    //Download Video from url
    String fileN = null ;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1234;
    boolean result;
    ProgressDialog mProgressDialog;
    private String waterMarkVideo;
    private Dialog popup;
    private String shareOrDownload = "";
    private ImageView socialShare;
    private static String mVideoUrl;

    public static String NOTIFY_LIKE_SWAGGER_TALENT="notify_like_swagger_talent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_entertainment_image);
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(mDataReceiver,
                new IntentFilter(ViewEntertainmentImageActivity.NITIFY_DATA_RECEIVE));
        if (getIntent().getSerializableExtra("images") != null) {
            images = (ArrayList<String>) getIntent().getSerializableExtra("images");
        }

        if (getIntent().getSerializableExtra("posts") != null) {
            posts = (ArrayList<Post>) getIntent().getSerializableExtra("posts");
        }

        position = getIntent().getIntExtra("position", 0);
        initView();
        setRecyclerView();
        initImageList();
        getPostDetails();
        setClickListener();
    }

    @Override
    public void onRestart() {
        super.onRestart();
       /* setClickListener();
        initView();
        if (comments!=null){
            if (CommentsActivity.isCommentPosted){
                ArrayList<Comment> comments1 = CommentsActivity.comments;
                int commentCount = 0;
                for (Comment comment : comments1) {
                    commentCount++;
                    if (comment.getSubComments() != null && comment.getSubComments().size() > 0) {
                        commentCount += comment.getSubComments().size();
                    }
                }
                comments.setText(commentCount+"");

                LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
                posts.get(layoutManager.findFirstVisibleItemPosition()).setCommentCount(commentCount);
                posts.get(layoutManager.findFirstVisibleItemPosition()).setComments(comments1);

                notifyState();
            }
        }*/
    }

    private void initView() {
        likes = findViewById(R.id.likes);
        likes_count = findViewById(R.id.likes_count);
        videoCount = findViewById(R.id.videoCount);
        comments = findViewById(R.id.comments);
        imageList = findViewById(R.id.image_list);

        txtUserName = findViewById(R.id.txtUserName);
        profileImage = findViewById(R.id.profile_picture);
        more = findViewById(R.id.more);
        less = findViewById(R.id.less);
        postText = findViewById(R.id.post_text);
        relProfile = findViewById(R.id.relProfile);

        socialShare = findViewById(R.id.social_share);
    }

    private void initImageList() {
        ViewCompat.setNestedScrollingEnabled(imageList, false);
        imageList.setNestedScrollingEnabled(false);
        imageList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        enterPostAdapter = new ViewEnterPostAdapter(this, posts, this);


        imageList.setAdapter(enterPostAdapter);
        if (imageList.getLayoutManager() != null) {
            imageList.getLayoutManager().scrollToPosition(position);
        }

        setLikesAndCommentsData(position);


       /* imageList.setOnTouchListener(new OnSwipeTouchListener(ViewEntertainmentImageActivity.this) {
            public void onSwipeTop() {
            }

            public void onSwipeRight() {
                if ((position - 1) >= 0) {
                    if (imageList.getLayoutManager() != null) {
                        position = position - 1;
                        imageList.getLayoutManager().scrollToPosition(position);
                        setLikesAndCommentsData(position);
                        imageList.getAdapter().notifyItemChanged(position);
                    }
                }
            }

            public void onSwipeLeft() {
//                LayoutAnimationController layoutAnimation = AnimationUtils.loadLayoutAnimation(ViewEntertainmentImageActivity.this, R.anim.layout_anim_r);

                if ((position +1) <= posts.size()-1) {
//                    imageList.setLayoutAnimation(layoutAnimation);
                    Logger.e("LLLLL_Left: ",""+position);
                    if (imageList.getLayoutManager() != null) {
                        position = position + 1;
                        imageList.getLayoutManager().scrollToPosition(position);
                        setLikesAndCommentsData(position);
                        imageList.getAdapter().notifyItemChanged(position);
                    }
                }else{
                    if (!isLoading) {
                        isLoading = true;
                        Logger.e("LLLLL_Left: ","Else :"+position);
//                    entertainmentPosts.add(null);
//                    entertainmentAdapter.notifyItemInserted(entertainmentPosts.size() - 1);
                        getHashtagNextPage();
                    }
                }
            }

            public void onSwipeBottom() {
            }

        });*/
         snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(imageList);



//        imageList.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                setLikesAndCommentsData(position);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
    }

    private void setRecyclerView() {
        imageList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE) {
                    View centerView = snapHelper.findSnapView(recyclerView.getLayoutManager());
                     position = recyclerView.getLayoutManager().getPosition(centerView);
                    Logger.e("Snapped Item Position:",""+position);
                    if ((position +1) <= posts.size()-1) {
//                    imageList.setLayoutAnimation(layoutAnimation);
                        Logger.e("LLLLL_Left: ",""+position);
                        if (imageList.getLayoutManager() != null) {
                            //position = position + 1;
                           // imageList.getLayoutManager().scrollToPosition(position);
                            setLikesAndCommentsData(position);
                           // imageList.getAdapter().notifyItemChanged(position);
                        }
                    }else{
                        if (!isLoading) {
                            isLoading = true;
                            Logger.e("LLLLL_Left: ","Else :"+position);
//                    entertainmentPosts.add(null);
//                    entertainmentAdapter.notifyItemInserted(entertainmentPosts.size() - 1);
                            getHashtagNextPage();
                        }
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                /*int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;

                if (recyclerView.getLayoutManager() != null) {
                    visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }

                if (!isLoading  && ((visibleItemCount + pastVisiblesItems) >= totalItemCount)) {
                    isLoading = true;
//                    entertainmentPosts.add(null);
//                    entertainmentAdapter.notifyItemInserted(entertainmentPosts.size() - 1);
                    getHashtagNextPage();
                }*/
            }
        });
    }

    private void callPostViewCount(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getOpenPostViewCount(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("onPostViewOpen",""+jsonObject.toString());
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    private void callPostView(int postId) {

        Logger.d("postview","postID : "+postId + "  userID : "+LocalStorage.getUserDetails().getUserId());

        JSONObject request = new JSONObject();
        try {
            request.put("user_id", LocalStorage.getUserDetails().getUserId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        String url = "open" + "/" + postId + "/most-popular-seen";

        Webservices.getData(Webservices.Method.PUT, request,headers, url, response -> {
            try {
                JSONObject object = new JSONObject(response);
                Logger.d("postview",""+object.toString());

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(ViewEntertainmentImageActivity.this, object.getString(Constants.MESSAGE));
                } else {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void callVideoCount(int videoPostId){
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getOpenVideoCount(videoPostId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("ViewCount",""+jsonObject.get("counter"));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    private void setLikesAndCommentsData(int position) {

        if (posts != null && posts.size() > 0) {

            Post post = posts.get(position);
            Logger.e("LLLLL_Like: ",post.getLikeCount() + "    "+post.getLiked());

            if(post.getPostId()!=null){
                callPostView(post.getPostId()); // call postview api
                callPostViewCount(post.getPostId()); // call postview count api
                callVideoCount(post.getPostId());
            }

            if (post.getLikeCount() != null) {
                if (post.getLiked()!=null && post.getLiked()){
                    likes_count.setTextColor(Color.parseColor("#FF7400"));
                    likes.setChecked(true);
                } else {
                    likes_count.setTextColor(Color.parseColor("#FFFFFF"));
                    likes.setChecked(false);
                }
                likes_count.setText(String.valueOf(post.getLikeCount()));
            }

            if (post.getCommentCount() != null) {
                comments.setText(String.valueOf(post.getCommentCount()));
            }

            if(post.getProfileImageUrl()!=null){
                Glide.with(this)
                        .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                        .into(profileImage);
            }

            if(post.getUser_name()!=null && !post.getUser_name().isEmpty()){
                txtUserName.setText(post.getUser_name());
            }else {
                String fullName = post.getFirstName() + " " + post.getLastName();
                txtUserName.setText(fullName);
            }

            //video Count
            if (post.getPostType().equalsIgnoreCase("shared")) {
                if (post.getSharedPost() != null) {
                    if (post.getSharedPost().getVideo_play_count() != null) {
                        Log.d("name_count", post.getFirstName() + "===" + post.getVideo_play_count());
                        videoCount.setVisibility(View.VISIBLE);
                        if (post.getSharedPost().getVideo_play_count() == 1) {
                            videoCount.setText(Utils.prettyCount(post.getSharedPost().getVideo_play_count()) + " " + "view");
                        } else {
                            videoCount.setText(Utils.prettyCount(post.getSharedPost().getVideo_play_count()) + " " + "views");
                        }
                    } else {
                        videoCount.setVisibility(View.GONE);
                    }
                }
            } else {
                if (post.getVideo_play_count() != null) {
                    videoCount.setVisibility(View.VISIBLE);
                    Log.d("name_count", post.getFirstName() + "===" + post.getVideo_play_count());
                    if (post.getVideo_play_count() == 1) {
                        videoCount.setText(Utils.prettyCount(post.getVideo_play_count()) + " " + "view");
                    } else {
                        videoCount.setText(Utils.prettyCount(post.getVideo_play_count()) + " " + "views");
                    }
                } else {
                    videoCount.setVisibility(View.GONE);
                }
            }

            postText.addAutoLinkMode(AutoLinkMode.MODE_HASHTAG,AutoLinkMode.MODE_URL);
            postText.setHashtagModeColor(ContextCompat.getColor(this, R.color.hashtag));
            postText.setUrlModeColor(ContextCompat.getColor(this, R.color.colorAccent));
            postText.setAutoLinkText(post.getPostText());


            postText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    postText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int linecount = postText.getLineCount();

                    if(linecount > 4){
                        more.setVisibility(View.VISIBLE);
                        postText.setMaxLines(4);
                        postText.setEllipsize(TextUtils.TruncateAt.END);
                        more.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                postText.setMaxLines(Integer.MAX_VALUE);
                                more.setVisibility(View.GONE);
                                less.setVisibility(View.VISIBLE);
                            }
                        });
                        less.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                postText.setMaxLines(4);
                                more.setVisibility(View.VISIBLE);
                                less.setVisibility(View.GONE);
                            }
                        });
                    }else {
                        more.setVisibility(View.GONE);
                        less.setVisibility(View.GONE);
                    }


                }
            });

            postText.setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
                @Override
                public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {
                    if(autoLinkMode == AutoLinkMode.MODE_HASHTAG){
                        Hashtag hashtag = new Hashtag();
                        if(matchedText.startsWith("#")){
                            matchedText =matchedText.substring(1);
                        }
                        hashtag.setHashtagSlug(matchedText);
                        Logger.d("hashtag1",""+hashtag.getHashtagSlug());
                        startActivity(new Intent(ViewEntertainmentImageActivity.this, HashtagPostsActivity.class).putExtra("hashtag",hashtag));
                    }else if(autoLinkMode == AutoLinkMode.MODE_URL){
                        try{
                            Uri uri = Uri.parse(matchedText);
                            Intent httpIntent = new Intent(Intent.ACTION_VIEW,uri);
                            startActivity(httpIntent);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
            });

        }
    }

    private void setClickListener() {

        socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(imageList.getLayoutManager() != null){
                    LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
                    String url = UrlEndpoints.BASE_SHARE_POST_URL + posts.get(layoutManager.findFirstVisibleItemPosition()).getPostId();
                    if ("shared".equals(posts.get(layoutManager.findFirstVisibleItemPosition()).getPostType())) {
                        onDownloadClick(posts.get(layoutManager.findFirstVisibleItemPosition()).getSharedPost().getPostVideo(), url);
                    } else {
                        onDownloadClick(posts.get(layoutManager.findFirstVisibleItemPosition()).getPostVideo(), url);
                    }
                }


            }
        });

        relProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (imageList.getLayoutManager() != null) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();

                    if ("shared".equals(posts.get(layoutManager.findFirstVisibleItemPosition()).getPostType()) && posts.get(layoutManager.findFirstVisibleItemPosition()).getSharedPost() != null) {
                        if (LocalStorage.getUserDetails().getUserId().equals(posts.get(layoutManager.findFirstVisibleItemPosition()).getSharedPost().getUserId()))
                            startActivity(new Intent(ViewEntertainmentImageActivity.this, MyProfileActivity.class));
                        else
                            startActivity(new Intent(ViewEntertainmentImageActivity.this, ProfileActivity.class)
                                    .putExtra("userId", posts.get(layoutManager.findFirstVisibleItemPosition()).getSharedPost().getUserId()));
                    } else {
                        if (LocalStorage.getUserDetails().getUserId().equals(posts.get(layoutManager.findFirstVisibleItemPosition()).getUserId()))
                            startActivity(new Intent(ViewEntertainmentImageActivity.this, MyProfileActivity.class));
                        else
                            startActivity(new Intent(ViewEntertainmentImageActivity.this, ProfileActivity.class)
                                    .putExtra("userId", posts.get(layoutManager.findFirstVisibleItemPosition()).getUserId()));
                    }
                }
            }
        });

        likes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    if (imageList.getLayoutManager() != null) {
                        LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
                        if (isChecked) {

                            likes_count.setTextColor(Color.parseColor("#FF7400"));

                            int counter = posts.get(layoutManager.findFirstVisibleItemPosition()).getLikeCount();
                            posts.get(layoutManager.findFirstVisibleItemPosition()).setLikeCount(counter + 1);

                            String likeCount = (Integer.valueOf(likes_count.getText().toString()) + 1) + "";
                            likes_count.setText(likeCount);


                        } else {
                            if(posts.get(layoutManager.findFirstVisibleItemPosition()).getLikeCount() > 0) {



                                likes_count.setTextColor(Color.parseColor("#FFFFFF"));

                                int counter = posts.get(layoutManager.findFirstVisibleItemPosition()).getLikeCount();
                                posts.get(layoutManager.findFirstVisibleItemPosition()).setLikeCount(counter - 1);

                                String likeCount = (Integer.parseInt(likes_count.getText().toString().trim()) - 1) + "";
                                likes_count.setText(likeCount);


                            }
                        }
                    }
//                    posts.get(imageList.getLayoutManager().findFirstVisibleItemPosition()).setLiked(isChecked);
//                    likeAndUnlikePost(posts.get(imageList.getCurrentItem()).getPostId());
                }else {
                    if (imageList.getLayoutManager() != null) {
                        LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();

                        if (isChecked) {

                            Log.d("liked09","333");

                            likes_count.setTextColor(Color.parseColor("#FF7400"));

                            int counter = posts.get(layoutManager.findFirstVisibleItemPosition()).getLikeCount();
                            posts.get(layoutManager.findFirstVisibleItemPosition()).setLikeCount(counter + 1);

                            String likeCount = (Integer.valueOf(likes_count.getText().toString()) + 1) + "";
                            likes_count.setText(likeCount);

                            //notifyState();
                        } else {
                            if(posts.get(layoutManager.findFirstVisibleItemPosition()).getLikeCount() > 0) {

                                Log.d("liked09","444");

                                likes_count.setTextColor(Color.parseColor("#FFFFFF"));

                                int counter = posts.get(layoutManager.findFirstVisibleItemPosition()).getLikeCount();
                                posts.get(layoutManager.findFirstVisibleItemPosition()).setLikeCount(counter - 1);

                                String likeCount = (Integer.parseInt(likes_count.getText().toString().trim()) - 1) + "";
                                likes_count.setText(likeCount);

                                //notifyState();
                            }
                        }
                    }
                }



                if (buttonView.isPressed()) {

                    LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
                    posts.get(layoutManager.findFirstVisibleItemPosition()).setLiked(isChecked);
                    likeAndUnlikePost(posts.get(layoutManager.findFirstVisibleItemPosition()).getPostId(),isChecked);
                    notifyState();
                    notifySwaggerTalentPosts();
                } else {

                    Log.d("liked09","666");

                    LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
                    posts.get(layoutManager.findFirstVisibleItemPosition()).setLiked(isChecked);
                    likeAndUnlikePost(posts.get(layoutManager.findFirstVisibleItemPosition()).getPostId(),isChecked);
                    notifyState();
                    notifySwaggerTalentPosts();
                }

            }
        });


        likes_count.setOnClickListener(v -> {
            if (!"0".equals(likes_count.getText().toString())) {
                if (imageList.getLayoutManager() != null) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
                    startActivity(new Intent(ViewEntertainmentImageActivity.this, PostLikedByActivity.class)
                            .putExtra("postId", posts.get(layoutManager.findFirstVisibleItemPosition()).getPostId()));
                }
//                    startActivity(new Intent(ViewEntertainmentImageActivity.this, PostLikedByActivity.class)
//                            .putExtra("postId", posts.get(imageList.getCurrentItem()).getPostId()));
            }
        });

        comments.setOnClickListener(v -> {
            if (imageList.getLayoutManager() != null) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();

                /*startActivityForResult(new Intent(ViewEntertainmentImageActivity.this, CommentsActivity.class)
                        .putExtra("comments", posts.get(layoutManager.findFirstVisibleItemPosition()).getComments())
                        .putExtra("postId", posts.get(layoutManager.findFirstVisibleItemPosition()).getPostId()), 101);*/

                EntertainmentCommentBottomSheetFragment entertainmentCommentBottomSheetFragment = new EntertainmentCommentBottomSheetFragment();
                EntertainmentCommentBottomSheetFragment.newInstance(posts.get(layoutManager.findFirstVisibleItemPosition()).getComments(),
                        posts.get(layoutManager.findFirstVisibleItemPosition()).getPostId(),ViewEntertainmentImageActivity.this).show(getSupportFragmentManager(), entertainmentCommentBottomSheetFragment.getTag());

            }

        });

        findViewById(R.id.close).setOnClickListener(v -> onBackPressed());
        //((PullBackLayout) findViewById(R.id.pull)).setCallback(this);
        findViewById(R.id.share).setOnClickListener(view -> {
            if (imageList.getLayoutManager() != null) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
                int position = layoutManager.findFirstVisibleItemPosition();
            }
//            int position = imageList.getCurrentItem();
            startActivity(new Intent(this, ViewPostActivity.class).putExtra("postId", posts.get(position).getPostId()));
        });
    }

    @Override
    public void onPullStart() {
    }

    @Override
    public void onPull(float v) {
    }

    @Override
    public void onPullCancel() {
    }

    @Override
    public void onPullComplete() {
        supportFinishAfterTransition();
        overridePendingTransition(0, 0);
    }

    public void onDownloadClick(String videoUrl, String postLink) {
        mVideoUrl = videoUrl;
        showShareSelectionDialog(ViewEntertainmentImageActivity.this,videoUrl,postLink);
    }

    public void showShareSelectionDialog(Context context, String videoUrl, String postLink) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_social_share);
            popup.setCancelable(true);
            popup.show();

            RelativeLayout download = popup.findViewById(R.id.rlDownload);
            RelativeLayout share = popup.findViewById(R.id.rlShare);

            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "download";
                    downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "share";
                    popup.dismiss();
                    shareVideoLink(postLink, context);

                }
            });

        }
    }

    private void downloadVideo(String videoUrl){
        String originalFilename = videoUrl.substring(videoUrl.lastIndexOf("/")+1);
        result = checkPermission();
        if(result){
            checkFolder();
            if (!isConnectingToInternet(ViewEntertainmentImageActivity.this)) {
                Toast.makeText(ViewEntertainmentImageActivity.this, getString(R.string.please_check_your_internet_connection), Toast.LENGTH_LONG).show();
                return;
            }

            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename);
            if(file.exists()){
                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename;
                shareVideoToSocialMedia(filePath);
            }else {
                callWatermarkVideoAPI(videoUrl);
            }
        }
    }

    //Get watermarked video from url
    private void callWatermarkVideoAPI(String videoUrl){
        waterMarkVideo = "";
        hud = KProgressHUD.create(ViewEntertainmentImageActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setLabel("Preparing...")
                .setCancellable(false)
                .show();

        JSONObject params = new JSONObject();
        try {
            params.put("video", videoUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.WATERMARK_VIDEO, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(ViewEntertainmentImageActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ViewEntertainmentImageActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        if(object.getBoolean("result")){
                            waterMarkVideo = object.getString("video_path");
                            String watermarkVideo = UrlEndpoints.MEDIA_BASE_URL + object.getString("video_path");
                            newDownload(watermarkVideo);
                        }else {
                            Utils.showAlert(ViewEntertainmentImageActivity.this, getString(R.string.opps_something_went_wrong));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //hare you can start downloding video
    public void newDownload(String url) {
        final DownloadTask downloadTask = new DownloadTask(ViewEntertainmentImageActivity.this);
        downloadTask.execute(url);
    }

    //Here you can check App Permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                /*if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Logger.d("videoUrl09","allow fragment.....");

                    checkFolder();
                    if(mVideoUrl!=null){
                        downloadVideo(mVideoUrl);
                    }

                } else {
                    //code for deny
                    Logger.d("videoUrl09","deny fragment.....");

                    checkAgain();
                }*/

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Logger.d("videoUrl09", "allow fragment.....");

                    checkFolder();
                    if (mVideoUrl != null) {
                        downloadVideo(mVideoUrl);
                    }
                } else {
                    //code for deny
                    Logger.d("videoUrl09", "deny fragment.....");
                    //checkAgain();
                    checkPermission();
                }

                break;
        }
    }

    //hare you can check folfer whare you want to store download Video
    public void checkFolder() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/";
        File dir = new File(path);
        boolean isDirectoryCreated = dir.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated = dir.mkdir();
        }
        if (isDirectoryCreated) {
            // do something

            Logger.d("Folder", "Already Created");
        }
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    // DownloadTask for downloding video from URL
    public class DownloadTask extends AsyncTask<String, Integer, String> {
        private Context context;
        private PowerManager.WakeLock mWakeLock;
        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                java.net.URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                input = connection.getInputStream();


                String filePath = sUrl[0];
                fileN = filePath.substring(filePath.lastIndexOf("/")+1);
                //fileN = "Afrocamgist_" + UUID.randomUUID().toString().substring(0, 10) + ".mp4";
                File filename = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/", fileN);
                output = new FileOutputStream(filename);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();

            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);

            mProgressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();

            if (result != null){
                Toast.makeText(context, getString(R.string.str_download_err) + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, getString(R.string.str_video_downloaded), Toast.LENGTH_SHORT).show();
                deleteWatermarkVideoInServer();
            }

            MediaScannerConnection.scanFile(ViewEntertainmentImageActivity.this,
                    new String[]{Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/AfrocamgistVideos/" + fileN}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String newpath, Uri newuri) {
                            Logger.i("ExternalStorage", "Scanned " + newpath + ":");
                            Logger.i("ExternalStorage", "-> uri=" + newuri);
                        }
                    });

            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + fileN;
            shareVideoToSocialMedia(filePath);
        }
    }

    //delete watermark video from server
    private void deleteWatermarkVideoInServer(){
        JSONObject params = new JSONObject();
        try {
            params.put("video", waterMarkVideo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.DELETE_WATERMARK_VIDEO;

        Webservices.getData(Webservices.Method.DELETE, params, headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.d("waterMark99","success");
                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(getActivity(), object.getString(Constants.MESSAGE));
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void shareVideoToSocialMedia(String filePath){

        MediaScannerConnection.scanFile(ViewEntertainmentImageActivity.this, new String[] { filePath },

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                        shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, "Afrocamgist");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                        startActivity(Intent.createChooser(shareIntent,
                                "Afrocamgist"));

                    }
                });
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(ViewEntertainmentImageActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                /*if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }*/

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ViewEntertainmentImageActivity.this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Permission necessary");
                alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();

                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void shareVideoLink(String videoUrl, Context context){
        //extraShareTextDialog(videoUrl, context);

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
        i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
        i.putExtra(Intent.EXTRA_TEXT, videoUrl);
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    private void likeAndUnlikePost(Integer postId,boolean like) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id", postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(ViewEntertainmentImageActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    Logger.e("LLLL_Res: ",response);
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(ViewEntertainmentImageActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(ViewEntertainmentImageActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void notifySwaggerTalentPosts() {
        Intent intent = new Intent(NOTIFY_LIKE_SWAGGER_TALENT);
        localBroadcastManager.sendBroadcast(intent);
    }

    private Post getPostDetails() {
        final Post[] post = {new Post()};
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + posts.get(position).getPostId();

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            try {
                Logger.e("LLLLL_JSON: ",response);
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(ViewEntertainmentImageActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    post[0] = new Gson().fromJson(object.getJSONObject(Constants.DATA).toString(), Post.class);
                    runOnUiThread(() -> enterPostAdapter.setNotigy(position, post[0]));
                }

            } catch (Exception e) {
                Utils.showAlert(ViewEntertainmentImageActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
        return post[0];
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (requestCode == 101 && resultCode == RESULT_OK) {
                /*if (data.getSerializableExtra("comments") != null) {
                    runOnUiThread(() -> {
                        ArrayList<Comment> comments1 = (ArrayList<Comment>) data.getSerializableExtra("comments");
                        int commentCount = 0;
                        for (Comment comment : comments1) {
                            commentCount++;
                            if (comment.getSubComments() != null && comment.getSubComments().size() > 0) {
                                commentCount += comment.getSubComments().size();
                            }
                        }
                        comments.setText(commentCount);
                        LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
                        notifyState();
                    });

                }*/
            }
        }


    }

    private void getHashtagNextPage(){
        Intent intent = new Intent(NITIFYDATA);
        localBroadcastManager.sendBroadcast(intent);
    }
    protected void notifyState(){
        LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
        Post post =posts.get(layoutManager.findFirstVisibleItemPosition());
        String post_id =String.valueOf(post.getPostId());
        String commnet_count = comments.getText().toString();
        String like_count =likes_count.getText().toString();
        Intent intent = new Intent(NITIFYSTATE);
        intent.putExtra(Post_Id, post_id);
        intent.putExtra(IsLike, post.getLiked());
        intent.putExtra(Comment_Count, commnet_count);
        intent.putExtra(Like_Count, like_count);
        localBroadcastManager.sendBroadcast(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(mDataReceiver);
    }

    private BroadcastReceiver mDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if (intent.getSerializableExtra("image") != null) {
                images = (ArrayList<String>) intent.getSerializableExtra("image");
            }

            if (intent.getSerializableExtra("post") != null) {
                posts = (ArrayList<Post>) intent.getSerializableExtra("post");
            }
            if(posts !=null && posts.size()>0) {
                enterPostAdapter.notifydata(posts);
            }
            isLoading = false;
        }
    };

    @Override
    public void onPostChecked() {
        if(likes.isChecked()){
            likes.setChecked(false);
        }else {
            likes.setChecked(true);
        }

    }

    @Override
    public void onDialogDismiss(ArrayList<Comment> commentList) {
        if(comments!=null){
            if(commentList!=null){
                int commentCount = 0;
                for (Comment comment : commentList) {
                    commentCount++;
                    if (comment.getSubComments() != null && comment.getSubComments().size() > 0) {
                        commentCount += comment.getSubComments().size();
                    }
                }
                comments.setText(""+commentCount);
                LinearLayoutManager layoutManager = (LinearLayoutManager) imageList.getLayoutManager();
                notifyState();
            }
        }
    }
}
