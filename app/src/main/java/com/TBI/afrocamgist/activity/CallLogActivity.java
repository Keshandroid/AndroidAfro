package com.TBI.afrocamgist.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.ArchivedAdapter;
import com.TBI.afrocamgist.adapters.CallLogAdapter;
import com.TBI.afrocamgist.adapters.NotificationAdapter;
import com.TBI.afrocamgist.afrocalls.RTCActivity;
import com.TBI.afrocamgist.afrocalls.RTCAudioActivity;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.chat.CallLog;
import com.TBI.afrocamgist.model.chat.CallLogDetail;
import com.TBI.afrocamgist.model.chat.Conversation;
import com.TBI.afrocamgist.model.chat.ConversationDetails;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.notification.Notification;
import com.TBI.afrocamgist.model.notification.NotificationData;
import com.TBI.afrocamgist.model.notification.NotificationDetails;
import com.TBI.afrocamgist.model.otheruser.OtherUser;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.Ack;

public class CallLogActivity extends BaseActivity implements ConnectivityListener, CallLogAdapter.OnCallClickListener {

    private KProgressHUD hud;
    private SwipeRefreshLayout swipeContainer;
    private ArrayList<CallLog> callLogList = new ArrayList<>();
    private CallLogAdapter callLogAdapter;
    private Dialog popup;
    FirebaseFirestore db = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log);

        setClickListener();
        initSwipeRefresh();

    }

    @Override
    public void onResume() {
        super.onResume();
        if(Utils.isConnected()){
            //socket call log
            getCallLogSocket();

            //normal call log
            //getCallLog();
        }
    }

    private void initSwipeRefresh() {

        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                //getCallLogSocket();
                getCallLog();
            else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(this, getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            if (hud!=null && hud.isShowing()){
                hud.dismiss();
            }
            Utils.closeAlert();
            //getCallLogSocket();
            getCallLog();
        } else {
            //Utils.showAlert(NotificationActivity.this, "No internet connection available.");
        }
    }

    private void setClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private void getCallLogSocket() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        //Socket call logs
        JSONObject data = new JSONObject();
        try {
            data.put("from_id", LocalStorage.getUserDetails().getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.callLog, data, new Ack() {
                @Override
                public void call(Object... args) {
                    swipeContainer.setRefreshing(false);
                    hud.dismiss();
                    Log.i("SocketEvent", "CallLog with Ack()");
                    //Log.i("SocketEvent", "CallLog List = "+ Arrays.toString(args));// print complete response
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (args[1] != null) {
                                JSONObject jsonObject = (JSONObject) args[1];

                                CallLogDetail callLogDetail = new Gson().fromJson(String.valueOf(jsonObject), CallLogDetail.class);

                                if (callLogDetail != null) {
                                    if (callLogDetail.getCallLogDetail().size() > 0) {
                                        callLogList = new ArrayList<>();
                                        callLogList.addAll(callLogDetail.getCallLogDetail());
                                        setNotificationList();
                                    }
                                }
                            }
                        }
                    });

                }
            });
        }
    }

    private void getCallLog() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        /*JSONObject request = new JSONObject();
        try {
            request.put("voip_token","");
            request.put("to_id",""+toId);
            request.put("calltype",calltype);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

//        Log.d("CALL_RESPONSE_REQ",new GsonBuilder().setPrettyPrinting().create().toJson(request));


        Webservices.getData(Webservices.Method.GET,new ArrayList<>(), headers, UrlEndpoints.NORMAL_CALL_LOG, response -> {
            swipeContainer.setRefreshing(false);
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    Log.d("CALL_RESPONSE",new GsonBuilder().setPrettyPrinting().create().toJson(object));

                    if(object.has(Constants.MESSAGE)){
                        Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                    }else {

                        CallLogDetail callLogDetail = new Gson().fromJson(response, CallLogDetail.class);

                        if (callLogDetail != null) {
                            if (callLogDetail.getCallLogDetail().size() > 0) {
                                callLogList = new ArrayList<>();
                                callLogList.addAll(callLogDetail.getCallLogDetail());
                                setNotificationList();
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }


    private void setNotificationList() {

        Log.d("callLogList",""+new GsonBuilder().setPrettyPrinting().create().toJson(callLogList));

        ListView notifications = findViewById(R.id.call_log_list);
        callLogAdapter = new CallLogAdapter(this,CallLogActivity.this,callLogList);
        notifications.setAdapter(callLogAdapter);

    }

    @Override
    public void onCallClicked(Integer callId, Integer receiverId, String personName, String personProfilePic) {
        showAlert(CallLogActivity.this,callId,receiverId,personName,personProfilePic);
    }

    public void showAlert(Activity activity, Integer callId, Integer receiverId, String personName, String personProfilePic) {

        if (activity!=null && !activity.isFinishing()) {
            popup = new Dialog(activity, R.style.DialogCustom);
            popup.setContentView(R.layout.call_popup);
            popup.setCancelable(true);
            popup.show();

            popup.findViewById(R.id.rlAudioCall).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popup.dismiss();
                    if (LocalStorage.getUserDetails() != null) {
                        if (LocalStorage.getUserDetails().getFirstName() != null && LocalStorage.getUserDetails().getProfileImageUrl() != null) {
                            //initAudioCall(receiverId, personName, personProfilePic, "audio");

                            // normal call log
                            //callIosNotification("audio",receiverId,personName,personProfilePic);

                            //socket call log
                            sendIOSCallNotificationSocket("audio", receiverId, personName,personProfilePic);

                        }
                    }
                }
            });

            popup.findViewById(R.id.rlVideoCall).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popup.dismiss();
                    if (LocalStorage.getUserDetails() != null) {
                        if (LocalStorage.getUserDetails().getFirstName() != null && LocalStorage.getUserDetails().getProfileImageUrl() != null) {


                            //normal call log
                            //callIosNotification("video",receiverId,personName,personProfilePic);

                            //socket call log
                            sendIOSCallNotificationSocket("video", receiverId,personName,personProfilePic);

                        }
                    }
                }
            });
        }
    }

    private void callIosNotification(String calltype, Integer toId, String personName, String personProfilePic){

        String oppositePersonName = LocalStorage.getUserDetails().getFirstName();
        String oppositePersonProfilePic = LocalStorage.getUserDetails().getProfileImageUrl() != null ? UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl() : "";

        JSONObject request = new JSONObject();
        try {
            request.put("voip_token","");
            request.put("to_id",""+toId);
            request.put("calltype",calltype);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Log.d("CALL_RESPONSE_REQ",new GsonBuilder().setPrettyPrinting().create().toJson(request));


        Webservices.getData(Webservices.Method.POST,request, headers, UrlEndpoints.IOS_CALL_NOTIFICATION, response -> {
            if ("".equals(response)) {
                Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    Log.d("CALL_RESPONSE",new GsonBuilder().setPrettyPrinting().create().toJson(object));

                    if(object.has(Constants.MESSAGE)){
                        Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                    }else {

                        int callID = object.getInt("call_id");

                        if(calltype.equalsIgnoreCase("audio")){
                            initAudioCall(toId, personName, personProfilePic, "audio", callID);
                        }else if(calltype.equalsIgnoreCase("video")){
                            initVideoCall(toId, oppositePersonName, oppositePersonProfilePic, "video", callID);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void initVideoCall(Integer userId, String oppositePersonName, String oppositePersonProfilePic, String callType, int callId) {
        if (userId != null) {
            db.collection("calls").document("" + userId).get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {

                            if (documentSnapshot.get("type") == "OFFER" || documentSnapshot.get("type") == "offer" ||
                                    documentSnapshot.get("type") == "ANSWER" || documentSnapshot.get("type") == "answer" ||
                                    documentSnapshot.get("type") == "END_CALL" || documentSnapshot.get("type") == "end_call") {

                                Toast.makeText(CallLogActivity.this, "Line is Busy", Toast.LENGTH_SHORT).show();

                            } else {
                                Intent intent = new Intent(CallLogActivity.this, RTCActivity.class);
                                intent.putExtra("meetingID", "" + userId);
                                intent.putExtra("oppositePersonName", oppositePersonName);
                                intent.putExtra("oppositePersonProfilePic", oppositePersonProfilePic);
                                intent.putExtra("isJoin", false);
                                intent.putExtra("callType", callType);
                                intent.putExtra("call_id", ""+callId);
                                startActivity(intent);
                            }

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("ChatActivity", "calling exception : " + e.getLocalizedMessage());
                        }
                    });
        }
    }

    private void initAudioCall(Integer userId, String personName, String personProfilePic, String callType, int callId) {
        if (userId != null) {
            db.collection("calls").document("" + userId).get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {

                            if (documentSnapshot.get("type") == "OFFER" || documentSnapshot.get("type") == "offer" ||
                                    documentSnapshot.get("type") == "ANSWER" || documentSnapshot.get("type") == "answer" ||
                                    documentSnapshot.get("type") == "END_CALL" || documentSnapshot.get("type") == "end_call") {

                                Toast.makeText(CallLogActivity.this, "User is having call with someone else", Toast.LENGTH_SHORT).show();

                            } else {

                                String myName = LocalStorage.getUserDetails().getFirstName();
                                String myProfilePic = LocalStorage.getUserDetails().getProfileImageUrl() != null ? UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl() : "";


                                Intent intent = new Intent(CallLogActivity.this, RTCAudioActivity.class);
                                intent.putExtra("meetingID", "" + userId);
                                intent.putExtra("oppositePersonName", myName);
                                intent.putExtra("oppositePersonProfilePic", myProfilePic);
                                intent.putExtra("isJoin", false);
                                intent.putExtra("callType", callType);
                                intent.putExtra("userName", personName);
                                intent.putExtra("userImage", personProfilePic);
                                intent.putExtra("call_id", ""+callId);
                                startActivity(intent);
                            }


                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("ChatActivity", "calling exception : " + e.getLocalizedMessage());
                        }
                    });
        }
    }

    private void sendIOSCallNotificationSocket(String calltype, Integer toId, String personName, String personProfilePic) {


        String oppositePersonName = LocalStorage.getUserDetails().getFirstName();
        String oppositePersonProfilePic = LocalStorage.getUserDetails().getProfileImageUrl() != null ? UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl() : "";

        JSONObject data = new JSONObject();
        try {
            data.put("from_id", LocalStorage.getUserDetails().getUserId());
            data.put("to_id", toId);
            data.put("calltype", calltype);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("SocketEvent", "REQ : " + new GsonBuilder().setPrettyPrinting().create().toJson(data));

        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.emit(UrlEndpoints.callNotification, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.i("SocketEvent", "sendIOSCallNotification with Ack()");
                    Log.i("SocketEvent", "CallNotification List = " + Arrays.toString(args));// print complete response
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (args[1] != null) {
                                try {
                                    JSONObject jsonObject = (JSONObject) args[1];
                                    int callID = jsonObject.getInt("call_id");

                                    if(calltype.equalsIgnoreCase("audio")){
                                        initAudioCall(toId, personName, personProfilePic, "audio", callID);
                                    }else if(calltype.equalsIgnoreCase("video")){
                                        initVideoCall(toId, oppositePersonName, oppositePersonProfilePic, "video", callID);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });

                }
            });
        }
    }

}
