package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.MyRoleModelAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.rolemodel.RoleModel;
import com.TBI.afrocamgist.model.rolemodel.RoleModelDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyRoleModelsActivity extends BaseActivity implements MyRoleModelAdapter.OnRoleModelClickListener {

    private KProgressHUD hud;
    private TextView roleModelCount;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_role_model);

        initView();
        initSwipeRefresh();
        setClickListener();

        if (Utils.isConnected())
            getRoleModelList();
        else
            Utils.showAlert(this,getString(R.string.please_check_your_internet_connection));
    }

    private void initView() {

        roleModelCount = findViewById(R.id.role_model_count);
        swipeContainer = findViewById(R.id.swipeContainer);
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getRoleModelList();
            else
                Utils.showAlert(this,getString(R.string.please_check_your_internet_connection));
        });
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private void getRoleModelList() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.ROLE_MODELS, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                swipeContainer.setRefreshing(false);
                hud.dismiss();
                if ("".equals(response)) {
                    Utils.showAlert(MyRoleModelsActivity.this, getString(R.string.opps_something_went_wrong));
                } else {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(MyRoleModelsActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            RoleModelDetails roleModelDetails = new Gson().fromJson(response,RoleModelDetails.class);

                            Logger.d("roleModelList1", new GsonBuilder().setPrettyPrinting().create().toJson(roleModelDetails));
                            Logger.d("roleModelList1", " SIZE : "+roleModelDetails.getRoleModels().size());

                            String count = roleModelDetails.getRoleModels()== null ? "0" : roleModelDetails.getRoleModels().size() + "";
                            roleModelCount.setText(count);
                            if (roleModelDetails.getRoleModels()==null || roleModelDetails.getRoleModels().isEmpty()) {
                                findViewById(R.id.no_role_model).setVisibility(View.VISIBLE);
                                findViewById(R.id.role_model_list).setVisibility(View.GONE);
                            } else {
                                ArrayList<RoleModel> roleModelList = roleModelDetails.getRoleModels();
                                setRecyclerView(roleModelList);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showAlert(MyRoleModelsActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            }
        });
    }

    private void setRecyclerView(ArrayList<RoleModel> roleModels) {

        RecyclerView roleModelList = findViewById(R.id.role_model_list);
        roleModelList.setLayoutManager(new LinearLayoutManager(this));
        roleModelList.setAdapter(new MyRoleModelAdapter(roleModels, this));
    }

    @Override
    public void onRemoveRoleModelClick(RoleModel roleModel) {
        removeRoleModel(roleModel.getUserId());
    }

    private void removeRoleModel(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/cancel-friend";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(MyRoleModelsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        getRoleModelList();
                    }

                } catch (Exception e) {
                    Utils.showAlert(MyRoleModelsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRoleModelClick(RoleModel roleModel) {
        startActivity(new Intent(MyRoleModelsActivity.this, ProfileActivity.class).putExtra("userId",roleModel.getUserId()));
    }
}
