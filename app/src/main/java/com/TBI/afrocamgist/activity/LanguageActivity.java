package com.TBI.afrocamgist.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.TBI.afrocamgist.LocaleManager;
import com.TBI.afrocamgist.R;

public class LanguageActivity extends BaseActivity {

    Dialog dialog;
    private CardView cardLanguage;
    private TextView languageText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        cardLanguage = findViewById(R.id.cardLanguage);
        languageText = findViewById(R.id.languageText);

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());

        String languageName = LocaleManager.getLanguagePref(this);
        if(languageName.equalsIgnoreCase("en")){
            languageText.setText("English");
        }else if(languageName.equalsIgnoreCase("fr")){
            languageText.setText("French");
        }else if(languageName.equalsIgnoreCase("ar")){
            languageText.setText("Arabic");
        }else if(languageName.equalsIgnoreCase("es")){
            languageText.setText("Spanish");
        }else if(languageName.equalsIgnoreCase("pt")){
            languageText.setText("Portuguese");
        }


        cardLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLanguageSelectionDialog();
            }
        });

    }

    private void openLanguageSelectionDialog() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_language_selection);
        dialog.setTitle("");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        RadioGroup radioGroup = dialog.findViewById(R.id.radioGroup);
        RadioButton radioEnglish = (RadioButton) dialog.findViewById(R.id.radioEnglish);
        RadioButton radioFrench = (RadioButton) dialog.findViewById(R.id.radioFrench);
        RadioButton radioArabic = (RadioButton) dialog.findViewById(R.id.radioArabic);
        RadioButton radioSpanish = (RadioButton) dialog.findViewById(R.id.radioSpanish);
        RadioButton radioPortuguese = (RadioButton) dialog.findViewById(R.id.radioPortuguese);

        String languageName = LocaleManager.getLanguagePref(this);
        if(languageName.equalsIgnoreCase("en")){
            radioEnglish.setChecked(true);
        }else if(languageName.equalsIgnoreCase("fr")){
            radioFrench.setChecked(true);
        }else if(languageName.equalsIgnoreCase("ar")){
            radioArabic.setChecked(true);
        }else if(languageName.equalsIgnoreCase("es")){
            radioSpanish.setChecked(true);
        }else if(languageName.equalsIgnoreCase("pt")){
            radioPortuguese.setChecked(true);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                String selectedLanguage = checkedRadioButton.getText().toString();

                if(selectedLanguage.equalsIgnoreCase("English")){
                    setNewLocale(LanguageActivity.this, LocaleManager.ENGLISH);
                }else if(selectedLanguage.equalsIgnoreCase("French")){
                    setNewLocale(LanguageActivity.this, LocaleManager.FRENCH);
                }else if(selectedLanguage.equalsIgnoreCase("Arabic")){
                    setNewLocale(LanguageActivity.this, LocaleManager.ARABIC);
                }else if(selectedLanguage.equalsIgnoreCase("Spanish")){
                    setNewLocale(LanguageActivity.this, LocaleManager.SPANISH);
                }else if(selectedLanguage.equalsIgnoreCase("Portuguese")){
                    setNewLocale(LanguageActivity.this, LocaleManager.PORTUGUESE);
                }

                dialog.dismiss();

                //Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

            }
        });

        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setNewLocale(AppCompatActivity mContext, @LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(this, language);
        sendBroadcast(new Intent("Language.changed"));

        /*Intent intent = new Intent(this, SplashActivity.class);
        this.startActivity(intent);
        this.finishAffinity();*/
//        Intent intent = mContext.getIntent();
//        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

    }
    
}
