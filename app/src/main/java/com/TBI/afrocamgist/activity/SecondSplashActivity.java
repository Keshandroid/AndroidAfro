package com.TBI.afrocamgist.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.TBI.afrocamgist.R;

public class SecondSplashActivity extends AppCompatActivity {

    private static final int SPLASH_DISPLAY_LENGTH = 7000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_splash);

        new Handler().postDelayed(() -> {
            //Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            //Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
            Intent intent = new Intent(SecondSplashActivity.this, UsernameActivity.class);
            startActivity(intent);
            finish();
        },SPLASH_DISPLAY_LENGTH);
    }
}