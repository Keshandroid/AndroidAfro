package com.TBI.afrocamgist.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.PopularPostAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import im.ene.toro.widget.Container;

public class PopularPostsActivity extends BaseActivity implements PopularPostAdapter.OnPostClickListener {

    private ArrayList<Post> popularPosts = new ArrayList<>();
    private PopularPostAdapter popularPostAdapter;
    private static int SAVED_SCROLL_POSITION = -1;
    protected LocalBroadcastManager localBroadcastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_popular_posts);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(mFollowStateReceiver,
                new IntentFilter(ProfileActivity.NITIFY_FOLLOW_STATE));
        
        if (getIntent().getSerializableExtra("posts")!=null)
            popularPosts = (ArrayList<Post>) getIntent().getSerializableExtra("posts");

        setPopularPosts();
        setClickListener();
    }

    private BroadcastReceiver mFollowStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int user_id = intent.getIntExtra(ProfileActivity.User_Id,0);
            boolean isfollow = intent.getBooleanExtra(ProfileActivity.IsFollow,false);
            setFollowAllUser(user_id,isfollow);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(mFollowStateReceiver);
    }

    private void setFollowAllUser(int userid, boolean isfollow){
        try {
            for (int i = 0; i< popularPosts.size();i++){
                if(popularPosts.get(i).getUserId() !=null && userid == popularPosts.get(i).getUserId()){
                    /*popularPosts.get(i).setFollowing(isfollow);
                    popularPostAdapter.notifyItemChanged(i);*/

                    //edited
                    if(popularPosts.get(i).getPrivateAccount()){

                        if(isfollow){
                            popularPosts.get(i).getRequestButtons().get(0).setButtonText("Requested");
                            popularPosts.get(i).setFollowing(false);
                        }else {
                            popularPosts.get(i).getRequestButtons().get(0).setButtonText("Request");
                            popularPosts.get(i).setFollowing(false);
                        }

                    }else {
                        popularPosts.get(i).setFollowing(isfollow);
                    }
                    //edited
                    popularPostAdapter.notifyItemChanged(i);

                }
            }
            popularPostAdapter.notifyDataSetChanged();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setPopularPosts() {

        Container popularPostList = findViewById(R.id.popular_post_list);
        int position = getIntent().getIntExtra("position",0);
        popularPostList.setLayoutManager(new LinearLayoutManager(this));
        popularPostAdapter = new PopularPostAdapter(this,popularPosts,this);
        popularPostList.setAdapter(popularPostAdapter);

        if (popularPostList.getLayoutManager()!=null)
            popularPostList.getLayoutManager().scrollToPosition(position);
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getAfroPopularPosts() {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MOST_POPULAR, response -> {
            if ("".equals(response)) {
                Utils.showAlert(PopularPostsActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(PopularPostsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails popularPostsDetails = new Gson().fromJson(response, PostDetails.class);

                        if (popularPostAdapter!=null && popularPostsDetails.getPosts()!=null) {
                            popularPosts = popularPostsDetails.getPosts();
                            popularPostAdapter.notifyDataSetChanged();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(PopularPostsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    @Override
    public void onDeletePost(Post post) {
        showConfirmDeletePostAlert(post);
    }

    private void showConfirmDeletePostAlert(Post post) {

        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (Utils.isConnected())
                    deletePost(post.getPostId());
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void deletePost(Integer postId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId;

        Webservices.getData(Webservices.Method.DELETE, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(PopularPostsActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        Toast.makeText(PopularPostsActivity.this, getString(R.string.post_deleted_successfully), Toast.LENGTH_LONG).show();
                        getAfroPopularPosts();
                    }

                } catch (Exception e) {
                    Utils.showAlert(PopularPostsActivity.this, getString(R.string.opps_something_went_wrong));
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onPostChecked(Post post) {
        likeAndUnlikePost(post.getPostId());
    }

    private void likeAndUnlikePost(Integer postId) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id",postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(PopularPostsActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(PopularPostsActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(PopularPostsActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    public void onFollowClicked(Post post) {
        followUser(post.getUserId());
        setFollowAllUser(post.getUserId(),true);
    }

    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {

            if (data.getSerializableExtra("comments")!=null) {
                updateCommentCount((ArrayList<Comment>) data.getSerializableExtra("comments"));
            }
        } else if (requestCode == 102 && resultCode == RESULT_OK) {

            if (data.getStringExtra("postText")!=null) {
                updatePostText(data.getStringExtra("postText"));
            }
        }
    }

    private void updatePostText(String postText) {
        if (popularPosts.size() > 0) {
            popularPosts.get(SAVED_SCROLL_POSITION).setPostText(postText);

            if (popularPostAdapter!=null) {
                popularPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }

        }
    }

    @Override
    public void onEditPost(Post post, int position) {

        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(this, EditPostActivity.class)
                .putExtra("post", post), 102);
    }

    private void updateCommentCount(ArrayList<Comment> comments) {

        if (popularPosts.size() > 0) {
            int commentCount = 0;
            for(Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments()!=null && comment.getSubComments().size() > 0){
                    commentCount += comment.getSubComments().size();
                }
            }
            popularPosts.get(SAVED_SCROLL_POSITION).setComments(comments);
            popularPosts.get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);

            if (popularPostAdapter!=null) {
                popularPostAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }
        }
    }

    @Override
    public void onCommentClicked(Post post, int position) {

        SAVED_SCROLL_POSITION = position;

        startActivityForResult(new Intent(this, CommentsActivity.class)
                .putExtra("comments",post.getComments())
                .putExtra("postId",post.getPostId()),101);
    }
}
