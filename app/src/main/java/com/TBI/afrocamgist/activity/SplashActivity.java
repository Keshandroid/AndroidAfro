package com.TBI.afrocamgist.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.TBI.afrocamgist.AESHelper;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.groups.Group;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.connection.Connectivity;
import com.TBI.afrocamgist.model.user.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import id.zelory.compressor.Compressor;

public class SplashActivity extends BaseActivity implements ConnectivityListener {

    private Connectivity mConnectivity;
    private static final int SPLASH_DISPLAY_LENGTH = 5000;
    public static boolean isNotification = false;

    private Dialog popup;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.activity_splash);

        printHashKey();
        Intent intent1 = getIntent();
        if (intent1 != null && intent1.getExtras() != null) {
            Bundle extras = intent1.getExtras();

            if (extras!=null) {

                JSONObject jsonObject = null;
                try {
                    if (extras.getString("data") != null) {
                        isNotification = true;
                        jsonObject = new JSONObject(Objects.requireNonNull(extras.getString("data")));
                        Logger.e("LLLLLL_Data_Forground: ", jsonObject.toString());
                        if (jsonObject.getString("notification_type").equals("post_like")) {
                            //Direct Oprn Single post page
                            startActivity(new Intent(SplashActivity.this, ViewPostActivity.class)
                                    .putExtra("postId", jsonObject.getInt("post_id")));

                            //open HomeScreen and than open single post page
                            /*startActivity(new Intent(SplashActivity.this, DashboardActivity.class)
                                    .putExtra("postId", jsonObject.getInt("post_id")));*/
                            finish();
                        } else if (jsonObject.getString("notification_type").equals("post_comment")) {
                            startActivity(new Intent(SplashActivity.this, ViewPostActivity.class)
                                    .putExtra("postId", jsonObject.getInt("post_id")));
                            /*startActivity(new Intent(SplashActivity.this, DashboardActivity.class)
                                    .putExtra("postId", jsonObject.getInt("post_id")));*/
                            finish();
                        } else if (jsonObject.getString("notification_type").equals("add_group_post")) {
                            startActivity(new Intent(SplashActivity.this, DashboardActivity.class)
                                    .putExtra("postId", jsonObject.getInt("post_id")));
                            finish();
                        }else if (jsonObject.getString("notification_type").equals("group_invite_accept")) {
                            if (jsonObject.getInt("group_id")!= 0) {
                                Group group = new Group();
                                group.setGroupId(jsonObject.getInt("group_id"));
                                Intent intent =new Intent(SplashActivity.this, AfroViewGroupPostActivity.class);
                                intent.putExtra("group", group);
                                intent.putExtra("notification_type", jsonObject.get("notification_type").toString());
                                startActivity(intent);
                                finish();
                            }
                        }else if (jsonObject.getString("notification_type").equals("group_post_report")) {
                            startActivity(new Intent(SplashActivity.this, DashboardActivity.class)
                                    .putExtra("postId", jsonObject.getInt("post_id")));
                            finish();
                        } else if (jsonObject.getString("notification_type").equals("share_post")) {
                            startActivity(new Intent(SplashActivity.this, DashboardActivity.class)
                                    .putExtra("postId", jsonObject.getInt("post_id")));
                            finish();
                        }else if (jsonObject.getString("notification_type").equals("Promo")) {
                            startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                            finish();
                        } else if (jsonObject.get("notification_type").equals("message")) {
                            JSONObject jsonObject1 = new JSONObject(String.valueOf(Objects.requireNonNull(jsonObject.getJSONObject("user_info"))));
                            Logger.d("jsonObjectUser", new GsonBuilder().setPrettyPrinting().create().toJson(jsonObject1));

                            User user = new User();

                            user.setFirstName(jsonObject1.getString("first_name"));
                            user.setLastName(jsonObject1.getString("last_name"));
                            user.setUserId(jsonObject1.getInt("user_id"));
                            user.setProfileImageUrl(jsonObject1.getString("profile_image_url"));
                            user.setBlockedByMe(jsonObject1.getBoolean("blocked_by_me"));

                            //startActivity(new Intent(SplashActivity.this, ChatActivity.class).putExtra("user", user));

                            startActivity(new Intent(SplashActivity.this, DashboardActivity.class).putExtra("user", user));

                            finish();
                        } else if (jsonObject.get("notification_type").equals("follow_user")) {
                            startActivity(new Intent(SplashActivity.this, ProfileActivity.class)
                                    .putExtra("userId", jsonObject.getInt("user_id")));
                            /*startActivity(new Intent(SplashActivity.this, DashboardActivity.class)
                                    .putExtra("userId", jsonObject.getInt("user_id")));*/
                            finish();
                        }else if(jsonObject.get("notification_type").equals("send_follow_request")){
                            startActivity(new Intent(SplashActivity.this, ProfileActivity.class)
                                    .putExtra("userId", jsonObject.getInt("user_id")));
                            /*startActivity(new Intent(SplashActivity.this, DashboardActivity.class)
                                    .putExtra("userId", jsonObject.getInt("user_id")));*/
                            finish();
                        } else if(jsonObject.get("notification_type").equals(Constants.GROUP_INVITE)){
                            if (jsonObject.getInt("group_id")!= 0) {
                                Group group = new Group();
                                group.setGroupId(jsonObject.getInt("group_id"));
                                Intent intent = new Intent(SplashActivity.this, AfroViewGroupPostActivity.class);
                                intent.putExtra("group", group);
                                intent.putExtra("notification_type", jsonObject.get("notification_type").toString());
                                startActivity(intent);
                                finish();
                            }
                        }else {
                            if (Utils.isConnected()) {
                                if (LocalStorage.getIsUserLoggedIn()) {
                                    if ("email".equals(LocalStorage.getUserDetails().getRegisteredWith())) {
                                        if(LocalStorage.getUserDetails().getIntroduced()){
                                            getAfroPopularPosts();
                                        }else{
                                            startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                        }

                                    } else {
                                        if (LocalStorage.getUserDetails().getIntroduced() == null) {
                                            if(LocalStorage.getUserDetails().getIntroduced()){
                                                getAfroPopularPosts();
                                            }else{
                                                startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                            }
                                        } else {
                                            // if (LocalStorage.getIsUserLoggedIn()) {
                                            if(LocalStorage.getUserDetails().getIntroduced()){
                                                getAfroPopularPosts();
                                            }else{
                                                startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                            }

//                                            }else
//                                                startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                        }
                                    }
                                } else {
                                    new Handler().postDelayed(() -> {
                                        //Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                        //Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
                                        Intent intent = new Intent(SplashActivity.this, UsernameActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }, SPLASH_DISPLAY_LENGTH);
                                }
                            } else {
                                initInternetConnectivityCheckListener();
                            }
                        }
                    } else {
                        if (Utils.isConnected()) {
                            if (LocalStorage.getIsUserLoggedIn()) {
                                if ("email".equals(LocalStorage.getUserDetails().getRegisteredWith())) {
                                    if(LocalStorage.getUserDetails().getIntroduced()){
                                        getAfroPopularPosts();
                                    }else{
                                        startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                    }
                                } else {
                                    if (LocalStorage.getUserDetails().getIntroduced() == null) {
                                        if(LocalStorage.getUserDetails().getIntroduced()){
                                            getAfroPopularPosts();
                                        }else{
                                            startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                        }
                                    }else {
                                        if(LocalStorage.getUserDetails().getIntroduced()){
                                            getAfroPopularPosts();
                                        }else{
                                            startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                        }
                                    }
                                }
                            } else {
                                new Handler().postDelayed(() -> {
                                    //Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                    //Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
                                    Intent intent = new Intent(SplashActivity.this, UsernameActivity.class);
                                    startActivity(intent);
                                    finish();
                                }, SPLASH_DISPLAY_LENGTH);
                            }
                        } else {
                            initInternetConnectivityCheckListener();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (Utils.isConnected()) {
                    if (LocalStorage.getIsUserLoggedIn()) {
                        if ("email".equals(LocalStorage.getUserDetails().getRegisteredWith())) {
                            if(LocalStorage.getUserDetails().getIntroduced()){
                                getAfroPopularPosts();
                            }else{
                                startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                            }
                        } else {
                            if (LocalStorage.getUserDetails().getIntroduced() == null) {
                                if(LocalStorage.getUserDetails().getIntroduced()){
                                    getAfroPopularPosts();
                                }else{
                                    startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            }else {
                                if(LocalStorage.getUserDetails().getIntroduced()){
                                    getAfroPopularPosts();
                                }else{
                                    startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            }
                        }
                    } else {
                        new Handler().postDelayed(() -> {
                            //Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                            //Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
                            Intent intent = new Intent(SplashActivity.this, UsernameActivity.class);
                            startActivity(intent);
                            finish();
                        }, SPLASH_DISPLAY_LENGTH);
                    }
                } else {
                    initInternetConnectivityCheckListener();
                }
            }
        } else {
            if (Utils.isConnected()) {
                if (LocalStorage.getIsUserLoggedIn()) {
                    if ("email".equals(LocalStorage.getUserDetails().getRegisteredWith())) {
                        if(LocalStorage.getUserDetails().getIntroduced()){
                            getAfroPopularPosts();
                        }else{
                            startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                        }
                    }else {
                        if (LocalStorage.getUserDetails().getIntroduced() == null) {
                            if(LocalStorage.getUserDetails().getIntroduced()){
                                getAfroPopularPosts();
                            }else{
                                startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                            }
                        }else {
                            if(LocalStorage.getUserDetails().getIntroduced()){
                                getAfroPopularPosts();
                            }else{
                                startActivity(new Intent(SplashActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                            }
                        }
                    }
                } else {
                    new Handler().postDelayed(() -> {
                        //Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        //Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
                        Intent intent = new Intent(SplashActivity.this, UsernameActivity.class);
                        startActivity(intent);
                        finish();
                    }, SPLASH_DISPLAY_LENGTH);
                }
            } else {
                initInternetConnectivityCheckListener();
            }
        }


    }

    private void initInternetConnectivityCheckListener() {

        mConnectivity = Connectivity.getInstance();
        mConnectivity.removeAllInternetConnectivityChangeListeners();
        mConnectivity.addInternetConnectivityListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            Utils.closeAlert();
            if (LocalStorage.getIsUserLoggedIn()){
                getAfroPopularPosts();
            } else {
                new Handler().postDelayed(() -> {
                    //Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    //Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
                    Intent intent = new Intent(SplashActivity.this, UsernameActivity.class);
                    startActivity(intent);
                    finish();
                },SPLASH_DISPLAY_LENGTH);
            }
        } else {
            showAlert(SplashActivity.this, getString(R.string.no_internet_connection_available));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getAfroPopularPosts() {
        Logger.e("LLLL_Bare: ",LocalStorage.getLoginToken());
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MOST_POPULAR, response -> {

            Logger.e("LLLLLL_Data_test=",response);

            if ("".equals(response)) {
                showAlert(SplashActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {

                    //new Popular posts response
                    /*JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SplashActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        if(object.getString(Constants.DATA) != null){

                            String encryptedUserData = object.getString(Constants.DATA);
                            decryptPopularPostsData(encryptedUserData);
                        }
                    }*/

                    //old popular posts response
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        showAlert(SplashActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);


                        //Intent intent = new Intent(SplashActivity.this, BubbleActivity.class);
                        Intent intent = new Intent(SplashActivity.this, DashboardActivity.class);

                        if (postDetails.getPosts() != null && postDetails.getPosts().size() != 0) {
                            LocalStorage.savePopularPosts(postDetails.getPosts());
                            getAndStorePopularPostsImages(postDetails.getPosts());
                            intent.putExtra("posts_bubble",postDetails.getPosts());
                        }

                        startActivity(intent);
                        finish();
                    }

                } catch (Exception e) {
                    Logger.e("LLLL_Data_test: ",e.getMessage());
                    e.printStackTrace();
                    showAlert(SplashActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void decryptPopularPostsData(String encryptedUserData){
        AESHelper aesHelper = new AESHelper();
        String decryptedData = aesHelper.decryptData(encryptedUserData);

        try {
            JSONArray array = new JSONArray(decryptedData);

            PostDetails postDetails = new Gson().fromJson(String.valueOf(array.getJSONObject(0)), PostDetails.class);

            //Intent intent = new Intent(SplashActivity.this, BubbleActivity.class);
            Intent intent = new Intent(SplashActivity.this, DashboardActivity.class);

            if (postDetails.getPosts() != null && postDetails.getPosts().size() != 0) {
                LocalStorage.savePopularPosts(postDetails.getPosts());
                getAndStorePopularPostsImages(postDetails.getPosts());
                intent.putExtra("posts_bubble",postDetails.getPosts());
            }

            startActivity(intent);
            finish();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("My App", "Could not parse malformed JSON: \"" + decryptedData + "\"");
        }

    }

    private void getAndStorePopularPostsImages(ArrayList<Post> posts) {
        try {
            new GetAndStoreImage(posts).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetAndStoreImage extends AsyncTask<Void, Void, ArrayList<String>> {



        private ArrayList<Post> posts;

        GetAndStoreImage(ArrayList<Post> posts) {
            this.posts = posts;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<String> images) {
            super.onPostExecute(images);

            LocalStorage.savePopularPostsImage(images);
        }

        @Override
        protected ArrayList<String> doInBackground(Void... voids) {

            ArrayList<String> drawables = new ArrayList<>();

            for (Post post : posts) {

                String src;

                if ("video".equals(post.getPostType())) {
                    src = UrlEndpoints.MEDIA_BASE_URL + post.getThumbnail() + "?width=200&height=200";
                    BitmapDrawable drawable = getBitmapFromURL(src);
                    if (drawable!=null){
                        if(drawable.getBitmap()!=null){
                            drawables.add(convertDrawableToString(drawable));
                        }
                    } else{
                        drawables.add(null);
                    }

                } else if ("image".equals(post.getPostType())) {
                    src = UrlEndpoints.MEDIA_BASE_URL + post.getPostImage().get(0) + "?width=200&height=200";
                    BitmapDrawable drawable = getBitmapFromURL(src);
                    if (drawable!=null){
                        if(drawable.getBitmap()!=null){
                            drawables.add(convertDrawableToString(drawable));
                        }
                    } else{
                        drawables.add(null);
                    }
                } else {
                    drawables.add(null);
                }
            }

            return drawables;
        }

        private String convertDrawableToString(BitmapDrawable drawable) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            drawable.getBitmap().compress(Bitmap.CompressFormat.PNG, 0, outputStream); //bm is the bitmap object
            byte[] b = outputStream.toByteArray();
            return Base64.encodeToString(b, Base64.DEFAULT);
        }
    }

    public BitmapDrawable getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return new BitmapDrawable(Resources.getSystem(), getCompressedBitmap(BitmapFactory.decodeStream(input)));
        } catch (Exception e) {
            return null;
        }
    }

    private Bitmap getCompressedBitmap(Bitmap image) {

        File imageFile = new File(Environment.getExternalStorageDirectory(), Calendar.getInstance().getTimeInMillis() + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            image.compress(Bitmap.CompressFormat.JPEG, 60, os);
            os.flush();
            os.close();

            Bitmap file =  new Compressor(SplashActivity.this)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setQuality(60)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    /*.setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())*/
                    .compressToBitmap(imageFile);

            imageFile.delete();

            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return image;
        }
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Logger.e("LLLLL", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Logger.e("TAG33", "printHashKey()"+ e);
        } catch (Exception e) {
            Logger.e("TAG33", "printHashKey()"+ e);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mConnectivity != null) {
            mConnectivity.removeAllInternetConnectivityChangeListeners();
        }
    }


    public void showAlert(Activity activity, String message) {

        if (activity != null && !activity.isFinishing()) {
            popup = new Dialog(activity, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_error_exit);
            popup.setCancelable(true);
            popup.show();

            TextView error = popup.findViewById(R.id.message);
            TextView reLaunch = popup.findViewById(R.id.reLaunch);
            error.setText(message);
            popup.findViewById(R.id.ok).setOnClickListener(v ->
                    popup.dismiss());

            reLaunch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popup.dismiss();
                    reLaunchApp(SplashActivity.this);
                }
            });

        }
    }

    public static void reLaunchApp(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(context.getPackageName());
        ComponentName componentName = intent.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
        context.startActivity(mainIntent);
        Runtime.getRuntime().exit(0);
    }

}
