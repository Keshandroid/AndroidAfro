/*
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.TagUser.CommonUtil;
import com.TBI.afrocamgist.TagUser.InstaTag;
import com.TBI.afrocamgist.TagUser.SomeOne;
import com.TBI.afrocamgist.TagUser.SomeOneAdapter;
import com.TBI.afrocamgist.TagUser.SomeOneClickListener;
import com.TBI.afrocamgist.TagUser.TagImageView;
import com.TBI.afrocamgist.TagUser.TagUserModel;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TagPhotoActivity extends BaseActivity implements SomeOneClickListener,
        View.OnClickListener {

    private ArrayList<String> images = new ArrayList<>();
    TagUserModel tagUserModel;
    private String videoPath;

    private InstaTag mInstaTag;
    private String mPhotoToBeTaggedUri;
    private RecyclerView mRecyclerViewSomeOneToBeTagged;
    private LinearLayout mHeaderSomeOneToBeTagged, mHeaderSearchSomeOne;
    private TextView mTapPhotoToTagSomeOneTextView;
    private float mAddTagInX, mAddTagInY;
    private EditText mEditSearchForSomeOne;
    private SomeOneAdapter mSomeOneAdapter;
    private final ArrayList<SomeOne> mSomeOnes = new ArrayList<>();
    private RequestOptions requestOptions =
            new RequestOptions()
                    .placeholder(0)
                    .fallback(0)
                    .centerCrop()
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);

    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_photo);

        if (getIntent().getSerializableExtra("images")!=null) {
            images = (ArrayList<String>) getIntent().getSerializableExtra("images");
            if(images!=null){
                mPhotoToBeTaggedUri = images.get(0);
            }
        }else if(getIntent().getSerializableExtra("video")!=null){
            videoPath = getIntent().getStringExtra("video");
            mPhotoToBeTaggedUri = videoPath;
        }


        //mPhotoToBeTaggedUri = getIntent().getData();


        mInstaTag = findViewById(R.id.insta_tag);
        mInstaTag.setImageToBeTaggedEvent(taggedImageEvent);

        final TextView cancelTextView = findViewById(R.id.cancel);
        final TagImageView doneImageView = findViewById(R.id.done);
        final TagImageView backImageView = findViewById(R.id.get_back);

        mRecyclerViewSomeOneToBeTagged = findViewById(R.id.rv_some_one_to_be_tagged);
        mTapPhotoToTagSomeOneTextView = findViewById(R.id.tap_photo_to_tag_someone);
        mHeaderSomeOneToBeTagged = findViewById(R.id.header_tag_photo);
        mHeaderSearchSomeOne = findViewById(R.id.header_search_someone);
        mEditSearchForSomeOne = findViewById(R.id.search_for_a_person);

        mEditSearchForSomeOne.addTextChangedListener(textWatcher);

        cancelTextView.setOnClickListener(this);
        doneImageView.setOnClickListener(this);
        backImageView.setOnClickListener(this);

        loadImage();

        getAllUserData();


    }

    private void loadImage() {
        Glide
                .with(this)
                .load(mPhotoToBeTaggedUri)
                .apply(requestOptions)
                .thumbnail(0.1f)
                .into(mInstaTag.getTagImageView());
    }

    private void getAllUserData(){

        hud = KProgressHUD.create(TagPhotoActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(false)
                .show();

        Call<TagUserModel> call = BaseServices.getAPI().create(ParameterServices.class).getAllUsers();
        call.enqueue(new Callback<TagUserModel>() {
            @Override
            public void onResponse(@NotNull Call<TagUserModel> call, @NotNull Response<TagUserModel> response) {

                hud.dismiss();

                Logger.d("userData", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
                tagUserModel = response.body();
                if(response.isSuccessful()){
                    if(tagUserModel!=null){
                        if(tagUserModel.getAllUsersData().size()>0) {

                            mEditSearchForSomeOne.setEnabled(true);

                            mSomeOnes.addAll(tagUserModel.getAllUsersData());
                            mSomeOneAdapter = new SomeOneAdapter(mSomeOnes, getBaseContext(), TagPhotoActivity.this);
                            mRecyclerViewSomeOneToBeTagged.setAdapter(mSomeOneAdapter);
                            mRecyclerViewSomeOneToBeTagged.setLayoutManager(new LinearLayoutManager(TagPhotoActivity.this));
                        }else {
                            mEditSearchForSomeOne.setEnabled(false);
                        }
                    }else {
                        mEditSearchForSomeOne.setEnabled(false);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<TagUserModel> call, @NotNull Throwable t) {
                hud.dismiss();
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                CommonUtil.hideKeyboard(this);
                mRecyclerViewSomeOneToBeTagged.scrollToPosition(0);
                mRecyclerViewSomeOneToBeTagged.setVisibility(View.GONE);
                mTapPhotoToTagSomeOneTextView.setVisibility(View.VISIBLE);
                mHeaderSearchSomeOne.setVisibility(View.GONE);
                mHeaderSomeOneToBeTagged.setVisibility(View.VISIBLE);
                break;
            case R.id.done:
                if (mInstaTag.getListOfTagsToBeTagged().isEmpty()) {
                    Toast.makeText(this,
                            getString(R.string.please_tag_atleast_one_user), Toast.LENGTH_SHORT).show();
                }else {

                    Logger.d("TaggedUsers", new GsonBuilder().setPrettyPrinting().create().toJson(mInstaTag.getListOfTagsToBeTagged()));

                    Intent intent = new Intent();
                    intent.putExtra("images",mInstaTag.getListOfTagsToBeTagged());
                    setResult(RESULT_OK,intent);
                    finish();

                    /*ArrayList<TaggedPhoto> taggedPhotoArrayList = InstaTagApplication
                            .getInstance().getTaggedPhotos();
                    taggedPhotoArrayList.add(
                            new TaggedPhoto(
                                    Calendar.getInstance().getTimeInMillis() + "",
                                    mPhotoToBeTaggedUri.toString(),
                                    mInstaTag.getListOfTagsToBeTagged()));
                    InstaTagApplication.getInstance().setTaggedPhotos(taggedPhotoArrayList);*/

                    Toast.makeText(this, "Photo tagged successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            case R.id.get_back:
                finish();
                break;
        }
    }

    private final InstaTag.TaggedImageEvent taggedImageEvent = new InstaTag.TaggedImageEvent() {
        @Override
        public void singleTapConfirmedAndRootIsInTouch(final float x, final float y) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(mInstaTag.getListOfTagsToBeTagged().size()<30){
                        mAddTagInX = x;
                        mAddTagInY = y;
                        mRecyclerViewSomeOneToBeTagged.setVisibility(View.VISIBLE);
                        mHeaderSomeOneToBeTagged.setVisibility(View.GONE);
                        mTapPhotoToTagSomeOneTextView.setVisibility(View.GONE);
                        mHeaderSearchSomeOne.setVisibility(View.VISIBLE);
                    }else {
                        Toast.makeText(TagPhotoActivity.this,"Already tagged maximum people",Toast.LENGTH_SHORT).show();
                    }


                }
            });
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {

        }
    };

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (mEditSearchForSomeOne.getText().toString().trim().equals("")) {
                mSomeOnes.clear();
                if(tagUserModel!=null){
                    mSomeOnes.addAll(tagUserModel.getAllUsersData());
                    mSomeOneAdapter.notifyDataSetChanged();
                }

            } else {
                mSomeOnes.clear();
                if(tagUserModel!=null){
                    mSomeOnes.addAll(getFilteredUser(mEditSearchForSomeOne.getText().toString().trim()));
                    mSomeOneAdapter.notifyDataSetChanged();
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public ArrayList<SomeOne> getFilteredUser(String searchString) {
        ArrayList<SomeOne> filteredUser = new ArrayList<>();
        for (SomeOne someOne : tagUserModel.getAllUsersData()) {
            if (someOne.getFullName().toLowerCase().contains(searchString)){
                filteredUser.add(someOne);
            }
        }
        if (filteredUser.isEmpty()) {

            SomeOne someOne = new SomeOne();
            someOne.setId("");
            someOne.setFirstName("No user Found");
            someOne.setLastName("No user Found");
            someOne.setUser_id(0);
            someOne.setUserName("No user Found");
            someOne.setFullName("No user Found");

            filteredUser.add(someOne);
            //Toast.makeText(getApplicationContext(),"No user Found",Toast.LENGTH_SHORT).show();
        }
        return filteredUser;
    }

    @Override
    public void onSomeOneClicked(final SomeOne someOne, int position) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CommonUtil.hideKeyboard(TagPhotoActivity.this);
                mInstaTag.addTag(mAddTagInX, mAddTagInY, someOne.getFullName(), someOne.getUser_id());
                mRecyclerViewSomeOneToBeTagged.setVisibility(View.GONE);
                mTapPhotoToTagSomeOneTextView.setVisibility(View.VISIBLE);
                mHeaderSearchSomeOne.setVisibility(View.GONE);
                mHeaderSomeOneToBeTagged.setVisibility(View.VISIBLE);
            }
        });
    }
}
