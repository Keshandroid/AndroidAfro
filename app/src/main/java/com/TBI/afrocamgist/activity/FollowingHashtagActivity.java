package com.TBI.afrocamgist.activity;

import android.content.Intent;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.FollowingHashtagAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.hashtag.HashtagDetails;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FollowingHashtagActivity extends BaseActivity implements FollowingHashtagAdapter.OnHashtagClickListener {

    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following_hashtag);

        setClickListener();

        if (Utils.isConnected())
            getFollowingHashtagList();
        else
            Utils.showAlert(this,getString(R.string.please_check_your_internet_connection));
    }

    private void setClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }

    //get following hashtags
    private void getFollowingHashtagList() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.FOLLOWING_HASHTAGS, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(FollowingHashtagActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(FollowingHashtagActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        HashtagDetails hashtagDetails = new Gson().fromJson(response, HashtagDetails.class);

                        if (hashtagDetails.getHashtagList().size() > 0) {
                            setHashtagRecyclerView(hashtagDetails.getHashtagList());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(FollowingHashtagActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setHashtagRecyclerView(ArrayList<Hashtag> hashtagList) {

        RecyclerView followingHashtagList = findViewById(R.id.followingHashtagList);
        followingHashtagList.setLayoutManager(new LinearLayoutManager(this));
        followingHashtagList.setAdapter(new FollowingHashtagAdapter(hashtagList, this));
        followingHashtagList.setNestedScrollingEnabled(false);
    }

    @Override
    public void onFollowOrUnfollowToggleClick(boolean isFollowed, Hashtag hashtag) {

        if (isFollowed)
            followHashtag(hashtag);
        else
            unFollowHashtag(hashtag);

    }

    private void followHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.FOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {}
        });

    }

    private void unFollowHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.UNFOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {}
        });

    }

    @Override
    public void onHashtagClick(Hashtag hashtag) {
        startActivity(new Intent(FollowingHashtagActivity.this, HashtagPostsActivity.class).putExtra("hashtag",hashtag));
    }
}
