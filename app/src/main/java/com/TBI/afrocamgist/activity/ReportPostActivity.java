package com.TBI.afrocamgist.activity;


import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ReportPostActivity extends BaseActivity {

    private EditText reportText;
    private int postId;
    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_post);

        postId = getIntent().getIntExtra("postId",0);
        initView();
        initClickListener();
    }

    private void initView() {

        reportText = findViewById(R.id.report_text);
    }

    private void initClickListener() {

        findViewById(R.id.report).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isConnected()) {
                    if (isValidInput())
                        reportPost();
                } else
                    Utils.showAlert(ReportPostActivity.this, getString(R.string.no_internet_connection_available));
            }
        });

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private boolean isValidInput() {

        if ("".equals(reportText.getText().toString())) {
            Utils.showAlert(ReportPostActivity.this, getString(R.string.please_provide_reason_to_report_the_post));
            return false;
        } else {
            return true;
        }
    }

    private void reportPost() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        JSONObject request = new JSONObject();
        try {
            request.put("report_reason",reportText.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/" + postId + "/report";

        Webservices.getData(Webservices.Method.POST, request,headers, url, response -> {
            hud.dismiss();
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(ReportPostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(ReportPostActivity.this, getString(R.string.post_reported_successfully),Toast.LENGTH_LONG).show();
                    finish();
                }

            } catch (Exception e) {
                Utils.showAlert(ReportPostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }
}
