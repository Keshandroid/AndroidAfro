package com.TBI.afrocamgist.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.ViewImageAdapter;

import java.util.ArrayList;

public class ViewImagesActivity extends BaseActivity implements ViewImageAdapter.OnImageClickListener {

    private ArrayList<String> images = new ArrayList<>();
    private ViewImageAdapter adapter;
    private boolean isItemRemoved = false, isItemEdited = false;
    public static final int FILTERED_IMAGE = 401;
    public static int EDIT_IMAGE_POSITION = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_images);

        if (getIntent().getSerializableExtra("images")!=null) {
            images = (ArrayList<String>) getIntent().getSerializableExtra("images");
        }

        initImagesListView();
        initClickListener();
    }

    private void initImagesListView() {

        RecyclerView imageList = findViewById(R.id.image_list);
        imageList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ViewImageAdapter(images, this);
        imageList.setAdapter(adapter);
    }

    private void initClickListener() {

        findViewById(R.id.done).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==FILTERED_IMAGE && resultCode==RESULT_OK && data!=null) {
            ArrayList<String> mPaths = data.getStringArrayListExtra("imagePath");
            if (mPaths!=null) {
                isItemEdited = true;
                images.set(EDIT_IMAGE_POSITION,mPaths.get(0));
                adapter.notifyItemChanged(EDIT_IMAGE_POSITION);
            }

        }
    }

    @Override
    public void onEditImageClick(int position) {
        EDIT_IMAGE_POSITION = position;
        startActivityForResult(new Intent(this,
                        EditPhotoActivity.class).putExtra("imagePath",images.get(position)),
                FILTERED_IMAGE);
    }

    @Override
    public void onRemoveImageClick(int position) {
        isItemRemoved = true;
        images.remove(position);
        adapter.notifyDataSetChanged();
    }

    private void showConfirmChangesDialog() {

        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_delete);
        popup.setCancelable(true);
        popup.show();

        TextView message = popup.findViewById(R.id.message);
        message.setTextColor(getResources().getColor(R.color.blue));

        String msg = "Confirm your changes?";
        message.setText(msg);

        TextView confirm = popup.findViewById(R.id.delete);
        confirm.setTextColor(getResources().getColor(R.color.blue));
        confirm.setText("Confirm");

        TextView discard = popup.findViewById(R.id.cancel);
        discard.setText("Discard");

        popup.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                Intent intent = new Intent();
                intent.putExtra("images",images);
                setResult(RESULT_OK,intent);
                finish();
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                ViewImagesActivity.super.onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isItemRemoved || isItemEdited) {
            showConfirmChangesDialog();
        } else {
            super.onBackPressed();
        }
    }
}
