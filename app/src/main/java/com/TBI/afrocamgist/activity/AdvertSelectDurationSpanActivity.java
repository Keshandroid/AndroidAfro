package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;

import it.mirko.rangeseekbar.OnRangeSeekBarListener;
import it.mirko.rangeseekbar.RangeSeekBar;

public class AdvertSelectDurationSpanActivity extends AppCompatActivity implements View.OnClickListener {
    SeekBar seekbarDays,seekbarBudget;
    TextView txt_days, txt_dollar;
    int dollar = 1;
    Button btn_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert_select_duration_span);
        initClickListener();
    }

    private void initClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
        txt_days = findViewById(R.id.txt_days);
        txt_dollar = findViewById(R.id.txt_dollar);
        seekbarDays = findViewById(R.id.seekbarDays);
        seekbarBudget = findViewById(R.id.seekbarBudget);
        txt_dollar.setText("$1");
        txt_days.setText("1 days");
        seekbarDays.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    i = i + 1;
                    //txt_dollar.setText("$" + dollar * i);
                    txt_days.setText("" + i + " days");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbarBudget.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    i = i + 1;
                    txt_dollar.setText("$" + dollar * i);
                    //txt_days.setText("" + i + " days");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btn_next = findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btn_next) {


            String days = txt_days.getText().toString().replaceAll("[^0-9]", "");
            String budget = txt_dollar.getText().toString().replaceAll("[^0-9]", "");

            if(Integer.parseInt(budget) < Integer.parseInt(days)){
                Toast.makeText(this,"Your Budget is smaller than Duration",Toast.LENGTH_SHORT).show();
            }else {

                LocalStorage.setBudgetDuration(txt_days.getText().toString().replaceAll("[^0-9]", ""));
                LocalStorage.setBudgetAmount(txt_dollar.getText().toString().replaceAll("[^0-9]", ""));

                Intent intent = new Intent(this, AdvertReviewActivity.class);
                intent.putExtra("dollar",txt_dollar.getText().toString());
                intent.putExtra("days",txt_days.getText().toString());
                startActivity(intent);
            }


        }
    }
}