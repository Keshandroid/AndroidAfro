package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.LocaleManager;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Params;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.user.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsernameActivity extends BaseActivity implements View.OnClickListener {

    private EditText userFirstname;
    private TextView registerHere,loginHere;
    private Button btnNext;
    private KProgressHUD hud;

    Dialog dialog;
    private CardView cardLanguage;
    private TextView languageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_username);

        initView();
        initClickListener();

        // Language Change
        String languageName = LocaleManager.getLanguagePref(this);
        if(languageName.equalsIgnoreCase("en")){
            languageText.setText("English");
        }else if(languageName.equalsIgnoreCase("fr")){
            languageText.setText("French");
        }else if(languageName.equalsIgnoreCase("ar")){
            languageText.setText("Arabic");
        }else if(languageName.equalsIgnoreCase("es")){
            languageText.setText("Spanish");
        }else if(languageName.equalsIgnoreCase("pt")){
            languageText.setText("Portuguese");
        }

        grantAllPermissions();

        if (LocalStorage.getIsUserNotRegisterd()) {
            proceedToNewPopularPost();
        }



    }

    private void grantAllPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WAKE_LOCK,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            grantAllPermissions();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                })
                .withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! " + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void initView() {
        userFirstname = findViewById(R.id.userFirstname);
        btnNext = findViewById(R.id.btnNext);
        loginHere = findViewById(R.id.loginHere);
        registerHere = findViewById(R.id.registerHere);

        cardLanguage = findViewById(R.id.cardLanguage);
        languageText = findViewById(R.id.languageText);
    }

    private void initClickListener() {
        btnNext.setOnClickListener(this);
        registerHere.setOnClickListener(this);
        loginHere.setOnClickListener(this);
        cardLanguage.setOnClickListener(this);
    }

    private void proceedToNewPopularPost(){
        Intent intent = new Intent(UsernameActivity.this, OpenHomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private boolean isValidInput() {

        if ("".equals(userFirstname.getText().toString())) {
            Utils.showAlert(UsernameActivity.this, getString(R.string.enter_first_name));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                if (isValidInput()) {
                    if (Utils.isConnected()) {
                        createID();
                    } else {
                        Utils.showAlert(UsernameActivity.this,getString(R.string.please_check_your_internet_connection));
                    }
                }
                break;
            case R.id.registerHere:
                startActivity(new Intent(UsernameActivity.this, SignUpActivity.class));
                break;
            case R.id.loginHere:
                startActivity(new Intent(UsernameActivity.this, LoginActivity.class));
                break;
            case R.id.cardLanguage:
                openLanguageSelectionDialog();
            default:
                break;
        }
    }

    private void openLanguageSelectionDialog() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_language_selection);
        dialog.setTitle("");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        RadioGroup radioGroup = dialog.findViewById(R.id.radioGroup);
        RadioButton radioEnglish = (RadioButton) dialog.findViewById(R.id.radioEnglish);
        RadioButton radioFrench = (RadioButton) dialog.findViewById(R.id.radioFrench);
        RadioButton radioArabic = (RadioButton) dialog.findViewById(R.id.radioArabic);
        RadioButton radioSpanish = (RadioButton) dialog.findViewById(R.id.radioSpanish);
        RadioButton radioPortuguese = (RadioButton) dialog.findViewById(R.id.radioPortuguese);

        String languageName = LocaleManager.getLanguagePref(this);
        if(languageName.equalsIgnoreCase("en")){
            radioEnglish.setChecked(true);
        }else if(languageName.equalsIgnoreCase("fr")){
            radioFrench.setChecked(true);
        }else if(languageName.equalsIgnoreCase("ar")){
            radioArabic.setChecked(true);
        }else if(languageName.equalsIgnoreCase("es")){
            radioSpanish.setChecked(true);
        }else if(languageName.equalsIgnoreCase("pt")){
            radioPortuguese.setChecked(true);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                String selectedLanguage = checkedRadioButton.getText().toString();

                if(selectedLanguage.equalsIgnoreCase("English")){
                    setNewLocale(UsernameActivity.this, LocaleManager.ENGLISH);
                }else if(selectedLanguage.equalsIgnoreCase("French")){
                    setNewLocale(UsernameActivity.this, LocaleManager.FRENCH);
                }else if(selectedLanguage.equalsIgnoreCase("Arabic")){
                    setNewLocale(UsernameActivity.this, LocaleManager.ARABIC);
                }else if(selectedLanguage.equalsIgnoreCase("Spanish")){
                    setNewLocale(UsernameActivity.this, LocaleManager.SPANISH);
                }else if(selectedLanguage.equalsIgnoreCase("Portuguese")){
                    setNewLocale(UsernameActivity.this, LocaleManager.PORTUGUESE);
                }

                dialog.dismiss();

                //Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

            }
        });

        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setNewLocale(AppCompatActivity mContext, @LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(this, language);
        sendBroadcast(new Intent("Language.changed"));
    }

    private void createID() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        JSONObject params = new JSONObject();
        try {
            params.put("user_name", userFirstname.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        /*Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());*/

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.CREATE_ID, response -> {
            if(hud!=null){
                hud.dismiss();
            }
            if ("".equals(response)) {
                Utils.showAlert(UsernameActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(UsernameActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        Logger.d("NON_REGISTER_USER",""+ new GsonBuilder().setPrettyPrinting().create().toJson(object));
                        LocalStorage.setIsUserNotRegisterd(true);
                        User user = new Gson().fromJson(object.getJSONObject(Constants.USER).toString(), User.class);
                        LocalStorage.saveUserDetails(user);
                        startActivity(new Intent(UsernameActivity.this, OpenHomeActivity.class));
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(getActivity(), "Oops something went wrong....");
                }
            }
        });
    }


}
