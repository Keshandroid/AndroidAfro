package com.TBI.afrocamgist.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.advert.Advert;
import com.TBI.afrocamgist.model.advert.AdvertPost;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdvertExistingAudienceActivity extends BaseActivity {

    RecyclerView recycler_view_target_audience;
    Activity activity = this;
    Advert selectedAudience;
    Button btn_next;
    CardView rlCreateNewAudience;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert_existing_audience);
        findViews();
        initClickListener();



    }

    @Override
    public void onResume() {
        super.onResume();
        getTargetAudience();

    }

    private void findViews() {
        recycler_view_target_audience = findViewById(R.id.recycler_view_target_audience);
        btn_next = findViewById(R.id.btn_next);
        rlCreateNewAudience = findViewById(R.id.rlCreateNewAudience);

        recycler_view_target_audience.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
    }

    private void initClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(AdvertExistingAudienceActivity.this, AdvertSelectTargetAudienceActivity.class);
                intent.putExtra("selectedAudience",selectedAudience);
                intent.putExtra("isCreated",true);

                startActivity(intent);

            }
        });

        rlCreateNewAudience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdvertExistingAudienceActivity.this, AdvertSelectTargetAudienceActivity.class);
                intent.putExtra("isCreated",false);

                startActivity(intent);
            }
        });

    }

    private void setTargetAudienceData(ArrayList<Advert> posts) {
        SelectTargetAdapter adapter = new SelectTargetAdapter(activity, posts);
        recycler_view_target_audience.setAdapter(adapter);
    }

    private void getTargetAudience(){
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.ADVERT_GET_AUDIENCE;

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if ("".equals(response)) {
                //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(AdvertCreateActivity.this, object.getString(Constants.MESSAGE));
                    } else {

                        Logger.d("GetAudience",new GsonBuilder().setPrettyPrinting().create().toJson(response));

                        AdvertPost advertPost = new Gson().fromJson(response, AdvertPost.class);
                        if (advertPost.getPosts().size() > 0) {
                            setTargetAudienceData(advertPost.getPosts());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    public class SelectTargetAdapter extends RecyclerView.Adapter<SelectTargetAdapter.DataObjectHolder> {

        private Activity context;
        private ArrayList<Advert> users;
        int row_index;


        public SelectTargetAdapter(Activity context, ArrayList<Advert> users) {
            this.context = context;
            this.users = users;
            row_index = 0;
        }

        @NonNull
        @Override
        public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_select_target_audience, viewGroup, false);

            return new DataObjectHolder(view);
        }


        @Override
        public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

            Advert user = users.get(position);

            String fullName = user.getName();
            if (fullName != null && !fullName.isEmpty())
                holder.rbTargetAudience.setText(fullName);

            holder.rbTargetAudience.setChecked(holder.getAdapterPosition() == row_index);
            selectedAudience = users.get(row_index);

            holder.rbTargetAudience.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //create_audience_spinner.setSelection(0); // original
                    row_index = holder.getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return users.size();
        }

        class DataObjectHolder extends RecyclerView.ViewHolder {
            RadioButton rbTargetAudience;

            DataObjectHolder(View itemView) {
                super(itemView);
                rbTargetAudience = itemView.findViewById(R.id.rbTargetAudience);
            }
        }
    }

}
