package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.HashtagAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.hashtag.Hashtag;
import com.TBI.afrocamgist.model.hashtag.HashtagDetails;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchHashtagActivity extends BaseActivity implements HashtagAdapter.OnHashtagClickListener {

    private RecyclerView hashtagList;
    private SwipeRefreshLayout swipeContainer;
    private EditText search;
    private int nextPage = 1;
    private boolean isLoading = false;
    private HashtagAdapter hashtagAdapter;
    private String searchQuery = "";
    private ArrayList<Hashtag> hashtags = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_hashtag);

        initView();
        setClickListener();
        initSwipeRefresh();
        initSearchQueryListener();

        if (Utils.isConnected())
            getFirstPageHashTags();
    }

    private void initView() {

        hashtagList = findViewById(R.id.hashtag_list);
        swipeContainer = findViewById(R.id.swipeContainer);
        search = findViewById(R.id.search);
    }

    private void setClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    private void initSwipeRefresh() {

        swipeContainer.setOnRefreshListener(() -> {
            if (Utils.isConnected())
                getFirstPageHashTags();
            else {
                swipeContainer.setRefreshing(false);
                Utils.showAlert(SearchHashtagActivity.this, getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    private void initSearchQueryListener() {

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchQuery = s.toString();
                getFirstPageHashTags();
            }
        });
    }

    private void getFirstPageHashTags() {

        /*hud = KProgressHUD.create(SearchHashtagActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();*/

        String url = UrlEndpoints.HASHTAGS + "?page=1" + "&search=" + searchQuery;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            swipeContainer.setRefreshing(false);
//            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(SearchHashtagActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SearchHashtagActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        HashtagDetails hashtagDetails = new Gson().fromJson(response, HashtagDetails.class);
                        nextPage = hashtagDetails.getNextPage();
                        if (hashtagDetails.getHashtagList().size() > 0) {
                            hashtags = new ArrayList<>();
                            hashtags.addAll(hashtagDetails.getHashtagList());
                            setHashtagRecyclerView();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SearchHashtagActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void setHashtagRecyclerView() {

        hashtagList.setLayoutManager(new GridLayoutManager(SearchHashtagActivity.this,2));
        hashtagAdapter = new HashtagAdapter(hashtags, this);
        hashtagList.setAdapter(hashtagAdapter);
        hashtagList.setNestedScrollingEnabled(false);
        hashtagList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;

                if (recyclerView.getLayoutManager() != null) {
                    visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }

                if (!isLoading && dy > 0 && ((visibleItemCount + pastVisiblesItems) >= totalItemCount)) {
                    isLoading = true;
                    hashtags.add(null);
                    hashtagAdapter.notifyItemInserted(hashtags.size() - 1);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hashtags.remove(hashtags.size() - 1);
                            hashtagAdapter.notifyItemRemoved(hashtags.size());
                            getHashtagNextPage();
                        }
                    }, 1000);
                }
            }
        });
    }

    private void getHashtagNextPage() {

        String url = UrlEndpoints.HASHTAGS + "?page=" + nextPage + "&search=" + searchQuery;

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if (!"".equals(response)) {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SearchHashtagActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        HashtagDetails hashtagDetails = new Gson().fromJson(response, HashtagDetails.class);
                        nextPage = hashtagDetails.getNextPage();
                        hashtags.addAll(hashtagDetails.getHashtagList());
                        hashtagAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(SearchHashtagActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    @Override
    public void onFollowOrUnfollowToggleClick(boolean isFollowed, Hashtag hashtag) {

        if (isFollowed)
            followHashtag(hashtag);
        else
            unFollowHashtag(hashtag);
    }

    @Override
    public void onHashtagClick(Hashtag hashtag) {
        startActivity(new Intent(SearchHashtagActivity.this, HashtagPostsActivity.class).putExtra("hashtag",hashtag));
    }

    private void followHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.FOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {}
        });

    }

    private void unFollowHashtag(Hashtag hashtag) {

        JSONObject request = new JSONObject();
        try {
            request.put(Constants.HASHTAG, hashtag.getHashtagSlug());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.UNFOLLOW_HASHTAG, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {}
        });

    }
}
