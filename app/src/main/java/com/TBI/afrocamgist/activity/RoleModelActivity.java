package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.RoleModelAdapter;
import com.TBI.afrocamgist.model.rolemodel.RoleModel;

import java.util.ArrayList;

public class RoleModelActivity extends BaseActivity implements RoleModelAdapter.OnRoleModelClickListener {

    private ArrayList<RoleModel> roleModels = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_role_model);

        if (getIntent().getSerializableExtra("roleModels")!=null)
            roleModels = (ArrayList<RoleModel>) getIntent().getSerializableExtra("roleModels");

        initView();
        setRecyclerView();
        setClickListener();

        ((SwipeRefreshLayout)findViewById(R.id.swipeContainer)).setRefreshing(false);
        findViewById(R.id.swipeContainer).setEnabled(false);
    }

    private void initView() {

        TextView roleModelCount = findViewById(R.id.role_model_count);
        String count = roleModels== null ? "0" : roleModels.size() + "";
        roleModelCount.setText(count);
    }

    private void setRecyclerView() {

        RecyclerView roleModelList = findViewById(R.id.role_model_list);
        roleModelList.setLayoutManager(new LinearLayoutManager(this));
        roleModelList.setAdapter(new RoleModelAdapter(roleModels, this));
    }

    private void setClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onRoleModelClick(RoleModel roleModel) {
        startActivity(new Intent(RoleModelActivity.this, ProfileActivity.class).putExtra("userId",roleModel.getUserId()));
    }
}
