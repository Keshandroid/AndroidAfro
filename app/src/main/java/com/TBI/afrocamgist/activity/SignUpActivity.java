package com.TBI.afrocamgist.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.LocaleManager;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.TagUser.TagToBeTagged;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Params;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.TBI.afrocamgist.model.user.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hbb20.CountryCodePicker;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private EditText firstName, lastName, username, password, uniqueUserName;
    private KProgressHUD hud;
    private CountryCodePicker countryCodePicker;
    Dialog dialog;
    private CardView cardLanguage;
    private TextView languageText;

    //Google Login
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN =7;
    private static final String TAG = GoogleLoginHomeActivity.class.getSimpleName();

    //fb login
    LoginButton loginButton;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        getWindow().setBackgroundDrawableResource(R.drawable.login_bg);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initView();
        //setData();
        initClickListener();


        //Google Login
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().requestProfile().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //fb login
        boolean loggedOut = AccessToken.getCurrentAccessToken() == null;
        if (!loggedOut) {
            //Picasso.get().load(Profile.getCurrentProfile().getProfilePictureUri(200, 200)).into(imageView);
            Logger.d("TAG", "Username is: " + Profile.getCurrentProfile().getName());
            //Using Graph API
            getUserProfile(AccessToken.getCurrentAccessToken());
        }

        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                //loginResult.getAccessToken();
                //loginResult.getRecentlyDeniedPermissions()
                //loginResult.getRecentlyGrantedPermissions()


                boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                Logger.d("API123", loggedIn + " ??");
                getUserProfile(AccessToken.getCurrentAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });


        // Language Change
        String languageName = LocaleManager.getLanguagePref(this);
        if(languageName.equalsIgnoreCase("en")){
            languageText.setText("English");
        }else if(languageName.equalsIgnoreCase("fr")){
            languageText.setText("French");
        }else if(languageName.equalsIgnoreCase("ar")){
            languageText.setText("Arabic");
        }else if(languageName.equalsIgnoreCase("es")){
            languageText.setText("Spanish");
        }else if(languageName.equalsIgnoreCase("pt")){
            languageText.setText("Portuguese");
        }

    }

    private void setData(){
        if(LocalStorage.getUserDetails()!=null){
            if(LocalStorage.getUserDetails().getFirstName()!=null){
                firstName.setText(LocalStorage.getUserDetails().getFirstName());
            }
            if(LocalStorage.getUserDetails().getLastName()!=null){
                lastName.setText(LocalStorage.getUserDetails().getLastName());
            }
        }
    }

    private void initView() {
        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        //uniqueUserName = findViewById(R.id.uniqueUserName);
        countryCodePicker = findViewById(R.id.countryCodePicker);

        cardLanguage = findViewById(R.id.cardLanguage);
        languageText = findViewById(R.id.languageText);

        loginButton = findViewById(R.id.login_button);

    }

    private void openLanguageSelectionDialog() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_language_selection);
        dialog.setTitle("");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        RadioGroup radioGroup = dialog.findViewById(R.id.radioGroup);
        RadioButton radioEnglish = (RadioButton) dialog.findViewById(R.id.radioEnglish);
        RadioButton radioFrench = (RadioButton) dialog.findViewById(R.id.radioFrench);
        RadioButton radioArabic = (RadioButton) dialog.findViewById(R.id.radioArabic);
        RadioButton radioSpanish = (RadioButton) dialog.findViewById(R.id.radioSpanish);
        RadioButton radioPortuguese = (RadioButton) dialog.findViewById(R.id.radioPortuguese);

        String languageName = LocaleManager.getLanguagePref(this);
        if(languageName.equalsIgnoreCase("en")){
            radioEnglish.setChecked(true);
        }else if(languageName.equalsIgnoreCase("fr")){
            radioFrench.setChecked(true);
        }else if(languageName.equalsIgnoreCase("ar")){
            radioArabic.setChecked(true);
        }else if(languageName.equalsIgnoreCase("es")){
            radioSpanish.setChecked(true);
        }else if(languageName.equalsIgnoreCase("pt")){
            radioPortuguese.setChecked(true);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                String selectedLanguage = checkedRadioButton.getText().toString();

                if(selectedLanguage.equalsIgnoreCase("English")){
                    setNewLocale(SignUpActivity.this, LocaleManager.ENGLISH);
                }else if(selectedLanguage.equalsIgnoreCase("French")){
                    setNewLocale(SignUpActivity.this, LocaleManager.FRENCH);
                }else if(selectedLanguage.equalsIgnoreCase("Arabic")){
                    setNewLocale(SignUpActivity.this, LocaleManager.ARABIC);
                }else if(selectedLanguage.equalsIgnoreCase("Spanish")){
                    setNewLocale(SignUpActivity.this, LocaleManager.SPANISH);
                }else if(selectedLanguage.equalsIgnoreCase("Portuguese")){
                    setNewLocale(SignUpActivity.this, LocaleManager.PORTUGUESE);
                }

                dialog.dismiss();

                //Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

            }
        });

        dialog.show();
    }

    private void setNewLocale(AppCompatActivity mContext, @LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(this, language);

        sendBroadcast(new Intent("Language.changed"));


        /*Intent intent = new Intent(this, SplashActivity.class);
        this.startActivity(intent);
        this.finishAffinity();*/
//        Intent intent = mContext.getIntent();
//        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

    }


    private boolean isValidInput() {

        if ("".equals(firstName.getText().toString())) {
            Utils.showAlert(SignUpActivity.this, getString(R.string.enter_first_name));
            return false;
        } else if ("".equals(lastName.getText().toString())) {
            Utils.showAlert(SignUpActivity.this, getString(R.string.enter_last_name));
            return false;
        } else if ("".equals(username.getText().toString())) {
            Utils.showAlert(SignUpActivity.this, getString(R.string.enter_email_or_mobile));
            return false;
        } else if (!(Patterns.PHONE.matcher(username.getText().toString().trim()).matches()) && !(Patterns.EMAIL_ADDRESS.matcher(username.getText().toString().trim()).matches())) {
            Utils.showAlert(SignUpActivity.this, getString(R.string.str_invalid_email_or_mobile));
            return false;
        } else if ("".equals(password.getText().toString())) {
            Utils.showAlert(SignUpActivity.this, getString(R.string.str_invalid_password));
            return false;
        } else if (password.getText().toString().length() < 6) {
            Utils.showAlert(SignUpActivity.this, getString(R.string.password_must_greater_than_six_character));
            return false;
        } else {
            return true;
        }
    }

    private void initClickListener() {
        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.signUp).setOnClickListener(this);
        findViewById(R.id.cardLanguage).setOnClickListener(this);
        findViewById(R.id.googleSignIn).setOnClickListener(this);
    }

    private void signUpOpenUser() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        String newUserName;
        if (Patterns.PHONE.matcher(username.getText().toString()).matches()){
            /*newUserName = Utils.getCurrentCountryCode(this) + (username.getText().charAt(0) == '0'
                    ? username.getText().toString().substring(1) : username.getText().toString());*/
            newUserName = countryCodePicker.getSelectedCountryCode() + (username.getText().charAt(0) == '0'
                    ? username.getText().toString().substring(1) : username.getText().toString());
        } else{
            newUserName = username.getText().toString().trim();
        }

        try {
            //ArrayList<Params> params = new ArrayList<>();

            JSONObject params = new JSONObject();

            params.put("user_id", LocalStorage.getUserDetails().getUserId());
            params.put(Constants.FIRST_NAME, firstName.getText().toString().trim());
            params.put(Constants.LAST_NAME, lastName.getText().toString().trim());
            params.put(Constants.USER_NAME, newUserName);
            //params.add(new Params(Constants.USER_NAME_UNIQUE, uniqueUserName.getText().toString().trim()));
            params.put(Constants.PASSWORD, password.getText().toString().trim());
            params.put(Constants.FIREBASE_TOKEN, LocalStorage.getToken());
            params.put(Constants.REG_FROM, "app");
            params.put(Constants.IP_ADDRESS, Utils.getIPAddress(true));


            //add device information
            try {
                PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);

                JSONArray deviceArray = new JSONArray();
                deviceArray.put(pi.versionName);
                deviceArray.put( currentVersionNumber(SignUpActivity.this));
                deviceArray.put(Build.MODEL);
                deviceArray.put(Build.VERSION.RELEASE);
                deviceArray.put(Build.BRAND);
                deviceArray.put(Build.DEVICE);
                params.put(Constants.REGISTER_DEVICE_DETAIL, deviceArray);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            Logger.d("register_params",""+params.toString());

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

            Webservices.getData(Webservices.Method.PUT, params, headers, UrlEndpoints.OPEN_REGISTER_ACCOUNT, new OnTaskCompleted() {
                @Override
                public void onResponse(String response) {
                    hud.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);

                        Logger.d("registerResponse", new GsonBuilder().setPrettyPrinting().create().toJson(object));

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(SignUpActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            if ("contact_number".equals(object.getString("registered_with"))) {
                                startActivity(new Intent(SignUpActivity.this, ConfirmOTPActivity.class).putExtra("username", newUserName).putExtra("registered_with","phone"));
                            } else {
                                //showSuccessAlert();
                                startActivity(new Intent(SignUpActivity.this, ConfirmEmailActivity.class).putExtra("username", newUserName).putExtra("registered_with","emailotp"));
                            }
                        }

                    } catch (Exception e) {
                        Logger.e("LLLLL_Signup: ",e.getMessage());
                        Utils.showAlert(SignUpActivity.this, getString(R.string.opps_something_went_wrong));
                        e.printStackTrace();
                    }
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void signUpUser() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        String newUserName;
        if (Patterns.PHONE.matcher(username.getText().toString()).matches()){
            /*newUserName = Utils.getCurrentCountryCode(this) + (username.getText().charAt(0) == '0'
                    ? username.getText().toString().substring(1) : username.getText().toString());*/
            newUserName = countryCodePicker.getSelectedCountryCode() + (username.getText().charAt(0) == '0'
                    ? username.getText().toString().substring(1) : username.getText().toString());
        } else{
            newUserName = username.getText().toString().trim();
        }

        try {
            //ArrayList<Params> params = new ArrayList<>();

            JSONObject params = new JSONObject();

            params.put(Constants.FIRST_NAME, firstName.getText().toString().trim());
            params.put(Constants.LAST_NAME, lastName.getText().toString().trim());
            params.put(Constants.USER_NAME, newUserName);
            //params.add(new Params(Constants.USER_NAME_UNIQUE, uniqueUserName.getText().toString().trim()));
            params.put(Constants.PASSWORD, password.getText().toString().trim());
            params.put(Constants.FIREBASE_TOKEN, LocalStorage.getToken());
            params.put(Constants.IP_ADDRESS, Utils.getIPAddress(true));

            //add device information
            try {
                PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);

                JSONArray deviceArray = new JSONArray();
                deviceArray.put(pi.versionName);
                deviceArray.put( currentVersionNumber(SignUpActivity.this));
                deviceArray.put(Build.MODEL);
                deviceArray.put(Build.VERSION.RELEASE);
                deviceArray.put(Build.BRAND);
                deviceArray.put(Build.DEVICE);
                params.put(Constants.REGISTER_DEVICE_DETAIL, deviceArray);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            Logger.d("register_params",""+params.toString());

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

            Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.SIGN_UP_APP, new OnTaskCompleted() {
                @Override
                public void onResponse(String response) {
                    hud.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);

                        Logger.d("registerResponse", new GsonBuilder().setPrettyPrinting().create().toJson(object));

                        if (object.has(Constants.MESSAGE)) {
                            Utils.showAlert(SignUpActivity.this, object.getString(Constants.MESSAGE));
                        } else {
                            if ("contact_number".equals(object.getString("registered_with"))) {
                                startActivity(new Intent(SignUpActivity.this, ConfirmOTPActivity.class).putExtra("username", newUserName).putExtra("registered_with","phone"));
                            } else {
                                //showSuccessAlert();
                                startActivity(new Intent(SignUpActivity.this, ConfirmEmailActivity.class).putExtra("username", newUserName).putExtra("registered_with","emailotp"));
                            }
                        }

                    } catch (Exception e) {
                        Logger.e("LLLLL_Signup: ",e.getMessage());
                        Utils.showAlert(SignUpActivity.this, getString(R.string.opps_something_went_wrong));
                        e.printStackTrace();
                    }
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String currentVersionNumber(Context a) {
        PackageManager pm = a.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo("com.TBI.afrocamgist",
                    PackageManager.GET_SIGNATURES);
            return pi.versionName
                    + (pi.versionCode > 0 ? " (" + pi.versionCode + ")"
                    : "");
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    private void showSuccessAlert() {
        final Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dialog_error);
        popup.setCancelable(true);
        popup.show();

        TextView error = popup.findViewById(R.id.message);
        //String message = "Success!! \n Please check your email to activate your account.";
        String message = "Please check your INBOX or SPAM box for email verification. Thanks.";

        error.setText(message);

        popup.findViewById(R.id.error).setVisibility(View.GONE);
        popup.findViewById(R.id.success).setVisibility(View.VISIBLE);

        popup.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                finish();
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.login:
                //finish();
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.signUp:
                if (isValidInput()) {
                    if (Utils.isConnected()) {
                        if(LocalStorage.getIsUserNotRegisterd()){
                            signUpOpenUser();
                        }else{
                            signUpUser();
                        }
                    } else {
                        Utils.showAlert(SignUpActivity.this, getString(R.string.please_check_your_internet_connection));
                    }
                }
                break;
            case R.id.cardLanguage:
                openLanguageSelectionDialog();
                break;
            case R.id.googleSignIn:
                signIn();
                break;
            default:
                break;
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        googleLoginAPI(account);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            /*String personPhotoUrl = account.getPhotoUrl().toString();
            Glide.with(getApplicationContext()).load(personPhotoUrl)
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfilePic);*/
            // Signed in successfully, show authenticated UI.
            googleLoginAPI(account);
        } catch (ApiException e) {
            Logger.w(TAG, "signInResult:failed code=" + e.getStatusCode()+"\n"+e.getLocalizedMessage()+"\nMEssg: "+e.getMessage());
            googleLoginAPI(null);
        }
    }

    private void googleLoginAPI(GoogleSignInAccount account) {

        if(account!=null) {
            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();

            if (account.getDisplayName() != null) {
                String[] splitString = account.getDisplayName().split("\\s+");
                Logger.d("first_last", "==" + splitString[0] + "==" + splitString[1]);

                ArrayList<Params> params = new ArrayList<>();
                params.add(new Params("google_id", account.getId()));
                params.add(new Params("first_name", splitString[0]));
                params.add(new Params("last_name", splitString[1]));
                params.add(new Params("email", account.getEmail()));
                //params.add(new Params("username", account.getId()));
                params.add(new Params("type", "google"));
                params.add(new Params(Constants.FIREBASE_TOKEN, LocalStorage.getToken()));

                Logger.d("paramGoogle", "" + params.toString() + "" + params);

                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

                Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LOGIN, response -> {
                    hud.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        Logger.e("LLLLLL_Res: ", response);

                        LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                        //LocalStorage.setUserPass(password.getText().toString().trim());
                        User user = new Gson().fromJson(object.getJSONObject(Constants.USER).toString(), User.class);
                        LocalStorage.saveUserDetails(user);

                        Toast.makeText(SignUpActivity.this, getString(R.string.user_logged_in_successfully), Toast.LENGTH_LONG).show();
                        getAfroPopularPosts();

                        if (user.getContactNumber() != null) {
                            if (!user.getContactNumber().equals("")) {
                                if ("google".equals(user.getRegisteredWith())) {
                                    if (user.getIntroduced()) {
                                        LocalStorage.setIsUserLoggedIn(true);
                                        proceedToDashboard();
                                    } else {
                                        LocalStorage.setIsUserLoggedIn(true);
                                        startActivity(new Intent(SignUpActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                    }
                                } else {
                                    if (LocalStorage.getIsUserLoggedIn()) {
                                        LocalStorage.setIsUserLoggedIn(true);
                                        proceedToDashboard();
                                    } else {
                                        LocalStorage.setIsUserLoggedIn(true);
                                        startActivity(new Intent(SignUpActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                    }
                                }
                            } else {
                                LocalStorage.setIsUserLoggedIn(true);
                                startActivity(new Intent(SignUpActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                            }
                        } else {
                            LocalStorage.setIsUserLoggedIn(true);
                            startActivity(new Intent(SignUpActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                        }


                    } catch (Exception e) {
                        try {
                            JSONObject object = new JSONObject(response);
                            Logger.e("LLLLLL_Data: ", response);
                            LoginManager.getInstance().logOut();
                            googleSignOut();
                            Utils.showAlert(SignUpActivity.this, getString(R.string.please_enter_valid_email_or_password));
                        } catch (JSONException e1) {
                            Utils.showAlert(SignUpActivity.this, getString(R.string.opps_something_went_wrong));
                            e1.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    private void googleSignOut(){
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //updateUI(null);
                    }
                });
    }

    private void fbLoginAPI(String id, String first_name, String last_name, String email) {

        if(id!=null){
            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();


            ArrayList<Params> params = new ArrayList<>();
            params.add(new Params("facebook_id", id));
            params.add(new Params("first_name", first_name));
            params.add(new Params("last_name", last_name));
            params.add(new Params("email", email));
            //params.add(new Params("username", id));
            params.add(new Params("type", "facebook"));
            params.add(new Params(Constants.FIREBASE_TOKEN, LocalStorage.getToken()));

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

            Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LOGIN, response -> {
                hud.dismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    Logger.e("LLLLLL_Res: ",response);

                    LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                    //LocalStorage.setUserPass(password.getText().toString().trim());
                    User user = new Gson().fromJson(object.getJSONObject(Constants.USER).toString(), User.class);
                    LocalStorage.saveUserDetails(user);

                    Toast.makeText(SignUpActivity.this, getString(R.string.user_logged_in_successfully), Toast.LENGTH_LONG).show();
                    getAfroPopularPosts();

                    if(user.getContactNumber()!=null){
                        if (!user.getContactNumber().equals("")) {
                            if ("facebook".equals(user.getRegisteredWith())) {
                                if (user.getIntroduced()) {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    proceedToDashboard();
                                } else {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    startActivity(new Intent(SignUpActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            } else {
                                if (LocalStorage.getIsUserLoggedIn()) {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    proceedToDashboard();
                                } else {
                                    LocalStorage.setIsUserLoggedIn(true);
                                    startActivity(new Intent(SignUpActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                                }
                            }
                        } else {
                            LocalStorage.setIsUserLoggedIn(true);
                            startActivity(new Intent(SignUpActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                        }
                    }else {
                        LocalStorage.setIsUserLoggedIn(true);
                        startActivity(new Intent(SignUpActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                    }

                } catch (Exception e) {
                    try {
                        JSONObject object = new JSONObject(response);
                        Logger.e("LLLLLL_Data: ",response);
                        Utils.showAlert(SignUpActivity.this, getString(R.string.please_enter_valid_email_or_password));
                    } catch (JSONException e1) {
                        Utils.showAlert(SignUpActivity.this, getString(R.string.opps_something_went_wrong));
                        e1.printStackTrace();
                    }
                }
            });
        }
    }

    private void proceedToDashboard() {
        // make bubble screen the main screen
        /*Intent intent = new Intent(SignUpActivity.this, BubbleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();*/
        Intent intent = new Intent(SignUpActivity.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void getAfroPopularPosts() {
        Logger.e("LLLL_Bare: ",LocalStorage.getLoginToken());
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.MOST_POPULAR, response -> {
            if ("".equals(response)) {
                Utils.showAlert(SignUpActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(SignUpActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails postDetails = new Gson().fromJson(response, PostDetails.class);

                        if (postDetails.getPosts() != null && postDetails.getPosts().size() != 0) {
                            LocalStorage.savePopularPosts(postDetails.getPosts());
                        }

                    }

                } catch (Exception e) {
//                    Logger.e("LLLL_Data: ",e.getMessage());
                    e.printStackTrace();
                    Utils.showAlert(SignUpActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }

        });
    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Logger.d("TAG", object.toString());
                        try {

                            if(object.has("email")){
                                //String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                                //Picasso.get().load(image_url).into(imageView);

                                String id = object.getString("id");
                                String first_name = object.getString("first_name");
                                String last_name = object.getString("last_name");
                                String email = object.getString("email");

                                //Logger.d("FB_DATA_1","=="+first_name+"=="+last_name+"=="+email+"=="+id);

                                fbLoginAPI(id,first_name,last_name,email);

                            }else {
                                Toast.makeText(SignUpActivity.this,"Your facebook account doesn't have email",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Logger.d("FbException",""+e.getLocalizedMessage());
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }
    
    
    
}
