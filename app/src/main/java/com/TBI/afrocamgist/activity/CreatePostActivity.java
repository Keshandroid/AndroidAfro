package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Layout;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.TagUser.CommonUtil;
import com.TBI.afrocamgist.TagUser.SomeOne;
import com.TBI.afrocamgist.TagUser.SomeOneAdapter;
import com.TBI.afrocamgist.TagUser.TagToBeTagged;
import com.TBI.afrocamgist.TagUser.TagUserModel;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.adapters.BackgroundImageAdapter;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.background.BackgroundImage;
import com.TBI.afrocamgist.model.background.BackgroundImageDetails;
import com.TBI.afrocamgist.model.media.Media;
import com.TBI.afrocamgist.model.media.MediaDetails;
import com.TBI.afrocamgist.service.LocationService;
import com.TBI.afrocamgist.socialview.Hashtag;
import com.TBI.afrocamgist.socialview.HashtagArrayAdapter;
import com.TBI.afrocamgist.socialview.HashtagPojo;
import com.TBI.afrocamgist.socialview.Mention;
import com.TBI.afrocamgist.socialview.MentionArrayAdapter;
import com.TBI.afrocamgist.socialview.Person;
import com.TBI.afrocamgist.socialview.PersonAdapter;
import com.TBI.afrocamgist.socialview.SocialArrayAdapter;
import com.TBI.afrocamgist.socialview.SocialAutoCompleteTextView;
import com.TBI.afrocamgist.socialview.SocialTextView;
import com.TBI.afrocamgist.socialview.SocialView;
import com.TBI.photofilters.imageprocessors.Filter;
import com.TBI.photofilters.imageprocessors.subfilters.BrightnessSubFilter;
import com.TBI.photofilters.imageprocessors.subfilters.ContrastSubFilter;


import com.abedelazizshe.lightcompressorlibrary.CompressionListener;
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor;
import com.abedelazizshe.lightcompressorlibrary.VideoQuality;
import com.abedelazizshe.lightcompressorlibrary.config.Configuration;
import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.Gson;

import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;
//import com.nagihong.videocompressor.VideoCompressor;
//import com.otaliastudios.transcoder.Transcoder;
//import com.otaliastudios.transcoder.TranscoderListener;
//import com.nagihong.videocompressor.VideoCompressor;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import im.ene.toro.exoplayer.ExoPlayable;
import im.ene.toro.exoplayer.Playable;
import im.ene.toro.exoplayer.ToroExo;
import im.ene.toro.media.VolumeInfo;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

import static net.alhazmy13.mediapicker.Video.VideoPicker.EXTRA_VIDEO_PATH;

public class CreatePostActivity extends BaseActivity implements View.OnClickListener, BackgroundImageAdapter.OnBackgroundImageClickListener {

    private TextView tagUser;
    private SocialAutoCompleteTextView postText;
    private RadioGroup toggleImageType;
    private RadioButton radioButtonImageType;
    private String imageTypeString = "normal", fontFace = "";

    private ImageView backgroundImage, postImage, moreImages;
    private EditText postTextWithBackgroundImage, mapText1, mapText2, mapText3, mapText4, mapText5;
    private TextView imageCount, hashtag, tagPeopleCount, hashtagTextview, mentionTextview, fontTextview;
    private TextView font1, font2, font3, font4, font5;
    private LinearLayout fontLayout;
    private RelativeLayout textlayout, rlImageType;
    private CircleImageView profileImage;
    private PlayerView player;
    private Playable playable;
    private KProgressHUD hud, hudVideoTrim, hudvideo;
    private static final int LOCATION_PERMISSION_INTENT = 102;
    public static final int CAMERA_INTENT = 100;
    public static final int SCREENSHOT_INTENT = 101;
    public static final int UPDATE_IMAGE_INTENT = 300;
    //public static final int TAG_USER_INTENT = 900;
    private static final int PICK_GALLERY_REQUEST = 200;
    public static final int CAMERA_VIEW = 301;
    public static final int FILTERED_IMAGE = 401;
    private ArrayList<String> imagePaths = new ArrayList<>();
    private ArrayList<File> imageFiles = new ArrayList<>();
    //private ArrayList<TagToBeTagged> tagUserList = new ArrayList<>();

    private List<String> tagUserList = new ArrayList<>();
    private String imageSource = "";


    private File videoFile, tempvideo;
    private PostType postType = PostType.TEXT;
    private String postFor = "", hashtagSlug = "", location = "", Path = "";
    private Integer groupId = -1;
    private String selectedBackgroundImageUrl = "";
    private String videoPath = "";
    private ToggleButton volume;
    private final int REQUEST_TAKE_GALLERY_VIDEO = 5555;

    private boolean isNormalPost = true; // remove true when you want to add STORY and POST both

    public static PostType postType1;
    public static String postText1 = "";
    public static String video1 = "";
    public static String location1 = "";
    //public static ArrayList<TagToBeTagged> tagUserList1;
    public static ArrayList<String> tagUserList1;
    public static String postCategory1 = "";
    public static String font_face1 = "";

    private Dialog popup;

    static {
        System.loadLibrary("NativeImageProcessor");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        //directly open camera for image post
        if (getIntent().getStringExtra("openCamera") != null) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                pickImage(CAMERA_VIEW);
            } else {
                Intent intent = new Intent(CreatePostActivity.this, OpenCamera.class);
                intent.putExtra("openCamera", "openCamera");
                startActivityForResult(intent, CAMERA_VIEW);
            }
        }

        initView();

        //identify for which section post need to be posted
        if (getIntent().getStringExtra("postFor") != null) {
            postFor = getIntent().getStringExtra("postFor");

            if (postFor.equalsIgnoreCase("afroswagger")) {
                //showPostSelection(this); // remove comment when you want to add STORY and POST both
            } else {
                isNormalPost = true;
            }
        }

        if (getIntent().getStringExtra("Path") != null) {
            String postStoryFor = getIntent().getStringExtra("postStoryFor");
            String isNormalPostIntent = getIntent().getStringExtra("isNormalPost");

            if (postStoryFor.equalsIgnoreCase("afroswagger")) {
                if (isNormalPostIntent.equalsIgnoreCase("isNormalPost")) {
                    isNormalPost = true;
                }
            }

            showTextAndVideo(getIntent().getStringExtra("Path"));
        }


        groupId = getIntent().getIntExtra("groupId", -1);

        if (getIntent().getStringExtra("hashtag") != null) {
            hashtagSlug = getIntent().getStringExtra("hashtag");
            String hash = "#" + hashtagSlug;
            hashtag.setText(hash);
        }

        //get list of background images from server
        if (Utils.isConnected())
            getBackgroundImages();

        setProfileImage();
        setClickListener();


        // Hashtag and mentions
        setHashtagData();
        setMentionData();


        //permission to access user for post
        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (!hasPermissions(PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, LOCATION_PERMISSION_INTENT);
        } else {
            getCurrentLocation();
        }

    }

    private void setHashtagData() {
        postText.setHashtagEnabled(true);
        postText.setHashtagColor(Color.BLUE);

        callHashtagAPI();
    }

    private void setMentionData() {
        postText.setMentionEnabled(true);
        postText.setMentionColor(Color.MAGENTA);

        getAllUserData();
    }

    private void setHashtagAdapter(ArrayList<HashtagPojo.HashtagAll> hashtagAlls) {
        ArrayAdapter<Hashtag> hashtagArrayAdapter = new HashtagArrayAdapter(this);

        for (int i = 0; i < hashtagAlls.size(); i++) {
            hashtagArrayAdapter.add(new Hashtag(hashtagAlls.get(i).getHashtag_slug()));
        }

        //add hashtag count and hashtag name
        /*hashtagArrayAdapter.addAll(new Hashtag(HASHTAG1),
                new Hashtag(HASHTAG2, HASHTAG2_COUNT),
                new Hashtag(HASHTAG3, HASHTAG3_COUNT));*/

        postText.setHashtagAdapter(hashtagArrayAdapter);
        postText.setHashtagTextChangedListener(new SocialView.OnChangedListener() {
            @Override
            public void onChanged(@NonNull SocialView view, @NonNull CharSequence text) {
                Logger.d("social_detail", text.toString());
            }
        });

    }

    private void callHashtagAPI() {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.ALL_HASHTAGS, response -> {
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(CreatePostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    HashtagPojo details = new Gson().fromJson(response, HashtagPojo.class);
                    if (details.getHashtagAlls() != null && details.getHashtagAlls().size() > 0) {
                        setHashtagAdapter(details.getHashtagAlls());
                    }
                }

            } catch (Exception e) {
                Utils.showAlert(CreatePostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private void getAllUserData() {

        Call<TagUserModel> call = BaseServices.getAPI().create(ParameterServices.class).getAllUsers();
        call.enqueue(new Callback<TagUserModel>() {
            @Override
            public void onResponse(@NotNull Call<TagUserModel> call, @NotNull Response<TagUserModel> response) {

                Logger.d("userData", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
                TagUserModel tagUserModel = response.body();
                if (response.isSuccessful()) {
                    if (tagUserModel != null) {
                        if (tagUserModel.getAllUsersData().size() > 0) {
                            setMentionAdapter(tagUserModel.getAllUsersData());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<TagUserModel> call, @NotNull Throwable t) {
                Logger.d("failure1", t.getLocalizedMessage() + "");
            }
        });
    }

    private void setMentionAdapter(ArrayList<SomeOne> allUsersData) {
        ArrayAdapter<Mention> mentionArrayAdapter = new MentionArrayAdapter(this);


        for (int i = 0; i < allUsersData.size(); i++) {
            if (allUsersData.get(i).getUserName() != null) {
                mentionArrayAdapter.add(new Mention(allUsersData.get(i).getUserName()));
            }
        }

        //add hashtag count and hashtag name
        /*hashtagArrayAdapter.addAll(new Hashtag(HASHTAG1),
                new Hashtag(HASHTAG2, HASHTAG2_COUNT),
                new Hashtag(HASHTAG3, HASHTAG3_COUNT));*/

        postText.setMentionAdapter(mentionArrayAdapter);
        postText.setMentionTextChangedListener(new SocialView.OnChangedListener() {
            @Override
            public void onChanged(@NonNull SocialView view, @NonNull CharSequence text) {
                Logger.d("social_detail", text.toString());
            }
        });


    }

    private void initView() {

        tagUser = findViewById(R.id.tagUser);
        tagPeopleCount = findViewById(R.id.tagPeopleCount);

        font1 = findViewById(R.id.font1);
        font2 = findViewById(R.id.font2);
        font3 = findViewById(R.id.font3);
        font4 = findViewById(R.id.font4);
        font5 = findViewById(R.id.font5);

        rlImageType = findViewById(R.id.rlImageType);
        toggleImageType = findViewById(R.id.toggleImageType);


        backgroundImage = findViewById(R.id.background_image);
        postImage = findViewById(R.id.post_image);
        moreImages = findViewById(R.id.more_images);
        postText = findViewById(R.id.post_text);
        enableScroll(postText);


        postTextWithBackgroundImage = findViewById(R.id.post_text_on_image);
        mapText1 = findViewById(R.id.map_text_1);
        mapText2 = findViewById(R.id.map_text_2);
        mapText3 = findViewById(R.id.map_text_3);
        mapText4 = findViewById(R.id.map_text_4);
        mapText5 = findViewById(R.id.map_text_5);
        imageCount = findViewById(R.id.image_count);
        hashtag = findViewById(R.id.hashtag);
        hashtagTextview = findViewById(R.id.hashtagTextview);
        mentionTextview = findViewById(R.id.mentionTextview);
        fontTextview = findViewById(R.id.fontTextview);
        fontLayout = findViewById(R.id.fontLayout);
        textlayout = findViewById(R.id.textlayout);
        profileImage = findViewById(R.id.profile_image);
        player = findViewById(R.id.player);
        volume = findViewById(R.id.volume);

        //rbNormalPost = findViewById(R.id.rbNormalPost);
        //rbStory = findViewById(R.id.rbStory);
        //radioGroupStoryType = findViewById(R.id.radioGroupStoryType);

    }

    public static void enableScroll(View view) {
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            textView.setMovementMethod(new ScrollingMovementMethod());
        }

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
    }

    public void showPostSelection(Activity activity) {

        if (activity != null && !activity.isFinishing()) {
            popup = new Dialog(activity, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_post_story_selection);
            popup.setCancelable(false);
            popup.show();

            TextView okButton = popup.findViewById(R.id.ok);
            RadioButton rbNormalPost = popup.findViewById(R.id.rbNormalPost);
            RadioButton rbStory = popup.findViewById(R.id.rbStory);

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (rbNormalPost.isChecked()) {
                        isNormalPost = true;
                    } else {
                        isNormalPost = false;
                    }
                    popup.dismiss();
                }
            });
        }
    }

    private void getBackgroundImages() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, UrlEndpoints.BACKGROUND_IMAGES, response -> {
            hud.dismiss();
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(CreatePostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    BackgroundImageDetails details = new Gson().fromJson(response, BackgroundImageDetails.class);

                    if (details.getBackgroundImages() != null && details.getBackgroundImages().size() > 0)
                        setBackgroundImageView(details.getBackgroundImages());
                }

            } catch (Exception e) {
                Utils.showAlert(CreatePostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    //set background images fetch from API on the list view.
    private void setBackgroundImageView(ArrayList<BackgroundImage> backgroundImages) {

        RecyclerView backgroundImageList = findViewById(R.id.background_image_list);
        backgroundImageList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        backgroundImageList.setAdapter(new BackgroundImageAdapter(backgroundImages, this));
    }

    @Override
    public void onBackgroundImageClick(BackgroundImage backgroundImage) {
        clearTagData();
        selectedBackgroundImageUrl = backgroundImage.getPath();
        showTextWithBackgroundImage(UrlEndpoints.MEDIA_BASE_URL + backgroundImage.getPath());
    }

    private void setProfileImage() {

        Glide.with(this)
                .load(UrlEndpoints.MEDIA_BASE_URL + LocalStorage.getUserDetails().getProfileImageUrl())
                .into(profileImage);
    }

    private void setClickListener() {

        toggleImageType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radioBtnID = group.getCheckedRadioButtonId();
                radioButtonImageType = group.findViewById(radioBtnID);
                imageTypeString = radioButtonImageType.getText().toString();

                if (imageTypeString.equalsIgnoreCase("normal")) {
                    startActivity(new Intent(CreatePostActivity.this, ViewCreatePostImageActivity.class)
                            .putExtra("position", 0)
                            .putExtra("imageType", "normal")
                            .putExtra("images", imagePaths));
                } else {
                    startActivity(new Intent(CreatePostActivity.this, ViewCreatePostImageActivity.class)
                            .putExtra("position", 0)
                            .putExtra("imageType", "zoom")
                            .putExtra("images", imagePaths));
                }

            }
        });

        tagUser.setOnClickListener(this);
        hashtagTextview.setOnClickListener(this);
        mentionTextview.setOnClickListener(this);
        fontTextview.setOnClickListener(this);
        font1.setOnClickListener(this);
        font2.setOnClickListener(this);
        font3.setOnClickListener(this);
        font4.setOnClickListener(this);
        font5.setOnClickListener(this);

        findViewById(R.id.no_background).setOnClickListener(this);
        findViewById(R.id.upload).setOnClickListener(this);
        findViewById(R.id.edit_image).setOnClickListener(this);
        findViewById(R.id.remove).setOnClickListener(this);
        findViewById(R.id.more_images_layout).setOnClickListener(this);
        findViewById(R.id.africa).setOnClickListener(this);
        findViewById(R.id.post).setOnClickListener(this);
        findViewById(R.id.no_color).setOnClickListener(this);
        findViewById(R.id.white).setOnClickListener(this);
        findViewById(R.id.yellow).setOnClickListener(this);
        findViewById(R.id.orange).setOnClickListener(this);
        findViewById(R.id.blue).setOnClickListener(this);
        findViewById(R.id.red).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);

        ((ToggleButton) findViewById(R.id.map_view)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    if (isChecked) {
                        showMayLayout();
                        initMapTextListener();
                        clearTagData();
//                        mapText1.setEnabled(false);
                        mapText1.requestFocus();
//                        mapText3.setEnabled(false);
//                        mapText4.setEnabled(false);
//                        mapText5.setEnabled(false);
                    } else {
                        clearTagData();
                        showOnlyText();
                    }
                }
            }
        });

        volume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    unMute();
                else
                    mute();
            }
        });


    }

    //fetch user current location
    private void getCurrentLocation() {

        LocationService locationService = new LocationService(this);

        if (locationService.isGPSEnabled()) {
            if (locationService.canGetLocation()) {
                if (locationService.getLocation() != null) {
                    location = locationService.getLocation().getLatitude() + "," +
                            locationService.getLocation().getLongitude();
                }
            }
        } else {
            locationService.showSettingsAlert();
        }
    }

    //display map layout to create map post
    private void showMayLayout() {

        findViewById(R.id.map_layout).setVisibility(View.VISIBLE);
        //findViewById(R.id.post_text).setVisibility(View.GONE);
        textlayout.setVisibility(View.GONE);
        findViewById(R.id.layout_only_images).setVisibility(View.GONE);
        rlImageType.setVisibility(View.GONE);
        findViewById(R.id.video_layout).setVisibility(View.GONE);
        findViewById(R.id.text_with_image_background).setVisibility(View.GONE);

        postType = PostType.MAP;
    }

    //initialise map text boxes
    private void initMapTextListener() {

        mapText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (getCurrentCursorLine(mapText1) == 3) {

                    int selectionEnd = mapText1.getSelectionEnd();
                    String text = mapText1.getText().toString();
                    if (selectionEnd >= 0) {
                        text = text.substring(0, selectionEnd);
                    }
                    String delimiter = " ";
                    int lastDelimiterPosition = text.lastIndexOf(delimiter);
                    String lastWord = lastDelimiterPosition == -1 ? text :
                            text.substring(lastDelimiterPosition + delimiter.length());

                    if (lastDelimiterPosition == -1) {
                        String last = mapText1.getText().toString();
                        last = last.substring(last.length() - 1);
                        mapText1.getText().delete(mapText1.length() - 1, mapText1.length());
                        mapText3.append(last);
                    } else {
                        mapText1.getText().delete(lastDelimiterPosition, mapText1.length());
                        mapText3.append(lastWord);
                    }

                    mapText3.setEnabled(true);
                    mapText3.requestFocus();
                    String[] words = mapText3.getText().toString().split(" ");
                    if (words.length > 1) {
                        mapText3.setSelection(0);
                    }
                } else {
                    mapText1.setTextSize(22f);
                    mapText1.setLines(3);
                    mapText1.setMaxLines(3);
                    /*if (s.toString().length() < 18 && s.toString().split(" ").length < 4) {
                        mapText1.setTextSize(22f);
                        mapText1.setLines(3);
                        mapText1.setMaxLines(3);
                    } else if (s.toString().length() < 36 && s.toString().split(" ").length < 8) {
                        mapText1.setTextSize(18f);
                        mapText1.setLines(4);
                        mapText1.setMaxLines(4);
                    } else {
                        mapText1.setTextSize(16f);
                        mapText1.setLines(4);
                        mapText1.setMaxLines(4);
                    }*/
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mapText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (getCurrentCursorLine(mapText2) == 2) {

                    int selectionEnd = mapText2.getSelectionEnd();
                    String text = mapText2.getText().toString();
                    if (selectionEnd >= 0) {
                        text = text.substring(0, selectionEnd);
                    }
                    String delimiter = " ";
                    int lastDelimiterPosition = text.lastIndexOf(delimiter);
                    String lastWord = lastDelimiterPosition == -1 ? text :
                            text.substring(lastDelimiterPosition + delimiter.length());

                    if (lastDelimiterPosition == -1) {
                        String last = mapText2.getText().toString();
                        last = last.substring(last.length() - 1);
                        mapText2.getText().delete(mapText2.length() - 1, mapText2.length());
                        mapText3.append(last);
                    } else {
                        mapText2.getText().delete(lastDelimiterPosition, mapText2.length());
                        mapText3.append(lastWord);
                    }

                    mapText3.requestFocus();

                } else if (mapText2.getText().toString().length() == 0) {
                    mapText1.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mapText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (getCurrentCursorLine(mapText3) == 1) {

                    int selectionEnd = mapText3.getSelectionEnd();
                    String text = mapText3.getText().toString();
                    if (selectionEnd >= 0) {
                        text = text.substring(0, selectionEnd);
                    }
                    String delimiter = " ";
                    int lastDelimiterPosition = text.lastIndexOf(delimiter);
                    String lastWord = lastDelimiterPosition == -1 ? text :
                            text.substring(lastDelimiterPosition + delimiter.length());

                    if (lastDelimiterPosition == -1) {
                        String last = mapText3.getText().toString();
                        last = last.substring(last.length() - 1);
                        mapText3.getText().delete(mapText3.length() - 1, mapText3.length());
                        mapText4.append(last);
                    } else {
                        mapText3.getText().delete(lastDelimiterPosition, mapText3.length());
                        mapText4.append(lastWord);
                    }

                    mapText4.setEnabled(true);
                    mapText4.requestFocus();
                    String[] words = mapText4.getText().toString().split(" ");
                    if (words.length > 1) {
                        mapText4.setSelection(0);
                    }

                } /*else if (s.toString().length() == 0) {
                    mapText1.requestFocus();
                    mapText3.setEnabled(false);
                }*/ /*else {
                    if (!isLastMapTextEdited) {
                        if (s.toString().length() < 12 && s.toString().split(" ").length < 4) {
                            mapText3.setTextSize(20f);
                        } else if (s.toString().length() < 25 && s.toString().split(" ").length < 6) {
                            mapText3.setTextSize(18f);
                        } else {
                            mapText3.setTextSize(13f);
                        }
                    }
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mapText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (getCurrentCursorLine(mapText4) == 1) {

                    int selectionEnd = mapText4.getSelectionEnd();
                    String text = mapText4.getText().toString();
                    if (selectionEnd >= 0) {
                        text = text.substring(0, selectionEnd);
                    }
                    String delimiter = " ";
                    int lastDelimiterPosition = text.lastIndexOf(delimiter);
                    String lastWord = lastDelimiterPosition == -1 ? text :
                            text.substring(lastDelimiterPosition + delimiter.length());

                    if (lastDelimiterPosition == -1) {
                        String last = mapText4.getText().toString();
                        last = last.substring(last.length() - 1);
                        mapText4.getText().delete(mapText4.length() - 1, mapText4.length());
                        mapText5.append(last);
                    } else {
                        mapText4.getText().delete(lastDelimiterPosition, mapText4.length());
                        mapText5.append(lastWord);
                    }

                    mapText5.setEnabled(true);
                    mapText5.requestFocus();
                    String[] words = mapText5.getText().toString().split(" ");
                    if (words.length > 1) {
                        mapText5.setSelection(0);
                    }

                } /*else if (s.toString().length() == 0) {
                    mapText3.requestFocus();
                    mapText4.setEnabled(false);
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mapText5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                /*if (getCurrentCursorLine(mapText5) == 5) {

                    String multiLines = mapText1.getText().toString();
                    String[] streets = multiLines.split("\n");

                    String lastMapText = mapText5.getText().toString();
                    ArrayList<String> last = new ArrayList<>(Arrays.asList(lastMapText.split("\n")));

                    if (streets.length != 5) {
                        mapText += mapText3.getText().toString();
                        mapText1.setText(mapText);
                        mapText3.setText(mapText4.getText().toString());
                        mapText4.setText(last.get(0));
                        last.remove(0);

                        StringBuilder builder = new StringBuilder();
                        for(String text : last){
                            builder.append(text);
                            builder.append(" ");
                        }

                        mapText5.setText(builder.toString());

                    }
                } else*/ /*if (s.toString().length() == 0) {
                        mapText4.requestFocus();
                        mapText5.setEnabled(false);
                        isLastMapTextEdited = false;
                }*/ /*else {
                    isLastMapTextEdited = true;
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (postType == PostType.MAP && keyCode == KeyEvent.KEYCODE_DEL) {

            if (mapText5.getText().toString().length() == 0) {
                if (mapText4.getText().toString().length() == 0) {
                    if (mapText3.getText().toString().length() == 0)
                        mapText1.requestFocus();
                    else
                        mapText3.requestFocus();
                } else {
                    mapText4.requestFocus();
                }
            } /*else if (mapText4.getText().toString().length() == 0) {
                mapText3.requestFocus();
            } else if (mapText3.getText().toString().length() == 0){
                mapText1.requestFocus();
            }*/
        }

        return super.onKeyDown(keyCode, event);
    }

    public int getCurrentCursorLine(EditText editText) {
        int selectionStart = Selection.getSelectionStart(editText.getText());
        Layout layout = editText.getLayout();

        if (!(selectionStart == -1)) {
            try {
                return layout.getLineForOffset(selectionStart);
            } catch (Exception e) {
                return -1;
            }
        }

        return -1;
    }

    //display background image to create background image with text post
    private void showTextWithBackgroundImage(String url) {

        //findViewById(R.id.post_text).setVisibility(View.GONE);
        textlayout.setVisibility(View.GONE);
        findViewById(R.id.layout_only_images).setVisibility(View.GONE);
        rlImageType.setVisibility(View.GONE);
        findViewById(R.id.video_layout).setVisibility(View.GONE);
        findViewById(R.id.map_layout).setVisibility(View.GONE);
        findViewById(R.id.text_with_image_background).setVisibility(View.VISIBLE);
        ((ToggleButton) findViewById(R.id.map_view)).setChecked(false);

        postType = PostType.BACKGROUND_IMAGE;

        Glide.with(this)
                .load(url)
                .into(backgroundImage);
    }

    //display only text layout to create text post
    private void showOnlyText() {

        //findViewById(R.id.post_text).setVisibility(View.VISIBLE);
        textlayout.setVisibility(View.VISIBLE);
        findViewById(R.id.layout_only_images).setVisibility(View.GONE);
        rlImageType.setVisibility(View.GONE);
        findViewById(R.id.video_layout).setVisibility(View.GONE);
        findViewById(R.id.map_layout).setVisibility(View.GONE);
        findViewById(R.id.text_with_image_background).setVisibility(View.GONE);
        ((ToggleButton) findViewById(R.id.map_view)).setChecked(false);

        postType = PostType.TEXT;
    }

    private void createImageFile(ArrayList<String> imagePaths) {

        imageFiles = new ArrayList<>();
        for (int i = 0; i < imagePaths.size(); i++) {

            try {
                imageFiles.add(new File(imagePaths.get(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createVideoFile(String videoPath) {

        try {
            videoFile = new File(videoPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showTextAndImage(List<String> imagePaths) {
        if (imagePaths != null && imagePaths.size() > 0) {

            Logger.d("imgURL", "" + imagePaths.get(0));

            //findViewById(R.id.post_text).setVisibility(View.VISIBLE);
            textlayout.setVisibility(View.VISIBLE);
            findViewById(R.id.layout_only_images).setVisibility(View.VISIBLE);
            rlImageType.setVisibility(View.VISIBLE);
            findViewById(R.id.video_layout).setVisibility(View.GONE);
            findViewById(R.id.map_layout).setVisibility(View.GONE);
            findViewById(R.id.text_with_image_background).setVisibility(View.GONE);

            postType = PostType.IMAGE;

            this.imagePaths = new ArrayList<>();
            this.imagePaths.addAll(imagePaths);

            findViewById(R.id.edit_layout).setVisibility(View.GONE);

            Glide.with(this)
                    .load(this.imagePaths.get(0))
                    .into(postImage);

            if (imagePaths.size() > 1) {
                findViewById(R.id.more_images_layout).setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(this.imagePaths.get(1))
                        .into(moreImages);

                if ((imagePaths.size() - 2) == 0)
                    imageCount.setVisibility(View.GONE);
                else {
                    String count = "+" + (this.imagePaths.size() - 1);
                    imageCount.setText(count);
                    imageCount.setVisibility(View.VISIBLE);
                }

            } else {
                findViewById(R.id.more_images_layout).setVisibility(View.GONE);
                findViewById(R.id.edit_layout).setVisibility(View.VISIBLE);
            }

        } else {
            clearTagData();
            showOnlyText();
        }
    }

    private Bitmap createImageFile(Uri uri) {

        try {

            Filter imageFilter = new Filter();
            imageFilter.addSubFilter(new BrightnessSubFilter(30));
            imageFilter.addSubFilter(new ContrastSubFilter(1.1f));

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            return imageFilter.processFilter(bitmap);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void showTextAndVideo(String videoPaths) {

        ArrayList<Uri> videoURIList = new ArrayList<>();
        Uri finalUri = Uri.parse(videoPaths);
        videoURIList.add(finalUri);


        Logger.d("vid_path_111", "" + videoPaths);

        File file = new File(videoPaths);
        long length = file.length();
        length = length / 1024;
        //Toast.makeText(this, "Video size:"+length+"KB", Toast.LENGTH_LONG).show();

        if (length > 20000) {

            File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/Video");
            if (!wallpaperDirectory.exists())
                wallpaperDirectory.mkdir();

            File f = new File(wallpaperDirectory, "VID_" + Calendar.getInstance().getTimeInMillis() + ".mp4");

            VideoCompressor.start(getApplicationContext(),videoURIList,false, Environment.DIRECTORY_MOVIES,
                    new CompressionListener() {
                @Override
                public void onCancelled(int index) {
                    // On Cancelled
                    hudvideo.dismiss();
                }

                @Override
                public void onProgress(int index, float percent) {
                    // Update UI with progress value
                    runOnUiThread(new Runnable() {
                        public void run() {
                            hudvideo.setDetailsLabel("Compressing Video:" + Math.round(percent) + "%");
                            Logger.e("LLLLL_Progress: ", "" + percent);
                        }
                    });
                }

                @Override
                public void onFailure(int index, @NotNull String failureMessage) {
                    Logger.e("LLLLL_Fail: ", "Fail");

                }

                @Override
                public void onSuccess(int index, long size, @Nullable String path) {
                    hudvideo.dismiss();
                    playVideo(f.getAbsolutePath());
                    tempvideo = new File(videoPaths);

                    if (tempvideo != null)
                        tempvideo.delete();
                    float length = f.length() / 1024f; // Size in KB
                    String value;
                    if (length >= 1024)
                        value = length / 1024f + " MB";
                    else
                        value = length + " KB";
                    String text = String.format(Locale.US, "Output: \nName:" + f.getName() + " \nSize: " + value);
                    Logger.e("LLLLL_End: ", text);

                    float length1 = new File(videoPaths).length() / 1024f;
                    String value1;
                    if (length1 >= 1024)
                        value1 = length1 / 1024f + " MB";
                    else
                        value1 = length1 + " KB";
                    String text1 = String.format(Locale.US, "Input: \nName:" + new File(videoPaths).getName() + " \nSize: " + value1);
                    Logger.e("LLLLL_End: ", text1);
                }

                @Override
                public void onStart(int index) {
                    hudvideo = KProgressHUD.create(CreatePostActivity.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setDimAmount(0.5f)
                            .setMaxProgress(100)
                            .setDetailsLabel("Compressing Video")
                            .setCancellable(true)
                            .show();
                }

            }, new Configuration(
                            VideoQuality.VERY_HIGH,
                            24, /*frameRate: int, or null*/
                            false, /*isMinBitrateCheckEnabled*/
                            null, /*videoBitrate: int, or null*/
                            false, /*disableAudio: Boolean, or null*/
                            false, /*keepOriginalResolution: Boolean, or null*/
                            360.0, /*videoWidth: Double, or null*/
                            480.0 /*videoHeight: Double, or null*/
                    ));

        } else {
            playVideo(videoPaths);
        }

    }

    private void playVideo(String videoPaths) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (videoPaths != null) {
                //findViewById(R.id.post_text).setVisibility(View.VISIBLE);
                textlayout.setVisibility(View.VISIBLE);
                findViewById(R.id.layout_only_images).setVisibility(View.GONE);
                rlImageType.setVisibility(View.GONE);
                findViewById(R.id.video_layout).setVisibility(View.VISIBLE);
                findViewById(R.id.map_layout).setVisibility(View.GONE);
                findViewById(R.id.text_with_image_background).setVisibility(View.GONE);
                ((ToggleButton) findViewById(R.id.map_view)).setChecked(false);

                postType = PostType.VIDEO;
                videoPath = videoPaths;
                playVideo(Uri.parse(videoPaths));
            }
        } else {
            runOnUiThread(() -> {
                if (videoPaths != null) {
                    //findViewById(R.id.post_text).setVisibility(View.VISIBLE);
                    textlayout.setVisibility(View.VISIBLE);
                    findViewById(R.id.layout_only_images).setVisibility(View.GONE);
                    rlImageType.setVisibility(View.GONE);
                    findViewById(R.id.video_layout).setVisibility(View.VISIBLE);
                    findViewById(R.id.map_layout).setVisibility(View.GONE);
                    findViewById(R.id.text_with_image_background).setVisibility(View.GONE);
                    ((ToggleButton) findViewById(R.id.map_view)).setChecked(false);
                    postType = PostType.VIDEO;
                    videoPath = videoPaths;
                    playVideo(Uri.parse(videoPaths));
                }
            });
        }
    }

    private void playVideo(Uri mediaUri) {

        Logger.d("PlayVideo111", "" + mediaUri.getPath());

        playable = new ExoPlayable(ToroExo.with(this).getDefaultCreator(), mediaUri, null);
        playable.prepare(true);
        playable.setPlayerView(player);
        playable.getPlayerView().getPlayer().setRepeatMode(Player.REPEAT_MODE_ALL);
        mute();
        if (!playable.isPlaying()) {
            playable.play();
        }
    }

    private void mute() {
        this.playable.setVolumeInfo(new VolumeInfo(true, 0));
    }

    private void unMute() {
        this.playable.setVolumeInfo(new VolumeInfo(false, 1));
    }

    private boolean hasPermissions(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions)
                if (ActivityCompat.checkSelfPermission(CreatePostActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case CAMERA_INTENT:
                    showMediaSelectionDialog();
                    break;
                case SCREENSHOT_INTENT:
                    createMapPost();
                    break;
                case LOCATION_PERMISSION_INTENT:
                    getCurrentLocation();
                    break;
                default:
                    break;
            }
        } else {
            Toast.makeText(CreatePostActivity.this, getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
        }
    }

    private void openImageCapture() {

        new ImagePicker.Builder(this)
                .mode(ImagePicker.Mode.CAMERA)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.JPG)
                .scale(600, 600)
                .allowOnlineImages(true)
                .enableDebuggingMode(true)
                .build();
    }

    private void openImagePicker() {
        Matisse.from(CreatePostActivity.this)
                .choose(MimeType.of(MimeType.JPEG))
                .countable(true) // initially added
                .maxSelectable(1)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(new GlideEngine())
                .forResult(PICK_GALLERY_REQUEST);

       /* new ImagePicker.Builder(this)
                .mode(ImagePicker.Mode.GALLERY)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.JPG)
                .scale(600, 600)
                .allowMultipleImages(true)
                .allowOnlineImages(true)
                .enableDebuggingMode(true)
                .build();*/
    }

    private void openVideoRecorder() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            startActivityForResult(takeVideoIntent, REQUEST_TAKE_GALLERY_VIDEO);
        } else {
            new VideoPicker.Builder(this)
                    .mode(VideoPicker.Mode.CAMERA)
                    .directory(VideoPicker.Directory.DEFAULT)
                    .extension(VideoPicker.Extension.MP4)
                    .enableDebuggingMode(true)
                    .build();
        }
    }

    private void openVideoPicker() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_TAKE_GALLERY_VIDEO);
        } else {
            new VideoPicker.Builder(this)
                    .mode(VideoPicker.Mode.GALLERY)
                    .directory(VideoPicker.Directory.DEFAULT)
                    .extension(VideoPicker.Extension.MP4)
                    .enableDebuggingMode(true)
                    .build();
        }


        hudVideoTrim = KProgressHUD.create(CreatePostActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setDetailsLabel("Please wait")
                .setCancellable(false)
                .show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_GALLERY_REQUEST && resultCode == RESULT_OK) {
            List<String> mPaths = Matisse.obtainPathResult(data);
            Logger.d("Images", mPaths.toString());

            //to check image is taken from camera or not
            imageSource = "Gallery";

            showTextAndImage(mPaths);
       /* if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            Logger.d("Images", mPaths.toString());
            showTextAndImage(mPaths);*/
        } else if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            Logger.e("LLLLL_Time: ", "000");
            ArrayList<String> mPaths = data.getStringArrayListExtra(EXTRA_VIDEO_PATH);
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(CreatePostActivity.this, Uri.parse(mPaths.get(0)));
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long durationMs = Long.parseLong(time);
            long minutes = TimeUnit.MILLISECONDS.toSeconds(durationMs);
            retriever.release();
            Logger.e("LLLLL_Time: ", String.valueOf(minutes));

            if (isNormalPost) {
                if (minutes < 60) {
                    if (hudVideoTrim != null) {
                        hudVideoTrim.dismiss();
                    }
                    showTextAndVideo(mPaths.get(0));
                } else {
                    if (hudVideoTrim != null) {
                        hudVideoTrim.dismiss();
                    }
                    Logger.e("LLLLL_Android_V: ", String.valueOf(Build.VERSION.SDK_INT));
                    Intent intent = new Intent(CreatePostActivity.this, VideoTrimmarActivity.class);
                    intent.putExtra(EXTRA_VIDEO_PATH, mPaths.get(0));
                    intent.putExtra("postStoryFor", postFor);
                    intent.putExtra("isNormalPost", "normalPost");
                    startActivity(intent);
                    finish();
                }
            } else {
                if (minutes < 15) {
                    if (hudVideoTrim != null) {
                        hudVideoTrim.dismiss();
                    }
                    showTextAndVideo(mPaths.get(0));
                } else {
                    if (hudVideoTrim != null) {
                        hudVideoTrim.dismiss();
                    }
                    Logger.e("LLLLL_Android_V: ", String.valueOf(Build.VERSION.SDK_INT));
                    Intent intent = new Intent(CreatePostActivity.this, VideoTrimmarActivity.class);
                    intent.putExtra(EXTRA_VIDEO_PATH, mPaths.get(0));
                    intent.putExtra("postStoryFor", postFor);
                    intent.putExtra("isNormalPost", "story");
                    startActivity(intent);
                    finish();
                }
            }


        } else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO && resultCode == RESULT_OK) {
            Logger.e("LLLLL_Time: ", "000");
            Uri videoUri = data.getData();
            String selectedImagePath = getPath(videoUri);

            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(CreatePostActivity.this, videoUri);
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long durationMs = Long.parseLong(time);
            long minutes = TimeUnit.MILLISECONDS.toSeconds(durationMs);
            retriever.release();
            Logger.e("LLLLL_Time: ", String.valueOf(minutes));

            if (isNormalPost) {
                if (minutes < 60) {
                    if (hudVideoTrim != null) {
                        hudVideoTrim.dismiss();
                    }
                    showTextAndVideo(selectedImagePath);
                } else {
                    if (hudVideoTrim != null) {
                        hudVideoTrim.dismiss();
                    }
                    Logger.e("LLLLL_Android_V: ", String.valueOf(Build.VERSION.SDK_INT));
                    Intent intent = new Intent(CreatePostActivity.this, VideoTrimmarActivity.class);
                    intent.putExtra(EXTRA_VIDEO_PATH, selectedImagePath);
                    intent.putExtra("postStoryFor", postFor);
                    intent.putExtra("isNormalPost", "normalPost");
                    startActivity(intent);
                    finish();
                }
            } else {
                if (minutes < 15) {
                    if (hudVideoTrim != null) {
                        hudVideoTrim.dismiss();
                    }
                    showTextAndVideo(selectedImagePath);
                } else {
                    if (hudVideoTrim != null) {
                        hudVideoTrim.dismiss();
                    }
                    Logger.e("LLLLL_Android_V: ", String.valueOf(Build.VERSION.SDK_INT));
                    Intent intent = new Intent(CreatePostActivity.this, VideoTrimmarActivity.class);
                    intent.putExtra(EXTRA_VIDEO_PATH, selectedImagePath);
                    intent.putExtra("postStoryFor", postFor);
                    intent.putExtra("isNormalPost", "story");
                    startActivity(intent);
                    finish();
                }
            }


        } else if (requestCode == UPDATE_IMAGE_INTENT && resultCode == RESULT_OK) {
            ArrayList<String> updatedImagePaths = data.getStringArrayListExtra("images");
            if (updatedImagePaths != null) {
                imageSource = "Gallery";
                showTextAndImage(updatedImagePaths);
            }

        } else if (requestCode == CAMERA_VIEW && resultCode == RESULT_OK) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                try {
                    File gpxfile = new File(imageFilePath);
                    int size = (int) gpxfile.length();
                    byte[] bytes1 = new byte[size];
                    BufferedInputStream buf = new BufferedInputStream(new FileInputStream(gpxfile));
                    buf.read(bytes1, 0, bytes1.length);
                    buf.close();

                    ArrayList<String> mPaths = new ArrayList<>();
                    mPaths.add(imageFilePath);
                    if (mPaths != null) {
                        imageSource = "Camera";
                        showTextAndImage(mPaths);
                    }

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                ArrayList<String> mPaths = data.getStringArrayListExtra("imagePath");
                if (mPaths != null) {
                    Logger.d("oldEditorPath", "" + mPaths.get(0));
                    imageSource = "Camera";
                    showTextAndImage(mPaths);
                }
            }

        } else if (requestCode == FILTERED_IMAGE && resultCode == RESULT_OK) {
            ArrayList<String> mPaths = data.getStringArrayListExtra("imagePath");
            if (mPaths != null) {
                imageSource = "Gallery";
                showTextAndImage(mPaths);
            }

        } /*else if (requestCode == TAG_USER_INTENT && resultCode == RESULT_OK) {
            if(data!=null && data.getSerializableExtra("images") != null){
                tagUserList = (ArrayList<TagToBeTagged>) data.getSerializableExtra("images");
                setTagUserCount(tagUserList);
            }
        }*/

        if (resultCode == RESULT_CANCELED) {
            if (hudVideoTrim != null) {
                hudVideoTrim.dismiss();
            }
        }
    }

    /*private void setTagUserCount(ArrayList<TagToBeTagged> tagUserList) {
        if(tagUserList!=null && !tagUserList.isEmpty()){
            tagPeopleCount.setVisibility(View.VISIBLE);
            tagPeopleCount.setText("( "+tagUserList.size()+" )");
        }
    }*/

    private void showMediaSelectionDialog() {

        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dialog_media_selection);
        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.images).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                showImageSourceSelectionDialog();
            }
        });

        popup.findViewById(R.id.video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                showVideoSourceSelectionDialog();
            }
        });
    }

    private void showImageSourceSelectionDialog() {
        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dialog_media_selection);

        ((TextView) popup.findViewById(R.id.images)).setText(R.string.camera);
        ((TextView) popup.findViewById(R.id.video)).setText(R.string.gallery);

        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.images).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    pickImage(CAMERA_VIEW);
                } else {
                    Intent intent = new Intent(CreatePostActivity.this, OpenCamera.class);
                    startActivityForResult(intent, CAMERA_VIEW);
                }

            }
        });

        popup.findViewById(R.id.video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                openImagePicker();
            }
        });
    }

    private void showVideoSourceSelectionDialog() {

        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dialog_media_selection);

        ((TextView) popup.findViewById(R.id.images)).setText(R.string.camera);
        ((TextView) popup.findViewById(R.id.video)).setText(R.string.gallery);

        popup.setCancelable(true);
        popup.show();

        popup.findViewById(R.id.images).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                openVideoRecorder();
            }
        });

        popup.findViewById(R.id.video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                openVideoPicker();
            }
        });
    }

    //call particular API based on the post type
    private void createPost() {

        switch (postType) {

            case TEXT:
                if (!"".equals(postText.getText().toString())) {
                    if (isNormalPost) {
                        createTextPost();
                    } else {
                        createStoryTextPost();
                    }
                }

                break;
            case IMAGE:
                if (imagePaths.size() > 0)
                    createImagePost();
                break;
            case VIDEO:

                Logger.d("videoPath222", "" + videoPath);

                if (!"".equals(videoPath))
                    createVideoPost();
                break;
            case BACKGROUND_IMAGE:
                if (!"".equals(selectedBackgroundImageUrl) && !"".equals(postTextWithBackgroundImage.getText().toString())) {

                    if (isNormalPost) {
                        createBackgroundImageTextPost();
                    } else {
                        createStoryBackgroundImageTextPost();
                    }

                }

                break;
            case MAP:
                String mapText = mapText1.getText().toString() + mapText2.getText().toString() + mapText3.getText().toString() +
                        mapText4.getText().toString() + mapText5.getText().toString();
                int wordCount = mapText.trim().split(" ").length;
                if (wordCount > 0) {
                    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
                    if (!hasPermissions(PERMISSIONS))
                        ActivityCompat.requestPermissions(CreatePostActivity.this, PERMISSIONS, SCREENSHOT_INTENT);
                    else
                        createMapPost();
                }
            default:
                break;
        }
    }

    /*if (!"".equals(mapText1.getText().toString()) || !"".equals(mapText2.getText().toString()) || !"".equals(mapText3.getText().toString())
            || !"".equals(mapText4.getText().toString()) || !"".equals(mapText5.getText().toString()))*/

    private void createTextPost() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createTextPostJsonRequest(), headers, UrlEndpoints.POSTS, response -> {
            hud.dismiss();
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(CreatePostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(CreatePostActivity.this, "Post created successfully", Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                }

            } catch (Exception e) {
                Utils.showAlert(CreatePostActivity.this, "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    private void createStoryTextPost() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createStoryTextPostJsonRequest(), headers, UrlEndpoints.STORYLINE, response -> {
            hud.dismiss();
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(CreatePostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(CreatePostActivity.this, "Story Added successfully", Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                }

            } catch (Exception e) {
                Utils.showAlert(CreatePostActivity.this, "Oops something went wrong....");
                e.printStackTrace();
            }
        });
    }

    private JSONObject createTextPostJsonRequest() {

        JSONObject request = new JSONObject();
        try {
            String post = postText.getText().toString() + ("".equals(hashtagSlug) ? hashtagSlug : (" #" + hashtagSlug));
            request.put("post_text", post);
            request.put("post_lat_long", location);
            request.put("posted_for", postFor);
            request.put("font_face", fontFace);

            tagUserList = postText.getMentions();

            if (tagUserList != null && !tagUserList.isEmpty()) {
                JSONArray taggedUsers = new JSONArray();
                for (String tagToBeTagged : tagUserList) {
                    taggedUsers.put(tagToBeTagged);
                }
                Logger.d("taggedUserRequest", taggedUsers.toString() + "");
                request.put("tagged_id", taggedUsers);
            }

            if (groupId != -1)
                request.put("group_id", groupId);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private JSONObject createStoryTextPostJsonRequest() {

        JSONObject request = new JSONObject();
        try {
            String post = postText.getText().toString() + ("".equals(hashtagSlug) ? hashtagSlug : (" #" + hashtagSlug));
            request.put("story_text", post);
            request.put("post_lat_long", location);
            request.put("posted_for", postFor);

            if (groupId != -1)
                request.put("group_id", groupId);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void createImagePost() {

        tagUserList = postText.getMentions();


        Intent intent = new Intent();
        intent.putExtra("postType", postType);
        intent.putExtra("postText", postText.getText().toString());
        intent.putExtra("images", imagePaths);
        intent.putExtra("location", location);
        intent.putExtra("tagUserList", (Serializable) tagUserList);

        if (imageSource.equalsIgnoreCase("camera")) {
            intent.putExtra("imageType", "zoom");
        } else {
            intent.putExtra("imageType", imageTypeString);
        }

        intent.putExtra("font_face", fontFace);

        intent.putExtra("postCategory", isNormalPost ? "normal_post" : "story_post");

        setResult(RESULT_OK, intent);
        finish();
        /*if ("afroswagger".equals(postFor)) {
            Intent intent = new Intent();
            intent.putExtra("postType",postType);
            intent.putExtra("postText",postText.getText().toString());
            intent.putExtra("images",imagePaths);
            intent.putExtra("location",location);
            setResult(RESULT_OK,intent);
            finish();
        } else {
            createImageFile(imagePaths);
            uploadImages();
        }*/
    }

    private void uploadImages() {

        Logger.d("UPLOAD_IMAGES_SS", "upload images...");

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (File photo : imageFiles) {
            builder.addFormDataPart(Constants.IMAGES, photo.getName(), RequestBody.create(MediaType.parse("image/*"), photo));
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "multipart/form-data");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.POSTS + "/upload";

        Webservices.getData(this, Webservices.Method.POST, builder, headers, url, response -> {
            if ("".equals(response)) {
                hud.dismiss();
                Utils.showAlert(CreatePostActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        hud.dismiss();
                        Utils.showAlert(CreatePostActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        MediaDetails mediaDetails = new Gson().fromJson(response, MediaDetails.class);

                        if (mediaDetails.getMediaList() != null && mediaDetails.getMediaList().size() > 0) {

                            if (isNormalPost) {
                                createImagePost(mediaDetails.getMediaList());
                            } else {
                                createStoryImagePost(mediaDetails.getMediaList());
                            }

                            for (File image : imageFiles)
                                image.delete();
                        } else {
                            hud.dismiss();
                            Utils.showAlert(CreatePostActivity.this, getString(R.string.opps_something_went_wrong_please_try_again_later));
                        }

                    }

                } catch (Exception e) {
                    hud.dismiss();
                    e.printStackTrace();
                    Utils.showAlert(CreatePostActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    private void createImagePost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createImagePostJsonRequest(mediaList), headers, UrlEndpoints.POSTS, response -> {
            hud.dismiss();
            try {
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(CreatePostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(CreatePostActivity.this, getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                }

            } catch (Exception e) {
                Utils.showAlert(CreatePostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private void createStoryImagePost(ArrayList<Media> mediaList) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createStoryImagePostJsonRequest(mediaList), headers, UrlEndpoints.STORYLINE, response -> {
            hud.dismiss();
            try {
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(CreatePostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(CreatePostActivity.this, getString(R.string.story_added_successfully), Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                }

            } catch (Exception e) {
                Utils.showAlert(CreatePostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private JSONObject createImagePostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            String post = postText.getText().toString() + ("".equals(hashtagSlug) ? hashtagSlug : (" #" + hashtagSlug));
            request.put("post_text", post);
            request.put("post_lat_long", location);
            request.put("posted_for", postFor);

            if (groupId != -1)
                request.put("group_id", groupId);

            JSONArray images = new JSONArray();
            for (Media media : mediaList) {
                images.put(media.getPath());
            }

            if (postType == PostType.MAP)
                request.put("bg_map_post", true);

            request.put("post_type", "image");
            request.put("post_image", images);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private JSONObject createStoryImagePostJsonRequest(ArrayList<Media> mediaList) {

        JSONObject request = new JSONObject();
        try {

            String post = postText.getText().toString() + ("".equals(hashtagSlug) ? hashtagSlug : (" #" + hashtagSlug));
            request.put("story_text", post);
            request.put("post_lat_long", location);
            request.put("posted_for", postFor);

            if (groupId != -1)
                request.put("group_id", groupId);

            JSONArray images = new JSONArray();
            for (Media media : mediaList) {
                images.put(media.getPath());
            }

            if (postType == PostType.MAP)
                request.put("bg_map_post", true);

            request.put("story_type", "image");
            request.put("story_image", images);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void createVideoPost() {
        postType1 = postType;
        postText1 = postText.getText().toString().trim();
        video1 = videoPath;
        location1 = location;
        tagUserList1 = (ArrayList<String>) tagUserList;
        font_face1 = fontFace;
        postCategory1 = isNormalPost ? "normal_post" : "story_post";

        Intent intent = new Intent();
        intent.putExtra("postType", postType);
        intent.putExtra("postText", postText.getText().toString());
        intent.putExtra("video", videoPath);
        intent.putExtra("location", location);
        intent.putExtra("font_face", fontFace);
        intent.putExtra("postCategory", isNormalPost ? "normal_post" : "story_post");

        setResult(RESULT_OK, intent);
        finish();
        /*createVideoFile(videoPath);
        uploadVideo();*/
    }


    private void createBackgroundImageTextPost() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createBackgroundImageTextPostJsonRequest(), headers, UrlEndpoints.POSTS, response -> {
            hud.dismiss();
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(CreatePostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(CreatePostActivity.this, getString(R.string.post_created_successfully), Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                }

            } catch (Exception e) {
                Utils.showAlert(CreatePostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private void createStoryBackgroundImageTextPost() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, createStoryBackgroundImageTextPostJsonRequest(), headers, UrlEndpoints.STORYLINE, response -> {
            hud.dismiss();
            try {

                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(CreatePostActivity.this, object.getString(Constants.MESSAGE));
                } else {
                    Toast.makeText(CreatePostActivity.this, getString(R.string.story_added_successfully), Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                }

            } catch (Exception e) {
                Utils.showAlert(CreatePostActivity.this, getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private JSONObject createBackgroundImageTextPostJsonRequest() {

        JSONObject request = new JSONObject();

        try {
            String post = postTextWithBackgroundImage.getText().toString() + ("".equals(hashtagSlug) ? hashtagSlug : (" #" + hashtagSlug));
            request.put("post_text", post);
            request.put("post_lat_long", location);
            request.put("posted_for", postFor);

            if (groupId != -1)
                request.put("group_id", groupId);

            request.put("bg_image_post", true);
            request.put("bg_image", selectedBackgroundImageUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private JSONObject createStoryBackgroundImageTextPostJsonRequest() {

        JSONObject request = new JSONObject();

        try {
            String post = postTextWithBackgroundImage.getText().toString() + ("".equals(hashtagSlug) ? hashtagSlug : (" #" + hashtagSlug));
            request.put("story_text", post);
            request.put("post_lat_long", location);
            request.put("posted_for", postFor);

            if (groupId != -1)
                request.put("group_id", groupId);

            request.put("bg_image_post", true);
            request.put("bg_image", selectedBackgroundImageUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return request;
    }

    private void createMapPost() {
        Logger.d("map_post", "clicked...");
        takeScreenshot();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.no_background:
                break;
            case R.id.no_color:
                postText.setBackground(null);
                showOnlyText();
                break;
            case R.id.white:
                postText.setBackgroundColor(getResources().getColor(R.color.white));
                showOnlyText();
                break;
            case R.id.yellow:
                postText.setBackgroundColor(getResources().getColor(R.color.yellow));
                showOnlyText();
                break;
            case R.id.orange:
                postText.setBackgroundColor(getResources().getColor(R.color.orange));
                showOnlyText();
                break;
            case R.id.blue:
                postText.setBackgroundColor(getResources().getColor(R.color.blue));
                showOnlyText();
                break;
            case R.id.red:
                postText.setBackgroundColor(getResources().getColor(R.color.red));
                showOnlyText();
                break;
            case R.id.edit_image:
                startActivityForResult(new Intent(this,
                                EditPhotoActivity.class).putExtra("imagePath", imagePaths.get(0)),
                        FILTERED_IMAGE);
                break;
            case R.id.remove:
                clearTagData();
                showOnlyText();
                break;
            case R.id.post:

                CommonUtil.hideKeyboard(this);

                if (Utils.isConnected())
                    createPost();
                else
                    Utils.showAlert(CreatePostActivity.this, getString(R.string.no_internet_connection_available));
                break;
            case R.id.more_images_layout:
                Intent intent = new Intent(CreatePostActivity.this, ViewImagesActivity.class);
                intent.putExtra("images", imagePaths);
                startActivityForResult(intent, UPDATE_IMAGE_INTENT);
                break;
            case R.id.upload:
                String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};

                if (!hasPermissions(PERMISSIONS)) {
                    ActivityCompat.requestPermissions(CreatePostActivity.this, PERMISSIONS, CAMERA_INTENT);
                } else {
                    showMediaSelectionDialog();
                }
                break;

            case R.id.hashtagTextview:
                if (!TextUtils.isEmpty(postText.getText().toString())) {
                    boolean isSpace = Character.isWhitespace(postText.getText().toString()
                            .charAt(postText.getText().toString()
                                    .length() - 1));

                    if (isSpace) {
                        postText.setSelection(postText.getText().length());
                        postText.append("#");
                    } else {
                        postText.setSelection(postText.getText().length());
                        postText.append(" #");
                    }

                } else {
                    postText.setSelection(postText.getText().length());
                    postText.append("#");
                }
                CommonUtil.showKeyboard(this);
                break;

            case R.id.mentionTextview:

                if (!TextUtils.isEmpty(postText.getText().toString())) {
                    boolean isSpace = Character.isWhitespace(postText.getText().toString()
                            .charAt(postText.getText().toString()
                                    .length() - 1));

                    if (isSpace) {
                        postText.setSelection(postText.getText().length());
                        postText.append("@");
                    } else {
                        postText.setSelection(postText.getText().length());
                        postText.append(" @");
                    }

                } else {
                    postText.setSelection(postText.getText().length());
                    postText.append("@");
                }
                CommonUtil.showKeyboard(this);
                break;

            case R.id.fontTextview:
                if (fontLayout.getVisibility() == View.VISIBLE) {
                    fontLayout.setVisibility(View.GONE);
                } else {
                    fontLayout.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.font1:
                setFontStyle("mick_caster");
                fontFace = "mick_caster";
                font1.setBackgroundResource(R.drawable.round_corner_orange_border);
                font2.setBackgroundResource(0);
                font3.setBackgroundResource(0);
                font4.setBackgroundResource(0);
                font5.setBackgroundResource(0);
                break;
            case R.id.font2:
                fontFace = "bullpen_three_d";
                setFontStyle("bullpen_three_d");
                font2.setBackgroundResource(R.drawable.round_corner_orange_border);
                font1.setBackgroundResource(0);
                font3.setBackgroundResource(0);
                font4.setBackgroundResource(0);
                font5.setBackgroundResource(0);
                break;
            case R.id.font3:
                setFontStyle("marlboro");
                fontFace = "marlboro";
                font3.setBackgroundResource(R.drawable.round_corner_orange_border);
                font2.setBackgroundResource(0);
                font1.setBackgroundResource(0);
                font4.setBackgroundResource(0);
                font5.setBackgroundResource(0);
                break;
            case R.id.font4:
                setFontStyle("mrsmonster");
                fontFace = "mrsmonster";
                font4.setBackgroundResource(R.drawable.round_corner_orange_border);
                font2.setBackgroundResource(0);
                font3.setBackgroundResource(0);
                font1.setBackgroundResource(0);
                font5.setBackgroundResource(0);
                break;
            case R.id.font5:
                setFontStyle("vonique");
                fontFace = "vonique";
                font5.setBackgroundResource(R.drawable.round_corner_orange_border);
                font2.setBackgroundResource(0);
                font3.setBackgroundResource(0);
                font4.setBackgroundResource(0);
                font1.setBackgroundResource(0);
                break;

            case R.id.tagUser:

                //Open screen to tag user like other social media

                /*if(postType == PostType.IMAGE){
                    if(imagePaths!=null && imagePaths.size()>0){
                        Intent intentTagUser = new Intent(CreatePostActivity.this, TagPhotoActivity.class);
                        intentTagUser.putExtra("images", imagePaths);
                        startActivityForResult(intentTagUser, TAG_USER_INTENT);
                    }else {
                        Toast.makeText(CreatePostActivity.this, "Please select post", Toast.LENGTH_SHORT).show();
                    }
                }else if(postType == PostType.VIDEO){
                    if(!"".equalsIgnoreCase(videoPath)){
                        Intent intentTagUser = new Intent(CreatePostActivity.this, TagPhotoActivity.class);
                        intentTagUser.putExtra("video", videoPath);
                        startActivityForResult(intentTagUser, TAG_USER_INTENT);
                    }else {
                        Toast.makeText(CreatePostActivity.this, "Please select post", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(CreatePostActivity.this, "Please select post", Toast.LENGTH_SHORT).show();
                }*/

                break;
            case R.id.back:
                onBackPressed();
                break;
        }

    }

    private void setFontStyle(String fontStyle) {
        Typeface face = Typeface.createFromAsset(getAssets(), "font/" + fontStyle + ".ttf");
        postText.setTypeface(face);
    }

    private void clearTagData() {
        //tagPeopleCount.setVisibility(View.GONE);
        tagUserList.clear();
    }

    public enum PostType implements Serializable {

        TEXT, IMAGE, VIDEO, BACKGROUND_IMAGE, MAP
    }

    @Override
    public void onStop() {
        super.onStop();
        if (playable != null) {
            playable.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (playable != null) {
            playable.setPlayerView(null);
            playable.release();
            playable = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (playable != null)
            playable.setPlayerView(null);
    }

    /* Logic to display text over map*/
    private void showMapTextLogic() {

        String dummyContent = getResources().getString(R.string.dummyContent);
        mapText1.setText(dummyContent);

        mapText1.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int start = mapText1.getLayout().getLineStart(0);
                int end = mapText1.getLayout().getLineEnd(2);

                String map1 = mapText1.getText().subSequence(start, end).toString();
                mapText1.setText(map1);
                String newDummyContent = dummyContent.replace(map1, "");
                mapText2.setText(newDummyContent);
            }
        });

        mapText2.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int start = mapText2.getLayout().getLineStart(0);
                int end = mapText2.getLayout().getLineEnd(1);

                String map2 = mapText2.getText().subSequence(start, end).toString();
                String newDummyContent = mapText2.getText().toString().replace(map2, "");
                mapText2.setText(map2);
                mapText3.setText(newDummyContent);
            }
        });

        mapText3.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int start = mapText3.getLayout().getLineStart(0);
                int end = mapText3.getLayout().getLineEnd(0);

                String map3 = mapText3.getText().subSequence(start, end).toString();
                String newDummyContent = mapText3.getText().toString().replace(map3, "");
                mapText3.setText(map3);
                mapText4.setText(newDummyContent);
            }
        });

        mapText4.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int start = mapText4.getLayout().getLineStart(0);
                int end = mapText4.getLayout().getLineEnd(0);

                String map4 = mapText4.getText().subSequence(start, end).toString();
                String newDummyContent = mapText4.getText().toString().replace(map4, "");
                mapText4.setText(map4);
                mapText5.setText(newDummyContent);
            }
        });

        /*ViewTreeObserver vto = mapText.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int start = mapText.getLayout().getLineStart(0);
                int end = mapText.getLayout().getLineEnd(2);

                mapText1.setText(mapText.getText().subSequence(start,end).toString());

                start = mapText.getLayout().getLineStart(3);
                end = mapText.getLayout().getLineEnd(4);

                mapText2.setText(mapText.getText().subSequence(start,end).toString());

                start = mapText.getLayout().getLineStart(5);
                end = mapText.getLayout().getLineEnd(5);

                mapText3.setText(mapText.getText().subSequence(start,end).toString());

                start = mapText.getLayout().getLineStart(6);
                end = mapText.getLayout().getLineEnd(6);

                mapText4.setText(mapText.getText().subSequence(start,end).toString());

                start = mapText.getLayout().getLineStart(7);
                end = mapText.getLayout().getLineEnd(14);

                mapText5.setText(mapText.getText().subSequence(start,end).toString());

                mapText.setVisibility(View.GONE);
            }
        });*/
    }

    private void takeScreenshot() {


        try {
           /* Date now = new Date();
            DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

            hideCursor();

            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View mapView = findViewById(R.id.map_layout);
            mapView.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(mapView.getDrawingCache());
            mapView.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.PNG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            imageFiles = new ArrayList<>();
            imageFiles.add(imageFile);

            uploadImages();*/


            Date now = new Date();
            DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

            hideCursor();

            // image naming and path  to include sd card  appending name you choose for file
            //String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View mapView = findViewById(R.id.map_layout);
            mapView.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(mapView.getDrawingCache());
            mapView.setDrawingCacheEnabled(false);


            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            File file = new File(directory, now + ".jpg");
            if (!file.exists()) {
                Logger.d("path99", file.toString());
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.flush();
                    fos.close();
                    imageFiles = new ArrayList<>();
                    imageFiles.add(file);

                    uploadImages();

                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

    private void hideCursor() {
        mapText1.setCursorVisible(false);
        mapText2.setCursorVisible(false);
        mapText3.setCursorVisible(false);
        mapText4.setCursorVisible(false);
        mapText5.setCursorVisible(false);
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private void pickImage(int code) {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);

//        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, getPackageName() + ".fileprovider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        code);
            }
//        }
    }

    String imageFilePath;

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }

}
