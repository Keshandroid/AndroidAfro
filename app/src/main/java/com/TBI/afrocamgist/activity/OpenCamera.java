package com.TBI.afrocamgist.activity;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorMatrix;
import android.graphics.ImageFormat;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.FiltersAdapter;
import com.TBI.afrocamgist.model.filter.Filter;
import com.TBI.captureview.CameraException;
import com.TBI.captureview.CameraListener;
import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.CameraOptions;
import com.TBI.captureview.CameraView;
import com.TBI.captureview.FaceDetection.FaceUtils;
import com.TBI.captureview.PictureResult;
import com.TBI.captureview.VideoResult;
import com.TBI.captureview.controls.Facing;
import com.TBI.captureview.controls.Flash;
import com.TBI.captureview.controls.Mode;
import com.TBI.captureview.controls.Preview;
import com.TBI.captureview.filter.Filters;
import com.TBI.captureview.frame.Frame;
import com.TBI.captureview.frame.FrameProcessor;
import com.airbnb.lottie.LottieAnimationView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class OpenCamera extends BaseActivity {


    private final static CameraLogger LOG = CameraLogger.create("Afrocamgist");
    private final static boolean USE_FRAME_PROCESSOR = true;
    private final static boolean DECODE_BITMAP = true;
    private static final String IMAGE_DIRECTORY = "/Afrocamgist";
    public static final int FILTERED_IMAGE = 401;

    private CameraView camera;
    private long captureTime;
    private final Filters[] filters = Filters.values();
    private int selectedTimer = 0;
    private CountDownTimer waitTimer;

    private ImageView flip;
    private LottieAnimationView capture;
    private ImageView timer;
    private ImageView flashOff;
    private ImageView flashAuto;
    private LinearLayout flashLayout;
    private ImageView flash;
    private ImageView flashOn;
    private ImageView timerOff;
    private ImageView timer2;
    private ImageView timer5;
    private ImageView timer10;
    private LinearLayout timerLayout;
    private TextView timerCounter;
    private RecyclerView listFilter;
    private ImageView filter;

    private int orientation;
    private int orientationCompensation;
    private FiltersAdapter filtersAdapter;

    public static final String OUTPUT_PHOTO_DIRECTORY = "photo_editor_sample";
    public static final int DS_PHOTO_EDITOR_REQUEST_CODE = 200;


    private class SimpleOrientationEventListener extends OrientationEventListener {

        SimpleOrientationEventListener(Context context) {
            super(context, SensorManager.SENSOR_DELAY_NORMAL);
        }

        @Override
        public void onOrientationChanged(int orientation) {
            // We keep the last known orientation. So if the user first orient
            // the camera then point the camera to floor or sky, we still have
            // the correct orientation.
            if (orientation == ORIENTATION_UNKNOWN) return;
            OpenCamera.this.orientation = FaceUtils.roundOrientation(orientation, OpenCamera.this.orientation);
            // When the screen is unlocked, display rotation may change. Always
            // calculate the up-to-date orientationCompensation.
            int orientationCompensation = OpenCamera.this.orientation
                    + FaceUtils.getDisplayRotation(OpenCamera.this);
            if (OpenCamera.this.orientationCompensation != orientationCompensation) {
                OpenCamera.this.orientationCompensation = orientationCompensation;
//                mFaceView.setOrientation(orientationCompensation);
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.activity_open_camera);

        initView();
        initOrientationListener();
        initFilters();
        initCamera();
        initWaterMarkAnimation();
        setFilterClickListener();
        setFlashClickListener();
        setTimerClickListener();
        setFlipCameraClickListener();
        setImageCaptureClickListener();
    }

    private void initView() {
        flip = findViewById(R.id.flip);
        capture = findViewById(R.id.capture);
        timer = findViewById(R.id.timer);
        flashOff = findViewById(R.id.flash_off);
        flashAuto = findViewById(R.id.flash_auto);
        flashLayout = findViewById(R.id.flash_layout);
        flash = findViewById(R.id.flash);
        flashOn = findViewById(R.id.flash_on);
        timerOff = findViewById(R.id.timer_off);
        timer2 = findViewById(R.id.timer_2);
        timer5 = findViewById(R.id.timer_5);
        timer10 = findViewById(R.id.timer_10);
        timerLayout = findViewById(R.id.timer_layout);
        timerCounter = findViewById(R.id.timer_counter);
        listFilter = findViewById(R.id.list_filter);
        filter = findViewById(R.id.filter);
    }

    private void initOrientationListener() {

        OrientationEventListener orientationEventListener = new SimpleOrientationEventListener(this);
        orientationEventListener.enable();
    }

    private void initFilters() {

        List<Filter> abc = Arrays.asList(
                new Filter("Original", new ColorMatrix(new float[]{
                        1, 0, 0, 0, 0,
                        0, 1, 0, 0, 0,
                        0.50f, 0, 1, 0, 0,
                        0, 0, 0, 1, 0})),
                new Filter("F01", new ColorMatrix(new float[]{
                        1, 0, 0, 0, 0,
                        0, 1, 0, 0, 0,
                        0.50f, 0, 1, 0, 0,
                        0, 0, 0, 1, 0})),
                /*new Filter("F03", new ColorMatrix(new float[]{
                        1.5f, 0, 0, 0, -40,
                        0, 1.5f, 0, 0, -40,
                        0, 0, 1.5f, 0, -40,
                        0, 0, 0, 1, 0})),*/
                new Filter("F02", new ColorMatrix(new float[]{
                        2, -1, 0, 0, 0,
                        -1, 2, 0, 0, 0,
                        0, -1, 2, 0, 0,
                        0, 0, 0, 1, 0})),
                new Filter("F12", new ColorMatrix(new float[]{ // TechColor mMatrix
                        1.9125277891456083f, -0.8545344976951645f, -0.09155508482755585f, 0, 11.793603434377337f,
                        -0.3087833385928097f, 1.7658908555458428f, -0.10601743074722245f, 0, -70.35205161461398f,
                        -0.231103377548616f, -0.7501899197440212f, 1.847597816108189f, 0, 30.950940869491138f,
                        0, 0, 0, 1, 0})),
                new Filter("F04", new ColorMatrix(new float[]{
                        1, 0, 0, 0.2f, 0,
                        0, 1, 0, 0, 0,
                        0, 0, 1, 0.2f, 0,
                        0, 0, 0, 1, 0})),
                new Filter("F05", new ColorMatrix(new float[]{
                        1, 0, 0, 0, 0,
                        0, 1.25f, 0, 0, 0,
                        0, 0, 0, 0.5f, 0,
                        0, 0, 0, 1, 0})),
                new Filter("F06", new ColorMatrix(new float[]{
                        1, 0, 0, 0, 0,
                        0, 0.5f, 0, 0, 0,
                        0, 0, 0, 0.5f, 0,
                        0, 0, 0, 1, 0})),
                new Filter("F07", new ColorMatrix(new float[]{
                        1, 0, 0, 0, 0,
                        0, 0, 0, 0, 0,
                        0, 0, 1, 1, 0,
                        0, 0, 0, 1, 0})),
                new Filter("F08", new ColorMatrix(new float[]{ // Polaroid mMatrix
                        1.438f, -0.062f, -0.062f, 0, 0,
                        -0.122f, 1.378f, -0.122f, 0, 0,
                        -0.016f, -0.016f, 1.483f, 0, 0,
                        0, 0, 0, 1, 0})),
                new Filter("F09", new ColorMatrix(new float[]{ // CodaChrome mMatrix
                        1.1285582396593525f, -0.3967382283601348f, -0.03992559172921793f, 0, 63.72958762196502f,
                        -0.16404339962244616f, 1.0835251566291304f, -0.05498805115633132f, 0, 24.732407896706203f,
                        -0.16786010706155763f, -0.5603416277695248f, 1.6014850761964943f, 0, 35.62982807460946f,
                        0, 0, 0, 1, 0})),

                new Filter("F10", new ColorMatrix(new float[]{ // LSD mMatrix
                        2, -0.4f, 0.5f, 0, 0,
                        -0.5f, 2, -0.4f, 0, 0,
                        -0.4f, -0.5f, 3, 0, 0,
                        0, 0, 0, 1, 0})),
                new Filter("F11", new ColorMatrix(new float[]{ // Vintage mMatrix
                        0.6279345635605994f, 0.3202183420819367f, -0.03965408211312453f, 0, 9.651285835294123f,
                        0.02578397704808868f, 0.6441188644374771f, 0.03259127616149294f, 0, 7.462829176470591f,
                        0.0466055556782719f, -0.0851232987247891f, 0.5241648018700465f, 0, 5.159190588235296f,
                        0, 0, 0, 1, 0})),
                new Filter("F13", new ColorMatrix(new float[]{ // Browni mMatrix
                        0.5997023498159715f, 0.34553243048391263f, -0.2708298674538042f, 0, 47.43192855600873f,
                        -0.037703249837783157f, 0.8609577587992641f, 0.15059552388459913f, 0, -36.96841498319127f,
                        0.24113635128153335f, -0.07441037908422492f, 0.44972182064877153f, 0, -7.562075277591283f,
                        0, 0, 0, 1, 0})),
                new Filter("BW01", new ColorMatrix(new float[]{
                        1, 0, 0, 0, 0,
                        1, 0, 0, 0, 0,
                        1, 0, 0, 0, 0,
                        0, 0, 0, 1, 0})),
                new Filter("BW02", new ColorMatrix(new float[]{
                        0, 1, 0, 0, 0,
                        0, 1, 0, 0, 0,
                        0, 1, 0, 0, 0,
                        0, 0, 0, 1, 0})),
                new Filter("BW03", new ColorMatrix(new float[]{
                        0, 0, 1, 0, 0,
                        0, 0, 1, 0, 0,
                        0, 0, 1, 0, 0,
                        0, 0, 0, 1, 0}))
        );

        filtersAdapter = new FiltersAdapter(OpenCamera.this, abc, OpenCamera.this);
        listFilter.setLayoutManager(new LinearLayoutManager(OpenCamera.this, LinearLayoutManager.HORIZONTAL, false));
        listFilter.setHasFixedSize(true);
        listFilter.setAdapter(filtersAdapter);
    }

    private void initCamera() {

        camera = findViewById(R.id.camera);
        camera.setFlash(Flash.AUTO);
        camera.setLifecycleOwner(OpenCamera.this);
        camera.addCameraListener(new Listener());

        if (USE_FRAME_PROCESSOR) {
            camera.addFrameProcessor(new FrameProcessor() {
                private long lastTime = System.currentTimeMillis();

                @Override
                public void process(@NonNull Frame frame) {
                    long newTime = frame.getTime();
                    long delay = newTime - lastTime;
                    lastTime = newTime;
                    LOG.e("Frame delayMillis:", delay, "FPS:", 1000 / delay);
                    if (DECODE_BITMAP) {
                        YuvImage yuvImage = new YuvImage(frame.getData(), ImageFormat.NV21,
                                frame.getSize().getWidth(),
                                frame.getSize().getHeight(),
                                null);
                        ByteArrayOutputStream jpegStream = new ByteArrayOutputStream();
                        yuvImage.compressToJpeg(new Rect(0, 0,
                                frame.getSize().getWidth(),
                                frame.getSize().getHeight()), 100, jpegStream);
                        byte[] jpegByteArray = jpegStream.toByteArray();
                        Bitmap bitmap = BitmapFactory.decodeByteArray(jpegByteArray, 0, jpegByteArray.length);
                        //noinspection ResultOfMethodCallIgnored
                        bitmap.toString();
                    }
                }
            });
        }
    }

    private void initWaterMarkAnimation() {

        View watermark = findViewById(R.id.watermark);
        ValueAnimator animator = ValueAnimator.ofFloat(1F, 0.8F);
        animator.setDuration(300);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = (float) animation.getAnimatedValue();
                watermark.setScaleX(scale);
                watermark.setScaleY(scale);
                watermark.setRotation(watermark.getRotation() + 2);
            }
        });
        animator.start();
    }

    private void message(@NonNull String content, boolean important) {
        if (important) {
            LOG.w(content);
        } else {
            LOG.i(content);
        }
    }

    private void setFilterClickListener() {

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (listFilter.getVisibility() == View.VISIBLE) {
                    listFilter.setVisibility(View.GONE);
                } else {
                    listFilter.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void changeCurrentFilter(int pos) {

        if (camera.getPreview() != Preview.GL_SURFACE) {
            message("Filters are supported only when preview is Preview.GL_SURFACE.", true);
            return;
        }

        int position;

        if (pos == 0) {
            position = 0;
        } else if (pos == 1) {
            position = 1;
        } else {
            position = pos - 1;
        }

        Filters filter = filters[position];
        camera.setFilter(filter.newInstance());
        filter.newInstance().getVertexShader();

        message(filter.toString(), false);
    }

    private void setFlashClickListener() {

        flashAuto.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));

        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (flashLayout.getVisibility() == View.VISIBLE) {
                    flashLayout.setVisibility(View.GONE);
                } else {
                    if (timerLayout.getVisibility() == View.VISIBLE) {
                        timerLayout.setVisibility(View.GONE);
                    }

                    flashLayout.setVisibility(View.VISIBLE);

                    YoYo.with(Techniques.ZoomIn)
                            .duration(500)
                            .onEnd(new YoYo.AnimatorCallback() {
                                @Override
                                public void call(Animator animator) {
                                }
                            })
                            .playOn(flashLayout);
                }

            }
        });

        flashOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                camera.setFlash(Flash.OFF);
                flash.setImageDrawable(getResources().getDrawable(R.drawable.flash_off));

                flashOn.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                flashOff.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));
                flashAuto.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));

                YoYo.with(Techniques.ZoomOut)
                        .duration(500)
                        .onEnd(new YoYo.AnimatorCallback() {
                            @Override
                            public void call(Animator animator) {
                                flashLayout.setVisibility(View.GONE);
                            }
                        })
                        .playOn(flashLayout);

            }
        });

        flashAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                camera.setFlash(Flash.AUTO);
                flash.setImageDrawable(getResources().getDrawable(R.drawable.flash_auto));

                flashOn.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                flashOff.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                flashAuto.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));

                YoYo.with(Techniques.ZoomOut)
                        .duration(500)
                        .onEnd(new YoYo.AnimatorCallback() {
                            @Override
                            public void call(Animator animator) {
                                flashLayout.setVisibility(View.GONE);
                            }
                        })
                        .playOn(flashLayout);

            }
        });

        flashOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                camera.setFlash(Flash.ON);
                flash.setImageDrawable(getResources().getDrawable(R.drawable.flash_on));

                flashOn.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));
                flashOff.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                flashAuto.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));

                YoYo.with(Techniques.ZoomOut)
                        .duration(500)
                        .onEnd(new YoYo.AnimatorCallback() {
                            @Override
                            public void call(Animator animator) {
                                flashLayout.setVisibility(View.GONE);
                            }
                        })
                        .playOn(flashLayout);

            }
        });
    }

    private void setTimerClickListener() {

        timerOff.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));

        timer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (timerLayout.getVisibility() == View.VISIBLE) {
                    timerLayout.setVisibility(View.GONE);
                } else {
                    if (flashLayout.getVisibility() == View.VISIBLE) {
                        flashLayout.setVisibility(View.GONE);
                    }

                    timerLayout.setVisibility(View.VISIBLE);

                    YoYo.with(Techniques.ZoomIn)
                            .duration(500)
                            .onEnd(new YoYo.AnimatorCallback() {
                                @Override
                                public void call(Animator animator) {
                                }
                            })
                            .playOn(timerLayout);
                }

            }
        });

        timerOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                selectedTimer = 0;
                timer.setImageDrawable(getResources().getDrawable(R.drawable.timer_off));

                timerOff.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));
                timer2.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                timer5.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                timer10.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));

                YoYo.with(Techniques.ZoomOut)
                        .duration(500)
                        .onEnd(new YoYo.AnimatorCallback() {
                            @Override
                            public void call(Animator animator) {
                                timerLayout.setVisibility(View.GONE);
                            }
                        })
                        .playOn(timerLayout);


            }
        });

        timer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedTimer = 2;
                timer.setImageDrawable(getResources().getDrawable(R.drawable.timer2));

                timerOff.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                timer2.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));
                timer5.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                timer10.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));

                YoYo.with(Techniques.ZoomOut)
                        .duration(500)
                        .onEnd(new YoYo.AnimatorCallback() {
                            @Override
                            public void call(Animator animator) {
                                timerLayout.setVisibility(View.GONE);
                            }
                        })
                        .playOn(timerLayout);

            }
        });


        timer5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedTimer = 5;
                timer.setImageDrawable(getResources().getDrawable(R.drawable.timer5));

                timerOff.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                timer2.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                timer5.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));
                timer10.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));

                YoYo.with(Techniques.ZoomOut)
                        .duration(500)
                        .onEnd(new YoYo.AnimatorCallback() {
                            @Override
                            public void call(Animator animator) {
                                timerLayout.setVisibility(View.GONE);
                            }
                        })
                        .playOn(timerLayout);

            }
        });

        timer10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedTimer = 10;
                timer.setImageDrawable(getResources().getDrawable(R.drawable.timer10));

                timerOff.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                timer2.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                timer5.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                timer10.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.orange)));

                YoYo.with(Techniques.ZoomOut)
                        .duration(500)
                        .onEnd(new YoYo.AnimatorCallback() {
                            @Override
                            public void call(Animator animator) {
                                timerLayout.setVisibility(View.GONE);
                            }
                        })
                        .playOn(timerLayout);


            }
        });
    }

    private void setFlipCameraClickListener() {

        flip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleCamera();
            }
        });
    }

    private void toggleCamera() {
        if (camera.isTakingPicture() || camera.isTakingVideo()) return;
        switch (camera.toggleFacing()) {
            case BACK:
                message("Switched to back camera!", false);
                break;

            case FRONT:
                message("Switched to front camera!", false);
                break;
        }
    }

    private void setImageCaptureClickListener() {

        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedTimer == 0) {
                    capturePictureSnapshot();
                    //capturePicture();
                } else {
                    timerCounter.setVisibility(View.VISIBLE);
                    timerCounter.setText(String.valueOf(selectedTimer));
                    waitTimer = new CountDownTimer(selectedTimer * 1000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);
                            timerCounter.setText(String.valueOf(seconds));
                        }

                        public void onFinish() {
                            selectedTimer = 0;
                            // capturePicture();
                            capturePictureSnapshot();
                            timerCounter.setVisibility(View.GONE);

                        }
                    }.start();

                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra("openCamera")==null)
            super.onBackPressed();
        else {
            startActivity(new Intent(this, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }

    private class Listener extends CameraListener {


        @Override
        public void onCameraOpened(@NonNull CameraOptions options) {
            Logger.d("mn13option", options.toString());


                /*YoYo.with(Techniques.FadeIn)
                        .duration(500)
                        .playOn(camera);*/
           /* AnimatorSet set;
            camera.setCameraDistance(10 * camera.getWidth());
            set = (AnimatorSet) AnimatorInflater.loadAnimator(OpenCamera.this, com.otaliastudios.cameraview.R.anim.flipping);
            set.setTarget(camera);
            set.setDuration(500);
            set.start();*/

            int cameraid = 0;
            if (camera.getFacing() == Facing.BACK) {
                cameraid = 0;
            } else if (camera.getFacing() == Facing.FRONT) {
                cameraid = 1;
            }
           /* mDisplayRotation = FaceUtils.getDisplayRotation(OpenCamera.this);
            mDisplayOrientation = FaceUtils.getDisplayOrientation(mDisplayRotation, cameraid);


            if (mFaceView != null) {
                mFaceView.setDisplayOrientation(mDisplayOrientation);
            }*/



        }

        @Override
        public void onOrientationChanged(int orientation) {
            super.onOrientationChanged(orientation);
            Logger.d("mn13option1", orientation + "");

        }

        @Override
        public void onCameraError(@NonNull CameraException exception) {
            super.onCameraError(exception);
            message("Got CameraException #" + exception.getReason(), true);
        }

        @Override
        public void onPictureTaken(@NonNull PictureResult result) {
            super.onPictureTaken(result);
            if (camera.isTakingVideo()) {
                message("Captured while taking video. Size=" + result.getSize(), false);
                return;
            }

            try {

                File directory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                try {
                    File photo = new File(directory, Calendar.getInstance().getTimeInMillis() + ".jpg");
                    FileOutputStream fo = new FileOutputStream(photo);
                    fo.write(result.getData());
                    fo.close();

                    /*MediaActionSound sound = new MediaActionSound();
                    sound.play(MediaActionSound.SHUTTER_CLICK);*/



                    //Previous Photo editor code
                    startActivityForResult(new Intent(OpenCamera.this,
                                    EditPhotoActivity.class).putExtra("imagePath",photo.getAbsolutePath()),
                            FILTERED_IMAGE);


                    /*ArrayList<String> paths = new ArrayList<>();
                    paths.add(photo.getAbsolutePath());
                    Intent intent = new Intent();
                    intent.putExtra("imagePath",paths);
                    setResult(RESULT_OK,intent);
                    finish();*/

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            captureTime = 0;
            LOG.w("onPictureTaken called! Launched activity.");
        }

        /*private void callPhotoEditor(String absolutePath) {
            Logger.d("imgURI0",""+Uri.parse(absolutePath));

            Intent dsPhotoEditorIntent = new Intent(OpenCamera.this, DsPhotoEditorActivity.class);
            dsPhotoEditorIntent.setData(Uri.parse(absolutePath));


            // This is optional. By providing an output directory, the edited photo
            // will be saved in the specified folder on your device's external storage;
            // If this is omitted, the edited photo will be saved to a folder
            // named "DS_Photo_Editor" by default.
            dsPhotoEditorIntent.putExtra(DsPhotoEditorConstants.DS_PHOTO_EDITOR_OUTPUT_DIRECTORY, OUTPUT_PHOTO_DIRECTORY);

            // You can also hide some tools you don't need as below
//                        int[] toolsToHide = {DsPhotoEditorActivity.TOOL_PIXELATE, DsPhotoEditorActivity.TOOL_ORIENTATION};
//                        dsPhotoEditorIntent.putExtra(DsPhotoEditorConstants.DS_PHOTO_EDITOR_TOOLS_TO_HIDE, toolsToHide);

            startActivityForResult(dsPhotoEditorIntent, PHOTO_EDITOR_REQUEST_CODE);
        }*/

        @Override
        public void onVideoTaken(@NonNull VideoResult result) {
            super.onVideoTaken(result);
            LOG.w("onVideoTaken called! Launching activity.");
           /* VideoPreviewActivity.setVideoResult(result);
            Intent intent = new Intent(CameraActivity.this, VideoPreviewActivity.class);
            startActivity(intent);*/
            LOG.w("onVideoTaken called! Launched activity.");
        }

        @Override
        public void onVideoRecordingStart() {
            super.onVideoRecordingStart();
            LOG.w("onVideoRecordingStart!");
        }

        @Override
        public void onVideoRecordingEnd() {
            super.onVideoRecordingEnd();
            message("Video taken. Processing...", false);
            LOG.w("onVideoRecordingEnd!");
        }

        @Override
        public void onExposureCorrectionChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
            super.onExposureCorrectionChanged(newValue, bounds, fingers);
            message("Exposure correction:" + newValue, false);
        }


        @Override
        public void onZoomChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
            super.onZoomChanged(newValue, bounds, fingers);
            message("Zoom:" + newValue, false);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILTERED_IMAGE && resultCode == RESULT_OK && data!=null) {
            ArrayList<String> mPaths = data.getStringArrayListExtra("imagePath");
            if (mPaths!=null) {
                Intent intent = new Intent();
                intent.putExtra("imagePath",mPaths);
                setResult(RESULT_OK,intent);
                finish();
            }
        }else if(requestCode == DS_PHOTO_EDITOR_REQUEST_CODE && resultCode == RESULT_OK && data!=null){
            Uri outputUri = data.getData();
            //imageView.setImageURI(outputUri);
            Toast.makeText(this, "Photo saved in " + OUTPUT_PHOTO_DIRECTORY + " folder.", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean valid = true;
        for (int grantResult : grantResults) {
            valid = valid && grantResult == PackageManager.PERMISSION_GRANTED;
        }
        if (valid && !camera.isOpened()) {
            camera.open();
        }
    }

    private void capturePictureSnapshot() {
        if (camera.isTakingPicture()) return;
        if (camera.getPreview() != Preview.GL_SURFACE) {
            message("Picture snapshots are only allowed with the GL_SURFACE preview.", true);
            return;
        }
        captureTime = System.currentTimeMillis();
        message("Capturing picture snapshot...", false);
        camera.setPictureMetering(true);
        camera.takePictureSnapshot();
    }

    private void capturePicture() {
        if (camera.getMode() == Mode.VIDEO) {
            message("Can't take HQ pictures while in VIDEO mode.", false);
            return;
        }
        if (camera.isTakingPicture()) return;
        captureTime = System.currentTimeMillis();
        message("Capturing picture...", false);
        camera.takePicture();
    }



}
