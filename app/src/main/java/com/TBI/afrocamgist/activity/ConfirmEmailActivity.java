package com.TBI.afrocamgist.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.text.HtmlCompat;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.Params;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ConfirmEmailActivity extends BaseActivity {

    private TextView note,txtResendOtp;
    private EditText otp;
    private KProgressHUD hud;
    private String username = "", registered_with = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_email);

        if (getIntent().getStringExtra("username")!=null)
            username = getIntent().getStringExtra("username");

        if(getIntent().getStringExtra("registered_with") != null)
            registered_with = getIntent().getStringExtra("registered_with");

        initView();
        showSuccessAlert();

        String noteText = "Please enter the OTP sent to <b>\"" + username + "\"</b> to verify your account.";
        note.setText(HtmlCompat.fromHtml(noteText, HtmlCompat.FROM_HTML_MODE_COMPACT));

        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.confirm_otp).setOnClickListener(v -> {
            if (otp.getText().toString().isEmpty())
                Utils.showAlert(this, getString(R.string.please_enter_valid_otp));
            else
                confirmOTP();
        });


        txtResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOTP();
            }
        });
    }

    private void initView() {

        otp = findViewById(R.id.otp);
        note = findViewById(R.id.note);
        txtResendOtp = findViewById(R.id.txtResendOtp);
    }

    //Resend otp API call.
    private void resendOTP() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        ArrayList<Params> params = new ArrayList<>();
        params.add(new Params(Constants.USER_NAME, username));
        params.add(new Params("type",registered_with));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE,"application/x-www-form-urlencoded");

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.RESEND_OTP, response -> {
            hud.dismiss();
            try {
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Toast.makeText(ConfirmEmailActivity.this,""+object.getString(Constants.MESSAGE),Toast.LENGTH_SHORT).show();

                }

            } catch (Exception e) {
//                Logger.e("LLLLL_Tok: ",e.getMessage());
                Utils.showAlert(ConfirmEmailActivity.this,getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }

    private void showSuccessAlert() {
        final Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dialog_error);
        popup.setCancelable(true);
        popup.show();

        TextView error = popup.findViewById(R.id.message);
        //String message = "Success!! \n Please check your email to activate your account.";
        String message = getString(R.string.please_check_your_email_for_otp_thanks);

        error.setText(message);

        popup.findViewById(R.id.error).setVisibility(View.GONE);
        popup.findViewById(R.id.success).setVisibility(View.VISIBLE);

        popup.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    //confirm otp API call.
    private void confirmOTP() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        ArrayList<Params> params = new ArrayList<>();
        params.add(new Params(Constants.USER_NAME, username));
        params.add(new Params(Constants.OTP,otp.getText().toString()));

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE,"application/x-www-form-urlencoded");

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.VERIFY_EMAIL, response -> {
            hud.dismiss();
            try {
                JSONObject object = new JSONObject(response);

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(ConfirmEmailActivity.this,object.getString(Constants.MESSAGE));
                } else {
                    LocalStorage.saveLoginToken(object.getString(Constants.TOKEN));
                    startActivity(new Intent(ConfirmEmailActivity.this, SettingsActivity.class).putExtra("fromSignUp", "fromSignUp"));
                    finish();
                }

            } catch (Exception e) {
                Logger.e("LLLLL_Tok: ",e.getMessage());
                Utils.showAlert(ConfirmEmailActivity.this,getString(R.string.opps_something_went_wrong));
                e.printStackTrace();
            }
        });
    }
}
