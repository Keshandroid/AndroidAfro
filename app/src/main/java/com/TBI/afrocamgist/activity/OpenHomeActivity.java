package com.TBI.afrocamgist.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.TBI.afrocamgist.Identity;
import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.StoryConstants;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.VideoPreLoadingService;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.fragment.NewAfroSwaggerFragment;
import com.TBI.afrocamgist.home.OpenHomeCommentBottomSheetFragment;
import com.TBI.afrocamgist.home.OpenHomeAdapter;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.connection.ConnectivityListener;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.post.PostDetails;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import im.ene.toro.widget.Container;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OpenHomeActivity extends BaseActivity implements ConnectivityListener, View.OnClickListener,
        OpenHomeAdapter.OnSwaggerPostClickListener, OpenHomeCommentBottomSheetFragment.DialogDismissListener{

    private KProgressHUD hud;
    private Dialog popupGuide;
    private int nextPage = 1;
    private ArrayList<Post> swaggerPosts = new ArrayList<>();
    OpenHomeAdapter videosAdapter;
    private boolean isLoading = false;
    SnapHelper snapHelper;
    private Container mainList;
    private int position;
    private TextView txtLogin;
    private static int SAVED_SCROLL_POSITION = -1;

    //Download Video from url
    String fileN = null ;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123;
    boolean result;
    ProgressDialog mProgressDialog;
    private String waterMarkVideo;
    private Dialog popup;
    private String shareOrDownload = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_home);

        initView();
        initClickListener();

        if (Utils.isConnected()) {
            getSwaggerFirstPagePosts(true);
        }

    }

    private void initView() {

        //videoViewPager = view.findViewById(R.id.videosViewPager);

        mainList = findViewById(R.id.main_list);
        txtLogin = findViewById(R.id.txtLogin);
    }

    private void initClickListener(){
        txtLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtLogin:
                startActivity(new Intent(OpenHomeActivity.this, SignUpActivity.class));

                break;
            default:
                break;
        }
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {

        }
    }

    private void getSwaggerFirstPagePosts(boolean needToLoader) {
        if(needToLoader) {
            hud = KProgressHUD.create(OpenHomeActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setDimAmount(0.5f)
                    .setCancellable(true)
                    .show();
        }

        String url = UrlEndpoints.GET_POPULAR_POST + "?page=1" + "&user_id=" + LocalStorage.getUserDetails().getUserId();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

       /* Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());*/


        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            if(needToLoader)
                hud.dismiss();

            if(!Identity.getDialogGuidePreference(OpenHomeActivity.this)){
                showGuideVideo(OpenHomeActivity.this);
            }

            if ("".equals(response)) {
                //Utils.showAlert(NewPopularPostActivity.this, "Oops something went wrong....");
            } else {
                try {
                    Logger.e("LLLLLL)_Res: ", response);

                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(OpenHomeActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroSwagger = new Gson().fromJson(response, PostDetails.class);
                        if (afroSwagger.getPosts().size() > 0) {
                            nextPage = afroSwagger.getNextPage();
                            startPreLoadingService(afroSwagger.getPosts());
                            swaggerPosts = new ArrayList<>();
                            swaggerPosts.addAll(afroSwagger.getPosts());

                            //setViewPager();
                            setVideosAdapter();
                            setRecyclerView();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(NewPopularPostActivity.this, "Oops something went wrong....");
                }
            }
        });
    }

    private void getSwaggerNextPagePosts() {

        String url = UrlEndpoints.GET_POPULAR_POST + "?page=" + nextPage + "&user_id=" + LocalStorage.getUserDetails().getUserId();

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, response -> {
            isLoading = false;
            if (!"".equals(response)) {

                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(OpenHomeActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                        PostDetails afroSwagger = new Gson().fromJson(response, PostDetails.class);

                        if (afroSwagger.getPosts() != null && afroSwagger.getPosts().size() > 0) {
                            nextPage = afroSwagger.getNextPage();
                            startPreLoadingService(afroSwagger.getPosts());
                            swaggerPosts.addAll(afroSwagger.getPosts());

                            videosAdapter.notifyDataSetChanged();

                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setVideosAdapter(){
        ViewCompat.setNestedScrollingEnabled(mainList, false);
        mainList.setNestedScrollingEnabled(false);
        mainList.setLayoutManager(new LinearLayoutManager(OpenHomeActivity.this, RecyclerView.VERTICAL, false));
        mainList.setItemAnimator(new DefaultItemAnimator());
        videosAdapter = new OpenHomeAdapter(OpenHomeActivity.this,swaggerPosts,this);
        mainList.setAdapter(videosAdapter);

        snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(mainList);

        //swipe to open user profile screen
        /*ItemTouchHelper.Callback callback=new PopularPostSwipeHelper(videosAdapter, NewPopularPostActivity.this);
        ItemTouchHelper helper=new ItemTouchHelper(callback);
        helper.attachToRecyclerView(mainList);*/
    }

    private void setRecyclerView() {
        mainList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE) {
                    View centerView = snapHelper.findSnapView(recyclerView.getLayoutManager());
                    position = recyclerView.getLayoutManager().getPosition(centerView);
                    Logger.e("Snapped Item Position:",""+position);
                    if ((position +1) <= swaggerPosts.size()-1) {
                        Logger.e("LLLLL_Left: ",""+position);
                        if (mainList.getLayoutManager() != null) {
                            //setLikesAndCommentsData(position); // modified
                        }
                    }else{
                        if (!isLoading) {
                            isLoading = true;
                            Logger.e("LLLLL_Left: ","Else :"+position);
                            //getHashtagNextPage();
                            getSwaggerNextPagePosts();
                        }
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });



    }

    private void startPreLoadingService(ArrayList<Post> posts) {

        ArrayList<String> videoList = new ArrayList<>();

        for (int i=0;i<posts.size();i++){
            if(posts.get(i).getPostType().equalsIgnoreCase("video")){
                String url = UrlEndpoints.MEDIA_BASE_URL + posts.get(i).getPostVideo();
                videoList.add(url);
            }

            if(posts.get(i).getPostType().equalsIgnoreCase("shared")){
                if(posts.get(i).getSharedPost() != null){
                    if(posts.get(i).getSharedPost().getPostType().equalsIgnoreCase("video")){
                        String url = UrlEndpoints.MEDIA_BASE_URL + posts.get(i).getSharedPost().getPostVideo();
                        videoList.add(url);
                    }
                }
            }

        }

        Logger.d("videoUrls",new GsonBuilder().setPrettyPrinting().create().toJson(videoList));

        Intent preloadingServiceIntent = new Intent(OpenHomeActivity.this, VideoPreLoadingService.class);
        preloadingServiceIntent.putStringArrayListExtra(StoryConstants.VIDEO_LIST, videoList);
        startService(preloadingServiceIntent);
    }

    private void showGuideVideo(Activity activity) {

        if (activity!=null && !activity.isFinishing()) {
            popupGuide = new Dialog(activity, R.style.DialogCustom);
            popupGuide.setContentView(R.layout.dialog_guide_user);
            popupGuide.setCancelable(false);
            popupGuide.show();

            TextView okButton = popupGuide.findViewById(R.id.ok);

            VideoView videoView = popupGuide.findViewById(R.id.videoGuide);
            String path = "android.resource://" + OpenHomeActivity.this.getPackageName() + "/" + R.raw.slow_guide_video;
            videoView.setVideoURI(Uri.parse(path));
            videoView.start();

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                }
            });

           /* videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Identity.setDialogGuidePreference(activity,true);
                    popupGuide.dismiss();
                }
            });*/

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Identity.setDialogGuidePreference(activity,true);
                    popupGuide.dismiss();
                }
            });
        }
    }

    @Override
    public void onDialogDismiss(ArrayList<Comment> comments, OpenHomeAdapter.VideoViewHolder holder) {
        if(comments!=null){
            updateCommentCount(comments,holder);
        }
    }

    private void updateCommentCount(ArrayList<Comment> comments, OpenHomeAdapter.VideoViewHolder holder) {

        if (swaggerPosts.size() > 0) {
            int commentCount = 0;
            for (Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments() != null && comment.getSubComments().size() > 0) {
                    commentCount += comment.getSubComments().size();
                }
            }
            swaggerPosts.get(SAVED_SCROLL_POSITION).setComments(comments);
            swaggerPosts.get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);


            /*if (videosAdapter != null) {
                videosAdapter.notifyItemChanged(SAVED_SCROLL_POSITION);
            }*/

            if(videosAdapter!=null){
                videosAdapter.onCommentDialogDismiss(SAVED_SCROLL_POSITION,commentCount,holder);
            }

        }
    }

    @Override
    public void onPauseVideo(Post post, SimpleExoPlayer simpleExoPlayer) {

    }

    @Override
    public void onPostChecked(Post post, int position, boolean isDoubleClicked) {
        likeAndUnlikePost(post.getPostId(),position,post,isDoubleClicked);
    }

    private void likeAndUnlikePost(Integer postId, int position, Post post, boolean isDoubleClicked) {

        JSONObject params = new JSONObject();
        try {
            params.put("post_id", postId);
            params.put("user_id", LocalStorage.getUserDetails().getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Logger.d("NON_REGISTER_ID",""+postId);
        Logger.d("NON_REGISTER_USER_ID",""+LocalStorage.getUserDetails().getUserId());

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        /*Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());*/

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.LIKE_POST, response -> {
            if ("".equals(response)) {
                Utils.showAlert(OpenHomeActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(OpenHomeActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        Logger.d("NON_REGISTER_LIKE",""+ new GsonBuilder().setPrettyPrinting().create().toJson(object));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(OpenHomeActivity.this, "Oops something went wrong....");
                }
            }
        });
    }

    @Override
    public void onFollowClicked(Post post) {
        startActivity(new Intent(OpenHomeActivity.this, SignUpActivity.class));
    }

    @Override
    public void onCommentClicked(Post post, int position, OpenHomeAdapter.VideoViewHolder holder) {
        SAVED_SCROLL_POSITION = position;
        OpenHomeCommentBottomSheetFragment openHomeCommentBottomSheetFragment = new OpenHomeCommentBottomSheetFragment();
        OpenHomeCommentBottomSheetFragment.newInstance(holder, post.getComments(),post.getPostId(),this).show(getSupportFragmentManager(), openHomeCommentBottomSheetFragment.getTag());
    }

    @Override
    public void onReportClicked(Integer userId) {

    }

    @Override
    public void onProfileClicked(Integer userId) {

    }

    @Override
    public void onPostView(Integer postId) {
        callPostView(postId);
    }

    private void callPostView(int postId) {

        JSONObject request = new JSONObject();
        try {
            request.put("user_id", LocalStorage.getUserDetails().getUserId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        String url = "open" + "/" + postId + "/most-popular-seen";

        Webservices.getData(Webservices.Method.PUT, request,headers, url, response -> {
            try {
                JSONObject object = new JSONObject(response);
                Logger.d("postview",""+object.toString());

                if (object.has(Constants.MESSAGE)) {
                    Utils.showAlert(OpenHomeActivity.this, object.getString(Constants.MESSAGE));
                } else {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onPlayItem(int adapterPosition) {

    }

    @Override
    public void onVideoCount(Integer postId) {
        callVideoCountNew(postId);
    }

    private void callVideoCountNew(int videoPostId){
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getOpenVideoCount(videoPostId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("ViewCount",""+jsonObject.get("counter"));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onPostViewCount(Integer postId) {
        callPostViewCount(postId);
    }

    private void callPostViewCount(int postId) {
        Call<JsonObject> call = BaseServices.getAPI().create(ParameterServices.class).getOpenPostViewCount(postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if(response.isSuccessful()){
                    if(jsonObject!=null){
                        Logger.d("onPostViewOpen",""+jsonObject.toString());
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                Logger.d("failure1",t.getLocalizedMessage()+"");
            }
        });
    }

    @Override
    public void onCreatePostClicked() {
        startActivity(new Intent(OpenHomeActivity.this, SignUpActivity.class));
    }

    @Override
    public void onShareClicked(Post post, Activity context) {

    }

    @Override
    public void onDownloadClick(String videoUrl, String postLink) {
        showShareSelectionDialog(OpenHomeActivity.this,videoUrl,postLink);
    }

    public void showShareSelectionDialog(Context context, String videoUrl, String postLink) {

        if (context!=null) {
            popup = new Dialog(context, R.style.DialogCustom);
            popup.setContentView(R.layout.dialog_social_share);
            popup.setCancelable(true);
            popup.show();

            RelativeLayout download = popup.findViewById(R.id.rlDownload);
            RelativeLayout share = popup.findViewById(R.id.rlShare);

            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "download";
                    downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareOrDownload = "share";
                    shareVideoLink(postLink);
                    //downloadVideo(videoUrl);
                    popup.dismiss();
                }
            });

        }
    }

    private void downloadVideo(String videoUrl){
        String originalFilename = videoUrl.substring(videoUrl.lastIndexOf("/")+1);
        result = checkPermission();
        if(result){
            checkFolder();
            if (!isConnectingToInternet(OpenHomeActivity.this)) {
                Toast.makeText(OpenHomeActivity.this, getString(R.string.please_check_your_internet_connection), Toast.LENGTH_LONG).show();
                return;
            }

            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename);
            if(file.exists()){
                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + originalFilename;
                shareVideoToSocialMedia(filePath);
            }else {
                callWatermarkVideoAPI(videoUrl);
            }
        }
    }

    //Get watermarked video from url
    private void callWatermarkVideoAPI(String videoUrl){
        waterMarkVideo = "";
        hud = KProgressHUD.create(OpenHomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setLabel("Preparing...")
                .setCancellable(false)
                .show();

        JSONObject params = new JSONObject();
        try {
            params.put("video", videoUrl);
            params.put("user_id", LocalStorage.getUserDetails().getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.OPEN_WATERMARK_VIDEO, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                Utils.showAlert(OpenHomeActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    Logger.d("downloadVideo",""+object.toString());

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(OpenHomeActivity.this, object.getString(Constants.MESSAGE));
                    }else {
                        if(object.getBoolean("result")){
                            waterMarkVideo = object.getString("video_path");
                            String watermarkVideo = UrlEndpoints.MEDIA_BASE_URL + object.getString("video_path");
                            newDownload(watermarkVideo);
                        }else {
                            Utils.showAlert(OpenHomeActivity.this, getString(R.string.opps_something_went_wrong));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //delete watermark video from server
    private void deleteWatermarkVideoInServer(){
        JSONObject params = new JSONObject();
        try {
            params.put("video", waterMarkVideo);
            params.put("user_id", LocalStorage.getUserDetails().getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/x-www-form-urlencoded");

        String url = UrlEndpoints.OPEN_DELETE_WATERMARK_VIDEO;

        Webservices.getData(Webservices.Method.DELETE, params, headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);
                    Logger.d("waterMark99","success");
                    if (object.has(Constants.MESSAGE)) {
                        //Utils.showAlert(OpenHomeActivity.this, object.getString(Constants.MESSAGE));
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    private void shareVideoLink(String videoUrl){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, "Afrocamgist");
        i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
        i.putExtra(Intent.EXTRA_TEXT, videoUrl);
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    private void shareVideoToSocialMedia(String filePath){

        MediaScannerConnection.scanFile(OpenHomeActivity.this, new String[] { filePath },

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                        shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, "Afrocamgist");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                        startActivity(Intent.createChooser(shareIntent,
                                "Afrocamgist"));

                    }
                });
    }

    //hare you can start downloding video
    public void newDownload(String url) {
        final DownloadTask downloadTask = new DownloadTask(OpenHomeActivity.this);
        downloadTask.execute(url);
    }

    // DownloadTask for downloding video from URL
    public class DownloadTask extends AsyncTask<String, Integer, String> {
        private Context context;
        private PowerManager.WakeLock mWakeLock;
        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                java.net.URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                input = connection.getInputStream();


                String filePath = sUrl[0];
                fileN = filePath.substring(filePath.lastIndexOf("/")+1);
                //fileN = "Afrocamgist_" + UUID.randomUUID().toString().substring(0, 10) + ".mp4";
                File filename = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/", fileN);
                output = new FileOutputStream(filename);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();

            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);

            mProgressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();

            if (result != null){
                Toast.makeText(context, getString(R.string.str_download_err) + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, getString(R.string.str_video_downloaded), Toast.LENGTH_SHORT).show();
                deleteWatermarkVideoInServer();
            }

            MediaScannerConnection.scanFile(OpenHomeActivity.this,
                    new String[]{Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/AfrocamgistVideos/" + fileN}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String newpath, Uri newuri) {
                            Logger.i("ExternalStorage", "Scanned " + newpath + ":");
                            Logger.i("ExternalStorage", "-> uri=" + newuri);
                        }
                    });

            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/" + fileN;
            shareVideoToSocialMedia(filePath);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(OpenHomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(OpenHomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(OpenHomeActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(OpenHomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(OpenHomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public void checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(OpenHomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(OpenHomeActivity.this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(OpenHomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(OpenHomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }

    //Here you can check App Permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkFolder();
                } else {
                    //code for deny
                    checkAgain();
                }
                break;
        }
    }

    //hare you can check folfer whare you want to store download Video
    public void checkFolder() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AfrocamgistVideos/";
        File dir = new File(path);
        boolean isDirectoryCreated = dir.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated = dir.mkdir();
        }
        if (isDirectoryCreated) {
            // do something
            Logger.d("Folder", "Already Created");
        }
    }
    
    @Override
    public void openDetailScreen(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    public void onEditPost(Post post, int position) {

    }

    @Override
    public void onDeletePost(Post post) {

    }

    @Override
    public void onHidePost(Post post) {

    }
}
