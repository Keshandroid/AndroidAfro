package com.TBI.afrocamgist.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.TBI.afrocamgist.BitmapUtils;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.fragment.EditImageFragment;
import com.TBI.afrocamgist.fragment.FiltersListFragment;
import com.google.android.material.tabs.TabLayout;
import com.TBI.photofilters.imageprocessors.Filter;
import com.TBI.photofilters.imageprocessors.subfilters.BrightnessSubFilter;
import com.TBI.photofilters.imageprocessors.subfilters.ContrastSubFilter;
import com.TBI.photofilters.imageprocessors.subfilters.SaturationSubFilter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//Filter and edit image screen
public class EditPhotoActivity extends BaseActivity implements FiltersListFragment.FiltersListFragmentListener,
        EditImageFragment.EditImageFragmentListener, View.OnClickListener {

    private ImageView imagePreview;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Bitmap originalImage;
    private Bitmap filteredImage;
    private Bitmap finalImage;

    FiltersListFragment filtersListFragment;
    private EditImageFragment editImageFragment;

    private int brightnessFinal = 0;
    private float saturationFinal = 1.0f;
    private float contrastFinal = 1.0f;
    public static String imagePath = "";

    static {
        System.loadLibrary("NativeImageProcessor");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_photo);

        if (getIntent().getStringExtra("imagePath")!=null)
            imagePath = getIntent().getStringExtra("imagePath");

        initView();
        initClickListener();
        loadImage();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initView() {
        imagePreview = findViewById(R.id.image);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
    }

    private void initClickListener() {
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.done).setOnClickListener(this);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        filtersListFragment = new FiltersListFragment();
        filtersListFragment.setListener(this);

        editImageFragment = new EditImageFragment();
        editImageFragment.setListener(this);

        adapter.addFragment(filtersListFragment, "Filters");
        adapter.addFragment(editImageFragment, "Edit");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onFilterSelected(Filter filter) {

        resetControls();

        filteredImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        imagePreview.setImageBitmap(filter.processFilter(filteredImage));
        finalImage = filteredImage.copy(Bitmap.Config.ARGB_8888, true);
    }

    @Override
    public void onBrightnessChanged(final int brightness) {
        brightnessFinal = brightness;
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightness));
        imagePreview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)));
    }

    @Override
    public void onSaturationChanged(final float saturation) {
        saturationFinal = saturation;
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new SaturationSubFilter(saturation));
        imagePreview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)));
    }

    @Override
    public void onContrastChanged(final float contrast) {
        contrastFinal = contrast;
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new ContrastSubFilter(contrast));
        imagePreview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)));
    }

    @Override
    public void onEditStarted() {

    }

    @Override
    public void onEditCompleted() {
        final Bitmap bitmap = filteredImage.copy(Bitmap.Config.ARGB_8888, true);

        Filter myFilter = new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightnessFinal));
        myFilter.addSubFilter(new ContrastSubFilter(contrastFinal));
        myFilter.addSubFilter(new SaturationSubFilter(saturationFinal));
        finalImage = myFilter.processFilter(bitmap);
    }

    private void resetControls() {
        if (editImageFragment != null) {
            editImageFragment.resetControls();
        }
        brightnessFinal = 0;
        saturationFinal = 1.0f;
        contrastFinal = 1.0f;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        @SuppressLint("WrongConstant")
        ViewPagerAdapter(FragmentManager manager) {
            super(manager, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void loadImage() {

        originalImage = BitmapUtils.getBitmapFromUri(this, Uri.fromFile(new File(imagePath)));

        filteredImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        finalImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);

        imagePreview.setImageBitmap(originalImage);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.done:

//                Log.d("Images999", ""+BitmapUtils.getFilePathForBitmap(EditPhotoActivity.this,finalImage));

                ArrayList<String> imagePaths = new ArrayList<>();
                imagePaths.add(BitmapUtils.getFilePathForBitmap(EditPhotoActivity.this,finalImage));
                Intent intent = new Intent();
                intent.putExtra("imagePath",imagePaths);
                setResult(RESULT_OK,intent);
                finish();
                break;
            default:
                break;
        }
    }
}
