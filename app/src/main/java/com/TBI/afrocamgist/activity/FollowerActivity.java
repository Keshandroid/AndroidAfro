package com.TBI.afrocamgist.activity;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.adapters.FollowerAdapter;
import com.TBI.afrocamgist.model.follow.Follow;

import java.util.ArrayList;

public class FollowerActivity extends BaseActivity {

    private ArrayList<Follow> followersList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follower);

        if (getIntent().getSerializableExtra("followers")!=null)
            followersList = (ArrayList<Follow>) getIntent().getSerializableExtra("followers");

        initView();
        setRecyclerView(followersList);
        setClickListener();

        ((SwipeRefreshLayout)findViewById(R.id.swipeContainer)).setRefreshing(false);
        findViewById(R.id.swipeContainer).setEnabled(false);
    }

    private void initView() {

        TextView followerCount = findViewById(R.id.follower_count);
        String count = followersList== null ? "0" : followersList.size() + "";
        followerCount.setText(count);
    }

    //set followers list in the view
    private void setRecyclerView(ArrayList<Follow> followers) {

        RecyclerView followerList = findViewById(R.id.follower_list);
        followerList.setLayoutManager(new LinearLayoutManager(this));
        followerList.setAdapter(new FollowerAdapter(this, followers));
    }

    private void setClickListener() {

        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });
    }
}
