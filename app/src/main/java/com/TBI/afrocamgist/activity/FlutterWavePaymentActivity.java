package com.TBI.afrocamgist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.model.ehealth.FlutterwaveCreateResponse;
import com.TBI.afrocamgist.model.ehealth.FlutterwaveResponse;
import com.flutterwave.raveandroid.RaveConstants;
import com.flutterwave.raveandroid.RavePayActivity;
import com.flutterwave.raveandroid.RavePayManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FlutterWavePaymentActivity extends BaseActivity {

    private TextView txtName,txtPlanName,txtAmount;
    private String eHealthPrice="",eHealthPlanID="",eHealthPlanName="",eHealthFirstName="",eHealthLastName="",eHealthEmail="";
    Button pay;
    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flutter_wave_payment);

        //get intent values
        if(getIntent().getStringExtra("ehealth_plan_id") != null){
            eHealthPlanID = getIntent().getStringExtra("ehealth_plan_id");
        }

        if(getIntent().getStringExtra("ehealth_price") != null){
            eHealthPrice = getIntent().getStringExtra("ehealth_price");
        }

        if(getIntent().getStringExtra("ehealth_plan_name") != null){
            eHealthPlanName = getIntent().getStringExtra("ehealth_plan_name");
        }

        if(getIntent().getStringExtra("ehealth_first_name") != null){
            eHealthFirstName = getIntent().getStringExtra("ehealth_first_name");
        }

        if(getIntent().getStringExtra("ehealth_last_name") != null){
            eHealthLastName = getIntent().getStringExtra("ehealth_last_name");
        }

        if(getIntent().getStringExtra("ehealth_email") != null){
            eHealthEmail = getIntent().getStringExtra("ehealth_email");
        }


        pay = findViewById(R.id.pay);

        txtName = findViewById(R.id.txtName);
        txtPlanName = findViewById(R.id.txtPlanName);
        txtAmount = findViewById(R.id.txtAmount);

        txtName.setText("Name : "+eHealthFirstName +" "+eHealthLastName);
        txtPlanName.setText("Health Insurance Plan : "+eHealthPlanName);
        txtAmount.setText("Cost : "+eHealthPrice);

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createFlutterWavePayment();
            }
        });



    }

    private void makePayment(FlutterwaveResponse customerData) {

        new RavePayManager(this)
                .setAmount(Double.parseDouble(eHealthPrice.replaceAll("[^0-9]", "")))
                .setEmail(customerData.getCustomer().getEmail())
                .setCountry("NG")
                .setCurrency("NGN")
                .setfName(customerData.getCustomer().getName())
                //.setlName(customerData.getCustomer().getName())
                .setNarration(eHealthPlanName)
                .setPublicKey("FLWPUBK_TEST-fa732eeb18a44ea93f1a5bfef0d01cef-X") //PublicKey
                .setEncryptionKey("FLWSECK_TEST3947494dfaa3") //EncryptionKey
                .setTxRef(customerData.getTx_ref())
                .acceptAccountPayments(true)
                .acceptCardPayments(true)
                .acceptMpesaPayments(true)
                .onStagingEnv(true)
                .shouldDisplayFee(true)
                .showStagingLabel(true)
                .initialize();

            /*new RavePayManager(this)
                    .setAmount(Double.parseDouble("500"))
                    .setEmail("test@gmail.com")
                    .setCountry("NG")
                    .setCurrency("NGN")
                    .setfName("Android")
                    .setlName("User")
                    .setNarration("Purchase Goods")
                    .setPublicKey("FLWPUBK_TEST-fa732eeb18a44ea93f1a5bfef0d01cef-X") //PublicKey
                    .setEncryptionKey("FLWSECK_TEST3947494dfaa3") //EncryptionKey
                    .setTxRef(System.currentTimeMillis() + "Ref")
                    .acceptAccountPayments(true)
                    .acceptCardPayments(true)
                    .acceptMpesaPayments(true)
                    .onStagingEnv(true)
                    .shouldDisplayFee(true)
                    .showStagingLabel(true)
                    .initialize();*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            String message = data.getStringExtra("response");
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {

                Logger.d("PaymentIntent", message);

                sendDataToDatabase(message);
            }
            else if (resultCode == RavePayActivity.RESULT_ERROR) {
                Toast.makeText(this, "ERROR " + message, Toast.LENGTH_LONG).show();
                finish();
            }
            else if (resultCode == RavePayActivity.RESULT_CANCELLED) {
                Toast.makeText(this, "CANCELLED " + message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void createFlutterWavePayment(){

        hud = KProgressHUD.create(FlutterWavePaymentActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .show();

        JSONObject request = new JSONObject();
        try {
            request.put("amount",eHealthPrice.replaceAll("[^0-9]", ""));
            request.put("currency","NGN");
            request.put("plan_id",eHealthPlanID);

            JSONObject customerObject = new JSONObject();
            customerObject.put("isNewCustomer",false);
            customerObject.put("first_name",eHealthFirstName);
            customerObject.put("last_name",eHealthLastName);
            customerObject.put("email",eHealthEmail);
            //customerObject.put("user_id",LocalStorage.getUserDetails().getUserId());//optional

            request.put("customer",customerObject);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.CREATE_FLUTTERWAVE_PAYMENT;

        Webservices.getData(Webservices.Method.POST, request, headers, url, response -> {
            hud.dismiss();
            if ("".equals(response)) {
                //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    Logger.d("CREATE_PAY","Fluterwave customer : "+new GsonBuilder().setPrettyPrinting().create().toJson(response));

                    FlutterwaveCreateResponse flutterwaveCreateResponse = new Gson().fromJson(response, FlutterwaveCreateResponse.class);

                    if(flutterwaveCreateResponse.getStatus()){
                        if (flutterwaveCreateResponse.getFlutterwaveResponse().getLink() != null &&
                                !flutterwaveCreateResponse.getFlutterwaveResponse().getLink().isEmpty()) {


                            //runOnUiThread(() -> pay.setEnabled(true));

                            makePayment(flutterwaveCreateResponse.getFlutterwaveResponse());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }




    private void sendDataToDatabase(String paymentResponse){

        try {
            JSONObject object = new JSONObject(paymentResponse);

            JSONObject dataObject = object.getJSONObject("data");
            String paymentId = dataObject.getString("id");
            String txRef = dataObject.getString("txRef");



            JSONObject request = new JSONObject();
            try {


                JSONObject paymentObject = new JSONObject();
                paymentObject.put("status","successful");
                paymentObject.put("tx_ref",txRef);
                paymentObject.put("transaction_id",paymentId);

                request.put("payment",paymentObject);


            } catch (Exception e) {
                e.printStackTrace();
            }

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

            String url = UrlEndpoints.E_HEALTH_SUBSCRIBE;

            Webservices.getData(Webservices.Method.POST, request, headers, url, response -> {
                if ("".equals(response)) {
                    //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
                } else {
                    try {

                        Logger.d("SUBSCRIBE"," E_HEALTH_SUBSCRIBE Response : "+new GsonBuilder().setPrettyPrinting().create().toJson(response));

                        Toast.makeText(this, "SUCCESS", Toast.LENGTH_LONG).show();
                        finish();



                    } catch (Exception e) {
                        e.printStackTrace();
                        //Utils.showAlert(AdvertCreateActivity.this, getString(R.string.opps_something_went_wrong));
                    }
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }



    }


}
