package com.TBI.afrocamgist.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import it.mirko.rangeseekbar.OnRangeSeekBarListener;
import it.mirko.rangeseekbar.RangeSeekBar;

public class AdvertSelectGoalActivity extends BaseActivity {

    Activity activity = this;
    LinearLayout ll_more_websites_visits;
    RadioButton rbProfileVisits, rbMoreWebsites;
    Button btn_next;
    EditText edt_website_url;
    String strUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert_select_goal);
        initClickListener();
    }

    private void initClickListener() {
        findViewById(R.id.back).setOnClickListener(v -> {
            onBackPressed();
        });

        rbProfileVisits = findViewById(R.id.rbProfileVisits);
        rbMoreWebsites = findViewById(R.id.rbMoreWebsites);
        ll_more_websites_visits = findViewById(R.id.ll_more_websites_visits);
        btn_next = findViewById(R.id.btn_next);
        edt_website_url = findViewById(R.id.edt_website_url);
        rbProfileVisits.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {


                    ll_more_websites_visits.setVisibility(View.GONE);
                    btn_next.setEnabled(true);
                    btn_next.setAlpha(1f);
                }
            }
        });

        rbMoreWebsites.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {


                    ll_more_websites_visits.setVisibility(View.VISIBLE);
                    if (strUrl != null && strUrl.length() > 0 && Utils.isUrlValid(strUrl)) {
                        btn_next.setEnabled(true);
                        btn_next.setAlpha(1f);
                    } else {
                        btn_next.setEnabled(false);
                        btn_next.setAlpha(0.7f);
                    }
                }
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(rbProfileVisits.isChecked()){
                    LocalStorage.setAdvertGoalType("More profile visits");
                    LocalStorage.setAdvertURL("/profile/"+LocalStorage.getUserDetails().getUserId());
                }else {
                    LocalStorage.setAdvertGoalType("More website visits");
                    LocalStorage.setAdvertURL(strUrl);
                }

                //startActivity(new Intent(activity, AdvertSelectTargetAudienceActivity.class));
                startActivity(new Intent(activity, AdvertExistingAudienceActivity.class));
            }
        });

        edt_website_url.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                strUrl = charSequence.toString();
                if (strUrl != null && strUrl.length() > 0 && Utils.isUrlValid(strUrl)) {
                    btn_next.setEnabled(true);
                    btn_next.setAlpha(1f);
                } else {
                    btn_next.setEnabled(false);
                    btn_next.setAlpha(0.7f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}