package com.TBI.afrocamgist.activity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.VideoView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.Logger;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.StoryConstants;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.api.BaseServices;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.app.AfrocamgistApplication;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.fragment.StoryCommentBottomSheetFragment;
import com.TBI.afrocamgist.listener.OnTaskCompleted;
import com.TBI.afrocamgist.listener.ParameterServices;
import com.TBI.afrocamgist.model.comment.Comment;
import com.TBI.afrocamgist.model.post.Post;
import com.TBI.afrocamgist.model.story.StoryUserData;
import com.bolaware.viewstimerstory.MainStory.StoryMomentz;
import com.bolaware.viewstimerstory.MainStory.StoryMomentzCallback;
import com.bolaware.viewstimerstory.Momentz;
import com.bolaware.viewstimerstory.MomentzCallback;
import com.bolaware.viewstimerstory.MomentzView;
import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.extractor.mp4.Mp4Extractor;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheUtil;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainStoryPostActivity extends BaseActivity implements StoryMomentzCallback, StoryCommentBottomSheetFragment.DialogDismissListener, View.OnTouchListener{

    private StoryMomentz objMomentz;

    //private ArrayList<StoryUserData> popularPosts = new ArrayList<>();
    private ArrayList<ArrayList<StoryUserData>> allPost = new ArrayList<>();
    private static int SAVED_SCROLL_POSITION = -1;
    private int postPosition = 0;
    private String postType;

    //Views
    private SimpleExoPlayer simpleExoPlayer;
    private ImageView storyImage,share,socialShare,report,background_image;
    private PlayerView playerView;
    private TextView txtUserName,likesCount,comment,follow,postText;
    private CircleImageView profile_picture;
    private ToggleButton like;
    private RelativeLayout relProfile,relControllers,text_with_image_background;
    private TextView txtStory,post_text_on_image;

    ViewGroup baseLayout;

    //swipe gestures
    private int previousFingerPosition = 0;
    private int baseLayoutPosition = 0;
    private int defaultViewHeight;

    private boolean isClosing = false;
    private boolean isScrollingUp = false;
    private boolean isScrollingDown = false;

    //follow broadcast send
    public static String NOTIFY_FOLLOW_STATE_STORYLINE="notify_follow_state_storyline";
    public static String User_Id="user_id";
    public static String IsFollow="isfollow";
    protected LocalBroadcastManager localBroadcastManager;

    //Exoplayer
    //Minimum Video you want to buffer while Playing
    public static final int MIN_BUFFER_DURATION = 2000; //was 2000
    //Max Video you want to buffer during PlayBack
    public static final int MAX_BUFFER_DURATION = 5000; // was 5000
    //Min Video you want to buffer before start Playing it
    public static final int MIN_PLAYBACK_START_BUFFER = 1500; // was 1500
    //Min video You want to buffer when user resumes video
    public static final int MIN_PLAYBACK_RESUME_BUFFER = 2000; //was 2000

    private boolean isPlayerPaused = false, isNewVideo = true;

    //Precache videos
    CacheDataSourceFactory cacheDataSourceFactory = null;
    SimpleCache simpleCache = null;

    public static String NOTIFY_MAIN_STORY_LIST="NOTIFY_MAIN_STORY_LIST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_popular_post);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);

        if (getIntent().getSerializableExtra("posts")!=null)
            //popularPosts = (ArrayList<StoryUserData>) getIntent().getSerializableExtra("posts");

        if (getIntent().getSerializableExtra("allPost")!=null)
            allPost = (ArrayList<ArrayList<StoryUserData>>) getIntent().getSerializableExtra("allPost");

        postPosition = (int) getIntent().getSerializableExtra("postPosition");

        Logger.d("postPosition111",""+postPosition);

        //baseLayout = (ConstraintLayout) findViewById(R.id.containerStory);

        baseLayout = (ViewGroup) findViewById(R.id.containerStory);

        List<MomentzView> listOfViews = new ArrayList<>();
        for (int i=0;i<allPost.get(postPosition).size();i++){
            View customView = LayoutInflater.from(this).inflate(R.layout.main_story_custom_view, null);
            MomentzView momentzView = new MomentzView(customView,10);
            listOfViews.add(momentzView);
        }

        new StoryMomentz(MainStoryPostActivity.this,listOfViews,baseLayout,this,postPosition,allPost.size()).start();
        //new StoryMomentz(MainStoryPostActivity.this,listOfViews,container,this).start();


        baseLayout.setOnTouchListener(this);


    }

    @Override
    public void done(boolean finishActivity) {
        pausePlayer();
        if (finishActivity){
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            objMomentz.pause(true);
        }catch (IndexOutOfBoundsException e){
            Logger.d("IndexOutOfBound===",""+e.getLocalizedMessage());
        }
        pausePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pausePlayer();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(objMomentz!=null){
            objMomentz.resume();
        }

        if(postType!=null){
            Logger.d("POST_TYPE1",""+postType);

            if(postType.equalsIgnoreCase("video")){
                startPlayer();
            }
        }

    }

    @Override
    public void onNextUserStory() {
        ++postPosition;
        Logger.d("plusPosition",""+postPosition);
        if(postPosition<allPost.size()){
            List<MomentzView> listOfViews = new ArrayList<>();
            for (int i=0;i<allPost.get(postPosition).size();i++){
                View customView = LayoutInflater.from(this).inflate(R.layout.main_story_custom_view, null);
                MomentzView momentzView = new MomentzView(customView,7);
                listOfViews.add(momentzView);
            }

            //ViewGroup container = (ViewGroup) findViewById(R.id.containerStory);
            new StoryMomentz(MainStoryPostActivity.this,listOfViews,baseLayout,this,postPosition,allPost.size()).start();
        }else {
            finish();
        }

    }

    @Override
    public void onPrevUserStory() {
        --postPosition;
        Logger.d("minusPosition",""+postPosition);
        if(postPosition>=0){
            List<MomentzView> listOfViews = new ArrayList<>();
            for (int i=0;i<allPost.get(postPosition).size();i++){
                View customView = LayoutInflater.from(this).inflate(R.layout.main_story_custom_view, null);
                MomentzView momentzView = new MomentzView(customView,7);
                listOfViews.add(momentzView);
            }

            //ViewGroup container = (ViewGroup) findViewById(R.id.containerStory);
            new StoryMomentz(MainStoryPostActivity.this,listOfViews,baseLayout,this,postPosition,allPost.size()).start();
        }else {
            finish();
        }

    }

    @Override
    public void onStoryPause() {

        isNewVideo = false;
        isPlayerPaused = false;

        try {
            objMomentz.pause(true);

        }catch (IndexOutOfBoundsException e){
            Logger.d("IndexOutOfBound===",""+e.getLocalizedMessage());
        }

        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(false);
            simpleExoPlayer.getPlaybackState();
        }
    }

    @Override
    public void onStoryResume() {

        isNewVideo = false;
        isPlayerPaused = false;

        if(objMomentz!=null){
            Logger.d("POST_TYPE99","resume..........");
            objMomentz.resume();
        }

        if(postType!=null){
            Logger.d("POST_TYPE99",""+postType);
            if(postType.equalsIgnoreCase("video")){
                startPlayer();
            }
        }
    }

    @Override
    public void onNextCalled(@NotNull View view, @NotNull StoryMomentz momentz, int index) {

        Logger.d("NEXT11","called....");

        isPlayerPaused = false;
        isNewVideo = true;
        this.objMomentz = momentz;

        StoryUserData post = allPost.get(postPosition).get(index);

        pausePlayer();
        initViews(view);
        setPostData(post);

        //Remove comment (Important)
        setClickListener(post,index,momentz);

        if(post.getStoryType().equalsIgnoreCase("image")){

            if(post.getMapPost()){
                postType = "MAP";
                storyImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }else {
                postType = "IMAGE";
                storyImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            momentz.pause(true);
            viewVisibility("image");
            viewStoryAPICall(post.getStoryId());

            Picasso.get()
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getStoryImage().get(0))
                    .into(storyImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            //do smth when picture is loaded successfully
                            try {
                                relControllers.setVisibility(View.VISIBLE);
                                momentz.resume();
                            }catch (IndexOutOfBoundsException e){
                                pausePlayer();
                                finish();
                            }
                        }

                        @Override
                        public void onError(Exception ex) {
                            //do smth when there is picture loading error
                            Toast.makeText(MainStoryPostActivity.this,ex.getLocalizedMessage()+"",Toast.LENGTH_LONG).show();
                        }
                    });
        }else if(post.getStoryType().equalsIgnoreCase("video")){
            postType = "VIDEO";
            momentz.pause(true);
            viewVisibility("video");
            viewStoryAPICall(post.getStoryId());
            //initializePlayer(playerView, index, momentz); <Play video directly from URL>
            initPlayer(playerView,index,momentz); //Play video from catch memory
        }else if(post.getStoryType().equalsIgnoreCase("text") && !post.getBackgroundImagePost()){
            postType = "TEXT";
            viewVisibility("text");
            viewStoryAPICall(post.getStoryId());
            txtStory.setText(post.getStoryText());
        }else if(post.getStoryType().equalsIgnoreCase("text") && post.getBackgroundImagePost()){
            postType = "BACKGROUND_IMG";
            viewVisibility("background_post");
            viewStoryAPICall(post.getStoryId());
            Glide.with(getApplicationContext())
                    .load(UrlEndpoints.MEDIA_BASE_URL + post.getBackgroundImage())
                    .into(background_image);
            post_text_on_image.setText(post.getStoryText());
        }
    }

    public void viewVisibility(String storyType){
        if(storyType.equalsIgnoreCase("image")){
            storyImage.setVisibility(View.VISIBLE);
            playerView.setVisibility(View.GONE);
            txtStory.setVisibility(View.GONE);
            text_with_image_background.setVisibility(View.GONE);
        }else if (storyType.equalsIgnoreCase("video")){
            playerView.setVisibility(View.VISIBLE);
            storyImage.setVisibility(View.GONE);
            txtStory.setVisibility(View.GONE);
            text_with_image_background.setVisibility(View.GONE);
        }else if (storyType.equalsIgnoreCase("text")){
            txtStory.setVisibility(View.VISIBLE);
            relControllers.setVisibility(View.VISIBLE);
            storyImage.setVisibility(View.GONE);
            playerView.setVisibility(View.GONE);
            text_with_image_background.setVisibility(View.GONE);
        }else if (storyType.equalsIgnoreCase("background_post")){
            text_with_image_background.setVisibility(View.VISIBLE);
            relControllers.setVisibility(View.VISIBLE);
            txtStory.setVisibility(View.GONE);
            storyImage.setVisibility(View.GONE);
            playerView.setVisibility(View.GONE);
        }
    }

    private void initViews(View view) {
        storyImage = view.findViewById(R.id.storyImage);
        playerView = view.findViewById(R.id.player);
        txtUserName = view.findViewById(R.id.txtUserName);
        profile_picture = view.findViewById(R.id.profile_picture);
        like = view.findViewById(R.id.like);
        likesCount = view.findViewById(R.id.likes_count);
        comment = view.findViewById(R.id.comment);
        share = view.findViewById(R.id.share);
        socialShare = view.findViewById(R.id.social_share);
        report = view.findViewById(R.id.report);
        follow = view.findViewById(R.id.follow);
        postText = view.findViewById(R.id.post_text);
        relProfile = view.findViewById(R.id.relProfile);
        relControllers = view.findViewById(R.id.relControllers);

        txtStory = view.findViewById(R.id.txtStory);
        text_with_image_background = view.findViewById(R.id.text_with_image_background);
        background_image = view.findViewById(R.id.background_image);
        post_text_on_image = view.findViewById(R.id.post_text_on_image);
    }

    private void setPostData(StoryUserData post) {
        if(post.getUser_name()!=null && !post.getUser_name().isEmpty()){
            txtUserName.setText(post.getUser_name());
        }else {
            String fullName = post.getFirstName() + " " + post.getLastName();
            txtUserName.setText(fullName);
        }
        Glide.with(getApplicationContext())
                .load(UrlEndpoints.MEDIA_BASE_URL + post.getProfileImageUrl())
                .into(profile_picture);

        //(Important)
        if (post.getLiked()) {
            like.setChecked(true);
            likesCount.setTextColor(Color.parseColor("#FF7400"));
        } else {
            like.setChecked(false);
            likesCount.setTextColor(Color.parseColor("#ffffff"));
        }

        String likeCount = post.getLikeCount() + "";
        likesCount.setText(likeCount);
        String commentCount = post.getCommentCount() + "";
        comment.setText(commentCount);



        if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId())) {
            follow.setVisibility(View.GONE);
        } else {
            follow.setVisibility(View.VISIBLE);
            if (post != null && post.getFollowing() != null) {
                if (post.getFollowing()) {
                    follow.setTextColor(getResources().getColor(R.color.orange));
                    follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                    follow.setText(getResources().getString(R.string.following));
                    follow.setEnabled(false);
                } else {
                    follow.setTextColor(getResources().getColor(R.color.blue));
                    follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                    follow.setText(getResources().getString(R.string.follow));
                    follow.setEnabled(true);

                    //(Important)
                    if(checkIsRequested(post)){
                        follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        follow.setTextColor(getResources().getColor(R.color.colorAccent));
                        follow.setText(R.string.requested);
                        follow.setEnabled(false);
                    }

                }
            } else {
                follow.setTextColor(getResources().getColor(R.color.blue));
                follow.setBackgroundResource(R.drawable.round_corner_blue_border);
                follow.setText(getResources().getString(R.string.follow));
                follow.setEnabled(true);
            }
        }

        postText.setText(post.getStoryText());

    }

    private void setClickListener(StoryUserData post, int index, StoryMomentz momentz) {

        //(Important)

        like.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    if (isChecked) {
                        likesCount.setTextColor(Color.parseColor("#FF7400"));

                        int counter = post.getLikeCount();
                        post.setLikeCount(counter + 1);

                        String likeCount = (Integer.valueOf(likesCount.getText().toString()) + 1) + "";
                        likesCount.setText(likeCount);
                    } else {
                        if (Integer.valueOf(likesCount.getText().toString()) > 0) {
                            likesCount.setTextColor(Color.parseColor("#ffffff"));

                            int counter = post.getLikeCount();
                            post.setLikeCount(counter - 1);

                            String likeCount = (Integer.parseInt(likesCount.getText().toString().trim()) - 1) + "";
                            likesCount.setText(likeCount);
                        }
                    }
                }

                if (buttonView.isPressed()) {
                    post.setLiked(isChecked);
                    onPostChecked(post);
                }
            }
        });

        follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFollow = false;
                if(post != null && post.getFollowing() != null){
                    if(post.getFollowing()) {
                        isFollow = false;
                    }else {
                        isFollow = true;
                        if(checkIsRequested(post)){
                            isFollow = false;
                        }
                    }
                }else {
                    isFollow = true;
                }

                if (isFollow) {
                    if(post.getPrivateAccount()) {
                        follow.setTextColor(getResources().getColor(R.color.orange));
                        follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        follow.setText(getResources().getString(R.string.requested));
                        follow.setEnabled(false);
                        onFollowClicked(post);
                    }else {
                        follow.setTextColor(getResources().getColor(R.color.orange));
                        follow.setBackgroundResource(R.drawable.round_corner_orange_border);
                        follow.setText(getResources().getString(R.string.following));
                        follow.setEnabled(false);
                        onFollowClicked(post);
                    }


                }
            }
        });

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCommentClicked(post,index,momentz);
            }
        });

        /*share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainStoryPostActivity.this, SharePostActivity.class).putExtra("post",post),100);
            }
        });

        socialShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = UrlEndpoints.BASE_SHARE_POST_URL + post.getStoryId();
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TITLE,"Afrocamgist");
                i.putExtra(Intent.EXTRA_SUBJECT, "Check out this post from Afrocamgist");
                i.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(i, "Share URL"));
            }
        });*/

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(MainStoryPostActivity.this, ReportPostActivity.class).putExtra("postId",post.getStoryId()));
                try {
                    objMomentz.pause(true);
                }catch (IndexOutOfBoundsException e){
                    Logger.d("IndexOutOfBound===",""+e.getLocalizedMessage());
                }
                pausePlayerToPlayAgain();
                showReportUserDialog(post.getUserId());
            }
        });

        //(Important)

        relProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocalStorage.getUserDetails().getUserId().equals(post.getUserId()))
                    startActivity(new Intent(MainStoryPostActivity.this, MyProfileActivity.class));
                else
                    startActivity(new Intent(MainStoryPostActivity.this, ProfileActivity.class)
                            .putExtra("userId", post.getUserId()));
            }
        });

    }

    private void viewStoryAPICall(Integer storyId) {
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.CONTENT_TYPE, "application/json");
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.STORY_VIEW + "/" + storyId + "/" + LocalStorage.getUserDetails().getUserId();

        Webservices.getData(Webservices.Method.PUT, new JSONObject(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject object = new JSONObject(response);

                    Logger.d("storyView1",""+object.toString());

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(MainStoryPostActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
    }

    private void showReportUserDialog(Integer userId) {

        Dialog popup = new Dialog(this, R.style.DialogCustom);
        popup.setContentView(R.layout.dailog_confirm_deactivate);
        popup.setCancelable(true);
        popup.show();

        TextView message = popup.findViewById(R.id.message);
        message.setTextColor(getResources().getColor(R.color.blue));

        String msg = "Are you sure you want to Report this user ?";
        message.setText(msg);

        TextView confirm = popup.findViewById(R.id.okay);
        confirm.setTextColor(getResources().getColor(R.color.blue));
        confirm.setText("Ok");

        TextView discard = popup.findViewById(R.id.cancel);
        discard.setText("Cancel");

        popup.findViewById(R.id.okay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
                callReport(userId);
                popup.dismiss();
            }
        });

        popup.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
                popup.dismiss();
            }
        });


        popup.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                onResume();
            }
        });

    }

    private void callReport(Integer userId){

        JSONObject request = new JSONObject();
        try {
            request.put("report_reason","");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.CHAT_REPORT + userId + "/report";

        Webservices.getData(Webservices.Method.POST,request, headers, url, response -> {
            onResume();
            if ("".equals(response)) {
                Utils.showAlert(MainStoryPostActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if(object.has(Constants.MESSAGE)){
                        Utils.showAlert(MainStoryPostActivity.this, getString(R.string.opps_something_went_wrong));
                    }else {
                        Toast.makeText(MainStoryPostActivity.this,getString(R.string.user_reported_successfully),Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(MainStoryPostActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }


    private void initializePlayer(PlayerView playerView, int index, StoryMomentz momentz){

        relControllers.setVisibility(View.VISIBLE);

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);


        //new added
        LoadControl loadControl = new DefaultLoadControl.Builder()
                .setAllocator(new DefaultAllocator(true, 16))
                .setBufferDurationsMs(MIN_BUFFER_DURATION,
                        MAX_BUFFER_DURATION,
                        MIN_PLAYBACK_START_BUFFER,
                        MIN_PLAYBACK_RESUME_BUFFER)
                .setTargetBufferBytes(-1)
                .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();


        //New Code ended

        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector,loadControl);
        simpleExoPlayer.setPlayWhenReady(true);
        playerView.setPlayer(simpleExoPlayer);
        playerView.hideController();

        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, Util.getUserAgent(this, "Afrocamgist"));

        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        String str = UrlEndpoints.MEDIA_BASE_URL + allPost.get(postPosition).get(index).getStoryVideo();
        Uri videoUri = Uri.parse(str);
        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory, extractorsFactory, null, null);
        simpleExoPlayer.prepare(videoSource);

        simpleExoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                super.onPlayerStateChanged(playWhenReady, playbackState);
                if (playWhenReady && playbackState == Player.STATE_READY) {
                    // media actually playing
                    momentz.editDurationAndResume(index, (int) ((simpleExoPlayer.getDuration()) / 1000));
                } else if (playWhenReady) {
                    // might be idle (plays after prepare()),
                    // buffering (plays when data available)
                    // or ended (plays when seek away from end)
                } else {
                    // simpleExoPlayer paused in any state
                }
            }
        });


    }

    private void pauseExoplayerVideo(){
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(false);
            simpleExoPlayer.stop();
            simpleExoPlayer.seekTo(0);
        }
    }

    private void pausePlayer(){
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(false);
            simpleExoPlayer.getPlaybackState();
            simpleExoPlayer.seekTo(0);
            simpleExoPlayer.release();
        }
    }

    private void pausePlayerToPlayAgain(){
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(false);
            simpleExoPlayer.getPlaybackState();
            //simpleExoPlayer.seekTo(0);
        }
    }

    private void startPlayer(){
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(true);
            simpleExoPlayer.getPlaybackState();
        }
    }

    private boolean checkIsRequested(StoryUserData profileUser) {
        if(profileUser!=null)
        {
            boolean isRequestSend = false;
            if(profileUser.getRequestButtons().get(0).getButtonText().equalsIgnoreCase("Requested")){
                isRequestSend = true;
            }
            return isRequestSend;
        }
        return false;
    }

    public void onFollowClicked(StoryUserData post) {
        followUser(post.getUserId());

        //Remove Comment (Important)
        setFollowAllUser(post.getUserId(),true);



        notifyFollowState(true,post.getUserId());

    }

    protected void notifyFollowState(boolean isFollow, Integer userId){
        Intent intent = new Intent(NOTIFY_FOLLOW_STATE_STORYLINE);
        intent.putExtra(User_Id, userId);
        intent.putExtra(IsFollow, isFollow);
        localBroadcastManager.sendBroadcast(intent);
    }

    private void followUser(Integer userId) {

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        String url = UrlEndpoints.PROFILE + "/" + userId + "/follow";

        Webservices.getData(Webservices.Method.GET, new ArrayList<>(), headers, url, new OnTaskCompleted() {
            @Override
            public void onResponse(String response) {
            }
        });
    }

    //(Important)

    private void setFollowAllUser(int userid, boolean isfollow){
        try {
            for (int i = 0; i< allPost.get(postPosition).size();i++){
                if(allPost.get(postPosition).get(i).getUserId() !=null && userid == allPost.get(postPosition).get(i).getUserId()){

                    //edited
                    if(allPost.get(postPosition).get(i).getPrivateAccount()){

                        if(isfollow){
                            allPost.get(postPosition).get(i).getRequestButtons().get(0).setButtonText("Requested");
                            allPost.get(postPosition).get(i).setFollowing(false);
                        }else {
                            allPost.get(postPosition).get(i).getRequestButtons().get(0).setButtonText("Request");
                            allPost.get(postPosition).get(i).setFollowing(false);
                        }

                    }else {
                        allPost.get(postPosition).get(i).setFollowing(isfollow);
                    }
                    //edited
                }
            }

        }catch (Exception e){
        }
    }

    public void onPostChecked(StoryUserData post) {
        likeAndUnlikePost(post.getStoryId());
        notifyMainStoryList();
    }

    private void notifyMainStoryList() {
        Intent intent = new Intent(NOTIFY_MAIN_STORY_LIST);
        localBroadcastManager.sendBroadcast(intent);
    }

    private void likeAndUnlikePost(Integer postId) {

        Logger.d("story_like_11","==ID=="+postId);

        JSONObject params = new JSONObject();
        try {
            params.put("story_id",postId);
            params.put("like_type","story");
        } catch (JSONException e) {
            e.printStackTrace();
        }



        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

        Webservices.getData(Webservices.Method.POST, params, headers, UrlEndpoints.STORY_LIKE_UNLIKE, response -> {
            if ("".equals(response)) {
                Utils.showAlert(MainStoryPostActivity.this, getString(R.string.opps_something_went_wrong));
            } else {
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has(Constants.MESSAGE)) {
                        Utils.showAlert(MainStoryPostActivity.this, object.getString(Constants.MESSAGE));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlert(MainStoryPostActivity.this, getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    public void onCommentClicked(StoryUserData post, int position, StoryMomentz momentz) {
        try {
            objMomentz.pause(true);
        }catch (IndexOutOfBoundsException e){
            Logger.d("IndexOutOfBound===",""+e.getLocalizedMessage());
        }
        pausePlayerToPlayAgain();

        Logger.d("comment90",new GsonBuilder().setPrettyPrinting().create().toJson(post.getComments()));

        //Open Comment in DialogFragment
        SAVED_SCROLL_POSITION = position;
        StoryCommentBottomSheetFragment storyCommentBottomSheetFragment = new StoryCommentBottomSheetFragment();
        StoryCommentBottomSheetFragment.newInstance(post.getComments(),post.getStoryId()).show(getSupportFragmentManager(), storyCommentBottomSheetFragment.getTag());

    }

    private void updateCommentCount(ArrayList<Comment> comments) {

        if (allPost.get(postPosition).size() > 0) {
            int commentCount = 0;
            for(Comment comment : comments) {
                commentCount++;
                if (comment.getSubComments()!=null && comment.getSubComments().size() > 0){
                    commentCount += comment.getSubComments().size();
                }
            }
            allPost.get(postPosition).get(SAVED_SCROLL_POSITION).setComments(comments);
            allPost.get(postPosition).get(SAVED_SCROLL_POSITION).setCommentCount(commentCount);

            comment.setText("" + commentCount);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {

            //Implemented

            /*if (data.getSerializableExtra("comments")!=null) {
                updateCommentCount((ArrayList<Comment>) data.getSerializableExtra("comments"));
            }*/
        } else if (requestCode == 102 && resultCode == RESULT_OK) {

            //remaining

            /*if (data.getStringExtra("postText")!=null) {
                updatePostText(data.getStringExtra("postText"));
            }*/
        }
    }

    @Override
    public void onDialogDismiss(ArrayList<Comment> comments) {
        onResume();
        if(comments!=null){
            //Remove Comment (Important)
            updateCommentCount(comments);
            notifyMainStoryList();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // Get finger position on screen
        final int Y = (int) event.getRawY();

        // Switch on motion event type
        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                // save default base layout height
                defaultViewHeight = baseLayout.getHeight();

                // Init finger and view position
                previousFingerPosition = Y;
                baseLayoutPosition = (int) baseLayout.getY();
                break;

            case MotionEvent.ACTION_UP:
                // If user was doing a scroll up
                if(isScrollingUp){
                    // Reset baselayout position
                    baseLayout.setY(0);
                    // We are not in scrolling up mode anymore
                    isScrollingUp = false;
                }

                // If user was doing a scroll down
                if(isScrollingDown){
                    // Reset baselayout position
                    baseLayout.setY(0);
                    // Reset base layout size
                    baseLayout.getLayoutParams().height = defaultViewHeight;
                    baseLayout.requestLayout();
                    // We are not in scrolling down mode anymore
                    isScrollingDown = false;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if(!isClosing){
                    int currentYPosition = (int) baseLayout.getY();

                    // If we scroll up
                    if(previousFingerPosition >Y){
                        // First time android rise an event for "up" move
                        if(!isScrollingUp){
                            isScrollingUp = true;
                        }

                        // Has user scroll down before -> view is smaller than it's default size -> resize it instead of change it position
                        if(baseLayout.getHeight()<defaultViewHeight){
                            baseLayout.getLayoutParams().height = baseLayout.getHeight() - (Y - previousFingerPosition);
                            baseLayout.requestLayout();
                        }
                        else {
                            // Has user scroll enough to "auto close" popup ?
                            if ((baseLayoutPosition - currentYPosition) > defaultViewHeight / 4) {
                                closeUpAndDismissDialog(currentYPosition);
                                return true;
                            }

                            //
                        }
                        baseLayout.setY(baseLayout.getY() + (Y - previousFingerPosition));

                    }
                    // If we scroll down
                    else{

                        // First time android rise an event for "down" move
                        if(!isScrollingDown){
                            isScrollingDown = true;
                        }

                        // Has user scroll enough to "auto close" popup ?
                        if (Math.abs(baseLayoutPosition - currentYPosition) > defaultViewHeight / 2)
                        {
                            closeDownAndDismissDialog(currentYPosition);
                            return true;
                        }

                        // Change base layout size and position (must change position because view anchor is top left corner)
                        baseLayout.setY(baseLayout.getY() + (Y - previousFingerPosition));
                        baseLayout.getLayoutParams().height = baseLayout.getHeight() - (Y - previousFingerPosition);
                        baseLayout.requestLayout();
                    }

                    // Update position
                    previousFingerPosition = Y;
                }
                break;
        }
        return true;
    }

    public void closeUpAndDismissDialog(int currentPosition){
        isClosing = true;
        ObjectAnimator positionAnimator = ObjectAnimator.ofFloat(baseLayout, "y", currentPosition, -baseLayout.getHeight());
        positionAnimator.setDuration(300);

        positionAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                finishActivity();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        positionAnimator.start();
    }

    public void closeDownAndDismissDialog(int currentPosition){
        isClosing = true;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenHeight = size.y;
        ObjectAnimator positionAnimator = ObjectAnimator.ofFloat(baseLayout, "y", currentPosition, screenHeight+baseLayout.getHeight());
        positionAnimator.setDuration(300);

        positionAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                finishActivity();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        positionAnimator.start();
    }

    private void finishActivity(){
        pausePlayer();
        finish();
    }


    private SimpleExoPlayer newSimpleExoPlayer() {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
        return ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
    }

    private MediaSource newVideoSource(String url) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        String userAgent = Util.getUserAgent(this, "Afrocamgist");
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, userAgent, bandwidthMeter);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        return new ExtractorMediaSource(Uri.parse(url), dataSourceFactory, extractorsFactory, null, null);
    }

    private void setCachedState(boolean cached) {
        Logger.d("VIDEO_CACHE","STATUS111=="+cached+"");
    }

    private void initPlayer(PlayerView playerView, int index, StoryMomentz momentz){
        relControllers.setVisibility(View.VISIBLE);
        simpleExoPlayer = newSimpleExoPlayer();

        String videoUrl = UrlEndpoints.MEDIA_BASE_URL + allPost.get(postPosition).get(index).getStoryVideo();
        String userAgent = Util.getUserAgent(this, "Afrocamgist");

        simpleCache = AfrocamgistApplication.simpleCache;
        cacheDataSourceFactory =
                new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(userAgent),CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

        MediaSource mediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(videoUrl));

        playerView.setPlayer(simpleExoPlayer);
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.seekTo(0);
        simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);
        simpleExoPlayer.prepare(mediaSource,true,false);

        simpleExoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                super.onPlayerStateChanged(playWhenReady, playbackState);

                if(simpleExoPlayer.getPlayWhenReady()){
                    if(isNewVideo && !isPlayerPaused){
                        momentz.editDurationAndResume(index, (int) ((simpleExoPlayer.getDuration()) / 1000));
                    }else if(!isNewVideo && isPlayerPaused){
                        isNewVideo = false;
                        isPlayerPaused = true;
                    }
                }



                /*if (playWhenReady && playbackState == Player.STATE_READY) {

                    momentz.editDurationAndResume(index, (int) ((simpleExoPlayer.getDuration()) / 1000));

                } else if (playWhenReady) {

                } else {

                    isPlayerPaused = true;

                }*/
            }
        });

    }


}
