package com.TBI.afrocamgist.app;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.LocaleManager;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.BaseActivity;
import com.TBI.afrocamgist.afrocalls.IncomingCallScreen;
import com.TBI.afrocamgist.afrocalls.RTCActivity;
import com.TBI.afrocamgist.afrocalls.VideoCallService;
import com.TBI.afrocamgist.connection.Connectivity;
//import com.crashlytics.android.Crashlytics;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.socket.SocketHandler;
import com.TBI.afrocamgist.socket.SocketListener;
import com.TBI.captureview.overlay.OverlayLayout;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.stripe.android.PaymentConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import io.socket.client.Ack;
import io.socket.client.Socket;

//import io.fabric.sdk.android.Fabric;

public class AfrocamgistApplication extends Application implements Application.ActivityLifecycleCallbacks, LifecycleObserver,
        SocketListener {

    private static AfrocamgistApplication instance;
    public static boolean onAppForegrounded = false;
    private static Handler handler = new Handler();
    private static Context mContext;

    public static boolean isPostUploading = false;

    //Precache videos
    public static SimpleCache simpleCache = null;
    LeastRecentlyUsedCacheEvictor leastRecentlyUsedCacheEvictor = null;
    ExoDatabaseProvider exoDatabaseProvider = null;
    long exoPlayerCacheSize = 90 * 1024 * 1024;

    private SocketHandler socketHandler;


    public static Context getContext() {
        return mContext;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackground() {
        Log.d("AfrocamgistApplication","App is running in background");


        /*Use socket here*/
        if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
            UrlEndpoints.socketIOClient.disconnect();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForeground() {
        Log.d("AfrocamgistApplication","App is running in foreground");

        /*Use socket here*/
        initSocket();
    }

    private void initSocket(){
        if (UrlEndpoints.socketIOClient != null) {
            if (UrlEndpoints.socketIOClient.connected()) {
                socketHandler = new SocketHandler(mContext);
                socketHandler.setSocketListener(this);
            } else {
                getSocketIoClient();
            }
        } else {
            getSocketIoClient();
        }
    }

    public Socket getSocketIoClient() {
        if (UrlEndpoints.socketIOClient == null && !UrlEndpoints.isSocketConnecting) {
            connectSocket();
        } else if (UrlEndpoints.socketIOClient != null) {
            if (!UrlEndpoints.socketIOClient.connected()) {
                connectSocket();
            }
        } else {
            socketHandler = new SocketHandler(mContext);
            socketHandler.setSocketListener(this);
        }
        return UrlEndpoints.socketIOClient;
    }

    public void connectSocket() {
        try {
            if (LocalStorage.getUserDetails().getUserId() != null) {
                SocketHandler socketHandler = new SocketHandler(mContext);
                socketHandler.setSocketListener(this);
                if (Utils.isInternetConnected(mContext)) {
                    socketHandler.connectToSocket();
                }
            }
        } catch (Exception e) {
            Log.d("socketConnect9", "" + e.toString());
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate() {
        super.onCreate();

        mContext = this;

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        //stripe payment intialization

        //Test
        /*PaymentConfiguration.init(
                mContext,
                "pk_test_51KQZ7lKp5rVqVzOxO9lb8sdRoZff5VWCEi9LMhIrG2fdfmArL2Pj9FPOT3nLp7zFnBBABYPjsiv2aixcY0akqvpz00qqIm9Uh4" // Test key
        );*/


        //Live
        PaymentConfiguration.init(
                mContext,
                "pk_live_51KQZ7lKp5rVqVzOxF28xZ5QmUb4gKx6llpu2B19vYjnRDiH4O5h28pJMekB3ZBq3fHVZqWzuTTSkGMLuyO7Ay8zY00PgTFOalC" // live key
        );







        //Fabric.with(this, new Crashlytics());
        instance = this;
        MultiDex.install(this);
        Connectivity.init(this);
        Fresco.initialize(this);

        //Precahce video
        if (leastRecentlyUsedCacheEvictor == null) {
            leastRecentlyUsedCacheEvictor = new LeastRecentlyUsedCacheEvictor(exoPlayerCacheSize);
        }

        if (exoDatabaseProvider != null) {
            exoDatabaseProvider = new ExoDatabaseProvider(this);
        }

        if (simpleCache == null) {
            simpleCache = new SimpleCache(context().getExternalCacheDir(), leastRecentlyUsedCacheEvictor, exoDatabaseProvider);
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Connectivity.getInstance().removeAllInternetConnectivityChangeListeners();
    }

    public static AfrocamgistApplication getInstance() {
        synchronized (AfrocamgistApplication.class) {
            onAppForegrounded = true;
            return instance;
        }
    }



    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        onAppForegrounded = true;
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
        onAppForegrounded = true;
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        onAppForegrounded = true;
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        onAppForegrounded = true;
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        onAppForegrounded = false;
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
        onAppForegrounded = false;
    }

    @Override
    public void onTrimMemory(int level) {
        if(level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN){
            onAppForegrounded = false;
        }
        super.onTrimMemory(level);
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {
        onAppForegrounded = true;
    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        onAppForegrounded = false;
    }

    public static Context context() {
        return instance.getApplicationContext();
    }

    @Override
    public void isSocketConnected(boolean connected) {
        Log.v("AfrocamgistApplication", "isSocketConnected:" + connected);
    }

    @Override
    public void isSocketReConnected() {

    }

    @Override
    public void onEvent(String event, Object... arg0) {

    }
}
