package com.TBI.afrocamgist.service;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.JsonReader;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.ChatActivity;
import com.TBI.afrocamgist.activity.LoginActivity;
import com.TBI.afrocamgist.activity.ProfileActivity;
import com.TBI.afrocamgist.activity.SocketChatActivity;
import com.TBI.afrocamgist.activity.ViewPostActivity;
import com.TBI.afrocamgist.app.AfrocamgistApplication;
import com.TBI.afrocamgist.model.user.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.RemoteMessage;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

import static com.TBI.afrocamgist.activity.SplashActivity.isNotification;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "LLLL_Firebase: ";
    NotificationManager notificationManager;
    NotificationCompat.Builder notificationBuilder;

    public static final String ANDROID_CHANNEL_ID = "com.TBI.afrocamgist";
    public static final String ANDROID_CHANNEL_NAME = "ANDROID CHANNEL";
    Context context;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("LLLL_firebase_Token", "Refreshed token: " + newToken);
                LocalStorage.setToken(newToken);
            }
        });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "onMessageReceived Data" + remoteMessage.getData());
        Log.e(TAG, "onMessageReceived Notification" + remoteMessage.getNotification());


        context = this;

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        if (!AfrocamgistApplication.onAppForegrounded) {
            Log.e("LLLLLL_APP: ", String.valueOf(AfrocamgistApplication.onAppForegrounded));
            if (remoteMessage.getData().size() > 0) {
                sendNotification(remoteMessage.getData());
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        AudioAttributes attributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build();
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        CharSequence adminChannelName = ANDROID_CHANNEL_NAME;
        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ANDROID_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        adminChannel.setSound(defaultSoundUri, attributes);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(Map<String, String> messageBody) {

        try {
            Log.d("LLLL_Mess_Body: ", messageBody.toString() + "");
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.afrocamgist_app_icon);
            notificationBuilder = new NotificationCompat.Builder(context, ANDROID_CHANNEL_ID);

            JSONObject object = new JSONObject(messageBody);
            //   JSONObject object = new JSONObject(messageBody.toString());

            String message = object.optString("body");
            String title = object.optString("title");
            String status = object.optString("status");

            isNotification = true;

            int id_notifaciton = (int) System.currentTimeMillis();
            notificationBuilder
                    .setSmallIcon(R.mipmap.afrocamgist_app_icon).setLargeIcon(icon)//a resource for your custom small icon
                    .setContentTitle(title)
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(true)//the "title" value you sent in your notification
                    .setContentText(message) //ditto
                    .setAutoCancel(true)  //dismisses the notification on click
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

            JSONObject jsonObject = new JSONObject(object.optString("data"));
            Log.e("LLLLLL_Data: ",jsonObject.toString());

            Log.e("LLLLLL_Data_Forground: ", jsonObject.toString());
            if (jsonObject.getString("notification_type").equals("post_like")) {
                notificationBuilder.setContentIntent(buildContentIntentLike(context, ViewPostActivity.class, LoginActivity.class, jsonObject.getInt("post_id")));
            } else if(jsonObject.getString("notification_type").equals("post_comment")){
                notificationBuilder.setContentIntent(buildContentIntentLike(context, ViewPostActivity.class, LoginActivity.class, jsonObject.getInt("post_id")));
            } else if (jsonObject.get("notification_type").equals("message")){

                JSONObject jsonObject1 = new JSONObject(String.valueOf(Objects.requireNonNull(jsonObject.getJSONObject("user_info"))));

                User user = new User();

                user.setFirstName(jsonObject1.getString("first_name"));
                user.setLastName(jsonObject1.getString("last_name"));
                user.setUserId(jsonObject1.getInt("user_id"));
                user.setProfileImageUrl(jsonObject1.getString("profile_image_url"));

                //KK
//                notificationBuilder.setContentIntent(buildContentIntentMessage(context, ChatActivity.class, LoginActivity.class, user));
                notificationBuilder.setContentIntent(buildContentIntentMessage(context, SocketChatActivity.class, LoginActivity.class, user));

            } else if (jsonObject.get("notification_type").equals("follow_user")){
                notificationBuilder.setContentIntent(buildContentIntentFollow(context, ProfileActivity.class, LoginActivity.class, jsonObject.getInt("user_id")));
            }

            notificationManager.notify(id_notifaciton /* ID of notification */, notificationBuilder.build());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static PendingIntent buildContentIntentLike(Context context, Class<? extends Activity> activityClass,Class<? extends Activity> activityClass1,int postId) {
        Intent intent;
        if (LocalStorage.getIsUserLoggedIn()) {
            intent = new Intent(context, activityClass);
            intent.putExtra("postId",postId);
        } else {
            intent = new Intent(context, activityClass1);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent buildContentIntentMessage(Context context, Class<? extends Activity> activityClass,Class<? extends Activity> activityClass1,User user) {

        Log.d("className1",""+activityClass.getName());

        Intent intent;
        if (LocalStorage.getIsUserLoggedIn()) {
            intent = new Intent(context, activityClass);
            intent.putExtra("user",user);
        } else {
            intent = new Intent(context, activityClass1);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent buildContentIntentFollow(Context context, Class<? extends Activity> activityClass,Class<? extends Activity> activityClass1,int userID) {
        Intent intent;
        if (LocalStorage.getIsUserLoggedIn()) {
            intent = new Intent(context, activityClass);
            intent.putExtra("userId",userID);
        } else {
            intent = new Intent(context, activityClass1);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

}
