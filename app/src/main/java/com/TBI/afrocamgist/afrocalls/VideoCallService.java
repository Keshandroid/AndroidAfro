package com.TBI.afrocamgist.afrocalls;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.TBI.afrocamgist.LocalStorage;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.Timer;
import java.util.TimerTask;

public class VideoCallService extends Service {

    private FirebaseFirestore db;
    public int counter=0;


/*
    public VideoCallService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        onTaskRemoved(intent);

        db = FirebaseFirestore.getInstance();

        *//*if(LocalStorage.getUserDetails() != null){
            if(LocalStorage.getUserDetails().getUserId() != null) {
                db.collection("calls").document("" + LocalStorage.getUserDetails().getUserId()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                        if (error != null) {
                            Log.e("VideoCallService", "Listen failed." + error.toString());
                            return;
                        }

                        if (value != null && value.exists()) {
                            Log.d("VideoCallService", "service data"+value.get("type"));
                            if(value.get("type") != null){
                                if(value.get("type") == "OFFER" || value.get("type") == "offer"){
                                    Toast.makeText(getApplicationContext(), "Pick up the phone .... ", Toast.LENGTH_SHORT).show();
                                }else if(value.get("type") == "ANSWER" || value.get("type") == "answer"){

                                }else if(value.get("type") == "END_CALL" || value.get("type") == "end_call"){

                                }
                            }
                        } else {
                            Log.d("VideoCallService", "Current data: null");
                        }

                    }
                });
            }
        }*//*

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        *//*Intent restartServiceIntent = new Intent(getApplicationContext(),this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        startService(restartServiceIntent);*//*
        super.onTaskRemoved(rootIntent);
    }*/


    @Override
    public void onCreate() {
        super.onCreate();

        db = FirebaseFirestore.getInstance();


        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground();
        else
            startForeground(1, new Notification());
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void startMyOwnForeground()
    {
        String NOTIFICATION_CHANNEL_ID = "com.TBI.afrocamgist";
        String channelName = "ANDROID CHANNEL";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_NONE)//Low importance prevent visual appearance for this notification channel on top
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();

        startForeground(2, notification);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        Log.i("onStartCommand", "=========   onStartCommand.......");


        //startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //stoptimertask();

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartservice");
        broadcastIntent.setClass(this, Restarter.class);
        this.sendBroadcast(broadcastIntent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
