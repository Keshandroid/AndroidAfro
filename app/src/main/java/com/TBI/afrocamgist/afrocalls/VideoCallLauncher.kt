package com.TBI.afrocamgist.afrocalls;

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.TBI.afrocamgist.R
import com.TBI.afrocamgist.model.user.User
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_videocall_launcher.*


class VideoCallLauncher : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()


    override fun onDestroy() {
        super.onDestroy()
        Log.d("VideoCallLauncher", "service called")


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_videocall_launcher)
        Constants.isIntiatedNow = true
        //Constants.isCallEnded = true




        //new added
      /*  db.collection("calls")
                .document("999")
                .get()
                .addOnSuccessListener {
                    if (it["type"]=="OFFER") {
                        Toast.makeText(this, "Pick up the phone .... ", Toast.LENGTH_SHORT).show()

                    } else {

                    }
                }
                .addOnFailureListener {
                    meeting_id.error = "Please enter new meeting ID"
                }

        db.collection("calls").document("777").addSnapshotListener { snapshot, error ->

            if (error != null) {
                Log.w("VideoCallLauncher", "Listen failed.", error)
                return@addSnapshotListener
            }

            if (snapshot != null && snapshot.exists()) {
                Toast.makeText(this, "Pick up the phone .... ", Toast.LENGTH_SHORT).show()
                Log.d("VideoCallLauncher", "Current data: ${snapshot.data}")

                if (snapshot["type"]=="OFFER") {
                    Log.d("VideoCallLauncher", "Current data: OFFER")
                }else if (snapshot["type"]=="ANSWER"){
                    Log.d("VideoCallLauncher", "Current data: ANSWER")
                }else if(snapshot["type"]=="END_CALL"){
                    Log.d("VideoCallLauncher", "Current data: END_CALL")
                    deleteRecord()
                }



                } else {
                Log.d("VideoCallLauncher", "Current data: null")
                }

        }*/











        db.collection("calls").document("777").addSnapshotListener { snapshot, error ->

            if (error != null) {
                Log.w("VideoCallLauncher", "Listen failed.", error)
                return@addSnapshotListener
            }

            if (snapshot != null && snapshot.exists()) {
                Toast.makeText(this, "Pick up the phone .... ", Toast.LENGTH_SHORT).show()
                Log.d("VideoCallLauncher", "Current data: ${snapshot.data}")

                if (snapshot["type"]=="OFFER") {
                    Log.d("VideoCallLauncher", "Current data: OFFER")
                }else if (snapshot["type"]=="ANSWER"){
                    Log.d("VideoCallLauncher", "Current data: ANSWER")
                }else if(snapshot["type"]=="END_CALL"){
                    Log.d("VideoCallLauncher", "Current data: END_CALL")
                    //deleteRecord()
                }


            } else {
                Log.d("VideoCallLauncher", "Current data: null")
            }

        }



        start_meeting.setOnClickListener {
            if (meeting_id.text.toString().trim().isNullOrEmpty())
                meeting_id.error = "Please enter meeting id"
            else {
                db.collection("calls")
                        .document(meeting_id.text.toString())
                        .get()
                        .addOnSuccessListener {
                            if (it["type"]=="OFFER" || it["type"]=="offer" ||
                                    it["type"]=="ANSWER" || it["type"]=="answer" ||
                                    it["type"]=="END_CALL") {
                                meeting_id.error = "Other person is already on another phone"
                            } else {
                                val intent = Intent(this@VideoCallLauncher, RTCActivity::class.java)
                                intent.putExtra("meetingID", meeting_id.text.toString())
                                intent.putExtra("isJoin", false)
                                startActivity(intent)
                            }

                        }
                        .addOnFailureListener {
                            meeting_id.error = "Please enter new meeting ID"
                        }
            }
        }
        join_meeting.setOnClickListener {
            if (meeting_id.text.toString().trim().isNullOrEmpty())
                meeting_id.error = "Please enter meeting id"
            else {
                val intent = Intent(this@VideoCallLauncher, RTCActivity::class.java)
                intent.putExtra("meetingID", meeting_id.text.toString())
                intent.putExtra("isJoin", true)
                startActivity(intent)
            }
        }
    }

    private fun deleteRecord() {


        db.collection("calls")
                .document("777")
                .delete()
                .addOnSuccessListener {
                    /*if (it["type"]=="OFFER" || it["type"]=="ANSWER" || it["type"]=="END_CALL") {
                        meeting_id.error = "Please enter new meeting ID"
                    } else {
                        val intent = Intent(this@VideoCallLauncher, RTCActivity::class.java)
                        intent.putExtra("meetingID", meeting_id.text.toString())
                        intent.putExtra("isJoin", false)
                        startActivity(intent)
                    }*/

                    Log.d("VideoCallLauncher", "Record deleted")


                }
                .addOnFailureListener {
                    Log.d("VideoCallLauncher", "Record not deleted")
                }


    }


}