package com.TBI.afrocamgist.afrocalls;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.activity.BaseActivity;
import com.TBI.afrocamgist.utils.RippleBackground;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class IncomingCallScreen extends BaseActivity {

    TextView txtCallerName;
    RelativeLayout tvReject,tvAnswer;
    FirebaseFirestore db;

    private ImageView foundDevice;
    private CircleImageView centerImage;
    private MediaPlayer ringtone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incoming_call_screen);

        db = FirebaseFirestore.getInstance();

        startCallAnimation();
        playRingtone();

        tvReject = findViewById(R.id.tvReject);
        tvAnswer = findViewById(R.id.tvAnswer);
        txtCallerName = findViewById(R.id.txtCallerName);
        centerImage = findViewById(R.id.centerImage);

        tvAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //stop ringtone
                ringtone.stop();

                Intent intent = new Intent(getApplicationContext(), RTCActivity.class);
                intent.putExtra("meetingID", ""+ LocalStorage.getUserDetails().getUserId());
                intent.putExtra("isJoin", true);
                startActivity(intent);
                finish();
            }
        });

        tvReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //stop ringtone
                ringtone.stop();

                //endCallAndDeleteData();
                endCall();
            }
        });

        checkIfCallerDetailAvailable();

        checkIfCallExist();

    }

    private void checkIfCallExist() {
        final DocumentReference docRef = db.collection("calls").document("" + LocalStorage.getUserDetails().getUserId());
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("IS_EXIST", "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {

                } else {
                    Log.d("IS_EXIST", "Current data: null");
                    ringtone.stop();
                    finish();
                }
            }
        });
    }

    private void startCallAnimation() {

        //animation
        final RippleBackground rippleBackground=(RippleBackground)findViewById(R.id.content);
        rippleBackground.startRippleAnimation();

    }

    private void playRingtone(){
        try {
            Uri alert = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.afro_ringtone);
            ringtone = new MediaPlayer();
            ringtone.setDataSource(this, alert);
            final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
                ringtone.setAudioStreamType(AudioManager.STREAM_RING);
                ringtone.setLooping(true);
                ringtone.prepare();
                ringtone.start();
            }
        } catch(Exception e) {
            e.printStackTrace();

        }
    }

    private void checkIfCallerDetailAvailable() {
        db.collection("calls").document("" + LocalStorage.getUserDetails().getUserId())
                .collection("oppositePerson")
                .document("detail")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot != null && documentSnapshot.exists()) {
                    if(documentSnapshot.get("oppositePersonName") != null){

                        String oppositePersonName = (String) documentSnapshot.get("oppositePersonName");

                        if (oppositePersonName != null) {
                            txtCallerName.setText(oppositePersonName+" is calling you...");
                        }
                    }

                    if(documentSnapshot.get("oppositePersonProfilePic") != null){

                        String profileImage = (String) documentSnapshot.get("oppositePersonProfilePic");

                        if (profileImage != null) {


                            Log.d("IncomingCallScreen", "profile : " +profileImage);

                            Glide.with(getBaseContext())
                                    .load(profileImage)
                                    .apply(RequestOptions.circleCropTransform())
                                    .into(centerImage);
                        }
                    }

                } else {
                    Log.d(getClass().getSimpleName(), "Current data: null");
                }
            }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(getClass().getSimpleName(), "Exception: " +e.getLocalizedMessage());

                }
            });
    }

    private void endCall(){
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("type", "END_CALL");

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("calls")
                .document(""+LocalStorage.getUserDetails().getUserId())
                .set(map)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("IncomingCallScreen", "END_CALL Added");

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("IncomingCallScreen", "END_CALL not Added");

            }
        });
        finish();
    }

    private void endCallAndDeleteData(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("calls")
                .document(""+LocalStorage.getUserDetails().getUserId())
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("IncomingCallScreen", "Record deleted");
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("IncomingCallScreen", "Record not deleted");

            }
        });
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //stop ringtone
        ringtone.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //stop ringtone
        ringtone.stop();
    }
}
