package com.TBI.afrocamgist.afrocalls;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.TBI.afrocamgist.LocalStorage;
import com.TBI.afrocamgist.R;
import com.TBI.afrocamgist.Utils;
import com.TBI.afrocamgist.activity.BaseActivity;
import com.TBI.afrocamgist.activity.CallLogActivity;
import com.TBI.afrocamgist.api.Webservices;
import com.TBI.afrocamgist.constants.Constants;
import com.TBI.afrocamgist.constants.UrlEndpoints;
import com.TBI.afrocamgist.utils.RippleBackground;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Ack;

public class CallBroadcastReceive extends AppCompatActivity {

    String callType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_broadcast_receive);

        NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        manager.cancelAll();

        if(getIntent().hasExtra("callType")){
            if(getIntent().getStringExtra("callType")!=null){
                callType = getIntent().getStringExtra("callType");

                if(callType.equalsIgnoreCase("audio")){
                    if(getIntent().hasExtra("Answer")){
                        Toast.makeText(getBaseContext(),"Answer audio",Toast.LENGTH_SHORT).show();

                        String oppositePersonName = getIntent().getStringExtra("oppositePersonName");
                        String profileImage = getIntent().getStringExtra("profileImage");

                        String call_id = "";
                        if(getIntent().hasExtra("call_id")){
                            call_id = getIntent().getStringExtra("call_id");
                            //callLogAcceptCallSocket(call_id);
                            callLogAcceptCall(call_id);
                        }

                        if(oppositePersonName!=null && !oppositePersonName.isEmpty() &&
                                call_id != null && !call_id.isEmpty()){
                            Intent intent = new Intent(getApplicationContext(), RTCAudioActivity.class);
                            intent.putExtra("meetingID", ""+ LocalStorage.getUserDetails().getUserId());
                            intent.putExtra("isJoin", true);
                            intent.putExtra("oppositePersonProfilePic", profileImage);
                            intent.putExtra("oppositePersonName", oppositePersonName);
                            intent.putExtra("call_id", call_id);
                            startActivity(intent);
                            finish();
                        }

                    }else if(getIntent().hasExtra("Reject")){
                        Toast.makeText(getBaseContext(),"Reject audio",Toast.LENGTH_SHORT).show();

                        if(getIntent().hasExtra("call_id")){
                            String call_id = getIntent().getStringExtra("call_id");
                            //callLogRejectCallSocket(call_id);
                            callLogRejectCall(call_id);
                        }

                        endCall();
                    }
                }else if(callType.equalsIgnoreCase("video")){
                    if(getIntent().hasExtra("Answer")){

                        String call_id = "";
                        if(getIntent().hasExtra("call_id")){
                            call_id = getIntent().getStringExtra("call_id");
                            //callLogAcceptCallSocket(call_id);
                            callLogAcceptCall(call_id);
                        }

                        if(call_id != null && !call_id.isEmpty()){
                            Toast.makeText(getBaseContext(),"Answer video",Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), RTCActivity.class);
                            intent.putExtra("meetingID", ""+ LocalStorage.getUserDetails().getUserId());
                            intent.putExtra("isJoin", true);
                            intent.putExtra("call_id", call_id);
                            startActivity(intent);
                            finish();
                        }



                    }else if(getIntent().hasExtra("Reject")){
                        Toast.makeText(getBaseContext(),"Reject video",Toast.LENGTH_SHORT).show();

                        if(getIntent().hasExtra("call_id")){
                            String call_id = getIntent().getStringExtra("call_id");
                            //callLogRejectCallSocket(call_id);
                            callLogRejectCall(call_id);
                        }

                        endCall();
                    }
                }



            }
        }
    }

    private void callLogRejectCall(String call_id) {
        if(LocalStorage.getUserDetails()!=null) {
            if (LocalStorage.getUserDetails().getUserId() != null) {
                JSONObject request = new JSONObject();
                try {
                    request.put("call_id",call_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Map<String, String> headers = new HashMap<>();
                headers.put(com.TBI.afrocamgist.constants.Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

                Log.d("CALL_RESPONSE_REQ","callLogRejectCall : "+new GsonBuilder().setPrettyPrinting().create().toJson(request));

                Webservices.getData(Webservices.Method.POST,request, headers, UrlEndpoints.NORMAL_REJECT_CALL, response -> {
                    if ("".equals(response)) {
                        //Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                    } else {
                        try {
                            JSONObject object = new JSONObject(response);

                            Log.d("CALL_RESPONSE","callLogRejectCall : "+new GsonBuilder().setPrettyPrinting().create().toJson(object));

                            if(object.has(Constants.MESSAGE)){
                                //Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                            }else {


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            //Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                        }
                    }
                });
            }
        }
    }

    private void callLogAcceptCall(String call_id) {

        if(LocalStorage.getUserDetails()!=null) {
            if (LocalStorage.getUserDetails().getUserId() != null) {
                JSONObject request = new JSONObject();
                try {
                    request.put("call_id",call_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Map<String, String> headers = new HashMap<>();
                headers.put(com.TBI.afrocamgist.constants.Constants.AUTHORIZATION, "Bearer " + LocalStorage.getLoginToken());

                Log.d("CALL_RESPONSE_REQ","callLogAcceptCall : "+new GsonBuilder().setPrettyPrinting().create().toJson(request));

                Webservices.getData(Webservices.Method.POST,request, headers, UrlEndpoints.NORMAL_ACCEPT_CALL, response -> {
                    if ("".equals(response)) {
                        //Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                    } else {
                        try {
                            JSONObject object = new JSONObject(response);

                            Log.d("CALL_RESPONSE","callLogAcceptCall : "+new GsonBuilder().setPrettyPrinting().create().toJson(object));

                            if(object.has(Constants.MESSAGE)){
                                //Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                            }else {


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            //Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                        }
                    }
                });
            }
        }
    }

    private void callLogAcceptCallSocket(String call_id) {
        if(LocalStorage.getUserDetails()!=null){
            if(LocalStorage.getUserDetails().getUserId()!=null){


                JSONObject data = new JSONObject();
                try {
                    data.put("from_id", LocalStorage.getUserDetails().getUserId());
                    data.put("call_id", call_id);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i("SocketEvent", "REQ : " + new GsonBuilder().setPrettyPrinting().create().toJson(data));

                if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
                    UrlEndpoints.socketIOClient.emit(UrlEndpoints.acceptCall, data, new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.i("SocketEvent", "callLogAcceptCallSocket with Ack()");
                            Log.i("SocketEvent", "callLogAcceptCallSocket List = " + Arrays.toString(args));// print complete response
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                            /*if (args[1] != null) {
                                try {
                                    JSONObject jsonObject = (JSONObject) args[1];
                                    int callID = jsonObject.getInt("call_id");

                                    if(calltype.equalsIgnoreCase("audio")){
                                        initAudioCall(user.getUserId(), oppositePersonName, oppositePersonProfilePic, "audio", callID);
                                    }else if(calltype.equalsIgnoreCase("video")){
                                        initVideoCall(user.getUserId(), oppositePersonName, oppositePersonProfilePic, "video", callID);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }*/
                                }
                            });

                        }
                    });
                }
            }
        }
    }

    private void callLogRejectCallSocket(String call_id) {
        if(LocalStorage.getUserDetails()!=null){
            if(LocalStorage.getUserDetails().getUserId()!=null){
                JSONObject data = new JSONObject();
                try {
                    data.put("from_id", LocalStorage.getUserDetails().getUserId());
                    data.put("call_id", call_id);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i("SocketEvent", "REQ : " + new GsonBuilder().setPrettyPrinting().create().toJson(data));

                if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
                    UrlEndpoints.socketIOClient.emit(UrlEndpoints.rejectCall, data, new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.i("SocketEvent", "callLogRejectCallSocket with Ack()");
                            Log.i("SocketEvent", "callLogRejectCallSocket List = " + Arrays.toString(args));// print complete response
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                            /*if (args[1] != null) {
                                try {
                                    JSONObject jsonObject = (JSONObject) args[1];
                                    int callID = jsonObject.getInt("call_id");

                                    if(calltype.equalsIgnoreCase("audio")){
                                        initAudioCall(user.getUserId(), oppositePersonName, oppositePersonProfilePic, "audio", callID);
                                    }else if(calltype.equalsIgnoreCase("video")){
                                        initVideoCall(user.getUserId(), oppositePersonName, oppositePersonProfilePic, "video", callID);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }*/
                                }
                            });

                        }
                    });
                }
            }
        }
    }

    private void endCall(){
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("type", "END_CALL");

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("calls")
                .document(""+LocalStorage.getUserDetails().getUserId())
                .set(map)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("IncomingCallScreen", "END_CALL Added");

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("IncomingCallScreen", "END_CALL not Added");

            }
        });
        finish();
    }

}
