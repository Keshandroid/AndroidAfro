package com.TBI.afrocamgist.afrocalls;

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.ViewGroup
import android.widget.Chronometer
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import com.TBI.afrocamgist.LocalStorage
import com.TBI.afrocamgist.R
import com.TBI.afrocamgist.api.Webservices
import com.TBI.afrocamgist.constants.UrlEndpoints
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.GsonBuilder
import io.socket.client.Ack
import kotlinx.android.synthetic.main.activity_audio_call.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.json.JSONException
import org.json.JSONObject
import org.webrtc.*
import java.util.*

@ExperimentalCoroutinesApi
class RTCAudioActivity : AppCompatActivity() {

    companion object {
        private const val CAMERA_AUDIO_PERMISSION_REQUEST_CODE = 1
        private const val CAMERA_PERMISSION = Manifest.permission.CAMERA
        private const val AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO
    }

    private lateinit var rtcClient: RTCClient
    private lateinit var signallingClient: SignalingClient

    private lateinit var meter: Chronometer
    private var isWorking = false


    private val audioManager by lazy { RTCAudioManager.create(this) }

    val TAG = "RTCActivity"

    private var meetingID : String = "test-call"

    private var oppositePersonName : String = ""
    private var oppositePersonProfilePic : String = ""
    private var callType : String = ""
    private var userName : String = ""
    private var userImage : String = ""
    private var call_id : String = ""


    private var isJoin = false

    private var isMute = false

    private var isVideoPaused = false

    private var inSpeakerMode = false

    //val db = Firebase.firestore
    val db = FirebaseFirestore.getInstance()

    private val sdpObserver = object : AppSdpObserver() {
        override fun onCreateSuccess(p0: SessionDescription?) {
            super.onCreateSuccess(p0)
//            signallingClient.send(p0)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_call)


        if (intent.hasExtra("meetingID"))
            meetingID = intent.getStringExtra("meetingID")!!
        if (intent.hasExtra("isJoin"))
            isJoin = intent.getBooleanExtra("isJoin", false)
        if(intent.hasExtra("oppositePersonName"))
            oppositePersonName = intent.getStringExtra("oppositePersonName")!!
        if(intent.hasExtra("oppositePersonProfilePic"))
            oppositePersonProfilePic = intent.getStringExtra("oppositePersonProfilePic")!!
        if(intent.hasExtra("callType"))
            callType = intent.getStringExtra("callType")!!
        if(intent.hasExtra("call_id"))
            call_id = intent.getStringExtra("call_id")!!




        if(intent.hasExtra("userName"))
            userName = intent.getStringExtra("userName")!!
        if(intent.hasExtra("userImage"))
            userImage = intent.getStringExtra("userImage")!!



        checkCameraAndAudioPermission()
        //audioManager.selectAudioDevice(RTCAudioManager.AudioDevice.EARPIECE)
        audio_output_button.setImageResource(R.drawable.ic_baseline_hearing_24)
        audioManager.setDefaultAudioDevice(RTCAudioManager.AudioDevice.EARPIECE)
        /*switch_camera_button.setOnClickListener {
            rtcClient.switchCamera()
        }*/


        meter = Chronometer(this)

        audio_output_button.setOnClickListener {
            if (inSpeakerMode) {
                inSpeakerMode = false
                audio_output_button.setImageResource(R.drawable.ic_baseline_hearing_24)
                audioManager.setDefaultAudioDevice(RTCAudioManager.AudioDevice.EARPIECE)
            } else {
                inSpeakerMode = true
                audio_output_button.setImageResource(R.drawable.ic_baseline_speaker_up_24)
                audioManager.setDefaultAudioDevice(RTCAudioManager.AudioDevice.SPEAKER_PHONE)
            }
        }
        /*video_button.setOnClickListener {
            if (isVideoPaused) {
                isVideoPaused = false
                video_button.setImageResource(R.drawable.ic_baseline_videocam_off_24)
            } else {
                isVideoPaused = true
                video_button.setImageResource(R.drawable.ic_baseline_videocam_24)
            }
            rtcClient.enableVideo(isVideoPaused)
        }*/
        mic_button.setOnClickListener {
            if (isMute) {
                isMute = false
                mic_button.setImageResource(R.drawable.ic_baseline_mic_off_24)
            } else {
                isMute = true
                mic_button.setImageResource(R.drawable.ic_baseline_mic_24)
            }
            rtcClient.enableAudio(isMute)
        }
        end_call_button.setOnClickListener {

            if (!isWorking) {
                meter.start()
                isWorking = true
            } else {
                meter.stop()
                isWorking = false
            }

            Log.d("CallDutration"," : "+meter.text)
//            callLogEndCallSocket(call_id)
            callLogEndCall(call_id)


            rtcClient.endCall(meetingID)
            Log.d("meetingID1", "" + meetingID)
            //rtcClient.endCallAndDeleteData(meetingID)
            //remote_view.isGone = false
            finish()
            //startActivity(Intent(this@RTCActivity, ChatActivity::class.java))
        }

    }

    /*private fun setSpeakerOn() {
        inSpeakerMode = true
        audio_output_button.setImageResource(R.drawable.ic_baseline_speaker_up_24)
        audioManager.setSpeakerphoneOn(true)
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION)
    }*/

    /*private fun setHeadsetOn() {
        inSpeakerMode = false
        audio_output_button.setImageResource(R.drawable.ic_baseline_hearing_24)
        audioManager.setSpeakerphoneOn(false)
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION)
    }*/

    @RequiresApi(Build.VERSION_CODES.N)
    private fun checkCameraAndAudioPermission() {
        if ((ContextCompat.checkSelfPermission(this, CAMERA_PERMISSION)
                    != PackageManager.PERMISSION_GRANTED) &&
            (ContextCompat.checkSelfPermission(this, AUDIO_PERMISSION)
                    != PackageManager.PERMISSION_GRANTED)) {
            requestCameraAndAudioPermission()
        } else {
            onCameraAndAudioPermissionGranted()
        }
    }

    fun setCallType(meetingID: String, callType: String){
        Log.d("callType : ", callType);

        val oppositePersonDetail = hashMapOf(
                "callType" to callType
                //"callStatus" to "calling"
                //"opositePersonId" to opositePersonId

        )

        db.collection("calls")
                .document(meetingID).collection("callType").document("detail")
                .set(oppositePersonDetail as Map<String, Any>)
                .addOnSuccessListener {
                    Log.e(TAG, "oppositePersonDetail: Added")
                }
                .addOnFailureListener {
                    Log.e(TAG, "oppositePersonDetail: Error $it")
                }
    }

    fun callStatusListener(meetingID: String){

        val docRef = db.collection("calls")
                .document(meetingID).collection("callType").document("detail")
        docRef.addSnapshotListener(EventListener { snapshot, e ->
            if (e != null) {
                Log.w("IS_EXIST", "Listen failed.", e)
                return@EventListener
            }
            if (snapshot != null && snapshot.exists()) {
                val data = snapshot.data
                if (data?.containsKey("callStatus")!!) {

                    if (data.getValue("callStatus").toString().equals("Calling")) {
                        connetionStatusAudio.setText("Calling...")
                    } else if (data.getValue("callStatus").toString().equals("Ringing")) {
                        connetionStatusAudio.setText("Ringing...")
                    }
                }
            } else {
                Log.d("IS_EXIST", "Current data: null")

            }
        })
    }

    fun sendOppositePersonData(meetingID: String, oppositePersonName: String, oppositePersonProfilePic: String, call_id: String) = runBlocking {
        //store oposite person detail in firestore database

        Log.d("oppositePersonName : ", oppositePersonName);

        val oppositePersonDetail = hashMapOf(
                "oppositePersonName" to oppositePersonName,
                "oppositePersonProfilePic" to oppositePersonProfilePic,
                "call_id" to call_id
                //"opositePersonId" to opositePersonId

        )

        db.collection("calls")
                .document(meetingID).collection("oppositePerson").document("detail")
                .set(oppositePersonDetail as Map<String, Any>)
                .addOnSuccessListener {
                    Log.e(TAG, "oppositePersonDetail: Added")
                }
                .addOnFailureListener {
                    Log.e(TAG, "oppositePersonDetail: Error $it")
                }
    }


    @RequiresApi(Build.VERSION_CODES.N)
    private fun onCameraAndAudioPermissionGranted() {
        sendOppositePersonData(meetingID, oppositePersonName, oppositePersonProfilePic,call_id)

        if(!isJoin){
            setCallType(meetingID, callType)
        }
        //callStatusListener(meetingID)


        rtcClient = RTCClient(
                application,
                object : PeerConnectionObserver() {
                    override fun onIceCandidate(p0: IceCandidate?) {
                        super.onIceCandidate(p0)

                        signallingClient.sendIceCandidate(p0, isJoin)
                        rtcClient.addIceCandidate(p0)
                    }

                    override fun onAddStream(p0: MediaStream?) {
                        super.onAddStream(p0)
                        Log.e(TAG, "onAddStream: $p0")
                        p0?.videoTracks?.get(0)?.addSink(remote_view)
                        //p0?.audioTracks?.get(0)?.setVolume(1.0)


                    }

                    override fun onIceConnectionChange(p0: PeerConnection.IceConnectionState?) {
                        Log.e(TAG, "onIceConnectionChange: $p0")

                        // call second time when status is CHECKING
                        if (p0.toString() == "CHECKING") {

                            runOnUiThread(
                                    object : Runnable {
                                        override fun run() {
                                            connetionStatusAudio.setText("Connecting...")
                                        }
                                    }
                            )

                            if (!isJoin)
                                rtcClient.call(sdpObserver, meetingID, oppositePersonName)
                        } else if (p0.toString() == "DISCONNECTED") {
                            //remote_view.isGone = false
                            finish()
                            //startActivity(Intent(this@RTCActivity, ChatActivity::class.java))
                        } else if (p0.toString() == "CONNECTED") {

                            runOnUiThread(
                                    object : Runnable {
                                        override fun run() {
                                            connetionStatusAudio.isGone = true
                                            if(!isWorking){
                                                startTimer()
                                            }
                                        }
                                    }
                            )
                        }

                    }

                    override fun onIceConnectionReceivingChange(p0: Boolean) {
                        Log.e(TAG, "onIceConnectionReceivingChange: $p0")
                    }

                    override fun onConnectionChange(newState: PeerConnection.PeerConnectionState?) {
                        Log.e(TAG, "onConnectionChange: $newState")
                    }

                    override fun onDataChannel(p0: DataChannel?) {
                        Log.e(TAG, "onDataChannel: $p0")
                    }

                    override fun onStandardizedIceConnectionChange(newState: PeerConnection.IceConnectionState?) {
                        Log.e(TAG, "onStandardizedIceConnectionChange: $newState")
                    }

                    override fun onAddTrack(p0: RtpReceiver?, p1: Array<out MediaStream>?) {
                        Log.e(TAG, "onAddTrack: $p0 \n $p1")
                    }

                    override fun onTrack(transceiver: RtpTransceiver?) {
                        Log.e(TAG, "onTrack: $transceiver")
                    }
                }
        )

        rtcClient.initSurfaceView(remote_view)
        rtcClient.initSurfaceView(local_view)
        rtcClient.startLocalVideoCapture(local_view)
        signallingClient =  SignalingClient(meetingID, oppositePersonName, createSignallingClientListener())
        if (!isJoin){
            rtcClient.call(sdpObserver, meetingID, oppositePersonName)
            setCallerDetail()
        }else{
            setOppositePersonDetail()
        }

    }

    private fun startTimer(){
        meter.setTextColor(Color.WHITE)
        meter.gravity = Gravity.CENTER_HORIZONTAL
        meter.setTextSize(TypedValue.COMPLEX_UNIT_IN,0.18f)

        val layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT)
        layoutParams.gravity = Gravity.CENTER
        meter.layoutParams = layoutParams


        callTimer.addView(meter)

        meter.start()
        isWorking = true

    }

    private fun setCallerDetail() {

        if(userName!=null){
            txtCallerName.setText(userName)
        }

        if (userImage != null) {
            Glide.with(baseContext)
                    .load(userImage)
                    .placeholder(R.drawable.man)
                    .apply(RequestOptions.circleCropTransform())
                    .into(centerImage)
        }

    }



    private fun setOppositePersonDetail() {

        if(oppositePersonName!=null){
            txtCallerName.setText(oppositePersonName)
        }

        if (oppositePersonProfilePic != null) {
            Glide.with(baseContext)
                    .load(oppositePersonProfilePic)
                    .placeholder(R.drawable.man)
                    .apply(RequestOptions.circleCropTransform())
                    .into(centerImage)
        }

    }

    private fun createSignallingClientListener() = object : SignalingClientListener {
        override fun onConnectionEstablished() {
            end_call_button.isClickable = true
        }

        override fun onOfferReceived(description: SessionDescription) {
            Log.d(TAG, "onOfferReceived");
            rtcClient.onRemoteSessionReceived(description)
            Constants.isIntiatedNow = false
            rtcClient.answer(sdpObserver, meetingID)
            remote_view_loading.isGone = true
        }

        override fun onAnswerReceived(description: SessionDescription) {

            Log.d(TAG, "onAnswerReceived");
            rtcClient.onRemoteSessionReceived(description)
            Constants.isIntiatedNow = false
            remote_view_loading.isGone = true
        }

        override fun onIceCandidateReceived(iceCandidate: IceCandidate) {
            Log.d(TAG, "onIceCandidateReceived");

            rtcClient.addIceCandidate(iceCandidate)
        }

        override fun onCallEnded() {

            if(isWorking){
                meter.stop()
                isWorking = false
            }

            Log.d("callEnded1", "111")
            rtcClient.endCallAndDeleteData(meetingID)
            finish()
        }
    }

    private fun requestCameraAndAudioPermission(dialogShown: Boolean = false) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA_PERMISSION) &&
            ActivityCompat.shouldShowRequestPermissionRationale(this, AUDIO_PERMISSION) &&
            !dialogShown) {
            showPermissionRationaleDialog()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(CAMERA_PERMISSION, AUDIO_PERMISSION), CAMERA_AUDIO_PERMISSION_REQUEST_CODE)
        }
    }

    private fun showPermissionRationaleDialog() {
        AlertDialog.Builder(this)
                .setTitle("Camera And Audio Permission Required")
                .setMessage("This app need the camera and audio to function")
                .setPositiveButton("Grant") { dialog, _ ->
                    dialog.dismiss()
                    requestCameraAndAudioPermission(true)
                }
                .setNegativeButton("Deny") { dialog, _ ->
                    dialog.dismiss()
                    onCameraPermissionDenied()
                }
                .show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_AUDIO_PERMISSION_REQUEST_CODE && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
            onCameraAndAudioPermissionGranted()
        } else {
            onCameraPermissionDenied()
        }
    }

    private fun onCameraPermissionDenied() {
        Toast.makeText(this, "Camera and Audio Permission Denied", Toast.LENGTH_LONG).show()
    }

    private fun callLogEndCallSocket(call_id: String) {
        if (LocalStorage.getUserDetails() != null) {
            if (LocalStorage.getUserDetails().userId != null) {

                val data = JSONObject()
                try {
                    data.put("from_id", LocalStorage.getUserDetails().userId)
                    data.put("call_id", call_id)
                    data.put("call_duration",meter.text)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                Log.i("SocketEvent", "REQ : " + GsonBuilder().setPrettyPrinting().create().toJson(data))
                if (UrlEndpoints.socketIOClient != null && UrlEndpoints.socketIOClient.connected()) {
                    UrlEndpoints.socketIOClient.emit(UrlEndpoints.endCall, data, Ack { args ->
                        Log.i("SocketEvent", "callLogEndCallSocket with Ack()")
                        Log.i("SocketEvent", "callLogEndCallSocket List = " + Arrays.toString(args)) // print complete response
                        Handler(Looper.getMainLooper()).post {
                            /*if (args[1] != null) {
                                                        try {
                                                            JSONObject jsonObject = (JSONObject) args[1];
                                                            int callID = jsonObject.getInt("call_id");

                                                            if(calltype.equalsIgnoreCase("audio")){
                                                                initAudioCall(user.getUserId(), oppositePersonName, oppositePersonProfilePic, "audio", callID);
                                                            }else if(calltype.equalsIgnoreCase("video")){
                                                                initVideoCall(user.getUserId(), oppositePersonName, oppositePersonProfilePic, "video", callID);
                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }*/
                        }
                    })
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if(rtcClient != null){
            rtcClient.enableAudio(false)
        }

        if(isFinishing){
            Log.d("CallDutration"," : "+meter.text)
            //callLogEndCallSocket(call_id)
            callLogEndCall(call_id)
            rtcClient.endCallAndDeleteData(meetingID)
            //remote_view.isGone = false
        }

    }

    private fun callLogEndCall(callId: String) {
        if (LocalStorage.getUserDetails() != null) {
            if (LocalStorage.getUserDetails().userId != null) {
                val request = JSONObject()
                try {
                    request.put("call_id", callId)
                    request.put("call_duration", meter.text)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
                val headers: MutableMap<String, String> = HashMap()
                headers[com.TBI.afrocamgist.constants.Constants.AUTHORIZATION] = "Bearer " + LocalStorage.getLoginToken()
                Log.d("CALL_RESPONSE_REQ", "callLogEndCall : " + GsonBuilder().setPrettyPrinting().create().toJson(request))
                Webservices.getData(Webservices.Method.POST, request, headers, UrlEndpoints.NORMAL_END_CALL) { response: String ->
                    if ("" == response) {
                        //Utils.showAlert(CallLogActivity.this, getString(R.string.opps_something_went_wrong));
                    } else {
                        val `object` = JSONObject(response)
                        Log.d("CALL_RESPONSE", "callLogEndCall" + GsonBuilder().setPrettyPrinting().create().toJson(`object`))
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if(rtcClient != null){
            rtcClient.enableAudio(true)
        }
    }


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onDestroy() {
        //rtcClient.endCall(meetingID)
        signallingClient.destroy()
        super.onDestroy()

    }

}