package com.TBI.captureview.overlay;

import android.graphics.Canvas;

import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public interface Overlay {

    enum Target {
        PREVIEW, PICTURE_SNAPSHOT, VIDEO_SNAPSHOT
    }

    void drawOn(@NonNull Target target, @NonNull Canvas canvas);
    boolean drawsOn(@NonNull Target target);
}
