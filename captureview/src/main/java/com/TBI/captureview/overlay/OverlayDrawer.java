package com.TBI.captureview.overlay;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.view.Surface;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.internal.Issue514Workaround;
import com.TBI.captureview.internal.egl.EglViewport;
import com.TBI.captureview.size.Size;

/**
 * TBI Demo
 */
public class OverlayDrawer {

    private static final String TAG = OverlayDrawer.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    private Overlay mOverlay;
    @VisibleForTesting int mTextureId;
    private SurfaceTexture mSurfaceTexture;
    private Surface mSurface;
    private float[] mTransform = new float[16];
    @VisibleForTesting EglViewport mViewport;
    private Issue514Workaround mIssue514Workaround;
    private final Object mIssue514WorkaroundLock = new Object();

    public OverlayDrawer(@NonNull Overlay overlay, @NonNull Size size) {
        mOverlay = overlay;
        mViewport = new EglViewport();
        mTextureId = mViewport.createTexture();
        mSurfaceTexture = new SurfaceTexture(mTextureId);
        mSurfaceTexture.setDefaultBufferSize(size.getWidth(), size.getHeight());
        mSurface = new Surface(mSurfaceTexture);
        mIssue514Workaround = new Issue514Workaround(mTextureId);
    }

    public void draw(@NonNull Overlay.Target target) {
        try {
            final Canvas surfaceCanvas = mSurface.lockCanvas(null);
            surfaceCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            mOverlay.drawOn(target, surfaceCanvas);
            mSurface.unlockCanvasAndPost(surfaceCanvas);
        } catch (Surface.OutOfResourcesException e) {
            LOG.w("Got Surface.OutOfResourcesException while drawing video overlays", e);
        }
        synchronized (mIssue514WorkaroundLock) {
            mIssue514Workaround.beforeOverlayUpdateTexImage();
            mSurfaceTexture.updateTexImage();
        }
        mSurfaceTexture.getTransformMatrix(mTransform);
    }

    public float[] getTransform() {
        return mTransform;
    }

    public void render() {
        GLES20.glDisable(GLES20.GL_CULL_FACE);
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        synchronized (mIssue514WorkaroundLock) {
            mViewport.drawFrame(mTextureId, mTransform);
        }
    }

    public void release() {
        if (mIssue514Workaround != null) {
            mIssue514Workaround.end();
            mIssue514Workaround = null;
        }
        if (mSurfaceTexture != null) {
            mSurfaceTexture.release();
            mSurfaceTexture = null;
        }
        if (mSurface != null) {
            mSurface.release();
            mSurface = null;
        }
        if (mViewport != null) {
            mViewport.release();
            mViewport = null;
        }
    }
}
