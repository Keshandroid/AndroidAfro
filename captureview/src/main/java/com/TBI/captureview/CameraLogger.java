package com.TBI.captureview;

import android.util.Log;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

/**
 * TBI Demo
 */
@SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
public final class CameraLogger {

    public final static int LEVEL_VERBOSE = 0;
    public final static int LEVEL_INFO = 1;
    public final static int LEVEL_WARNING = 2;
    public final static int LEVEL_ERROR = 3;

    @IntDef({LEVEL_VERBOSE, LEVEL_INFO, LEVEL_WARNING, LEVEL_ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LogLevel {}

    public interface Logger {

        void log(@LogLevel int level, @NonNull String tag, @NonNull String message, @Nullable Throwable throwable);
    }

    @VisibleForTesting static String lastMessage;
    @VisibleForTesting static String lastTag;

    private static int sLevel;
    private static List<Logger> sLoggers = new ArrayList<>();

    @VisibleForTesting static Logger sAndroidLogger = new Logger() {
        @Override
        public void log(int level, @NonNull String tag, @NonNull String message, @Nullable Throwable throwable) {
            switch (level) {
                case LEVEL_VERBOSE: Log.v(tag, message, throwable); break;
                case LEVEL_INFO: Log.i(tag, message, throwable); break;
                case LEVEL_WARNING: Log.w(tag, message, throwable); break;
                case LEVEL_ERROR: Log.e(tag, message, throwable); break;
            }
        }
    };

    static {
        setLogLevel(LEVEL_ERROR);
        sLoggers.add(sAndroidLogger);
    }

    public static CameraLogger create(@NonNull String tag) {
        return new CameraLogger(tag);
    }


    public static void setLogLevel(@LogLevel int logLevel) {
        sLevel = logLevel;
    }

    @SuppressWarnings("WeakerAccess")
    public static void registerLogger(@NonNull Logger logger) {
        sLoggers.add(logger);
    }

    @SuppressWarnings("WeakerAccess")
    public static void unregisterLogger(@NonNull Logger logger) {
        sLoggers.remove(logger);
    }

    @NonNull
    private String mTag;

    private CameraLogger(@NonNull String tag) {
        mTag = tag;
    }

    private boolean should(int messageLevel) {
        return sLevel <= messageLevel && sLoggers.size() > 0;
    }

    @Nullable
    public String v(@NonNull Object... data) {
        return log(LEVEL_VERBOSE, data);
    }

    @Nullable
    public String i(@NonNull Object... data) {
        return log(LEVEL_INFO, data);
    }

    @Nullable
    public String w(@NonNull Object... data) {
        return log(LEVEL_WARNING, data);
    }

    @Nullable
    public String e(@NonNull Object... data) {
        return log(LEVEL_ERROR, data);
    }

    @Nullable
    private String log(@LogLevel int level, @NonNull Object... data) {
        if (!should(level)) return null;

        StringBuilder message = new StringBuilder();
        Throwable throwable = null;
        for (Object object : data) {
            if (object instanceof Throwable) {
                throwable = (Throwable) object;
            }
            message.append(String.valueOf(object));
            message.append(" ");
        }
        String string = message.toString().trim();
        for (Logger logger : sLoggers) {
            logger.log(level, mTag, string, throwable);
        }
        lastMessage = string;
        lastTag = mTag;
        return string;
    }
}

