package com.TBI.captureview;

import android.graphics.Bitmap;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;

/**
 * TBI Demo
 */
public interface BitmapCallback {

    @UiThread
    void onBitmapReady(@Nullable Bitmap bitmap);
}
