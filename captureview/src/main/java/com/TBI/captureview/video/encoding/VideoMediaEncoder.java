package com.TBI.captureview.video.encoding;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.TBI.captureview.CameraLogger;

import java.io.IOException;

/**
 * TBI Demo
 */
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
abstract class VideoMediaEncoder<C extends VideoConfig> extends MediaEncoder {

    private static final String TAG = VideoMediaEncoder.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    @SuppressWarnings("WeakerAccess")
    protected C mConfig;

    @SuppressWarnings("WeakerAccess")
    protected Surface mSurface;

    @SuppressWarnings("WeakerAccess")
    protected int mFrameNumber = -1;

    private boolean mSyncFrameFound = false;

    VideoMediaEncoder(@NonNull C config) {
        super("VideoEncoder");
        mConfig = config;
    }

    @EncoderThread
    @Override
    protected void onPrepare(@NonNull MediaEncoderEngine.Controller controller, long maxLengthUs) {
        MediaFormat format = MediaFormat.createVideoFormat(mConfig.mimeType, mConfig.width, mConfig.height);
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
        format.setInteger(MediaFormat.KEY_BIT_RATE, mConfig.bitRate);
        format.setInteger(MediaFormat.KEY_FRAME_RATE, mConfig.frameRate);
        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1); // seconds between key frames!
        format.setInteger("rotation-degrees", mConfig.rotation);

        try {
            if (mConfig.encoder != null) {
                mMediaCodec = MediaCodec.createByCodecName(mConfig.encoder);
            } else {
                mMediaCodec = MediaCodec.createEncoderByType(mConfig.mimeType);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        mMediaCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        mSurface = mMediaCodec.createInputSurface();
        mMediaCodec.start();
    }

    @EncoderThread
    @Override
    protected void onStart() {
        // Nothing to do here. Waiting for the first frame.
        mFrameNumber = 0;
    }

    @EncoderThread
    @Override
    protected void onStop() {
        LOG.i("onStop", "setting mFrameNumber to 1 and signaling the end of input stream.");
        mFrameNumber = -1;
        mMediaCodec.signalEndOfInputStream();
        drainOutput(true);
    }

    @Override
    protected void onWriteOutput(@NonNull OutputBufferPool pool, @NonNull OutputBuffer buffer) {
        if (!mSyncFrameFound) {
            LOG.w("onWriteOutput:", "sync frame not found yet. Checking.");
            int flag = MediaCodec.BUFFER_FLAG_SYNC_FRAME;
            boolean hasFlag = (buffer.info.flags & flag) == flag;
            if (hasFlag) {
                LOG.w("onWriteOutput:", "SYNC FRAME FOUND!");
                mSyncFrameFound = true;
                super.onWriteOutput(pool, buffer);
            } else {
                LOG.w("onWriteOutput:", "DROPPING FRAME and requesting a sync frame soon.");
                if (Build.VERSION.SDK_INT >= 19) {
                    Bundle params = new Bundle();
                    params.putInt(MediaCodec.PARAMETER_KEY_REQUEST_SYNC_FRAME, 0);
                    mMediaCodec.setParameters(params);
                }
                pool.recycle(buffer);
            }
        } else {
            super.onWriteOutput(pool, buffer);
        }
    }

    @Override
    protected int getEncodedBitRate() {
        return mConfig.bitRate;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    protected boolean shouldRenderFrame(long timestampUs) {
        if (timestampUs == 0) return false;
        if (mFrameNumber < 0) return false;
        if (hasReachedMaxLength()) return false;
        mFrameNumber++;
        return true;
    }
}
