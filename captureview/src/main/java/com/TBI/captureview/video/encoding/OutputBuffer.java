package com.TBI.captureview.video.encoding;

import android.media.MediaCodec;

import java.nio.ByteBuffer;

/**
 * TBI Demo
 */
@SuppressWarnings("WeakerAccess")
public class OutputBuffer {
    public MediaCodec.BufferInfo info;
    public int trackIndex;
    public ByteBuffer data;
}
