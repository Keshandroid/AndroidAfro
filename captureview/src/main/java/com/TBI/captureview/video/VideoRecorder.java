package com.TBI.captureview.video;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.TBI.captureview.VideoResult;

/**
 * TBI Demo
 */
public abstract class VideoRecorder {

    public interface VideoResultListener {

        void onVideoResult(@Nullable VideoResult.Stub result, @Nullable Exception exception);

        void onVideoRecordingStart();

        void onVideoRecordingEnd();
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED) VideoResult.Stub mResult;
    private final VideoResultListener mListener;
    @SuppressWarnings("WeakerAccess")
    protected Exception mError;
    private boolean mIsRecording;

    VideoRecorder(@Nullable VideoResultListener listener) {
        mListener = listener;
    }

    public final void start(@NonNull VideoResult.Stub stub) {
        mResult = stub;
        mIsRecording = true;
        onStart();
    }


    public final void stop(boolean isCameraShutdown) {
        onStop(isCameraShutdown);
    }

    public boolean isRecording() {
        return mIsRecording;
    }

    protected abstract void onStart();

    protected abstract void onStop(boolean isCameraShutdown);

    @CallSuper
    protected void dispatchResult() {
        mIsRecording = false;
        if (mListener != null) {
            mListener.onVideoResult(mResult, mError);
        }
        mResult = null;
        mError = null;
    }

    @SuppressWarnings("WeakerAccess")
    @CallSuper
    protected void dispatchVideoRecordingStart() {
        if (mListener != null) {
            mListener.onVideoRecordingStart();
        }
    }

    @SuppressWarnings("WeakerAccess")
    @CallSuper
    protected void dispatchVideoRecordingEnd() {
        if (mListener != null) {
            mListener.onVideoRecordingEnd();
        }
    }
}
