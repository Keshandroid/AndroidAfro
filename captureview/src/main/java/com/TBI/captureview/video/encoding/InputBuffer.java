package com.TBI.captureview.video.encoding;

import java.nio.ByteBuffer;

@SuppressWarnings("WeakerAccess")
public class InputBuffer {
    public ByteBuffer data;
    public ByteBuffer source;
    public int index;
    public int length;
    public long timestamp;
    public boolean isEndOfStream;
}
