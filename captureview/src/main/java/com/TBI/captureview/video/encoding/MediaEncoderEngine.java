package com.TBI.captureview.video.encoding;

import android.annotation.SuppressLint;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.internal.utils.WorkerHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * TBI Demo
 */
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class MediaEncoderEngine {
    public interface Listener {

        @EncoderThread
        void onEncodingStart();

        @EncoderThread
        void onEncodingStop();

        @EncoderThread
        void onEncodingEnd(int reason, @Nullable Exception e);
    }

    private final static String TAG = MediaEncoderEngine.class.getSimpleName();
    private final static CameraLogger LOG = CameraLogger.create(TAG);
    private static final boolean DEBUG_PERFORMANCE = true;

    @SuppressWarnings("WeakerAccess")
    public final static int END_BY_USER = 0;
    public final static int END_BY_MAX_DURATION = 1;
    public final static int END_BY_MAX_SIZE = 2;

    private final List<MediaEncoder> mEncoders = new ArrayList<>();
    private MediaMuxer mMediaMuxer;
    private int mStartedEncodersCount = 0;
    private int mStoppedEncodersCount = 0;
    private boolean mMediaMuxerStarted = false;
    @SuppressWarnings("FieldCanBeLocal")
    private final Controller mController = new Controller();
    private final WorkerHandler mControllerThread = WorkerHandler.get("EncoderEngine");
    private final Object mControllerLock = new Object();
    private Listener mListener;
    private int mEndReason = END_BY_USER;
    private int mPossibleEndReason;

    public MediaEncoderEngine(@NonNull File file,
                              @NonNull VideoMediaEncoder videoEncoder,
                              @Nullable AudioMediaEncoder audioEncoder,
                              final int maxDuration,
                              final long maxSize,
                              @Nullable Listener listener) {
        mListener = listener;
        mEncoders.add(videoEncoder);
        if (audioEncoder != null) {
            mEncoders.add(audioEncoder);
        }
        try {
            mMediaMuxer = new MediaMuxer(file.toString(), MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        int bitRate = 0;
        for (MediaEncoder encoder : mEncoders) {
            bitRate += encoder.getEncodedBitRate();
        }
        int byteRate = bitRate / 8;
        long sizeMaxDurationUs = (maxSize / byteRate) * 1000L * 1000L;
        long maxDurationUs = maxDuration * 1000L;
        long finalMaxDurationUs = Long.MAX_VALUE;
        if (maxSize > 0 && maxDuration > 0) {
            mPossibleEndReason = sizeMaxDurationUs < maxDurationUs ? END_BY_MAX_SIZE : END_BY_MAX_DURATION;
            finalMaxDurationUs = Math.min(sizeMaxDurationUs, maxDurationUs);
        } else if (maxSize > 0) {
            mPossibleEndReason = END_BY_MAX_SIZE;
            finalMaxDurationUs = sizeMaxDurationUs;
        } else if (maxDuration > 0) {
            mPossibleEndReason = END_BY_MAX_DURATION;
            finalMaxDurationUs = maxDurationUs;
        }
        LOG.w("Computed a max duration of", (finalMaxDurationUs / 1000000F));
        for (MediaEncoder encoder : mEncoders) {
            encoder.prepare(mController, finalMaxDurationUs);
        }
    }

    public final void start() {
        LOG.i("Passing event to encoders:", "START");
        for (MediaEncoder encoder : mEncoders) {
            encoder.start();
        }
    }

    @SuppressWarnings("SameParameterValue")
    public final void notify(final String event, final Object data) {
        LOG.i("Passing event to encoders:", event);
        for (MediaEncoder encoder : mEncoders) {
            encoder.notify(event, data);
        }
    }

    public final void stop() {
        LOG.i("Passing event to encoders:", "STOP");
        for (MediaEncoder encoder : mEncoders) {
            encoder.stop();
        }
        if (mListener != null) {
            mListener.onEncodingStop();
        }
    }

    private void end() {
        LOG.i("end:", "Releasing muxer after all encoders have been released.");
        Exception error = null;
        if (mMediaMuxer != null) {
            try {
                mMediaMuxer.stop();
            } catch (Exception e) {
                error = e;
            }
            try {
                mMediaMuxer.release();
            } catch (Exception e) {
                if (error == null) error = e;
            }
            mMediaMuxer = null;
        }
        LOG.w("end:", "Dispatching end to listener - reason:", mEndReason, "error:", error);
        if (mListener != null) {
            mListener.onEncodingEnd(mEndReason, error);
            mListener = null;
        }
        mEndReason = END_BY_USER;
        mStartedEncodersCount = 0;
        mStoppedEncodersCount = 0;
        mMediaMuxerStarted = false;
        mControllerThread.destroy();
        LOG.i("end:", "Completed.");
    }

    @NonNull
    public VideoMediaEncoder getVideoEncoder() {
        return (VideoMediaEncoder) mEncoders.get(0);
    }

    @SuppressWarnings("unused")
    @Nullable
    public AudioMediaEncoder getAudioEncoder() {
        if (mEncoders.size() > 1) {
            return (AudioMediaEncoder) mEncoders.get(1);
        } else {
            return null;
        }
    }

    @SuppressWarnings("WeakerAccess")
    public class Controller {

        public int notifyStarted(@NonNull MediaFormat format) {
            synchronized (mControllerLock) {
                if (mMediaMuxerStarted) {
                    throw new IllegalStateException("Trying to start but muxer started already");
                }
                int track = mMediaMuxer.addTrack(format);
                LOG.w("notifyStarted:", "Assigned track", track, "to format", format.getString(MediaFormat.KEY_MIME));
                if (++mStartedEncodersCount == mEncoders.size()) {
                    LOG.w("notifyStarted:", "All encoders have started. Starting muxer and dispatching onEncodingStart().");
                    mControllerThread.run(new Runnable() {
                        @Override
                        public void run() {
                            mMediaMuxer.start();
                            mMediaMuxerStarted = true;
                            if (mListener != null) {
                                mListener.onEncodingStart();
                            }
                        }
                    });
                }
                return track;
            }
        }

        public boolean isStarted() {
            synchronized (mControllerLock) {
                return mMediaMuxerStarted;
            }
        }

        @SuppressLint("UseSparseArrays")
        private Map<Integer, Integer> mDebugCount = new HashMap<>();

        public void write(@NonNull OutputBufferPool pool, @NonNull OutputBuffer buffer) {
            if (DEBUG_PERFORMANCE) {
                // When AUDIO = mono, this is called about twice the time. (200 vs 100 for 5 sec).
                Integer count = mDebugCount.get(buffer.trackIndex);
                mDebugCount.put(buffer.trackIndex, count == null ? 1 : ++count);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(buffer.info.presentationTimeUs / 1000);
                LOG.v("write:", "Writing into muxer -",
                                "track:", buffer.trackIndex,
                        "presentation:", buffer.info.presentationTimeUs,
                        "readable:", calendar.get(Calendar.SECOND) + ":" + calendar.get(Calendar.MILLISECOND),
                        "count:", count);
            } else {
                LOG.v("write:", "Writing into muxer -",
                        "track:", buffer.trackIndex,
                        "presentation:", buffer.info.presentationTimeUs);
            }
            mMediaMuxer.writeSampleData(buffer.trackIndex, buffer.data, buffer.info);
            pool.recycle(buffer);
        }

        public void requestStop(int track) {
            synchronized (mControllerLock) {
                LOG.w("requestStop:", "Called for track", track);
                if (--mStartedEncodersCount == 0) {
                    LOG.w("requestStop:", "All encoders have requested a stop. Stopping them.");
                    mEndReason = mPossibleEndReason;
                    mControllerThread.run(new Runnable() {
                        @Override
                        public void run() {
                            stop();
                        }
                    });
                }
            }
        }

        public void notifyStopped(int track) {
            synchronized (mControllerLock) {
                LOG.w("notifyStopped:", "Called for track", track);
                if (++mStoppedEncodersCount == mEncoders.size()) {
                    LOG.w("requestStop:", "All encoders have been stopped. Stopping the muxer.");
                    mControllerThread.run(new Runnable() {
                        @Override
                        public void run() {
                            end();
                        }
                    });
                }
            }
        }
    }
}
