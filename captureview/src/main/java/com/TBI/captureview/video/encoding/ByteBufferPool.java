package com.TBI.captureview.video.encoding;

import com.TBI.captureview.internal.utils.Pool;

import java.nio.ByteBuffer;

/**
 * TBI Demo
 */
class ByteBufferPool extends Pool<ByteBuffer> {

    ByteBufferPool(final int bufferSize, int maxPoolSize) {
        super(maxPoolSize, new Factory<ByteBuffer>() {
            @Override
            public ByteBuffer create() {
                return ByteBuffer.allocateDirect(bufferSize);
            }
        });
    }
}
