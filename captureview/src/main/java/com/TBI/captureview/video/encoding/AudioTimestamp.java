package com.TBI.captureview.video.encoding;

/**
 * TBI Demo
 */
class AudioTimestamp {

    static long bytesToUs(long bytes, int byteRate) {
        return (1000000L * bytes) / byteRate;
    }

    static long bytesToMillis(long bytes, int byteRate) {
        return (1000L * bytes) / byteRate;
    }

    private int mByteRate;
    private long mBaseTimeUs;
    private long mBytesSinceBaseTime;
    private long mGapUs;

    AudioTimestamp(int byteRate) {
        mByteRate = byteRate;
    }

    @SuppressWarnings("SameParameterValue")
    long increaseUs(int readBytes) {
        long bufferDurationUs = bytesToUs((long) readBytes, mByteRate);
        long bufferEndTimeUs = System.nanoTime() / 1000; // now
        long bufferStartTimeUs = bufferEndTimeUs - bufferDurationUs;

        if (mBytesSinceBaseTime == 0) mBaseTimeUs = bufferStartTimeUs;
        long correctedTimeUs = mBaseTimeUs + bytesToUs(mBytesSinceBaseTime, mByteRate);
        long correctionUs = bufferStartTimeUs - correctedTimeUs;

        if (correctionUs >= 2L * bufferDurationUs) {
            mBaseTimeUs = bufferStartTimeUs;
            mBytesSinceBaseTime = readBytes;
            mGapUs = correctionUs;
            return mBaseTimeUs;
        } else {
            mGapUs = 0;
            mBytesSinceBaseTime += readBytes;
            return correctedTimeUs;
        }
    }

    int getGapCount(int frameBytes) {
        if (mGapUs == 0) return 0;
        long durationUs = bytesToUs((long) frameBytes, mByteRate);
        return (int) (mGapUs / durationUs);
    }

    long getGapStartUs(long lastTimeUs) {
        return lastTimeUs - mGapUs;
    }
}
