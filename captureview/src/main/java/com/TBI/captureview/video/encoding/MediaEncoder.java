package com.TBI.captureview.video.encoding;

import android.annotation.SuppressLint;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Build;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.internal.utils.WorkerHandler;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * TBI Demo
 */
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public abstract class MediaEncoder {

    private final static String TAG = MediaEncoder.class.getSimpleName();
    private final static CameraLogger LOG = CameraLogger.create(TAG);

    // INPUT_TIMEOUT_US 10000: 46 seconds
    // INPUT_TIMEOUT_US 1000: 37 seconds
    // INPUT_TIMEOUT_US 100: 33 seconds
    // INPUT_TIMEOUT_US 0: 32 seconds
    private final static int INPUT_TIMEOUT_US = 0;
    private final static int OUTPUT_TIMEOUT_US = 0;
    private final static int STATE_NONE = 0;
    private final static int STATE_PREPARING = 1;
    private final static int STATE_PREPARED = 2;
    private final static int STATE_STARTING = 3;
    private final static int STATE_STARTED = 4;
    private final static int STATE_LIMIT_REACHED = 5;
    private final static int STATE_STOPPING = 6;
    private final static int STATE_STOPPED = 7;

    private int mState = STATE_NONE;
    private final String mName;

    @SuppressWarnings("WeakerAccess")
    protected MediaCodec mMediaCodec;

    @SuppressWarnings("WeakerAccess")
    protected WorkerHandler mWorker;

    private MediaEncoderEngine.Controller mController;
    private int mTrackIndex;
    private OutputBufferPool mOutputBufferPool;
    private MediaCodec.BufferInfo mBufferInfo;
    private MediaCodecBuffers mBuffers;
    private final Map<String, AtomicInteger> mPendingEvents = new HashMap<>();

    private long mMaxLengthUs;
    private boolean mMaxLengthReached;

    private long mStartTimeMillis = 0; // In System.currentTimeMillis()
    private long mFirstTimeUs = Long.MIN_VALUE; // In unknown reference
    private long mLastTimeUs = 0;

    private long mDebugSetStateTimestamp = Long.MIN_VALUE;

    @SuppressWarnings("WeakerAccess")
    protected MediaEncoder(@NonNull String name) {
        mName = name;
    }

    private void setState(int newState) {
        if (mDebugSetStateTimestamp == Long.MIN_VALUE) {
            mDebugSetStateTimestamp = System.currentTimeMillis();
        }
        long millis = System.currentTimeMillis() - mDebugSetStateTimestamp;
        mDebugSetStateTimestamp = System.currentTimeMillis();

        String newStateName = null;
        switch (newState) {
            case STATE_NONE: newStateName = "NONE"; break;
            case STATE_PREPARING: newStateName = "PREPARING"; break;
            case STATE_PREPARED: newStateName = "PREPARED"; break;
            case STATE_STARTING: newStateName = "STARTING"; break;
            case STATE_STARTED: newStateName = "STARTED"; break;
            case STATE_LIMIT_REACHED: newStateName = "LIMIT_REACHED"; break;
            case STATE_STOPPING: newStateName = "STOPPING"; break;
            case STATE_STOPPED: newStateName = "STOPPED"; break;
        }
        LOG.w(mName, "setState:", newStateName, "millisSinceLastState:", millis);
        mState = newState;
    }

    final void prepare(@NonNull final MediaEncoderEngine.Controller controller, final long maxLengthUs) {
        if (mState >= STATE_PREPARING) {
            LOG.e(mName, "Wrong state while preparing. Aborting.", mState);
            return;
        }
        mController = controller;
        mBufferInfo = new MediaCodec.BufferInfo();
        mMaxLengthUs = maxLengthUs;
        mWorker = WorkerHandler.get(mName);
        mWorker.getThread().setPriority(Thread.MAX_PRIORITY);
        LOG.i(mName, "Prepare was called. Posting.");
        mWorker.post(new Runnable() {
            @Override
            public void run() {
                LOG.i(mName, "Prepare was called. Executing.");
                setState(STATE_PREPARING);
                onPrepare(controller, maxLengthUs);
                setState(STATE_PREPARED);
            }
        });
    }

    final void start() {
        LOG.w(mName, "Start was called. Posting.");
        mWorker.post(new Runnable() {
            @Override
            public void run() {
                if (mState < STATE_PREPARED || mState >= STATE_STARTING) {
                    LOG.e(mName, "Wrong state while starting. Aborting.", mState);
                    return;
                }
                setState(STATE_STARTING);
                LOG.w(mName, "Start was called. Executing.");
                onStart();
            }
        });
    }

    @SuppressWarnings("ConstantConditions")
    final void notify(final @NonNull String event, final @Nullable Object data) {
        if (!mPendingEvents.containsKey(event)) mPendingEvents.put(event, new AtomicInteger(0));
        final AtomicInteger pendingEvents = mPendingEvents.get(event);
        pendingEvents.incrementAndGet();
        LOG.v(mName, "Notify was called. Posting. pendingEvents:", pendingEvents.intValue());
        mWorker.post(new Runnable() {
            @Override
            public void run() {
                LOG.v(mName, "Notify was called. Executing. pendingEvents:", pendingEvents.intValue());
                onEvent(event, data);
                pendingEvents.decrementAndGet();
            }
        });
    }

    final void stop() {
        if (mState >= STATE_STOPPING) {
            LOG.e(mName, "Wrong state while stopping. Aborting.", mState);
            return;
        }
        setState(STATE_STOPPING);
        LOG.w(mName, "Stop was called. Posting.");
        mWorker.post(new Runnable() {
            @Override
            public void run() {
                LOG.w(mName, "Stop was called. Executing.");
                onStop();
            }
        });
    }

    @EncoderThread
    protected abstract void onPrepare(@NonNull MediaEncoderEngine.Controller controller, long maxLengthUs);

    @EncoderThread
    protected abstract void onStart();

    @EncoderThread
    protected void onEvent(@NonNull String event, @Nullable Object data) {}

    @EncoderThread
    protected abstract void onStop();

    @CallSuper
    protected void onStopped() {
        LOG.w(mName, "is being released. Notifying controller and releasing codecs.");
        // TODO should we call notifyStopped after this method ends?
        mController.notifyStopped(mTrackIndex);
        mMediaCodec.stop();
        mMediaCodec.release();
        mMediaCodec = null;
        mOutputBufferPool.clear();
        mOutputBufferPool = null;
        mBuffers = null;
        setState(STATE_STOPPED);
        mWorker.destroy();
    }

    @SuppressWarnings("WeakerAccess")
    protected boolean tryAcquireInputBuffer(@NonNull InputBuffer holder) {
        if (mBuffers == null) {
            mBuffers = new MediaCodecBuffers(mMediaCodec);
        }
        int inputBufferIndex = mMediaCodec.dequeueInputBuffer(INPUT_TIMEOUT_US);
        if (inputBufferIndex < 0) {
            return false;
        } else {
            holder.index = inputBufferIndex;
            holder.data = mBuffers.getInputBuffer(inputBufferIndex);
            return true;
        }
    }

    @SuppressWarnings({"StatementWithEmptyBody", "WeakerAccess"})
    protected void acquireInputBuffer(@NonNull InputBuffer holder) {
        while (!tryAcquireInputBuffer(holder)) {}
    }

    @SuppressWarnings("WeakerAccess")
    protected void encodeInputBuffer(InputBuffer buffer) {
        LOG.v(mName, "ENCODING - Buffer:", buffer.index,
                "Bytes:", buffer.length,
                "Presentation:", buffer.timestamp);
        if (buffer.isEndOfStream) { // send EOS
            mMediaCodec.queueInputBuffer(buffer.index, 0, 0,
                    buffer.timestamp, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
        } else {
            mMediaCodec.queueInputBuffer(buffer.index, 0, buffer.length,
                    buffer.timestamp, 0);
        }
    }

    @SuppressLint("LogNotTimber")
    @SuppressWarnings("WeakerAccess")
    protected final void drainOutput(boolean drainAll) {
        LOG.i(mName, "DRAINING - EOS:", drainAll);
        if (mMediaCodec == null) {
            LOG.e("drain() was called before prepare() or after releasing.");
            return;
        }
        if (mBuffers == null) {
            mBuffers = new MediaCodecBuffers(mMediaCodec);
        }
        while (true) {
            int encoderStatus = mMediaCodec.dequeueOutputBuffer(mBufferInfo, OUTPUT_TIMEOUT_US);
            if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                // no output available yet
                if (!drainAll) break; // out of while

            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                // not expected for an encoder
                mBuffers.onOutputBuffersChanged();

            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                // should happen before receiving buffers, and should only happen once
                if (mController.isStarted()) throw new RuntimeException("MediaFormat changed twice.");
                MediaFormat newFormat = mMediaCodec.getOutputFormat();
                mTrackIndex = mController.notifyStarted(newFormat);
                setState(STATE_STARTED);
                mOutputBufferPool = new OutputBufferPool(mTrackIndex);
            } else if (encoderStatus < 0) {
                LOG.e("Unexpected result from dequeueOutputBuffer: " + encoderStatus);
                // let's ignore it
            } else {
                ByteBuffer encodedData = mBuffers.getOutputBuffer(encoderStatus);
                boolean isCodecConfig = (mBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0;
                if (!isCodecConfig && mController.isStarted() && mBufferInfo.size != 0) {

                    // adjust the ByteBuffer values to match BufferInfo (not needed?)
                    encodedData.position(mBufferInfo.offset);
                    encodedData.limit(mBufferInfo.offset + mBufferInfo.size);

                    if (mFirstTimeUs == Long.MIN_VALUE) {
                        mFirstTimeUs = mBufferInfo.presentationTimeUs;
                        LOG.w(mName, "DRAINING - Got the first presentation time:", mFirstTimeUs);
                    }
                    mLastTimeUs = mBufferInfo.presentationTimeUs;
                    mBufferInfo.presentationTimeUs = (mStartTimeMillis * 1000) + mLastTimeUs - mFirstTimeUs;

                    // Write.
                    LOG.i(mName, "DRAINING - About to write(). Adjusted presentation:", mBufferInfo.presentationTimeUs);
                    OutputBuffer buffer = mOutputBufferPool.get();
                    //noinspection ConstantConditions
                    buffer.info = mBufferInfo;
                    buffer.trackIndex = mTrackIndex;
                    buffer.data = encodedData;
                    onWriteOutput(mOutputBufferPool, buffer);
                }
                mMediaCodec.releaseOutputBuffer(encoderStatus, false);

                if (!drainAll
                        && !mMaxLengthReached
                        && mFirstTimeUs != Long.MIN_VALUE
                        && mLastTimeUs - mFirstTimeUs > mMaxLengthUs) {
                    LOG.w(mName, "DRAINING - Reached maxLength! mLastTimeUs:", mLastTimeUs,
                            "mStartTimeUs:", mFirstTimeUs,
                            "mDeltaUs:", mLastTimeUs - mFirstTimeUs,
                            "mMaxLengthUs:", mMaxLengthUs);
                    onMaxLengthReached();
                    break;
                }

                // Check for the EOS flag so we can call onStopped.
                if ((mBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    LOG.w(mName, "DRAINING - Got EOS. Releasing the codec.");
                    onStopped();
                    break;
                }
            }
        }
    }

    @CallSuper
    protected void onWriteOutput(@NonNull OutputBufferPool pool, @NonNull OutputBuffer buffer) {
        mController.write(pool, buffer);
    }

    protected abstract int getEncodedBitRate();

    @SuppressWarnings("WeakerAccess")
    protected long getMaxLengthUs() {
        return mMaxLengthUs;
    }

    @SuppressWarnings("WeakerAccess")
    protected void notifyMaxLengthReached() {
        onMaxLengthReached();
    }

    @SuppressWarnings("WeakerAccess")
    protected boolean hasReachedMaxLength() {
        return mMaxLengthReached;
    }

    private void onMaxLengthReached() {
        if (mMaxLengthReached) return;
        mMaxLengthReached = true;
        if (mState >= STATE_LIMIT_REACHED) {
            LOG.w(mName, "onMaxLengthReached: Reached in wrong state. Aborting.", mState);
        } else {
            LOG.w(mName, "onMaxLengthReached: Requesting a stop.");
            setState(STATE_LIMIT_REACHED);
            mController.requestStop(mTrackIndex);
        }
    }

    @SuppressWarnings("WeakerAccess")
    protected final void notifyFirstFrameMillis(long firstFrameMillis) {
        mStartTimeMillis = firstFrameMillis;
    }

    @SuppressWarnings({"SameParameterValue", "ConstantConditions", "WeakerAccess"})
    protected final int getPendingEvents(@NonNull String event) {
        return mPendingEvents.get(event).intValue();
    }
}
