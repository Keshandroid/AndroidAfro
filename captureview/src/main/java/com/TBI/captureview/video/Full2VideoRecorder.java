package com.TBI.captureview.video;

import android.annotation.SuppressLint;
import android.hardware.camera2.CaptureRequest;
import android.media.MediaRecorder;
import android.os.Build;
import android.view.Surface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.VideoResult;
import com.TBI.captureview.engine.Camera2Engine;
import com.TBI.captureview.engine.action.Action;
import com.TBI.captureview.engine.action.ActionHolder;
import com.TBI.captureview.engine.action.BaseAction;
import com.TBI.captureview.engine.action.CompletionCallback;
import com.TBI.captureview.internal.utils.CamcorderProfiles;
import com.TBI.captureview.size.Size;

/**
 * TBI Demo
 */
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
public class Full2VideoRecorder extends FullVideoRecorder {

    private static final String TAG = Full2VideoRecorder.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    private ActionHolder mHolder;
    private final String mCameraId;
    private Surface mInputSurface;

    public Full2VideoRecorder(@NonNull Camera2Engine engine, @NonNull String cameraId) {
        super(engine);
        mHolder = engine;
        mCameraId = cameraId;
    }

    @Override
    protected void onStart() {
        Action action = new BaseAction() {
            @Override
            public void onCaptureStarted(@NonNull ActionHolder holder, @NonNull CaptureRequest request) {
                super.onCaptureStarted(holder, request);
                Object tag = holder.getBuilder(this).build().getTag();
                Object currentTag = request.getTag();
                if (tag == null ? currentTag == null : tag.equals(currentTag)) {
                    setState(STATE_COMPLETED);
                }
            }
        };
        action.addCallback(new CompletionCallback() {
            @Override
            protected void onActionCompleted(@NonNull Action action) {
                Full2VideoRecorder.super.onStart();
            }
        });
        action.start(mHolder);
    }

    @SuppressLint("NewApi")
    @Override
    protected boolean onPrepareMediaRecorder(@NonNull VideoResult.Stub stub, @NonNull MediaRecorder mediaRecorder) {
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        Size size = stub.rotation % 180 != 0 ? stub.size.flip() : stub.size;
        mProfile = CamcorderProfiles.get(mCameraId, size);
        return super.onPrepareMediaRecorder(stub, mediaRecorder);
    }

    @NonNull
    public Surface createInputSurface(@NonNull VideoResult.Stub stub) throws PrepareException {
        if (!prepareMediaRecorder(stub)) {
            throw new PrepareException(mError);
        }
        mInputSurface = mMediaRecorder.getSurface();
        return mInputSurface;
    }

    @Nullable
    public Surface getInputSurface() {
        return mInputSurface;
    }

    public class PrepareException extends Exception {
        private PrepareException(Throwable cause) {
            super(cause);
        }
    }

}
