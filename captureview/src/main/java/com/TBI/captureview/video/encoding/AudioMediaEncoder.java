package com.TBI.captureview.video.encoding;

import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaRecorder;
import android.os.Build;

import com.TBI.captureview.CameraLogger;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
/**
 * TBI Demo
 */
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class AudioMediaEncoder extends MediaEncoder {

    private static final String TAG = AudioMediaEncoder.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    private static final boolean PERFORMANCE_DEBUG = false;
    private static final boolean PERFORMANCE_FILL_GAPS = true;
    private static final int PERFORMANCE_MAX_GAPS = 8;

    private boolean mRequestStop = false;
    private AudioEncodingThread mEncoder;
    private AudioRecordingThread mRecorder;
    private ByteBufferPool mByteBufferPool;
    private final AudioTimestamp mTimestamp;
    private AudioConfig mConfig;
    private InputBufferPool mInputBufferPool = new InputBufferPool();
    private final LinkedBlockingQueue<InputBuffer> mInputBufferQueue = new LinkedBlockingQueue<>();
    private AudioNoise mAudioNoise;

    // Just to debug performance.
    private int mDebugSendCount = 0;
    private int mDebugExecuteCount = 0;
    private long mDebugSendAvgDelay = 0;
    private long mDebugExecuteAvgDelay = 0;
    private Map<Long, Long> mDebugSendStartMap = new HashMap<>();

    public AudioMediaEncoder(@NonNull AudioConfig config) {
        super("AudioEncoder");
        mConfig = config.copy();
        mTimestamp = new AudioTimestamp(mConfig.byteRate());
        // These two were in onPrepare() but it's better to do warm-up here
        // since thread and looper creation is expensive.
        mEncoder = new AudioEncodingThread();
        mRecorder = new AudioRecordingThread();
    }

    @EncoderThread
    @Override
    protected void onPrepare(@NonNull MediaEncoderEngine.Controller controller, long maxLengthUs) {
        final MediaFormat audioFormat = MediaFormat.createAudioFormat(
                mConfig.mimeType,
                mConfig.samplingFrequency,
                mConfig.channels);
        audioFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        audioFormat.setInteger(MediaFormat.KEY_CHANNEL_MASK, mConfig.audioFormatChannels());
        audioFormat.setInteger(MediaFormat.KEY_BIT_RATE, mConfig.bitRate);
        try {
            if (mConfig.encoder != null) {
                mMediaCodec = MediaCodec.createByCodecName(mConfig.encoder);
            } else {
                mMediaCodec = MediaCodec.createEncoderByType(mConfig.mimeType);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        mMediaCodec.configure(audioFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        mMediaCodec.start();
        mByteBufferPool = new ByteBufferPool(mConfig.frameSize(), mConfig.bufferPoolMaxSize());
        mAudioNoise = new AudioNoise(mConfig);
    }

    @EncoderThread
    @Override
    protected void onStart() {
        mRequestStop = false;
        mRecorder.start();
        mEncoder.start();
    }

    @EncoderThread
    @Override
    protected void onStop() {
        mRequestStop = true;
    }

    @Override
    protected void onStopped() {
        super.onStopped();
        mRequestStop = false;
        mEncoder = null;
        mRecorder = null;
        if (mByteBufferPool != null) {
            mByteBufferPool.clear();
            mByteBufferPool = null;
        }
    }

    @Override
    protected int getEncodedBitRate() {
        return mConfig.bitRate;
    }

    private void skipFrames(int frames) {
        try {
            Thread.sleep(AudioTimestamp.bytesToMillis(
                    mConfig.frameSize() * frames,
                    mConfig.byteRate()));
        } catch (InterruptedException ignore) {}
    }

    private class AudioRecordingThread extends Thread {

        private AudioRecord mAudioRecord;
        private ByteBuffer mCurrentBuffer;
        private int mCurrentReadBytes;

        private long mLastTimeUs;
        private long mFirstTimeUs = Long.MIN_VALUE;

        private AudioRecordingThread() {
            setPriority(Thread.MAX_PRIORITY);
            final int minBufferSize = AudioRecord.getMinBufferSize(
                    mConfig.samplingFrequency,
                    mConfig.audioFormatChannels(),
                    mConfig.encoding);

            int bufferSize = mConfig.frameSize() * mConfig.audioRecordBufferFrames();
            while (bufferSize < minBufferSize) {
                bufferSize += mConfig.frameSize(); // Unlikely.
            }
            mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.CAMCORDER,
                    mConfig.samplingFrequency,
                    mConfig.audioFormatChannels(),
                    mConfig.encoding,
                    bufferSize);
        }

        @Override
        public void run() {
            mAudioRecord.startRecording();
            while (!mRequestStop) {
                if (!hasReachedMaxLength()) {
                    read(false);
                } else {
                    //noinspection UnnecessaryContinue
                    continue;
                }
            }
            LOG.w("Stop was requested. We're out of the loop. Will post an endOfStream.");
            read(true);
            mAudioRecord.stop();
            mAudioRecord.release();
            mAudioRecord = null;
        }

        private void read(boolean endOfStream) {
            mCurrentBuffer = mByteBufferPool.get();
            if (mCurrentBuffer == null) {
                if (endOfStream) {
                    LOG.v("read thread - eos: true - No buffer, retrying.");
                    read(true); // try again
                } else {
                    LOG.w("read thread - eos: false - Skipping audio frame, encoding is too slow.");
                    skipFrames(6); // sleep a bit
                }
            } else {
                mCurrentBuffer.clear();
                if (PERFORMANCE_DEBUG) {
                    long before = System.nanoTime();
                    mCurrentReadBytes = mAudioRecord.read(mCurrentBuffer, mConfig.frameSize());
                    long after = System.nanoTime();
                    float delayMillis = (after - before) / 1000000F;
                    float durationMillis = AudioTimestamp.bytesToMillis(mCurrentReadBytes, mConfig.byteRate());
                    LOG.v("read thread - reading took:", delayMillis,
                            "should be:", durationMillis,
                            "delay:", delayMillis - durationMillis);
                } else {
                    mCurrentReadBytes = mAudioRecord.read(mCurrentBuffer, mConfig.frameSize());
                }
                LOG.i("read thread - eos:", endOfStream, "- Read new audio frame. Bytes:", mCurrentReadBytes);
                if (mCurrentReadBytes > 0) { // Good read: increase PTS.
                    increaseTime(mCurrentReadBytes, endOfStream);
                    LOG.i("read thread - eos:", endOfStream, "- mLastTimeUs:", mLastTimeUs);
                    mCurrentBuffer.limit(mCurrentReadBytes);
                    enqueue(mCurrentBuffer, mLastTimeUs, endOfStream);
                } else if (mCurrentReadBytes == AudioRecord.ERROR_INVALID_OPERATION) {
                    LOG.e("read thread - eos:", endOfStream, "- Got AudioRecord.ERROR_INVALID_OPERATION");
                } else if (mCurrentReadBytes == AudioRecord.ERROR_BAD_VALUE) {
                    LOG.e("read thread - eos:", endOfStream, "- Got AudioRecord.ERROR_BAD_VALUE");
                }
            }
        }

        private void increaseTime(int readBytes, boolean endOfStream) {
            // Get the latest frame timestamp.
            mLastTimeUs = mTimestamp.increaseUs(readBytes);
            if (mFirstTimeUs == Long.MIN_VALUE) {
                mFirstTimeUs = mLastTimeUs;
                // Compute the first frame milliseconds as well.
                notifyFirstFrameMillis(System.currentTimeMillis()
                        - AudioTimestamp.bytesToMillis(readBytes, mConfig.byteRate()));
            }

            // See if we reached the max length value.
            if (!hasReachedMaxLength()) {
                boolean didReachMaxLength = (mLastTimeUs - mFirstTimeUs) > getMaxLengthUs();
                if (didReachMaxLength && !endOfStream) {
                    LOG.w("read thread - this frame reached the maxLength! deltaUs:", mLastTimeUs - mFirstTimeUs);
                    notifyMaxLengthReached();
                }
            }

            // Maybe add noise.
            maybeAddNoise();
        }

        private void enqueue(@NonNull ByteBuffer byteBuffer, long timestamp, boolean isEndOfStream) {
            if (PERFORMANCE_DEBUG) {
                mDebugSendStartMap.put(timestamp, System.nanoTime() / 1000000);
            }
            int readBytes = byteBuffer.remaining();
            InputBuffer inputBuffer = mInputBufferPool.get();
            //noinspection ConstantConditions
            inputBuffer.source = byteBuffer;
            inputBuffer.timestamp = timestamp;
            inputBuffer.length = readBytes;
            inputBuffer.isEndOfStream = isEndOfStream;
            mInputBufferQueue.add(inputBuffer);
        }

        private void maybeAddNoise() {
            if (!PERFORMANCE_FILL_GAPS) return;
            int gaps = mTimestamp.getGapCount(mConfig.frameSize());
            if (gaps <= 0) return;

            long gapStart = mTimestamp.getGapStartUs(mLastTimeUs);
            long frameUs = AudioTimestamp.bytesToUs(mConfig.frameSize(), mConfig.byteRate());
            LOG.w("read thread - GAPS: trying to add", gaps, "noise buffers. PERFORMANCE_MAX_GAPS:", PERFORMANCE_MAX_GAPS);
            for (int i = 0; i < Math.min(gaps, PERFORMANCE_MAX_GAPS); i++) {
                ByteBuffer noiseBuffer = mByteBufferPool.get();
                if (noiseBuffer == null) {
                    LOG.e("read thread - GAPS: aborting because we have no free buffer.");
                    break;
                }
                noiseBuffer.clear();
                mAudioNoise.fill(noiseBuffer);
                noiseBuffer.rewind();
                enqueue(noiseBuffer, gapStart, false);
                gapStart += frameUs;
            }
        }
    }

    private class AudioEncodingThread extends Thread {
        private AudioEncodingThread() {
            setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run() {
            encoding: while (true) {
                if (mInputBufferQueue.isEmpty()) {
                    skipFrames(2);
                } else {
                    LOG.i("encoding thread - performing", mInputBufferQueue.size(), "pending operations.");
                    InputBuffer inputBuffer;
                    while ((inputBuffer = mInputBufferQueue.peek()) != null) {

                        // Performance logging
                        if (PERFORMANCE_DEBUG) {
                            long sendEnd = System.nanoTime() / 1000000;
                            Long sendStart = mDebugSendStartMap.remove(inputBuffer.timestamp);
                            //noinspection StatementWithEmptyBody
                            if (sendStart != null) {
                                mDebugSendAvgDelay = ((mDebugSendAvgDelay * mDebugSendCount) + (sendEnd - sendStart)) / (++mDebugSendCount);
                                LOG.v("send delay millis:", sendEnd - sendStart, "average:", mDebugSendAvgDelay);
                            } else {
                                // This input buffer was already processed (but tryAcquire failed for now).
                            }
                        }

                        // Actual work
                        if (inputBuffer.isEndOfStream) {
                            acquireInputBuffer(inputBuffer);
                            encode(inputBuffer);
                            break encoding;
                        } else if (tryAcquireInputBuffer(inputBuffer)) {
                            encode(inputBuffer);
                        } else {
                            skipFrames(1);
                        }
                    }
                }
            }
            // We got an end of stream.
            mInputBufferPool.clear();
            if (PERFORMANCE_DEBUG) {
                LOG.e("EXECUTE DELAY MILLIS:", mDebugExecuteAvgDelay, "COUNT:", mDebugExecuteCount);
                LOG.e("SEND DELAY MILLIS:", mDebugSendAvgDelay, "COUNT:", mDebugSendCount);
            }
        }

        private void encode(@NonNull InputBuffer buffer) {
            long executeStart = System.nanoTime() / 1000000;

            LOG.i("encoding thread - performing pending operation for timestamp:", buffer.timestamp, "- encoding.");
            buffer.data.put(buffer.source); // NOTE: this copy is prob. the worst part here for performance
            mByteBufferPool.recycle(buffer.source);
            mInputBufferQueue.remove(buffer);
            encodeInputBuffer(buffer);
            boolean eos = buffer.isEndOfStream;
            mInputBufferPool.recycle(buffer);
            LOG.i("encoding thread - performing pending operation for timestamp:", buffer.timestamp, "- draining.");
            drainOutput(eos);

            if (PERFORMANCE_DEBUG) {
                long executeEnd = System.nanoTime() / 1000000;
                mDebugExecuteAvgDelay = ((mDebugExecuteAvgDelay * mDebugExecuteCount) + (executeEnd - executeStart)) / (++mDebugExecuteCount);
                LOG.v("execute delay millis:", executeEnd - executeStart, "average:", mDebugExecuteAvgDelay);
            }
        }
    }
}
