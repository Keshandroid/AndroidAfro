package com.TBI.captureview.video.encoding;

import android.media.AudioFormat;

import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
@SuppressWarnings("WeakerAccess")
public class AudioConfig {

    // Configurable options
    public int bitRate; // ENCODED bit rate
    public int channels = 1;
    public String encoder;
    public String mimeType = "audio/mp4a-latm";

    // Not configurable options (for now)
    final int encoding = AudioFormat.ENCODING_PCM_16BIT; // Determines the sampleSizePerChannel
    // The 44.1KHz frequency is the only setting guaranteed to be available on all devices.
    final int samplingFrequency = 44100; // samples/sec
    final int sampleSizePerChannel = 2; // byte/sample/channel [16bit]. If this changes, review noise introduction
    final int byteRatePerChannel = samplingFrequency * sampleSizePerChannel; // byte/sec/channel

    @NonNull
    AudioConfig copy() {
        AudioConfig config = new AudioConfig();
        config.bitRate = this.bitRate;
        config.channels = this.channels;
        config.encoder = this.encoder;
        config.mimeType = mimeType;
        return config;
    }

    int byteRate() { // RAW byte rate
        return byteRatePerChannel * channels; // byte/sec
    }

    @SuppressWarnings("unused")
    int bitRate() { // RAW bit rate
        return byteRate() * 8; // bit/sec
    }

    int audioFormatChannels() {
        if (channels == 1) {
            return AudioFormat.CHANNEL_IN_MONO;
        } else if (channels == 2) {
            return AudioFormat.CHANNEL_IN_STEREO;
        }
        throw new RuntimeException("Invalid number of channels: " + channels);
    }

    int frameSize() {
        return 1024 * channels;
    }

    int audioRecordBufferFrames() {
        return 50;
    }

    int bufferPoolMaxSize() {
        return 500;
    }
}
