package com.TBI.captureview.video.encoding;

import com.TBI.captureview.internal.utils.Pool;

/**
 * TBI Demo
 */
class InputBufferPool extends Pool<InputBuffer> {

    InputBufferPool() {
        super(Integer.MAX_VALUE, new Factory<InputBuffer>() {
            @Override
            public InputBuffer create() {
                return new InputBuffer();
            }
        });
    }
}
