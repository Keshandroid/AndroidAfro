package com.TBI.captureview.video.encoding;

import android.opengl.Matrix;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.filter.Filter;
import com.TBI.captureview.internal.egl.EglCore;
import com.TBI.captureview.internal.egl.EglViewport;
import com.TBI.captureview.internal.egl.EglWindowSurface;
import com.TBI.captureview.internal.utils.Pool;

/**
 * TBI Demo
 */
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class TextureMediaEncoder extends VideoMediaEncoder<TextureConfig> {

    private static final String TAG = TextureMediaEncoder.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    public final static String FRAME_EVENT = "frame";
    public final static String FILTER_EVENT = "filter";

    private int mTransformRotation;
    private EglCore mEglCore;
    private EglWindowSurface mWindow;
    private EglViewport mViewport;
    private Pool<Frame> mFramePool = new Pool<>(Integer.MAX_VALUE, new Pool.Factory<Frame>() {
        @Override
        public Frame create() {
            return new Frame();
        }
    });

    private long mFirstTimeUs = Long.MIN_VALUE;

    public TextureMediaEncoder(@NonNull TextureConfig config) {
        super(config.copy());
    }

    public static class Frame {
        private Frame() {}

        public long timestampNanos;
        public long timestampMillis;
        public float[] transform = new float[16];

        private long timestampUs() {
            return timestampNanos / 1000L;
        }
    }

    @NonNull
    public Frame acquireFrame() {
        if (mFramePool.isEmpty()) {
            throw new RuntimeException("Need more frames than this! Please increase the pool size.");
        } else {
            //noinspection ConstantConditions
            return mFramePool.get();
        }
    }

    @EncoderThread
    @Override
    protected void onPrepare(@NonNull MediaEncoderEngine.Controller controller, long maxLengthUs) {
        mTransformRotation = mConfig.rotation;
        mConfig.rotation = 0;
        super.onPrepare(controller, maxLengthUs);
        mEglCore = new EglCore(mConfig.eglContext, EglCore.FLAG_RECORDABLE);
        mWindow = new EglWindowSurface(mEglCore, mSurface, true);
        mWindow.makeCurrent();
        mViewport = new EglViewport();
    }

    @Override
    protected boolean shouldRenderFrame(long timestampUs) {
        if (!super.shouldRenderFrame(timestampUs)) {
            return false;
        } else if (mFrameNumber <= 10) {
            // Always render the first few frames, or muxer fails.
            return true;
        } else if (getPendingEvents(FRAME_EVENT) > 2) {
            LOG.w("shouldRenderFrame - Dropping frame because we already have too many pending events:",
                    getPendingEvents(FRAME_EVENT));
            return false;
        } else {
            return true;
        }
    }

    @EncoderThread
    @Override
    protected void onEvent(@NonNull String event, @Nullable Object data) {
        switch (event) {
            case FILTER_EVENT:
                //noinspection ConstantConditions
                onFilter((Filter) data);
                break;
            case FRAME_EVENT:
                //noinspection ConstantConditions
                onFrame((Frame) data);
                break;
        }
    }

    private void onFilter(@NonNull Filter filter) {
        mViewport.setFilter(filter);
    }

    private void onFrame(@NonNull Frame frame) {
        if (!shouldRenderFrame(frame.timestampUs())) {
            mFramePool.recycle(frame);
            return;
        }

        // Notify we're got the first frame and its absolute time.
        if (mFrameNumber == 1) {
            notifyFirstFrameMillis(frame.timestampMillis);
        }

        // Notify we have reached the max length value.
        if (mFirstTimeUs == Long.MIN_VALUE) mFirstTimeUs = frame.timestampUs();
        if (!hasReachedMaxLength()) {
            boolean didReachMaxLength = (frame.timestampUs() - mFirstTimeUs) > getMaxLengthUs();
            if (didReachMaxLength) {
                LOG.w("onEvent -",
                        "frameNumber:", mFrameNumber,
                        "timestampUs:", frame.timestampUs(),
                        "firstTimeUs:", mFirstTimeUs,
                        "- reached max length! deltaUs:", frame.timestampUs() - mFirstTimeUs);
                notifyMaxLengthReached();
            }
        }

        // First, drain any previous data.
        LOG.i("onEvent -",
                "frameNumber:", mFrameNumber,
                "timestampUs:", frame.timestampUs(),
                "- draining.");
        drainOutput(false);

        // Then draw on the surface.
        LOG.i("onEvent -",
                "frameNumber:", mFrameNumber,
                "timestampUs:", frame.timestampUs(),
                "- rendering.");

        float[] transform = frame.transform;
        float scaleX = mConfig.scaleX;
        float scaleY = mConfig.scaleY;
        float scaleTranslX = (1F - scaleX) / 2F;
        float scaleTranslY = (1F - scaleY) / 2F;
        Matrix.translateM(transform, 0, scaleTranslX, scaleTranslY, 0);
        Matrix.scaleM(transform, 0, scaleX, scaleY, 1);
        Matrix.translateM(transform, 0, 0.5F, 0.5F, 0);
        Matrix.rotateM(transform, 0, mTransformRotation, 0, 0, 1);
        Matrix.translateM(transform, 0, -0.5F, -0.5F, 0);

        if (mConfig.hasOverlay()) {
            mConfig.overlayDrawer.draw(mConfig.overlayTarget);
            Matrix.translateM(mConfig.overlayDrawer.getTransform(), 0, 0.5F, 0.5F, 0);
            Matrix.rotateM(mConfig.overlayDrawer.getTransform(), 0, mConfig.overlayRotation, 0, 0, 1);
            Matrix.translateM(mConfig.overlayDrawer.getTransform(), 0, -0.5F, -0.5F, 0);
        }
        mViewport.drawFrame(mConfig.textureId, transform);
        if (mConfig.hasOverlay()) {
            mConfig.overlayDrawer.render();
        }
        mWindow.setPresentationTime(frame.timestampNanos);
        mWindow.swapBuffers();
        mFramePool.recycle(frame);
    }

    @Override
    protected void onStopped() {
        super.onStopped();
        mFramePool.clear();
        if (mWindow != null) {
            mWindow.release();
            mWindow = null;
        }
        if (mViewport != null) {
            mViewport.release();
            mViewport = null;
        }
        if (mEglCore != null) {
            mEglCore.release();
            mEglCore = null;
        }
    }
}
