package com.TBI.captureview.video;

import android.media.CamcorderProfile;
import android.media.MediaRecorder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.VideoResult;
import com.TBI.captureview.controls.Audio;
import com.TBI.captureview.controls.VideoCodec;
import com.TBI.captureview.internal.DeviceEncoders;

public abstract class FullVideoRecorder extends VideoRecorder {

    private static final String TAG = FullVideoRecorder.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    @SuppressWarnings("WeakerAccess") protected MediaRecorder mMediaRecorder;
    @SuppressWarnings("WeakerAccess") protected CamcorderProfile mProfile;
    private boolean mMediaRecorderPrepared;


    FullVideoRecorder(@Nullable VideoResultListener listener) {
        super(listener);
    }

    @SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
    protected boolean prepareMediaRecorder(@NonNull VideoResult.Stub stub) {
        if (mMediaRecorderPrepared) return true;
        return onPrepareMediaRecorder(stub, new MediaRecorder());
    }

    protected boolean onPrepareMediaRecorder(@NonNull VideoResult.Stub stub, @NonNull MediaRecorder mediaRecorder) {
        mMediaRecorder = mediaRecorder;
        boolean hasAudio = stub.audio == Audio.ON
                || stub.audio == Audio.MONO
                || stub.audio == Audio.STEREO;
        if (hasAudio) {
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        }
        mMediaRecorder.setOutputFormat(mProfile.fileFormat);

        String audioType;
        switch (mProfile.audioCodec) {
            case MediaRecorder.AudioEncoder.AMR_NB: audioType = "audio/3gpp"; break;
            case MediaRecorder.AudioEncoder.AMR_WB: audioType = "audio/amr-wb"; break;
            case MediaRecorder.AudioEncoder.AAC:
            case MediaRecorder.AudioEncoder.HE_AAC:
            case MediaRecorder.AudioEncoder.AAC_ELD: audioType = "audio/mp4a-latm"; break;
            case MediaRecorder.AudioEncoder.VORBIS: audioType = "audio/vorbis"; break;
            case MediaRecorder.AudioEncoder.DEFAULT:
            default: audioType = "audio/3gpp";
        }

        String videoType;
        if (stub.videoCodec == VideoCodec.H_264) mProfile.videoCodec = MediaRecorder.VideoEncoder.H264;
        if (stub.videoCodec == VideoCodec.H_263) mProfile.videoCodec = MediaRecorder.VideoEncoder.H263;
        switch (mProfile.videoCodec) {
            case MediaRecorder.VideoEncoder.H263: videoType = "video/3gpp"; break;
            case MediaRecorder.VideoEncoder.H264: videoType = "video/avc"; break;
            case MediaRecorder.VideoEncoder.MPEG_4_SP: videoType = "video/mp4v-es"; break;
            case MediaRecorder.VideoEncoder.VP8: videoType = "video/x-vnd.on2.vp8"; break;
            case MediaRecorder.VideoEncoder.HEVC: videoType = "video/hevc"; break;
            case MediaRecorder.VideoEncoder.DEFAULT:
            default: videoType = "video/avc";
        }

        // Merge stub and profile
        stub.videoFrameRate = stub.videoFrameRate > 0 ? stub.videoFrameRate : mProfile.videoFrameRate;
        stub.videoBitRate = stub.videoBitRate > 0 ? stub.videoBitRate : mProfile.videoBitRate;
        if (hasAudio) {
            stub.audioBitRate = stub.audioBitRate > 0 ? stub.audioBitRate : mProfile.audioBitRate;
        }

        // Check DeviceEncoders support
        DeviceEncoders encoders = new DeviceEncoders(videoType, audioType, DeviceEncoders.MODE_TAKE_FIRST);
        boolean flip = stub.rotation % 180 != 0;
        if (flip) stub.size = stub.size.flip();
        stub.size = encoders.getSupportedVideoSize(stub.size);
        stub.videoBitRate = encoders.getSupportedVideoBitRate(stub.videoBitRate);
        stub.audioBitRate = encoders.getSupportedAudioBitRate(stub.audioBitRate);
        stub.videoFrameRate = encoders.getSupportedVideoFrameRate(stub.size, stub.videoFrameRate);
        if (flip) stub.size = stub.size.flip();

        // Set video params
        mMediaRecorder.setVideoSize(
                flip ? stub.size.getHeight() : stub.size.getWidth(),
                flip ? stub.size.getWidth() : stub.size.getHeight());
        mMediaRecorder.setVideoFrameRate(stub.videoFrameRate);
        mMediaRecorder.setVideoEncoder(mProfile.videoCodec);
        mMediaRecorder.setVideoEncodingBitRate(stub.videoBitRate);

        // Set audio params
        if (hasAudio) {
            if (stub.audio == Audio.ON) {
                mMediaRecorder.setAudioChannels(mProfile.audioChannels);
            } else if (stub.audio == Audio.MONO) {
                mMediaRecorder.setAudioChannels(1);
            } else if (stub.audio == Audio.STEREO) {
                mMediaRecorder.setAudioChannels(2);
            }
            mMediaRecorder.setAudioSamplingRate(mProfile.audioSampleRate);
            mMediaRecorder.setAudioEncoder(mProfile.audioCodec);
            mMediaRecorder.setAudioEncodingBitRate(stub.audioBitRate);
        }

        // Set other params
        if (stub.location != null) {
            mMediaRecorder.setLocation(
                    (float) stub.location.getLatitude(),
                    (float) stub.location.getLongitude());
        }
        mMediaRecorder.setOutputFile(stub.file.getAbsolutePath());
        mMediaRecorder.setOrientationHint(stub.rotation);
        mMediaRecorder.setMaxFileSize(stub.maxSize);
        mMediaRecorder.setMaxDuration(stub.maxDuration);
        mMediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mediaRecorder, int what, int extra) {
                switch (what) {
                    case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
                        mResult.endReason = VideoResult.REASON_MAX_DURATION_REACHED;
                        stop(false);
                        break;
                    case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:
                        mResult.endReason = VideoResult.REASON_MAX_SIZE_REACHED;
                        stop(false);
                        break;
                }
            }
        });

        // Prepare the Recorder
        try {
            mMediaRecorder.prepare();
            mMediaRecorderPrepared = true;
            mError = null;
            return true;
        } catch (Exception e) {
            LOG.w("prepareMediaRecorder:", "Error while preparing media recorder.", e);
            mMediaRecorderPrepared = false;
            mError = e;
            return false;
        }
    }

    @Override
    protected void onStart() {
        if (!prepareMediaRecorder(mResult)) {
            mResult = null;
            stop(false);
            return;
        }

        try {
            mMediaRecorder.start();
            dispatchVideoRecordingStart();
        } catch (Exception e) {
            LOG.w("start:", "Error while starting media recorder.", e);
            mResult = null;
            mError = e;
            stop(false);
        }
    }

    @Override
    protected void onStop(boolean isCameraShutdown) {
        if (mMediaRecorder != null) {
            dispatchVideoRecordingEnd();
            try {
                mMediaRecorder.stop();
            } catch (Exception e) {
                LOG.w("stop:", "Error while closing media recorder.", e);
                mResult = null;
                if (mError == null) mError = e;
            }
            mMediaRecorder.release();
        }
        mProfile = null;
        mMediaRecorder = null;
        mMediaRecorderPrepared = false;
        dispatchResult();
    }

}
