package com.TBI.captureview;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.PointF;
import android.graphics.Rect;
import android.location.Location;
import android.media.MediaActionSound;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

import com.TBI.captureview.controls.Audio;
import com.TBI.captureview.controls.Control;
import com.TBI.captureview.controls.ControlParser;
import com.TBI.captureview.controls.Engine;
import com.TBI.captureview.controls.Facing;
import com.TBI.captureview.controls.Flash;
import com.TBI.captureview.controls.Grid;
import com.TBI.captureview.controls.Hdr;
import com.TBI.captureview.controls.Mode;
import com.TBI.captureview.controls.Preview;
import com.TBI.captureview.controls.VideoCodec;
import com.TBI.captureview.controls.WhiteBalance;
import com.TBI.captureview.engine.Camera1Engine;
import com.TBI.captureview.engine.Camera2Engine;
import com.TBI.captureview.engine.CameraEngine;
import com.TBI.captureview.engine.offset.Reference;
import com.TBI.captureview.filter.Filter;
import com.TBI.captureview.filter.FilterParser;
import com.TBI.captureview.filter.NoFilter;
import com.TBI.captureview.filter.OneParameterFilter;
import com.TBI.captureview.filter.TwoParameterFilter;
import com.TBI.captureview.frame.Frame;
import com.TBI.captureview.frame.FrameProcessor;
import com.TBI.captureview.gesture.Gesture;
import com.TBI.captureview.gesture.GestureAction;
import com.TBI.captureview.gesture.GestureFinder;
import com.TBI.captureview.gesture.GestureParser;
import com.TBI.captureview.gesture.PinchGestureFinder;
import com.TBI.captureview.gesture.ScrollGestureFinder;
import com.TBI.captureview.gesture.SwipeGesture;
import com.TBI.captureview.gesture.TapGestureFinder;
import com.TBI.captureview.internal.GridLinesLayout;
import com.TBI.captureview.internal.utils.CropHelper;
import com.TBI.captureview.internal.utils.OrientationHelper;
import com.TBI.captureview.internal.utils.WorkerHandler;
import com.TBI.captureview.markers.AutoFocusMarker;
import com.TBI.captureview.markers.AutoFocusTrigger;
import com.TBI.captureview.markers.MarkerLayout;
import com.TBI.captureview.markers.MarkerParser;
import com.TBI.captureview.overlay.OverlayLayout;
import com.TBI.captureview.preview.CameraPreview;
import com.TBI.captureview.preview.FilterCameraPreview;
import com.TBI.captureview.preview.GlCameraPreview;
import com.TBI.captureview.preview.SurfaceCameraPreview;
import com.TBI.captureview.preview.TextureCameraPreview;
import com.TBI.captureview.size.AspectRatio;
import com.TBI.captureview.size.Size;
import com.TBI.captureview.size.SizeSelector;
import com.TBI.captureview.size.SizeSelectorParser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static android.view.View.MeasureSpec.AT_MOST;
import static android.view.View.MeasureSpec.EXACTLY;
import static android.view.View.MeasureSpec.UNSPECIFIED;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * TBI Demo
 */
public class CameraView extends FrameLayout implements LifecycleObserver {

    private final static String TAG = CameraView.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    public final static int PERMISSION_REQUEST_CODE = 16;

    final static long DEFAULT_AUTOFOCUS_RESET_DELAY_MILLIS = 3000;
    final static boolean DEFAULT_PLAY_SOUNDS = true;
    final static boolean DEFAULT_USE_DEVICE_ORIENTATION = true;
    final static boolean DEFAULT_PICTURE_METERING = true;
    final static boolean DEFAULT_PICTURE_SNAPSHOT_METERING = false;

    // Self managed parameters
    private boolean mPlaySounds;
    private boolean mUseDeviceOrientation;
    private HashMap<Gesture, GestureAction> mGestureMap = new HashMap<>(4);
    private Preview mPreview;
    private Engine mEngine;
    private Filter mPendingFilter;

    // Components
    @VisibleForTesting
    CameraCallbacks mCameraCallbacks;
    private CameraPreview mCameraPreview;
    private OrientationHelper mOrientationHelper;
    private CameraEngine mCameraEngine;
    private MediaActionSound mSound;
    private AutoFocusMarker mAutoFocusMarker;
    @VisibleForTesting
    List<CameraListener> mListeners = new CopyOnWriteArrayList<>();
    @VisibleForTesting
    List<FrameProcessor> mFrameProcessors = new CopyOnWriteArrayList<>();
    private Lifecycle mLifecycle;

    // Gestures
    @VisibleForTesting
    PinchGestureFinder mPinchGestureFinder;
    @VisibleForTesting
    TapGestureFinder mTapGestureFinder;
    @VisibleForTesting
    ScrollGestureFinder mScrollGestureFinder;
    @VisibleForTesting
    SwipeGesture swipeGesture;
    //DetectSwipeGestureListener detectSwipeGestureListener;

    // Views
    @VisibleForTesting
    GridLinesLayout mGridLinesLayout;
    @VisibleForTesting
    MarkerLayout mMarkerLayout;
    private boolean mKeepScreenOn;
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private boolean mExperimental;
    private boolean mInEditor;

    // Overlays
    @VisibleForTesting
    OverlayLayout mOverlayLayout;

    // Threading
    private Handler mUiHandler;
    private WorkerHandler mFrameProcessorsHandler;

    public CameraView(@NonNull Context context) {
        super(context, null);
        initialize(context, null);
    }

    public CameraView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    //region Init

    @SuppressWarnings("WrongConstant")
    private void initialize(@NonNull Context context, @Nullable AttributeSet attrs) {
        mInEditor = isInEditMode();
        if (mInEditor) return;

        setWillNotDraw(false);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CameraView, 0, 0);
        ControlParser controls = new ControlParser(context, a);

        // Self managed
        boolean playSounds = a.getBoolean(R.styleable.CameraView_cameraPlaySounds, DEFAULT_PLAY_SOUNDS);
        boolean useDeviceOrientation = a.getBoolean(R.styleable.CameraView_cameraUseDeviceOrientation, DEFAULT_USE_DEVICE_ORIENTATION);
        mExperimental = a.getBoolean(R.styleable.CameraView_cameraExperimental, false);
        mPreview = controls.getPreview();
        mEngine = controls.getEngine();

        // Camera engine params
        int gridColor = a.getColor(R.styleable.CameraView_cameraGridColor, GridLinesLayout.DEFAULT_COLOR);
        long videoMaxSize = (long) a.getFloat(R.styleable.CameraView_cameraVideoMaxSize, 0);
        int videoMaxDuration = a.getInteger(R.styleable.CameraView_cameraVideoMaxDuration, 0);
        int videoBitRate = a.getInteger(R.styleable.CameraView_cameraVideoBitRate, 0);
        int audioBitRate = a.getInteger(R.styleable.CameraView_cameraAudioBitRate, 0);
        long autoFocusResetDelay = (long) a.getInteger(R.styleable.CameraView_cameraAutoFocusResetDelay, (int) DEFAULT_AUTOFOCUS_RESET_DELAY_MILLIS);
        boolean pictureMetering = a.getBoolean(R.styleable.CameraView_cameraPictureMetering, DEFAULT_PICTURE_METERING);
        boolean pictureSnapshotMetering = a.getBoolean(R.styleable.CameraView_cameraPictureSnapshotMetering, DEFAULT_PICTURE_SNAPSHOT_METERING);

        // Size selectors and gestures
        SizeSelectorParser sizeSelectors = new SizeSelectorParser(a);
        GestureParser gestures = new GestureParser(a);
        MarkerParser markers = new MarkerParser(a);
        FilterParser filters = new FilterParser(a);

        a.recycle();

        // Components
        mCameraCallbacks = new CameraCallbacks();
        mUiHandler = new Handler(Looper.getMainLooper());
        mFrameProcessorsHandler = WorkerHandler.get("FrameProcessorsWorker");

        // Gestures
        mPinchGestureFinder = new PinchGestureFinder(mCameraCallbacks);
        mTapGestureFinder = new TapGestureFinder(mCameraCallbacks);
        mScrollGestureFinder = new ScrollGestureFinder(mCameraCallbacks);
        swipeGesture = new SwipeGesture(mCameraCallbacks);

        // Views
        mGridLinesLayout = new GridLinesLayout(context);
        mOverlayLayout = new OverlayLayout(context);
        mMarkerLayout = new MarkerLayout(context);
        addView(mGridLinesLayout);
        addView(mMarkerLayout);
        addView(mOverlayLayout);

        // Create the engine
        doInstantiateEngine();

        // Apply self managed
        setPlaySounds(playSounds);
        setUseDeviceOrientation(useDeviceOrientation);
        setGrid(controls.getGrid());
        setGridColor(gridColor);

        // Apply camera engine params
        // Adding new ones? See setEngine().
        setFacing(controls.getFacing());
        setFlash(controls.getFlash());
        setMode(controls.getMode());
        setWhiteBalance(controls.getWhiteBalance());
        setHdr(controls.getHdr());
        setAudio(controls.getAudio());
        setAudioBitRate(audioBitRate);
        setPictureSize(sizeSelectors.getPictureSizeSelector());
        setPictureMetering(pictureMetering);
        setPictureSnapshotMetering(pictureSnapshotMetering);
        setVideoSize(sizeSelectors.getVideoSizeSelector());
        setVideoCodec(controls.getVideoCodec());
        setVideoMaxSize(videoMaxSize);
        setVideoMaxDuration(videoMaxDuration);
        setVideoBitRate(videoBitRate);
        setAutoFocusResetDelay(autoFocusResetDelay);

        // Apply gestures
        mapGesture(Gesture.TAP, gestures.getTapAction());
        mapGesture(Gesture.LONG_TAP, gestures.getLongTapAction());
        mapGesture(Gesture.PINCH, gestures.getPinchAction());
        mapGesture(Gesture.SCROLL_HORIZONTAL, gestures.getHorizontalScrollAction());
        mapGesture(Gesture.SCROLL_VERTICAL, gestures.getVerticalScrollAction());
        mapGesture(Gesture.SWIPE_DOWN, gestures.getswipedownAction());
        mapGesture(Gesture.SWIPE_UP, gestures.getswipeupAction());

        // Apply markers
        setAutoFocusMarker(markers.getAutoFocusMarker());

        // Apply filters
        setFilter(filters.getFilter());

        // Create the orientation helper
        mOrientationHelper = new OrientationHelper(context, mCameraCallbacks);
    }

    private void doInstantiateEngine() {
        LOG.w("doInstantiateEngine:", "instantiating. engine:", mEngine);
        mCameraEngine = instantiateCameraEngine(mEngine, mCameraCallbacks);
        LOG.w("doInstantiateEngine:", "instantiated. engine:", mCameraEngine.getClass().getSimpleName());
        mCameraEngine.setOverlay(mOverlayLayout);
    }

    @VisibleForTesting
    void doInstantiatePreview() {
        LOG.w("doInstantiateEngine:", "instantiating. preview:", mPreview);
        mCameraPreview = instantiatePreview(mPreview, getContext(), this);
        LOG.w("doInstantiateEngine:", "instantiated. preview:", mCameraPreview.getClass().getSimpleName());
        mCameraEngine.setPreview(mCameraPreview);
        if (mPendingFilter != null) {
            setFilter(mPendingFilter);
            mPendingFilter = null;
        }
    }

    @NonNull
    public CameraEngine instantiateCameraEngine(@NonNull Engine engine, @NonNull CameraEngine.Callback callback) {
        if (mExperimental && engine == Engine.CAMERA2 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return new Camera2Engine(callback);
        } else {
            mEngine = Engine.CAMERA1;
            return new Camera1Engine(callback);
        }
    }

    public void getfaces() {

    }

    @NonNull
    protected CameraPreview instantiatePreview(@NonNull Preview preview, @NonNull Context context, @NonNull ViewGroup container) {
        switch (preview) {
            case SURFACE:
                return new SurfaceCameraPreview(context, container);
            case TEXTURE: {
                if (isHardwareAccelerated()) {
                    // TextureView is not supported without hardware acceleration.
                    return new TextureCameraPreview(context, container);
                }
            }
            case GL_SURFACE:
            default: {
                mPreview = Preview.GL_SURFACE;
                return new GlCameraPreview(context, container);
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mInEditor) return;
        if (mCameraPreview == null) {
            doInstantiatePreview();
        }
        mOrientationHelper.enable(getContext());
    }

    @Override
    protected void onDetachedFromWindow() {
        if (!mInEditor) mOrientationHelper.disable();
        super.onDetachedFromWindow();
    }

    private String ms(int mode) {
        switch (mode) {
            case AT_MOST:
                return "AT_MOST";
            case EXACTLY:
                return "EXACTLY";
            case UNSPECIFIED:
                return "UNSPECIFIED";
        }
        return null;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mInEditor) {
            final int width = MeasureSpec.getSize(widthMeasureSpec);
            final int height = MeasureSpec.getSize(heightMeasureSpec);
            super.onMeasure(MeasureSpec.makeMeasureSpec(width, EXACTLY),
                    MeasureSpec.makeMeasureSpec(height, EXACTLY));
            return;
        }

        Size previewSize = mCameraEngine.getPreviewStreamSize(Reference.VIEW);
        if (previewSize == null) {
            LOG.w("onMeasure:", "surface is not ready. Calling default behavior.");
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        // Let's which dimensions need to be adapted.
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        final int widthValue = MeasureSpec.getSize(widthMeasureSpec);
        final int heightValue = MeasureSpec.getSize(heightMeasureSpec);
        final float previewWidth = previewSize.getWidth();
        final float previewHeight = previewSize.getHeight();

        // Pre-process specs
        final ViewGroup.LayoutParams lp = getLayoutParams();
        if (!mCameraPreview.supportsCropping()) {
            // We can't allow EXACTLY constraints in this case.
            if (widthMode == EXACTLY) widthMode = AT_MOST;
            if (heightMode == EXACTLY) heightMode = AT_MOST;
        } else {
            // If MATCH_PARENT is interpreted as AT_MOST, transform to EXACTLY
            // to be consistent with our semantics (and our docs).
            if (widthMode == AT_MOST && lp.width == MATCH_PARENT) widthMode = EXACTLY;
            if (heightMode == AT_MOST && lp.height == MATCH_PARENT) heightMode = EXACTLY;
        }

        LOG.i("onMeasure:", "requested dimensions are", "(" + widthValue + "[" + ms(widthMode) + "]x" +
                heightValue + "[" + ms(heightMode) + "])");
        LOG.i("onMeasure:", "previewSize is", "(" + previewWidth + "x" + previewHeight + ")");

        if (widthMode == EXACTLY && heightMode == EXACTLY) {
            LOG.i("onMeasure:", "both are MATCH_PARENT or fixed value. We adapt.",
                    "This means CROP_CENTER.", "(" + widthValue + "x" + heightValue + ")");
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        if (widthMode == UNSPECIFIED && heightMode == UNSPECIFIED) {
            LOG.i("onMeasure:", "both are completely free.",
                    "We respect that and extend to the whole preview size.",
                    "(" + previewWidth + "x" + previewHeight + ")");
            super.onMeasure(
                    MeasureSpec.makeMeasureSpec((int) previewWidth, EXACTLY),
                    MeasureSpec.makeMeasureSpec((int) previewHeight, EXACTLY));
            return;
        }

        float ratio = previewHeight / previewWidth;
        if (widthMode == UNSPECIFIED || heightMode == UNSPECIFIED) {
            boolean freeWidth = widthMode == UNSPECIFIED;
            int height, width;
            if (freeWidth) {
                height = heightValue;
                width = Math.round(height / ratio);
            } else {
                width = widthValue;
                height = Math.round(width * ratio);
            }
            LOG.i("onMeasure:", "one dimension was free, we adapted it to fit the aspect ratio.",
                    "(" + width + "x" + height + ")");
            super.onMeasure(MeasureSpec.makeMeasureSpec(width, EXACTLY),
                    MeasureSpec.makeMeasureSpec(height, EXACTLY));
            return;
        }

        if (widthMode == EXACTLY || heightMode == EXACTLY) {
            boolean freeWidth = widthMode == AT_MOST;
            int height, width;
            if (freeWidth) {
                height = heightValue;
                width = Math.min(Math.round(height / ratio), widthValue);
            } else {
                width = widthValue;
                height = Math.min(Math.round(width * ratio), heightValue);
            }
            LOG.i("onMeasure:", "one dimension was EXACTLY, another AT_MOST.",
                    "We have TRIED to fit the aspect ratio, but it's not guaranteed.",
                    "(" + width + "x" + height + ")");
            super.onMeasure(MeasureSpec.makeMeasureSpec(width, EXACTLY),
                    MeasureSpec.makeMeasureSpec(height, EXACTLY));
            return;
        }

        int height, width;
        float atMostRatio = (float) heightValue / (float) widthValue;
        if (atMostRatio >= ratio) {
            // We must reduce height.
            width = widthValue;
            height = Math.round(width * ratio);
        } else {
            height = heightValue;
            width = Math.round(height / ratio);
        }
        LOG.i("onMeasure:", "both dimension were AT_MOST.",
                "We fit the preview aspect ratio.",
                "(" + width + "x" + height + ")");
        super.onMeasure(MeasureSpec.makeMeasureSpec(width, EXACTLY),
                MeasureSpec.makeMeasureSpec(height, EXACTLY));
    }

    public boolean mapGesture(@NonNull Gesture gesture, @NonNull GestureAction action) {
        GestureAction none = GestureAction.NONE;
        if (gesture.isAssignableTo(action)) {
            mGestureMap.put(gesture, action);
            switch (gesture) {
                case PINCH:
                    mPinchGestureFinder.setActive(mGestureMap.get(Gesture.PINCH) != none);
                    break;
                case TAP:
                    // case DOUBLE_TAP:
                case LONG_TAP:
                    mTapGestureFinder.setActive(
                            mGestureMap.get(Gesture.TAP) != none ||
                                    // mGestureMap.get(Gesture.DOUBLE_TAP) != none ||
                                    mGestureMap.get(Gesture.LONG_TAP) != none);
                    break;
                /*case SCROLL_HORIZONTAL:
                case SCROLL_VERTICAL:
                    mScrollGestureFinder.setActive(
                            mGestureMap.get(Gesture.SCROLL_HORIZONTAL) != none ||
                                    mGestureMap.get(Gesture.SCROLL_VERTICAL) != none);
                    break;*/
                case SWIPE_DOWN:
                    swipeGesture.setActive(
                            mGestureMap.get(Gesture.SWIPE_DOWN) != none ||
                                    mGestureMap.get(Gesture.SWIPE_UP) != none);
            }
            return true;
        }
        mapGesture(gesture, none);
        return false;
    }

    @NonNull
    public GestureAction getGestureAction(@NonNull Gesture gesture) {
        //noinspection ConstantConditions
        return mGestureMap.get(gesture);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true; // Steal our own events.
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isOpened()) return true;

        // Pass to our own GestureLayouts
        CameraOptions options = mCameraEngine.getCameraOptions(); // Non null
        if (options == null) throw new IllegalStateException("Options should not be null here.");
        if (mPinchGestureFinder.onTouchEvent(event)) {
            LOG.i("onTouchEvent", "pinch!");
            onGesture(mPinchGestureFinder, options);
        } /*else if (mScrollGestureFinder.onTouchEvent(event)) {
            LOG.i("onTouchEvent", "scroll!");
            onGesture(mScrollGestureFinder, options);
        }*/ else if (mTapGestureFinder.onTouchEvent(event)) {
            LOG.i("onTouchEvent", "tap!");
            onGesture(mTapGestureFinder, options);
        } else if (swipeGesture.onTouchEvent(event)) {
            LOG.i("onTouchEvent", "tap!");
            onGesture(swipeGesture, options);
        }

        return true;
    }

    private void onGesture(@NonNull GestureFinder source, @NonNull CameraOptions options) {
        Gesture gesture = source.getGesture();
        GestureAction action = mGestureMap.get(gesture);
        PointF[] points = source.getPoints();
        float oldValue, newValue;
        //noinspection ConstantConditions
        switch (action) {

            case TAKE_PICTURE:
                takePicture();
                break;

            case AUTO_FOCUS:
                mCameraEngine.startAutoFocus(gesture, points[0]);
                break;

            case ZOOM:
                oldValue = mCameraEngine.getZoomValue();
                newValue = source.computeValue(oldValue, 0, 1);
                if (newValue != oldValue) {
                    mCameraEngine.setZoom(newValue, points, true);
                }
                break;

            case EXPOSURE_CORRECTION:
                oldValue = mCameraEngine.getExposureCorrectionValue();
                float minValue = options.getExposureCorrectionMinValue();
                float maxValue = options.getExposureCorrectionMaxValue();
                newValue = source.computeValue(oldValue, minValue, maxValue);
                if (newValue != oldValue) {
                    float[] bounds = new float[]{minValue, maxValue};
                    mCameraEngine.setExposureCorrection(newValue, bounds, points, true);

                }
                break;

            case FILTER_CONTROL_1:
                if (!mExperimental) break;
                if (getFilter() instanceof OneParameterFilter) {
                    OneParameterFilter filter = (OneParameterFilter) getFilter();
                    oldValue = filter.getParameter1();
                    newValue = source.computeValue(oldValue, 0, 1);
                    if (newValue != oldValue) {
                        filter.setParameter1(newValue);
                    }
                }
                break;

            case FILTER_CONTROL_2:
                if (!mExperimental) break;
                if (getFilter() instanceof TwoParameterFilter) {
                    TwoParameterFilter filter = (TwoParameterFilter) getFilter();
                    oldValue = filter.getParameter2();
                    newValue = source.computeValue(oldValue, 0, 1);
                    if (newValue != oldValue) {
                        filter.setParameter2(newValue);
                    }
                }
                break;

            case NONE:
                if (gesture.name().equals("SWIPE_DOWN")) {

                    if (mCameraEngine.getFacing() != Facing.BACK) {
                        mCameraEngine.setFacing(Facing.BACK);
                        Log.d("mn13helloworld", "hell");
                    } else {
                        mCameraEngine.setFacing(Facing.FRONT);
                        Log.d("mn13helloworld1", "hell");
                    }

                } else if (gesture.name().equalsIgnoreCase("SWIPE_UP")) {

                    try {
                        Intent myIntent = new Intent(getContext(), Class.forName("com.TBI.Client.Bluff.Activity.ProfilePage"));
                        myIntent.putExtra("type","top");
                        getContext().startActivity(myIntent);
                        ((Activity)getContext()).overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
                        //overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    /*if (mCameraEngine.getFacing() != Facing.FRONT) {
                        mCameraEngine.setFacing(Facing.FRONT);
                        Log.d("mn13helloworld2","hell");
                    } else {
                        mCameraEngine.setFacing(Facing.BACK);
                        Log.d("mn13helloworld3","hell");
                    }*/
                }
               /* AnimatorSet set;
                set = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.anim.flipping);
                set.setTarget(mCameraEngine);
                set.setDuration(10000);
                set.start();*/
                break;

            case SWIPEDOWN:

                break;


        }
    }

    public boolean isOpened() {
        return mCameraEngine.getEngineState() >= CameraEngine.STATE_STARTED;
    }

    private boolean isClosed() {
        return mCameraEngine.getEngineState() == CameraEngine.STATE_STOPPED;
    }

    public void setLifecycleOwner(@NonNull LifecycleOwner owner) {
        if (mLifecycle != null) mLifecycle.removeObserver(this);
        mLifecycle = owner.getLifecycle();
        mLifecycle.addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void open() {
        if (mInEditor) return;
        if (mCameraPreview != null) mCameraPreview.onResume();
        if (checkPermissions(getAudio())) {
            // Update display orientation for current CameraEngine
            mOrientationHelper.enable(getContext());
            mCameraEngine.getAngles().setDisplayOffset(mOrientationHelper.getDisplayOffset());
            mCameraEngine.start();
        }
    }

    @SuppressWarnings("ConstantConditions")
    @SuppressLint("NewApi")
    protected boolean checkPermissions(@NonNull Audio audio) {
        checkPermissionsManifestOrThrow(audio);
        // Manifest is OK at this point. Let's check runtime permissions.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return true;

        Context c = getContext();
        boolean needsCamera = true;
        boolean needsAudio = audio == Audio.ON || audio == Audio.MONO || audio == Audio.STEREO;

        needsCamera = needsCamera && c.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
        needsAudio = needsAudio && c.checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED;

        if (needsCamera || needsAudio) {
            requestPermissions(needsCamera, needsAudio);
            return false;
        }
        return true;
    }

    private void checkPermissionsManifestOrThrow(@NonNull Audio audio) {
        if (audio == Audio.ON || audio == Audio.MONO || audio == Audio.STEREO) {
            try {
                PackageManager manager = getContext().getPackageManager();
                PackageInfo info = manager.getPackageInfo(getContext().getPackageName(), PackageManager.GET_PERMISSIONS);
                for (String requestedPermission : info.requestedPermissions) {
                    if (requestedPermission.equals(Manifest.permission.RECORD_AUDIO)) {
                        return;
                    }
                }
                String message = LOG.e("Permission error:", "When audio is enabled (Audio.ON),",
                        "the RECORD_AUDIO permission should be added to the app manifest file.");
                throw new IllegalStateException(message);
            } catch (PackageManager.NameNotFoundException e) {
                // Not possible.
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void close() {
        if (mInEditor) return;
        mCameraEngine.stop();
        if (mCameraPreview != null) mCameraPreview.onPause();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void destroy() {
        if (mInEditor) return;
        clearCameraListeners();
        clearFrameProcessors();
        mCameraEngine.destroy();
        if (mCameraPreview != null) mCameraPreview.onDestroy();
    }

    public void setExperimental(boolean experimental) {
        mExperimental = experimental;
    }

    public void set(@NonNull Control control) {
        if (control instanceof Audio) {
            setAudio((Audio) control);
        } else if (control instanceof Facing) {
            setFacing((Facing) control);
        } else if (control instanceof Flash) {
            setFlash((Flash) control);
        } else if (control instanceof Grid) {
            setGrid((Grid) control);
        } else if (control instanceof Hdr) {
            setHdr((Hdr) control);
        } else if (control instanceof Mode) {
            setMode((Mode) control);
        } else if (control instanceof WhiteBalance) {
            setWhiteBalance((WhiteBalance) control);
        } else if (control instanceof VideoCodec) {
            setVideoCodec((VideoCodec) control);
        } else if (control instanceof Preview) {
            setPreview((Preview) control);
        } else if (control instanceof Engine) {
            setEngine((Engine) control);
        }
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public <T extends Control> T get(@NonNull Class<T> controlClass) {
        if (controlClass == Audio.class) {
            return (T) getAudio();
        } else if (controlClass == Facing.class) {
            return (T) getFacing();
        } else if (controlClass == Flash.class) {
            return (T) getFlash();
        } else if (controlClass == Grid.class) {
            return (T) getGrid();
        } else if (controlClass == Hdr.class) {
            return (T) getHdr();
        } else if (controlClass == Mode.class) {
            return (T) getMode();
        } else if (controlClass == WhiteBalance.class) {
            return (T) getWhiteBalance();
        } else if (controlClass == VideoCodec.class) {
            return (T) getVideoCodec();
        } else if (controlClass == Preview.class) {
            return (T) getPreview();
        } else if (controlClass == Engine.class) {
            return (T) getEngine();
        } else {
            throw new IllegalArgumentException("Unknown control class: " + controlClass);
        }
    }

    public void setPreview(@NonNull Preview preview) {
        boolean isNew = preview != mPreview;
        if (isNew) {
            mPreview = preview;
            boolean isAttachedToWindow = getWindowToken() != null;
            if (!isAttachedToWindow && mCameraPreview != null) {
                // Null the preview: will create another when re-attaching.
                mCameraPreview.onDestroy();
                mCameraPreview = null;
            }
        }
    }

    @NonNull
    public Preview getPreview() {
        return mPreview;
    }

    public void setEngine(@NonNull Engine engine) {
        if (!isClosed()) return;
        mEngine = engine;
        CameraEngine oldEngine = mCameraEngine;
        doInstantiateEngine();
        if (mCameraPreview != null) mCameraEngine.setPreview(mCameraPreview);

        // Set again all parameters
        setFacing(oldEngine.getFacing());
        setFlash(oldEngine.getFlash());
        setMode(oldEngine.getMode());
        setWhiteBalance(oldEngine.getWhiteBalance());
        setHdr(oldEngine.getHdr());
        setAudio(oldEngine.getAudio());
        setAudioBitRate(oldEngine.getAudioBitRate());
        setPictureSize(oldEngine.getPictureSizeSelector());
        setVideoSize(oldEngine.getVideoSizeSelector());
        setVideoCodec(oldEngine.getVideoCodec());
        setVideoMaxSize(oldEngine.getVideoMaxSize());
        setVideoMaxDuration(oldEngine.getVideoMaxDuration());
        setVideoBitRate(oldEngine.getVideoBitRate());
        setAutoFocusResetDelay(oldEngine.getAutoFocusResetDelay());
    }

    @NonNull
    public Engine getEngine() {
        return mEngine;
    }

    @Nullable
    public CameraOptions getCameraOptions() {
        return mCameraEngine.getCameraOptions();
    }

    public void setExposureCorrection(float EVvalue) {
        CameraOptions options = getCameraOptions();
        if (options != null) {
            float min = options.getExposureCorrectionMinValue();
            float max = options.getExposureCorrectionMaxValue();
            if (EVvalue < min) EVvalue = min;
            if (EVvalue > max) EVvalue = max;
            float[] bounds = new float[]{min, max};
            mCameraEngine.setExposureCorrection(EVvalue, bounds, null, false);
        }
    }

    public float getExposureCorrection() {
        return mCameraEngine.getExposureCorrectionValue();
    }

    public void setZoom(float zoom) {
        if (zoom < 0) zoom = 0;
        if (zoom > 1) zoom = 1;
        mCameraEngine.setZoom(zoom, null, false);
    }

    public float getZoom() {
        return mCameraEngine.getZoomValue();
    }

    public void setGrid(@NonNull Grid gridMode) {
        mGridLinesLayout.setGridMode(gridMode);
    }

    @NonNull
    public Grid getGrid() {
        return mGridLinesLayout.getGridMode();
    }

    public void setGridColor(@ColorInt int color) {
        mGridLinesLayout.setGridColor(color);
    }

    public int getGridColor() {
        return mGridLinesLayout.getGridColor();
    }

    public void setHdr(@NonNull Hdr hdr) {
        mCameraEngine.setHdr(hdr);
    }

    @NonNull
    public Hdr getHdr() {
        return mCameraEngine.getHdr();
    }

    public void setLocation(double latitude, double longitude) {
        Location location = new Location("Unknown");
        location.setTime(System.currentTimeMillis());
        location.setAltitude(0);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        mCameraEngine.setLocation(location);
    }

    public void setLocation(@Nullable Location location) {
        mCameraEngine.setLocation(location);
    }

    @Nullable
    public Location getLocation() {
        return mCameraEngine.getLocation();
    }

    public void setWhiteBalance(@NonNull WhiteBalance whiteBalance) {
        mCameraEngine.setWhiteBalance(whiteBalance);
    }

    @NonNull
    public WhiteBalance getWhiteBalance() {
        return mCameraEngine.getWhiteBalance();
    }

    public void setFacing(@NonNull Facing facing) {
        mCameraEngine.setFacing(facing);
    }

    @NonNull
    public Facing getFacing() {
        return mCameraEngine.getFacing();
    }

    public Facing toggleFacing() {
        Facing facing = mCameraEngine.getFacing();
        switch (facing) {
            case BACK:
                setFacing(Facing.FRONT);
                break;

            case FRONT:
                setFacing(Facing.BACK);
                break;
        }

        return mCameraEngine.getFacing();
    }

    public void setFlash(@NonNull Flash flash) {
        mCameraEngine.setFlash(flash);
    }

    @NonNull
    public Flash getFlash() {
        return mCameraEngine.getFlash();
    }

    public void setAudio(@NonNull Audio audio) {

        if (audio == getAudio() || isClosed()) {
            mCameraEngine.setAudio(audio);

        } else if (checkPermissions(audio)) {
            mCameraEngine.setAudio(audio);
        } else {
            close();
        }
    }

    @NonNull
    public Audio getAudio() {
        return mCameraEngine.getAudio();
    }

    public void setAutoFocusMarker(@Nullable AutoFocusMarker autoFocusMarker) {
        mAutoFocusMarker = autoFocusMarker;
        mMarkerLayout.onMarker(MarkerLayout.TYPE_AUTOFOCUS, autoFocusMarker);
    }

    public void setAutoFocusResetDelay(long delayMillis) {
        mCameraEngine.setAutoFocusResetDelay(delayMillis);
    }

    @SuppressWarnings("unused")
    public long getAutoFocusResetDelay() {
        return mCameraEngine.getAutoFocusResetDelay();
    }

    public void startAutoFocus(float x, float y) {
        if (x < 0 || x > getWidth())
            throw new IllegalArgumentException("x should be >= 0 and <= getWidth()");
        if (y < 0 || y > getHeight())
            throw new IllegalArgumentException("y should be >= 0 and <= getHeight()");
        mCameraEngine.startAutoFocus(null, new PointF(x, y));
    }

    public void setPreviewStreamSize(@NonNull SizeSelector selector) {
        mCameraEngine.setPreviewStreamSizeSelector(selector);
    }

    public void setMode(@NonNull Mode mode) {
        mCameraEngine.setMode(mode);
    }

    @NonNull
    public Mode getMode() {
        return mCameraEngine.getMode();
    }

    public void setPictureSize(@NonNull SizeSelector selector) {
        mCameraEngine.setPictureSizeSelector(selector);
    }

    public void setPictureMetering(boolean enable) {
        mCameraEngine.setPictureMetering(enable);
    }

    public boolean getPictureMetering() {
        return mCameraEngine.getPictureMetering();
    }

    public void setPictureSnapshotMetering(boolean enable) {
        mCameraEngine.setPictureSnapshotMetering(enable);
    }

    public boolean getPictureSnapshotMetering() {
        return mCameraEngine.getPictureSnapshotMetering();
    }

    public void setVideoSize(@NonNull SizeSelector selector) {
        mCameraEngine.setVideoSizeSelector(selector);
    }

    public void setVideoBitRate(int bitRate) {
        mCameraEngine.setVideoBitRate(bitRate);
    }

    @SuppressWarnings("unused")
    public int getVideoBitRate() {
        return mCameraEngine.getVideoBitRate();
    }

    public void setAudioBitRate(int bitRate) {
        mCameraEngine.setAudioBitRate(bitRate);
    }

    @SuppressWarnings("unused")
    public int getAudioBitRate() {
        return mCameraEngine.getAudioBitRate();
    }

    public void addCameraListener(@NonNull CameraListener cameraListener) {
        mListeners.add(cameraListener);
    }

    public void removeCameraListener(@NonNull CameraListener cameraListener) {
        mListeners.remove(cameraListener);
    }

    public void clearCameraListeners() {
        mListeners.clear();
    }

    public void addFrameProcessor(@Nullable FrameProcessor processor) {
        if (processor != null) {
            mFrameProcessors.add(processor);
            if (mFrameProcessors.size() == 1) {
                mCameraEngine.setHasFrameProcessors(true);
            }
        }
    }

    public void removeFrameProcessor(@Nullable FrameProcessor processor) {
        if (processor != null) {
            mFrameProcessors.remove(processor);
            if (mFrameProcessors.size() == 0) {
                mCameraEngine.setHasFrameProcessors(false);
            }
        }
    }

    public void clearFrameProcessors() {
        boolean had = mFrameProcessors.size() > 0;
        mFrameProcessors.clear();
        if (had) {
            mCameraEngine.setHasFrameProcessors(false);
        }
    }

    public void takePicture() {
        PictureResult.Stub stub = new PictureResult.Stub();
        mCameraEngine.takePicture(stub);
    }

    public void getcamerapreview() {
        PictureResult.Stub stub = new PictureResult.Stub();
        // mCameraEngine.get(stub);
    }

    public void takePictureSnapshot() {
        PictureResult.Stub stub = new PictureResult.Stub();
        mCameraEngine.takePictureSnapshot(stub);
    }

    public void takeVideo(@NonNull File file) {
        VideoResult.Stub stub = new VideoResult.Stub();
        mCameraEngine.takeVideo(stub, file);
        mUiHandler.post(new Runnable() {
            @Override
            public void run() {
                mKeepScreenOn = getKeepScreenOn();
                if (!mKeepScreenOn) setKeepScreenOn(true);
            }
        });
    }

    public void takeVideoSnapshot(@NonNull File file) {
        VideoResult.Stub stub = new VideoResult.Stub();
        mCameraEngine.takeVideoSnapshot(stub, file);
        mUiHandler.post(new Runnable() {
            @Override
            public void run() {
                mKeepScreenOn = getKeepScreenOn();
                if (!mKeepScreenOn) setKeepScreenOn(true);
            }
        });
    }

    public void takeVideo(@NonNull File file, int durationMillis) {
        final int old = getVideoMaxDuration();
        addCameraListener(new CameraListener() {
            @Override
            public void onVideoTaken(@NonNull VideoResult result) {
                setVideoMaxDuration(old);
                removeCameraListener(this);
            }

            @Override
            public void onCameraError(@NonNull CameraException exception) {
                super.onCameraError(exception);
                if (exception.getReason() == CameraException.REASON_VIDEO_FAILED) {
                    setVideoMaxDuration(old);
                    removeCameraListener(this);
                }
            }
        });
        setVideoMaxDuration(durationMillis);
        takeVideo(file);
    }


    public void takeVideoSnapshot(@NonNull File file, int durationMillis) {
        final int old = getVideoMaxDuration();
        addCameraListener(new CameraListener() {
            @Override
            public void onVideoTaken(@NonNull VideoResult result) {
                setVideoMaxDuration(old);
                removeCameraListener(this);
            }

            @Override
            public void onCameraError(@NonNull CameraException exception) {
                super.onCameraError(exception);
                if (exception.getReason() == CameraException.REASON_VIDEO_FAILED) {
                    setVideoMaxDuration(old);
                    removeCameraListener(this);
                }
            }
        });
        setVideoMaxDuration(durationMillis);
        takeVideoSnapshot(file);
    }

    public void stopVideo() {
        mCameraEngine.stopVideo();
        mUiHandler.post(new Runnable() {
            @Override
            public void run() {
                if (getKeepScreenOn() != mKeepScreenOn) setKeepScreenOn(mKeepScreenOn);
            }
        });
    }

    public void setSnapshotMaxWidth(int maxWidth) {
        mCameraEngine.setSnapshotMaxWidth(maxWidth);
    }

    public void setSnapshotMaxHeight(int maxHeight) {
        mCameraEngine.setSnapshotMaxHeight(maxHeight);
    }

    @Nullable
    public Size getSnapshotSize() {
        if (getWidth() == 0 || getHeight() == 0) return null;

        // Get the preview size and crop according to the current view size.
        // It's better to do calculations in the REF_VIEW reference, and then flip if needed.
        Size preview = mCameraEngine.getUncroppedSnapshotSize(Reference.VIEW);
        if (preview == null) return null; // Should never happen.
        AspectRatio viewRatio = AspectRatio.of(getWidth(), getHeight());
        Rect crop = CropHelper.computeCrop(preview, viewRatio);
        Size cropSize = new Size(crop.width(), crop.height());
        if (mCameraEngine.getAngles().flip(Reference.VIEW, Reference.OUTPUT)) {
            return cropSize.flip();
        } else {
            return cropSize;
        }
    }

    @Nullable
    public Size getPictureSize() {
        return mCameraEngine.getPictureSize(Reference.OUTPUT);
    }

    @Nullable
    public Size getVideoSize() {
        return mCameraEngine.getVideoSize(Reference.OUTPUT);
    }

    // If we end up here, we're in M.
    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissions(boolean requestCamera, boolean requestAudio) {
        Activity activity = null;
        Context context = getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                activity = (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }

        List<String> permissions = new ArrayList<>();
        if (requestCamera) permissions.add(Manifest.permission.CAMERA);
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (requestAudio) permissions.add(Manifest.permission.RECORD_AUDIO);
        if (activity != null) {
            activity.requestPermissions(permissions.toArray(new String[0]),
                    PERMISSION_REQUEST_CODE);
        }
    }

    @SuppressLint("NewApi")
    private void playSound(int soundType) {
        if (mPlaySounds) {
            if (mSound == null) mSound = new MediaActionSound();
            mSound.play(soundType);
        }
    }

    public void setPlaySounds(boolean playSounds) {
        mPlaySounds = playSounds && Build.VERSION.SDK_INT >= 16;
        mCameraEngine.setPlaySounds(playSounds);
    }

    public boolean getPlaySounds() {
        return mPlaySounds;
    }

    public void setUseDeviceOrientation(boolean useDeviceOrientation) {
        mUseDeviceOrientation = useDeviceOrientation;
    }

    public boolean getUseDeviceOrientation() {
        return mUseDeviceOrientation;
    }

    public void setVideoCodec(@NonNull VideoCodec codec) {
        mCameraEngine.setVideoCodec(codec);
    }

    @NonNull
    public VideoCodec getVideoCodec() {
        return mCameraEngine.getVideoCodec();
    }

    public void setVideoMaxSize(long videoMaxSizeInBytes) {
        mCameraEngine.setVideoMaxSize(videoMaxSizeInBytes);
    }

    public long getVideoMaxSize() {
        return mCameraEngine.getVideoMaxSize();
    }

    public void setVideoMaxDuration(int videoMaxDurationMillis) {
        mCameraEngine.setVideoMaxDuration(videoMaxDurationMillis);
    }

    public int getVideoMaxDuration() {
        return mCameraEngine.getVideoMaxDuration();
    }

    public boolean isTakingVideo() {
        return mCameraEngine.isTakingVideo();
    }

    public boolean isTakingPicture() {
        return mCameraEngine.isTakingPicture();
    }

    @VisibleForTesting
    class CameraCallbacks implements
            CameraEngine.Callback,
            OrientationHelper.Callback,
            GestureFinder.Controller {

        private CameraLogger mLogger = CameraLogger.create(CameraCallbacks.class.getSimpleName());

        @NonNull
        @Override
        public Context getContext() {
            return CameraView.this.getContext();
        }

        @Override
        public int getWidth() {
            return CameraView.this.getWidth();
        }

        @Override
        public int getHeight() {
            return CameraView.this.getHeight();
        }

        @Override
        public void dispatchOnCameraOpened(final CameraOptions options) {
            mLogger.i("dispatchOnCameraOpened", options);
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    for (CameraListener listener : mListeners) {
                        listener.onCameraOpened(options);
                    }
                }
            });
        }

        @Override
        public void dispatchOnCameraClosed() {
            mLogger.i("dispatchOnCameraClosed");
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    for (CameraListener listener : mListeners) {
                        listener.onCameraClosed();
                    }
                }
            });
        }

        @Override
        public void onCameraPreviewStreamSizeChanged() {
            mLogger.i("onCameraPreviewStreamSizeChanged");
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    requestLayout();
                }
            });
        }

        @Override
        public void onShutter(boolean shouldPlaySound) {
            if (shouldPlaySound && mPlaySounds) {
                playSound(MediaActionSound.SHUTTER_CLICK);
            }
        }

        @Override
        public void dispatchOnPictureTaken(final PictureResult.Stub stub) {
            mLogger.i("dispatchOnPictureTaken", stub);
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    PictureResult result = new PictureResult(stub);
                    for (CameraListener listener : mListeners) {
                        listener.onPictureTaken(result);
                    }
                }
            });
        }

        @Override
        public void dispatchOnVideoTaken(final VideoResult.Stub stub) {
            mLogger.i("dispatchOnVideoTaken", stub);
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    VideoResult result = new VideoResult(stub);
                    for (CameraListener listener : mListeners) {
                        listener.onVideoTaken(result);
                    }
                }
            });
        }

        @Override
        public void dispatchOnFocusStart(@Nullable final Gesture gesture, @NonNull final PointF point) {
            mLogger.i("dispatchOnFocusStart", gesture, point);
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    mMarkerLayout.onEvent(MarkerLayout.TYPE_AUTOFOCUS, new PointF[]{point});
                    if (mAutoFocusMarker != null) {
                        AutoFocusTrigger trigger = gesture != null ?
                                AutoFocusTrigger.GESTURE : AutoFocusTrigger.METHOD;
                        mAutoFocusMarker.onAutoFocusStart(trigger, point);
                    }

                    for (CameraListener listener : mListeners) {
                        listener.onAutoFocusStart(point);
                    }
                }
            });
        }

        @Override
        public void dispatchOnFocusEnd(@Nullable final Gesture gesture, final boolean success,
                                       @NonNull final PointF point) {
            mLogger.i("dispatchOnFocusEnd", gesture, success, point);
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (success && mPlaySounds) {
                        playSound(MediaActionSound.FOCUS_COMPLETE);
                    }

                    if (mAutoFocusMarker != null) {
                        AutoFocusTrigger trigger = gesture != null ?
                                AutoFocusTrigger.GESTURE : AutoFocusTrigger.METHOD;
                        mAutoFocusMarker.onAutoFocusEnd(trigger, success, point);
                    }

                    for (CameraListener listener : mListeners) {
                        listener.onAutoFocusEnd(success, point);
                    }
                }
            });
        }

        @Override
        public void onDeviceOrientationChanged(int deviceOrientation) {
            mLogger.i("onDeviceOrientationChanged", deviceOrientation);
            int displayOffset = mOrientationHelper.getDisplayOffset();
            if (!mUseDeviceOrientation) {
                // To fool the engine to return outputs in the VIEW reference system,
                // The device orientation should be set to -displayOffset.
                int fakeDeviceOrientation = (360 - displayOffset) % 360;
                mCameraEngine.getAngles().setDeviceOrientation(fakeDeviceOrientation);
            } else {
                mCameraEngine.getAngles().setDeviceOrientation(deviceOrientation);
            }
            final int value = (deviceOrientation + displayOffset) % 360;
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    for (CameraListener listener : mListeners) {
                        listener.onOrientationChanged(value);
                    }
                }
            });
        }

        @Override
        public void dispatchOnZoomChanged(final float newValue, @Nullable final PointF[] fingers) {
            mLogger.i("dispatchOnZoomChanged", newValue);
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    for (CameraListener listener : mListeners) {
                        listener.onZoomChanged(newValue, new float[]{0, 1}, fingers);
                    }
                }
            });
        }

        @Override
        public void dispatchOnExposureCorrectionChanged(final float newValue,
                                                        @NonNull final float[] bounds,
                                                        @Nullable final PointF[] fingers) {
            mLogger.i("dispatchOnExposureCorrectionChanged", newValue);
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    for (CameraListener listener : mListeners) {
                        listener.onExposureCorrectionChanged(newValue, bounds, fingers);
                    }
                }
            });
        }

        @Override
        public void dispatchFrame(@NonNull final Frame frame) {
            // The getTime() below might crash if developers incorrectly release frames asynchronously.
            mLogger.v("dispatchFrame:", frame.getTime(), "processors:", mFrameProcessors.size());
            if (mFrameProcessors.isEmpty()) {
                // Mark as released. This instance will be reused.
                frame.release();
            } else {
                // Dispatch this frame to frame processors.
                mFrameProcessorsHandler.run(new Runnable() {
                    @Override
                    public void run() {
                        mLogger.v("dispatchFrame: dispatching", frame.getTime(), "to processors.");
                        for (FrameProcessor processor : mFrameProcessors) {
                            try {
                                processor.process(frame);
                            } catch (Exception e) {
                                // Don't let a single processor crash the processor thread.
                                mLogger.w("Frame processor crashed:", e);
                            }
                        }
                        frame.release();
                    }
                });
            }
        }

        @Override
        public void dispatchError(final CameraException exception) {
            mLogger.i("dispatchError", exception);
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    for (CameraListener listener : mListeners) {
                        listener.onCameraError(exception);
                    }
                }
            });
        }

        @Override
        public void dispatchOnVideoRecordingStart() {
            mLogger.i("dispatchOnVideoRecordingStart");
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    for (CameraListener listener : mListeners) {
                        listener.onVideoRecordingStart();
                    }
                }
            });
        }

        @Override
        public void dispatchOnVideoRecordingEnd() {
            mLogger.i("dispatchOnVideoRecordingEnd");
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    for (CameraListener listener : mListeners) {
                        listener.onVideoRecordingEnd();
                    }
                }
            });
        }
    }

    //endregion

    //region Overlays

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        if (!mInEditor && mOverlayLayout.isOverlay(attributeSet)) {
            return mOverlayLayout.generateLayoutParams(attributeSet);
        }
        return super.generateLayoutParams(attributeSet);
    }

    // We don't support removeView on overlays for now.
    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (!mInEditor && mOverlayLayout.isOverlay(params)) {
            mOverlayLayout.addView(child, params);
        } else {
            super.addView(child, index, params);
        }
    }

    public void setFilter(@NonNull Filter filter) {
        if (mCameraPreview == null) {
            mPendingFilter = filter;
        } else if (!(filter instanceof NoFilter) && !mExperimental) {
            throw new RuntimeException("Filters are an experimental features and need the experimental flag set.");
        } else if (mCameraPreview instanceof FilterCameraPreview) {
            ((FilterCameraPreview) mCameraPreview).setFilter(filter);
        } else {
            throw new RuntimeException("Filters are only supported by the GL_SURFACE preview. Current:" + mPreview);
        }
    }

    @NonNull
    public Filter getFilter() {
        if (!mExperimental) {
            throw new RuntimeException("Filters are an experimental features and need the experimental flag set.");
        } else if (mCameraPreview == null) {
            return mPendingFilter;
        } else if (mCameraPreview instanceof FilterCameraPreview) {
            return ((FilterCameraPreview) mCameraPreview).getCurrentFilter();
        } else {
            throw new RuntimeException("Filters are only supported by the GL_SURFACE preview. Current:" + mPreview);
        }

    }

}
