package com.TBI.captureview;

import android.graphics.PointF;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;

/**
 * TBI Demo
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class CameraListener {

    @UiThread
    public void onCameraOpened(@NonNull CameraOptions options) { }

    @UiThread
    public void onCameraClosed() { }

    @UiThread
    public void onCameraError(@NonNull CameraException exception) { }

    @UiThread
    public void onPictureTaken(@NonNull PictureResult result) { }

    @UiThread
    public void onVideoTaken(@NonNull VideoResult result) { }

    @UiThread
    public void onOrientationChanged(int orientation) { }

    @UiThread
    public void onAutoFocusStart(@NonNull PointF point) { }

    @UiThread
    public void onAutoFocusEnd(boolean successful, @NonNull PointF point) { }

    @UiThread
    public void onZoomChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) { }

    @UiThread
    public void onExposureCorrectionChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) { }

    @UiThread
    public void onVideoRecordingStart() {

    }

    @UiThread
    public void onVideoRecordingEnd() {

    }

}