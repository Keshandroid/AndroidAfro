package com.TBI.captureview.filter;

import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public final class NoFilter extends BaseFilter {

    @NonNull
    @Override
    public String getFragmentShader() {
        return createDefaultFragmentShader();
    }
}
