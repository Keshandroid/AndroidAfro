package com.TBI.captureview.filter;

/**
 * TBI Demo
 */
public interface OneParameterFilter extends Filter {

    void setParameter1(float value);

    float getParameter1();
}
