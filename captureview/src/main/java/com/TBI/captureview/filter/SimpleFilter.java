package com.TBI.captureview.filter;

import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public final class SimpleFilter extends BaseFilter {

    private final String fragmentShader;

    @SuppressWarnings("WeakerAccess")
    public SimpleFilter(@NonNull String fragmentShader) {
        this.fragmentShader = fragmentShader;
    }

    @NonNull
    @Override
    public String getFragmentShader() {
        return fragmentShader;
    }

    @Override
    protected BaseFilter onCopy() {
        return new SimpleFilter(fragmentShader);
    }
}
