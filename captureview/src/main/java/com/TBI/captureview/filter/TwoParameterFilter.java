package com.TBI.captureview.filter;

/**
 * TBI Demo
 */
public interface TwoParameterFilter extends OneParameterFilter {

    void setParameter2(float value);

    float getParameter2();
}
