package com.TBI.captureview.filter;

import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public interface Filter {

    @NonNull
    String getVertexShader();

    @NonNull
    String getFragmentShader();

    void onCreate(int programHandle);

    void onDestroy();

    void draw(float[] transformMatrix);

    void setSize(int width, int height);

    @NonNull
    Filter copy();
}
