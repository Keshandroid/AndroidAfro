package com.TBI.captureview.filter;

import androidx.annotation.NonNull;

import com.TBI.captureview.filters.AutoFixFilter;
import com.TBI.captureview.filters.BW01;
import com.TBI.captureview.filters.BW02;
import com.TBI.captureview.filters.BW03;
import com.TBI.captureview.filters.BlackAndWhiteFilter;
import com.TBI.captureview.filters.BrightnessFilter;
import com.TBI.captureview.filters.ContrastFilter;
import com.TBI.captureview.filters.CrossProcessFilter;
import com.TBI.captureview.filters.DocumentaryFilter;
import com.TBI.captureview.filters.DuotoneFilter;
import com.TBI.captureview.filters.F01;
import com.TBI.captureview.filters.F02;
import com.TBI.captureview.filters.F04;
import com.TBI.captureview.filters.F05;
import com.TBI.captureview.filters.F06;
import com.TBI.captureview.filters.F07;
import com.TBI.captureview.filters.F08;
import com.TBI.captureview.filters.F09;
import com.TBI.captureview.filters.F10;
import com.TBI.captureview.filters.F11;
import com.TBI.captureview.filters.F13;
import com.TBI.captureview.filters.FillLightFilter;
import com.TBI.captureview.filters.GammaFilter;
import com.TBI.captureview.filters.GrainFilter;
import com.TBI.captureview.filters.GrayscaleFilter;
import com.TBI.captureview.filters.HueFilter;
import com.TBI.captureview.filters.InvertColorsFilter;
import com.TBI.captureview.filters.LomoishFilter;
import com.TBI.captureview.filters.PosterizeFilter;
import com.TBI.captureview.filters.SaturationFilter;
import com.TBI.captureview.filters.SepiaFilter;
import com.TBI.captureview.filters.SharpnessFilter;
import com.TBI.captureview.filters.TemperatureFilter;
import com.TBI.captureview.filters.TintFilter;
import com.TBI.captureview.filters.VignetteFilter;

/**
 * TBI Demo
 */
public enum Filters {

    /** @see NoFilter *//*
    NONE(NoFilter.class),

    *//** @see AutoFixFilter *//*
    AUTO_FIX(AutoFixFilter.class),

    *//** @see BlackAndWhiteFilter *//*
    BLACK_AND_WHITE(BlackAndWhiteFilter.class),

    *//** @see BrightnessFilter *//*
    BRIGHTNESS(BrightnessFilter.class),

    *//** @see ContrastFilter *//*
    CONTRAST(ContrastFilter.class),

    *//** @see CrossProcessFilter *//*
    CROSS_PROCESS(CrossProcessFilter.class),

    *//** @see DocumentaryFilter *//*
    DOCUMENTARY(DocumentaryFilter.class),

    *//** @see DuotoneFilter *//*
    DUOTONE(DuotoneFilter.class),

    *//** @see FillLightFilter *//*
    FILL_LIGHT(FillLightFilter.class),

    *//** @see GammaFilter *//*
    GAMMA(GammaFilter.class),

    *//** @see GrainFilter *//*
    GRAIN(GrainFilter.class),

    *//** @see GrayscaleFilter *//*
    GRAYSCALE(GrayscaleFilter.class),

    *//** @see HueFilter *//*
    HUE(HueFilter.class),

    *//** @see InvertColorsFilter *//*
    INVERT_COLORS(InvertColorsFilter.class),

    *//** @see LomoishFilter *//*
    LOMOISH(LomoishFilter.class),

    *//** @see PosterizeFilter *//*
    POSTERIZE(PosterizeFilter.class),

    *//** @see SaturationFilter *//*
    SATURATION(SaturationFilter.class),

    *//** @see SepiaFilter *//*
    SEPIA(SepiaFilter.class),

    *//** @see SharpnessFilter *//*
    SHARPNESS(SharpnessFilter.class),

    *//** @see TemperatureFilter *//*
    TEMPERATURE(TemperatureFilter.class),

    *//** @see TintFilter *//*
    TINT(TintFilter.class),

    *//**
     * @see VignetteFilter
     *//*
    VIGNETTE(VignetteFilter.class);*/
    NONE(NoFilter.class),
    F_01(F01.class),
    F_02(F02.class),
    //F_03(F03.class),
    F_04(F04.class),
    F_05(F05.class),
    F_06(F06.class),
    F_07(F07.class),
    F_08(F08.class),
    F_09(F09.class),
    F_10(F10.class),
    F_11(F11.class),
    F_13(F13.class),
    BW_01(BW01.class),
    BW_02(BW02.class),
    BW_03(BW03.class);

    private Class<? extends Filter> filterClass;

    Filters(@NonNull Class<? extends Filter> filterClass) {
        this.filterClass = filterClass;
    }

    @NonNull
    public Filter newInstance() {
        try {
            return filterClass.newInstance();
        } catch (IllegalAccessException e) {
            return new NoFilter();
        } catch (InstantiationException e) {
            return new NoFilter();
        }
    }
}
