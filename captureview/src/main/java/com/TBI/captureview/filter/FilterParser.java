package com.TBI.captureview.filter;

import android.content.res.TypedArray;

import androidx.annotation.NonNull;

import com.TBI.captureview.R;

/**
 * TBI Demo
 */
public class FilterParser {

    private Filter filter = null;

    public FilterParser(@NonNull TypedArray array) {
        String filterName = array.getString(R.styleable.CameraView_cameraFilter);
        try {
            //noinspection ConstantConditions
            Class<?> filterClass = Class.forName(filterName);
            filter = (Filter) filterClass.newInstance();
        } catch (Exception ignore) {
            filter = new NoFilter();
        }
    }

    @NonNull
    public Filter getFilter() {
        return filter;
    }
}
