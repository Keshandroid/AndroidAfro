package com.TBI.captureview.filters;

import android.opengl.GLES20;

import androidx.annotation.NonNull;

import com.TBI.captureview.filter.BaseFilter;
import com.TBI.captureview.filter.OneParameterFilter;
import com.TBI.captureview.internal.GlUtils;

/**
 * TBI Demo
 */
public class GammaFilter extends BaseFilter implements OneParameterFilter {

    private final static String FRAGMENT_SHADER = "#extension GL_OES_EGL_image_external : require\n"
            + "precision mediump float;\n"
            + "varying vec2 vTextureCoord;\n"
            + "uniform samplerExternalOES sTexture;\n"
            + "uniform float gamma;\n"
            + "void main() {\n"
            + "  vec4 textureColor = texture2D(sTexture, vTextureCoord);\n"
            + "  gl_FragColor = vec4(pow(textureColor.rgb, vec3(gamma)), textureColor.w);\n"
            + "}\n";

    private float gamma = 2.0f;
    private int gammaLocation = -1;

    public GammaFilter() { }

    @SuppressWarnings("WeakerAccess")
    public void setGamma(float gamma) {
        if (gamma < 0.0f) gamma = 0.0f;
        if (gamma > 2.0f) gamma = 2.0f;
        this.gamma = gamma;
    }

    @SuppressWarnings("WeakerAccess")
    public float getGamma() {
        return gamma;
    }

    @Override
    public void setParameter1(float value) {
        setGamma(value * 2F);
    }

    @Override
    public float getParameter1() {
        return getGamma() / 2F;
    }

    @NonNull
    @Override
    public String getFragmentShader() {
        return FRAGMENT_SHADER;
    }

    @Override
    public void onCreate(int programHandle) {
        super.onCreate(programHandle);
        gammaLocation = GLES20.glGetUniformLocation(programHandle, "gamma");
        GlUtils.checkLocation(gammaLocation, "gamma");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gammaLocation = -1;
    }

    @Override
    protected void onPreDraw(float[] transformMatrix) {
        super.onPreDraw(transformMatrix);
        GLES20.glUniform1f(gammaLocation, gamma);
        GlUtils.checkError("glUniform1f");
    }
}