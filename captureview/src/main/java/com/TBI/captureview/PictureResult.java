package com.TBI.captureview;

import android.graphics.BitmapFactory;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.TBI.captureview.controls.Facing;
import com.TBI.captureview.size.Size;

import java.io.File;

/**
 * TBI Demo
 */
@SuppressWarnings("unused")
public class PictureResult {
    public static class Stub {

        Stub() {}

        public boolean isSnapshot;
        public Location location;
        public int rotation;
        public Size size;
        public Facing facing;
        public byte[] data;
        public int format;
    }

    public final static int FORMAT_JPEG = 0;
    // public final static int FORMAT_PNG = 1;

    private final boolean isSnapshot;
    private final Location location;
    private final int rotation;
    private final Size size;
    private final Facing facing;
    private final byte[] data;
    private final int format;

    PictureResult(@NonNull Stub builder) {
        isSnapshot = builder.isSnapshot;
        location = builder.location;
        rotation = builder.rotation;
        size = builder.size;
        facing = builder.facing;
        data = builder.data;
        format = builder.format;
    }

    public boolean isSnapshot() {
        return isSnapshot;
    }

    @Nullable
    public Location getLocation() {
        return location;
    }

    public int getRotation() {
        return rotation;
    }

    @NonNull
    public Size getSize() {
        return size;
    }

    @NonNull
    public Facing getFacing() {
        return facing;
    }

    @NonNull
    public byte[] getData() {
        return data;
    }

    public int getFormat() {
        return format;
    }

    public void toBitmap(int maxWidth, int maxHeight, @NonNull BitmapCallback callback) {
        CameraUtils.decodeBitmap(getData(), maxWidth, maxHeight, new BitmapFactory.Options(), rotation, callback);
    }

    public void toBitmap(@NonNull BitmapCallback callback) {
        toBitmap(-1, -1, callback);
    }

    public void toFile(@NonNull File file, @NonNull FileCallback callback) {
        CameraUtils.writeToFile(getData(), file, callback);
    }
}
