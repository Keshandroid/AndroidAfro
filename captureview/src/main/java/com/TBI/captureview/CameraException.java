package com.TBI.captureview;


public class CameraException extends RuntimeException {

    public static final int REASON_UNKNOWN = 0;
    public static final int REASON_FAILED_TO_CONNECT = 1;
    public static final int REASON_FAILED_TO_START_PREVIEW = 2;
    public static final int REASON_DISCONNECTED = 3;
    public static final int REASON_PICTURE_FAILED = 4;
    public static final int REASON_VIDEO_FAILED = 5;
    public static final int REASON_NO_CAMERA = 6;
    private int reason = REASON_UNKNOWN;

    @SuppressWarnings("WeakerAccess")
    public CameraException(Throwable cause) {
        super(cause);
    }

    public CameraException(Throwable cause, int reason) {
        super(cause);
        this.reason = reason;
    }

    public CameraException(int reason) {
        super();
        this.reason = reason;
    }

    public int getReason() {
        return reason;
    }

    @SuppressWarnings("unused")
    public boolean isUnrecoverable() {
        switch (getReason()) {
            case REASON_FAILED_TO_CONNECT: return true;
            case REASON_FAILED_TO_START_PREVIEW: return true;
            case REASON_DISCONNECTED: return true;
            default: return false;
        }
    }
}
