package com.TBI.captureview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.exifinterface.media.ExifInterface;

import com.TBI.captureview.controls.Facing;
import com.TBI.captureview.engine.mappers.Camera1Mapper;
import com.TBI.captureview.internal.utils.ExifHelper;
import com.TBI.captureview.internal.utils.WorkerHandler;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * TBI Demo
 */
@SuppressWarnings("unused")
public class CameraUtils {

    @SuppressWarnings("WeakerAccess")
    public static boolean hasCameras(@NonNull Context context) {
        PackageManager manager = context.getPackageManager();
        // There's also FEATURE_CAMERA_EXTERNAL , should we support it?
        return manager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
                || manager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);
    }

    public static boolean hasCameraFacing(@SuppressWarnings("unused") @NonNull Context context,
                                          @NonNull Facing facing) {
        int internal = Camera1Mapper.get().mapFacing(facing);
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int i = 0, count = Camera.getNumberOfCameras(); i < count; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == internal) return true;
        }
        return false;
    }

    @SuppressWarnings("WeakerAccess")
    @Nullable
    @WorkerThread
    @SuppressLint("NewApi")
    public static File writeToFile(@NonNull final byte[] data, @NonNull File file) {
        if (file.exists() && !file.delete()) return null;
        try (OutputStream stream = new BufferedOutputStream(new FileOutputStream(file))) {
            stream.write(data);
            stream.flush();
            return file;
        } catch (IOException e) {
            return null;
        }
    }

    @SuppressWarnings("WeakerAccess")
    public static void writeToFile(@NonNull final byte[] data, @NonNull final File file, @NonNull final FileCallback callback) {
        final Handler ui = new Handler();
        WorkerHandler.execute(new Runnable() {
            @Override
            public void run() {
                final File result = writeToFile(data, file);
                ui.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onFileReady(result);
                    }
                });
            }
        });
    }

    @SuppressWarnings("WeakerAccess")
    @Nullable
    @WorkerThread
    public static Bitmap decodeBitmap(@NonNull final byte[] source) {
        return decodeBitmap(source, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    @SuppressWarnings("WeakerAccess")
    public static void decodeBitmap(@NonNull final byte[] source, @NonNull final BitmapCallback callback) {
        decodeBitmap(source, Integer.MAX_VALUE, Integer.MAX_VALUE, callback);
    }

    @SuppressWarnings("WeakerAccess")
    public static void decodeBitmap(@NonNull final byte[] source, final int maxWidth, final int maxHeight, @NonNull final BitmapCallback callback) {
        decodeBitmap(source, maxWidth, maxHeight, new BitmapFactory.Options(), callback);
    }

    @SuppressWarnings("WeakerAccess")
    public static void decodeBitmap(@NonNull final byte[] source, final int maxWidth, final int maxHeight, @NonNull final BitmapFactory.Options options, @NonNull final BitmapCallback callback) {
        decodeBitmap(source, maxWidth, maxHeight, options, -1, callback);
    }

    @SuppressWarnings("WeakerAccess")
    static void decodeBitmap(@NonNull final byte[] source, final int maxWidth, final int maxHeight, @NonNull final BitmapFactory.Options options, final int rotation, @NonNull final BitmapCallback callback) {
        final Handler ui = new Handler();
        WorkerHandler.execute(new Runnable() {
            @Override
            public void run() {
                final Bitmap bitmap = decodeBitmap(source, maxWidth, maxHeight, options, rotation);
                ui.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onBitmapReady(bitmap);
                    }
                });
            }
        });
    }

    @SuppressWarnings("SameParameterValue")
    @Nullable
    @WorkerThread
    public static Bitmap decodeBitmap(@NonNull byte[] source, int maxWidth, int maxHeight) {
        return decodeBitmap(source, maxWidth, maxHeight, new BitmapFactory.Options());
    }

    @SuppressWarnings({"SuspiciousNameCombination", "WeakerAccess"})
    @Nullable
    @WorkerThread
    public static Bitmap decodeBitmap(@NonNull byte[] source, int maxWidth, int maxHeight, @NonNull BitmapFactory.Options options) {
        return decodeBitmap(source, maxWidth, maxHeight, options, -1);
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    @Nullable
    private static Bitmap decodeBitmap(@NonNull byte[] source, int maxWidth, int maxHeight, @NonNull BitmapFactory.Options options, int rotation) {
        if (maxWidth <= 0) maxWidth = Integer.MAX_VALUE;
        if (maxHeight <= 0) maxHeight = Integer.MAX_VALUE;
        int orientation;
        boolean flip;
        if (rotation == -1) {
            InputStream stream = null;
            try {
                // http://sylvana.net/jpegcrop/exif_orientation.html
                stream = new ByteArrayInputStream(source);
                ExifInterface exif = new ExifInterface(stream);
                int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                orientation = ExifHelper.readExifOrientation(exifOrientation);
                flip = exifOrientation == ExifInterface.ORIENTATION_FLIP_HORIZONTAL ||
                        exifOrientation == ExifInterface.ORIENTATION_FLIP_VERTICAL ||
                        exifOrientation == ExifInterface.ORIENTATION_TRANSPOSE ||
                        exifOrientation == ExifInterface.ORIENTATION_TRANSVERSE;

            } catch (IOException e) {
                e.printStackTrace();
                orientation = 0;
                flip = false;
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (Exception ignored) { }
                }
            }
        } else {
            orientation = rotation;
            flip = false;
        }

        Bitmap bitmap;
        try {
            if (maxWidth < Integer.MAX_VALUE || maxHeight < Integer.MAX_VALUE) {
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(source, 0, source.length, options);

                int outHeight = options.outHeight;
                int outWidth = options.outWidth;
                if (orientation % 180 != 0) {
                    //noinspection SuspiciousNameCombination
                    outHeight = options.outWidth;
                    //noinspection SuspiciousNameCombination
                    outWidth = options.outHeight;
                }

                options.inSampleSize = computeSampleSize(outWidth, outHeight, maxWidth, maxHeight);
                options.inJustDecodeBounds = false;
                bitmap = BitmapFactory.decodeByteArray(source, 0, source.length, options);
            } else {
                bitmap = BitmapFactory.decodeByteArray(source, 0, source.length);
            }

            if (orientation != 0 || flip) {
                Matrix matrix = new Matrix();
                matrix.setRotate(orientation);
                Bitmap temp = bitmap;
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                temp.recycle();
            }
        } catch (OutOfMemoryError e) {
            bitmap = null;
        }
        return bitmap;
    }

    private static int computeSampleSize(int width, int height, int maxWidth, int maxHeight) {
        // https://developer.android.com/topic/performance/graphics/load-bitmap.html
        int inSampleSize = 1;
        if (height > maxHeight || width > maxWidth) {
            while ((height / inSampleSize) >= maxHeight
                    || (width / inSampleSize) >= maxWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }


}
