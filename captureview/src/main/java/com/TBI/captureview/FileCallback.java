package com.TBI.captureview;

import java.io.File;

import androidx.annotation.Nullable;
import androidx.annotation.UiThread;

/**
 * TBI Demo
 */
public interface FileCallback {

    @UiThread
    void onFileReady(@Nullable File file);
}
