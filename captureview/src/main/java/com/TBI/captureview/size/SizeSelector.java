package com.TBI.captureview.size;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * TBI Demo
 */
public interface SizeSelector {

    @NonNull
    List<Size> select(@NonNull List<Size> source);
}
