package com.TBI.captureview.size;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * TBI Demo
 */
public class SizeSelectors {

    public interface Filter {
        boolean accepts(@NonNull Size size);
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    public static SizeSelector withFilter(@NonNull Filter filter) {
        return new FilterSelector(filter);
    }

    @NonNull
    public static SizeSelector maxWidth(final int width) {
        return withFilter(new Filter() {
            @Override
            public boolean accepts(@NonNull Size size) {
                return size.getWidth() <= width;
            }
        });
    }

    @NonNull
    public static SizeSelector minWidth(final int width) {
        return withFilter(new Filter() {
            @Override
            public boolean accepts(@NonNull Size size) {
                return size.getWidth() >= width;
            }
        });
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    public static SizeSelector maxHeight(final int height) {
        return withFilter(new Filter() {
            @Override
            public boolean accepts(@NonNull Size size) {
                return size.getHeight() <= height;
            }
        });
    }

    @NonNull
    public static SizeSelector minHeight(final int height) {
        return withFilter(new Filter() {
            @Override
            public boolean accepts(@NonNull Size size) {
                return size.getHeight() >= height;
            }
        });
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    public static SizeSelector aspectRatio(AspectRatio ratio, final float delta) {
        final float desired = ratio.toFloat();
        return withFilter(new Filter() {
            @Override
            public boolean accepts(@NonNull Size size) {
                float candidate = AspectRatio.of(size.getWidth(), size.getHeight()).toFloat();
                return candidate >= desired - delta && candidate <= desired + delta;
            }
        });
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    public static SizeSelector biggest() {
        return new SizeSelector() {
            @NonNull
            @Override
            public List<Size> select(@NonNull List<Size> source) {
                Collections.sort(source);
                Collections.reverse(source);
                return source;
            }
        };
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    public static SizeSelector smallest() {
        return new SizeSelector() {
            @NonNull
            @Override
            public List<Size> select(@NonNull List<Size> source) {
                Collections.sort(source);
                return source;
            }
        };
    }

    @NonNull
    @SuppressWarnings("WeakerAccess")
    public static SizeSelector maxArea(final int area) {
        return withFilter(new Filter() {
            @Override
            public boolean accepts(@NonNull Size size) {
                return size.getHeight() * size.getWidth() <= area;
            }
        });
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    public static SizeSelector minArea(final int area) {
        return withFilter(new Filter() {
            @Override
            public boolean accepts(@NonNull Size size) {
                return size.getHeight() * size.getWidth() >= area;
            }
        });
    }

    @NonNull
    public static SizeSelector and(SizeSelector... selectors) {
        return new AndSelector(selectors);
    }

    @NonNull
    public static SizeSelector or(SizeSelector... selectors) {
        return new OrSelector(selectors);
    }

    private static class FilterSelector implements SizeSelector {

        private Filter constraint;

        private FilterSelector(@NonNull Filter constraint) {
            this.constraint = constraint;
        }

        @Override
        @NonNull
        public List<Size> select(@NonNull List<Size> source) {
            List<Size> sizes = new ArrayList<>();
            for (Size size : source) {
                if (constraint.accepts(size)) {
                    sizes.add(size);
                }
            }
            return sizes;
        }
    }

    private static class AndSelector implements SizeSelector {

        private SizeSelector[] values;

        private AndSelector(@NonNull SizeSelector... values) {
            this.values = values;
        }

        @Override
        @NonNull
        public List<Size> select(@NonNull List<Size> source) {
            List<Size> temp = source;
            for (SizeSelector selector : values) {
                temp = selector.select(temp);
            }
            return temp;
        }
    }

    private static class OrSelector implements SizeSelector {

        private SizeSelector[] values;

        private OrSelector(@NonNull SizeSelector... values) {
            this.values = values;
        }

        @Override
        @NonNull
        public List<Size> select(@NonNull List<Size> source) {
            List<Size> temp = null;
            for (SizeSelector selector : values) {
                temp = selector.select(source);
                if (!temp.isEmpty()) {
                    break;
                }
            }
            return temp == null ? new ArrayList<Size>() : temp;
        }

    }
}
