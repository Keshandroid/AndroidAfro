package com.TBI.captureview.markers;

import android.graphics.PointF;

import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public interface AutoFocusMarker extends Marker {
    void onAutoFocusStart(@NonNull AutoFocusTrigger trigger, @NonNull PointF point);
    void onAutoFocusEnd(@NonNull AutoFocusTrigger trigger, boolean successful, @NonNull PointF point);
}
