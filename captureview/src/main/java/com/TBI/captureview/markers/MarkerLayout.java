package com.TBI.captureview.markers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PointF;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

/**
 * TBI Demo
 */
public class MarkerLayout extends FrameLayout {

    public final static int TYPE_AUTOFOCUS = 1;

    @SuppressLint("UseSparseArrays")
    private final HashMap<Integer, View> mViews = new HashMap<>();

    public MarkerLayout(@NonNull Context context) {
        super(context);
    }

    public void onMarker(int type, @Nullable Marker marker) {

        View oldView = mViews.get(type);
        if (oldView != null) removeView(oldView);

        if (marker == null) return;

        View newView = marker.onAttach(getContext(), this);
        if (newView != null) {
            mViews.put(type, newView);
            addView(newView);
        }
    }

    public void onEvent(int type, @NonNull PointF[] points) {
        View view = mViews.get(type);
        if (view == null) return;
        view.clearAnimation();
        if (type == TYPE_AUTOFOCUS) {

            PointF point = points[0];
            float x = (int) (point.x - view.getWidth() / 2);
            float y = (int) (point.y - view.getHeight() / 2);
            view.setTranslationX(x);
            view.setTranslationY(y);
        }
    }
}
