package com.TBI.captureview.markers;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * TBI Demo
 */
public interface Marker {

    @Nullable
    View onAttach(@NonNull Context context, @NonNull ViewGroup container);
}
