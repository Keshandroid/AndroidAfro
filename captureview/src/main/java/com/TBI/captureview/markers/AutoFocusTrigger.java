package com.TBI.captureview.markers;

/**
 * TBI Demo
 */
public enum AutoFocusTrigger {

    GESTURE,
    METHOD
}
