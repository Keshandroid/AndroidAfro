package com.TBI.captureview.engine.offset;

/**
 * TBI Demo
 */
public enum Reference {

    BASE,
    SENSOR,
    VIEW,
    OUTPUT
}
