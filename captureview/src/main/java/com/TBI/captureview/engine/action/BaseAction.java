package com.TBI.captureview.engine.action;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.os.Build;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;

/**
 * TBI Demo
 */
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
public abstract class BaseAction implements Action {

    private final List<ActionCallback> callbacks = new ArrayList<>();
    private int state;
    private ActionHolder holder;

    @Override
    public final int getState() {
        return state;
    }

    @Override
    public final void start(@NonNull ActionHolder holder) {
        holder.addAction(this);
        onStart(holder);
    }

    @Override
    public final void abort(@NonNull ActionHolder holder) {
        holder.removeAction(this);
        if (!isCompleted()) {
            onAbort(holder);
            setState(STATE_COMPLETED);
        }
    }

    @CallSuper
    protected void onStart(@NonNull ActionHolder holder) {
        this.holder = holder; // must be here
        // Overrideable
    }

    @SuppressWarnings("unused")
    protected void onAbort(@NonNull ActionHolder holder) {
        // Overrideable
    }

    @Override
    public void onCaptureStarted(@NonNull ActionHolder holder, @NonNull CaptureRequest request) {
        // Overrideable
    }

    @Override
    public void onCaptureProgressed(@NonNull ActionHolder holder, @NonNull CaptureRequest request, @NonNull CaptureResult result) {
        // Overrideable
    }

    @Override
    public void onCaptureCompleted(@NonNull ActionHolder holder, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
        // Overrideable
    }

    protected void setState(int newState) {
        if (newState != state) {
            state = newState;
            for (ActionCallback callback : callbacks) {
                callback.onActionStateChanged(this, state);
            }
            if (state == STATE_COMPLETED) {
                holder.removeAction(this);
                onCompleted(holder);
            }
        }
    }

    public boolean isCompleted() {
        return state == STATE_COMPLETED;
    }

    protected void onCompleted(@NonNull ActionHolder holder) {
        // Overrideable
    }

    @NonNull
    protected ActionHolder getHolder() {
        return holder;
    }

    @NonNull
    protected <T> T readCharacteristic(@NonNull CameraCharacteristics.Key<T> key,
                                       @NonNull T fallback) {
        T value = holder.getCharacteristics(this).get(key);
        return value == null ? fallback : value;
    }

    @Override
    public void addCallback(@NonNull ActionCallback callback) {
        if (!callbacks.contains(callback)) {
            callbacks.add(callback);
            callback.onActionStateChanged(this, getState());
        }
    }

    @Override
    public void removeCallback(@NonNull ActionCallback callback) {
        callbacks.remove(callback);
    }
}
