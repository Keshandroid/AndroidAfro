package com.TBI.captureview.engine.offset;

/**
 * TBI Demo
 */
public enum Axis {

    ABSOLUTE,
    RELATIVE_TO_SENSOR
}
