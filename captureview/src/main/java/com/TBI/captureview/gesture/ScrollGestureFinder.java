package com.TBI.captureview.gesture;

import androidx.annotation.NonNull;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.TBI.captureview.CameraLogger;
/**
 * TBI Demo
 */
public class ScrollGestureFinder extends GestureFinder {

    private static final String TAG = ScrollGestureFinder.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    private GestureDetector mDetector;
    private boolean mNotify;
    private float mFactor;

    public ScrollGestureFinder(final @NonNull Controller controller) {
        super(controller, 2);
        mDetector = new GestureDetector(controller.getContext(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                boolean horizontal;
                LOG.i("onScroll:", "distanceX="+distanceX, "distanceY="+distanceY);
                if (e1 == null || e2 == null) return false; // Got some crashes about this.
                if (e1.getX() != getPoint(0).x || e1.getY() != getPoint(0).y) {
                    horizontal = Math.abs(distanceX) >= Math.abs(distanceY);
                    setGesture(horizontal ? Gesture.SCROLL_HORIZONTAL : Gesture.SCROLL_VERTICAL);
                    getPoint(0).set(e1.getX(), e1.getY());
                } else {
                    horizontal = getGesture() == Gesture.SCROLL_HORIZONTAL;
                }
                getPoint(1).set(e2.getX(), e2.getY());
                mFactor = horizontal ? (distanceX / controller.getWidth()) : (distanceY / controller.getHeight());
                mFactor = horizontal ? -mFactor : mFactor; // When vertical, up = positive
                mNotify = true;
                return true;
            }
        });

        mDetector.setIsLongpressEnabled(false);
    }

    @Override
    protected boolean handleTouchEvent(@NonNull MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mNotify = false;
        }

        mDetector.onTouchEvent(event);

        if (mNotify) LOG.i("Notifying a gesture of type", getGesture().name());
        return mNotify;
    }

    @Override
    public float getValue(float currValue, float minValue, float maxValue) {
        float delta = getFactor();

        delta *= (maxValue - minValue);
        delta *= 2;

        return currValue + delta;
    }

    protected float getFactor() {
        return mFactor;
    }
}
