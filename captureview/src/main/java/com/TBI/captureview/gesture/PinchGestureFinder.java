package com.TBI.captureview.gesture;

import android.os.Build;
import androidx.annotation.NonNull;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

/**
 * TBI Demo
 */
public class PinchGestureFinder extends GestureFinder {

    private final static float ADD_SENSITIVITY = 2f;

    private ScaleGestureDetector mDetector;
    private boolean mNotify;
    private float mFactor = 0;

    public PinchGestureFinder(@NonNull Controller controller) {
        super(controller, 2);
        setGesture(Gesture.PINCH);
        mDetector = new ScaleGestureDetector(controller.getContext(), new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                mNotify = true;
                mFactor = ((detector.getScaleFactor() - 1) * ADD_SENSITIVITY);
                return true;
            }
        });

        if (Build.VERSION.SDK_INT >= 19) {
            mDetector.setQuickScaleEnabled(false);
        }
    }

    @Override
    protected boolean handleTouchEvent(@NonNull MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mNotify = false;
        }

        mDetector.onTouchEvent(event);

        if (mNotify) {
            getPoint(0).x = event.getX(0);
            getPoint(0).y = event.getY(0);
            if (event.getPointerCount() > 1) {
                getPoint(1).x = event.getX(1);
                getPoint(1).y = event.getY(1);
            }
            return true;
        }
        return false;
    }

    @Override
    public float getValue(float currValue, float minValue, float maxValue) {
        float add = getFactor();

        add *= (maxValue - minValue);

        return currValue + add;
    }

    protected float getFactor() {
        return mFactor;
    }
}
