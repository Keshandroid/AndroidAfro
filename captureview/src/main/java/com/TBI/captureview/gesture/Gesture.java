package com.TBI.captureview.gesture;


import androidx.annotation.NonNull;


/**
 * TBI Demo
 */
public enum Gesture {

    PINCH(GestureType.CONTINUOUS),
    TAP(GestureType.ONE_SHOT),
    LONG_TAP(GestureType.ONE_SHOT),
    SCROLL_HORIZONTAL(GestureType.CONTINUOUS),
    SCROLL_VERTICAL(GestureType.CONTINUOUS),
    SWIPE_DOWN(GestureType.ONE_SHOT),
    SWIPE_UP(GestureType.ONE_SHOT);
    //SWIPE_DOWN(GestureType.ONE_SHOT);

    Gesture(@NonNull GestureType type) {
        this.type = type;
    }

    private GestureType type;

    public boolean isAssignableTo(@NonNull GestureAction action) {
        return action == GestureAction.NONE || action.type() == type;
    }

}
