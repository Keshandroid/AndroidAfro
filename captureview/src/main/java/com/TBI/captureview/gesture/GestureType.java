package com.TBI.captureview.gesture;


/**
 * TBI Demo
 */
public enum GestureType {

    ONE_SHOT,
    CONTINUOUS
}
