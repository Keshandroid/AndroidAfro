package com.TBI.captureview.gesture;

import android.view.GestureDetector;
import android.view.MotionEvent;

import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public class TapGestureFinder extends GestureFinder {

    private GestureDetector mDetector;
    private boolean mNotify;

    public TapGestureFinder(@NonNull Controller controller) {
        super(controller, 1);
        mDetector = new GestureDetector(controller.getContext(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                mNotify = true;
                setGesture(Gesture.TAP);
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                mNotify = true;
                setGesture(Gesture.LONG_TAP);
            }
        });

        mDetector.setIsLongpressEnabled(true);
    }

    @Override
    protected boolean handleTouchEvent(@NonNull MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mNotify = false;
        }

        mDetector.onTouchEvent(event);

        if (mNotify) {
            getPoint(0).x = event.getX();
            getPoint(0).y = event.getY();
            return true;
        }
        return false;
    }

    @Override
    public float getValue(float currValue, float minValue, float maxValue) {
        return 0;
    }

}
