package com.TBI.captureview.gesture;

import android.content.Context;
import android.graphics.PointF;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import android.view.MotionEvent;

/**
 * TBI Demo
 */
public abstract class GestureFinder {

    public interface Controller {
        @NonNull Context getContext();
        int getWidth();
        int getHeight();
    }

    private final static int GRANULARITY = 50;

    private boolean mActive;
    @VisibleForTesting Gesture mType;
    private PointF[] mPoints;
    private Controller mController;

    GestureFinder(@NonNull Controller controller, int points) {
        mController = controller;
        mPoints = new PointF[points];
        for (int i = 0; i < points; i++) {
            mPoints[i] = new PointF(0, 0);
        }
    }

    public void setActive(boolean active) {
        mActive = active;
    }

    public boolean isActive() {
        return mActive;
    }

    public final boolean onTouchEvent(@NonNull MotionEvent event) {
        if (!mActive) return false;
        return handleTouchEvent(event);
    }

    protected abstract boolean handleTouchEvent(@NonNull MotionEvent event);

    @NonNull
    public final Gesture getGesture() {
        return mType;
    }

    protected final void setGesture(Gesture gesture) {
        mType = gesture;
    }

    @NonNull
    public final PointF[] getPoints() {
        return mPoints;
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    protected final PointF getPoint(int which) {
        return mPoints[which];
    }

    public final float computeValue(float currValue, float minValue, float maxValue) {
        return capValue(currValue, getValue(currValue, minValue, maxValue), minValue, maxValue);
    }

    protected abstract float getValue(float currValue, float minValue, float maxValue);

    private static float capValue(float oldValue, float newValue, float minValue, float maxValue) {
        if (newValue < minValue) newValue = minValue;
        if (newValue > maxValue) newValue = maxValue;

        float distance = (maxValue - minValue) / (float) GRANULARITY;
        float half = distance / 2;
        if (newValue >= oldValue - half && newValue <= oldValue + half) {
            // Too close! Return the oldValue.
            return oldValue;
        }
        return newValue;
    }

    @NonNull
    protected Controller getController() {
        return mController;
    }
}
