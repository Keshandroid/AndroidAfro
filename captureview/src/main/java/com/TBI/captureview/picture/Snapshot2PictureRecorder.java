package com.TBI.captureview.picture;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.PictureResult;
import com.TBI.captureview.engine.Camera2Engine;
import com.TBI.captureview.engine.action.Action;
import com.TBI.captureview.engine.action.ActionHolder;
import com.TBI.captureview.engine.action.Actions;
import com.TBI.captureview.engine.action.BaseAction;
import com.TBI.captureview.engine.action.CompletionCallback;
import com.TBI.captureview.engine.lock.LockAction;
import com.TBI.captureview.preview.GlCameraPreview;
import com.TBI.captureview.size.AspectRatio;

/**
 * TBI Demo
 */
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
public class Snapshot2PictureRecorder extends SnapshotGlPictureRecorder {

    private final static String TAG = Snapshot2PictureRecorder.class.getSimpleName();
    private final static CameraLogger LOG = CameraLogger.create(TAG);
    private final static long LOCK_TIMEOUT = 2500;

    private static class FlashAction extends BaseAction {

        @Override
        protected void onStart(@NonNull ActionHolder holder) {
            super.onStart(holder);
            LOG.i("FlashAction:", "Parameters locked, opening torch.");
            holder.getBuilder(this).set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH);
            holder.getBuilder(this).set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
            holder.applyBuilder(this);
        }

        @Override
        public void onCaptureCompleted(@NonNull ActionHolder holder,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            super.onCaptureCompleted(holder, request, result);
            Integer flashState = result.get(CaptureResult.FLASH_STATE);
            if (flashState == null) {
                LOG.w("FlashAction:", "Waiting flash, but flashState is null! Taking snapshot.");
                setState(STATE_COMPLETED);
            } else if (flashState == CaptureResult.FLASH_STATE_FIRED) {
                LOG.i("FlashAction:", "Waiting flash and we have FIRED state! Taking snapshot.");
                setState(STATE_COMPLETED);
            } else {
                LOG.i("FlashAction:", "Waiting flash but flashState is",
                        flashState, ". Waiting...");
            }
        }
    }

    private final Action mAction;
    private final ActionHolder mHolder;
    private final boolean mActionNeeded;
    private Integer mOriginalAeMode;
    private Integer mOriginalFlashMode;

    public Snapshot2PictureRecorder(@NonNull PictureResult.Stub stub,
                                    @NonNull Camera2Engine engine,
                                    @NonNull GlCameraPreview preview,
                                    @NonNull AspectRatio outputRatio) {
        super(stub, engine, preview, outputRatio);
        mHolder = engine;

        mAction = Actions.sequence(
                Actions.timeout(LOCK_TIMEOUT, new LockAction()),
                new FlashAction());
        mAction.addCallback(new CompletionCallback() {
            @Override
            protected void onActionCompleted(@NonNull Action action) {
                LOG.i("Taking picture with super.take().");
                Snapshot2PictureRecorder.super.take();
            }
        });

        Integer aeState = mHolder.getLastResult(mAction).get(CaptureResult.CONTROL_AE_STATE);
        mActionNeeded = engine.getPictureSnapshotMetering()
                && aeState != null
                && aeState == CaptureResult.CONTROL_AE_STATE_FLASH_REQUIRED;
        mOriginalAeMode = mHolder.getBuilder(mAction).get(CaptureRequest.CONTROL_AE_MODE);
        mOriginalFlashMode = mHolder.getBuilder(mAction).get(CaptureRequest.FLASH_MODE);
    }

    @Override
    public void take() {
        if (!mActionNeeded) {
            LOG.i("take:", "Engine does no metering or needs no flash, taking fast snapshot.");
            super.take();
        } else {
            LOG.i("take:", "Engine needs flash. Starting action");
            mAction.start(mHolder);
        }
    }

    @Override
    protected void dispatchResult() {
        // Revert our changes.
        LOG.i("dispatchResult:", "Reverting the flash changes.");
        try {
            CaptureRequest.Builder builder = mHolder.getBuilder(mAction);
            builder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
            builder.set(CaptureRequest.FLASH_MODE, CaptureResult.FLASH_MODE_OFF);
            mHolder.applyBuilder(mAction, builder);
            builder.set(CaptureRequest.CONTROL_AE_MODE, mOriginalAeMode);
            builder.set(CaptureRequest.FLASH_MODE, mOriginalFlashMode);
            mHolder.applyBuilder(mAction);
        } catch (CameraAccessException ignore) {}
        super.dispatchResult();
    }
}
