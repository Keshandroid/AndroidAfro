package com.TBI.captureview.picture;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.TBI.captureview.PictureResult;

/**
 * TBI Demo
 */
public abstract class PictureRecorder {

    public interface PictureResultListener {
        void onPictureShutter(boolean didPlaySound);
        void onPictureResult(@Nullable PictureResult.Stub result, @Nullable Exception error);
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED) PictureResult.Stub mResult;
    @VisibleForTesting PictureResultListener mListener;
    @SuppressWarnings("WeakerAccess")
    protected Exception mError;

    @SuppressWarnings("WeakerAccess")
    public PictureRecorder(@NonNull PictureResult.Stub stub, @Nullable PictureResultListener listener) {
        mResult = stub;
        mListener = listener;
    }

    public abstract void take();

    @SuppressWarnings("WeakerAccess")
    protected void dispatchOnShutter(boolean didPlaySound) {
        if (mListener != null) mListener.onPictureShutter(didPlaySound);
    }

    protected void dispatchResult() {
        if (mListener != null) {
            mListener.onPictureResult(mResult, mError);
            mListener = null;
            mResult = null;
        }
    }
}
