package com.TBI.captureview.picture;

import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;

import androidx.annotation.NonNull;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.PictureResult;
import com.TBI.captureview.engine.Camera1Engine;
import com.TBI.captureview.engine.offset.Reference;
import com.TBI.captureview.internal.utils.CropHelper;
import com.TBI.captureview.internal.utils.RotationHelper;
import com.TBI.captureview.internal.utils.WorkerHandler;
import com.TBI.captureview.size.AspectRatio;
import com.TBI.captureview.size.Size;

import java.io.ByteArrayOutputStream;

/**
 * TBI Demo
 */
public class Snapshot1PictureRecorder extends PictureRecorder {

    private static final String TAG = Snapshot1PictureRecorder.class.getSimpleName();
    @SuppressWarnings("unused")
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    private Camera1Engine mEngine1;
    private Camera mCamera;
    private AspectRatio mOutputRatio;
    private int mFormat;

    public Snapshot1PictureRecorder(
            @NonNull PictureResult.Stub stub,
            @NonNull Camera1Engine engine,
            @NonNull Camera camera,
            @NonNull AspectRatio outputRatio) {
        super(stub, engine);
        mEngine1 = engine;
        mCamera = camera;
        mOutputRatio = outputRatio;
        mFormat = camera.getParameters().getPreviewFormat();
    }

    @Override
    public void take() {
        mCamera.setOneShotPreviewCallback(new Camera.PreviewCallback() {
            @Override
            public void onPreviewFrame(@NonNull final byte[] yuv, Camera camera) {
                dispatchOnShutter(false);
                final int sensorToOutput = mResult.rotation;
                final Size outputSize = mResult.size;
                final Size previewStreamSize = mEngine1.getPreviewStreamSize(Reference.SENSOR);
                if (previewStreamSize == null) {
                    throw new IllegalStateException("Preview stream size should never be null here.");
                }
                WorkerHandler.execute(new Runnable() {
                    @Override
                    public void run() {

                        //noinspection deprecation
                        byte[] data = RotationHelper.rotate(yuv, previewStreamSize, sensorToOutput);
                        YuvImage yuv = new YuvImage(data, mFormat, outputSize.getWidth(), outputSize.getHeight(), null);

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        Rect outputRect = CropHelper.computeCrop(outputSize, mOutputRatio);
                        yuv.compressToJpeg(outputRect, 90, stream);
                        data = stream.toByteArray();

                        mResult.data = data;
                        mResult.size = new Size(outputRect.width(), outputRect.height());
                        mResult.rotation = 0;
                        mResult.format = PictureResult.FORMAT_JPEG;
                        dispatchResult();
                    }
                });

                // It seems that the buffers are already cleared here, so we need to allocate again.
                camera.setPreviewCallbackWithBuffer(null); // Release anything left
                camera.setPreviewCallbackWithBuffer(mEngine1); // Add ourselves
                mEngine1.getFrameManager().setUp(mFormat, previewStreamSize);
            }
        });
    }

    @Override
    protected void dispatchResult() {
        mEngine1 = null;
        mCamera = null;
        mOutputRatio = null;
        mFormat = 0;
        super.dispatchResult();
    }
}
