package com.TBI.captureview.picture;

import android.hardware.Camera;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.exifinterface.media.ExifInterface;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.PictureResult;
import com.TBI.captureview.internal.utils.ExifHelper;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * TBI Demo
 */
public class Full1PictureRecorder extends PictureRecorder {

    private static final String TAG = Full1PictureRecorder.class.getSimpleName();
    @SuppressWarnings("unused")
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    private Camera mCamera;

    public Full1PictureRecorder(@NonNull PictureResult.Stub stub,
                                @Nullable PictureResultListener listener,
                                @NonNull Camera camera) {
        super(stub, listener);
        mCamera = camera;

        Camera.Parameters params = mCamera.getParameters();
        params.setRotation(mResult.rotation);
        mCamera.setParameters(params);
    }

    @Override
    public void take() {
        mCamera.takePicture(
                new Camera.ShutterCallback() {
                    @Override
                    public void onShutter() {
                        dispatchOnShutter(true);
                    }
                },
                null,
                null,
                new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] data, final Camera camera) {
                        int exifRotation;
                        try {
                            ExifInterface exif = new ExifInterface(new ByteArrayInputStream(data));
                            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                            exifRotation = ExifHelper.readExifOrientation(exifOrientation);
                        } catch (IOException e) {
                            exifRotation = 0;
                        }
                        mResult.format = PictureResult.FORMAT_JPEG;
                        mResult.data = data;
                        mResult.rotation = exifRotation;
                        camera.startPreview(); // This is needed, read somewhere in the docs.
                        dispatchResult();
                    }
                }
        );
    }

    @Override
    protected void dispatchResult() {
        mCamera = null;
        super.dispatchResult();
    }
}
