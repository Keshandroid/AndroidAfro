package com.TBI.captureview.picture;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLContext;
import android.opengl.Matrix;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.PictureResult;
import com.TBI.captureview.controls.Facing;
import com.TBI.captureview.engine.CameraEngine;
import com.TBI.captureview.engine.offset.Axis;
import com.TBI.captureview.engine.offset.Reference;
import com.TBI.captureview.filter.Filter;
import com.TBI.captureview.internal.egl.EglBaseSurface;
import com.TBI.captureview.internal.egl.EglCore;
import com.TBI.captureview.internal.egl.EglViewport;
import com.TBI.captureview.internal.egl.EglWindowSurface;
import com.TBI.captureview.internal.utils.CropHelper;
import com.TBI.captureview.internal.utils.WorkerHandler;
import com.TBI.captureview.overlay.Overlay;
import com.TBI.captureview.overlay.OverlayDrawer;
import com.TBI.captureview.preview.GlCameraPreview;
import com.TBI.captureview.preview.RendererFrameCallback;
import com.TBI.captureview.preview.RendererThread;
import com.TBI.captureview.size.AspectRatio;
import com.TBI.captureview.size.Size;

/**
 * TBI Demo
 */
public class SnapshotGlPictureRecorder extends PictureRecorder {

    private static final String TAG = SnapshotGlPictureRecorder.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    private CameraEngine mEngine;
    private GlCameraPreview mPreview;
    private AspectRatio mOutputRatio;

    private Overlay mOverlay;
    private boolean mHasOverlay;
    private OverlayDrawer mOverlayDrawer;

    private int mTextureId;
    private float[] mTransform;


    private EglViewport mViewport;

    public SnapshotGlPictureRecorder(
            @NonNull PictureResult.Stub stub,
            @NonNull CameraEngine engine,
            @NonNull GlCameraPreview preview,
            @NonNull AspectRatio outputRatio) {
        super(stub, engine);
        mEngine = engine;
        mPreview = preview;
        mOutputRatio = outputRatio;
        mOverlay = engine.getOverlay();
        mHasOverlay = mOverlay != null && mOverlay.drawsOn(Overlay.Target.PICTURE_SNAPSHOT);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void take() {
        mPreview.addRendererFrameCallback(new RendererFrameCallback() {

            @RendererThread
            public void onRendererTextureCreated(int textureId) {
                SnapshotGlPictureRecorder.this.onRendererTextureCreated(textureId);
            }

            @RendererThread
            @Override
            public void onRendererFilterChanged(@NonNull Filter filter) {
                SnapshotGlPictureRecorder.this.onRendererFilterChanged(filter);
            }

            @RendererThread
            @Override
            public void onRendererFrame(@NonNull SurfaceTexture surfaceTexture, final float scaleX, final float scaleY) {
                mPreview.removeRendererFrameCallback(this);
                SnapshotGlPictureRecorder.this.onRendererFrame(surfaceTexture, scaleX, scaleY);
            }

        });
    }

    @RendererThread
    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected void onRendererTextureCreated(int textureId) {
        mTextureId = textureId;
        mViewport = new EglViewport();
        // Need to crop the size.
        Rect crop = CropHelper.computeCrop(mResult.size, mOutputRatio);
        mResult.size = new Size(crop.width(), crop.height());
        mTransform = new float[16];
        Matrix.setIdentityM(mTransform, 0);

        if (mHasOverlay) {
            mOverlayDrawer = new OverlayDrawer(mOverlay, mResult.size);
        }
    }

    @RendererThread
    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected void onRendererFilterChanged(@NonNull Filter filter) {
        mViewport.setFilter(filter.copy());
    }

    @RendererThread
    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected void onRendererFrame(@SuppressWarnings("unused") @NonNull final SurfaceTexture surfaceTexture,
                                 final float scaleX,
                                 final float scaleY) {

        final EGLContext eglContext = EGL14.eglGetCurrentContext();
        WorkerHandler.execute(new Runnable() {
            @Override
            public void run() {
                takeFrame(surfaceTexture, scaleX, scaleY, eglContext);

            }
        });
    }

    @WorkerThread
    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected void takeFrame(@NonNull SurfaceTexture surfaceTexture, float scaleX, float scaleY, @NonNull EGLContext eglContext) {

        // 0. EGL window will need an output.
        // We create a fake one as explained in javadocs.
        final int fakeOutputTextureId = 9999;
        SurfaceTexture fakeOutputSurface = new SurfaceTexture(fakeOutputTextureId);
        fakeOutputSurface.setDefaultBufferSize(mResult.size.getWidth(), mResult.size.getHeight());

        // 1. Create an EGL surface
        final EglCore core = new EglCore(eglContext, EglCore.FLAG_RECORDABLE);
        final EglBaseSurface eglSurface = new EglWindowSurface(core, fakeOutputSurface);
        eglSurface.makeCurrent();

        // 2. Apply scale and crop
        boolean flip = mEngine.getAngles().flip(Reference.VIEW, Reference.SENSOR);
        float realScaleX = flip ? scaleY : scaleX;
        float realScaleY = flip ? scaleX : scaleY;
        float scaleTranslX = (1F - realScaleX) / 2F;
        float scaleTranslY = (1F - realScaleY) / 2F;
        Matrix.translateM(mTransform, 0, scaleTranslX, scaleTranslY, 0);
        Matrix.scaleM(mTransform, 0, realScaleX, realScaleY, 1);

        // 3. Apply rotation and flip
        Matrix.translateM(mTransform, 0, 0.5F, 0.5F, 0); // Go back to 0,0
        Matrix.rotateM(mTransform, 0, -mResult.rotation, 0, 0, 1); // Rotate (not sure why we need the minus)
        mResult.rotation = 0;
        if (mResult.facing == Facing.FRONT) { // 5. Flip horizontally for front camera
            Matrix.scaleM(mTransform, 0, -1, 1, 1);
        }
        Matrix.translateM(mTransform, 0, -0.5F, -0.5F, 0); // Go back to old position

        // 4. Do pretty much the same for overlays
        if (mHasOverlay) {
            // 1. First we must draw on the texture and get latest image
            mOverlayDrawer.draw(Overlay.Target.PICTURE_SNAPSHOT);

            // 2. Then we can apply the transformations
            int rotation = mEngine.getAngles().offset(Reference.VIEW, Reference.OUTPUT, Axis.ABSOLUTE);
            Matrix.translateM(mOverlayDrawer.getTransform(), 0, 0.5F, 0.5F, 0);
            Matrix.rotateM(mOverlayDrawer.getTransform(), 0, rotation, 0, 0, 1);
            // No need to flip the x axis for front camera, but need to flip the y axis always.
            Matrix.scaleM(mOverlayDrawer.getTransform(), 0, 1, -1, 1);
            Matrix.translateM(mOverlayDrawer.getTransform(), 0, -0.5F, -0.5F, 0);
        }

        // 5. Draw and save
        LOG.i("takeFrame:", "timestamp:", surfaceTexture.getTimestamp());
        mViewport.drawFrame(mTextureId, mTransform);
        if (mHasOverlay) mOverlayDrawer.render();
        mResult.format = PictureResult.FORMAT_JPEG;
        mResult.data = eglSurface.saveFrameTo(Bitmap.CompressFormat.JPEG);

        // 6. Cleanup
        eglSurface.releaseEglSurface();
        mViewport.release();
        fakeOutputSurface.release();
        if (mHasOverlay) mOverlayDrawer.release();
        core.release();
        dispatchResult();
    }

    @Override
    protected void dispatchResult() {
        mEngine = null;
        mOutputRatio = null;
        super.dispatchResult();
    }
}
