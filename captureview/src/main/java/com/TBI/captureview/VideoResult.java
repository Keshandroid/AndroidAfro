package com.TBI.captureview;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.TBI.captureview.controls.Audio;
import com.TBI.captureview.controls.Facing;
import com.TBI.captureview.controls.VideoCodec;
import com.TBI.captureview.size.Size;

import java.io.File;

/**
 * TBI Demo
 */
@SuppressWarnings("WeakerAccess")
public class VideoResult {

    public static class Stub {

        Stub() {}

        public boolean isSnapshot;
        public Location location;
        public int rotation;
        public Size size;
        public File file;
        public Facing facing;
        public VideoCodec videoCodec;
        public Audio audio;
        public long maxSize;
        public int maxDuration;
        public int endReason;
        public int videoBitRate;
        public int videoFrameRate;
        public int audioBitRate;
    }

    @SuppressWarnings({"WeakerAccess", "unused"})
    public static final int REASON_USER = 0;

    @SuppressWarnings("WeakerAccess")
    public static final int REASON_MAX_SIZE_REACHED = 1;

    @SuppressWarnings("WeakerAccess")
    public static final int REASON_MAX_DURATION_REACHED = 2;

    private final boolean isSnapshot;
    private final Location location;
    private final int rotation;
    private final Size size;
    private final File file;
    private final Facing facing;
    private final VideoCodec videoCodec;
    private final Audio audio;
    private final long maxSize;
    private final int maxDuration;
    private final int endReason;
    private final int videoBitRate;
    private final int videoFrameRate;
    private final int audioBitRate;

    VideoResult(@NonNull Stub builder) {
        isSnapshot = builder.isSnapshot;
        location = builder.location;
        rotation = builder.rotation;
        size = builder.size;
        file = builder.file;
        facing = builder.facing;
        videoCodec = builder.videoCodec;
        audio = builder.audio;
        maxSize = builder.maxSize;
        maxDuration = builder.maxDuration;
        endReason = builder.endReason;
        videoBitRate = builder.videoBitRate;
        videoFrameRate = builder.videoFrameRate;
        audioBitRate = builder.audioBitRate;
    }

    public boolean isSnapshot() {
        return isSnapshot;
    }

    @Nullable
    public Location getLocation() {
        return location;
    }

    public int getRotation() {
        return rotation;
    }

    @NonNull
    public Size getSize() {
        return size;
    }

    @NonNull
    public File getFile() {
        return file;
    }

    @NonNull
    public Facing getFacing() {
        return facing;
    }

    @NonNull
    public VideoCodec getVideoCodec() {
        return videoCodec;
    }

    public long getMaxSize() {
        return maxSize;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    @NonNull
    public Audio getAudio() {
        return audio;
    }

    public int getTerminationReason() {
        return endReason;
    }

    public int getVideoBitRate() {
        return videoBitRate;
    }

    public int getVideoFrameRate() {
        return videoFrameRate;
    }

    public int getAudioBitRate() {
        return audioBitRate;
    }
}
