package com.TBI.captureview.preview;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.TBI.captureview.filter.Filter;

/**
 * TBI Demo
 */
public abstract class FilterCameraPreview<T extends View, Output> extends CameraPreview<T, Output> {

    @SuppressWarnings("WeakerAccess")
    public FilterCameraPreview(@NonNull Context context, @NonNull ViewGroup parent) {
        super(context, parent);
    }

    public abstract void setFilter(@NonNull Filter filter);

    @SuppressWarnings("unused")
    @NonNull
    public abstract Filter getCurrentFilter();
}
