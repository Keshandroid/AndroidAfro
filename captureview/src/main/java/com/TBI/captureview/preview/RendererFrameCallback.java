package com.TBI.captureview.preview;

import android.graphics.SurfaceTexture;

import androidx.annotation.NonNull;

import com.TBI.captureview.filter.Filter;

/**
 * TBI Demo
 */
public interface RendererFrameCallback {

    @RendererThread
    void onRendererTextureCreated(int textureId);

    @RendererThread
    void onRendererFrame(@NonNull SurfaceTexture surfaceTexture, float scaleX, float scaleY);

    @RendererThread
    void onRendererFilterChanged(@NonNull Filter filter);
}
