package com.TBI.captureview.preview;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.annotation.VisibleForTesting;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.internal.utils.Op;
import com.TBI.captureview.size.Size;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;

/**
 * TBI Demo
 */
public abstract class CameraPreview<T extends View, Output> {

    protected final static CameraLogger LOG = CameraLogger.create(CameraPreview.class.getSimpleName());

    public interface SurfaceCallback {
        void onSurfaceAvailable();
        void onSurfaceChanged();
        void onSurfaceDestroyed();
    }

    @VisibleForTesting
    Op<Void> mCropOp = new Op<>();
    private SurfaceCallback mSurfaceCallback;
    private T mView;
    boolean mCropping;


    int mOutputSurfaceWidth;
    int mOutputSurfaceHeight;


    int mInputStreamWidth;
    int mInputStreamHeight;

    int mDrawRotation;

    public CameraPreview(@NonNull Context context, @NonNull ViewGroup parent) {
        mView = onCreateView(context, parent);
    }

    public final void setSurfaceCallback(@Nullable SurfaceCallback callback) {
        if (hasSurface() && mSurfaceCallback != null) {
            mSurfaceCallback.onSurfaceDestroyed();
        }
        mSurfaceCallback = callback;
        if (hasSurface() && mSurfaceCallback != null) {
            mSurfaceCallback.onSurfaceAvailable();
        }
    }

    @NonNull
    protected abstract T onCreateView(@NonNull Context context, @NonNull ViewGroup parent);

    @NonNull
    public final T getView() {
        return mView;
    }

    @SuppressWarnings("unused")
    @VisibleForTesting
    @NonNull
    abstract View getRootView();

    @NonNull
    public abstract Output getOutput();

    @NonNull
    public abstract Class<Output> getOutputClass();

    public void setStreamSize(int width, int height) {
        LOG.i("setStreamSize:", "desiredW=", width, "desiredH=", height);
        mInputStreamWidth = width;
        mInputStreamHeight = height;
        if (mInputStreamWidth > 0 && mInputStreamHeight > 0) {
            crop(mCropOp);
        }
    }

    @SuppressWarnings("unused")
    @NonNull
    final Size getStreamSize() {
        return new Size(mInputStreamWidth, mInputStreamHeight);
    }

    @NonNull
    public final Size getSurfaceSize() {
        return new Size(mOutputSurfaceWidth, mOutputSurfaceHeight);
    }

    public final boolean hasSurface() {
        return mOutputSurfaceWidth > 0 && mOutputSurfaceHeight > 0;
    }

    @SuppressWarnings("WeakerAccess")
    protected final void dispatchOnSurfaceAvailable(int width, int height) {
        LOG.i("dispatchOnSurfaceAvailable:", "w=", width, "h=", height);
        mOutputSurfaceWidth = width;
        mOutputSurfaceHeight = height;
        if (mOutputSurfaceWidth > 0 && mOutputSurfaceHeight > 0) {
            crop(mCropOp);
        }
        if (mSurfaceCallback != null) {
            mSurfaceCallback.onSurfaceAvailable();
        }
    }

    @SuppressWarnings("WeakerAccess")
    protected final void dispatchOnSurfaceSizeChanged(int width, int height) {
        LOG.i("dispatchOnSurfaceSizeChanged:", "w=", width, "h=", height);
        if (width != mOutputSurfaceWidth || height != mOutputSurfaceHeight) {
            mOutputSurfaceWidth = width;
            mOutputSurfaceHeight = height;
            if (width > 0 && height > 0) {
                crop(mCropOp);
            }
            if (mSurfaceCallback != null) {
                mSurfaceCallback.onSurfaceChanged();
            }
        }
    }

    @SuppressWarnings("WeakerAccess")
    protected final void dispatchOnSurfaceDestroyed() {
        mOutputSurfaceWidth = 0;
        mOutputSurfaceHeight = 0;
        if (mSurfaceCallback != null) {
            mSurfaceCallback.onSurfaceDestroyed();
        }
    }

    public void onResume() {}

    public void onPause() {}

    @CallSuper
    public void onDestroy() {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            onDestroyView();
        } else {
            // Do this on the UI thread and wait.
            Handler ui = new Handler(Looper.getMainLooper());
            final TaskCompletionSource<Void> task = new TaskCompletionSource<>();
            ui.post(new Runnable() {
                @Override
                public void run() {
                    onDestroyView();
                    task.setResult(null);
                }
            });
            try { Tasks.await(task.getTask()); } catch (Exception ignore) {}
        }
    }

    @SuppressWarnings("WeakerAccess")
    @UiThread
    protected void onDestroyView() {
        View root = getRootView();
        ViewParent parent = root.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(root);
        }
    }

    protected void crop(@NonNull Op<Void> op) {
        // The base implementation does not support cropping.
        op.start();
        op.end(null);
    }

    public boolean supportsCropping() {
        return false;
    }

    public boolean isCropping() {
        return mCropping;
    }

    public void setDrawRotation(int drawRotation) {
        mDrawRotation = drawRotation;
    }
}
