package com.TBI.captureview.preview;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.TBI.captureview.R;
import com.TBI.captureview.filter.Filter;
import com.TBI.captureview.filter.NoFilter;
import com.TBI.captureview.internal.egl.EglViewport;
import com.TBI.captureview.internal.utils.Op;
import com.TBI.captureview.size.AspectRatio;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * TBI Demo
 */
public class GlCameraPreview extends FilterCameraPreview<GLSurfaceView, SurfaceTexture> {

    private boolean mDispatched;
    private final float[] mTransformMatrix = new float[16];
    private int mOutputTextureId = 0;
    private SurfaceTexture mInputSurfaceTexture;
    private EglViewport mOutputViewport;
    private final Set<RendererFrameCallback> mRendererFrameCallbacks = Collections.synchronizedSet(new HashSet<RendererFrameCallback>());
    @VisibleForTesting float mCropScaleX = 1F;
    @VisibleForTesting float mCropScaleY = 1F;
    private View mRootView;
    private Filter mCurrentFilter;

    public GlCameraPreview(@NonNull Context context, @NonNull ViewGroup parent) {
        super(context, parent);
    }

    @NonNull
    @Override
    protected GLSurfaceView onCreateView(@NonNull Context context, @NonNull ViewGroup parent) {
        ViewGroup root = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.cameraview_gl_view, parent, false);
        GLSurfaceView glView = root.findViewById(R.id.gl_surface_view);
        glView.setEGLContextClientVersion(2);
        glView.setRenderer(instantiateRenderer());
        glView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        glView.getHolder().addCallback(new SurfaceHolder.Callback() {
            public void surfaceCreated(SurfaceHolder holder) {}
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) { }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                dispatchOnSurfaceDestroyed();
                mDispatched = false;
            }
        });
        parent.addView(root, 0);
        mRootView = root;
        return glView;
    }

    @NonNull
    @Override
    View getRootView() {
        return mRootView;
    }

    /*@Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRect(new Rect((int) Math.random() * 100,
                (int) Math.random() * 100, 200, 200), rectanglePaint);
        Log.w(this.getClass().getName(), "On Draw Called");

        Paint p = new Paint();
        ColorMatrixColorFilter fl = new ColorMatrixColorFilter(new ColorMatrix(filter));
        p.setColorFilter(fl);
        canvas.drawPaint(p);
        Log.d("mn13", "overDraw");

    }*/


    @Override
    public void onResume() {
        super.onResume();
        getView().onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        getView().onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // View is gone, so EGL context is gone: callbacks make no sense anymore.
        mRendererFrameCallbacks.clear();
        if (mInputSurfaceTexture != null) {
            mInputSurfaceTexture.setOnFrameAvailableListener(null);
            mInputSurfaceTexture.release();
            mInputSurfaceTexture = null;
        }
        mOutputTextureId = 0;
        if (mOutputViewport != null) {
            mOutputViewport.release();
            mOutputViewport = null;
        }
    }

    public class Renderer implements GLSurfaceView.Renderer {

        @RendererThread
        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            if (mCurrentFilter == null) {
                mCurrentFilter = new NoFilter();
            }
            mOutputViewport = new EglViewport(mCurrentFilter);
            mOutputTextureId = mOutputViewport.createTexture();
            mInputSurfaceTexture = new SurfaceTexture(mOutputTextureId);
            getView().queueEvent(new Runnable() {
                @Override
                public void run() {
                    // Need to synchronize when iterating the Collections.synchronizedSet
                    synchronized (mRendererFrameCallbacks) {
                        for (RendererFrameCallback callback : mRendererFrameCallbacks) {
                            callback.onRendererTextureCreated(mOutputTextureId);
                        }
                    }
                }
            });

            mInputSurfaceTexture.setOnFrameAvailableListener(new SurfaceTexture.OnFrameAvailableListener() {
                @Override
                public void onFrameAvailable(SurfaceTexture surfaceTexture) {
                    getView().requestRender(); // requestRender is thread-safe.
                }
            });
        }

        @RendererThread
        @Override
        public void onSurfaceChanged(GL10 gl, final int width, final int height) {
            gl.glViewport(0, 0, width, height);
            mCurrentFilter.setSize(width, height);
            if (!mDispatched) {
                dispatchOnSurfaceAvailable(width, height);
                mDispatched = true;
            } else if (width != mOutputSurfaceWidth || height != mOutputSurfaceHeight) {
                dispatchOnSurfaceSizeChanged(width, height);
            }
        }

        @RendererThread
        @Override
        public void onDrawFrame(GL10 gl) {
            if (mInputSurfaceTexture == null) return;
            if (mInputStreamWidth <= 0 || mInputStreamHeight <= 0) {
                // Skip drawing. Camera was not opened.
                return;
            }

            mInputSurfaceTexture.updateTexImage();
            mInputSurfaceTexture.getTransformMatrix(mTransformMatrix);
            // LOG.v("onDrawFrame:", "timestamp:", mInputSurfaceTexture.getTimestamp());

            if (mDrawRotation != 0) {
                Matrix.translateM(mTransformMatrix, 0, 0.5F, 0.5F, 0);
                Matrix.rotateM(mTransformMatrix, 0, mDrawRotation, 0, 0, 1);
                Matrix.translateM(mTransformMatrix, 0, -0.5F, -0.5F, 0);
            }

            if (isCropping()) {
                float translX = (1F - mCropScaleX) / 2F;
                float translY = (1F - mCropScaleY) / 2F;
                Matrix.translateM(mTransformMatrix, 0, translX, translY, 0);
                Matrix.scaleM(mTransformMatrix, 0, mCropScaleX, mCropScaleY, 1);
            }
            mOutputViewport.drawFrame(mOutputTextureId, mTransformMatrix);
            synchronized (mRendererFrameCallbacks) {
                // Need to synchronize when iterating the Collections.synchronizedSet
                for (RendererFrameCallback callback : mRendererFrameCallbacks) {
                    callback.onRendererFrame(mInputSurfaceTexture, mCropScaleX, mCropScaleY);
                }
            }
        }
    }

    @NonNull
    @Override
    public Class<SurfaceTexture> getOutputClass() {
        return SurfaceTexture.class;
    }

    @NonNull
    @Override
    public SurfaceTexture getOutput() {
        return mInputSurfaceTexture;
    }

    @Override
    public boolean supportsCropping() {
        return true;
    }

    @Override
    protected void crop(@NonNull Op<Void> op) {
        op.start();
        if (mInputStreamWidth > 0 && mInputStreamHeight > 0 && mOutputSurfaceWidth > 0 && mOutputSurfaceHeight > 0) {
            float scaleX = 1f, scaleY = 1f;
            AspectRatio current = AspectRatio.of(mOutputSurfaceWidth, mOutputSurfaceHeight);
            AspectRatio target = AspectRatio.of(mInputStreamWidth, mInputStreamHeight);
            if (current.toFloat() >= target.toFloat()) {
                // We are too short. Must increase height.
                scaleY = current.toFloat() / target.toFloat();
            } else {
                // We must increase width.
                scaleX = target.toFloat() / current.toFloat();
            }
            mCropping = scaleX > 1.02f || scaleY > 1.02f;
            mCropScaleX = 1F / scaleX;
            mCropScaleY = 1F / scaleY;
            getView().requestRender();
        }
        op.end(null);
    }

    public void addRendererFrameCallback(@NonNull final RendererFrameCallback callback) {
        getView().queueEvent(new Runnable() {
            @Override
            public void run() {
                mRendererFrameCallbacks.add(callback);
                if (mOutputTextureId != 0) callback.onRendererTextureCreated(mOutputTextureId);
                callback.onRendererFilterChanged(mCurrentFilter);
            }
        });
    }

    public void removeRendererFrameCallback(@NonNull final RendererFrameCallback callback) {
        mRendererFrameCallbacks.remove(callback);
    }

    @SuppressWarnings("unused")
    protected int getTextureId() {
        return mOutputTextureId;
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    protected Renderer instantiateRenderer() {
        return new Renderer();
    }

    @NonNull
    @Override
    public Filter getCurrentFilter() {
        return mCurrentFilter;
    }

    @Override
    public void setFilter(final @NonNull Filter filter) {
        mCurrentFilter = filter;
        if (hasSurface()) {
            filter.setSize(mOutputSurfaceWidth, mOutputSurfaceHeight);
        }

        getView().queueEvent(new Runnable() {
            @Override
            public void run() {
                if (mOutputViewport != null) {
                    mOutputViewport.setFilter(filter);
                }

                // Need to synchronize when iterating the Collections.synchronizedSet
                synchronized (mRendererFrameCallbacks) {
                    for (RendererFrameCallback callback : mRendererFrameCallbacks) {
                        callback.onRendererFilterChanged(filter);
                    }
                }
            }
        });
    }
}
