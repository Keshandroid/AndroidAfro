package com.TBI.captureview.frame;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.size.Size;

import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public class Frame {

    private final static String TAG = Frame.class.getSimpleName();
    private final static CameraLogger LOG = CameraLogger.create(TAG);

    private final FrameManager mManager;

    private byte[] mData = null;
    private long mTime = -1;
    private long mLastTime = -1;
    private int mRotation = 0;
    private Size mSize = null;
    private int mFormat = -1;

    Frame(@NonNull FrameManager manager) {
        mManager = manager;
    }

    void setContent(@NonNull byte[] data, long time, int rotation, @NonNull Size size, int format) {
        this.mData = data;
        this.mTime = time;
        this.mLastTime = time;
        this.mRotation = rotation;
        this.mSize = size;
        this.mFormat = format;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean hasContent() {
        return mData != null;
    }

    private void ensureHasContent() {
        if (!hasContent()) {
            LOG.e("Frame is dead! time:", mTime, "lastTime:", mLastTime);
            throw new RuntimeException("You should not access a released frame. " +
                    "If this frame was passed to a FrameProcessor, you can only use its contents synchronously, " +
                    "for the duration of the process() method.");
        }
    }


    @Override
    public boolean equals(Object obj) {
        // We want a super fast implementation here, do not compare arrays.
        return obj instanceof Frame && ((Frame) obj).mTime == mTime;
    }

    @NonNull
    public Frame freeze() {
        ensureHasContent();
        byte[] data = new byte[mData.length];
        System.arraycopy(mData, 0, data, 0, mData.length);
        Frame other = new Frame(mManager);
        other.setContent(data, mTime, mRotation, mSize, mFormat);
        return other;
    }

    public void release() {
        if (!hasContent()) return;
        LOG.v("Frame with time", mTime, "is being released.");
        byte[] data = mData;
        mData = null;
        mRotation = 0;
        mTime = -1;
        mSize = null;
        mFormat = -1;
        // After the manager is notified, this frame instance can be taken by
        // someone else, possibly from another thread. So this should be the
        // last call in this method. If we null data after, we can have issues.
        mManager.onFrameReleased(this, data);
    }

    @NonNull
    public byte[] getData() {
        ensureHasContent();
        return mData;
    }

    public long getTime() {
        ensureHasContent();
        return mTime;
    }

    public int getRotation() {
        ensureHasContent();
        return mRotation;
    }

    @NonNull
    public Size getSize() {
        ensureHasContent();
        return mSize;
    }

    public int getFormat() {
        ensureHasContent();
        return mFormat;
    }
}
