package com.TBI.captureview.frame;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

/**
 * TBI Demo
 */
public interface FrameProcessor {
    @WorkerThread
    void process(@NonNull Frame frame);
}
