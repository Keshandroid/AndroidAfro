package com.TBI.captureview.frame;


import android.graphics.ImageFormat;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.size.Size;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * TBI Demo
 */
public class FrameManager {

    private static final String TAG = FrameManager.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    public interface BufferCallback {
        void onBufferAvailable(@NonNull byte[] buffer);
    }

    private final int mPoolSize;
    private int mBufferSize = -1;
    private Size mFrameSize = null;
    private int mFrameFormat = -1;
    private LinkedBlockingQueue<Frame> mFrameQueue;
    private LinkedBlockingQueue<byte[]> mBufferQueue;
    private BufferCallback mBufferCallback;
    private final int mBufferMode;

    private final static int BUFFER_MODE_DISPATCH = 0;

    private final static int BUFFER_MODE_ENQUEUE = 1;

    public FrameManager(int poolSize, @Nullable BufferCallback callback) {
        mPoolSize = poolSize;
        mFrameQueue = new LinkedBlockingQueue<>(mPoolSize);
        if (callback != null) {
            mBufferCallback = callback;
            mBufferMode = BUFFER_MODE_DISPATCH;
        } else {
            mBufferQueue = new LinkedBlockingQueue<>(mPoolSize);
            mBufferMode = BUFFER_MODE_ENQUEUE;
        }
    }

    public int setUp(int format, @NonNull Size size) {
        if (isSetUp()) {
            // TODO throw or just reconfigure?
        }
        mFrameSize = size;
        mFrameFormat = format;
        int bitsPerPixel = ImageFormat.getBitsPerPixel(format);
        long sizeInBits = size.getHeight() * size.getWidth() * bitsPerPixel;
        mBufferSize = (int) Math.ceil(sizeInBits / 8.0d);
        for (int i = 0; i < mPoolSize; i++) {
            if (mBufferMode == BUFFER_MODE_DISPATCH) {
                mBufferCallback.onBufferAvailable(new byte[mBufferSize]);
            } else {
                mBufferQueue.offer(new byte[mBufferSize]);
            }
        }
        return mBufferSize;
    }

    private boolean isSetUp() {
        return mFrameSize != null;
    }

    @Nullable
    public byte[] getBuffer() {
        if (mBufferMode != BUFFER_MODE_ENQUEUE) {
            throw new IllegalStateException("Can't call getBuffer() when not in BUFFER_MODE_ENQUEUE.");
        }
        return mBufferQueue.poll();
    }

    public void onBufferUnused(@NonNull byte[] buffer) {
        if (mBufferMode != BUFFER_MODE_ENQUEUE) {
            throw new IllegalStateException("Can't call onBufferUnused() when not in BUFFER_MODE_ENQUEUE.");
        }

        if (isSetUp()) {
            mBufferQueue.offer(buffer);
        } else {
            LOG.w("onBufferUnused: buffer was returned but we're not set up anymore.");
        }
    }

    @NonNull
    public Frame getFrame(@NonNull byte[] data, long time, int rotation) {
        if (!isSetUp()) {
            throw new IllegalStateException("Can't call getFrame() after releasing or before setUp.");
        }

        Frame frame = mFrameQueue.poll();
        if (frame != null) {
            LOG.v("getFrame for time:", time, "RECYCLING.");
        } else {
            LOG.v("getFrame for time:", time, "CREATING.");
            frame = new Frame(this);
        }
        frame.setContent(data, time, rotation, mFrameSize, mFrameFormat);
        return frame;
    }

    void onFrameReleased(@NonNull Frame frame, @NonNull byte[] buffer) {
        if (!isSetUp()) return;
        // If frame queue is full, let's drop everything.
        // If frame queue accepts this frame, let's recycle the buffer as well.
        if (mFrameQueue.offer(frame)) {
            int currSize = buffer.length;
            int reqSize = mBufferSize;
            if (currSize == reqSize) {
                if (mBufferMode == BUFFER_MODE_DISPATCH) {
                    mBufferCallback.onBufferAvailable(buffer);
                } else {
                    mBufferQueue.offer(buffer);
                }
            }
        }
    }

    public void release() {
        if (!isSetUp()) {
            LOG.w("release called twice. Ignoring.");
            return;
        }

        LOG.i("release: Clearing the frame and buffer queue.");
        mFrameQueue.clear();
        if (mBufferMode == BUFFER_MODE_ENQUEUE) {
            mBufferQueue.clear();
        }
        mBufferSize = -1;
        mFrameSize = null;
        mFrameFormat = -1;
    }
}
