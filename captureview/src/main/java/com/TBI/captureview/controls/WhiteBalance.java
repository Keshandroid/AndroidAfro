package com.TBI.captureview.controls;


import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public enum WhiteBalance implements Control {

    AUTO(0),
    INCANDESCENT(1),
    FLUORESCENT(2),
    DAYLIGHT(3),
    CLOUDY(4);

    static final WhiteBalance DEFAULT = AUTO;

    private int value;

    WhiteBalance(int value) {
        this.value = value;
    }

    int value() {
        return value;
    }

    @NonNull
    static WhiteBalance fromValue(int value) {
        WhiteBalance[] list = WhiteBalance.values();
        for (WhiteBalance action : list) {
            if (action.value() == value) {
                return action;
            }
        }
        return DEFAULT;
    }
}