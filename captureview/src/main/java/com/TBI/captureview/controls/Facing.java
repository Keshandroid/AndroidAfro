package com.TBI.captureview.controls;


import android.content.Context;

import com.TBI.captureview.CameraUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * TBI Demo
 */
public enum Facing implements Control {

    BACK(0),
    FRONT(1);

    @NonNull
    static Facing DEFAULT(@Nullable Context context) {
        if (context == null) {
            return BACK;
        } else if (CameraUtils.hasCameraFacing(context, BACK)) {
            return BACK;
        } else if (CameraUtils.hasCameraFacing(context, FRONT)) {
            return FRONT;
        } else {
            // The controller will throw a CameraException.
            // This device has no cameras.
            return BACK;
        }
    }

    private int value;

    Facing(int value) {
        this.value = value;
    }

    int value() {
        return value;
    }

    @Nullable
    static Facing fromValue(int value) {
        Facing[] list = Facing.values();
        for (Facing action : list) {
            if (action.value() == value) {
                return action;
            }
        }
        return null;
    }
}
