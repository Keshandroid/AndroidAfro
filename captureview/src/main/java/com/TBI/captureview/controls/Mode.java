package com.TBI.captureview.controls;


import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public enum Mode implements Control {

    PICTURE(0),
    VIDEO(1);

    static final Mode DEFAULT = PICTURE;

    private int value;

    Mode(int value) {
        this.value = value;
    }

    int value() {
        return value;
    }

    @NonNull
    static Mode fromValue(int value) {
        Mode[] list = Mode.values();
        for (Mode action : list) {
            if (action.value() == value) {
                return action;
            }
        }
        return DEFAULT;
    }
}
