package com.TBI.captureview.controls;


import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public enum Engine implements Control {

    CAMERA1(0),
    CAMERA2(1);

    final static Engine DEFAULT = CAMERA1;

    private int value;

    Engine(int value) {
        this.value = value;
    }

    int value() {
        return value;
    }

    @NonNull
    static Engine fromValue(int value) {
        Engine[] list = Engine.values();
        for (Engine action : list) {
            if (action.value() == value) {
                return action;
            }
        }
        return DEFAULT;
    }
}
