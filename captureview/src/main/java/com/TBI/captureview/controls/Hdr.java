package com.TBI.captureview.controls;


import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public enum Hdr implements Control {

    OFF(0),
    ON(1);

    final static Hdr DEFAULT = OFF;

    private int value;

    Hdr(int value) {
        this.value = value;
    }

    int value() {
        return value;
    }

    @NonNull
    static Hdr fromValue(int value) {
        Hdr[] list = Hdr.values();
        for (Hdr action : list) {
            if (action.value() == value) {
                return action;
            }
        }
        return DEFAULT;
    }
}
