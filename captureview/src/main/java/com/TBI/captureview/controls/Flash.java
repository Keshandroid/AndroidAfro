package com.TBI.captureview.controls;


import androidx.annotation.NonNull;

/**
 * TBI Demo
 */
public enum Flash implements Control {

    OFF(0),
    ON(1),
    AUTO(2),
    TORCH(3);

    static final Flash DEFAULT = OFF;

    private int value;

    Flash(int value) {
        this.value = value;
    }

    int value() {
        return value;
    }

    @NonNull
    static Flash fromValue(int value) {
        Flash[] list = Flash.values();
        for (Flash action : list) {
            if (action.value() == value) {
                return action;
            }
        }
        return DEFAULT;
    }
}
