package com.TBI.captureview.internal;

import android.opengl.GLES11Ext;
import android.opengl.GLES20;

public class Issue514Workaround {

    private final int textureId;

    public Issue514Workaround(int textureId) {
        this.textureId = textureId;
    }

    public void beforeOverlayUpdateTexImage() {
        bindTexture(textureId);
    }

    public void end() {
        bindTexture(0);
    }

    private void bindTexture(int textureId) {
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureId);
    }
}
