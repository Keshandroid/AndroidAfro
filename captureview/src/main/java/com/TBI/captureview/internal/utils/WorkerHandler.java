package com.TBI.captureview.internal.utils;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import androidx.annotation.NonNull;

import com.TBI.captureview.CameraLogger;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;

import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

/**
 * TBI Demo
 */
public class WorkerHandler {

    private final static CameraLogger LOG = CameraLogger.create(WorkerHandler.class.getSimpleName());
    private final static ConcurrentHashMap<String, WeakReference<WorkerHandler>> sCache = new ConcurrentHashMap<>(4);

    private final static String FALLBACK_NAME = "FallbackCameraThread";

    @SuppressWarnings("FieldCanBeLocal")
    private static WorkerHandler sFallbackHandler;

    @NonNull
    public static WorkerHandler get(@NonNull String name) {
        if (sCache.containsKey(name)) {
            //noinspection ConstantConditions
            WorkerHandler cached = sCache.get(name).get();
            if (cached != null) {
                if (cached.getThread().isAlive() && !cached.getThread().isInterrupted()) {
                    LOG.w("get:", "Reusing cached worker handler.", name);
                    return cached;
                } else {
                    // Cleanup the old thread before creating a new one
                    cached.destroy();
                    LOG.w("get:", "Thread reference found, but not alive or interrupted. Removing.", name);
                    sCache.remove(name);
                }
            } else {
                LOG.w("get:", "Thread reference died. Removing.", name);
                sCache.remove(name);
            }
        }

        LOG.i("get:", "Creating new handler.", name);
        WorkerHandler handler = new WorkerHandler(name);
        sCache.put(name, new WeakReference<>(handler));
        return handler;
    }

    @NonNull
    public static WorkerHandler get() {
        sFallbackHandler = get(FALLBACK_NAME);
        return sFallbackHandler;
    }

    public static void execute(@NonNull Runnable action) {
        get().post(action);
    }

    private HandlerThread mThread;
    private Handler mHandler;
    private Executor mExecutor;

    private WorkerHandler(@NonNull String name) {
        mThread = new HandlerThread(name);
        mThread.setDaemon(true);
        mThread.start();
        mHandler = new Handler(mThread.getLooper());
        mExecutor = new Executor() {
            @Override
            public void execute(Runnable command) {
                WorkerHandler.this.run(command);
            }
        };

        // HandlerThreads/Handlers sometimes have a significant warmup time.
        // We want to spend this time here so when this object is built, it
        // is fully operational.
        final CountDownLatch latch = new CountDownLatch(1);
        post(new Runnable() {
            @Override
            public void run() {
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException ignore) {}
    }

    public void run(@NonNull Runnable runnable) {
        if (Thread.currentThread() == getThread()) {
            runnable.run();
        } else {
            post(runnable);
        }
    }

    public <T> Task<T> run(@NonNull Callable<T> callable) {
        if (Thread.currentThread() == getThread()) {
            try {
                return Tasks.forResult(callable.call());
            } catch (Exception e) {
                return Tasks.forException(e);
            }
        } else {
            return post(callable);
        }
    }

    public void post(@NonNull Runnable runnable) {
        mHandler.post(runnable);
    }

    public <T> Task<T> post(@NonNull final Callable<T> callable) {
        final TaskCompletionSource<T> source = new TaskCompletionSource<>();
        post(new Runnable() {
            @Override
            public void run() {
                try {
                    source.trySetResult(callable.call());
                } catch (Exception e) {
                    source.trySetException(e);
                }
            }
        });
        return source.getTask();
    }

    public void post(long delay, @NonNull Runnable runnable) {
        mHandler.postDelayed(runnable, delay);
    }

    public void remove(@NonNull Runnable runnable) {
        mHandler.removeCallbacks(runnable);
    }

    @NonNull
    public Handler getHandler() {
        return mHandler;
    }

    @NonNull
    public HandlerThread getThread() {
        return mThread;
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    public Looper getLooper() {
        return mThread.getLooper();
    }

    @NonNull
    public Executor getExecutor() {
        return mExecutor;
    }

    public void destroy() {
        HandlerThread thread = getThread();
        if (thread.isAlive()) {
            thread.interrupt();
            thread.quit();
            // after quit(), the thread will die at some point in the future. Might take some ms.
            // try { handler.getThread().join(); } catch (InterruptedException ignore) {}
        }
    }

    public static void destroyAll() {
        for (String key : sCache.keySet()) {
            WeakReference<WorkerHandler> ref = sCache.get(key);
            //noinspection ConstantConditions
            WorkerHandler handler = ref.get();
            if (handler != null) handler.destroy();
            ref.clear();
        }
        sCache.clear();
    }
}
