package com.TBI.captureview.internal.utils;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.TBI.captureview.CameraLogger;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * TBI Demo
 */
public class Pool<T> {

    private static final String TAG = Pool.class.getSimpleName();
    private static final CameraLogger LOG = CameraLogger.create(TAG);

    private int maxPoolSize;
    private int activeCount;
    private LinkedBlockingQueue<T> queue;
    private Factory<T> factory;
    private final Object lock = new Object();

    public interface Factory<T> {
        T create();
    }

    public Pool(int maxPoolSize, @NonNull Factory<T> factory) {
        this.maxPoolSize = maxPoolSize;
        this.queue = new LinkedBlockingQueue<>(maxPoolSize);
        this.factory = factory;
    }

    public boolean isEmpty() {
        synchronized (lock) {
            return count() >= maxPoolSize;
        }
    }

    @Nullable
    public T get() {
        synchronized (lock) {
            T item = queue.poll();
            if (item != null) {
                activeCount++; // poll decreases, this fixes
                LOG.v("GET - Reusing recycled item.", this);
                return item;
            }

            if (isEmpty()) {
                LOG.v("GET - Returning null. Too much items requested.", this);
                return null;
            }

            activeCount++;
            LOG.v("GET - Creating a new item.", this);
            return factory.create();
        }
    }

    public void recycle(@NonNull T item) {
        synchronized (lock) {
            LOG.v("RECYCLE - Recycling item.", this);
            if (--activeCount < 0) {
                throw new IllegalStateException("Trying to recycle an item which makes activeCount < 0." +
                        "This means that this or some previous items being recycled were not coming from " +
                        "this pool, or some item was recycled more than once. " + this);
            }
            if (!queue.offer(item)) {
                throw new IllegalStateException("Trying to recycle an item while the queue is full. " +
                        "This means that this or some previous items being recycled were not coming from " +
                        "this pool, or some item was recycled more than once. " + this);
            }
        }
    }

    @CallSuper
    public void clear() {
        synchronized (lock) {
            queue.clear();
        }
    }

    @SuppressWarnings("WeakerAccess")
    public final int count() {
        synchronized (lock) {
            return activeCount() + recycledCount();
        }
    }

    @SuppressWarnings("WeakerAccess")
    public final int activeCount() {
        synchronized (lock) {
            return activeCount;
        }
    }

    @SuppressWarnings("WeakerAccess")
    public final int recycledCount() {
        synchronized (lock) {
            return queue.size();
        }
    }

    @NonNull
    @Override
    public String toString() {
        return getClass().getSimpleName() + " - count:" + count() + ", active:" + activeCount() + ", recycled:" + recycledCount();
    }
}
