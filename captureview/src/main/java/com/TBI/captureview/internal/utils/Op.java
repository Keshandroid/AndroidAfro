package com.TBI.captureview.internal.utils;

import androidx.annotation.NonNull;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * TBI Demo
 */
public class Op<T> {

    private CountDownLatch mLatch;
    private T mResult;
    private int mCount;

    public Op() { }

    public Op(boolean startListening) {
        if (startListening) listen();
    }

    private boolean isListening() {
        return mLatch != null;
    }

    public void start() {
        if (!isListening()) mCount++;
    }

    public void end(T result) {
        if (mCount > 0) {
            mCount--;
            return;
        }

        if (isListening()) { // Should be always true.
            mResult = result;
            mLatch.countDown();
        }
    }

    public void listen() {
        if (isListening()) throw new RuntimeException("Should not happen.");
        mResult = null;
        mLatch = new CountDownLatch(1);
    }

    public T await(long millis) {
        return await(millis, TimeUnit.MILLISECONDS);
    }

    public T await() {
        return await(1, TimeUnit.MINUTES);
    }

    private T await(long time, @NonNull TimeUnit unit) {
        try {
            mLatch.await(time, unit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        T result = mResult;
        mResult = null;
        mLatch = null;
        return result;
    }


}
