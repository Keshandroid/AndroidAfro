package com.TBI.captureview.internal;

import android.annotation.SuppressLint;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.TBI.captureview.CameraLogger;
import com.TBI.captureview.size.Size;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * TBI Demo
 */
public class DeviceEncoders {

    private final static String TAG = DeviceEncoders.class.getSimpleName();
    private final static CameraLogger LOG = CameraLogger.create(TAG);

    @VisibleForTesting static boolean ENABLED = Build.VERSION.SDK_INT >= 21;

    public final static int MODE_TAKE_FIRST = 0;
    public final static int MODE_PREFER_HARDWARE = 1;

    @SuppressWarnings("FieldCanBeLocal")
    private final MediaCodecInfo mVideoEncoder;
    @SuppressWarnings("FieldCanBeLocal")
    private final MediaCodecInfo mAudioEncoder;
    private final MediaCodecInfo.VideoCapabilities mVideoCapabilities;
    private final MediaCodecInfo.AudioCapabilities mAudioCapabilities;

    @SuppressLint("NewApi")
    public DeviceEncoders(@NonNull String videoType, @NonNull String audioType, int mode) {
        if (ENABLED) {
            List<MediaCodecInfo> encoders = getDeviceEncoders();
            mVideoEncoder = findDeviceEncoder(encoders, videoType, mode);
            LOG.i("Enabled. Found video encoder:", mVideoEncoder.getName());
            mAudioEncoder = findDeviceEncoder(encoders, audioType, mode);
            LOG.i("Enabled. Found audio encoder:", mAudioEncoder.getName());
            mVideoCapabilities = mVideoEncoder.getCapabilitiesForType(videoType).getVideoCapabilities();
            mAudioCapabilities = mAudioEncoder.getCapabilitiesForType(audioType).getAudioCapabilities();
        } else {
            mVideoEncoder = null;
            mAudioEncoder = null;
            mVideoCapabilities = null;
            mAudioCapabilities = null;
            LOG.i("Disabled.");
        }
    }

    @NonNull
    @SuppressLint("NewApi")
    @VisibleForTesting
    List<MediaCodecInfo> getDeviceEncoders() {
        ArrayList<MediaCodecInfo> results = new ArrayList<>();
        MediaCodecInfo[] array = new MediaCodecList(MediaCodecList.REGULAR_CODECS).getCodecInfos();
        for (MediaCodecInfo info : array) {
            if (info.isEncoder()) results.add(info);
        }
        return results;
    }

    @SuppressLint("NewApi")
    @VisibleForTesting
    boolean isHardwareEncoder(@NonNull String encoder) {
        encoder = encoder.toLowerCase();
        boolean isSoftwareEncoder = encoder.startsWith("omx.google.")
                || encoder.startsWith("c2.android.")
                || (!encoder.startsWith("omx.") && !encoder.startsWith("c2."));
        return !isSoftwareEncoder;
    }

    @SuppressLint("NewApi")
    @NonNull
    @VisibleForTesting
    MediaCodecInfo findDeviceEncoder(@NonNull List<MediaCodecInfo> encoders, @NonNull String mimeType, int mode) {
        ArrayList<MediaCodecInfo> results = new ArrayList<>();
        for (MediaCodecInfo encoder : encoders) {
            String[] types = encoder.getSupportedTypes();
            for (String type : types) {
                if (type.equalsIgnoreCase(mimeType)) {
                    results.add(encoder);
                    break;
                }
            }
        }
        LOG.i("findDeviceEncoder -", "type:", mimeType, "encoders:", results.size());
        if (mode == MODE_PREFER_HARDWARE) {
            Collections.sort(results, new Comparator<MediaCodecInfo>() {
                @Override
                public int compare(MediaCodecInfo o1, MediaCodecInfo o2) {
                    boolean hw1 = isHardwareEncoder(o1.getName());
                    boolean hw2 = isHardwareEncoder(o2.getName());
                    if (hw1 && hw2) return 0;
                    if (hw1) return -1;
                    if (hw2) return 1;
                    return 0;
                }
            });
        }
        if (results.isEmpty()) {
            throw new RuntimeException("No encoders for type:" + mimeType);
        }
        return results.get(0);
    }

    @SuppressLint("NewApi")
    @NonNull
    public Size getSupportedVideoSize(@NonNull Size size) {
        if (!ENABLED) return size;
        int width = size.getWidth();
        int height = size.getHeight();
        double aspect = (double) width / height;

        // If width is too large, scale down, but keep aspect ratio.
        if (mVideoCapabilities.getSupportedWidths().getUpper() < width) {
            width = mVideoCapabilities.getSupportedWidths().getUpper();
            height = (int) Math.round(width / aspect);
        }

        // If height is too large, scale down, but keep aspect ratio.
        if (mVideoCapabilities.getSupportedHeights().getUpper() < height) {
            height = mVideoCapabilities.getSupportedHeights().getUpper();
            width = (int) Math.round(aspect * height);
        }

        // Adjust the alignment.
        while (width % mVideoCapabilities.getWidthAlignment() != 0) width--;
        while (height % mVideoCapabilities.getHeightAlignment() != 0) height--;

        // It's still possible that we're BELOW the lower.
        if (!mVideoCapabilities.getSupportedWidths().contains(width)) {
            throw new RuntimeException("Width not supported after adjustment." +
                    " Desired:" + width +
                    " Range:" + mVideoCapabilities.getSupportedWidths());
        }
        if (!mVideoCapabilities.getSupportedHeights().contains(height)) {
            throw new RuntimeException("Height not supported after adjustment." +
                    " Desired:" + height +
                    " Range:" + mVideoCapabilities.getSupportedHeights());
        }

        // It's still possible that we're unsupported for other reasons.
        if (!mVideoCapabilities.isSizeSupported(width, height)) {
            throw new RuntimeException("Size not supported for unknown reason." +
                    " Might be an aspect ratio issue." +
                    " Desired size:" + new Size(width, height));
        }
        Size adjusted = new Size(width, height);
        LOG.i("getSupportedVideoSize -", "inputSize:", size, "adjustedSize:", adjusted);
        return adjusted;
    }

    @SuppressLint("NewApi")
    public int getSupportedVideoBitRate(int bitRate) {
        if (!ENABLED) return bitRate;
        int newBitRate = mVideoCapabilities.getBitrateRange().clamp(bitRate);
        LOG.i("getSupportedVideoBitRate -", "inputRate:", bitRate, "adjustedRate:", newBitRate);
        return newBitRate;
    }

    @SuppressLint("NewApi")
    public int getSupportedVideoFrameRate(@NonNull Size size, int frameRate) {
        if (!ENABLED) return frameRate;
        int newFrameRate = (int) (double) mVideoCapabilities
                .getSupportedFrameRatesFor(size.getWidth(), size.getHeight())
                .clamp((double) frameRate);
        LOG.i("getSupportedVideoFrameRate -", "inputRate:", frameRate, "adjustedRate:", newFrameRate);
        return newFrameRate;
    }

    @SuppressLint("NewApi")
    public int getSupportedAudioBitRate(int bitRate) {
        if (!ENABLED) return bitRate;
        int newBitRate = mAudioCapabilities.getBitrateRange().clamp(bitRate);
        LOG.i("getSupportedAudioBitRate -", "inputRate:", bitRate, "adjustedRate:", newBitRate);
        return newBitRate;
    }

    @SuppressLint("NewApi")
    @Nullable
    public String getVideoEncoder() {
        if (mVideoEncoder != null) {
            return mVideoEncoder.getName();
        } else {
            return null;
        }
    }

    @SuppressLint("NewApi")
    @Nullable
    public String getAudioEncoder() {
        if (mAudioEncoder != null) {
            return mAudioEncoder.getName();
        } else {
            return null;
        }
    }

}
