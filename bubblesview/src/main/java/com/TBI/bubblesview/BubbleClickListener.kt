package com.TBI.bubblesview

import com.TBI.bubblesview.model.BubbleItem

/**
 * TBI Demo.
 */
interface BubbleClickListener {

    fun onBubbleSelected(item: BubbleItem)

    /*fun onBubbleDeselected(item: BubbleItem)*/

}