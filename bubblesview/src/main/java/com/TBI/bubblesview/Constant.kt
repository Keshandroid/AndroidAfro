package com.TBI.bubblesview

/**
 * TBI Demo.
 */
object Constant {

    val TEXTURE_VERTICES = floatArrayOf(0f, 0f, 0f, 1f, 1f, 0f, 1f, 1f)

    val FLOAT_SIZE = 4

}