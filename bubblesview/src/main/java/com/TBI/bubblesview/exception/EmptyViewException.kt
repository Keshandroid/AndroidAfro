package com.TBI.bubblesview.exception

/**
 * TBI Demo.
 */
class EmptyViewException() : Exception("View must have at least one item when it becomes visible")