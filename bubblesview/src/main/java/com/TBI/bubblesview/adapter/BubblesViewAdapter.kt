package com.TBI.bubblesview.adapter

import com.TBI.bubblesview.model.BubbleItem

/**
 * TBI Demo.
 */
interface BubblesViewAdapter {

    val totalCount: Int

    fun getItem(position: Int): BubbleItem

}