package com.TBI.bubblesview.model

import android.graphics.Typeface
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt

/**
 * TBI Demo.
 */
data class BubbleItem @JvmOverloads constructor(var title: String? = null,
                                                var icon: Drawable? = null,
                                                var postId: Int? = 0,
                                                var postPosition: Int? = 0,
                                                var iconOnTop: Boolean = true,
                                                @ColorInt var color: Int? = null,
                                                var gradient: BubbleGradient? = null,
                                                var overlayAlpha: Float = 0f,
                                                var typeface: Typeface = Typeface.DEFAULT,
                                                @ColorInt var textColor: Int? = null,
                                                var textSize: Float = 60f,
                                                var backgroundImage: Drawable? = null,
                                                var isSelected: Boolean = false,
                                                var area: Float = 1.4f,
                                                var customData: Any? = null)