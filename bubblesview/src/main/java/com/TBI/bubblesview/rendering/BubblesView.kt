package com.TBI.bubblesview.rendering

import android.content.Context
import android.graphics.PixelFormat
import android.opengl.GLSurfaceView
import androidx.annotation.ColorInt
import android.util.AttributeSet
import android.view.MotionEvent
import com.TBI.bubblesview.BubbleClickListener
import com.TBI.bubblesview.R
import com.TBI.bubblesview.adapter.BubblesViewAdapter
import com.TBI.bubblesview.model.Color
import com.TBI.bubblesview.model.BubbleItem

/**
 * TBI Demo.
 */
class BubblesView : GLSurfaceView {

    @ColorInt var background: Int = 0
        set(value) {
            field = value
            renderer.backgroundColor = Color(value)
        }
    @Deprecated(level = DeprecationLevel.WARNING,
            message = "Use BubblesViewAdapter for the view setup instead")
    var items: ArrayList<BubbleItem>? = null
        set(value) {
            field = value
            renderer.items = value ?: ArrayList()
        }
    var adapter: BubblesViewAdapter? = null
        set(value) {
            field = value
            if (value != null) {
                renderer.items = ArrayList((0..value.totalCount - 1)
                        .map { value.getItem(it) }.toList())
            }
        }
    var maxSelectedCount: Int? = null
        set(value) {
            renderer.maxSelectedCount = value
        }
    var listener: BubbleClickListener? = null
        set(value) {
            renderer.listener = value
        }
    var bubbleSize = 50
        set(value) {
            if (value in 1..100) {
                renderer.bubbleSize = value
            }
        }
    val selectedItems: List<BubbleItem?>
        get() = renderer.selectedItems

    var centerImmediately = false
        set(value) {
            field = value
            renderer.centerImmediately = value
        }

    var scaledTouchSlop: Int = -1

    public val renderer = ViewRenderer(this)
    private var startX = 0f
    private var startY = 0f
    private var previousX = 0f
    private var previousY = 0f

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        setZOrderOnTop(false)
        setEGLContextClientVersion(2)
        setEGLConfigChooser(8, 8, 8, 8, 16, 0)
        holder.setFormat(PixelFormat.RGBA_8888)
        setRenderer(renderer)
        renderMode = RENDERMODE_CONTINUOUSLY
        attrs?.let { retrieveAttrubutes(attrs) }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = event.x
                startY = event.y
                previousX = event.x
                previousY = event.y
            }
            MotionEvent.ACTION_UP -> {
                if (isClick(event)) renderer.resize(event.x, event.y)
                renderer.release()
            }
            MotionEvent.ACTION_MOVE -> {

                val isPrimMoving = isScrollGesture(event, 0, startX, startY)

                if (isPrimMoving) {
                    if (isSwipe(event)) {
                        renderer.swipe(previousX - event.x, previousY - event.y)
                        previousX = event.x
                        previousY = event.y
                    } else {
                        release()
                    }
                }
            }
            else -> release()
        }

        return true
    }

    private fun isScrollGesture(event: MotionEvent, ptrIndex: Int, originalX: Float, originalY: Float): Boolean {
        val moveX = Math.abs(event.getX(ptrIndex) - originalX)
        val moveY = Math.abs(event.getY(ptrIndex) - originalY)

        return ptrIndex < 1 && (moveX > scaledTouchSlop || moveY > scaledTouchSlop)
    }

    private fun release() = postDelayed({ renderer.release() }, 0)

    private fun isClick(event: MotionEvent) = Math.abs(event.x - startX) < 20 && Math.abs(event.y - startY) < 20

    private fun isSwipe(event: MotionEvent) = Math.abs(event.x - previousX) > 20 && Math.abs(event.y - previousY) > 20

    private fun retrieveAttrubutes(attrs: AttributeSet) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.BubblesView)

        if (array.hasValue(R.styleable.BubblesView_maxSelectedCount)) {
            maxSelectedCount = array.getInt(R.styleable.BubblesView_maxSelectedCount, -1)
        }

        if (array.hasValue(R.styleable.BubblesView_backgroundColor)) {
            background = array.getColor(R.styleable.BubblesView_backgroundColor, -1)
        }

        array.recycle()
    }

}
