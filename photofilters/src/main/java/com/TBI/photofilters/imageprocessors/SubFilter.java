package com.TBI.photofilters.imageprocessors;

import android.graphics.Bitmap;

/**
 * TBI Demo.
 */
public interface SubFilter {
    Bitmap process(Bitmap inputImage);

    Object getTag();

    void setTag(Object tag);
}
