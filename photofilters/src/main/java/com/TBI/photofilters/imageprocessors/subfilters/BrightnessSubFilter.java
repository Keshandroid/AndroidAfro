package com.TBI.photofilters.imageprocessors.subfilters;

import android.graphics.Bitmap;

import com.TBI.photofilters.imageprocessors.ImageProcessor;
import com.TBI.photofilters.imageprocessors.SubFilter;


/**
 * TBI Demo
 */
public class BrightnessSubFilter implements SubFilter {
    private static String tag = "";
    // Value is in integer
    private int brightness = 0;

    public BrightnessSubFilter(int brightness) {
        this.brightness = brightness;
    }

    @Override
    public Bitmap process(Bitmap inputImage) {
        return ImageProcessor.doBrightness(brightness, inputImage);
    }

    @Override
    public String getTag() {
        return tag;
    }

    @Override
    public void setTag(Object tag) {
        BrightnessSubFilter.tag = (String) tag;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public void changeBrightness(int value) {
        this.brightness += value;
    }

    public int getBrightness() {
        return brightness;
    }
}
