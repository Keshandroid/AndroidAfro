package com.TBI.photofilters.imageprocessors.subfilters;

import android.graphics.Bitmap;

import com.TBI.photofilters.imageprocessors.ImageProcessor;
import com.TBI.photofilters.imageprocessors.SubFilter;


/**
 * TBI Demo
 */
public class ContrastSubFilter implements SubFilter {

    private static String tag = "";
    private float contrast = 0;

    public ContrastSubFilter(float contrast) {
        this.contrast = contrast;
    }

    @Override
    public Bitmap process(Bitmap inputImage) {
        return ImageProcessor.doContrast(contrast, inputImage);
    }

    @Override
    public String getTag() {
        return tag;
    }

    @Override
    public void setTag(Object tag) {
        ContrastSubFilter.tag = (String) tag;
    }

    public void setContrast(float contrast) {
        this.contrast = contrast;
    }

    public void changeContrast(float value) {
        this.contrast += value;
    }

    public float getContrast() {
        return contrast;
    }
}
