package com.TBI.photofilters.imageprocessors.subfilters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.TBI.photofilters.R;
import com.TBI.photofilters.imageprocessors.SubFilter;


/**
 * TBI Demo
 */
public class VignetteSubFilter implements SubFilter {

    private static String tag = "";
    private Context context;

    // value of alpha is between 0-255
    private int alpha = 0;

    public VignetteSubFilter(Context context, int alpha) {
        this.context = context;
        this.alpha = alpha;
    }

    @Override
    public Bitmap process(Bitmap inputImage) {
        Bitmap vignette = BitmapFactory.decodeResource(context.getResources(), R.drawable.vignette);

        vignette = Bitmap.createScaledBitmap(vignette, inputImage.getWidth(), inputImage.getHeight(), true);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setAlpha(alpha);

        Canvas comboImage = new Canvas(inputImage);
        comboImage.drawBitmap(vignette, 0f, 0f, paint);

        return inputImage;
    }

    @Override
    public Object getTag() {
        return tag;
    }

    @Override
    public void setTag(Object tag) {
        VignetteSubFilter.tag = (String) tag;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public void changeAlpha(int value) {
        this.alpha += value;
        if (alpha > 255) {
            alpha = 255;
        } else if (alpha < 0) {
            alpha = 0;
        }
    }
}
