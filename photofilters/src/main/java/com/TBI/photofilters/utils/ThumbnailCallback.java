package com.TBI.photofilters.utils;

import com.TBI.photofilters.imageprocessors.Filter;

public interface ThumbnailCallback {

    void onThumbnailClick(Filter filter);
}
