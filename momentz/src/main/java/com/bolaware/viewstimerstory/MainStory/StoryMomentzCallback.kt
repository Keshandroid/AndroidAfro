package com.bolaware.viewstimerstory.MainStory

import android.accounts.AccountAuthenticatorActivity
import android.view.View

interface StoryMomentzCallback{
    fun done(finishActivity: Boolean)

    fun onNextCalled(view: View, momentz : StoryMomentz, index: Int)

    fun onNextUserStory()

    fun onPrevUserStory()

    fun onStoryPause()

    fun onStoryResume()

}
