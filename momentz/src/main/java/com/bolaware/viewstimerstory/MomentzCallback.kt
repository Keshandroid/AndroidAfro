package com.bolaware.viewstimerstory

import android.view.View

interface MomentzCallback{
    fun done(finishActivity: Boolean)

    fun onNextCalled(view: View, momentz : Momentz, index: Int)

}
