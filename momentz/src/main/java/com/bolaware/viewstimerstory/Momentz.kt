package com.bolaware.viewstimerstory

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.util.DisplayMetrics
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.progress_story_view.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import androidx.constraintlayout.widget.ConstraintLayout
import java.lang.Exception


class Momentz : ConstraintLayout {
    private var currentlyShownIndex = 0
    private lateinit var currentView: View
    private var momentzViewList: List<MomentzView>
    private var libSliderViewList = mutableListOf<MyProgressBar>()
    private var momentzCallback : MomentzCallback
    private lateinit var view: View
    private val passedInContainerView: ViewGroup
    private var mProgressDrawable : Int = R.drawable.orange_lightgrey_drawable
    private var pausedState : Boolean = false
    lateinit var gestureDetector: GestureDetector


    private var previousFingerPosition = 0
    private var baseLayoutPosition = 0
    private var defaultViewHeight = 0

    private var isClosing = false
    private var isScrollingUp = false
    private var isScrollingDown = false

    constructor(
            context: Context,
            momentzViewList: MutableList<MomentzView>,
            passedInContainerView: ViewGroup,
            momentzCallback: MomentzCallback,
            postPosition: Int

        //@DrawableRes mProgressDrawable : Int = R.drawable.orange_lightgrey_drawable
    ) : super(context) {
        this.momentzViewList = momentzViewList
        this.momentzCallback = momentzCallback
        this.passedInContainerView = passedInContainerView
        this.currentlyShownIndex = postPosition

        //this.mProgressDrawable = mProgressDrawable
        initView()
        init()
    }

    private fun init() {
        momentzViewList.forEachIndexed { index, sliderView ->
            val myProgressBar = MyProgressBar(
                context,
                index,
                sliderView.durationInSeconds,
                object : ProgressTimeWatcher {
                    override fun onEnd(indexFinished: Int) {
                        currentlyShownIndex = indexFinished + 1
                        next()
                    }
                },
                mProgressDrawable)
            libSliderViewList.add(myProgressBar)
            view.linearProgressIndicatorLay.addView(myProgressBar)
        }
        //start()
    }

    fun callPause(pause : Boolean){
        try {
            if(pause){
                if(!pausedState){
                    this.pausedState = !pausedState
                    pause(false)
                }
            } else {
                if(pausedState){
                    this.pausedState = !pausedState
                    resume()
                }
            }
        }catch (e : Exception){
            e.printStackTrace()
        }
    }

    private fun initView() {
        view = View.inflate(context, R.layout.progress_story_view, this)
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        gestureDetector = GestureDetector(context, SingleTapConfirm())

        val touchListener = object  : OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                if (gestureDetector.onTouchEvent(event)) {
                    // single tap
                    if(v?.id == view.rightLay.id){
                        next()
                    } else if(v?.id == view.leftLay.id){
                        prev()
                    }
                    return true
                } else {
                    // your code for move and drag
                    /*when(event?.action){
                        MotionEvent.ACTION_DOWN -> {
                            //callPause(true)
                            return true
                        }

                        MotionEvent.ACTION_UP -> {
                            //callPause(false)
                            return true
                        }
                        else -> return false
                    }*/






                    //========New Code for swipe down gestures ===============

                    // Get finger position on screen
                    val Y = event!!.rawY.toInt()

                    when (event!!.action and MotionEvent.ACTION_MASK) {
                        MotionEvent.ACTION_DOWN -> {
                            // save default base layout height
                            defaultViewHeight = view.container.getHeight()

                            // Init finger and view position
                            previousFingerPosition = Y
                            baseLayoutPosition = view.container.getY().toInt()

                            /*holdAndPauseStory()
                            return true*/
                        }
                        MotionEvent.ACTION_UP -> {
                            // If user was doing a scroll up
                            if (isScrollingUp) {
                                // Reset baselayout position
                                view.container.setY(0f)
                                // We are not in scrolling up mode anymore
                                isScrollingUp = false
                            }

                            // If user was doing a scroll down
                            if (isScrollingDown) {
                                // Reset baselayout position
                                view.container.setY(0f)
                                // Reset base layout size
                                view.container.getLayoutParams().height = defaultViewHeight
                                view.container.requestLayout()
                                // We are not in scrolling down mode anymore
                                isScrollingDown = false
                            }

                            /*releaseAndResumeStory()
                            return true*/
                        }
                        MotionEvent.ACTION_MOVE -> if (!isClosing) {
                            val currentYPosition = view.container.getY().toInt()

                            // If we scroll up
                            if (previousFingerPosition > Y) {
                                // First time android rise an event for "up" move
                                if (!isScrollingUp) {
                                    isScrollingUp = true
                                }

                                // Has user scroll down before -> view is smaller than it's default size -> resize it instead of change it position
                                if (view.container.getHeight() < defaultViewHeight) {
                                    view.container.getLayoutParams().height = view.container.getHeight() - (Y - previousFingerPosition)
                                    view.container.requestLayout()
                                } else {
                                    // Has user scroll enough to "auto close" popup ?
                                    if (baseLayoutPosition - currentYPosition > defaultViewHeight / 3) {
                                        closeUpAndDismissDialog(currentYPosition)
                                        return true
                                    }

                                    //
                                }
                                view.container.setY(view.container.getY() + (Y - previousFingerPosition))
                            } else {

                                // First time android rise an event for "down" move
                                if (!isScrollingDown) {
                                    isScrollingDown = true
                                }

                                // Has user scroll enough to "auto close" popup ?
                                if (Math.abs(baseLayoutPosition - currentYPosition) > defaultViewHeight / 3) {
                                    closeDownAndDismissDialog(currentYPosition)
                                    return true
                                }

                                // Change base layout size and position (must change position because view anchor is top left corner)
                                view.container.setY(view.container.getY() + (Y - previousFingerPosition))
                                view.container.getLayoutParams().height = view.container.getHeight() - (Y - previousFingerPosition)
                                view.container.requestLayout()
                            }

                            // Update position
                            previousFingerPosition = Y
                        }
                    }
                    return true






                }
            }
        }

//        view.leftLay.setOnClickListener { prev() }
//        view.rightLay.setOnClickListener { next() }
        view.leftLay.setOnTouchListener(touchListener)
        view.rightLay.setOnTouchListener(touchListener)
        //view.container.setOnTouchListener(touchListener)

        this.layoutParams = params
        passedInContainerView.addView(this)
    }

    fun show() {
        view.loaderProgressbar.visibility = View.GONE
        if (currentlyShownIndex != 0) {
            for (i in 0..Math.max(0, currentlyShownIndex - 1)) {
                libSliderViewList[i].progress = 100
                libSliderViewList[i].cancelProgress()
            }
        }

        if (currentlyShownIndex != libSliderViewList.size - 1) {
            for (i in currentlyShownIndex + 1..libSliderViewList.size - 1) {
                libSliderViewList[i].progress = 0
                libSliderViewList[i].cancelProgress()
            }
        }

        currentView = momentzViewList[currentlyShownIndex].view

        libSliderViewList[currentlyShownIndex].startProgress()

        momentzCallback.onNextCalled(currentView, this, currentlyShownIndex)

        view.currentlyDisplayedView.removeAllViews()
        view.currentlyDisplayedView.addView(currentView)
        val params = LinearLayout.LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT, 1f
        )
        //params.gravity = Gravity.CENTER_VERTICAL
        if(currentView is ImageView) {
            (currentView as ImageView).scaleType = ImageView.ScaleType.FIT_CENTER
            (currentView as ImageView).adjustViewBounds = true
        }
        currentView.layoutParams = params
    }

    fun start() {
//            Handler().postDelayed({
//                show()
//            }, 2000)
        show()
    }

    fun editDurationAndResume(index: Int, newDurationInSecons : Int){
        view.loaderProgressbar.visibility = View.GONE
        libSliderViewList[index].editDurationAndResume(newDurationInSecons)
    }

    fun pause(withLoader : Boolean) {
        if(withLoader){
            view.loaderProgressbar.visibility = View.VISIBLE
        }
        libSliderViewList[currentlyShownIndex].pauseProgress()
        /*if(momentzViewList[currentlyShownIndex].view is VideoView){
            (momentzViewList[currentlyShownIndex].view as VideoView).pause()
        }*/
    }

    fun resume() {
        view.loaderProgressbar.visibility = View.GONE
        libSliderViewList[currentlyShownIndex].resumeProgress()
        /*if(momentzViewList[currentlyShownIndex].view is VideoView){
            (momentzViewList[currentlyShownIndex].view as VideoView).start()
        }*/
    }

    private fun stop() {

    }

    fun next() {
        try {
            if (currentView == momentzViewList[currentlyShownIndex].view) {
                currentlyShownIndex++
                if (momentzViewList.size <= currentlyShownIndex) {
                    finish(true)
                    return
                }
            }
            show()
        } catch (e: IndexOutOfBoundsException) {
            finish(true)
        }
    }

    /*private fun finish() {
        momentzCallback.done()
        for (progressBar in libSliderViewList) {
            progressBar.cancelProgress()
            progressBar.progress = 100
        }
    }*/

    private fun finish(finishActivity: Boolean) {
        momentzCallback.done(finishActivity)
        for (progressBar in libSliderViewList) {
            progressBar.cancelProgress()
            progressBar.progress = 100
        }
    }

    fun prev() {
        try {
            if (currentView == momentzViewList[currentlyShownIndex].view) {
                currentlyShownIndex--
                if (0 > currentlyShownIndex) {
                    currentlyShownIndex = 0
                }
            }
        } catch (e: IndexOutOfBoundsException) {
            currentlyShownIndex -= 2
        } finally {
            show()
        }
    }

    fun closeUpAndDismissDialog(currentPosition: Int) {
        isClosing = true
        val positionAnimator = ObjectAnimator.ofFloat(view.container, "y", currentPosition.toFloat(), -view.container.getHeight().toFloat())
        positionAnimator.duration = 300
        positionAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                finish(true)
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })
        positionAnimator.start()
    }

    fun closeDownAndDismissDialog(currentPosition: Int) {
        isClosing = true

        //New
        val metrics = DisplayMetrics()
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager?.defaultDisplay?.getMetrics(metrics)
        val screenHeight = metrics.heightPixels

        //Original
        /*val display: Display = getWindowManager().getDefaultDisplay()
        val size = Point()
        display.getSize(size)
        val screenHeight = size.y*/

        val positionAnimator = ObjectAnimator.ofFloat(view.container, "y", currentPosition.toFloat(), screenHeight + view.container.getHeight().toFloat())
        positionAnimator.duration = 300
        positionAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                finish(true)
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })
        positionAnimator.start()
    }

    private inner class SingleTapConfirm : SimpleOnGestureListener() {

        override fun onSingleTapUp(event: MotionEvent): Boolean {
            return true
        }
    }


}